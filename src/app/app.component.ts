import { ConfirmationService } from 'primeng/api';
import { Component } from '@angular/core';
import { LocaleService, TranslationService, Language } from 'angular-l10n';
import { MessageService } from 'primeng/api';
import { HelperService } from './shared/services/helper.service';
import * as moment from "moment"
import { CommonUtils } from './shared/services';
declare global {
  interface Window { TwoFactor: any; }
}
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  providers: [MessageService]
})
export class AppComponent {
  title = 'vhr-web-app';
  public blocked = false;
  public timer = '00:00:00:00';
  @Language() lang: string;
  constructor(public locale: LocaleService
    , public translation: TranslationService
    , public helperService: HelperService
    , private confirmationService: ConfirmationService
    , private messageService: MessageService
  ) {
    this.helperService.APP_TOAST_MESSAGE.subscribe(data => {
      if (data && data.value && data.type.toLowerCase() === 'error') {
        this.messageError(data.type, data.code, data.value);
        return;
      }
      this.processReturnMessage(data);
    });
    if (!this.helperService.APP_CONFIRM_DELETE.getValue()) {
      this.helperService.APP_CONFIRM_DELETE.subscribe(data => {
        if (data && data['accept']) {
          this.confirmDelete(data['messageCode'], data['accept']);
        }
      });
    }
    this.helperService.APP_SHOW_PROCESSING.subscribe(isProcessing => {
      this.isProcessing(isProcessing);
    });
  }
  public selectLanguage(language: string): void {
    this.locale.setCurrentLanguage(language);
  }
  public getConfirmationService(): ConfirmationService {
    return this.confirmationService;
  }

  /**
   * confirmMessage
   */
  confirmMessage(messageCode: string, accept: Function, reject: Function) {
    const message = this.translation.translate(messageCode || 'common.message.confirm.save');
    const header = this.translation.translate('common.message.confirmation');
    const btnSave = this.translation.translate('common.button.yes');
    const btnCancel = this.translation.translate('common.button.no');
    return this.confirmationService.confirm({
      message: message,
      header: header,
      icon: 'pi pi-exclamation-triangle',
      accept: accept,
      reject: reject,
      acceptLabel: btnSave,
      rejectLabel: btnCancel
    });
  }

  /**
   * confirmMessage
   */
  confirmMessageError(messageCode: string, accept: Function, reject: Function, valueError?: any) {
    const message = valueError + ' ' + this.translation.translate(messageCode || 'common.message.confirm.save');
    const header = this.translation.translate('common.message.confirmation');
    const btnSave = this.translation.translate('common.button.yes');
    const btnCancel = this.translation.translate('common.button.no');
    return this.confirmationService.confirm({
      message: message,
      header: header,
      icon: 'pi pi-exclamation-triangle',
      accept: accept,
      reject: reject,
      acceptLabel: btnSave,
      rejectLabel: btnCancel
    });
  }
  /**
   * confirmDelete
   */
  confirmDelete(messageCode: string, accept: Function, reject?: Function) {
    if (!accept) {
      return;
    }
    if (!reject) {
      reject = () => {
        return false;
      };
    }
    const message = this.translation.translate(messageCode || 'common.message.confirm.delete');
    const header = this.translation.translate('common.message.deleteConfirmation');
    const btnSave = this.translation.translate('common.button.yes');
    const btnCancel = this.translation.translate('common.button.no');
    return this.confirmationService.confirm({
      message: message,
      header: header,
      icon: 'pi pi-info-circle',
      accept: accept,
      reject: reject,
      acceptLabel: btnSave,
      rejectLabel: btnCancel
    });
  }
  /**
   * successMessage
   * param errorType
   * param errorCode
   */
  successMessage(code: string, message?: string) {
    this.toastMessage('SUCCESS', code, message);
  }
  /**
   * errorMessage
   * param errorType
   * param errorCode
   */
  errorMessage(code: string, message?: string) {
    this.toastMessage('ERROR', code, message);
  }
  /**
   * warningMessage
   * param errorType
   * param errorCode
   */
  warningMessage(code: string, message?: string) {
    this.toastMessage('WARNING', code, message);
  }
  /**
   * toastMessage
   * param severity
   * param errorType
   * param errorCode
   */
  public toastMessage(severity: string, code: string, message?: string) {
    let detail;
    message = severity === 'CONFIRM' ? null : message;
    severity = severity === 'CONFIRM' ? 'WARNING' : severity;
    if (!message) {
      detail = this.translation.translate(`${severity}.${code}`);
    } else {
      detail = this.translation.translate(`${severity}.${code}.${message}`);
    }
    severity = severity === 'WARNING' ? 'WARN' : severity;
    const summary = this.translation.translate(`app.messageSummary`);
    this.messageService.add({ severity: severity.toLowerCase(), summary: summary, detail: detail });
  }
  public message(severity: string, text: string) {
    this.messageService.add({ severity: severity.toLowerCase(), summary: this.translation.translate(`app.messageSummary`), detail: text });
  }
  public messageError(severity: string, text: string, value: any) {
    const message = this.translation.translate(text);
    const textDetail = message + ' ' + value;
    this.messageService.add({
      severity: severity.toLowerCase()
      , summary: this.translation.translate(`app.messageSummary`)
      , detail: textDetail
    });
  }
  public validateMessenger(keyTranslate: string, value?: any): string {
    var text = this.translation.translate(keyTranslate);
    let dateFormat = CommonUtils.getDateFormat().toUpperCase();;
    for (const key in value) {
      let t = value[key] && key.toLowerCase().indexOf('date') !== -1 ? moment(new Date(value[key])).format(dateFormat) : value[key];
      text = text.replace(new RegExp('\\$\\{' + key + '\\}', 'g'), t == null || t == undefined ? "" : t)
    }
    text = text.replace(new RegExp('\\$\\{(.*?)\\}', 'g'), "")
    return text;
  }

  public messError(severity: string, text: string, valueError?: any) {
    const message = this.translation.translate(text);
    const textDetail = (valueError ? valueError + ' ' : '') + message;
    this.messageService.add({
      severity: severity.toLowerCase()
      , summary: this.translation.translate(`app.messageSummary`)
      , detail: textDetail
    });
  }
  /**
   * process return message
   * param serviceResponse
   */
  public processReturnMessage(serviceResponse: any) {
    if (!serviceResponse) {
      return;
    }
    if (serviceResponse.status === 500 || serviceResponse.status === 0) {
      this.errorMessage('haveError');
      return;
    }
    if (serviceResponse.code && serviceResponse.type) {
      this.toastMessage(serviceResponse.type, serviceResponse.code, serviceResponse.message);
      return;
    }
  }
  /**
   * request is success
   */
  public requestIsError(): void {
    this.toastMessage('ERROR', 'haveError');
  }
  public isProcessing(isProcessing: boolean) {
    if (this.blocked && !isProcessing) {
      setTimeout(() => {
        this.blocked = isProcessing;
        this.updateViewChange();
      }, 500);
    } else if (!this.blocked && isProcessing == true) {
      this.blocked = isProcessing;
      this.updateViewChange();
    }
  }
  private updateViewChange() {
    const progressSpinnerCheck = document.getElementById('progressSpinnerCheck');
    if (progressSpinnerCheck) {
      this.countTime();
      document.getElementById('progressSpinnerCheck').className = this.blocked ? 'progressing' : '';
    }
  }

  countTime() {
    // Set the date we're counting down to
    var distance = 0;
    if (!this.blocked) {
      return;
    }
    var that = this;
    // Update the count down every 1 second
    var x = setInterval(function () {

      // Find the distance between now and the count down date
      distance += 1000;

      // Time calculations for days, hours, minutes and seconds
      var days = Math.floor(distance / (1000 * 60 * 60 * 24));
      var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((distance % (1000 * 60)) / 1000);

      var daysStr = days < 10 ? "0" + days : days;
      var hoursStr = hours < 10 ? "0" + hours : hours;
      var minutesStr = minutes < 10 ? "0" + minutes : minutes;
      var secondsStr = seconds < 10 ? "0" + seconds : seconds;

      // Output the result in an element with id="demo"
      that.timer = daysStr + ":" + hoursStr + ":" + minutesStr + ":" + secondsStr;
      // If the count down is over, write some text
      // if (hours === 1) { //response quá 1 tiếng.
      //   clearInterval(x);
      //   that.blocked = false;
      //   this.errorMessage('haveError');
      // }
      if (!that.blocked) {
        clearInterval(x);
        that.timer = '00:00:00:00';
      }
    }, 1000);
  }
}
