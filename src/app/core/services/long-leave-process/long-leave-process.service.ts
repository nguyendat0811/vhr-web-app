import { HelperService } from '../../../shared/services/helper.service';
import { HttpClient } from '@angular/common/http';
import { BasicService } from '../basic.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LongLeaveProcessService extends BasicService {
  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('emp', 'longLeaveProcess', httpClient, helperService);
  }

  public getInforLongLeaveProcessStillValidated(employeeId: number) {
    const url = `${this.serviceUrl}/infor-long-leave-process-still-validated/${employeeId}`;
    return this.getRequest(url);
  }
}

