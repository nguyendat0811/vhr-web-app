import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';
import { CommonUtils } from '@app/shared/services';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PartyUnionService extends BasicService {

  constructor(public httpClient: HttpClient
    , public helperService: HelperService) {
    super('emp', 'partyUnion', httpClient, helperService)
  }

  public findByEmployeeId(employeeId: number) {
    const emp = CommonUtils.nvl(employeeId);
    const url = `${this.serviceUrl}/${emp}/other-partys`;
    return this.getRequest(url);
  }


  public findUnionByEmployeeId(employeeId: number) {
    const emp = CommonUtils.nvl(employeeId);
    const url = `${this.serviceUrl}/${emp}/party-union-updates`;
    return this.getRequest(url);
  }

}
