import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class AllowanceTypeService extends BasicService {
  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('emp', 'allowanceType', httpClient, helperService);
  }
  public findAllowanceTypeList(): Observable<any> {
    const urlStatus = `${this.serviceUrl}/allowance-type-by-market-id`;
    return this.getRequest(urlStatus);
  }

  public findById(allowanceTypeId: number): Observable<any>{
    const urlStatus = `${this.serviceUrl}/${allowanceTypeId}`;
    return this.getRequest(urlStatus);
  }
}
