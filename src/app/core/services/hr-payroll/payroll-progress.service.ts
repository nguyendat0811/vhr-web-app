import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';
import { Observable } from 'rxjs';
import { CommonUtils } from '@app/shared/services';

@Injectable({
  providedIn: 'root'
})
export class PayrollProgressService extends BasicService {
  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('payroll', 'payrollMaster', httpClient, helperService)
  }
  public getList(): Observable<any> {
    const url = `${this.serviceUrl}/search`;
    return this.getRequest(url);
  }
  
  public getPercent(data?: any): Observable<any> {
    const searchData = CommonUtils.convertData(data);
    if (event) {
      searchData._search = event;
    }
    const buildParams = CommonUtils.buildParams(searchData);
    const url = `${this.serviceUrl}/get-percent?`;
    return this.getRequest(url, {params: buildParams});
  }

  public processFilterData(formSearch?, event?): Observable<any> {
    const params = formSearch ? formSearch.value : null;
    delete params._search;
    if (!event) {
      this.credentials = Object.assign({}, params);
    }

    const searchData = CommonUtils.convertData(this.credentials);
    if (event) {
      searchData._search = event;
    }
    const buildParams = CommonUtils.buildParams(searchData);
    const url = `${this.serviceUrl}/search?`;
    return this.getRequest(url, {params: buildParams});
  }
}
