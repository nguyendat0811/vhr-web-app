import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SalaryOrganizationMappingService extends BasicService {

  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('payroll', 'salaryOrganizationMapping', httpClient, helperService);
  }

  /**
   * getMainProcessByEmployeeId
   */
  public getData(orgId?: number) {
    const url = `${this.serviceUrl}/get-data-table/${orgId}`;
    return this.getRequest(url);
  }
   /**
   * getgetDataFromConfig
   */
  public getDataFromConfig(orgId?: number, salaryTypeId?: any) {
    let url = `${this.serviceUrl}/get-data-config/${orgId}`;
    if(salaryTypeId !== null && salaryTypeId !== ""){
      url += `/${salaryTypeId}`;
    }else{
      url += `/0`;
    }
    return this.getRequest(url);
  }
}
