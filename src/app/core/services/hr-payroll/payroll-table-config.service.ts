import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PayrollTableConfigService extends BasicService{

  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('payroll', 'payrollTableConfig', httpClient, helperService);
  }

  public getListPayrollItem(organizationId: number, salaryTypeId: number): Observable<any> {
    const url = `${this.serviceUrl}/list-payroll-item/${organizationId}/${salaryTypeId}`;
    return this.getRequest(url);
  }

  public getListPayrollItemConfig(organizationId: number, salaryTypeId: number): Observable<any> {
    const url = `${this.serviceUrl}/list-payroll-item-config/${organizationId}/${salaryTypeId}`;
    return this.getRequest(url);
  }
  
  public getListPayrollItemConfigV2(organizationIds, salaryTypeId: number): Observable<any> {
    const url = `${this.serviceUrl}/list-payroll-item-config-v2/${organizationIds}/${salaryTypeId}`;
    return this.getRequest(url);
  }
}


