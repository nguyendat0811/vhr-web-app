import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';
import { Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class XnkNotificationService extends BasicService {

  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('payroll', 'xnkNotification', httpClient, helperService);
  }

  public getList(): Observable<any> {
    const url = `${this.serviceUrl}/search`;
    return this.getRequest(url);
  }

  public getActiveNotification(): Observable<any> {
    const url = `${this.serviceUrl}/findActiveNotification`;
    return this.getRequest(url);
  }
}
