import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';
import { Observable } from 'rxjs';
import { CommonUtils } from '@app/shared/services/common-utils.service';

@Injectable({
  providedIn: 'root'
})
export class PayslipService extends BasicService {
  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('payroll', 'payslip', httpClient, helperService);
  }

  public processSendPayslip(data) {
    const url = `${this.serviceUrl}/send-payslip`;
    return this.postRequest(url, CommonUtils.convertData(data));
  }

  getLinkViewDetail(form: any) {
    let url = `${this.serviceUrl}/view-detail?`;
    const esc = encodeURIComponent;
    const query = Object.keys(form)
    .map(k => `${esc(k)}=${esc(form[k])}`)
    .join('&');
    return url + query;
  }
  public processGetDetail(params: string) {
    const url = `${this.serviceUrl}/get-detail-payslip?params=${params}`;
    return this.getRequest(url);
  }

  public buildUrlViewDetail(params: string, payrollGeneralId: number) {
    const url = `${this.serviceUrl}/view-detail-payslip?params=${params}&payrollGeneralId=${payrollGeneralId}`;
    return url;
  }

  public downloadPDF(params: string, payrollGeneralId: number) {
    const url = `${this.serviceUrl}/view-detail-payslip?params=${params}&payrollGeneralId=${payrollGeneralId}`;
    return this.getRequest(url, {responseType: 'blob'});
  }

  public downloadDocx(form: any) : Observable<any>{
    let url = `${this.serviceUrl}/download-detail-payslip?`;
    const esc = encodeURIComponent;
    const query = Object.keys(form)
    .map(k => `${esc(k)}=${esc(form[k])}`)
    .join('&');
    url = url + query;
    return this.getRequest(url, {responseType: 'blob'});
  }
}
