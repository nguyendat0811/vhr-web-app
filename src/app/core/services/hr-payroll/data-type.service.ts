import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';
import { CommonUtils } from '@app/shared/services';
import { Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataTypeService extends BasicService {

  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('payroll', 'dataType', httpClient, helperService);
  }

  /**
   * getDataTypeIsImport
   */
  public getDataTypeIsImport() {
    const url = `${this.serviceUrl}/get-data-type-is-import`;
    return this.getRequest(url);
  }

  /**
   * find list data specification by dataTypeId
   */
  public findDataSpecification(id: number) {
    const dataTypeId = CommonUtils.nvl(id);
    const url = `${this.serviceUrl}/${dataTypeId}/data-specification`;
    return this.getRequest(url);
  }

  /**
   * find list column from table data_input_process
   */
  public findListMainTableColumnName() {
    const url = `${this.serviceUrl}/main-table-column`;
    return this.getRequest(url);
  }

  /**
   * find list column from table data_input_process_general
   */
  public findListSubTableColumnName() {
    const url = `${this.serviceUrl}/sub-table-column`;
    return this.getRequest(url);
  }

  public getListConfigBySalary(salaryTypeId) {
    const url = `${this.serviceUrl}/get-list-config-by-salary/${salaryTypeId}`;
    return this.getRequest(url);
  }

  public getBeanById(dataTypeId) {
    const url = `${this.serviceUrl}/get-bean-by-id/${dataTypeId}`;
    return this.getRequest(url);
  }

  public generateSpecdata(data) {
    const url = `${this.serviceUrl}/generate-specdata`;
    return this.postRequest(url, CommonUtils.convertData(data));
  }
  
}
