import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class XnkPayrollConfigService extends BasicService {

  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('payroll', 'xnkPayrollConfig', httpClient, helperService);
  }

  public getListPayrollItem(configType: string, salaryTypeId: number): Observable<any> {
    const url = `${this.serviceUrl}/list-payroll-item/${configType}/${salaryTypeId}`;
    return this.getRequest(url);
  }

  public getListPayrollItemConfig(configType: string, salaryTypeId: number): Observable<any> {
    const url = `${this.serviceUrl}/list-payroll-item-config/${configType}/${salaryTypeId}`;
    return this.getRequest(url);
  }
}
