import { HelperService } from '@app/shared/services/helper.service';
import { HttpClient } from '@angular/common/http';
import { BasicService } from '@app/core/services/basic.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FormulaDiagramService extends BasicService {

  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('payroll', 'formulaDiagram', httpClient, helperService);
  }
  public findByPayrollDiagramId(payrollDiagramId): Observable<any> {
    const url = `${this.serviceUrl}/get-list-detail/${payrollDiagramId}`;
    return this.getRequest(url);
  }

  public findAllDfFormulaId(): Observable<any> {
    const url = `${this.serviceUrl}/find-all-formula`;
    return this.getRequest(url);
  }
}
