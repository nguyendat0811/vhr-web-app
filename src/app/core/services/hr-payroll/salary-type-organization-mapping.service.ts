import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SalaryTypeOrganizationMappingService extends BasicService {

  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('payroll', 'salaryTypeOrganizationMapping', httpClient, helperService);
  }

  /**
   * getData
   */
  public getData(orgId?: number) {
    const url = `${this.serviceUrl}/get-data-table/${orgId}`;
    return this.getRequest(url);
  }

  /**
   * getData
   */
  public findByOrgId(orgId?: number) {
    const url = `${this.serviceUrl}/find-by-orgId/${orgId}`;
    return this.getRequest(url);
  }

  /**
   * getData
   */
  public findByOrgParentId(orgId?: number) {
    const url = `${this.serviceUrl}/find-by-orgParentId/${orgId}`;
    return this.getRequest(url);
  }

  /**
   * getData
   */
  public findByOrgIds(orgIds) {
    const url = `${this.serviceUrl}/find-by-orgIds/${orgIds}`;
    return this.getRequest(url);
  }
  
}
