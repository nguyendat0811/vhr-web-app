import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';
import { Observable } from 'rxjs';


@Injectable({
    providedIn: 'root'
})
export class PayrollGroupService extends BasicService {

    constructor(public httpClient: HttpClient, public helperService: HelperService) {
        super('payroll', 'payrollGroup', httpClient, helperService);
    }

    public findAll(): Observable<any> {
        const url = `${this.serviceUrl}/find-all`;
        return this.getRequest(url);
    }
}
