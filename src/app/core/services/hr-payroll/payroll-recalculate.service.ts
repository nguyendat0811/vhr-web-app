import { HelperService } from '@app/shared/services/helper.service';
import { HttpClient } from '@angular/common/http';
import { BasicService } from '@app/core/services/basic.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CommonUtils } from '@app/shared/services';
import { tap, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PayrollRecalculateService extends BasicService {

  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('payroll', 'payrollReCalculate', httpClient, helperService);
  }

  public actionRunProcess(item: any): Observable<any>{
    const formdata = CommonUtils.convertData(item);
    const url = `${this.serviceUrl}/check-change`;
    this.helperService.isProcessing(true);
    return this.httpClient.post(url, formdata)
    .pipe(
      tap( // Log the result or error
        res => {
          this.helperService.APP_TOAST_MESSAGE.next(res);
          this.helperService.isProcessing(false);
        },
        error => {
          this.helperService.APP_TOAST_MESSAGE.next(error);
          this.helperService.isProcessing(false);
        }
      ),
      catchError(this.handleError)
    );
  }

  public getDetailChange(item: any): Observable<any>{
    const formdata = CommonUtils.convertData(item);
    const url = `${this.serviceUrl}/get-detail-change`;
    this.helperService.isProcessing(true);
    return this.httpClient.post(url, formdata)
    .pipe(
      tap( // Log the result or error
        res => {
          this.helperService.APP_TOAST_MESSAGE.next(res);
          this.helperService.isProcessing(false);
        },
        error => {
          this.helperService.APP_TOAST_MESSAGE.next(error);
          this.helperService.isProcessing(false);
        }
      ),
      catchError(this.handleError)
    );
  }

  public runRecalculate(item: any): Observable<any>{
    const formdata = CommonUtils.convertData(item);
    const url = `${this.serviceUrl}/recalculate`;
    this.helperService.isProcessing(true);
    return this.httpClient.post(url, formdata)
    .pipe(
      tap( // Log the result or error
        res => {
          this.helperService.APP_TOAST_MESSAGE.next(res);
          this.helperService.isProcessing(false);
        },
        error => {
          this.helperService.APP_TOAST_MESSAGE.next(error);
          this.helperService.isProcessing(false);
        }
      ),
      catchError(this.handleError)
    );
  }
}
