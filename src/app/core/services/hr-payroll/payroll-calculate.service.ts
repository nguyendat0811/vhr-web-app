import { HelperService } from '@app/shared/services/helper.service';
import { HttpClient } from '@angular/common/http';
import { BasicService } from '@app/core/services/basic.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CommonUtils } from '@app/shared/services';
import { tap, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PayrollCalculateService extends BasicService {

  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('payroll', 'payrollCalculate', httpClient, helperService);
  }

  public actionRunProcess(item: any): Observable<any>{
    const formdata = CommonUtils.convertData(item);
    const url = `${this.serviceUrl}`;
    return this.httpClient.post(url, formdata)
    .pipe(
      tap( // Log the result or error
        res => {
          
        },
        error => {
          this.helperService.APP_TOAST_MESSAGE.next(error);
        }
      ),
      catchError(this.handleError)
    );
  }

  public actionDestroyProcess(item: any): Observable<any>{
    const formdata = CommonUtils.convertData(item);
    const url = `${this.serviceUrl}/destroy-payroll`;
    return this.httpClient.post(url, formdata)
    .pipe(
      tap( // Log the result or error
        res => {
          //this.helperService.APP_TOAST_MESSAGE.next(res);
        },
        error => {
          this.helperService.APP_TOAST_MESSAGE.next(error);
        }
      ),
      catchError(this.handleError)
    );
  }
  public getListDataCalculate(salaryTypeId) {
    const url = `${this.serviceUrl}/data-type-calculate/${salaryTypeId}`;
    return this.getRequest(url);
  }

  public processExport(data): Observable<any> {
    const url = `${this.serviceUrl}/export`;
    return this.getRequest(url, {params: data, responseType: 'blob'});
  }

  public reportBySalatyType(data): Observable<any> {
    const url = `${this.serviceUrl}/report-by-salaty-type`;
    return this.getRequest(url, {params: data, responseType: 'blob'});
  }

  public getListConfigIgnored(orgId, organizationRootId, salaryTypeId): Observable<any> {
    const url = `${this.serviceUrl}/get-list-config-ignored/${orgId}/${organizationRootId}/${salaryTypeId}`;
    return this.getRequest(url);
  }

  public processSaveOrUpdatePayrolGeneral(item: any): Observable<any>  {
    const url =  `${this.serviceUrl}/save-or-update-payroll-general`;
    return this.postRequest(url, CommonUtils.convertData(item));
  }

  public findByPayrollGeneralId(payrollGeneralId) {
    const url = `${this.serviceUrl}/find-by-payroll-general-id/${payrollGeneralId}`;
    return this.getRequest(url);
  }

  public processValidateImport(data): Observable<any>  {
    const url =  `${this.serviceUrl}/validate-import-data-input`;
    const formdata = CommonUtils.convertFormFile(data);
    return this.postRequest(url, formdata);
  }

  public downloadTemplateImport(data): Observable<any> {
    const url = `${this.serviceUrl}/download-template-import`;
    return this.getRequest(url, {params: data, responseType: 'blob'});
  }

  public processImport(data): Observable<any>  {
    const url =  `${this.serviceUrl}/import-data-input`;
    const formdata = CommonUtils.convertFormFile(data);
    return this.postRequest(url, formdata);
  }

  public getListIncomeItem(periodId): Observable<any>{
    const url = `${this.serviceUrl}/get-list-income/${periodId}`;
    return this.getRequest(url);
  }

  public getListPaymentItem(periodId): Observable<any>{
    const url = `${this.serviceUrl}/get-list-payment-item/${periodId}`;
    return this.getRequest(url);
  }

  public getListDeclarePeriod(periodId): Observable<any>{
    const url = `${this.serviceUrl}/get-list-declare-period/${periodId}`;
    return this.getRequest(url);
  }
  public processSyncTax(item: any): Observable<any>  {
    const url =  `${this.serviceUrl}/syncTax`;
    return this.postRequest(url, CommonUtils.convertData(item));
  }
  public processSyncPayment(item: any): Observable<any>  {
    const url =  `${this.serviceUrl}/syncPayment`;
    return this.postRequest(url, CommonUtils.convertData(item));
  }
  public cancelSyncPayment(item: any): Observable<any>  {
    const url =  `${this.serviceUrl}/cancel-payment`;
    return this.postRequest(url, CommonUtils.convertData(item));
  }
  public getTotalSalaryByOrg(orgId, salaryTypeId, periodId): Observable<any>  {
    const url =  `${this.serviceUrl}/get-total-salary-by-org/${orgId}/${salaryTypeId}/${periodId}`;
    return this.httpClient.get(url);
  }

  public prepareTaxSync(item: any): Observable<any>{
    const formdata = CommonUtils.convertData(item);
    const url = `${this.serviceUrl}/perpare-taxsync`;
    return this.httpClient.post(url, formdata)
    .pipe(
      tap( // Log the result or error
        res => {
          
        },
        error => {
          this.helperService.APP_TOAST_MESSAGE.next(error);
        }
      ),
      catchError(this.handleError)
    );
  }
  
  public checkBeforeShowDelete(data): Observable<any>  {
    const url =  `${this.serviceUrl}/check-before-show-delete`;
    return this.httpClient.post(url, data);
  }

  public deletePayrollGeneral(payrollGeneralId): Observable<any> {
    const url =  `${this.serviceUrl}/delete-payroll-general/${payrollGeneralId}`;
    return this.deleteRequest(url);
  }

  public actionExportProcess(item: any): Observable<any>{
    const formdata = CommonUtils.convertData(item);
    const url = `${this.serviceUrl}/export-process-calculate`;
    return this.postRequestFile(url, formdata);
  }

  public deleteDataImport(data: any): Observable<any> {
    const buildParams = CommonUtils.buildParams(data);
    const url = `${this.serviceUrl}/process-delete-data-import?`;
    return this.getRequest(url, {params: buildParams});
  }
}
