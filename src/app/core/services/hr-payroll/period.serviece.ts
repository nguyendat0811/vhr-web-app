import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';
import { Observable } from 'rxjs';
import { CommonUtils } from '@app/shared/services/common-utils.service';

@Injectable({
providedIn: 'root'
})
export class PeriodService extends BasicService {

constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('payroll', 'period', httpClient, helperService);
}

    public getList(): Observable<any> {
    const url = `${this.serviceUrl}/search`;
    return this.getRequest(url);
    }

    /**
   * findByYear
   */
  public findByYear(year: any) {
    const years = year;
    const url = `${this.serviceUrl}/find-by-year/${years}`;
    return this.getRequest(url);
  }
  
  public getDataByOrgIdAndYear(data?: any): Observable<any> {
    const buildParams = CommonUtils.buildParams(data);
    const url = `${this.serviceUrl}/find-by-orgId-and-year`;
    return this.getRequest(url, {params: buildParams});
  }

  public getDataByOrgIdsAndYear(data?: any): Observable<any> {
    const buildParams = CommonUtils.buildParams(data);
    const url = `${this.serviceUrl}/find-by-orgIds-and-year`;
    return this.getRequest(url, {params: buildParams});
  }

  public getDataByOrgParentIdAndYear(data?: any): Observable<any> {
    const buildParams = CommonUtils.buildParams(data);
    const url = `${this.serviceUrl}/find-by-orgParentId-and-year`;
    return this.getRequest(url, {params: buildParams});
  }

  public getParamByPeriodId(periodId: number) {
    return this.getRequest(`${this.serviceUrl}/${periodId}/get-param`);
  }

  public deleteParam(periodParamId: number) {
    return this.deleteRequest(`${this.serviceUrl}/delete-param/${periodParamId}`);
  }

  public closing(periodId: number) {
    return this.getRequest(`${this.serviceUrl}/closing/${periodId}`);
  }
}
