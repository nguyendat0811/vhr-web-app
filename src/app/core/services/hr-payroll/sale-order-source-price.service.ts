import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';
import { Observable } from 'rxjs';
import { CommonUtils } from '@app/shared/services/common-utils.service';


@Injectable({
    providedIn: 'root'
})
export class SaleOrderSourcePriceService extends BasicService {

    constructor(public httpClient: HttpClient, public helperService: HelperService) {
        super('payroll', 'saleOrderSourcePrice', httpClient, helperService);
    }

    public getList(): Observable<any> {
        const url = `${this.serviceUrl}/search`;
        return this.getRequest(url);
    }

    public processExport(data): Observable<any> {
        const url = `${this.serviceUrl}/export`;
        const buildParams = CommonUtils.buildParams(data);
        return this.getRequest(url, { params: buildParams, responseType: 'blob' });
    }

    public processImport(data): Observable<any> {
        const url = `${this.serviceUrl}/import`;
        const formdata = CommonUtils.convertFormFile(data);
        return this.postRequest(url, formdata);
    }

    public processValidateImport(data): Observable<any> {
        const url = `${this.serviceUrl}/validate-import`;
        const formdata = CommonUtils.convertFormFile(data);
        return this.postRequest(url, formdata);
    }
}
