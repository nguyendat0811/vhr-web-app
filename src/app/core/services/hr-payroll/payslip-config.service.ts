import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';
import { Observable } from 'rxjs';
import { CommonUtils } from '@app/shared/services/common-utils.service';

@Injectable({
providedIn: 'root'
})
export class PayslipConfigService extends BasicService {
  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('payroll', 'payslipConfig', httpClient, helperService);
  }

  public getList(): Observable<any> {
    const url = `${this.serviceUrl}/search`;
    return this.getRequest(url);
  }
  public processSendPayslip(data) {
    const buildParams = CommonUtils.buildParams(data);
    const url = `${this.serviceUrl}/send-payslip`;
    return this.getRequest(url, {params: buildParams});
  }

}
