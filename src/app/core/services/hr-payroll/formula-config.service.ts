import { Observable } from 'rxjs';
import { HelperService } from '@app/shared/services/helper.service';
import { HttpClient } from '@angular/common/http';
import { BasicService } from './../basic.service';
import { Injectable } from '@angular/core';
import { CommonUtils } from '@app/shared/services';

@Injectable({
  providedIn: 'root'
})
export class FormulaConfigService  extends BasicService {

  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('payroll', 'formulaConfig', httpClient, helperService);
   }
   public findListElement(salaryTypeId): Observable<any> {
    const url = `${this.serviceUrl}/element/${salaryTypeId}`;
    return this.getRequest(url);
  }

  public findByPayrollConfigId(dfFormulaId): Observable<any> {
    const url = `${this.serviceUrl}/get-list-detail/${dfFormulaId}`;
    return this.getRequest(url);
  }

  public findBySalaryTypeId(salaryTypeId): Observable<any> {
    const url = `${this.serviceUrl}/find-by-salaryTypeId/${salaryTypeId}`;
    return this.getRequest(url);
  }

  // downloadTemplateImport and processImport
  public processDownloadTemplateImport(salaryTypeId?: any, dfFormulaId?: any) {
    const url =  `${this.serviceUrl}/download-template-import/${salaryTypeId}/${dfFormulaId}`;
    return this.getRequest(url, {responseType: 'blob'});
  }

  public processImport(data): Observable<any>  {
    const url =  `${this.serviceUrl}/import-expression-list`;
    const formdata = CommonUtils.convertFormFile(data);
    return this.postRequest(url, formdata);
  }

  public updateStatusById(dfFormulaId , isActive): Observable<any> {
    const url =  `${this.serviceUrl}/update-formula-config/${dfFormulaId}/${isActive}`;
    return this.getRequest(url);
  }

  public processValidateImport(data): Observable<any>  {
    const url =  `${this.serviceUrl}/validate-import`;
    const formdata = CommonUtils.convertFormFile(data);
    return this.postRequest(url, formdata);
  }

  public processSearchHistory(data?: any, event?: any): Observable<any> {
    if (!event) {
      this.credentials = Object.assign({}, data);
    }

    const searchData = CommonUtils.convertData(this.credentials);
    if (event) {
      searchData._search = event;
    }
    const buildParams = CommonUtils.buildParams(searchData);
    const url = `${this.serviceUrl}/search-history?`;
    return this.getRequest(url, {params: buildParams});
  }
}
