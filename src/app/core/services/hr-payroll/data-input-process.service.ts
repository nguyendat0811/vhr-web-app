import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';
import { Observable } from 'rxjs';
import { CommonUtils } from '@app/shared/services';

@Injectable({
  providedIn: 'root'
})
export class DataInputProcessService extends BasicService {

  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('payroll', 'dataInputProcess', httpClient, helperService);
  }

  public getListActive(): Observable<any> {
    const url = `${this.serviceUrl}/all-active`;
    return this.getRequest(url);
  }
  public getList(): Observable<any> {
    const url = `${this.serviceUrl}/search`;
    return this.getRequest(url);
  }
  public getListColumn(dataTypeId: number): Observable<any> {
    const url = `${this.serviceUrl}/search-column/by-data-type/${dataTypeId}`;
    return this.getRequest(url);
  }

  public downloadTemplateImport(data): Observable<any> {
    const url = `${this.serviceUrl}/download-template-import`;
    return this.getRequest(url, {params: data, responseType: 'blob'});
  }

  public processImport(data): Observable<any>  {
    const url =  `${this.serviceUrl}/import-data-input`;
    const formdata = CommonUtils.convertFormFile(data);
    return this.postRequest(url, formdata);
  }
  public processValidateImport(data): Observable<any>  {
    const url =  `${this.serviceUrl}/validate-import-data-input`;
    const formdata = CommonUtils.convertFormFile(data);
    return this.postRequest(url, formdata);
  }

   /**
   * updateProcessSearch
   */
  public updateProcessSearch(item: any): Observable<any> {
    const url = `${this.serviceUrl}`;
    return this.postRequest(url, CommonUtils.convertData(item));
  }

  public deleteData(data?: any): Observable<any> {
    if (!event) {
      this.credentials = Object.assign({}, data);
    }
    const searchData = CommonUtils.convertData(this.credentials);
    const buildParams = CommonUtils.buildParams(searchData);
    const url = `${this.serviceUrl}/delete-data?`;
    return this.getRequest(url, {params: buildParams});
  }
}
