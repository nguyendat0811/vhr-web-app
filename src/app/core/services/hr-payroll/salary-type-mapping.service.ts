import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SalaryTypeMappingService extends BasicService {

  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('payroll', 'salaryTypeMapping', httpClient, helperService);
  }

  public getListDataTypeBySalary(salaryTypeId?: number): Observable<any> {
    const url = `${this.serviceUrl}/list-data-type-by-salary/${salaryTypeId}`;
    return this.getRequest(url);
  }


}
