import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SalaryTypeService extends BasicService {

  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('payroll', 'salaryType', httpClient, helperService);
  }

  public getListActive(): Observable<any> {
    const url = `${this.serviceUrl}/all-active`;
    return this.getRequest(url);
  }
  public getList(): Observable<any> {
    const url = `${this.serviceUrl}/search`;
    return this.getRequest(url);
  }
  public findAll(): Observable<any> {
    const url = `${this.serviceUrl}/find-all`;
    return this.getRequest(url);
  }
  public updateStatusById(salarytypeId , status): Observable<any> {
    const url =  `${this.serviceUrl}/update-salary-type-status/${salarytypeId}/${status}`;
    return this.getRequest(url);
  }
  public findIsLoock(salaryTypeId): Observable<any> {
    const url = `${this.serviceUrl}/is-loock/${salaryTypeId}`;
    return this.getRequest(url);
  }
}
