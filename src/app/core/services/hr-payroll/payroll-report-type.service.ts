import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';
import { Observable } from 'rxjs';
import { CommonUtils } from '@app/shared/services/common-utils.service';

@Injectable({
providedIn: 'root'
})
export class PayrollReportTypeService extends BasicService {
  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('payroll', 'payrollReportType', httpClient, helperService);
  }

  public getList(): Observable<any> {
    const url = `${this.serviceUrl}/search`;
    return this.getRequest(url);
  }

  public getListActiveReportType(salarytypeId, organizationId): Observable<any> {
    const url = `${this.serviceUrl}/active-report-type/${salarytypeId}/${organizationId}`;
    return this.getRequest(url);
  }
}
