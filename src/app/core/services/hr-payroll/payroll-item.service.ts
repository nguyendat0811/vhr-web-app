import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BasicService } from '../basic.service';
import { HelperService } from '@app/shared/services/helper.service';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class PayrollItemService extends BasicService {

  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('payroll', 'payrollItem', httpClient, helperService);
  }
  public getListActive(): Observable<any> {
    const url = `${this.serviceUrl}/all-active`;
    return this.getRequest(url);
  }
  public getList(): Observable<any> {
    const url = `${this.serviceUrl}/search`;
    return this.getRequest(url);
  }
  public getColumName(): Observable<any> {
    const url = `${this.serviceUrl}/getcolumname`;
    return this.getRequest(url);
  }
  public findBySalaryTypeId(salaryTypeId): Observable<any> {
    const url = `${this.serviceUrl}/find-by-salaryTypeId/${salaryTypeId}`;
    return this.getRequest(url);
  }
  public cloneData(param): Observable<any> {
    const url = `${this.serviceUrl}/clone-payroll-item/${param.salaryTypeIdSrc}/${param.salaryTypeIdTarget}`;
    return this.getRequest(url);
  }
  public deletePayrollItemBySalaryType(param): Observable<any> {
    const url = `${this.serviceUrl}/delete-payroll-item-by-salary-type/${param.salaryTypeId}`;
    return this.getRequest(url);
  }
}
