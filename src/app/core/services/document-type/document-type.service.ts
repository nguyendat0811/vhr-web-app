import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HelperService } from '@app/shared/services/helper.service';

@Injectable({
  providedIn: 'root'
})

export class DocumentTypeService extends BasicService {
  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('emp', 'documentType', httpClient, helperService);
  }
  public getFindAll(): Observable<any> {
    const listDocumentTypeUrl = `${this.serviceUrl}/findAll`;
    return this.getRequest(listDocumentTypeUrl);
  }
  public getDocumentTypeInUsed(): Observable<any> {
    const listDocumentTypeUrl = `${this.serviceUrl}/find-document-type-in-used`;
    return this.getRequest(listDocumentTypeUrl);
  }
  public findReasonByDocumentTypeId(documentTypeId: number): Observable<any> {
    const url = `${this.serviceUrl}/${documentTypeId}/document-reason`;
    return this.getRequest(url);
  }
  public findDocumentTypeJoinMarket(): Observable<any> {
    const url = `${this.serviceUrl}/document-type-join-market`;
    return this.getRequest(url);
  }
}
