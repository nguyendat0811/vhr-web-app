import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';
import { Observable } from 'rxjs';
import { CommonUtils } from '@app/shared/services';

@Injectable({
  providedIn: 'root'
})
export class EmpTypeProcessReportService extends BasicService {
  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('report', 'empTypeProcessReport', httpClient, helperService);
  }
  public exportLabour(data): Observable<any> {
    const url = `${this.serviceUrl}/export-labour/${data}`;
    return this.getRequest(url, {responseType: 'blob'});
  }
  public exportTerminationAgreement(data): Observable<any> {
    const url = `${this.serviceUrl}/termination-agreement/${data}`;
    return this.getRequest(url, {responseType: 'blob'});
  }
}
