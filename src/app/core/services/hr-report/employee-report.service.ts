import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';
import { Observable } from 'rxjs';
import { CommonUtils } from '@app/shared/services';

@Injectable({
  providedIn: 'root'
})
export class EmployeeReportService extends BasicService {
  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('report', 'employeeReport', httpClient, helperService);
  }

  public export2C(data): Observable<any> {
    const url = `${this.serviceUrl}/export-2c/${data}`;
    return this.getRequest(url, {responseType: 'blob'});
  }
}
