import { CommonUtils } from '@app/shared/services';
import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HelperService } from '@app/shared/services/helper.service';

@Injectable({
  providedIn: 'root'
})
export class SystemParameterService extends BasicService {

  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('sys', 'systemParameter', httpClient, helperService);
  }

  public getValueByCode(code: string): Observable<any> {
    const url = `${this.serviceUrl}/get-value-by-code/${code}`;
    return this.getRequest(url);
  }
  public findByName(name: string): Observable<any> {
    const url = `${this.serviceUrl}/name/${name}`;
    return this.getRequest(url);
  }
  public listParamUsed(): Observable<any> {
    const url = `${this.serviceUrl}/paramUsed`;
    return this.getRequest(url);
  }

  public findEffectiveParamByName(name: string): Observable<any> {
    const url = `${this.serviceUrl}/effective-sys-param/${name}`;
    return this.getRequest(url);
  }

  public saveTranferSetrvice(item: any): Observable<any> {
    const url = `${this.serviceUrl}/save-tranfer-service`;
    return this.postRequest(url, CommonUtils.convertData(item));
  }
}
