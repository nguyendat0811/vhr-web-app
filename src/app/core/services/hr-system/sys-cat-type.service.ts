import { CommonUtils } from '@app/shared/services/common-utils.service';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { BasicService } from './../basic.service';
import { Injectable } from '@angular/core';
import { HelperService } from '@app/shared/services/helper.service';

@Injectable({
  providedIn: 'root'
})
export class SysCatTypeService extends BasicService {

  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('sys', 'sysCatType', httpClient, helperService);
  }

  public findByNation(): Observable<any> {
    const url = `${this.serviceUrl}/find-by-nation/${CommonUtils.getNationId()}`;
    return this.getRequest(url);
  }

  public findTransferNation(): Observable<any> {
    const url = `${this.serviceUrl}/nations`;
    return this.getRequest(url);
  }

  public listParamUsed(): Observable<any> {
    const url = `${this.serviceUrl}/paramUsed`;
    return this.getRequest(url);
  }

  public transferNation(data): Observable<any> {
    const url = `${this.serviceUrl}/transfer`;
    return this.postRequest(url, data);
  }

  public getListSysCat(code: string): Observable<any> {
    const url = `${this.serviceUrl}/${code}/sys-cats`;
    return this.getRequest(url);
  }
}
