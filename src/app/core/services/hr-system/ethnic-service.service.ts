import { CommonUtils } from '@app/shared/services';
import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HelperService } from '@app/shared/services/helper.service';

@Injectable({
  providedIn: 'root'
})
export class EthnicService extends BasicService {

  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('sys', 'ethnic', httpClient, helperService);
  }

  /**
   * Lay danh sach dan toc theo quoc gia
   */
  public findByNationId(nationId?: any): Observable<any> {
    const url = `${this.serviceUrl}/find-by-nation/${CommonUtils.nvl(nationId, 0)}`;
    return this.getRequest(url);
  }
}
