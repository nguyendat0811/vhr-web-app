import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';
import { Observable } from 'rxjs';
import { CommonUtils } from '@app/shared/services';

@Injectable({
  providedIn: 'root'
})
export class SysLanguageService extends BasicService {
  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('sys', 'sysLanguage', httpClient, helperService);
  }

  public findAll(): Observable<any> {
    const url = `${this.serviceUrl}/all`;
    return this.getRequest(url);
  }
}
