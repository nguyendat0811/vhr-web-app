import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmpSystemConfigService extends BasicService {
  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('sys', 'empSystemConfig', httpClient, helperService);
  }

  public getEmpTabs(): Observable<any> {
    const url = `${this.serviceUrl}/emp-tabs`;
    return this.getRequest(url);
  }

  public saveEmpTabs(data): Observable<any> {
    const url = `${this.serviceUrl}/emp-tabs`;
    return this.httpClient.post(url, data);
  }

  public getIntro() {
    const url = `${this.serviceUrl}/find-intro`;
    return this.getRequest(url);
  }

  public saveIntro(data): Observable<any> {
    const url = `${this.serviceUrl}/save-intro`;
    return this.httpClient.post(url, data);
  }
}
