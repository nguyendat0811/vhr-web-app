import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';
import {EmployeeInfoService} from '../employee-info/employee-info.service';

@Injectable({
  providedIn: 'root'
})
export class HomeEmployeeService extends BasicService {
  constructor(public httpClient: HttpClient
    , public helperService: HelperService
    , public employeeInfoService: EmployeeInfoService
    ) {
    super('emp', 'homeEmployee', httpClient, helperService);
  }
  getMarketCompany() {
    const url = `${this.serviceUrl}/market-company`;
    return this.getRequest(url);
  }
  getListDOBEmployee() {
    return this.employeeInfoService.getListDOBEmployee();
  }
  getListOnOffEmployee(dateId: number) {
    return this.employeeInfoService.getListOnOffEmployee(dateId);
  }
}
