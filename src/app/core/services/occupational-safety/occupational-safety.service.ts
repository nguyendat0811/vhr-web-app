import { HelperService } from './../../../shared/services/helper.service';
import { HttpClient } from '@angular/common/http';
import { BasicService } from './../basic.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OccupationalSafetyService extends BasicService {
  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('emp', 'occupationalSafety', httpClient, helperService);
  }
}
