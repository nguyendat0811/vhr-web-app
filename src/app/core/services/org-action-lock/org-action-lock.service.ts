import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';
import { Observable } from 'rxjs/Observable';
import { CommonUtils } from '@app/shared/services';

@Injectable({
  providedIn: 'root'
})
export class OrgActionLockService extends BasicService {
  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('emp', 'orgActionLock', httpClient, helperService);
  }

  public getDate(): Observable<any> {
    const url = `${this.serviceUrl}/get-date`;
    return this.getRequest(url);
  }
}
