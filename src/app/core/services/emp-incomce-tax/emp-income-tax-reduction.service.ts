import { FamilyRelationshipService } from './../family-relationship/family-relationship.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';
import { BasicService } from '../basic.service';
import { CommonUtils } from '@app/shared/services';

@Injectable({
  providedIn: 'root'
})
export class EmpIncomeTaxReductionService extends BasicService {
  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('emp', 'empIncomeTaxReduction', httpClient, helperService);
  }

  /**
   * find emp bank account id
   */
  public findByEmpFamilyRelationshipId(empFamilyRelationshipId: number) {
    const emp = CommonUtils.nvl(empFamilyRelationshipId);
    const url = `${this.serviceUrl}/find-by-emp-family-relationship-id/${emp}`;
    return this.getRequest(url);
  }
}
