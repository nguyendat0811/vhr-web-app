import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';
import { BasicService } from '../basic.service';
import { CommonUtils } from '@app/shared/services';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmpIncomeTaxService extends BasicService {
  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('emp', 'empIncomeTax', httpClient, helperService);
  }

  public findIncomeDetailByEmployeeId(employeeId: number) {
    const emp = CommonUtils.nvl(employeeId);
    const url = `${this.serviceUrl}/${emp}/income-tax-detail`;
    return this.getRequest(url);
  }

  public findIncomeTaxByEmployeeId(employeeId: number) {
    const emp = CommonUtils.nvl(employeeId);
    const url = `${this.serviceUrl}/${emp}/income-tax`;
    return this.getRequest(url);
  }

   /**
   * deleteByEmpIncomeTaxDetailId
   * param id
   */
  public deleteByEmpIncomeTaxDetailId(id: number): Observable<any> {
    const url = `${this.serviceUrl}/delete-emp-income-tax-detail-id/${id}`;
    this.helperService.isProcessing(true);
    return this.deleteRequest(url);
  }

  public findEmpIncomeTaxDetailByEmployeeId(employeeId: number): Observable<any> {
    const list = `${this.serviceUrl}/find-income-tax-detail/${employeeId}`;
    return this.getRequest(list);
  }
}
