import { CommonUtils } from '@app/shared/services/common-utils.service';
import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmpTypeService extends BasicService {
  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('emp', 'empType', httpClient, helperService);
  }

  public getAllByEmpTypeByIsUsed(): Observable<any> {
    const url = `${this.serviceUrl}/by-market-and-is-used`;
    return this.getRequest(url);
  }

  public findByIdMarketCompanyId(marketCompanyId: number, empTypeId: number): Observable<any> {
    const url = `${this.serviceUrl}/${marketCompanyId}/${empTypeId}`;
    return this.getRequest(url);
  }

  public findEmpTypeByMarket(marketCompanyId: number): Observable<any> {
    const url = `${this.serviceUrl}/find-by-market/${marketCompanyId}`;
    return this.getRequest(url);
  }

  public findAll(): Observable<any> {
    const url = `${this.serviceUrl}/find-all`;
    return this.getRequest(url);
  }
}
