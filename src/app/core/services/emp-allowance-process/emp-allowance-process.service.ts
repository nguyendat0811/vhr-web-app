import { Observable } from 'rxjs/Observable';
import { HelperService } from '@app/shared/services/helper.service';
import { HttpClient } from '@angular/common/http';
import { BasicService } from './../basic.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EmpAllowanceProcessService extends BasicService {
  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('emp', 'empAllowanceProcess', httpClient, helperService);
  }

  public getAllowanceList(allowanceTypeId: number): Observable<any> {
    const urlStatus = `${this.serviceUrl}/allowance-lists/${allowanceTypeId}`;
    return this.getRequest(urlStatus);
  }

  public getAllowanceTypeList(allowanceId: number): Observable<any> {
    const urlStatus = `${this.serviceUrl}/allowance-type-lists/${allowanceId}`;
    return this.getRequest(urlStatus);
  }
}
