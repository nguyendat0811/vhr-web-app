import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';
import { Observable } from 'rxjs';
import { CommonUtils } from '@app/shared/services';

@Injectable({
  providedIn: 'root'
})
export class EmpVisaService extends BasicService {
  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('emp', 'empVisa', httpClient, helperService);
  }

  public getListPassportNumber(employeeId: number): Observable<any> {
    const emp = CommonUtils.nvl(employeeId);
    const url = `${this.serviceUrl}/${emp}/list-passport-number`;
    return this.getRequest(url);
  }
}
