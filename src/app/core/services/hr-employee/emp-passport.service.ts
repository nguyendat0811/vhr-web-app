import { CommonUtils } from '@app/shared/services/common-utils.service';
import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmpPassportService extends BasicService {

  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('emp', 'empPassport', httpClient, helperService);
  }

  public checkDuplicateNumber(data: any) : Observable<any> {
    const formdata = CommonUtils.convertData(data);
    const buildParams = CommonUtils.buildParams(formdata);
    console.log('formdata', formdata);
    const url = `${this.serviceUrl}/check-duplicate-number`;
    return this.getRequest(url, {params: buildParams});
  }
}
