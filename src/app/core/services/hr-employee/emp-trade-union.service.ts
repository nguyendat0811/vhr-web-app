import { Observable } from 'rxjs/Observable';
import { CommonUtils } from './../../../shared/services/common-utils.service';
import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';

@Injectable({
  providedIn: 'root'
})
export class EmpTradeUnionService extends BasicService {
  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('emp', 'empTradeUnion', httpClient, helperService);
  }

  public finishPreviousProcess(item: any): Observable<any> {
    const formdata = CommonUtils.convertFormFile(item);
    const url = `${this.serviceUrl}/finish-previous-process`;
    return this.postRequest(url, formdata);
  }
}
