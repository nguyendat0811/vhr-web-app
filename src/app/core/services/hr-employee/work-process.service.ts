import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WorkProcessService extends BasicService {
  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('emp', 'workProcess', httpClient, helperService);
  }

  public getDraftProcess(employeeId: number): Observable<any> {
    const getDraftProcessUrl = `${this.serviceUrl}/get-draft-process/${employeeId}`;
    return this.getRequest(getDraftProcessUrl);
  }

  /**
   * getMainProcessByEmployeeId
   */
  public getMainProcessByEmployeeId(employeeId: number) {
    const url = `${this.serviceUrl}/main-process-by-employee/${employeeId}`;
    return this.getRequest(url);
  }

  /**
  * getProcessList
  */
  public getProcessList(employeeId: number) {
    const url = `${this.serviceUrl}/get-process-list/${employeeId}`;
    return this.getRequest(url);
  }

  /**
  * getProcessByDocumentTypeId
  */
  public getProcessByDocumentTypeId(documentTypeId: number) {
    const url = `${this.serviceUrl}/get-process-by-document-type-id/${documentTypeId}`;
    return this.getRequest(url);
  }
}
