import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';
import { BasicService } from '../../basic.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SalaryGradeService extends BasicService {
  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('emp', 'salaryGrade', httpClient, helperService);
  }

  /**
  * get list by salary table
  */
  public getListBySalaryTable(salaryTableId: number): Observable<any> {
    const url = `${this.serviceUrl}/get-by-salary-table/${salaryTableId}`;
    return this.getRequest(url);
  }

  public findById(salaryGradeId: number): Observable<any> {
    const url = `${this.serviceUrl}/${salaryGradeId}`;
    return this.getRequest(url);
  }
}
