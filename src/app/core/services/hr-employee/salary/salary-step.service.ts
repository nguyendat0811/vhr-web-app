import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';
import { BasicService } from '../../basic.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SalaryStepService extends BasicService {
  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('emp', 'salaryStep', httpClient, helperService);
  }

  /**
  * get list by salary table
  */
  public getListBySalaryTable(salaryTableId: number): Observable<any> {
    const url = `${this.serviceUrl}/get-by-salary-table/${salaryTableId}`;
    return this.getRequest(url);
  }
}
