import { BasicService } from './../../basic.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';
import { Observable } from 'rxjs';
import { CommonUtils } from '@app/shared/services';

@Injectable({
  providedIn: 'root'
})
export class SalaryTableService extends BasicService {
  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('emp', 'salaryTable', httpClient, helperService);
  }

  /**
   * Lay danh sach quy che, ngach luong, bang luong
   */
  public findByParentId(parentId?: any): Observable<any> {
    const url = `${this.serviceUrl}/salary-table-by-parent/${CommonUtils.nvl(parentId, 0)}`;
    return this.getRequest(url);
  }

  /**
   * Lay danh sach quy che cau hinh trong chuc danh
   */
  public findRegulateByPosition(positionId: any): Observable<any> {
    const url = `${this.serviceUrl}/regulate-config-by-position/${positionId}`;
    return this.getRequest(url);
  }
  /**
   * Lay danh sach bang luong cau hinh trong chuc danh
   */
  public findGradeLadderByPosition(positionId: any, salaryRegulationId: any): Observable<any> {
    const url = `${this.serviceUrl}/ladder-config-by-position/${positionId}/${salaryRegulationId}`;
    return this.getRequest(url);
  }
  /**
   * Lay danh sach ngach luong cau hinh trong chuc danh
   */
  public findGradeRateByPosition(positionId: any, salaryGradeLadderId: any): Observable<any> {
    const url = `${this.serviceUrl}/rate-config-by-position/${positionId}/${salaryGradeLadderId}`;
    return this.getRequest(url);
  }

  public findSalaryPolicy(): Observable<any> {
    const urlStatus = `${this.serviceUrl}/salary-policy/`;
    return this.getRequest(urlStatus);
  }

  public getSalaryTableById(salaryTableId: number): Observable<any> {
    const urlStatus = `${this.serviceUrl}/salary-table-by-id/${salaryTableId}`;
    return this.getRequest(urlStatus);
  }

  public getSalaryStepById(salaryTableId: number): Observable<any> {
    const urlStatus = `${this.serviceUrl}/salary-step-by-id/${salaryTableId}`;
    return this.getRequest(urlStatus);
  }

  public getTypeById(salaryTableId: number): Observable<any> {
    const urlStatus = `${this.serviceUrl}/type-by-id/${salaryTableId}`;
    return this.getRequest(urlStatus);
  }

  /**
   * Danh sanh step or grade by ngach va bang luong
   */
  public getSalaryStepGrade(salaryLadderId: number, salaryRateId: number): Observable<any> {
    const urlStatus = `${this.serviceUrl}/salary-step-grade/${salaryLadderId}/${salaryRateId}`;
    return this.getRequest(urlStatus);
  }

  /**
  * action load salary-table
  */
  public actionInitAjax(nationId: number, params: any): Observable<any> {
    const url = `${this.serviceUrl}/salary-table-tree`;
    return this.postRequest(url, params);
  }

  public actionSaveGradeRate(formSubmit: any) {
    const url = `${this.serviceUrl}/save-rate`;
    return this.postRequest(url, CommonUtils.convertData(formSubmit));
  }

  public getPositionSalaryTable(salaryTableId: number): Observable<any> {
    const urlStatus = `${this.serviceUrl}/${salaryTableId}/position-salary-table`;
    return this.getRequest(urlStatus);
  }
}
