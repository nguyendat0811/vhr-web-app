import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmpEducationProcessService extends BasicService {

  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('emp', 'empEducationProcess', httpClient, helperService);
   }

   public getEducationByEmployeeId(employeeId: number): Observable<any> {
    const nameUrl = `${this.serviceUrl}/get-education-by-employee-id/${employeeId}`;
    return this.getRequest(nameUrl);
  }
}
