import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HelperService } from '@app/shared/services/helper.service';
import { CommonUtils } from '@app/shared/services';

@Injectable({
  providedIn: 'root'
})
export class FamilyRelationshipService extends BasicService {
  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('emp', 'familyRelationship', httpClient, helperService);
  }

    /**
   * find emp bank account id
   */
  public findByEmployeeId(employeeId: number) {
    const emp = CommonUtils.nvl(employeeId);
    const url = `${this.serviceUrl}/${emp}/emp-family-relationship`;
    return this.getRequest(url);
  }

  public getListByCurrenRelated(employeeId: number): Observable<any> {
    const emp = CommonUtils.nvl(employeeId);
    const urlStatus = `${this.serviceUrl}/${emp}/family-relationship-by-current-related/`;
    return this.getRequest(urlStatus);
  }

  public findFamilyByEmpId(employeeId: number, action: String): Observable<any> {
    const emp = CommonUtils.nvl(employeeId);
    const urlStatus = `${this.serviceUrl}/find-all/${emp}/${action}`;
    return this.getRequest(urlStatus);
  }
}
