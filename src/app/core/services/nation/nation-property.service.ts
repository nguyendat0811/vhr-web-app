import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';

@Injectable({
    providedIn: 'root'
})
export class NationPropertyService extends BasicService {
  constructor(public httpClient: HttpClient, public helperService: HelperService) {
      super('sys', 'nationProperty', httpClient, helperService);
  }
  /**
   * findByNationId
   */
  public findByNationId(nationId: number): Observable<any> {
    const url = `${this.serviceUrl}/nation-id/${nationId}`;
    return this.getRequest(url);
  }

}
