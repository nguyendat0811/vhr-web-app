import { CommonUtils } from '@app/shared/services/common-utils.service';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { BasicService } from '../basic.service';
import { Injectable } from '@angular/core';
import { HelperService } from '@app/shared/services/helper.service';

@Injectable({
  providedIn: 'root'
})
export class NationConfigTypeService extends BasicService {

  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('sys', 'nationConfigType', httpClient, helperService);
  }
  public getListSysCat(code: string): Observable<any> {
    const url = `${this.serviceUrl}/${code}/sys-cats`;
    return this.getRequest(url);
  }
  public searchAll(): Observable<any> {
    const url = `${this.serviceUrl}/find-by-all`;
    return this.getRequest(url);
  }
  public listParamUsed(): Observable<any> {
    const url = `${this.serviceUrl}/paramUsed`;
    return this.getRequest(url);
  }


}
