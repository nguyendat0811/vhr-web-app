import { environment } from './../../../../environments/environment';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';

@Injectable({
    providedIn: 'root'
})
export class NationConfigService extends BasicService {

  private API_URL: string = environment.serverUrl.sys;

  constructor(public httpClient: HttpClient, public helperService: HelperService) {
      super('sys', 'nationConfig', httpClient, helperService);
  }

  /**
   * Lay danh sach nation_config
   * getNationList
   */
  public getNationConfigList(nationConfigType: string, nationId: number): Observable<any> {
    const url = `${this.serviceUrl}/${nationConfigType}/${nationId}/list`;
    return this.getRequest(url);
  }

  public getAllByNationConfigType(code: string): Observable<any> {
    const url = `${this.serviceUrl}/by-nation-config-type/${code}`;
    return this.getRequest(url);
  }
}
