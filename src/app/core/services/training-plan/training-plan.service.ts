import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';

@Injectable({
  providedIn: 'root'
})
export class TrainingPlanService extends BasicService {
  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('emp', 'trainingPlan', httpClient, helperService);
  }

  public findAllTrainingPlan(): Observable<any> {
    const urlStatus = `${this.serviceUrl}/find-all`;
    return this.getRequest(urlStatus);
  }
}
