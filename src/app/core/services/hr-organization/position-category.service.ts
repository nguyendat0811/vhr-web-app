import { EmployeeInfoService } from './../employee-info/employee-info.service';
import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HelperService } from '@app/shared/services/helper.service';
import { CommonUtils } from '@app/shared/services';

@Injectable({
  providedIn: 'root'
})
export class PositionCategoryService extends BasicService {

  constructor(public httpClient: HttpClient, public helperService: HelperService, public employeeInfoService: EmployeeInfoService) {
    super('emp', 'position', httpClient, helperService);
  }

  public getListStatus(): Observable<any> {
    const urlStatus = `${this.serviceUrl}/status`;
    return this.getRequest(urlStatus);
  }
  public findByPositionId(id: number) {
    return this.employeeInfoService.findByPositionId(id);
  }

  public getListLevelCareer(): Observable<any> {
    const listLevelCareerUrl = `${this.serviceUrl}/career-levels`;
    return this.getRequest(listLevelCareerUrl);
  }

  public getPositionDetail(positionId: number): Observable<any> {
    const listView = `${this.serviceUrl}/view-details/${positionId}`;
    return this.getRequest(listView);
  }

  public getByPositionId(positionId: number): Observable<any> {
    const list = `${this.serviceUrl}/find-by-position-id/${positionId}`;
    return this.getRequest(list);
  }

  public findCareerLevelById(careerLevelId: number): Observable<any> {
    const listView = `${this.serviceUrl}/find-career-level/${careerLevelId}`;
    return this.getRequest(listView);
  }

  public getCodeExit(form: any): Observable<any> {
    const url = `${this.serviceUrl}/code-exit?`;
    const buildParams = CommonUtils.buildParams(form);
    return this.getRequest(url, {params: buildParams});
  }

    /**
   * deletePositionSalaryTable
   * param id
   */
  public deletePositionSalaryTable(id: number): Observable<any> {
    const url = `${this.serviceUrl}/delete-position-salary-table/${id}`;
    this.helperService.isProcessing(true);
    return this.deleteRequest(url);
  }
}
