import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';

@Injectable({
  providedIn: 'root'
})
export class RequirementDetailService extends BasicService {
  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('emp', 'requirementDetail', httpClient, helperService);
  }

  public getRequirementDetail(requirementId): Observable<any> {
    const urlStatus = `${this.serviceUrl}/list-requirement-detail/${requirementId}/`;
    return this.getRequest(urlStatus);
  }

  public checkExistRequirement(requirementDetailId): Observable<any> {
    const urlStatus = `${this.serviceUrl}/check-exist/${requirementDetailId}/`;
    return this.getRequest(urlStatus);
  }



}
