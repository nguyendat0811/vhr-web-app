import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { Observable } from 'rxjs';
import { HelperService } from '@app/shared/services/helper.service';
import { BasicService } from '../../basic.service';

@Injectable({
  providedIn: 'root'
})
export class OrgSelectorService extends BasicService {

  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('emp', 'orgSelector', httpClient, helperService);
  }
  /**
   * action search org for payroll module
   * @param data 
   * @param event 
   */
  public searchOrgForPayroll(data?: any, event?: any): Observable<any> {
    if (!event) {
      this.credentials = Object.assign({}, data);
    }

    const searchData = CommonUtils.convertData(this.credentials);
    if (event) {
      searchData._search = event;
    }
    const buildParams = CommonUtils.buildParams(searchData);
    const url = `${this.serviceUrl}/search-payroll?`;
    return this.getRequest(url, {params: buildParams});
  }
  /**
   * action load org tree
   */
  public actionInitAjax(params: any): Observable<any> {
    const url = `${this.serviceUrl}/action-init-ajax`;
    return this.postRequest(url, params);
  }
  /**
   * action load org tree for payroll
   */
  public actionInitAjaxPayroll(params: any): Observable<any> {
    const url = `${this.serviceUrl}/action-init-ajax-payroll`;
    return this.postRequest(url, params);
  }
  /**
   * action load org tree
   */
  public actionLazyRead(params: any): Observable<any> {
    const url = `${this.serviceUrl}/action-lazy-read`;
    return this.postRequest(url, params);
  }
  /**
   * action load org tree for payroll
   */
  public actionLazyReadPayroll(params: any): Observable<any> {
    const url = `${this.serviceUrl}/action-lazy-read-payroll`;
    return this.postRequest(url, params);
  }
    /**
   * action load org tree by line org
   */
  public actionInitAjaxByLineOrg(params: any): Observable<any> {
    const url = `${this.serviceUrl}/action-init-ajax-by-line-org`;
    return this.postRequest(url, params);
  }
  /**
   * action load org tree by line org
   */
  public actionLazyReadByLineOrg(params: any): Observable<any> {
    const url = `${this.serviceUrl}/action-lazy-read-by-line-org`;
    return this.postRequest(url, params);
  }
}
