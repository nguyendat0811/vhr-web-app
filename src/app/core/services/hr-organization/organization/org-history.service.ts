import { Injectable } from '@angular/core';
import { HelperService } from '@app/shared/services/helper.service';
import { HttpClient } from '@angular/common/http';
import { BasicService } from '../../basic.service';

@Injectable({
  providedIn: 'root'
})
export class OrgHistoryService extends BasicService {

  constructor(public httpClient: HttpClient, public helperService: HelperService) { 
    super('emp', 'orgHistory', httpClient, helperService);
  }
}
