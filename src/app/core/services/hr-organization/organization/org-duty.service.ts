import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HelperService } from '@app/shared/services/helper.service';
import { BasicService } from '../../basic.service';

@Injectable({
  providedIn: 'root'
})
export class OrgDutyService extends BasicService {

  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('emp', 'orgDuty', httpClient, helperService);
  }
  /**
   * find org duty
   */
  public findOrgDuty(orgId: number): Observable<any> {
    const url = `${this.serviceUrl}/${orgId}`;
    return this.getRequest(url);
  }
}
