import { BasicService } from './../../basic.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';
import { CommonUtils } from '@app/shared/services';

@Injectable({
  providedIn: 'root'
})
export class OrgBoundaryDraffService extends BasicService {

  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('emp', 'orgBoundaryDraff', httpClient, helperService);
  }

  /**
   * find org boundary by organizationId co ca don vi con
   */
  public findAllOrgBoundaryByOrg(orgPlantId: number, orgId: number) {
    const organizationId = CommonUtils.nvl(orgId);
    const url = `${this.serviceUrl}/find-all-by-organization/${orgPlantId}/${organizationId}`;
    return this.getRequest(url);
  }

  /**
   * find org boundary by organizationId
   */
  public findOrgBoundaryByOrg(orgId: number) {
    const organizationId = CommonUtils.nvl(orgId);
    const url = `${this.serviceUrl}/find-by-organization/${organizationId}`;
    return this.getRequest(url);
  }
}
