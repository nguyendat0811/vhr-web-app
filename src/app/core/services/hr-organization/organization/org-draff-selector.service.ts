import { BasicService } from './../../basic.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HelperService } from '@app/shared/services/helper.service';

@Injectable({
  providedIn: 'root'
})
export class OrgDraffSelectorService extends BasicService {

  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('emp', 'orgDraffSelector', httpClient, helperService);
  }
  /**
   * action load org tree
   */
  public actionInitAjax(orgPlanId: number, params: any): Observable<any> {
    const url = `${this.serviceUrl}/${orgPlanId}/action-init-ajax`;
    return this.postRequest(url, params);
  }
  /**
   * action load org tree
   */
  public actionLazyRead(orgPlanId: number, params: any): Observable<any> {
    const url = `${this.serviceUrl}/${orgPlanId}/action-lazy-read`;
    return this.postRequest(url, params);
  }
}
