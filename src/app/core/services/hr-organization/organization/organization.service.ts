import { Observable } from 'rxjs/Observable';
import { CommonUtils } from '@app/shared/services';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HelperService } from '@app/shared/services/helper.service';
import { BasicService } from '../../basic.service';

@Injectable({
  providedIn: 'root'
})
export class OrganizationService extends BasicService {

  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('emp', 'organization', httpClient, helperService);
  }

  /**
   * find multi language organization by id
   */
  public findOrgMultiLanguageById(orgId: number) {
    const organizationId = CommonUtils.nvl(orgId);
    const url = `${this.serviceUrl}/multi-language/${organizationId}`;
    return this.getRequest(url);
  }

  /**
   * find org relation id
   */
  public findOrgRelationId(orgId: number) {
    const organizationId = CommonUtils.nvl(orgId);
    const url = `${this.serviceUrl}/${organizationId}/org-relation`;
    return this.getRequest(url);
  }

  /**
   * find findAllParrent
   */
  public findAllParrent(orgId: number) {
    const organizationId = CommonUtils.nvl(orgId);
    const url = `${this.serviceUrl}/${organizationId}/all-parent-org`;
    return this.getRequest(url);
  }

  /**
   * find org additional info
   */
  public findOrgAdditionalInfo(orgId: number) {
    const organizationId = CommonUtils.nvl(orgId);
    const url = `${this.serviceUrl}/${organizationId}/other-info`;
    return this.getRequest(url);
  }

  /**
   * find org business by organizationId
   */
  public findOrgBusiness(orgId: number) {
    const organizationId = CommonUtils.nvl(orgId);
    const url = `${this.serviceUrl}/${organizationId}/org-business`;
    return this.getRequest(url);
  }

  /**
   * find org document by organizationId
   */
  public findOrgDocument(orgId: number) {
    const organizationId = CommonUtils.nvl(orgId);
    const url = `${this.serviceUrl}/${organizationId}/org-document`;
    return this.getRequest(url);
  }

  /**
   * find org boundary by organizationId
   */
  public findOrgBoundary(orgId: number) {
    const organizationId = CommonUtils.nvl(orgId);
    const url = `${this.serviceUrl}/${organizationId}/org-boundary`;
    return this.getRequest(url);
  }

  /**
   * saveOrUpdateOtherInfo
   */
  public saveOrUpdateOtherInfo(item: any, organizationId: number): Observable<any> {
    const formdata = CommonUtils.convertFormFile(item);
    const url = `${this.serviceUrl}/${organizationId}/other-info`;
    return this.postRequest(url, formdata);
  }

  /**
   * get ListChildOrg
   */
  public findListChildOrg(orgId: number): Observable<any> {
    const organizationId = CommonUtils.nvl(orgId);
    const url = `${this.serviceUrl}/${organizationId}/list-child`;
    return this.getRequest(url);
  }

  /**
   * get ListChildOrg
   */
  public findOneWithPlan(orgId: number, planId: number): Observable<any> {
    const organizationId = CommonUtils.nvl(orgId);
    const url = `${this.serviceUrl}/${organizationId}/plan/${planId}`;
    return this.getRequest(url);
  }

  /**
   * get node org view detail
   */
  public findOrgViewDetail(id: number): Observable<any> {
    const organizationId = CommonUtils.nvl(id);
    const url = `${this.serviceUrl}/${organizationId}/node-view-detail`;
    return this.getRequest(url);
  }

  /**
   * get child nodes org
   */
  public findListChildViewDetail(id: number): Observable<any> {
    const organizationId = CommonUtils.nvl(id);
    const url = `${this.serviceUrl}/${organizationId}/childs-node-view-detail`;
    return this.getRequest(url);
  }

  /**
   * get total boundary
   */
  public getTotalBoundary(id: number): Observable<any> {
    const organizationId = CommonUtils.nvl(id);
    const url = `${this.serviceUrl}/${organizationId}/total-boundary`;
    return this.getRequest(url);
  }

  /**
   * get org parent name
   */
  public findOrgFullName(id: number): Observable<any> {
    const organizationId = CommonUtils.nvl(id);
    const url = `${this.serviceUrl}/${organizationId}/org-full-name`;
    return this.getRequest(url);
  }

  /**
   * check allowed add employee
   */
  public checkAllowedAddEmployee(id: number): Observable<any> {
    const organizationId = CommonUtils.nvl(id);
    const url = `${this.serviceUrl}/${organizationId}/allowed-add-employee`;
    return this.getRequest(url);
  }
  public findByIds(orgIds: any): Observable<any> {
    const url = `${this.serviceUrl}/find-by-ids/${orgIds}`;
    return this.getRequest(url);
  }
}
