import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HelperService } from '@app/shared/services/helper.service';
import { BasicService } from '../../basic.service';
import { CommonUtils } from '@app/shared/services';
import { tap, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OrgPlanService extends BasicService {

  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('emp', 'orgPlan', httpClient, helperService);
  }
  /**
   * find tree draff
   */
  public findTreeOrgDraffById(id: number) {
    const orgPlanId = CommonUtils.nvl(id);
    const url = `${this.serviceUrl}/${orgPlanId}/org-draff-tree`;
    return this.getRequest(url);
  }
  /**
   * find tree real with draff
   */
  public findTreeOrgRealDraffById(id: number) {
    const orgPlanId = CommonUtils.nvl(id);
    const url = `${this.serviceUrl}/${orgPlanId}/org-real-draff-tree`;
    return this.getRequest(url);
  }
  /**
   * find all parent orgDraff by OrgPlanId
   */
  public findListParentOrgDraff(id: number) {
    const orgPlanId = CommonUtils.nvl(id);
    const url = `${this.serviceUrl}/${orgPlanId}/parent-org-draff`;
    return this.getRequest(url);
  }
  /**
   * validate bofore save Tree Org Draff ById
   */
  public validateSaveTreeOrgDraffById(id: number, treeNode) {
    const orgPlanId = CommonUtils.nvl(id);
    const url = `${this.serviceUrl}/${orgPlanId}/validate-org-draff-tree`;
    return this.postRequest(url, treeNode);
  }
  /**
   * save Tree Org Draff ById
   */
  public saveTreeOrgDraffById(id: number, treeNode) {
    const orgPlanId = CommonUtils.nvl(id);
    const url = `${this.serviceUrl}/${orgPlanId}/org-draff-tree`;
    return this.postRequest(url, treeNode);
  }
  /**
   * save Tree Org Draff ById
   */
  public saveOrUpdateTree(id: number, formData) {
    const orgPlanId = CommonUtils.nvl(id);
    const url = `${this.serviceUrl}/${orgPlanId}/create-draff-tree`;
    return this.postRequest(url, formData);
  }
  /**
   * sign plan
   */
  public signPlan(id: number) {
    const orgPlanId = CommonUtils.nvl(id);
    const url = `${this.serviceUrl}/${orgPlanId}/sign`;
    return this.getRequest(url);
  }
  /**
   * cancel sign plan
   */
  public cancelSign(id: number) {
    const orgPlanId = CommonUtils.nvl(id);
    const url = `${this.serviceUrl}/${orgPlanId}/cancel-sign`;
    return this.getRequest(url);
  }
  /**
   * approve plan
   */
  public approveSign(id: number) {
    const orgPlanId = CommonUtils.nvl(id);
    const url = `${this.serviceUrl}/${orgPlanId}/approve`;
    return this.postRequest(url);
  }

  /**
   * approve plan
   */
  public unapproveSign(orgPlanId, formData) {
    const url = `${this.serviceUrl}/${orgPlanId}/un-approve`;
    return this.postRequest(url, formData);
  }

  public getOrgDraffError(id: number): Observable<any> {
    const orgPlanId = CommonUtils.nvl(id);
    const url = `${this.serviceUrl}/${orgPlanId}/org-draff-error`;
    return this.httpClient.get(url)
      .pipe(
        tap( // Log the result or error
          res => {
            this.helperService.APP_TOAST_MESSAGE.next(res);
          },
          error => {
            this.helperService.APP_TOAST_MESSAGE.next(error);
          }
        ),
        catchError(this.handleError)
      );
  }

  /**
   * get org parent draff
   * view detail plan
   */
  public findListParentViewDetail(id: number): Observable<any> {
    const orgPlanId = CommonUtils.nvl(id);
    const url = `${this.serviceUrl}/${orgPlanId}/parents-node-view-detail`;
    return this.getRequest(url);
  }

  /**
   * get org parent draff
   * view detail plan
   */
  public findListChildViewDetail(id: number): Observable<any> {
    const orgDraffId = CommonUtils.nvl(id);
    const url = `${this.serviceUrl}/${orgDraffId}/childs-node-view-detail`;
    return this.getRequest(url);
  }

  public importOrganizationTree(orgPlanId, data) {
    const url =  `${this.serviceUrl}/${orgPlanId}/import-organization-tree`;
    const formdata = CommonUtils.convertFormFile(data);
    return this.postRequest(url, formdata);
  }

  public downloadTemplateImport() {
    const url =  `${this.serviceUrl}/download-template-import`;
    return this.getRequest(url, {responseType: 'blob'});
  }

  public checkIsCreatedDraffTree(id: number) {
    const orgPlanId = CommonUtils.nvl(id);
    const url =  `${this.serviceUrl}/${orgPlanId}/org-draff-tree-created`;
    return this.getRequest(url);
  }
}

