import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HelperService } from '@app/shared/services/helper.service';
import { Observable } from 'rxjs/Observable';
import { CommonUtils } from '@app/shared/services';
import { BasicService } from '../../basic.service';

@Injectable({
  providedIn: 'root'
})
export class OrgDraffService extends BasicService {

  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('emp', 'orgDraff', httpClient, helperService);
  }

  /**
   * find org relation id
   */
  public findTreeOrgDraffById(id: number) {
    const orgPlanId = CommonUtils.nvl(id);
    const url = `${this.serviceUrl}/${orgPlanId}/org-draff-tree`;
    return this.getRequest(url);
  }

/**
   * find org relation id
   */
  public findChildByParentId(id: number) {
    const orgDraffId = CommonUtils.nvl(id);
    const url = `${this.serviceUrl}/${orgDraffId}/list-child`;
    return this.getRequest(url);
  }

  /**
   * find org other info draff by orgDraffId
   */
  public findOrgOtherInfoDraff(id: number) {
    const orgDraffId = CommonUtils.nvl(id);
    const url = `${this.serviceUrl}/${orgDraffId}/other-info-draff`;
    return this.getRequest(url);
  }

  /**
   * find org duty draff by orgDraffId
   */
  public findOrgDutyDraff(id: number) {
    const orgDraffId = CommonUtils.nvl(id);
    const url = `${this.serviceUrl}/${orgDraffId}/org-duty-draff`;
    return this.getRequest(url);
  }

  /**
   * find org business draff by orgDraffId
   */
  public findOrgBusinessDraff(id: number) {
    const orgDraffId = CommonUtils.nvl(id);
    const url = `${this.serviceUrl}/${orgDraffId}/org-business-draff`;
    return this.getRequest(url);
  }

  /**
   * find org document draff by orgDraffId
   */
  public findOrgDocumentDraff(id: number) {
    const orgDraffId = CommonUtils.nvl(id);
    const url = `${this.serviceUrl}/${orgDraffId}/org-document-draff`;
    return this.getRequest(url);
  }

  /**
   * saveOrUpdateOtherInfo draff
   */
  public saveOrUpdateOtherInfoDraff(item: any, orgDraffId: number): Observable<any> {
    const formdata = CommonUtils.convertFormFile(item);
    const url = `${this.serviceUrl}/${orgDraffId}/other-info-draff`;
    return this.postRequest(url, formdata);
  }

  /**
   * find org boundary draff by orgDraffId
   */
  public findOrgBoundaryDraff(id: number) {
    const orgDraffId = CommonUtils.nvl(id);
    const url = `${this.serviceUrl}/${orgDraffId}/org-boundary-draff`;
    return this.getRequest(url);
  }

  /**
   * get org draff
   */
  public findOneWithPlan(orgId: number, planId?: number): Observable<any> {
    const organizationId = CommonUtils.nvl(orgId);
    const url = `${this.serviceUrl}/${organizationId}`;
    return this.getRequest(url);
  }
}
