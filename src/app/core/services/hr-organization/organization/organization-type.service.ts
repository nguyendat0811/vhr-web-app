import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HelperService } from '@app/shared/services/helper.service';
import { BasicService } from '../../basic.service';

@Injectable({
  providedIn: 'root'
})
export class OrganizationTypeService extends BasicService {

  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('emp', 'orgType', httpClient, helperService);
  }
  public getListOrgType(): Observable<any> {
    const listOrgTypeUrl = `${this.serviceUrl}/`;
    return this.getRequest(listOrgTypeUrl);
  }
}
