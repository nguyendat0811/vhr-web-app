import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmpCodeConfigService  extends BasicService  {
  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('emp', 'empCodeConfig', httpClient, helperService);
  }

  public searchData(item: any, event?: any): Observable<any> {
    if (event) {
      item._search = JSON.stringify(event); ;
    }
    const url = `${this.serviceUrl}/search`;
    return this.getRequest(url,{params: item});  
  }

  public findAllMarket(): Observable<any> {
    const url = `${this.serviceUrl}/find-all-market`;
    return this.getRequest(url);
  }

}
