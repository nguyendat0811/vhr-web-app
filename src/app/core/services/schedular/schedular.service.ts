import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';
import { CommonUtils } from '@app/shared/services';
import { HrStorage } from '../HrStorage';


@Injectable({
  providedIn: 'root'
})
export class SchedularService extends BasicService {
  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('schedular', 'oauthToken', httpClient, helperService);
    // this.http = httpClient;
  }
  // private http : HttpClient;

  public getShiftEmployee(data: any): Observable<any> {
    const user = HrStorage.getUserToken();
    data = data || {};
    data.employeeIdEdit = user.employeeCode;
    const urlStatus = `${this.serviceUrl}timekeeping/shift/employee/get`;
    this.helperService.isProcessing(true);
    return this.postRequest(urlStatus, data);
  }
  public searchHistory(data: any): Observable<any> {
    const user = HrStorage.getUserToken();
    data = data || {};
    data.employeeIdEdit = user.employeeCode;
    const urlStatus = `${this.serviceUrl}timekeeping/shift/employee/log/get`;
    return this.postRequest(urlStatus, data);
  }
  public shiftReport(data: any): Observable<any> {
    const user = HrStorage.getUserToken();
    data = data || {};
    data.employeeIdEdit = user.employeeCode;
    const urlStatus = `${this.serviceUrl}timekeeping/shift/report`;
    return this.postRequest(urlStatus, data);
  }


  public searchShiftLock(data: any): Observable<any> {
    const user = HrStorage.getUserToken();
    data = data || {};
    data.employeeIdEdit = user.employeeCode;
    const url = `${this.serviceUrl}shiftslocking/shift/lock/get`;
    return this.postRequest(url, data);
  }

  public searchEmployeeWorkTime(data: any): Observable<any> {
    const user = HrStorage.getUserToken();
    data = data || {};
    data.employeeIdEdit = user.employeeCode;
    const url = `${this.serviceUrl}timework/employee/get`;
    return this.postRequest(url, data);
  }

  public updateEmployeeWorkTime(data: any): Observable<any> {
    const user = HrStorage.getUserToken();
    data = data || {};
    data.employeeIdEdit = user.employeeCode;
    const url = `${this.serviceUrl}timework/employee/update`;
    return this.postRequest(url, data);
  }

  public getWarningTimeworkReport(data: any): Observable<any> {
    const url = `${this.serviceUrl}timework/report/warning`;
    return this.postRequest(url, data);
  }

  public getBreachTimeworkReport(data: any): Observable<any> {
    const url = `${this.serviceUrl}timework/report/breach`;
    return this.postRequest(url, data);
  }

  public updateShiftLock(data: any): Observable<any> {
    const user = HrStorage.getUserToken();
    data = data || {};
    data.employeeIdEdit = user.employeeCode;
    const url = `${this.serviceUrl}shiftslocking/shift/lock/update`;
    return this.postRequest(url, data);
  }

  public searchWorkLock(data: any): Observable<any> {
    const user = HrStorage.getUserToken();
    data = data || {};
    data.employeeIdEdit = user.employeeCode;
    const url = `${this.serviceUrl}worklock/work/lock/get`;
    return this.postRequest(url, data);
  }

  public updateWorkLock(data: any): Observable<any> {
    const user = HrStorage.getUserToken();
    data = data || {};
    data.employeeIdEdit = user.employeeCode;
    const url = `${this.serviceUrl}worklock/work/lock/update`;
    return this.postRequest(url, data);
  }
  public getTimeKeeping(data: any): Observable<any> {
    const user = HrStorage.getUserToken();
    data = data || {};
    data.employeeIdEdit = user.employeeCode;
    const url = `${this.serviceUrl}schedular/timesheet/get`;
    return this.postRequest(url, data);
  }
  public updateTimeKeeping(data: any): Observable<any> {
    const user = HrStorage.getUserToken();
    data = data || {};
    data.employeeIdEdit = user.employeeCode;
    const url = `${this.serviceUrl}schedular/timesheet/update`;
    return this.postRequest(url, data);
  }
  public getShiftEmploy(data: any): Observable<any> {
    const user = HrStorage.getUserToken();
    data = data || {};
    data.employeeIdEdit = user.employeeCode;
    const urlStatus = `${this.serviceUrl}/shift/employee/get`;
    return this.postRequestExtend(urlStatus, CommonUtils.convertData(data));
  }
  public getWorkType(): Observable<any> {

    const urlStatus = `${this.serviceUrl}workType/find-all-working-shift-type`;
    return this.getRequest(urlStatus);
  }
  public importShiftEmploy(data: any): Observable<any> {
    const user = HrStorage.getUserToken();
    data = data || {};
    data.employeeIdEdit = user.employeeCode;
    const urlStatus = `${this.serviceUrl}timekeeping/shift/employee/update`;
    return this.postRequest(urlStatus, CommonUtils.convertData(data));
  }
  public checkPermission(data: any): Observable<any> {
    const user = HrStorage.getUserToken();
    data = data || {};
    data.employeeIdEdit = user.employeeCode;
    const urlStatus = `${this.serviceUrl}timekeeping/shift/change/get`;
    return this.postRequest(urlStatus, CommonUtils.convertData(data));
  }

  public getLock(data: any): Observable<any> {
    const user = HrStorage.getUserToken();
    data = data || {};
    data.employeeIdEdit = user.employeeCode;
    const urlStatus = `${this.serviceUrl}schedular/timesheet/lock/get`;
    return this.postRequest(urlStatus, data);
  }

  public updateLock(data: any): Observable<any> {
    const user = HrStorage.getUserToken();
    data = data || {};
    data.employeeIdEdit = user.employeeCode;
    const urlStatus = `${this.serviceUrl}schedular/timesheet/lock/update`;
    return this.postRequest(urlStatus, data);
  }
}

