import { Injectable } from '@angular/core';
import * as XLSX from 'ts-xlsx';
import { ShiftEmploy, WorkDay } from '@app/core/models/employee.model';
import { Observable, Subscriber } from 'rxjs';
import * as Excel from 'exceljs/dist/exceljs.min.js';
import * as ExcelProper from 'exceljs';
import * as FileSaver from 'file-saver';
import * as moment from 'moment'

const EXCEL_EXTENSION = '.xlsx';
const blobType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';


@Injectable({
  providedIn: 'root'
})


export class ReadFileSchedularService {


  public getDynamicHeader(fromDate: Date, toDate: Date, parentHeader: number): Partial<IHeaderConfig[]> {
    toDate = new Date(toDate);
    toDate = new Date(toDate.getFullYear(), toDate.getMonth(), toDate.getDate(), 23, 59, 59);
    let index = 1;
    let dynamicHeaders = Array<IHeaderConfig>();

    for (let currentDate = new Date(fromDate); currentDate <= toDate; currentDate.setDate(currentDate.getDate() + 1)) {
      const day = currentDate.getDay();
      dynamicHeaders.push({
        index,
        title: `${currentDate.getDate()}(${day == 0 ? "CN" : "T" + (day + 1)})`,
        key: "" + currentDate.getDate(),
        extend: day,
        width: 6,
        fixed: false,
        parentHeader,
        date: moment(currentDate).format("YYYY-MM-DD"),
        color:  'FFFF99'
      });
      index++;
    }
    return dynamicHeaders;
  }
  private getHeaderFromStaticConfig(headerGroupName: string): IHeaderConfig[] {
    return [];
  }
  public getHeader(param: IParamExport): IHeaderConfig[] {
    param = param || {};
    if (param.headerConstant)
      return this.getHeaderFromStaticConfig(param.headerConstant);
    let currentDate = param.month || new Date();
    let fromDate = param.fromDate || new Date(currentDate.getFullYear(), currentDate.getMonth(), 1);
    let toDate = param.toDate || new Date(currentDate.getFullYear(), currentDate.getMonth() + 1, 0);
    let dynamic = this.getDynamicHeader(fromDate, toDate, 7);
    let headers = [
      { index: 1, title: 'STT', key: 'index', width: 5, fixed: true, parentHeader: 0, color: "FFFF99" },
      { index: 2, title: 'Mã nhân viên', key: 'employeeCode', width: 10, fixed: true, parentHeader: 0, color: "FFFF99" },
      { index: 3, title: 'Họ tên', key: 'employeeName', width: 20, fixed: true, parentHeader: 0, color: "FFFF99" },
      { index: 4, title: 'Chức danh', key: 'title', width: 15, fixed: true, parentHeader: 0, color: "FFFF99" },
      { index: 5, title: 'Ký hiệu đơn vị', key: 'organizationCode', width: 10, fixed: true, parentHeader: 0, color: "FFFF99" },
      { index: 6, title: 'Chức vụ', key: 'unitName', width: 15, fixed: true, parentHeader: 0, color: "FFFF99" },
      { index: 7, title: 'Ngày trong tháng', key: null, width: 6, fixed: true, parentHeader: 0, color: "FFFF99" },
      { index: 8, title: 'Trạng thái', key: 'verified', width: 10, fixed: true, parentHeader: 0, color: "FFFF99" },
    ] as IHeaderConfig[];
    headers = dynamic ? headers.concat(dynamic) : headers;
    return headers;
  }
  private buildHeader(worksheet: ExcelProper.Worksheet, headers: IHeaderConfig[], startRow: number) {
    var row = worksheet.getRow(startRow);
    var subRow : ExcelProper.Row;
    let currentColl = 1;
    for (let index = 0; index < headers.length; index++) {
      let header = headers[index];
      if (header.parentHeader) {
        // child
        subRow = subRow || worksheet.getRow(startRow + 1);
        let parent = headers.find(x => x.index == header.parentHeader);
        let collChild = subRow.getCell(header.index + parent.index - 1);
        collChild.style = header.style || {};
        collChild.font = { bold: true };
        collChild.value = header.title;
        collChild.fill = header.color ? { type: 'pattern', pattern: 'darkTrellis', fgColor: { argb: header.color }, bgColor: { argb: header.color } } : null;
        header.realIndex = header.index + parent.index - 1;
        worksheet.getColumn(header.index + parent.index - 1).key = header.key;
        worksheet.getColumn(header.index + parent.index - 1).width = header.width;
        continue;
      }


      // not child
      let coll = row.getCell(currentColl);
      coll.value = header.title;
      coll.style = header.style || {};
      coll.fill = header.color ? { type: 'pattern', pattern: 'darkTrellis', fgColor: { argb: header.color }, bgColor: { argb: header.color } } : null;
      coll.font = { bold: true }

      if (!header.key) {
        //parent child will not have key
        let sub = headers.filter(x => x.parentHeader == header.index);
        if (sub && sub.length) {
          row.getCell(currentColl + sub.length - 1).merge(coll);
          row.getCell(currentColl + sub.length - 1).style.alignment = { horizontal: "center", vertical: "middle" }
          currentColl += sub.length
        }
        continue
      }
      //not parent
      subRow = subRow || worksheet.getRow(startRow + 1);
      subRow.getCell(currentColl).merge(coll);
      subRow.getCell(currentColl).alignment = { horizontal: "center", vertical: "middle" };
      worksheet.getColumn(currentColl).width = header.width;
      worksheet.getColumn(currentColl).key = header.key;
      worksheet.getColumn(currentColl).protection = { locked: true };
      header.realIndex = currentColl;
      // span header
      if (header.spanCol && header.spanCol > 1) {
        row.getCell(currentColl + header.spanCol - 1).merge(coll);
        currentColl += header.spanCol - 1;
      }
      currentColl++;
    }
    row.eachCell({ includeEmpty: true }, x => x.border = { top: { style: "thin" }, left: { style: "thin" }, bottom: { style: "thin" }, right: { style: "thin" } });
    subRow && subRow.eachCell({ includeEmpty: true }, x => x.border = { top: { style: "thin" }, left: { style: "thin" }, bottom: { style: "thin" }, right: { style: "thin" } });
  }
  private buildTitle(worksheet: ExcelProper.Worksheet, headerLength: number, titleReport?: string, org?: string, config?: IFixedTitle[]) {
    var tittleHeader = config || [
      { start: 1, tittle: "CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM", style: {} },
      { start: 2, tittle: "Độc lập - Tự do - Hạnh phúc", style: {} },
      { start: 5, tittle: titleReport || "BẢNG LẬP LỊCH PHÂN CA", style: {} },
      { start: 6, tittle: org || "Đơn vị: TQG01 - TQG, P, 607, Tổ 24, Tuyên Quang", style: {} }
    ];

    for (const titleCfg of tittleHeader) {
      let cell = worksheet.getCell(`A${titleCfg.start}`);
      // if (titleCfg.style && Object.keys(titleCfg.style).length !== 0)
      cell.style = titleCfg.style || {};
      // else {
      cell.font = { bold: true };
      // }
      cell.value = titleCfg.tittle;
      cell.alignment = { horizontal: 'center', };
      worksheet.mergeCells(titleCfg.start, 1, titleCfg.start, headerLength);
    }
  }
  private buildData(worksheet: ExcelProper.Worksheet, source: any[], groupBy: string, sheetWidth: number, colSpanHeaders: IHeaderConfig[] = []) {
    var index = 0;
    var groupOrgs = this.groupBy(source, groupBy);
    console.log(source);

    for (const iterator in groupOrgs) {
      let so = groupOrgs[iterator];
      //Set groupHeader
      var row = worksheet.addRow({ "index": iterator });
      row.getCell(sheetWidth).merge(row.getCell(1))
      row.getCell(1).font = { bold: true };
      row.getCell(1).border = { top: { style: "thin" }, left: { style: "thin" }, bottom: { style: "thin" }, right: { style: "thin" } };
      //binding data
      for (let i = 0; i < so.length; i++) {
        let item = { ...so[i] };
        item["index"] = ++index;
        if (item.workDay && item.workDay.length) {
          for (const i of item.workDay) {
            item[i.date] = i.shiftCodes;
          }
        }
        const newRows = worksheet.addRow(item);
        colSpanHeaders.forEach(h => {
          newRows.getCell(h.index + h.spanCol - 1).merge(newRows.getCell(h.index));
        });
        newRows.eachCell({ includeEmpty: true }, x =>
          x.border = { top: { style: "thin" }, left: { style: "thin" }, bottom: { style: "thin" }, right: { style: "thin" } }
        )
      }
    }
  }
  public getWorkSheetExport(source: any[], excelFileName: string, config?: IParamExport): ExcelProper.Workbook {
    config = config || {};
    const workbook: ExcelProper.Workbook = new Excel.Workbook();
    let worksheet = workbook.addWorksheet(excelFileName);
    let headers = config.headers || this.getHeader(config);
    let sheetWidth = headers.filter(x => x.key).length;

    this.buildTitle(worksheet, sheetWidth, config.reportTitle, config.orgMaster, config.tittleConfig);

    //Header table
    this.buildHeader(worksheet, headers, config.headerStart || 9);

    //Binding data
    console.log(source);
    this.buildData(worksheet, source, config.groupBy || "organizationName", sheetWidth);
    return workbook;
  }
  public exportAsExcelFile(source: any[], excelFileName: string, config?: Partial<IParamExport>): void {
    const workbook = this.getWorkSheetExport(source, excelFileName, config);
    //SaveFile
    this.exportFile(workbook, excelFileName);
  }
  public exportExcelMultipleSource(excelFileName: string, configs: Partial<IParamExport[]>): void {
    configs = configs || [];

    const workbook: ExcelProper.Workbook = new Excel.Workbook();
    const worksheet = workbook.addWorksheet(excelFileName);
    let maxSheetWidth = 1;


    for (const config of configs) {
      let headers = config.headers || this.getHeader(config);
      let currentWidth = headers.filter(x => x.key).length;
      const colSpanHeaders = headers.filter(h => h.spanCol > 1);
      if (colSpanHeaders.length > 0) {
        currentWidth += colSpanHeaders.map(h => h.spanCol - 1).reduce((p, c) => p + c);
      }
      maxSheetWidth = maxSheetWidth < currentWidth ? currentWidth : maxSheetWidth;
      //Build header label for dataSource
      if (config.tittleStartDataSource) {
        let row = worksheet.getRow(config.headerStart && config.headerStart - 1 || worksheet.lastRow.number + 3);
        let cellHeader = row.getCell(1);
        cellHeader.alignment = { wrapText: true, }
        cellHeader.value = config.tittleStartDataSource;
        cellHeader.font = { bold: true };
        row.getCell(3).merge(cellHeader);
      }
      //Header table. default next row;

      this.buildHeader(worksheet, headers, config.headerStart || worksheet.lastRow.number + 1);
      console.log("Datasource export:", config.dataSource);

      //Binding data
      this.buildData(worksheet, config.dataSource || [], config.groupBy || "organizationName", maxSheetWidth, colSpanHeaders);
    }
    const config = configs[0] || {};

    this.buildTitle(worksheet, maxSheetWidth, config.reportTitle, config.orgMaster, config.tittleConfig);
    //SaveFile
    this.exportFile(workbook, excelFileName);
  }
  public exportFile(workbook: ExcelProper.Workbook, excelFileName: string) {
    workbook.xlsx.writeBuffer().then((data) => {
      const blob = new Blob([data], { type: blobType });
      FileSaver.saveAs(blob, excelFileName + "_" + moment(new Date).format("MM-DD_HH:mm"));
    });
  }

  private parseDynamicHeaders(worksheet: XLSX.IWorkSheet, headerStart: number, exportHeaders: IHeaderConfig[]): IHeaderConfig[] {
    const csvData = XLSX.utils.sheet_to_csv(worksheet).split("\n");
    const dynamicHeaders = csvData[headerStart].split(",").filter(Boolean);
    const dynamicParentHeaderIndex = exportHeaders.findIndex((h, i) => !h.key);
    const fixedExportHeader = exportHeaders.filter(header => header.fixed);
    const dynamicParentHeader = exportHeaders[dynamicParentHeaderIndex];
    const dynamicHeadersData = dynamicHeaders.map((h, index) => ({
      fixed: false,
      index: index + 1,
      key: parseInt(h) + "", // date(dow) => date
      parentHeader: dynamicParentHeader.index,
      title: h,
      width: 6,
    } as IHeaderConfig));
    return fixedExportHeader.slice(0, dynamicParentHeaderIndex + 1).concat(dynamicHeadersData, fixedExportHeader.slice(dynamicParentHeaderIndex + 1));
  }

  private sortHeaders(headers: IHeaderConfig[]): IHeaderConfig[] {
    return headers.sort((a, b) => {
      if (a.parentHeader > b.parentHeader)
        return a.parentHeader > b.index ? 1 : -1
      return a.index > b.index ? 1 : -1
    });
  }

  public readFile<Type>(file, config?: IParamExport): Observable<IImportData<Type>> {
    const self = this;
    config = config || {};
    let fileReader = new FileReader();
    let exportHeaders = config.headers || this.getHeader(config);

    fileReader.readAsArrayBuffer(file);
    return Observable.create((observer: Subscriber<IImportData<Type>>): void => {
      // if success
      fileReader.onload = (e) => {
        let arrayBuffer = fileReader.result as any;
        let data = new Uint8Array(arrayBuffer);
        let arr = new Array();
        for (let i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
        let bstr = arr.join("");
        let workbook = XLSX.read(bstr, { type: "binary" });
        let first_sheet_name = workbook.SheetNames[0];
        let worksheet = workbook.Sheets[first_sheet_name];
        let headers = exportHeaders.some(h => !h.fixed) ? self.parseDynamicHeaders(worksheet, config.headerStart || 9, exportHeaders) : exportHeaders;

        let jsonData = XLSX.utils.sheet_to_json(worksheet, { header: headers.map(x => x.key && x.title).filter(Boolean) });
        let importData = {
          ColumnDetail: headers,
          SheetMaster: first_sheet_name,
          Datasource: this.transformData<Type>(jsonData, headers, config.primaryKey || "employeeCode", config.groupBy || 'organizationName'),
          FileName: "",
          Title: "",
        }
        observer.next(importData);
        observer.complete();
      }
      // if failed
      fileReader.onerror = (error: any): void => {
        observer.error(error);
      }
    });
  }
  public groupBy(dataSource: any[], key: string): { [key: string]: any[] } {
    return dataSource.reduce(function (rv, x) {
      (rv[x[key]] = rv[x[key]] || []).push(x);
      return rv;
    }, {});
  };
  private transformData<Type>(dataSource: Array<any>, headers: IHeaderConfig[], primaryKey: string, groupBy: string) {
    let fixHeaders = headers.filter(x => x.fixed && x.key).reduce((acc, curr) => (acc[curr.key] = curr.title, acc), {});
    let dynamicHeader = headers.filter(x => !x.fixed);
    let source = Array<Type>();
    let headerStart = 0;
    let groupValue = "";
    for (let index = 0; index < dataSource.length; index++) {
      if (!dataSource[index][fixHeaders[primaryKey]]) {
        if (headerStart != 0)
          groupValue = dataSource[index][Object.keys(dataSource[index])[0]];
        // filler empty data and check current org
        continue;
      }

      if (dataSource[index][fixHeaders[primaryKey]] == fixHeaders[primaryKey]) {
        //header start
        headerStart = index;
        continue;
      }
      let element = dataSource[index];
      let item = {};
      for (let prop in fixHeaders) {
        item[prop] = element[fixHeaders[prop]];
      }
      item["workDay"] = this.getDayShift(element, dynamicHeader);
      item[groupBy] = groupValue;
      source.push(item as Type);
    }

    return source;
  }
  private getDayShift(value: any, headers: IHeaderConfig[]): Array<WorkDay> {
    let source = new Array<any>();
    for (const iterator of headers) {
      if (value[iterator.title]) {
        source.push({ date: iterator.key, shiftCodes: value[iterator.title] })
      }
    }
    return source;
  }


}
export interface IParamExport {
  /**
   * Report title label
   */
  reportTitle?: string,
  /**
   * Name organization master
   */
  orgMaster?: string,
  /**
   * Header config
   * default :
   */
  headers?: Partial<IHeaderConfig[]>,
  /**
   * default start from 9
   */
  headerStart?: number,
  /**
   * Header group constant declare config
   */
  headerConstant?: string,
  /**
   * Group by data
   * default : organizationName
   */
  groupBy?: string,
  /**
   * config tittle
   */
  tittleConfig?: IFixedTitle[];
  /**
   * Default the first day of month
   */
  fromDate?: Date,
  /**
   * default the last day of month
   */
  toDate?: Date,
  /**
   * default current month
   */
  month?: Date,
  tittleStartDataSource?: string,
  dataSource?: any[],
  primaryKey?: string,
}
export interface IFixedTitle {
  start: number, tittle: string, style?: Partial<ExcelProper.Style>
}

export interface IHeaderConfig {
  index: number,
  title: string,
  key: string,
  width: number,
  fixed: boolean,
  parentHeader: number,
  realIndex?: number,
  extend?: any,
  date?: string,
  spanCol?: number,
  //rgb color without #
  color?: string,
  style?: Partial<ExcelProper.Style>
}

export class ImportData {
  ColumnDetail: Array<IHeaderConfig>;
  Datasource: Array<ShiftEmploy>;
  Title: string;
  FileName: string;
  SheetMaster?: string;
}
export interface IImportData<Type> {
  //  (arg: Type): Type;
  ColumnDetail: Array<IHeaderConfig>;
  Datasource: Array<Type>;
  Title: string;
  FileName: string;
  SheetMaster?: string;
}
