import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';

@Injectable({
  providedIn: 'root'
})
export class LongLeaveReasonService extends BasicService {
  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('timekeeping', 'longLeaveReason', httpClient, helperService);
  }

  public findAllLongLeaveReason(): Observable<any> {
    const urlStatus = `${this.serviceUrl}/find-all`;
    return this.getRequest(urlStatus);
  }

  public findLongLeaveReasonByMarketId(): Observable<any> {
    const urlStatus = `${this.serviceUrl}/long-leave-reason-by-market-id`;
    return this.getRequest(urlStatus);
  }
}
