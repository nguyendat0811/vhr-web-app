import {HttpClient} from '@angular/common/http';
import {BasicService} from '@app/core/services/basic.service';
import {Injectable} from '@angular/core';
import {HelperService} from "@app/shared/services/helper.service";

@Injectable({
  providedIn: 'root'
})
export class HolidayService extends BasicService {

  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('timekeeping', 'holiday', httpClient, helperService);
  }

  /**
   * findByDate
   */
  public findByDate(holidayDate: any) {
    const day = holidayDate;
    const url = `${this.serviceUrl}/find-by-date/${day}`;
    return this.getRequest(url);
  }

  /**
   * findByYear
   */
  public findByYear(year: any) {
    const years = year;
    const url = `${this.serviceUrl}/find-by-year/${years}`;
    return this.getRequest(url);
  }

  /**
   * cloneHoliday
   */
  public cloneHoliday(data) {
    const url = `${this.serviceUrl}/clone-holiday`;
    return this.postRequest(url, data);
  }
}
