import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';

@Injectable({
  providedIn: 'root'
})
export class WorkdayTypeService extends BasicService {
  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('timekeeping', 'workdayType', httpClient, helperService);
  }

  public findAllWorkdayType(): Observable<any> {
    const urlStatus = `${this.serviceUrl}/find-all`;
    return this.getRequest(urlStatus);
  }

  public findAllWorkdayTypeByMarketId(): Observable<any> {
    const urlStatus = `${this.serviceUrl}/find-all-workday-type-by-market-id`;
    return this.getRequest(urlStatus);
  }

  public getListWorkdayTypeCode(): Observable<any> {
    const urlStatus = `${this.serviceUrl}/get-list-workday-type-code`;
    return this.getRequest(urlStatus);
  }
}
