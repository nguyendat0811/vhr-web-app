import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {HelperService} from '@app/shared/services/helper.service';
import {BasicService} from '../basic.service';
import {Observable} from 'rxjs';
import {CommonUtils} from '@app/shared/services';

@Injectable({
  providedIn: 'root'
})
export class OpenCloseScheduleService extends BasicService {
  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('timesheet', 'openCloseMarket', httpClient, helperService);

  }

  public findAllDetail(id: number): Observable<any> {
    const urlStatus = `${this.serviceUrl}/${id}/find-all-detail`;
    return this.getRequest(urlStatus);
  }

  public findAllWeekend(id: number): Observable<any> {
    const urlStatus = `${this.serviceUrl}/${id}/find-all-weekend`;
    return this.getRequest(urlStatus);
  }

  public processCheckApply(id: number): Observable<any> {
    const urlStatus = `${this.serviceUrl}/check-apply/${id}`;
    return this.getRequest(urlStatus);
  }

  public processApply(id: number, formData?): Observable<any> {
    const urlStatus = `${this.serviceUrl}/apply/${id}`;
    return this.postRequest(urlStatus, CommonUtils.convertData(formData));
  }

  public processApplyEmp(id: number, formArray): Observable<any> {
    const urlStatus = `${this.serviceUrl}/apply-emp/${id}`;
    return this.postRequest(urlStatus, CommonUtils.convertData(formArray));
  }

  public processDetailReport(data): Observable<any> {
    const url = `${this.serviceUrl}/detail`;
    return this.getRequest(url, {params: data, responseType: 'blob'});
  }

  // Huynq73: Lap lich lam viec cho nhan vien dc them moi
  public processSaveEmpWorkSchedule(data: any): Observable<any> {
    const url = `${this.serviceUrl}/emp-work-schedule`;
    return this.postRequest(url, data);
  }

  // Huynq73: Tim lich lam viec dc cau hinh cho don vi cua nhan vien
  public findByOrganizationId(organizationId: number): Observable<any> {
    const url = `${this.serviceUrl}/organization-id/${organizationId}`;
    return this.getRequest(url);
  }

  findAllInsuranceType() {
    const urlStatus = `${this.serviceUrl}/find-insurance-type`;
    return this.getRequest(urlStatus);
  }

  public importOpenCloseMarket(data) {
    const url = `${this.serviceUrl}/import-open-close-market`;
    const formData = CommonUtils.convertFormFile(data);
    return this.postRequest(url, formData);
  }

  public downloadTemplateImport() {
    const url = `${this.serviceUrl}/download-template-import`;
    return this.getRequest(url, {responseType: 'blob'});
  }

  public downloadResultsImport(data) {
    const url = `${this.serviceUrl}/download-results-import/` + data;
    return this.getRequest(url, {responseType: 'blob'});
  }
}

