import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';

import { HelperService } from '@app/shared/services/helper.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CommonUtils } from '@app/shared/services';

@Injectable({
  providedIn: 'root'
})
export class OrganizationKiService extends BasicService {

  constructor(public httpClient: HttpClient, public helperService: HelperService) { 
    super('emp','organization-ki',httpClient, helperService);
  }

  // get Ki from sysCat
  public getListKi(): Observable<any>{
    const url = `${this.serviceUrl}/list-ki`;
    return this.getRequest(url);
  }

  public processImport(data): Observable<any> {
    const url = `${this.serviceUrl}/import`;
    const formdata = CommonUtils.convertFormFile(data);
    return this.postRequest(url, formdata);
  }
  public downloadTemplateImport(data): Observable<any> {
    const url = `${this.serviceUrl}/export-template`;
    return this.getRequest(url, {params: data, responseType: 'blob'});
  }  

  public prepareExport(data): Observable<any> {
    const url = `${this.serviceUrl}/prepare-export`;
    return this.getRequest(url, {params: data});
  }
   
  public processExport(data?: any, event?: any): Observable<any> {
    if (!event) {
      this.credentials = Object.assign({}, data);
    }
    const searchData = CommonUtils.convertData(this.credentials);
    if (event) {
      searchData._search = event;
    }
    const buildParams = CommonUtils.buildParams(searchData);
    const url = `${this.serviceUrl}/export`;
    return this.getRequest(url, {params: buildParams, responseType: 'blob'});
  }
  public getYearList() {
    var listYear = [];
    const currentYear = new Date().getFullYear();
    for (let i = (currentYear - 1 ) ; i <= (currentYear + 1) ; i++ ) {
      const obj = {
        year: i
      };
      listYear.push(obj);
    }
    return listYear;
  }

  public getQuaterList() {
    var listQuarter = [];
    for (let i = 1 ; i <=4 ; i++ ) {
      const obj = {
        quarter: i
      };
      listQuarter.push(obj);
    }
    return listQuarter;
  }

  public getMonthList() {
    var listMonth = [];
    for (let i = 1 ; i <=12 ; i++ ) {
      const obj = {
        month: i
      };
      listMonth.push(obj);
    }
    return listMonth;
  }
}
