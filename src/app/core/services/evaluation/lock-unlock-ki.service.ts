import { FormArray } from '@angular/forms';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';
import { BasicService } from '../basic.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LockUnlockKiService extends BasicService {
  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('emp', 'lockUnlockKi', httpClient, helperService);
  }

  public processDetailReport(data): Observable<any> {
    const url = `${this.serviceUrl}/report`;
    return this.getRequest(url, {params: data, responseType: 'blob'});
  }
}
