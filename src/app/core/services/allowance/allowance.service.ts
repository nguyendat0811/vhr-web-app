import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';
import {AllowanceTypeService} from '../allowance-type/allowance-type.service';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class AllowanceService extends BasicService {
  constructor(public httpClient: HttpClient, public helperService: HelperService, public allowanceTypeService: AllowanceTypeService) {
    super('emp', 'allowance', httpClient, helperService);
  }
  public getAllowanceType(): Observable<any>{
    return this.allowanceTypeService.findAllowanceTypeList();
  }
  public getAllowanceTypeById( allowanceTypeId: number): Observable<any>{
    return this.allowanceTypeService.findById(allowanceTypeId);
  }
}
