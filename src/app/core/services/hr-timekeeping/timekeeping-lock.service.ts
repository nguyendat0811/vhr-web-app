import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';
import { Observable } from 'rxjs/Observable';
import { CommonUtils } from '@app/shared/services';

@Injectable({
  providedIn: 'root'
})
export class TimekeepingLockService extends BasicService {
  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('timekeeping', 'timekeepingLock', httpClient, helperService);
  }

  public getDate(): Observable<any> {
    const url = `${this.serviceUrl}/get-date`;
    return this.getRequest(url);
  }
}
