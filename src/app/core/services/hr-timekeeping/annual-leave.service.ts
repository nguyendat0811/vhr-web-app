import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';
import { Observable } from 'rxjs';
import { CommonUtils } from '@app/shared/services';

@Injectable({
  providedIn: 'root'
})
export class AnnualLeaveService extends BasicService {
  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('timekeeping', 'annuaLeave', httpClient, helperService);
  }

  public export(data): Observable<any> {
    const url = `${this.serviceUrl}/export`;
    return this.getRequest(url, {params: data, responseType: 'blob'});
  }

  public calculate(data): Observable<any> {
    const url = `${this.serviceUrl}/calculate-al`;
    return this.getRequest(url, {params: data});
  }

  public getDateConfig(data): Observable<any> {
    const url = `${this.serviceUrl}/date-config`;
    return this.getRequest(url, {params: data});
  }

}
