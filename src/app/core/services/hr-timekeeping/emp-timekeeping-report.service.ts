import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmpTimekeepingReportService extends BasicService {
  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('timekeeping', 'empTimekeepingReport', httpClient, helperService);
  }
  public processReportSummary(data): Observable<any> {
    const url = `${this.serviceUrl}/summary`;
    return this.getRequest(url, {params: data, responseType: 'blob'});
  }

  public processDetailReport(data): Observable<any> {
    const url = `${this.serviceUrl}/detail`;
    return this.getRequest(url, {params: data, responseType: 'blob'});
  }
}
