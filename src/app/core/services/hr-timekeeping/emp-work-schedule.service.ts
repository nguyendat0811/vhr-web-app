import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HelperService } from '@app/shared/services/helper.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmpWorkScheduleService extends BasicService {
  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('timekeeping', 'empWorksChedule', httpClient, helperService);
  }
  public findEmpCalendar(empWorkScheduleId, fromDate, toDate): Observable<any> {
    const url = `${this.serviceUrl}/${empWorkScheduleId}/calendar`;
    return this.getRequest(url, {params: {fromDate: fromDate, toDate: toDate}});
  }
  public processOnOffWorkingDay(empWorkScheduleId, data): Observable<any> {
    const url = `${this.serviceUrl}/${empWorkScheduleId}/on-off-working-day`;
    return this.postRequest(url, data);
  }
  public findEmpWorkOff(empWorkScheduleId, dateTimekeeping): Observable<any> {
    const url = `${this.serviceUrl}/${empWorkScheduleId}/emp-work-off`;
    return this.getRequest(url, {params:  {dateTimekeeping: dateTimekeeping}});
  }
  public getEmpWorkScheduleBeanById(employeeId): Observable<any> {
    const url = `${this.serviceUrl}/${employeeId}/get-emp-work-schedule-bean-by-employee-id`;
    return this.getRequest(url);
  }
}
