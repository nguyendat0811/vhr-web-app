import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TimekeepingLockConfigService extends BasicService {
  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('timekeeping', 'timekeepingLockConfig', httpClient, helperService);
  }

  public getListConfigByOrdId(organizationId: Number): Observable<any> {
    const url = `${this.serviceUrl}/${organizationId}`;
    return this.getRequest(url);
  }
}
