import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { BasicService } from '../basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';

@Injectable({
  providedIn: 'root'
})
export class LabourContractTypeService extends BasicService {
  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('emp', 'labourContractType', httpClient, helperService);
  }

  public findByLabourContractTypeId(labourContractTypeId: number) {
    const url = `${this.serviceUrl}/${labourContractTypeId}/labour-cotract-detail`;
    return this.getRequest(url);
  }

  public getAllLabourContractTypeByStatus(): Observable<any> {
    const url = `${this.serviceUrl}/by-market-and-status`;
    return this.getRequest(url);
  }

  public checkLabourType(code: any): Observable<any> {
    const url = `${this.serviceUrl}/${code}/check-labour-type`;
    return this.getRequest(url);
  }

  public findByIdMarketCompanyId(marketCompanyId: number, labourContractTypeId: number): Observable<any> {
    const url = `${this.serviceUrl}/${marketCompanyId}/${labourContractTypeId}`;
    return this.getRequest(url);
  }

  public findActiveLabourContractType(): Observable<any> {
    const url = `${this.serviceUrl}/by-market-and-status-active`;
    return this.getRequest(url);
  }
}
