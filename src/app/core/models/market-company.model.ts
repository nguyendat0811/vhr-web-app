export class MarketCompany {
  marketCompanyId:    number;
  code:               string;
  logoPath:           string;
  organizationId:     boolean;
  nationId:           string;
}
