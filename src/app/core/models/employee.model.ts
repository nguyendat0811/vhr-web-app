export class EmployeeBean {
  employeeId: number;
  nationId: number;
  employeeCode: string;
  fullName: string;
  aliasName: string;
  dateOfBirth: number;
  placeOfBirth: string;
  gender: number;
  email: string;
  otherEmail: string;
  marketEmail: string;
  origin: string;
  maritalStatus: number;
  permanentAddress: string;
  pidNumber: string;
  pidIssuedDate: number;
  pidIssuedPlace: string;
  passportIssueDate: number;
  passportNumber: string;
  currentAddress: number;
  phoneNumber: string;
  mobileNumber: string;
  fax: string;
  imagePath: string;
  mobileNumber2: string;
  mobileNumber3: string;
  emailStatus: number;
  taxNumber: string;
  taxNumberUpdatedTime: number;
  taxCodeDate: number;
  taxManageOrgId: number;
  taxManageOrg: string;
  personalContact: string;
  ethnicId: number;
  religionId: number;
  organizationId: number;
  organizationName: String;
  createdTime: number;
  modifiedTime: number;
  createdBy: string;
  modifiedBy: string;
  empTypeId: number;
  labourContractTypeId: number;
  labourContractTypeName: string;
  positionId: number;
  status: number;
  viettelStartDate: number;
  note: string;
  recruitTypeId: number;
  firstName: string;
  lastName: string;
  middleName: string;
}


export class WorkDay {
  constructor(
    public date: number,
    public shiftCodes: string,
    public shiftCodeRaw: string,
    public dayOfWeek: number,
    public title: string,
    public isShow: boolean,
    public locked: boolean,
    public shiftDate?: string ,

  ) { }

  public isWeekend(): boolean {
    return (this.dayOfWeek == 0) || (this.dayOfWeek == 6);
  }
}

export class ShiftEmploy {
  id?: number; // employeeId
  employeeCode: string;
  employeeName?: string;
  fullName?: string;
  organizationId?: number;
  organizationName?: string;
  organizationCode?: string;
  unitName?: string;
  status?: string;
  position?: string;
  positionId?: number;
  verified?: boolean;
  workDay: WorkDay[];
  workDayControls: {
    [key: number]: WorkDay;
  };
  isLocked: boolean;
  error?:string;
}

interface HrLockData {
  id: number;
  orgenizationId: number;
  shiftsDate: string;
  isLocked: boolean;
}
export interface LockShiftOrg {
  id: number;
  parentId: number;
  organizationCode: string;
  organizationName: string;
  data: HrLockData[];
  hasChild?: boolean;
  relativeLevel?: number;
}

export interface LockWorkOrg {
  id: number;
  parentId: number;
  organizationCode: string;
  organizationName: string;
  data: HrLockData[];
  hasChild?: boolean;
  relativeLevel?: number;
}

interface OrganizationInfo {
  id: number,
  organizationTypeId?: number,
  code: string,
  name: string,
  orgLevel: number,
  shortName: string,
  orgParentId: number
}
interface EmployeeInfo {
  id: number,
  employeeCode: string,
  fullName: string,
  organizationId: number,
  positionId?: number,
  position?: string,
  organization: OrganizationInfo,
  status?: string
}

export interface HrWorkEmployee {
  id: number,
  employeeId: string,
  workDay: string,
  totalHours: number

}

export interface WorkEmployee {
  employee: EmployeeInfo,
  works: HrWorkEmployee[],
  hoursReal?: number,
  hoursWork?: number,
  hoursShift?: number,
}
