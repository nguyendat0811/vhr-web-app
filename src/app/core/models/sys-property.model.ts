export class SysPropertyBean {
  propertyId: number;
  code: string;
  name: string;
  resourceCode: string;
  startDate: string;
  endDate: string;
  createdBy: string;
  tableName: string;
  columnName: string;
  actionForm: string;
}
