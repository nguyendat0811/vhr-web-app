import { Decimal } from "angular-l10n/src/models/types";
import { Decipher } from "crypto";
import { ShiftEmploy } from "./employee.model";

export interface RepositoryPaging<Type> {
  (arg: Type): Type;
  code: number;
  message: string;
  totalPage: number;
  data: Type;
}
export interface LogShiftEmployee {
  orgShifts: ShiftEmployOrg[];

  logs: LogShiftEmployeeDetail[];
}
export interface LogShiftEmployeeDetail {
  id: number,
  employeeId: string;
  fullName: string;
  createDate: Date;
  shiftsDate: Date;
  fieldChange: string;//"WorkCode";
  beforChange: string; //"trên server nó vậy ";
  afterChange: string;// "C1";
  changeType: number;
  userEdit: number;
}
export interface HrShiftsEmployeeDetail {
  shifts: ShiftEmployOrg[];
  logs: [];
  shiftEmployeeImportErrors: shiftEmployeeImportError[];

  warningReport: warningReport;
}
export interface warningReport {
  employeeWarningReports: employeeWarningReport[];
  organizationWarningReport: organizationWarningReport[];
}
export interface organizationWarningReport {
  organization: Organization;
  totalEmployee: number,
  warningReports: warningReportDetail[],
}
export interface employeeWarningReport {
  employee: ShiftsEmployee;
  warningReports: warningReportDetail[]
}
export interface warningReportOrganization extends warningReportDetail {
  organizationName: string,
  organizationCode: string,
  totalEmployee: number,
}
export interface warningReportEmployee extends warningReportDetail {
  employeeCode: string,
  employeeName: string,
  organizationName: string,
}
export interface warningReportDetail {

  countDayRest: number
  date: Date,
  errorCode: number,
  warningMessage: string,
  headcount: number
  fromDate: Date,
  hours: number,
  toDate: Date,
}
export interface shiftEmployeeImportError {
  dataShifts: []
  employeeIdEdit: string
  errorCode: number
  errorMessage: string
  organizationId: number
}
export interface ShiftEmployOrg {
  employee: ShiftsEmployee;
  position: { id: number, name: string };
  shiftsEmployees: ShiftsEmployeeDetail[];
}


export interface ShiftsEmployeeDetail {
  id: number;
  orgenizationId: number;

  employeeId: string;

  shiftsDate: Date;//"2021-09-20";

  createDate?: Date;//"2021-09-28T00:55:44Z";

  workCode: string;

  totalWorkTime: Decimal;

  createUser: number;

  editDate?: Date;
}
export interface ShiftsEmployee {
  id: number;
  employeeCode: string;
  fullName: string;
  organizationId: number;
  positionId: string;
  organization: Organization;
}
export interface Organization {
  id: number;
  organizationTypeId?: number;
  code: string;
  name: string;
  orgLevel: number;
  shortName: number;
  orgParentId: number;

}
export interface timeSheet {
  employee: ShiftsEmployee;
  position: { id: number, name: string };
  sheets: sheet[],
  errorCode?: number,
  dataTimeSheets?: timeSheetDetail[],

}
export interface timeSheetDetail {
  employeeId: string;
  errorCode?: number,
  errorMessage?: string;
  timesheetDetails: string;
  timeSheets: timeSheet[];
  workDay: string;
}
export interface sheet {
  employeeId: string;
  hrMdWorkType: hrMdWorkType;
  hrMdWorkTypeId: number;
  hrTimesheetsId: number;
  workDay: string;
  errorCode?: number,

}
export interface hrMdWorkType {
  id: number, workCode: string, workName: string, workTypeId: number, totalWorkTime: number;
}
export interface timekeeping {
  timeSheets: timeSheet[]
  timeSheetsUpdateErrors: timeSheet
}

export interface timeSheetModelView {
  employeeCode: string,
  fullName: string,
  workCode: string,
  workCodeBefore: string,
  totalWorkTime: string,
  workTypeId: number
   check: boolean
}
