export class PositionCategoryBean {
    positionId: number;
    careerLevelId: number;
    careerLevelName: String;
    lineOrgName: String;
    lineOrgId: number;
    majorCareerId: number;
    majorCareerName: String;
    code: string;
    name: string;
    shortName: string;
    status: number;
    description: string;
    positionOrder: number;
    effectiveDate: number;
    expiredDate: number;
    refusedDate: number;
    createdDate: number;
    refusedReason: string;
    createdBy: string;
    approvedRefusedBy: string;
    modifiedBy: string;
    modifiedDate: number;
    approvedRefusedDate: number;
}
