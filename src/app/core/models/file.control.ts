import { FormControl } from '@angular/forms';
import { ShiftEmploy } from './employee.model';
export class FileAttachment {
  id: string;
  fileName: string;
  length: number;
  chunkSize: number;
  uploadDate: number;
  isTemp: boolean;
  file: any;
  target: any;
  errorKey: string;
  // huynq73: file depend on language
  sysLanguageCode?: string;
  languageObj?: any;
}
export class FileControl extends FormControl {
  public fileAttachment: Array<FileAttachment>;
  public setFileAttachment(fileAttachment: any) {
    this.fileAttachment = fileAttachment;
  }
  public getFileAttachment(): Array<FileAttachment> {
    return this.fileAttachment;
  }
}

// export class Column {
//   Name: string;
//   Type: string;
//   Title: string;
//   Field: string;
//   Index: Number;
//   Dynamic:boolean;
//   EX: any;
// }
