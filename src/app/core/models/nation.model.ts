// Generated by https://quicktype.io

export class Nation {
    nationId:           number;
    code:               string;
    name:               string;
    isDefault:          boolean;
    description:        string;
    requirePersionalId: boolean;
    phoneAreaCode:      string;
}

export class NationProperty {
  nationId?:            number;
  nationPropertyId?:    number;
  dateFormat?:          string;
  numberFormat?:        string;
  currencyFormat?:      string;
  language?:            string;
}
