import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
  HttpResponse
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { HrStorage } from '../services/HrStorage';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { CommonUtils } from '@app/shared/services';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  urlsToNotUse: Array<string>;
  constructor(public router: Router) {
    this.urlsToNotUse= [
      'get-detail-payslip+',
      'view-detail-payslip+'
    ];
  }
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if(this.isValidRequestForInterceptor(request.url)){
      request = request.clone({
        setHeaders: {
          'Authorization': `Bearer ${this.getAccessToken()}`,
          // 'viettel-api-key': `c9f55dab-d44a-43c0-a922-d7133efdb28d`,
          'Current-Language':  this.getCurrentLanguageCode(),
          'Current-Market':  this.getCurrentMarket(),
          'sso-two-factor-ticket': this.getTwoFactorTicket(),
        }
      });
      return next.handle(request).pipe(
        tap(event => {
          if (event instanceof HttpResponse) {

          }
        }, error => {
          /*if (error.status === 401) {
            // Do later: set currentUrl when redirect from SSO
            // HrStorage.setCurrentUrl(this.router.url);
            // this.router.navigate(['/auth-sso']);
            CommonUtils.logoutAction();
          }*/
        })
      );
    }
    return next.handle(request);
  }

  private isValidRequestForInterceptor(requestUrl: string): boolean {
    let positionIndicator: string = 'v1/payslip/';
    let position = requestUrl.indexOf(positionIndicator);
    if (position > 0) {
      let destination: string = requestUrl.substr(position + positionIndicator.length);
      for (let address of this.urlsToNotUse) {
        if (new RegExp(address).test(destination)) {
          return false;
        }
      }
    }
    return true;
  }

  getCurrentLanguageCode(): string {
    const selectedLang = HrStorage.getSelectedLang();
    if (selectedLang == null) {
      return 'en';
    } else {
      return selectedLang.code;
    }
  }
  getCurrentMarket(): string {
    const selectedMarket = HrStorage.getSelectedMarket();
    if (selectedMarket == null) {
      return '';
    } else {
      return selectedMarket.marketCompanyId;
    }
  }
  getAccessToken() {
    const userToken = HrStorage.getUserToken();
    if (userToken == null) {
      return '';
    } else {
      return userToken.access_token;
    }
  }
  getTwoFactorTicket() {
    if (window.TwoFactor && window.TwoFactor.getTicket()) {
      return window.TwoFactor.getTicket();
    }
    return '';
  }
}
