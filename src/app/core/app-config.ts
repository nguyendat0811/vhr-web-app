export const CONFIG: any = {
  API_PATH: {
  /********************OAUTH SERVICE*****************/
      'oauthToken'        : '/',
  /********************File SYSTEM****************/
      'fileStorage'       : '/file',
  /********************Sys SYSTEM*****************/
      'sysCat'            : '/v1/sys-cats',
      'sysCatType'        : '/v1/sys-cat-types',
      'nation'            : '/v1/nations',
      'nationConfig'      : '/v1/nation-configs',
      'language'          : '/v1/languages',
      'sysProperty'       : '/v1/sys-propertys',
      'sysPropertyDetails': '/v1/sys-property-details',
      'workFollow'        : '/v1/work-follow',
      'wfMenuMapping'     : '/v1/wf-menu-mappings',
      'workFlows'         : '/v1/work-flows',
      'location'          : '/v1/locations',
      'nationProperty'    : '/v1/nation-propertys',
      'infoChange'        : '/v1/info-change',
      'sysLanguage'       : '/v1/sys-language',
      'empSystemConfig'   : '/v1/emp-system-config',
      'systemParameter'   : '/v1/system-parameters',
      'ethnic'            : '/v1/ethnics',
      'religion'          : '/v1/religions',
      'nationConfigType'  : '/v1/nation-config-types',
  /********************EMPLOYEE SYSTEM*****************/
      'homeEmployee'      : '/v1/homes',
      'dataPicker'        : '/v1/data-pickers',
      'empType'           : '/v1/emp-types',
      'positionCareer'    : '/v1/position-careers',
      'labourContractType': '/v1/labour-contract-types',
      'allowanceType'     : '/v1/allowance-types',
      'allowance'         : '/v1/allowance',
      'documentType'      : '/v1/document-types',
      'empFile'     		  : '/v1/emp-files',
      'positionSalary' : '/v1/position-salary',
      'empPositionSalary' : '/v1/emp-position-salary-process',

      'familyRelationship': '/v1/employee/family-relationships',
      'empBankAccount'   : '/v1/employee/emp-bank-account',

      'salaryProcess'     : '/v1/employee/salary-process',
      'employeeInfo'      : '/v1/employee/employee-infos',
      'workProcess'       : '/v1/employee/work-process',

      'otherInformation'  : '/v1/employee/employee-infos',
      'partyUnion'        : '/v1/party-unions',
      'militaryInformation': '/v1/employee/employee-infos',
      'languageDegree'    : '/v1/employee/language-degrees',
      'empTradeUnion'     : '/v1/employee/emp-trade-unions',
      'documentReason'    : '/v1/document-reasons',
      'socialInsuranceProcess': '/v1/emp-social-insurance',
      'empHealthInsurance'    : '/v1/emp-health-insurances',
      'empIncomeTax'      : '/v1/emp-income-taxs',
      'empIncomeTaxReduction'      : '/v1/emp-income-tax-reduction',
      'longLeaveProcess'  : '/v1/long-leaves',
      'occupationalSafety'    : '/v1/occupational-safetys',
      'trainingPlan'    : '/v1/training-plans',
      'empRewardProcess'  : '/v1/employee/emp-reward-process',
      'empDiscipline'     : '/v1/employee/emp-disciplines',
      'empTypeProcess'    : '/v1/employee/emp-type-process',
      'empLanguaeDegree'  : '/v1/emp-language-degrees',
      'empPoliticalDegree': '/v1/employee/emp-political-degree',
      'empEducationProcess': '/v1/employee/emp-education-process',
      'empPassport'       : '/v1/employee/emp-passport',
      'empVisa'           : '/v1/employee/emp-visa',
      'empAllowanceProcess': '/v1/emp-allowance-process',

      'salaryTable'       : '/v1/salary-tables',
      'salaryStep'        : '/v1/salary-steps',
      'salaryGrade'       : '/v1/salary-grades',

      'labourContractDetail'       : '/v1/label-contract-details',
      'empHistory'        : '/v1/emp-history',
      'orgActionLock'     : '/v1/org-action-locks',
      'empCodeConfig'     : '/v1/emp-code-config',
      /******************ORGANIZATION SYSTEM***************/
      'organization'      : '/v1/organizations',
      'orgPlan'          : '/v1/org-plans',
      'orgDraff'          : '/v1/org-draffs',
      'orgDuty'           : '/v1/org-dutys',
      'orgSelector'       : '/v1/org-selectors',
      'orgDraffSelector'  : '/v1/org-draff-selectors',
      'orgBoundary'       : '/v1/org-boundarys',
      'orgBoundaryDraff'  : '/v1/org-boundarys-draffs',
      'lineOrg'           : '/v1/line-orgs',
      'majorCareer'       : '/v1/major-careers',
      'position'          : '/v1/positions',
      'positionDes'       : '/v1/position-descriptions',
      'orgType'           : '/v1/organization-types',
      'orgHistory'        : '/v1/org-historys',
      'requirementDetail' : '/v1/requirement-details',
      /******************REPORT SYSTEM***************/
      'homeWarning'       : '/v1/home-warning',
      'templateDynamic'   : '/v1/template-dynamics',
      'reportDynamic'     : '/v1/report-dynamics',
      'empTypeProcessReport'   : '/v1/emp-type-process-reports',
      'employeeReport': '/v1/employee-reports',
      'employeeList': '/v1/report-employee-list',
      'reportCustom': '/v1/reports/report-custom',
      /******************TIMEKEEPING SYSTEM***************/
      'workSchedule'      : '/v1/work-schedule',
      'workdayType'       : '/workType',
      'longLeaveReason'   : '/v1/long-leave-reasons',
      'timekeepingLock'   : '/v1/timekeeping-locks',
      'empTimekeeping'    : '/v1/emp-timekeepings',
      'empTimekeepingReport': '/v1/emp-timekeeping-reports',
      'holiday'           : '/v1/holidays',
      'timekeepingLockConfig': '/v1/timekeeping-lock-configs',
      'annuaLeave'        : '/v1/annual-leaves',
      'empWorksChedule'   : '/v1/emp-work-schedules',
      'schedular'         : '/timekeeping',

      /******************TIMESHEET SYSTEM***************/
      'openCloseMarket'      : '/openCloseMarket',

      /******************PAYROLL***************/
      'salaryType'          : '/v1/setting/salary-types',
      'dataType'            : '/v1/data-input/data-types',
      'salaryTypeMapping'   : '/v1/data-input/salary-type-mappings',
      'period'              : '/v1/setting/periods',
      'payrollPrice'        : '/v1/setting/payroll-prices',
      'payrollSpecialPrice' : '/v1/setting/special-prices',
      'saleOrderSourcePrice': '/v1/setting/sale-order-source-prices',
      'payrollServicePrice' : '/v1/setting/service-prices',
      'payrollOrgMapping'   : '/v1/setting/org-mappings',
      'xnkNotification'     : '/v1/setting/notifications',
      'payrollGroup'        : '/v1/setting/payroll-groups',
      'saleOrderSource'     : '/v1/setting/sale-order-sources',
      'formulaDiagram'      : '/v1/formula/diagrams',
      'salaryOrganizationMapping'      : '/v1/data-input/salary-organization-mappings',
      'salaryTypeOrganizationMapping'      : '/v1/data-input/salary-type-organization-mappings',
      'dataInputProcess'    : '/v1/data-input/data-input-process',
      'payrollTableConfig'  : '/v1/payroll/payroll-table-config',
      'xnkPayrollConfig'  : '/v1/payroll/xnk-payroll-config',
      'formulaConfig'      : '/v1/formula/configs',
      'payrollItem'          : '/v1/setting/payroll-items',
      'payrollCalculate'    : '/v1/payroll/payroll-calculate',
      'payrollReCalculate'  : '/v1/payroll/payroll-recalculate',
      'payslipConfig'       : '/v1/payslip/configs',
      'payslip'             : '/v1/payslip',
      'payrollReportType'   : '/v1/payroll-report-type',
      'payrollMaster'    : '/v1/payroll/payroll-masters',
       /******************EVALUATION***************/
      'organization-ki' : '/v1/evaluation/organization-ki-process',
      'employee-ki'     : '/v1/evaluation/employee-ki-process',
      'lockUnlockKi'    : '/v1/evaluation/lock-unlock-ki',
    }
};
export const VPS_MENU: any = {
'00'  : 'glyphicons glyphicons-home',
'QLNV': 'glyphicons glyphicons-group',
'DGNV': 'glyphicons glyphicons-message-forward',
'QLMH': 'glyphicons glyphicons-tree-structure',
'QLCD': 'glyphicons glyphicons-group-chat',
'QLCC': 'glyphicons glyphicons-one-day',
'BC'  : 'glyphicons glyphicons-list-alt',
'settings': 'glyphicons glyphicons-settings',
'DM'  : 'glyphicons glyphicons-book',
'PAYROLL' : 'glyphicons glyphicons-fees-payments'
};
export const DEFAULT_MODAL_OPTIONS: any = {
  size: 'lg',
  backdrop: 'static'
};
export const SMALL_MODAL_OPTIONS: any = {
size: 'md',
keyboard: true,
};
export const MEDIUM_MODAL_OPTIONS: any = {
size: 'md',
keyboard: true,
backdrop: 'static'
};
export const LARGE_MODAL_OPTIONS: any = {
size: 'lg',
backdrop: 'static',
windowClass: 'modal-xxl',
keyboard: false
};
export const PERMISSION_CODE: any = {
// action tac dong
  'action.view': 'VIEW'
, 'action.insert': 'INSERT'
, 'action.update': 'UPDATE'
, 'action.delete': 'DELETE'
, 'action.import': 'IMPORT'
, 'action.export': 'EXPORT'
, 'action.approval': 'APPROVAL'
, 'action.decide': 'DECIDE'
, 'action.unDecide': 'UN_DECIDE'
, 'action.unApproveAll': 'UN_APPROVE_ALL'
, 'action.approveAll': 'APPROVE_ALL'
, 'action.removeEmp': 'REMOVE_EMP'
, 'action.addEmp': 'ADD_EMP'
, 'action.unApprove': 'UN_APPROVE'
, 'action.unLock': 'UN_LOCK'
, 'action.lock': 'LOCK'
, 'action.calculate': 'CALCULATE'
, 'action.viewHistory': 'VIEW_HISTORY'
, 'action.enable': 'ENABLE'
, 'action.disable': 'DISABLE'
, 'action.issueAgain': 'ISSUE_AGAIN'
, 'action.issueChange': 'ISSUE_CHANGE'
, 'action.quickImport': 'QUICK_IMPORT'
, 'action.transfer': 'TRANSFER'
, 'action.manage': 'MANAGE'
, 'action.viewAll': 'VIEW_ALL'
, 'action.payrollCalculate': 'PAYROLL_CALCULATE'
, 'action.payrollDestroy': 'DESTROY_PAYROLL'
, 'action.syncTax': 'SYNC_TAX'
, 'action.syncPayroll': 'SYNC_PAYROLL'

//GiangTH
, 'action.addAction': 'ADD_ACTION'
, 'action.grantRole': 'GRANT_ROLE'
, 'action.grantMenu': 'GRANT_MENU'
, 'action.grantPermission': 'GRANT_PERMISSION'
// resource tai nguyen he thong
, 'resource.empType': 'EMP_TYPE'
, 'resource.sysProperty': 'SYS_PROPERTY'
, 'resource.allowanceType': 'ALLOWANCE_TYPE'
, 'resource.systemParameter': 'SYSTEM_PARAMETER'
, 'resource.allowance': 'ALLOWANCE'
, 'resource.documentType': 'DOCUMENT_TYPE'
, 'resource.documentReason': 'DOCUMENT_REASON'
, 'resource.visa': 'VISA_INFORMATION'
, 'resource.passport': 'PASSPORT_INFORMATION'
, 'resource.labourContractType': 'LABOUR_CONTRACT_TYPE'
, 'resource.positionCareer': 'POSITION_CAREER'

, 'resource.organization': 'ORGANIZATION'
, 'resource.orgCommonInfo': 'ORG_COMMON_INFO'
, 'resource.orgAdditionalInfo': 'ORG_ADDITIONAL_INFO'
, 'resource.orgBoundary': 'ORG_BOUNDARY'
, 'resource.orgPlan': 'ORGANIZATION_PLAN'
, 'resource.orgDraffCommonInfo': 'ORG_DRAFF_COMMON_INFO'
, 'resource.orgDraffAdditionalInfo': 'ORG_DRAFF_ADDITIONAL_INFO'
, 'resource.orgDraffBoundary': 'ORG_DRAFF_BOUNDARY'

, 'resource.nation': 'NATION'
, 'resource.employee': 'EMPLOYEE'
, 'resource.home': 'HOME'
, 'resource.empFile' : 'EMP_FILE'
, 'resource.militaryInformation' : 'MILITARY_INFORMATION'
, 'resource.languageInformation' : 'LANGUAGE_INFORMATION'
, 'resource.empHealthInsurance' : 'EMP_HEALTH_INSURANCE'
, 'resource.longLeaveProcess' : 'LONG_LEAVE_PROCESS'
, 'resource.empWorkSchedule' : 'EMP_WORK_SCHEDULE'
, 'resource.templateDynamic' : 'TEMPLATE_DYNAMIC'
, 'resource.reportDynamic' : 'REPORT_DYNAMIC'

// Tanptn
, 'resource.familyRelationship': 'FAMILY_RELATIONSHIP'
, 'resource.financial': 'FINANCIAL'
// HiepNC
, 'resource.empSalaryProcess': 'EMP_SALARY_PROCESS'
, 'resource.otherInformation': 'OTHER_INFORMATION'
, 'resource.partyUnion': 'PARTY_UNION'
, 'resource.socialInsuranceProcess': 'SOCIAL_INSURANCE_PROCESS'
, 'resource.workProcess': 'WORK_PROCESS'
, 'resource.positionSalary' : 'POSITION_SALARY'
, 'resource.empPositionSalary' : 'EMP_POSITION_SALARY'
, 'resource.empAllowanceProcess' : 'EMP_ALLOWANCE_PROCESS'
, 'resource.empIncomeTax' : 'EMP_INCOME_TAX'
, 'resource.empIncomeTaxReduction' : 'EMP_TAX_REDUCTION'
, 'resource.longLeaveReason' : 'LONG_LEAVE_REASON'
, 'resource.occupationalSafety' : 'OCCUPATIONAL_SAFETY'
, 'resource.trainingPlan' : 'TRAINING_PLAN'

// thaopv
, 'resource.empRewardDiscipline': 'REWARD_DISCIPLINE'
, 'resource.positionCategory': 'POSITION_CATEGORY'
, 'resource.empTypeProcess' : 'EMP_TYPE_PROCESS'
, 'resource.empEducationProcess' : 'EMP_EDUCATION_PROCESS'
, 'resource.sysCat' : 'SYS_CAT'
, 'resource.sysCatType' : 'SYS_CAT_TYPE'
, 'resource.empTradeUnion' : 'TRADE_UNION'
, 'resource.attendanceTypeDefinition' : 'ATTENDANCE_TYPE_DEFINITION'
, 'resource.empHistory' : 'EMP_HISTORY'
, 'resource.salaryTable' : 'SALARY_TABLE'
, 'resource.empTimekeeping' : 'EMP_TIMEKEEPING'
, 'resource.detailReport' : 'DETAIL_REPORT'
, 'resource.timekeepingLock' : 'TIMEKEEPING_LOCK'
, 'resource.timekeepingLockConfig' : 'TIMEKEEPING_LOCK_CONFIG'
// KienPT
, 'resource.workSchedule' : 'WORK_SCHEDULE'
, 'resource.annualLeave' : 'ANNUAL_LEAVE'
// QuocTV
, 'resource.orgActionLock' : 'ORG_ACTION_LOCK'
, 'resource.nationConfig' : 'NATION_CONFIG'
, 'resource.nationConfigType' : 'NATION_CONFIG_TYPE'
, 'resource.jobRequirement' : 'JOB_REQUIREMENT'
// Payroll
, 'resource.salaryTypeMapping' : 'SALARY_TYPE_MAPPING'
, 'resource.salaryType' : 'SALARY_TYPE'
, 'resource.formulaDiagram': 'FORMULA_DIAGRAM'
, 'resource.period' : 'PERIOD'
, 'resource.salaryOrganizationMapping' : 'SALARY_ORGANIZATION_MAPPING'
, 'resource.dataType' : 'DATA_TYPE'
, 'resource.payrollTableConfig' : 'PAYROLL_TABLE_CONFIG'
, 'resource.dataInputProcess' : 'DATA_INPUT_PROCESS'
, 'resource.formulaConfig' : 'FORMULA_CONFIG'
, 'resource.payrollGeneral' : 'PAYROLL_GENERAL'
, 'resource.payrollItem' : 'PAYROLL_ITEM'
, 'resource.salaryTypeOrganizationMapping' : 'SALARY_TYPE_ORGANIZATION_MAPPING'

, 'resource.payslipConfig' : 'PAYSLIP_CONFIG'
, 'resource.payrollMaster' : 'PAYROLL_MASTER'
, 'resource.payrollReportType' : 'PAYROLL_REPORT_TYPE'
// Evaluation
, 'resource.lockUnlockKi' : 'LOCK_UNLOCK_KI'
, 'resource.employeeKiProcess':'EMPLOYEE_KI_PROCESS'
, 'resource.organizationKiProcess':'ORGANIZATION_KI_PROCESS'

// Permission
, 'resource.permissionAction': 'PERMISSION_ACTION'
, 'resource.permissionRole': 'PERMISSION_ROLE'
, 'resource.permissionMenu': 'PERMISSION_MENU'
, 'resource.permissionResource': 'PERMISSION_RESOURCE'
, 'resource.permissionUser': 'PERMISSION_USER'
};

export const APP_CONSTANTS = {

  STATICAL_TYPE: [
    { label: 'app.reportCustom.gender', id: 'gender' }, // giới tính
    { label: 'app.reportCustom.empType', id: 'empType' }, // diện đối tượng
    { label: 'app.reportCustom.educationGrade', id: 'educationGrade' }, // trình độ chuyên môn
    { label: 'app.reportCustom.age', id: 'age' }, // nhóm tuổi
    { label: 'app.reportCustom.party', id: 'party' }, // là đảng viên
    { label: 'app.reportCustom.religion', id: 'religionId' },  // tôn giáo
    { label: 'app.reportCustom.politicalLever', id: 'politicalLever' }, // trình độ lý luận chính trị 1
    { label: 'app.reportCustom.languagerLever', id: 'languagerLever' }, // trình độ ngoại ngữ 1
    { label: 'app.reportCustom.ethnic', id: 'ethnic' }, // Dân tộc
    { label: 'app.reportCustom.ethnicMinorities', id: 'ethnicMinorities' }, // Là dân tộc thiểu số
  ],

MANAGEMENT_TYPE: [
  { label: 'positionCareer.managementType.1', id: 1 },
  { label: 'positionCareer.managementType.2', id: 2 },
],
ORG_EXPIRED_TYPE: [
  { label: 'app.organization.expiredType.changeName', id: 1 },
  { label: 'app.organization.expiredType.merger', id: 2 },
  { label: 'app.organization.expiredType.separate', id: 3 },
],
ORG_RELATION_TYPE: [
  { label: 'organizationPlan.type.addNew', id: 1 },
  { label: 'organizationPlan.type.changName', id: 2 },
  { label: 'organizationPlan.type.merger', id: 3 },
  { label: 'organizationPlan.type.termination', id: 4 },
  { label: 'organizationPlan.type.re-establish', id: 5 }
],
GENDERS: [
  { label: 'app.gender.male', id: 1 },
  { label: 'app.gender.female', id: 0 },
],
PLAN_TYPE: [
  { label: 'organizationPlan.type.addNew', id: 0 },
  { label: 'organizationPlan.type.changName', id: 1 },
  // { label: 'organizationPlan.type.merger', id: 2 },
  // { label: 'organizationPlan.type.restructure', id: 3 },
],
PLAN_STATUS: [
  { label: 'organizationPlan.status.init', id: 0 },
  { label: 'organizationPlan.status.waitingForApprove', id: 1 },
  { label: 'organizationPlan.status.approved', id: 2 },
  { label: 'organizationPlan.status.refused', id: 3 }
],
STATUS: [
  { label: 'app.organization.status.active', id: 1 },
  { label: 'app.organization.status.inactive', id: 2 }
],
WORKING_STATUS: [
  { label: 'app.employee-info.isWorking', id: 1 },
  { label: 'app.employee-info.notWorking', id: 2 }
],
EDU_LANGUAGE_TYPE: [
  { label: 'app.educationProcess.language.degree', id: 1 },
  { label: 'app.educationProcess.language.Diploma', id: 2 }
],
EMP_FILE_STATUS: [
  { label: 'app.empFile.pendingForApproval', id: 1 },
  { label: 'app.empFile.approved', id: 2 },
  { label: 'app.empFile.rejected', id: 3 },
  { label: 'app.empFile.acceptHardCopy', id: 4 },
  { label: 'app.empFile.hasBeenStored', id: 5 },
  { label: 'app.empFile.individualHandover', id: 6 }
],
EMP_FILE_PROFILETYPE: [
  { label: 'app.empFile.socialInsuranceBook', id: 1 },
  { label: 'app.empFile.cv', id: 2 },
  { label: 'app.empFile.profile', id: 3 },
  { label: 'app.empFile.personProfileDeclaration', id: 4 },
  { label: 'app.empFile.graduationDiplomaxNotarizes', id: 5 },
  { label: 'app.empFile.birthCertificate', id: 6 },
  { label: 'app.empFile.certificateOfRegistration', id: 7 },
  { label: 'app.empFile.healthCertificate', id: 8 },
  { label: 'app.empFile.identityCare', id: 9 },
  { label: 'app.empFile.3Image', id: 10 },
  { label: 'app.empFile.decisions', id: 11 },
  { label: 'app.empFile.contract', id: 12 },
  { label: 'app.empFile.profileOfTheDefenseStaff', id: 13 },
  { label: 'app.empFile.soldierProfile', id: 14 },
  { label: 'app.empFile.decision', id: 15 },
  { label: 'app.empFile.typeOfContract', id: 16 },
],
EMP_FILE_FIXEDEMPFILE: [
  { label: 'app.empFile.saved', id: 1 },
  { label: 'app.empFile.notSaved', id: 2 },
],
SYS_CAT_TYPE_COCE: {
  KI_RANK: 'KI',
  RECRUITMENT_TYPE: 'TTHN',
  ETHNIC_CODE: 'DDT',
  RELIGION_CODE: '02',
  BANK_CODE: '07',
  LANGUAGE: 'DMNN',
  FAMILY_RELATION: 'QHGD',
  RELATYPE: 'TTTN',
  POLICY_TYPE: 'LCS',
  EDUCATION_LEVEL: 'DMTDDT',
  EDUCATION_TYPE: 'DMLHDT',
  EDUCATION_SPECIALITY: '05',
  EDUCATION_UNIVER_CITY: 'DMTDH',
  EDUCATION_RANK: 'DMXLDT',
  RECUITMENT_CATEGORY: 'DTD',
  SOLDIER_LEVEL: 'DMCBQD',
  CURRENCY: '01',
  FAMILY_TYPE: 'TPGD',
  TRADE_UNION: 'DMCD',
  SOCIAL_INSURANCE_FUND: 'DSTC',
  WORKING_TIME: 'TGLV',
  REWARD_FORM: 'HTKT',
  REWARD_HONOR: 'DHKT',
  REWARD_TYPE: 'LKT',
  DISCIPLINE_FORM: 'HTKLCQ',
  PARTY_DISCIPLINE_FORM: 'HTKLD',
  DISCIPLINE_TYPE: 'LP',
  DISCIPLINE_LEVEL: 'CQD',
  WORK_FLOW: 'DML',
  POSITION_TYPY_LIST: 'DSLCD',
  EMP_FILE_LIST: 'DSLHS',
  ALLOWANCE: 'PC',
  LEVEL: 'DMTD',
  PASSPORT: 'DSLHC',
  VISA: 'DSLTT',
  TABLE: 'TABLE',
  ATTENDANCE_TYPE: 'AT',
  FORM_TYPE: 'FORM_TYPE',
  CHTD: 'CHTD',
  WORK_PLACE: 'WORK_PLACE',
  MANAGE_ORG: 'DSCQT',
  CALCULATE_TYPE: 'CTT',
  MANAGEMENT_TYPE: 'LCNNV',
  TEMPLATE_DYNAMIC_TYPE: 'LBM',
  TRAINING_TYPE: 'TT',
  DEGREE_DIPLOMA_TYPE: 'DT',
  REQUIREMENT: 'DMYCK',
  EXPERIENCE: 'DMKN',
  ANNUAL_LEAVE_CODE: 'LIST_WORK_DAY_TYPE_ANNUAL_LEAVE',
  FORMULA_DIAGRAM_STATUS: 'FORMULA_DIAGRAM_STATUS',
  DATA_TYPE: 'DATA_TYPE',
  PAYROLL_REPORT_TYPE:'PAYROLL_REPORT_TYPE',
},
NATION_CONFIG_TYPE: {
    ETHNIC: 'ETHNIC'
  , RELIGION: 'RELIGION'
  , BANK_BRANCH: 'BANK_BRANCH'
  , BANK: 'BANK'
  , EDU_ISSUED_PLACE: 'EDU_ISSUED_PLACE'
  , TAX_UNIT: 'TAX_UNIT'
},
EMP_TYPE_PROCESS: {
  IS_LABOUR_CONTRACR: 'HDLD'
},
ALLOWANCE: {
  THEOSOTIEN: 'TST',
  THEOSOTIEN_LH: 'TSTLH',
  LUONGBAOHIEM: 'TLBH',
  LUONGCHUCDANH: 'TLCD'
},
WORK_PROCESS: {
  WORK_PROCESS_TYPE: [
    { label: 'app.workProcess.interiot', type: 1 },
    { label: 'app.workProcess.exterior', type: 2 },
    { label: 'app.workProcess.extraWork', type: 3 },
  ],
  HAS_DRAFT: [{ label: 'app.workProcess.interiot', type: 1 }],
  NO_HAS_DRAFT: [
    { label: 'app.workProcess.interiot', type: 1 },
    { label: 'app.workProcess.exterior', type: 2 },
  ],
  TYPE_INNER: 1,
  TYPE_OUTER: 2,
},
POLITICAL_LEVEL: [
  {id: 1, name: 'Sơ cấp'},
  {id: 2, name: 'Trung cấp'},
  {id: 3, name: 'Cao cấp'}
],
LABOUR_CONTRACT_TYPE: {
  OFFICIALS: 'Viên chức',
  LABOUR_CONTRACT: 'HĐLĐ',
},
LABOUR_CONTRACT: [
  {id: 1, name: 'Nhóm 1_Khoán việc'},
  {id: 2, name: 'Nhóm 2_Có thời hạn'},
  {id: 3, name: 'Nhóm 3_Không xác định thời hạn'}
],
ATTENDANCE_TYPE: [
  {id: 1, name: 'Working Day Type'},
  {id: 2, name: 'Insurance Day Type'},
  {id: 3, name: 'Leave Day Type'},
  {id: 4, name: 'Overtime Type'},
  {id: 5, name: 'Shift Day Type'},
],
DAY_OF_WEEK: [
  {id: 1, name: 'app.workSchedule.dayOfWeek.sun'},
  {id: 2, name: 'app.workSchedule.dayOfWeek.mon'},
  {id: 3, name: 'app.workSchedule.dayOfWeek.tue'},
  {id: 4, name: 'app.workSchedule.dayOfWeek.wed'},
  {id: 5, name: 'app.workSchedule.dayOfWeek.thu'},
  {id: 6, name: 'app.workSchedule.dayOfWeek.fri'},
  {id: 7, name: 'app.workSchedule.dayOfWeek.sat'},
],
ATTENDANCE_CONFIGURATION: [
  {id: 1, name: 'app.timesheet.workType.shift'},
  {id: 2, name: 'app.timesheet.workType.regime'},
  {id: 3, name: 'app.timesheet.workType.overTime'},
],

INSURANCE_TYPE_CONFIGURATION: [
  {id: 1, name: 'app.timesheet.insurance.phsk1'},
  {id: 2, name: 'app.timesheet.insurance.phsk2'},
  {id: 3, name: 'app.timesheet.insurance.phsk3'},
  {id: 4, name: 'app.timesheet.insurance.tnld'},
  {id: 5, name: 'app.timesheet.insurance.tsn'},
  {id: 6, name: 'app.timesheet.insurance.otn'},
  {id: 7, name: 'app.timesheet.insurance.odu'}
],
ATTENDANCE_TYPE_CONFIGURATION: [
  {id: 0, name: 'app.timesheet.workType.regime'},
  {id: 1, name: 'app.timesheet.workType.shift'},
  {id: 2, name: 'app.timesheet.workType.overTime'},
],
ATTENDANCE_FORM: [
  {id: 1, name: 'app.workSchedule.attendanceFrom.byFingerprint'},
  {id: 2, name: 'app.workSchedule.attendanceFrom.byFace'},
  {id: 3, name: 'app.workSchedule.attendanceFrom.byCard'},
],
LOCK_TYPE: [
  {id: 1, name: 'app.payrolls.timekeepingLockConfig.lockByPeriod'},
  {id: 2, name: 'app.payrolls.timekeepingLockConfig.lockByDay'}
],
OFFICIALS: [
  {id: 1, name: 'Viên chức'},
  {id: 2, name: 'Viên chức tập sự'}
],
LIST_GRADE_FORM: [
  {id: 1, label: 'app.salaryTable.gradeLadder.formPosition'},
  {id: 2, label: 'app.salaryTable.gradeLadder.formInsurance'}
],
LIST_GRADE_TYPE: [
  {id: 1, label: 'app.salaryTable.gradeLadder.typeStep'},
  {id: 2, label: 'app.salaryTable.gradeLadder.typeGrade'}
],
LIST_GRADE_RATE_TYPE: [
  {id: 1, label: 'app.salaryTable.gradeRate.typeSalary'},
  {id: 2, label: 'app.salaryTable.gradeRate.typeBonus'},
  {id: 3, label: 'app.salaryTable.gradeRate.typeOT'},
],
NUMBER_VALIDATE: [
  { label: 'app.sys_property.isNumber', code: 'isNumber' },
  { label: 'app.sys_property.positiveNumber', code: 'positiveNumber' },
  { label: 'app.sys_property.integer', code: 'integer' },
  { label: 'app.sys_property.positiveInteger', code: 'positiveInteger' },
],
ORG_ACTION_LOCK_TYPE: [
  {id: 1, name: 'app.orgActionLock.lockType.workProcess'},
  {id: 2, name: 'app.orgActionLock.lockType.contractProcess'},
  {id: 3, name: 'app.orgActionLock.lockType.longLeaveProcess'},
  {id: 4, name: 'app.orgActionLock.lockType.positionSalaryProcess'},
  {id: 5, name: 'app.orgActionLock.lockType.socialInsuranceProcess'},
  {id: 6, name: 'app.orgActionLock.lockType.allowanceProcess'}
],
SALARY_TYPE: [
  {id: 1, label: 'salarytype.dataInputType.salarymoth'},
  {id: 2, label: 'salarytype.dataInputType.salaryquatery'},
  {id: 3, label: 'salarytype.dataInputType.salaryyear'},
  {id: 4, label: 'salarytype.dataInputType.salaryreward'}
],
FORMULA_DIAGRAM_STATUS: [
  {id: 0, label: 'Không hoạt động'},
  {id: 1, label: 'Hoạt động'}
],
FORMULA_CONFIG_STATUS: [
  {id: 0, label: 'Không hoạt động'},
  {id: 1, label: 'Hoạt động'}
],
ATTRIBUTE_TYPE: [
  {id: 1, label: 'Date(DD/MM/YYYY)'},
  {id: 2, label: 'Date(MM/DD/YYYY)'},
  {id: 3, label: 'Date(YYYY/MM/DD)'},
  {id: 4, label: 'Double'},
  {id: 5, label: 'Long'},
  {id: 6, label: 'String'}
],
PAYROLL_ITEM: [
  {id: 1, label: 'payroll.item.status.yes'},
  {id: 0, label: 'payroll.item.status.no'}
],
PAYSLIP_CONFIG_STATUS: [
  {id: 0, label: 'payroll.formula.diagram.inactive'},
  {id: 1, label: 'payroll.formula.diagram.active'}
],
PAYROLL_MASTER_STATUS: [
  {id: 1, label: 'payroll.progress.status.calculate'},
  {id: 2, label: 'payroll.progress.status.payment'}
],
EVALUATION_PERIOD: [
  {id: 1, label: 'evaluation.employeeKi.month'},
  {id: 2, label: 'evaluation.employeeKi.quarter'},
  {id: 3, label: 'evaluation.employeeKi.year'}
],
QUARTER_OF_YEAR: [
  {id: 1, name: 'I'},
  {id: 2, name: 'II'},
  {id: 3, name: 'III'},
  {id: 4, name: 'IV'}
],
MONTHS_OF_YEAR: [
  {id: 1, name: '1'},
  {id: 2, name: '2'},
  {id: 3, name: '3'},
  {id: 4, name: '4'},
  {id: 5, name: '5'},
  {id: 6, name: '6'},
  {id: 7, name: '7'},
  {id: 8, name: '8'},
  {id: 9, name: '9'},
  {id: 10, name: '10'},
  {id: 11, name: '11'},
  {id: 12, name: '12'}
],
//quoctv List dien doi tuong cho Payroll Report Type
EMP_TYPE: [
  {id: 1, label: 'payroll.payrollReportType.inCorp'},
  {id: 2, label: 'payroll.payrollReportType.outCorp'},
],
  PARTY_MEMBER: [
    { id: 1, label: 'employeeList.isPartyMember' },
    { id: 2, label: 'employeeList.notPartyMember' },
  ],

  COMMON_INFO: [
    { name: 'ngayThangNamSinh', value: 'employeeList.commonInfo.ngayThangNamSinh' },
    { name: 'gioiTinh', value: 'employeeList.commonInfo.gioiTinh' },
    { name: 'tinhTrangHonNhan', value: 'employeeList.maritalStatus' },
    { name: 'danToc', value: 'employeeList.commonInfo.danToc' },
    { name: 'tonGiao', value: 'employeeList.commonInfo.tonGiao' },
    { name: 'soCMT', value: 'employeeList.commonInfo.soCMT' },
    { name: 'ngayCapCMT', value: 'employeeList.commonInfo.ngayCapCMT' },
    { name: 'noiCapCMT', value: 'employeeList.commonInfo.noiCapCMT' },
    { name: 'noiSinh', value: 'employeeList.commonInfo.noiSinh' },
    { name: 'nguyenQuan', value: 'employeeList.commonInfo.nguyenQuan' },
    { name: 'hoKhauThuongChu', value: 'employeeList.commonInfo.hoKhauThuongChu' },
    { name: 'noiOHienTai', value: 'employeeList.commonInfo.noiOHienTai' }
  ],

  PARTY_INFO: [
    { name: 'soTheDoan', value: 'employeeList.partyInfo.soTheDoan' },
    { name: 'ngayVaoDoan', value: 'employeeList.partyInfo.ngayVaoDoan' },
    { name: 'noiKetNapDoan', value: 'employeeList.partyInfo.noiKetNapDoan' },
    { name: 'soTheDang', value: 'employeeList.partyInfo.soTheDang' },
    { name: 'ngayKetNapDang', value: 'employeeList.partyInfo.ngayKetNapDang' },
    { name: 'ngayVaoDangChinhThuc', value: 'employeeList.partyInfo.ngayVaoDangChinhThuc' }
  ],

  FINANCIAL_INFO: [
    { name: 'taiKhoan', value: 'employeeList.financialInfo.taiKhoan' },
    { name: 'nganHang', value: 'employeeList.financialInfo.nganHang' },
    { name: 'chiNhanhNganHang', value: 'employeeList.financialInfo.chiNhanhNganHang' }
  ],

  WORK_PROCESS_INFO: [
    { name: 'doiTuong', value: 'employeeList.workProcessInfo.doiTuong' },
    { name: 'donVi', value: 'employeeList.workProcessInfo.donVi' },
    { name: 'phongBan', value: 'employeeList.workProcessInfo.phongBan' },
    { name: 'toNhom', value: 'employeeList.workProcessInfo.toNhom' },
    { name: 'chucDanh', value: 'employeeList.workProcessInfo.chucDanh' },
    // { name: 'ngayTuyenDung', value: 'employeeList.workProcessInfo.ngayTuyenDung' }
  ],


  EDUCATION_PROCESS_INFO: [
    { name: 'trinhDoChuyenMon', value: 'employeeList.educationProcessInfo.trinhDoChuyenMon' },
    { name: 'chuyenNganhDaoTao', value: 'employeeList.educationProcessInfo.chuyenNganhDaoTao' },
    { name: 'truongDaotao', value: 'employeeList.educationProcessInfo.truongDaotao' },
    { name: 'xepLoaiTotNghiep', value: 'employeeList.educationProcessInfo.xepLoaiTotNghiep' },
    { name: 'trinhDoNgoaiNgu', value: 'employeeList.educationProcessInfo.trinhDoNgoaiNgu' },
    // { name: 'trinhDoTinHoc', value: 'employeeList.educationProcessInfo.trinhDoTinHoc' },
    { name: 'trinhDoChinhTri', value: 'employeeList.educationProcessInfo.trinhDoChinhTri' },
    // { name: 'trinhDoQLNhaNuoc', value: 'employeeList.educationProcessInfo.trinhDoQLNhaNuoc' }
  ],
  SALARY_FACTOR: [
    // { name: 'ngachCongChuc', value: 'employeeList.salaryFactor.ngachCongChuc' },
    { name: 'bangLuong', value: 'employeeList.salaryFactor.bangLuong' },
    { name: 'ngachluong', value: 'employeeList.salaryFactor.ngachluong' },
    { name: 'bacLuong', value: 'employeeList.salaryFactor.bacLuong' },
    { name: 'hscd', value: 'employeeList.salaryFactor.hscd' },
    { name: 'ngayApDung', value: 'employeeList.salaryFactor.ngayApDung' },
    { name: 'mocNangLuong', value: 'employeeList.salaryFactor.mocNangLuong' },
    { name: 'heSoChenhLech', value: 'employeeList.salaryFactor.heSoChenhLech' },
    { name: 'heSoThamNien', value: 'employeeList.salaryFactor.heSoThamNien' },
     { name: 'phuCapChucVu', value: 'employeeList.salaryFactor.phuCapChucVu' }
  ],
  SALARY_PROCESS: [
    { name: 'bangLuongBH', value: 'employeeList.salaryProcess.bangLuongBH' }
    , { name: 'ngachLuongBH', value: 'employeeList.salaryProcess.ngachLuongBH' }
    , { name: 'bacLuongBH', value: 'employeeList.salaryProcess.bacLuongBH' }
    , { name: 'heSoBaoHiem', value: 'employeeList.salaryProcess.heSoBaoHiem' }
    , { name: 'ngayApDungBH', value: 'employeeList.salaryProcess.ngayApDungBH' }
    , { name: 'mocNangLuongBH', value: 'employeeList.salaryProcess.mocNangLuongBH' }
  ],
  CONFIG_PROPERTIES : {
    VPS_ADMIN : 'VPS_ADMIN'
  }

} as any;
export enum SYSCAT_WORKFLOWS {
SETTINGS = 'SYSTEM',
EMPLOYEES = 'EMPLOYEE',
REPORTS = 'REPORT',
ORGANIZATIONS = 'ORGANIZATION',
TIMEKEEPING = 'TIME_KEEPING',
}
/* Tham số cấu hình thuộc tính động */
export enum INPUT_TYPE {
TEXT = 'text',
NUMBER= 'number',
TEXT_AREA = 'text-area',
CURRENCY = 'currency',
DATE = 'date',
URL = 'url'
}
export enum ACTION_FORM {
SEARCH = 'SEARCH',
INSERT = 'INSERT',
UPDATE = 'UPDATE',
IMPORT = 'IMPORT',
VIEW = 'VIEW',
DELETE = 'DELETE',
CONFIG = 'CONFIG',

EXTERIOR_SEARCH = 'EXTERIOR_SEARCH',
EXTERIOR_UPDATE = 'EXTERIOR_UPDATE',
EXTERIOR_INSERT = 'EXTERIOR_INSERT',
INTERIOR_SEARCH = 'INTERIOR_SEARCH',
INTERIOR_UPDATE = 'INTERIOR_UPDATE',
INTERIOR_INSERT = 'INTERIOR_INSERT',
LANGUAGE_INFORMATION_INSERT = 'LANGUAGE_INFORMATION_INSERT',
POLITICAL_DEGREE_INSERT = 'POLITICAL_DEGREE_INSERT',
LINE_ORG_INSERT = 'LINE_ORG_INSERT',
LINE_ORG_UPDATE = 'LINE_ORG_UPDATE',
GRADE_LADDER_UPDATE = 'GRADE_LADDER_UPDATE',
LIST_GRADE_UPDATE = 'LIST_GRADE_UPDATE',
LIST_STEP_UPDATE = 'LIST_STEP_UPDATE',
LIST_POSITION_SALARY = 'LIST_POSITION_SALARY',
GRADE_RATE_UPDATE = 'GRADE_RATE_UPDATE',

MAJOR_CAREER_INSERT = 'MAJOR_CAREER_INSERT',
MAJOR_CAREER_UPDATE = 'MAJOR_CAREER_UPDATE',

EMP_COMMON_INFO_INSERT = 'EMP_COMMON_INFO_INSERT',
EMP_PERSONAL_INFO_INSERT = 'EMP_PERSONAL_INFO_INSERT',
EMP_EMPLOYMENT_INFO_INSERT = 'EMP_EMPLOYMENT_INFO_INSERT',
EMP_INNER_PROCESS_INSERT = 'EMP_INNER_PROCESS_INSERT',
EMP_INNER_PROCESS_UPDATE = 'EMP_INNER_PROCESS_UPDATE',
EMP_OUTER_PROCESS_INSERT = 'EMP_OUTER_PROCESS_INSERT',
EMP_OUTER_PROCESS_UPDATE = 'EMP_OUTER_PROCESS_UPDATE',

ORG_COMMON_INFO_INSERT = 'ORG_COMMON_INFO_INSERT',
ORG_COMMON_INFO_UPDATE = 'ORG_COMMON_INFO_UPDATE',
ORG_RELATION_UPDATE = 'ORG_RELATION_UPDATE',
LABOUR_CONTRACT_DETAIL = 'LABOUR_CONTRACT_DETAIL',
ORG_ADDITIONAL_INFO_UPDATE = 'ORG_ADDITIONAL_INFO_UPDATE',
ORG_DUTY_UPDATE = 'ORG_DUTY_UPDATE',
ORG_BUSINESS_REGIS_UPDATE = 'ORG_BUSINESS_REGIS_UPDATE',
ORG_DOCUMENT_UPDATE = 'ORG_DOCUMENT_UPDATE',
ORG_BOUNDARY_UPDATE = 'ORG_BOUNDARY_UPDATE',

REWARD_PROCESS_SEARCH = 'REWARD_PROCESS_SEARCH',
REWARD_PROCESS_INSERT = 'REWARD_PROCESS_INSERT',
REWARD_PROCESS_UPDATE = 'REWARD_PROCESS_UPDATE',
DISCIPLINE_SEARCH = 'DISCIPLINE_SEARCH',
DISCIPLINE_INSERT = 'DISCIPLINE_INSERT',
DISCIPLINE_UPDATE = 'DISCIPLINE_UPDATE',

ORG_PLAN_SEARCH = 'ORG_PLAN_SEARCH',
ORG_PLAN_VIEW = 'ORG_PLAN_VIEW',
ORG_PLAN_INSERT = 'ORG_PLAN_INSERT',
ORG_PLAN_UPDATE = 'ORG_PLAN_UPDATE',
ORG_PLAN_CLONE = 'ORG_PLAN_CLONE',
ORG_DRAFF_COMMON_INFO_INSERT = 'ORG_DRAFF_COMMON_INSERT',
ORG_DRAFF_COMMON_INFO_UPDATE = 'ORG_DRAFF_COMMON_UPDATE',
ORG_DRAFF_RELATION_UPDATE = 'ORG_DRAFF_RELATION_UPDATE',
ORG_DRAFF_ADDITIONAL_INFO_UPDATE = 'ORG_DRAFF_ADDITIONAL_UPDATE',
ORG_DRAFF_DUTY_UPDATE = 'ORG_DRAFF_DUTY_UPDATE',
ORG_DRAFF_BUSINESS_REGIS_UPDATE = 'ORG_DRAFF_BUSINESS_UPDATE',
ORG_DRAFF_DOCUMENT_UPDATE = 'ORG_DRAFF_DOCUMENT_UPDATE',
ORG_DRAFF_BOUNDARY_UPDATE = 'ORG_DRAFF_BOUNDARY_UPDATE',

PASSPORT_SEARCH = 'PASSPORT_SEARCH',
PASSPORT_UPDATE = 'PASSPORT_UPDATE',
PASSPORT_INSERT = 'PASSPORT_INSERT',

VISA_PASSPORT_SEARCH = 'VISA_PASSPORT_SEARCH',
VISA_PASSPORT_UPDATE = 'VISA_PASSPORT_SEARCH',
VISA_PASSPORT_INSERT = 'VISA_PASSPORT_SEARCH',

VISA_SEARCH = 'VISA_SEARCH',
VISA_UPDATE = 'VISA_UPDATE',
VISA_INSERT = 'VISA_INSERT',
EMP_IDENTIFICATION_INSERT = 'EMP_IDENTIFICATION_INSERT',

EMP_HEALTH_INSURANCE_SEARCH = 'EMP_HEALTH_INSURANCE_SEARCH',
EMP_HEALTH_INSURANCE_INSERT = 'EMP_HEALTH_INSURANCE_INSERT',
EMP_HEALTH_INSURANCE_UPDATE = 'EMP_HEALTH_INSURANCE_UPDATE',
FAM_HEALTH_INSURANCE_SEARCH = 'FAM_HEALTH_INSURANCE_SEARCH',
FAM_HEALTH_INSURANCE_INSERT = 'FAM_HEALTH_INSURANCE_INSERT',
FAM_HEALTH_INSURANCE_UPDATE = 'FAM_HEALTH_INSURANCE_UPDATE',

POS_DES_ACCOUNTAB_INSERT = 'POS_DES_ACCOUNTAB_INSERT',
POS_DES_AUTHORITYAND_SCOPE_INSERT = 'POS_DES_AUTHORITYAND_SCOPE_INSERT',
POS_DES_INTERACTION_INSERT = 'POS_DES_INTERACTION_INSERT',
POS_DES_JOB_PURPOSE_INSERT = 'POS_DES_JOB_PURPOSE_INSERT',
POS_DES_JOB_REQUIREMENT_INSERT = 'POS_DES_JOB_REQUIREMENT_INSERT',

POS_DES_ACCOUNTAB_UPDATE = 'POS_DES_ACCOUNTAB_UPDATE',
POS_DES_AUTHORITYAND_SCOPE_UPDATE = 'POS_DES_AUTHORITYAND_SCOPE_UPDATE',
POS_DES_INTERACTION_UPDATE = 'POS_DES_INTERACTION_UPDATE',
POS_DES_JOB_PURPOSE_UPDATE = 'POS_DES_JOB_PURPOSE_UPDATE',
POS_DES_JOB_REQUIREMENT_UPDATE = 'POS_DES_JOB_REQUIREMENT_UPDATE',

TIMEKEEPING_LOCK_SEARCH = 'TIMEKEEPING_LOCK_SEARCH',
TIMEKEEPING_LOCK_LOCK = 'TIMEKEEPING_LOCK_LOCK',
TIMEKEEPING_LOCK_UNLOCK = 'TIMEKEEPING_LOCK_LOCK',
TIMEKEEPING_LOCK_CONFIG_SEARCH = 'TIMEKEEPING_LOCK_CONFIG_SEARCH',
TIMEKEEPING_LOCK_CONFIG_INSERT = 'TIMEKEEPING_LOCK_CONFIG_INSERT',
TIMEKEEPING_LOCK_CONFIG_UPDATE = 'TIMEKEEPING_LOCK_CONFIG_UPDATE',
ORG_ACTION_LOCK_SEARCH = 'ORG_ACTION_LOCK_SEARCH',
ORG_ACTION_LOCK_LOCK = 'ORG_ACTION_LOCK_LOCK',
ORG_ACTION_LOCK_UNLOCK = 'ORG_ACTION_LOCK_UNLOCK',
DETAIL_REPORT = 'DETAIL_REPORT',
SUMMARY_REPORT = 'SUMMARY_REPORT',

EMP_TAX_REDUCTION_INSERT = 'EMP_TAX_REDUCTION_INSERT',
EMP_TAX_REDUCTION_UPDATE = 'EMP_TAX_REDUCTION_UPDATE',
EMP_TAX_REDUCTION_SEARCH = 'EMP_TAX_REDUCTION_SEARCH',

SYS_CAT_TYPE_INSERT = 'SYS_CAT_TYPE_INSERT',
SYS_CAT_TYPE_UPDATE = 'SYS_CAT_TYPE_UPDATE',
SYS_CAT_TYPE_SEARCH = 'SYS_CAT_TYPE_SEARCH',

SALARY_TYPE_MAPPING_UPDATE = 'SALARY_TYPE_MAPPING_UPDATE',
SALARY_ORGANIZATION_MAPPING_UPDATE = 'SALARY_ORGANIZATION_MAPPING_UPDATE',

FORMULA_DIAGRAM_INSERT = 'FORMULA_DIAGRAM_INSERT',
FORMULA_DIAGRAM_UPDATE = 'FORMULA_DIAGRAM_UPDATE',

DATA_SPECIFICATION_UPDATE = 'DATA_SPECIFICATION_UPDATE',

FORMULA_CONFIG_INSERT = 'FORMULA_CONFIG_INSERT',
FORMULA_CONFIG_UPDATE = 'FORMULA_CONFIG_UPDATE',
FORMULA_CONFIG_IMPORT = 'FORMULA_CONFIG_IMPORT',


PAYROLL_GENERAL_SEARCH = 'PAYROLL_GENERAL_SEARCH',
SALARY_TYPE_ORGANIZATION_MAPPING_SEARCH = 'SALARY_TYPE_ORGANIZATION_MAPPING_SEARCH',
SALARY_TYPE_ORGANIZATION_MAPPING_UPDATE = 'SALARY_TYPE_ORGANIZATION_MAPPING_UPDATE',

PAYROLL_SYNC_TAX = 'SYNC_TAX',

}
export enum RESOURCE {
HOME = 'HOME',
SYS_PROPERTY = 'SYS_PROPERTY',
SYS_CAT = 'SYS_CAT',
SYS_CAT_TYPE = 'SYS_CAT_TYPE',
NATION = 'NATION',
EMP_FILE = 'EMP_FILE',
EMP_TYPE = 'EMP_TYPE',
LABOUR_CONTRACT_TYPE = 'LABOUR_CONTRACT_TYPE',
LABOUR_CONTRACT_DETAIL = 'LABOUR_CONTRACT_DETAIL',
MILITARY_INFORMATION = 'MILITARY_INFORMATION',
HEALTH_INSURANCE = 'HEALTH_INSURANCE',
LONG_LEAVE_PROCESS = 'LONG_LEAVE_PROCESS',
POSITION_CAREER = 'POSITION_CAREER',
EMPLOYEE_INFO = 'EMPLOYEE_INFO',
EMPLOYEE = 'EMPLOYEE',
CREATE_EMPLOYEE = 'CREATE_EMPLOYEE',
TRADE_UNION = 'TRADE_UNION',
POSITION_CATEGORY = 'POSITION_CATEGORY',
ORGANIZATION = 'ORGANIZATION',
ORGANIZATION_PLAN = 'ORGANIZATION_PLAN',
SOCIAL_INSURANCE_PROCESS = 'SOCIAL_INSURANCE_PROCESS',
OCCUPATIONAL_SAFETY = 'OCCUPATIONAL_SAFETY',
TRAINING_PLAN = 'TRAINING_PLAN',
ALLOWANCE_TYPE = 'ALLOWANCE_TYPE',
ALLOWANCE = 'ALLOWANCE',
SYSTEM_PARAMETER = 'SYSTEM_PARAMETER',
DOCUMENT_TYPE = 'DOCUMENT_TYPE',
REWARD_DISCIPLINE = 'REWARD_DISCIPLINE',
EMP_TYPE_PROCESS = 'EMP_TYPE_PROCESS',
REWARD_PROCESS = 'REWARD_PROCESS',
DISCIPLINE = 'DISCIPLINE',
FINANCIAL = 'FINANCIAL',
OTHER_INFORMATION = 'OTHER_INFORMATION',
PARTY_UNION = 'PARTY_UNION',
POSITION_SALARY = 'POSITION_SALARY',
VISA_PASSPORT_INFORMATION = 'VISA_PASSPORT_INFORMATION',
WORK_PROCESS = 'WORK_PROCESS',
EMP_EDUCATION_PROCESS = 'EMP_EDUCATION_PROCESS',
EMP_WORK_SCHEDULE = 'EMP_WORK_SCHEDULE',
DOCUMENT_REASON = 'DOCUMENT_REASON',
EMP_CODE_CONFIG = 'EMP_CODE_CONFIG',

CHANGE_HISTORY = 'CHANGE_HISTORY',
EMP_SALARY_PROCESS = 'EMP_SALARY_PROCESS',
LANGUAGE_INFORMATION = 'LANGUAGE_INFORMATION',
FAMILY_RELATIONSHIP = 'FAMILY_RELATIONSHIP',
SALARY_TABLE = 'SALARY_TABLE',
EMP_POSITION_SALARY = 'EMP_POSITION_SALARY',
EMP_ALLOWANCE_PROCESS = 'EMP_ALLOWANCE_PROCESS',
ATTENDANCE_TYPE = 'ATTENDANCE_TYPE',
ATTENDANCE_TYPE_DEFINITION='ATTENDANCE_TYPE_DEFINITION',
EMP_TAX_INFORMATION = 'EMP_TAX_INFORMATION',
EMP_INCOME_TAX = 'EMP_INCOME_TAX',
EMP_TAX_REDUCTION = 'EMP_TAX_REDUCTION',
LONG_LEAVE_REASON = 'LONG_LEAVE_REASON',
EMP_TIMEKEEPING = 'EMP_TIMEKEEPING',
DETAIL_REPORT = 'DETAIL_REPORT',
REQUIREMENT_DETAIL = 'REQUIREMENT_DETAIL',
POSITION = 'POSITION',
JOB_REQUIREMENT = 'JOB_REQUIREMENT',
TIMEKEEPING_LOCK = 'TIMEKEEPING_LOCK',
TIMEKEEPING_LOCK_CONFIG = 'TIMEKEEPING_LOCK_CONFIG',
WORK_SCHEDULE = 'WORK_SCHEDULE',
EMP_LANGUAGE_DEGREE = 'EMP_LANGUAGE_DEGREE',
ANNUAL_LEAVE = 'ANNUAL_LEAVE',
ORG_ACTION_LOCK = 'ORG_ACTION_LOCK',
/** REPORT Resource  */
TEMPLATE_DYNAMIC = 'TEMPLATE_DYNAMIC',
REPORT_DYNAMIC = 'REPORT_DYNAMIC',
REPORT_CUSTOM_STATISTIC = 'REPORT_CUSTOM_STATISTIC',
REPORT_EMPLOYEE_LIST = 'REPORT_EMPLOYEE_LIST',
REPORT_CUSTOM = 'REPORT_CUSTOM',
/** END REPORT Resource  */
NATION_CONFIG = 'NATION_CONFIG',
NATION_CONFIG_TYPE = 'NATION_CONFIG_TYPE',

/** PAYROLL Resource  */
SALARY_TYPE_MAPPING = 'SALARY_TYPE_MAPPING',
SALARY_TYPE = 'SALARY_TYPE',
PERIOD = 'PERIOD',
DATA_TYPE = 'DATA_TYPE',
PAYROLL_ITEM = 'PAYROLL_ITEM',
SALARY_TYPE_ORGANIZATION_MAPPING = 'SALARY_TYPE_ORGANIZATION_MAPPING',
// thanhnd
PAYROLL_DIAGRAM = 'PAYROLL_DIAGRAM',
FORMULA_CONFIG = 'FORMULA_CONFIG',
SALARY_ORGANIZATION_MAPPING = 'SALARY_ORGANIZATION_MAPPING',
  // quoctv
DATA_INPUT_PROCESS = 'DATA_INPUT_PROCESS',
PAYROLL_TABLE_CONFIG = 'PAYROLL_TABLE_CONFIG',
PAYROLL_GENERAL = 'PAYROLL_GENERAL',
PAYROLL_MASTER = 'PAYROLL_MASTER',
// quoctv: payslip-config
PAYSLIP_CONFIG = 'PAYSLIP_CONFIG',
PAYROLL_REPORT_TYPE = 'PAYROLL_REPORT_TYPE',
/** EVALUATION Resource  */
LOCK_UNLOCK_KI='LOCK_UNLOCK_KI',
ORGANIZATION_KI_PROCESS ='ORGANIZATION_KI_PROCESS',
EMPLOYEE_KI_PROCESS='EMPLOYEE_KI_PROCESS',

// PERMISSION
PERMISSION_ACTION = 'PERMISSION_ACTION',
PERMISSION_ROLE = 'PERMISSION_ROLE',
PERMISSION_MENU = 'PERMISSION_MENU',
PERMISSION_RESOURCE = 'PERMISSION_RESOURCE',
PERMISSION_USER = 'PERMISSION_USER',
SHIFT_ASSIGN = 'SHIFT_ASSIGN',
}
export enum SYSTEM_PARAMETER_CODE {
ORGANIZATION_ROOT = 'ORGANIZATION_ROOT',
TEMPLATE_DYNAMIC_FORM_TYPE = 'TEMPLATE_DYNAMIC_FORM_TYPE',
REPORT_DYNAMIC_CONDITION_TYPE = 'REPORT_DYNAMIC_CONDITION_TYPE',
REPORT_DYNAMIC_FORMAT_REPORT = 'REPORT_DYNAMIC_FORMAT_REPORT',
REPORT_DYNAMIC_DATA_TYPE = 'REPORT_DYNAMIC_DATA_TYPE',

// Phuc vụ tính lương Được cấu hình theo Group. Với name
TRANSFER_SERVICE = 'TRANSFER_SERVICE',
CALCULATE_SERVICE = 'CALCULATE_SERVICE',
GROUP_DATA = 'GROUP_DATA',
SALARY_FUND = 'SALARY_FUND',
PAYMENT_CODE = 'PAYMENT_CODE',
SALARY_TYPE = 'SALARY_TYPE',
REGULATION_SALARY_TYPE = 'REGULATION_SALARY_TYPE'
// End tính lương
}
export enum REPORT_DYNAMIC_CONDITION_TYPE {
ORGANIZATION_PERMISSION = 'CON_ORGANIZATION_PERMISSION',
LIST_STRING = 'CON_LIST_STRING',
STRING = 'CON_STRING',
LONG = 'CON_LONG',
ORGANIZATION_FULL = 'CON_ORGANIZATION_FULL',
COMBOBOX_CONDITION = 'CON_COMBOBOX_CONDITION',
DATE = 'CON_DATE',
DOUBLE = 'CON_DOUBLE',
LIST_LONG = 'CON_LIST_LONG',
ORGANIZATION_TREE = 'CON_ORGANIZATION_TREE',
COMBOBOX = 'CON_COMBOBOX',
GENDER = 'CON_GENDER',
ORGANIZATION_SALARY = 'CON_ORGANIZATION_SALARY',
ORGANIZATION_REPORT = 'CON_ORGANIZATION_REPORT'
}

export enum PAYROLL_CHANGE_TYPE{
  ARREARS = 1,
  RETROSPECTIVE = 2
}
//GiangTH List menu thu 3 khong khai bao url trong DB
export const FIXED_MENU = [
  { url: '/employees/position-category/description', name: 'Mô tả chức danh'}
]
