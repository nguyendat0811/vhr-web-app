import { AfterViewInit, Component, EventEmitter, Input, OnChanges, OnInit, Output, ViewChildren } from '@angular/core';
import { FormControl } from '@angular/forms';
import { OrganizationService } from '@app/core';
import { DEFAULT_MODAL_OPTIONS } from '@app/core/app-config';
import { HrStorage } from '@app/core/services/HrStorage';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ButtonOrgSelectorModalComponent } from './button-org-selector-modal/button-org-selector-modal.component';

@Component({
  selector: 'button-org-selector',
  templateUrl: './button-org-selector.component.html',
})
export class ButtonOrgSelectorComponent implements OnInit, AfterViewInit, OnChanges {
  @Input()
  public inputModel: number;

  @Input()
  public operationKey: string;

  @Input()
  public adResourceKey: string;

  @Input()
  public defaultValue: boolean;

  @Input()
  public filterCondition: string;

  @Input()
  public rootId: string;

  @Output()
  public onChange: EventEmitter<any> = new EventEmitter<any>();

  @Input()
  public disabled = false;

  // Huynq73: Check lay full don vi - khong phan quyen (phuc vu Bao cao dong)
  @Input()
  public checkPermission = true;

  @ViewChildren('displayName')
  public displayName;
  @ViewChildren('buttonChose')
  public buttonChose;

  constructor(
        private service: OrganizationService
      , private modalService: NgbModal
  ) {
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
  }
  /**
   * ngAfterViewInit
   */
  ngAfterViewInit() {
  }
  /**
   * delete
   */
  delete() {
    this.inputModel = 0;
    this.onChangeOrgId();
    this.onChange.emit(event);
  }
  /**
   * onChange orgId then load org name
   */
  public onChangeOrgId() {
    if (!this.inputModel || (this.inputModel !==0)) {
      if (this.displayName) {
        this.displayName.first.nativeElement.value = '';
      }
      if (this.defaultValue) {
        const market = HrStorage.getSelectedMarket();
        if (!market) {
          return;
        }
        const defaultDomain = market.grantedDomainId;
        if (CommonUtils.isNullOrEmpty(defaultDomain)) {
          return;
        }
        const a = defaultDomain.split(',');
        // thuc hien lay ten don vi de hien thi
        this.service.findOne(a[0])
        .subscribe((res) => {
          const data = res.data;
          if (data) {
            this.displayName.first.nativeElement.value = data.name;
            this.inputModel = data.organizationId;
          }
        });
      }
      return;
    }
    // thuc hien lay ten don vi de hien thi
    this.service.findOne(this.inputModel)
      .subscribe((res) => {
        const data = res.data;
        if (data) {
          this.displayName.first.nativeElement.value = data.name;
        }
      });
  }
  /**
   * ngOnChanges
   */
  ngOnChanges() {
    this.onChangeOrgId();
  }
  /**
   * onFocus
   */
  public onFocus(event) {
    // Sửa bug khi vào 1 form thì sẽ tự động autofocus !
    if ( !event.relatedTarget && !event.sourceCapabilities) {
      return;
    } else {
      this.buttonChose.first.nativeElement.focus();
      this.buttonChose.first.nativeElement.click();
    }
  }
  /**
   * onChose
   */
  public onChose() {
    const modalRef = this.modalService.open(ButtonOrgSelectorModalComponent, DEFAULT_MODAL_OPTIONS);
    modalRef.componentInstance
    .setInitValue({
      operationKey: this.operationKey
      , adResourceKey: this.adResourceKey
      , filterCondition: this.filterCondition
      , rootId: this.rootId
      , checkPermission: this.checkPermission
    });

    modalRef.result.then((node) => {
      if (!node) {
        return;
      }
      this.inputModel = node.organizationId;
      // callback on chose item
      this.onChange.emit(node);
    });
  }

}
