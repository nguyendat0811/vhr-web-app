import { HolidayService } from '../../../core/services/hr-timekeeping/holiday.service';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from './../base-component/base-component.component';
import { DEFAULT_MODAL_OPTIONS } from '@app/core';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import * as moment from 'moment';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
// tslint:disable-next-line:max-line-length
import { HolidayDefinitionFormComponent } from '@app/modules/timekeeping/attendance-type/holiday-definition-in-market/holiday-definition-form/holiday-definition-form.component';
// tslint:disable-next-line:max-line-length
import { HolidayDefinitionCloneComponent } from '@app/modules/timekeeping/attendance-type/holiday-definition-in-market/holiday-definition-clone/holiday-definition-clone.component';

@Component({
  selector: 'payroll-calendar',
  templateUrl: './payroll-calendar.component.html',
})
export class PayrollCalendarComponent extends BaseComponent implements OnInit {
  @Input()
  public month: string;
  public firstDayOfMonth;
  public firstDayOfWeek;
  public listWeek = [];
  @Input()
  public holidayMap = {};
  public holidayMapByDate = {};
  @Output()
  public onChangeHoliday: EventEmitter<any> = new EventEmitter<any>();
  checkData: boolean;
  constructor(private modalService: NgbModal
            , public actr: ActivatedRoute
            , private holidayService: HolidayService ) {
    super(actr);
    this.setMainService(this.holidayService);
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.firstDayOfMonth = moment('01/' + this.month, 'DD/MM/YYYY');
    this.firstDayOfWeek = moment('01/' + this.month, 'DD/MM/YYYY').startOf('week');
    const numOfDays = 42;
    for (let i = 0; i < numOfDays; i++) {
      const t = moment('01/' + this.month, 'DD/MM/YYYY').startOf('week');
      let weekNum = i / 7;
      weekNum = parseInt('' + weekNum);
      let current = [];
      if (this.listWeek.length > weekNum) {
        current = this.listWeek[weekNum];
      } else {
        this.listWeek[weekNum] = current;
      }
      const item = this.firstDayOfWeek.clone().add(i, 'day');
      current.push(item);
      this.holidayMapByDate[item.toDate().getTime()] = this.makeStringWorkDay(item.toDate().getTime());
      this.listWeek[weekNum] = current;
    }

  }

  showPopup(day?: any) {
    const modalRef = this.modalService.open(HolidayDefinitionFormComponent, DEFAULT_MODAL_OPTIONS);
    if (day) {
      this.holidayService.findByDate(day)
      .subscribe(res => {
        modalRef.componentInstance.setFormValue(this.propertyConfigs, res, day);
      });
    } else {
      modalRef.componentInstance.setFormValue(this.propertyConfigs);
    }
    modalRef.result.then((result) => {
      this.onChangeHoliday.emit(result);
    });
  }

  makeStringWorkDay(time): string {
    const list = this.holidayMap[time];
    if (list && list.length > 0) {
      const str = [];
      for (const item of list) {
        str.push(item.workdayTypeCode + ':' + item.hours);
      }
      return str.join(', ');
    } else {
      return null;
    }
  }
}
