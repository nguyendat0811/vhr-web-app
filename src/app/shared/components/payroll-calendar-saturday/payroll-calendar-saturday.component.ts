import { HolidayService } from '../../../core/services/hr-timekeeping/holiday.service';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from './../base-component/base-component.component';
import { Component, OnInit, Input, Output} from '@angular/core';
import * as moment from 'moment';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormArray, FormControl } from '@angular/forms';

@Component({
  selector: 'payroll-calendar-saturday',
  templateUrl: './payroll-calendar-saturday.component.html'
})
export class PayrollCalendarSaturdayComponent extends BaseComponent implements OnInit {
  public isChecked: boolean;
  @Input()
  public property: FormArray;
  @Input()
  public month: string;
  @Input()
  public startDate: any;
  @Input()
  public endDate: any
  @Input()
  listToTal: FormArray

  public firstDayOfMonth;
  public firstDayOfWeek;
  public listWeek = [];
  @Input()
  public mapToTal = {};
  public mapDateTimekeeping = {};
  @Output()
  checkData: boolean;
  listData = [];
  constructor(public actr: ActivatedRoute, private holidayService: HolidayService ) {
    super(actr);
    this.setMainService(this.holidayService);
    this.isChecked = false;
  }

  selectedValue: any;
  checkShowClear = false;

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.firstDayOfMonth = moment('01/' + this.month, 'DD/MM/YYYY');
    this.firstDayOfWeek = moment('01/' + this.month, 'DD/MM/YYYY').startOf('week');
    const numOfDays = 42;
    for (let i = 0; i < numOfDays; i++) {
      const t = moment('01/' + this.month, 'DD/MM/YYYY').startOf('week');
      let weekNum = i / 7;
      weekNum = parseInt('' + weekNum);
      let current = [];
      if (this.listWeek.length > weekNum) {
        current = this.listWeek[weekNum];
      } else {
        this.listWeek[weekNum] = current;
      }
      const item = this.firstDayOfWeek.clone().add(i, 'day');
      current.push(item);
      this.listWeek[weekNum] = current;
    }
    this.initMapDateTimekeeping();
  }

  initMapDateTimekeeping() {
    this.mapDateTimekeeping = {};
    if (this.property && this.property.controls) {
      for (const control of this.property.controls) {
        if (this.startDate <= control.value && this.endDate >= control.value) {
          this.mapDateTimekeeping[control.value] = control;
        } else {
          const indexOf = this.property.controls.indexOf(control);
          this.property.removeAt(indexOf);
          delete this.mapToTal[control.value];
        }
      }
    }
  }


  isExist(day): boolean {
    for (const control of this.listToTal.controls) {
      if(control.value == day)
      {
        return true;
      }
    }
    return false;
  }

  selectedChange(day) {
    const time = day.toDate().getTime();
    if (this.mapDateTimekeeping && this.mapDateTimekeeping[time]) {
      const control = this.mapDateTimekeeping[time];
      const indexOf = this.property.controls.indexOf(control);
      this.property.removeAt(indexOf);
      delete this.mapToTal[time];
    } else {
      const control = new FormControl(day.toDate().getTime());
      this.property.insert(1, control);
      this.mapToTal[time] = control;
    }
    this.property.controls.sort(function(obj1, obj2) {
      return obj1.value - obj2.value;
    });
    this.initMapDateTimekeeping();
  }

  Duplidate(day): boolean  {
    const time = day.toDate().getTime();
    if ( this.mapToTal && this.mapToTal[time] && !this.mapDateTimekeeping[time]){
        return true;
    }
      return false;
  }

}
