import { Component, OnInit, Input, ViewChild, ElementRef, Output, EventEmitter, OnChanges } from '@angular/core';
import { CommonUtils } from '@app/shared/services';
import { FileControl, FileAttachment } from '@app/core/models/file.control';
import { AppComponent } from '@app/app.component';
import { FileStorageService } from '@app/core/services/file-storage.service';
import { saveAs } from 'file-saver';
import { TranslationService } from 'angular-l10n';

@Component({
  selector: 'multil-file-choser',
  templateUrl: './multil-file-choser.component.html',
})
export class MultilFileChoserComponent implements OnInit, OnChanges {
  @Input()
  property: FileControl;
  public files: any;
  @Input()
  private validMaxSize = -1;
  @Input()
  private validType: string;
  @Input()
  private maxFile = -1;
  @Output()
  public onFileChange: EventEmitter<any> = new EventEmitter<any>();
  @Input()
  public disabled: boolean;
  public emptyFile = new File([''], '-', {type: 'vhr_stored_file'});

  /**
   * constructor
   */
  constructor(private app: AppComponent
    , private fileStorage: FileStorageService
    , private translation: TranslationService
    ) { }
  /**
   * ngOnChanges
   */
  ngOnChanges() {
    this.ngOnInit();
  }
  /**
   * ngOnInit
   */
  ngOnInit() {
    // tạo giá trị mặc định cho fileAttachment
    if (this.property) {
      if (!this.property.fileAttachment || this.property.fileAttachment.length === 0) {
        this.property.fileAttachment = [this.makeEmptyFileAttach()];
      } else {
        for (const fileAttr of this.property.fileAttachment ) {
          if (!fileAttr.isTemp) {
            fileAttr.file = this.emptyFile;
          }
        }
        this.onFileChanged();
      }
    }
  }
  /**
   * onChange
   */
  public onChange(event, fileAttr: FileAttachment) {
    // const files = this.multiple ? event.target.files : event.target.files[0];
    const file = event.target.files[0];
    fileAttr.target = event.target;
    if (!this.isValidFile(file, fileAttr)) {
      fileAttr.fileName = this.translation.translate('controls.noFile');
      fileAttr.target.value = '';
      return;
    } else {
      this.setFileTemp(fileAttr, file);
      this.onFileChanged();
    }
  }
  /**
   * setFileTemp
   */
  private setFileTemp(fileAttr, file) {
    fileAttr.file = file;
    if (file === null) {
      fileAttr.fileName = this.translation.translate('controls.noFile');
      if (fileAttr.target) {
        fileAttr.target.value = '';
      }
    } else {
      fileAttr.fileName = file.name;
    }
  }
  /**
   * onFileChanged
   */
  public onFileChanged() {
    const files = [];
    for (const fileAttr of this.property.fileAttachment ) {
      if ( fileAttr.file ) {
        files.push(fileAttr.file);
      }
    }
    if (files.length > 0) {
      this.property.setValue( files );
    } else {
      this.property.setValue( null );
    }
  }
  /**
   * delete
   */
  public delete(fileAttr: FileAttachment) {
    if (fileAttr.isTemp === false || fileAttr.isTemp === undefined) {
      this.app.confirmDelete('common.message.confirm.deleteFile', () => {// on accepted
        this.fileStorage.deleteFile(fileAttr.id)
        .subscribe(res => {
          if (this.fileStorage.requestIsSuccess(res)) {
            this.setFileTemp(fileAttr, null);
            fileAttr.id = null;
            fileAttr.length = null;
            fileAttr.chunkSize = null;
            fileAttr.uploadDate = null;
            fileAttr.file = null;
            fileAttr.isTemp = true;
            this.onFileChanged();
          }
        });
      }, () => {// on rejected

      });
    } else {
      this.setFileTemp(fileAttr, null);
      this.onFileChanged();
    }
  }
  /**
   * isValidFile
   */
  public isValidFile(file, fileAttr): boolean {
    if (!file) {
      return true;
    }
    if (this.validMaxSize > 0) {
      if (CommonUtils.tctGetFileSize(file) > this.validMaxSize) {
        fileAttr.errorKey = 'controls.maxFileSize';
        return false;
      }
    }
    if (!CommonUtils.isNullOrEmpty(this.validType)) {
      const fileName = file.name;
      const temp = fileName.split('.');
      const ext = temp[temp.length - 1].toLowerCase();
      const ruleFile = ',' + this.validType;
      if (ruleFile.toLowerCase().indexOf(ext) === -1) {
        fileAttr.errorKey = 'controls.fileType';
        return false;
      }
    }

    fileAttr.errorKey = null;
    return true;
  }
  /**
   * downloadFile
   */
  public downloadFile(file) {
    this.fileStorage.downloadFile(file.id)
        .subscribe(res => {
          saveAs(res, file.fileName);
        });
  }

  /**
   * makeEmptyFileAttach
   */
  private makeEmptyFileAttach(): FileAttachment {
    return {
      id: null,
      fileName: this.translation.translate('controls.noFile'),
      length: null,
      chunkSize: null,
      uploadDate: null,
      isTemp: true,
      file: null,
      target: null,
      errorKey: null,
    };
  }
  /**
   * addItem
   */
  public addItem(item) {
    if (this.maxFile > 0 && this.property.fileAttachment.length >= this.maxFile) {
      return;
    }
    this.property.fileAttachment.push(this.makeEmptyFileAttach());
    this.onFileChanged();
  }
  /**
   * removeItem
   */
  public removeItem(item) {
    this.property.fileAttachment.splice(this.property.fileAttachment.indexOf(item), 1);
    if (this.property.fileAttachment.length === 0) {
      this.property.fileAttachment.push(this.makeEmptyFileAttach());
    }
    this.onFileChanged();
  }

  public hideError(fileAttr) {
    fileAttr.errorKey = null;
  }
}
