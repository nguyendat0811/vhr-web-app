import { Component, OnInit, Input, EventEmitter, Output, ViewChildren, ViewChild, SimpleChanges, SimpleChange } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LocationPickerModalComponent } from './location-picker-modal/location-picker-modal.component';
import { FormControl } from '@angular/forms';
import { LocationService, NationService } from '@app/core';
import { BaseControl } from '@app/core/models/base.control';
import { TranslationService } from 'angular-l10n';

@Component({
  selector: 'location-picker',
  templateUrl: './location-picker.component.html'
})
export class LocationPickerComponent implements OnInit {
  @Input()
  public property: FormControl;
  @Input()
  public propertyNation = new BaseControl();
  @Input()
  public nation: number;
  @Input()
  public isRequiredField = false;
  @Input()
  public readonly = false;
  @Input()
  public disabled = false;
  @Output()
  public onChange: EventEmitter<any> = new EventEmitter<any>();
  @Input()
  public maxLength: number;
  public nationObj: any;
  // Start fix request select by nation
  @Input()
  public nationConfigType: string;
  public nationSelected: any;
  public nationFilter = '';
  @ViewChildren('btnItem')
  public btnItems;
  @ViewChildren('inputSearch')
  public inputSearch;
  // End

  locationFullName = '';
  titleChooseNation = '';

  listNation: [];

  @ViewChildren('displayName')
  public displayName;
  @ViewChildren('text')
  public text;
  @ViewChildren('buttonChooseLocation')
  public buttonChooseLocation;

  @ViewChild('op') overlayPanel: any;
  constructor(
    private modalService: NgbModal,
    public locationService: LocationService,
    private nationService: NationService,
    public translation: TranslationService
  ) {
    this.nationObj = {};
  }

  ngOnChanges(changes: SimpleChanges) {
    const nation: SimpleChange = changes.nation;
    if (nation) {
      this.propertyNation.setValue(nation.currentValue);
    }
  }

  ngOnInit() {
    this.nationService.getNationList()
      .subscribe(res => { this.listNation = res.data });
  }
  /**
   * onChooseLocation
   * param item
   */
  public onChooseLocation() {
    if (!this.propertyNation.value) {
      return;
    }
    const modalRef = this.modalService.open(LocationPickerModalComponent, {
      backdrop: 'static',
      size: 'sm',
    });
    modalRef.componentInstance.defaultNation = this.propertyNation.value;
    modalRef.result.then((item) => {
      if (!item) {
        return;
      }
      this.locationFullName = item.label;
      if (item.parent) {
        this.locationFullName += ' - ' + item.parent.label;
        if (item.parent.parent) {
          this.locationFullName += ' - ' + item.parent.parent.label;
        }
      }
      this.property.setValue(this.locationFullName);
    });
  }

  public getTitleChooseNation() {
    return this.nationObj.name || this.translation.translate('button.title.selectNation');
  }
  public choseNation(nation) {
    this.nationObj = nation;
    this.propertyNation.setValue(this.nationObj.nationId);
  }
  changeIndex(index) { // tính index của element đang focus
    let currentIndex = -1; // chưa có item nào được focus
    for (let i = 0; i < this.btnItems._results.length; i++) {
      const item = this.btnItems._results[i];
      if (item.nativeElement.classList.contains('focus')) {
        item.nativeElement.classList.remove('focus');
        currentIndex = i;
        break;
      }
    }
    if (currentIndex <= -1) { // không tìm thấy item focus
      this.btnItems.first.nativeElement.focus();
      this.btnItems.first.nativeElement.classList.add('focus');
      this.inputSearch.first.nativeElement.focus();
      return;
    }
    let nextIndex = currentIndex += index;
    nextIndex = (nextIndex <= 0) ? 0 : (nextIndex >= this.btnItems._results.length ? this.btnItems._results.length - 1 : nextIndex);
    this.btnItems._results[nextIndex].nativeElement.classList.add('focus');
  }
  focusElement() { // focus vào element để scroll down up
    for (const item of this.btnItems._results) {
      if (item.nativeElement.className.includes('focus')) {
        item.nativeElement.focus();
        this.inputSearch.first.nativeElement.focus();
        return;
      }
    }
  }
  focusInputSearch() {
    setTimeout(() => {
      this.inputSearch.first.nativeElement.focus();
    }, 400);
  }
  choseFocusNation() {
    // chọn element đang focus
    for (const item of this.btnItems._results) {
      if (item.nativeElement.className.includes('focus')) {
        item.nativeElement.click();
        this.inputSearch.first.nativeElement.focus();
        return;
      }
    }
    // đóng pupop
    for (const item of this.btnItems._results) {
      if (item.nativeElement.className.includes('selected')) {
        item.nativeElement.click();
        return;
      }
    }
  }
}
