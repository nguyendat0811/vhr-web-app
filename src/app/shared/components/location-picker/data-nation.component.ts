import { NationService } from './../../../core/services/nation/nation.service';
import { FormControl } from '@angular/forms';
import { Component, OnInit, Input, EventEmitter, Output, ViewChildren, ViewChild, SimpleChanges, SimpleChange } from '@angular/core';

@Component({
  selector: 'data-nation',
  templateUrl: './data-nation.component.html'
})
export class DataNationComponent implements OnInit  {
  @Input()
  public property: FormControl;
  public selectIcon: any;
  @ViewChild('op') overlayPanel: any;

  constructor(
    private nationService: NationService
  ) {
  }
  ngOnInit() {
    this.selectIcon = this.property.value;
  }

  public chooseNation(icon) {
    this.property.setValue(icon);
    this.selectIcon = icon;
  }
}
