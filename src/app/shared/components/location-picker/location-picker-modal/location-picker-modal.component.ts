import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TreeNode } from 'primeng/api';
import { CommonUtils } from '@app/shared/services';
import { LocationService } from '@app/core';

@Component({
  selector: 'location-picker-modal',
  templateUrl: './location-picker-modal.component.html'
})
export class LocationPickerModalComponent implements OnInit {

  nodes: TreeNode[];
  selectedNode: TreeNode;
  defaultNation: number;
  @Output()
  public treeSelectNode: EventEmitter<TreeNode> = new EventEmitter<TreeNode>();
  constructor(
      public activeModal: NgbActiveModal,
      public locationService: LocationService
    ) { }

  ngOnInit() {
    this.actionInitAjax();
  }

  /**
   * action init ajax
   */
  private actionInitAjax() {
    this.defaultNation = this.defaultNation || CommonUtils.getNationId();
    this.locationService.actionInitAjax(this.defaultNation)
      .subscribe((res) => {
        this.nodes = CommonUtils.toTreeNode(res);
    });
  }
  /**
   * actionLazyRead
   * @ param event
   */
  public actionLazyRead(event) {
    this.locationService.actionLazyRead(event.node.nodeId)
        .subscribe((res) => {
          event.node.children = CommonUtils.toTreeNode(res);
        });
  }

  /**
   * nodeSelect
   * @ param event
   */
  public nodeSelect(event) {
  }

  public choose() {
    this.activeModal.close(this.selectedNode);
  }
}
