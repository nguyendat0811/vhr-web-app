import { CommonUtils } from './../../../services/common-utils.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from '@app/app.component';

@Component({
  selector: 'diagram-dashboard-dialog',
  templateUrl: './diagram-dashboard-dialog.component.html',
  styles: []
})
export class DiagramDashboardDialogComponent implements OnInit {
  formSave: FormGroup;
  paramsArr = [];

  url: string;
  constructor(public formGroup: FormBuilder
            , public activeModal: NgbActiveModal
            , public app: AppComponent) {
    this.formSave = this.formGroup.group({});
   }

  ngOnInit() {
  }
  public setFormValue(url: any) {
    this.formSave = new FormGroup({});
    this.url = (' ' + url).slice(1);
    let idxStart = url.indexOf('{');
    let idxEnd = url.indexOf('}');
    while (idxStart && idxEnd && idxStart > 0 && idxEnd > 0) {
      const lbl = url.substring(idxStart + 1, idxEnd);
      this.paramsArr.push(lbl);
      this.formSave.addControl(lbl, new FormControl(''));
      this.formSave.get(lbl).updateValueAndValidity();
      const urlRemoved = url.slice(idxStart, idxEnd + 1);
      url = url.replace(urlRemoved, '');
      idxStart = url.indexOf('{');
      idxEnd = url.indexOf('}');
    }
  }
  public processSaveOrUpdate(): void {
      for (const param of this.paramsArr) {
        const _value = this.formSave.get(param).value ? this.formSave.get(param).value : '';
        this.url = this.url.replace(param, _value);
      }
      while (this.url.indexOf('{') > 0) {
        this.url = this.url.replace('{', '');
      }
      while (this.url.indexOf('}') > 0) {
        this.url = this.url.replace('}', '');
      }
      console.log(this.url);
      this.activeModal.close(this.url);
  }
}
