import { HelperService } from './../../services/helper.service';
import { WorkFlowsService } from './../../../modules/settings/work-flows/work-flows.service';
import { ViewChild, Injectable } from '@angular/core';
import { DiagramComponent, NodeModel, PointPortModel, SnapSettingsModel, Connector
  , FlowShapeModel, MarginModel, TextStyleModel, StrokeStyleModel, OrthogonalSegmentModel
  , NodeConstraints, ConnectorConstraints } from '@syncfusion/ej2-ng-diagrams';
import { CommonUtils } from '@app/shared/services';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SMALL_MODAL_OPTIONS } from '@app/core';
import { DiagramDashboardDialogComponent } from './diagram-dashboard-dialog/diagram-dashboard-dialog.component';

@Injectable()
export class DiagramDashboardComponent {
  userMenus = [];
  userPermissions = [];
  WORKFLOWS_ID = '';
  navigationSubscription;
  isShowDiagram = true;
  @ViewChild('diagram')
  public diagram: DiagramComponent;
  @ViewChild('diagramTmp')
  public diagramTmp: DiagramComponent;
  constructor(public activatedRoute: ActivatedRoute
            , public router: Router
            , public workFlowsService: WorkFlowsService
            , public helperService: HelperService
            , public modalService: NgbModal) {
    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      // If it is a NavigationEnd event re-initalise the component
      if (e instanceof NavigationEnd) {
        this.isShowDiagram = true;
        this.diagram.clear();
        this.WORKFLOWS_ID = this.activatedRoute.snapshot.params.workFlows;
        this.initData();
      }
    });
  }
  private initData(): void {
    this.userMenus = CommonUtils.getUserMenu();
    this.userPermissions = CommonUtils.getUserPermission();
    this.workFlowsService.findByCode(this.WORKFLOWS_ID).subscribe(res => {
      if (res.data && res.data.length > 0) {
        this.helperService.isProcessing(true);
        setTimeout(() => {
          this.diagramTmp.loadDiagram(res.data[0].diagram);
          const nodesAfterCheck = this.checkPermission(this.diagramTmp.nodes);
          this.diagram.height = Number(this.diagramTmp.height) + 40;
          this.diagram.nodes = Object.assign([], nodesAfterCheck);
          this.diagram.connectors = Object.assign([], this.lockConnectors(this.diagramTmp.connectors));
          this.helperService.isProcessing(false);
        }, 500);
      }
      if (!res.data || !res.data.diagram ) {
        this.isShowDiagram = false;
      }
    });
  }
  public checkPermission(nodes: any): any {
    if (nodes.length <= 0) {
      this.isShowDiagram = false;
    } else {
      for (const node of nodes) {
        this.styleNode(node);
        if (node && (node.id.includes('Task') || (node.id.includes('Process') && node.data && node.data.url)) ) {
          if (node.data && node.data.url) {
            if (this.hasPermission(node.data)) {
              node.style = { fill: '#ccf2ff', strokeColor: '#58666E' }; // Da cau hinh URL, Co quyen
            } else {
              node.style = { fill: '#ff6666', strokeColor: '#58666E' }; // Da cau hinh URL, Ko co quyen
            }
          } else {
            node.style = { fill: '#ffc107', strokeColor: '#58666E' }; // Chua cau hinh URL
          }
        }
      }
    }
    return nodes;
  }
  public styleNode(node: any): any {
    node.constraints = NodeConstraints.PointerEvents | NodeConstraints.Select;
    for (let j: number = 0; j < node.annotations.length; j++) {
      const textStyle: TextStyleModel = node.annotations[j].style;
      textStyle.bold = true;
      textStyle.color = '#000';
      textStyle.fontFamily = 'Arial';
    }
    return node;
  }
  public lockConnectors(connectors: any): any {
    for (const connector of connectors) {
      connector.constraints = ConnectorConstraints.PointerEvents | ConnectorConstraints.Select;
    }
    return connectors;
  }
  public selectionChange(args: any): void {
    if (args.element && this.diagram.selectedItems.nodes && this.diagram.selectedItems.nodes.length === 1) {
      const node = this.diagram.selectedItems.nodes[0];
      if (node.data && node.data['url'] && this.hasPermission(node.data)) {
        if (node.data['url'].indexOf('{') >= 0) {
          this.configURLWithParams(node.data['url']);
        } else {
          this.router.navigate([node.data['url']]);
        }
      }
    }
  }
  private hasPermission(param: any): boolean {
    let result = false;
    result = this.userMenus.some(x => x.url && param.url.includes(x.url));
    if (!result) {
      result = this.userPermissions.some(x => x.resourceCode && x.resourceCode === param.resource);
    }
    return result;
  }
  private configURLWithParams(url: string) {
    const modalRef = this.modalService.open(DiagramDashboardDialogComponent, SMALL_MODAL_OPTIONS);
    modalRef.componentInstance.setFormValue(url);
    modalRef.result.then((result) => {
      if (result) {
        this.router.navigate([result]);
      }
    });
  }
 // #region Diagram Properties
 // tslint:disable: member-ordering
 public terminator: FlowShapeModel = { type: 'Flow', shape: 'Terminator' };
 public process: FlowShapeModel = { type: 'Flow', shape: 'Process' };
 public decision: FlowShapeModel = { type: 'Flow', shape: 'Decision' };
 public data: FlowShapeModel = { type: 'Flow', shape: 'Data' };
 public directdata: FlowShapeModel = { type: 'Flow', shape: 'DirectData' };
 public margin: MarginModel = { left: 25, right: 25 };
 public connAnnotStyle: TextStyleModel = { fill: '#fff' };
 public strokeStyle: StrokeStyleModel = { strokeDashArray: '2,2' };

 public segments: OrthogonalSegmentModel = [{ type: 'Orthogonal', direction: 'Top', length: 120 }];
 public segments1: OrthogonalSegmentModel = [
   { type: 'Orthogonal', direction: 'Right', length: 100 }
 ];
  public interval: number[] = [
    1, 9, 0.25, 9.75, 0.25, 9.75, 0.25, 9.75, 0.25, 9.75, 0.25,
    9.75, 0.25, 9.75, 0.25, 9.75, 0.25, 9.75, 0.25, 9.75
  ];
 public nodeDefaults(node: NodeModel): NodeModel {
  const obj: NodeModel = {};
  if (obj.width === undefined) {
    obj.width = 145;
  } else {
    const ratio: number = 100 / obj.width;
    obj.width = 100;
    obj.height *= ratio;
  }
  obj.style = { fill: '#fff', strokeColor: '#58666E' };
  obj.annotations = [{ style: { color: '#58666E', fill: 'transparent' } }];
  obj.ports = getPorts(node);
  return obj;
}
public connDefaults(obj: Connector): void {
  if (obj.id.indexOf('connector') !== -1) {
    obj.type = 'Orthogonal';
    obj.targetDecorator = { shape: 'Arrow', width: 10, height: 10 };
  }
}
public snapSettings: SnapSettingsModel = {
  horizontalGridlines: { lineColor: '#e0e0e0', lineIntervals: this.interval },
  verticalGridlines: { lineColor: '#e0e0e0', lineIntervals: this.interval }
};

  //#endregion Diagram Properties
}
function getPorts(obj: NodeModel): PointPortModel[] {
  const ports: PointPortModel[] = [
    { id: 'port1', shape: 'Circle', offset: { x: 0, y: 0.5 } },
    { id: 'port2', shape: 'Circle', offset: { x: 0.5, y: 1 } },
    { id: 'port3', shape: 'Circle', offset: { x: 1, y: 0.5 } },
    { id: 'port4', shape: 'Circle', offset: { x: 0.5, y: 0 } }
  ];
  return ports;
 }
