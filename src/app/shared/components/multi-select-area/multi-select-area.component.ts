import { FormControl } from '@angular/forms';
import { Component, OnInit, Input, OnChanges, SimpleChanges, SimpleChange, EventEmitter, Output } from '@angular/core';
import { SelectItem } from 'primeng/api';
import { TranslationService } from 'angular-l10n';


@Component({
  selector: 'multi-select-area',
  templateUrl: './multi-select-area.component.html'
})
export class MultiSelectAreaComponent  implements OnChanges, OnInit {

  @Input()
  public property: FormControl;
  @Input()
  public isMultiLanguage: boolean;
  @Input()
  public options: any;
  @Input()
  public optionLabel: string;
  @Input()
  public optionValue: string;
  @Input()
  public filterPlaceHolder: string;

  @Input()
  public placeHolder: string;
  @Input()
  public disabled = false;
  @Input()
  public autoFocus = false;
  @Input()
  public appendTo = '';

  @Input()
  public height: any = 150;

  lstSelected : Array<any>;
  selectedValue: any;
  listData: SelectItem[];
  selectedCities: string[] = [];
  @Output()
  public onChange: EventEmitter<any> = new EventEmitter<any>();
  constructor(
    public translation: TranslationService
  ) {
    this.listData = [];
  }

  ngOnChanges(changes: SimpleChanges) {
    const options: SimpleChange = changes.options;
    const property: SimpleChange = changes.property;
    if (options && options.currentValue) {
      this.initDropdown(options.currentValue);
    }
    this.selectedValue = this.property.value;
  }

  ngOnInit() {
  }

  initDropdown(data?: any) {
    if (data) {
      this.listData = [];
      for (const item of data) {
        let label = item[this.optionLabel];
        if (this.isMultiLanguage) {
          label = this.translation.translate(label);
        }
        this.listData.push({label: label, value: item[this.optionValue]});
      }
    }
  }
  selectedChange() {
    var lst = [];
    if(this.lstSelected && this.lstSelected.length > 0){
      this.lstSelected.forEach(element => {
        lst.push(element.value);
      });
    }
    this.property.setValue(lst);
    this.onChange.emit(this.lstSelected);
  }


  // public onRowSelect(event) {
  //   console.log('select', event.originalEvent.target['nodeName'])
  //   if (event.originalEvent.target['nodeName'] === "p-tablecheckbox") {
  //     console.log("clicked on a Checkbox")
  //   }
  //   else {
  //     console.log("clicked on a Row")
  //   }
  // }

}
