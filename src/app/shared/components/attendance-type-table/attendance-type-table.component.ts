import { Component, OnInit } from '@angular/core';
import { WorkdayTypeService } from '@app/core/services/hr-timekeeping/workday-type.service';

@Component({
  selector: 'attendance-type-table',
  templateUrl: './attendance-type-table.component.html',
  styleUrls: ['./attendance-type-table.component.css']
})
export class AttendanceTypeTableComponent implements OnInit {
  isHide = false;
  list: any;
  listWorkdayTypeCodeShift1: any;
  listWorkdayTypeCodeShift2: any;
  listWorkdayTypeCodeShift3: any;
  constructor(private workdayTypeService: WorkdayTypeService) {
    this.workdayTypeService.search().subscribe(res => {
      this.list = res;
      this.listWorkdayTypeCodeShift1 = this.list.extendData.listWorkdayTypeCodeShift1;
      this.listWorkdayTypeCodeShift2 = this.list.extendData.listWorkdayTypeCodeShift2;
      this.listWorkdayTypeCodeShift3 = this.list.extendData.listWorkdayTypeCodeShift3;
    });
  }

  ngOnInit() {
  }
}
