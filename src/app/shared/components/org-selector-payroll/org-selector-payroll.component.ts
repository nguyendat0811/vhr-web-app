import { AfterViewInit, Component, EventEmitter, Input, OnChanges, OnInit, Output, ViewChildren } from '@angular/core';
import { FormControl } from '@angular/forms';
import { OrganizationService } from '@app/core';
import { DEFAULT_MODAL_OPTIONS } from '@app/core/app-config';
import { HrStorage } from '@app/core/services/HrStorage';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { OrgSelectorPayrollModalComponent } from './org-selector-payroll-modal/org-selector-payroll-modal.component';

@Component({
  selector: 'org-selector-payroll',
  templateUrl: './org-selector-payroll.component.html',
})
export class OrgSelectorPayrollComponent implements OnInit, AfterViewInit, OnChanges {
  @Input()
  public property: FormControl;

  @Input()
  public isRequiredField = false;

  @Input()
  public operationKey: string;

  @Input()
  public adResourceKey: string;

  @Input()
  public defaultValue: boolean;

  @Input()
  public rootId: string;

  @Input()
  public filterCondition: string;

  @Output()
  public onChange: EventEmitter<any> = new EventEmitter<any>();

  @Input()
  public disabled = false;

  // Huynq73: Check lay full don vi - khong phan quyen (phuc vu Bao cao dong)
  @Input()
  public checkPermission = true;

  @ViewChildren('displayName')
  public displayName;
  @ViewChildren('buttonChose')
  public buttonChose;

  constructor(
        private service: OrganizationService
      , private modalService: NgbModal
  ) {
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
  }
  /**
   * ngAfterViewInit
   */
  ngAfterViewInit() {
  }
  /**
   * delete
   */
  delete() {
    this.property.setValue('');
    this.onChangeOrgId();
    this.onChange.emit(event);
  }
  /**
   * onChange orgId then load org name
   */
  public onChangeOrgId() {
    if (!this.property || CommonUtils.isNullOrEmpty(this.property.value)) {
      if (this.displayName) {
        this.displayName.first.nativeElement.value = '';
      }
      if (this.defaultValue) {
        const market = HrStorage.getSelectedMarket();
        if (!market) {
          return;
        }
        const defaultDomain = market.grantedDomainId;
        if (CommonUtils.isNullOrEmpty(defaultDomain)) {
          return;
        }
        const a = defaultDomain.split(',');
        // thuc hien lay ten don vi de hien thi
        this.service.findOne(a[0])
        .subscribe((res) => {
          const data = res.data;
          if (data) {
            this.displayName.first.nativeElement.value = data.name;
            this.property.setValue(data.organizationId);
          }
        });
      }
      return;
    }
    // thuc hien lay ten don vi de hien thi
    this.service.findOne(this.property.value)
      .subscribe((res) => {
        const data = res.data;
        if (data) {
          this.displayName.first.nativeElement.value = data.name;
        }
      });
  }
  /**
   * ngOnChanges
   */
  ngOnChanges() {
    this.onChangeOrgId();
  }
  /**
   * onFocus
   */
  public onFocus(event) {
    // Sửa bug khi vào 1 form thì sẽ tự động autofocus !
    if ( !event.relatedTarget && !event.sourceCapabilities) {
      return;
    } else {
      this.buttonChose.first.nativeElement.focus();
      this.buttonChose.first.nativeElement.click();
    }
  }
  /**
   * onChose
   */
  public onChose() {
    const modalRef = this.modalService.open(OrgSelectorPayrollModalComponent, DEFAULT_MODAL_OPTIONS);
    modalRef.componentInstance
    .setInitValue({
      operationKey: this.operationKey
      , adResourceKey: this.adResourceKey
      , filterCondition: this.filterCondition
      , rootId: this.rootId
      , checkPermission: this.checkPermission
    });

    modalRef.result.then((node) => {
      if (!node) {
        return;
      }
      this.property.setValue(node.organizationId);
      this.displayName.first.nativeElement.value = node.name;
      // callback on chose item
      this.onChange.emit(node);
    });
  }

}
