import { Component, OnInit, Input, ViewChildren } from '@angular/core';
import { DataPickerService } from '@app/core';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'button-data-picker-modal',
  templateUrl: './button-data-picker-modal.component.html'
})
export class ButtonDataPickerModalComponent implements OnInit {
  @Input()
  params: any;
  form: FormGroup;
  resultList: any = {};
  l10nCodeField: string;
  l10nNameField: string;
  titleChose: string;
  placeholder: string;
  fnSearch;
  @ViewChildren('row') row;
  @ViewChildren('inputSearch') inputSearch;
  constructor(
      private service: DataPickerService
    , private formBuilder: FormBuilder
    , public activeModal: NgbActiveModal) {
      this.buildForm();
    }

  ngOnInit() {

  }
  /**
   * set init value
   */
  setInitValue(params: {
        operationKey: ''
      , adResourceKey: ''
      , filterCondition: ''
      , objectBO: ''
      , codeField: ''
      , nameField: ''
      , orderField: ''
      , selectField: ''
      , nameData: ''
      , systemCode: ''
      , orderDirection: 'ASC'
    }) {
    this.params = params;
    this.titleChose = params.objectBO + '.dataPickerTitle' + ( params.nameData ? '.' + params.nameData : '' );
    this.placeholder = params.objectBO + '.dataPickerPlaceholder' + ( params.nameData ? '.' + params.nameData : '' );
    this.l10nCodeField = params.objectBO + '.' + params.codeField + ( params.nameData ? '.' + params.nameData : '' );
    this.l10nNameField = params.objectBO + '.' + params.nameField + ( params.nameData ? '.' + params.nameData : '' );
    if (params.systemCode) {
      this.service.setSystemCode(params.systemCode);
    } else {
      this.service.setSystemCode('emp');
    }
    this.processSearch(null);
  }
  /**
   * buildForm
   */
  private buildForm(): void {
    this.form = this.formBuilder.group({
      codeInput: [''],
      nationId: [CommonUtils.getNationId()],
    });

    this.form.get('codeInput').valueChanges.subscribe(value => {
      this.processSearchTimeout();
    });
  }
  /**
   * action init ajax
   */
  private actionInitAjax() {
    this.service.actionInitAjax(CommonUtils.getNationId(), this.params)
        .subscribe((res) => {
        });
  }
  /**
   * processSearch
   * @ param event
   */
  public processSearch(event) {
    if (CommonUtils.isValidForm(this.form)) {
      const paramsSearch = this.form.value;
      Object.keys(this.params).forEach(key => {
        paramsSearch[key] = this.params[key] || '';
      });
      this.service.search(paramsSearch, event).subscribe(res => {
        this.resultList = res;
      });
    }
  }
  public processSearchTimeout() {
    if (this.fnSearch) {
      clearTimeout(this.fnSearch);
    }
    this.fnSearch = setTimeout(() => {
      const paramsSearch = this.form.value;
      Object.keys(this.params).forEach(key => {
        paramsSearch[key] = this.params[key] || '';
      });
      this.service.search(paramsSearch).subscribe(res => {
        this.resultList = res;
      });
    }, 1000);
  }
  /**
   * @ param item
   */
  public chose(item) {
    this.activeModal.close(item);
  }

  changeIndex(index) { // tính index của element đang focus
    let currentIndex = -1; // chưa có item nào được focus
    for (let i = 0; i < this.row._results.length; i++) {
      const item = this.row._results[i];
      if (item.nativeElement.classList.contains('datapickerSelected')) {
        item.nativeElement.classList.remove('datapickerSelected');
        currentIndex = i;
        break;
      }
    }
    let nextIndex = currentIndex += index;
    nextIndex = (nextIndex <= 0) ? 0 : (nextIndex >= this.row._results.length ? this.row._results.length - 1 : nextIndex);
    this.row._results[nextIndex].nativeElement.classList.add('datapickerSelected');
  }
  onSelectEnter() {
    for (const item of this.row._results) {
      if (item.nativeElement.className.includes('datapickerSelected')) {
        // item.nativeElement.
        item.nativeElement.firstElementChild.firstElementChild.click();
        this.inputSearch.first.nativeElement.focus();
        return;
      }
    }
    this.processSearch(null);
  }
}
