import { DEFAULT_MODAL_OPTIONS } from '@app/core/app-config';
import { Component, OnInit, Input, ViewChildren, AfterViewInit, OnChanges, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { DataPickerService } from '@app/core';
import { ButtonDataPickerModalComponent } from './button-data-picker-modal/button-data-picker-modal.component';

@Component({
  selector: 'button-data-picker',
  templateUrl: './button-data-picker.component.html'
})
export class ButtonDataPickerComponent  implements OnInit, AfterViewInit, OnChanges  {

  @Input()
  public systemCode: string;
  @Input()
  public property: FormControl;
  @Input()
  public isRequiredField = false;
  @Input()
  public operationKey: string;
  @Input()
  public adResourceKey: string;
  @Input()
  public objectBO: string;
  @Input()
  public codeField: string;
  @Input()
  public nameField: string;
  @Input()
  public orderField: string;
  @Input()
  public orderDirection = 'ASC';
  @Input()
  public selectField: string;
  @Input()
  public filterCondition: string;
  @Input()
  public isDisplayCode: false;
  @Input()
  public nameData: string;
  @Input()
  public disabled = false;
  @Output()
  public onChange: EventEmitter<any> = new EventEmitter<any>();
  @Input()
  public labelButton = false;
  @ViewChildren('displayName')
  public displayName;
  @ViewChildren('buttonChose')
  public buttonChose;

  constructor(
    private modalService: NgbModal
    , private service: DataPickerService
  ) {
  }

  ngOnInit() {
  }
  /**
   * ngAfterViewInit
   */
  ngAfterViewInit() {
  }
  
  /**
   * onFocus
   */
  public onFocus() {
    
  }
  /**
   * onChose
   * param item
   */
  public onChose() {
    const modalRef = this.modalService.open(ButtonDataPickerModalComponent, {
      backdrop: 'static',
    });
    modalRef
      .componentInstance
      .setInitValue({
          operationKey: this.operationKey
        , adResourceKey: this.adResourceKey
        , filterCondition: this.filterCondition
        , objectBO: this.objectBO
        , codeField: this.codeField
        , nameField: this.nameField
        , orderField: this.orderField
        , orderDirection: this.orderDirection
        , selectField: this.selectField
        , nameData: this.nameData
        , systemCode: this.systemCode
      });
    modalRef.result.then((item) => {
      if (!item) {
        return;
      }
      if(this.property){
        this.property.setValue(item.selectField);
      }
      
      // if (this.isDisplayCode) {
      //   this.displayName.first.nativeElement.value = item.codeField;
      // } else {
      //   this.displayName.first.nativeElement.value = item.nameField;
      // }
      // callback on chose item
      this.onChange.emit(item);
    });
  }
  /**
   * delete
   */
  public delete() {
    if(this.property){
      this.property.setValue('');
    }
    this.onChange.emit(event);
  }
  /**
   * ngOnChanges
   */
  ngOnChanges() {
    if (this.systemCode) {
      this.service.setSystemCode(this.systemCode);
    }
  }

}
