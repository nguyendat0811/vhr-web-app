import { BaseControl } from '@app/core/models/base.control';
import { Component, OnInit, ViewChildren, ViewChild, Input, SimpleChanges, SimpleChange, Output, EventEmitter } from '@angular/core';
import { TranslationService } from 'angular-l10n';
import { FormControl } from '@angular/forms';
import { LocationService, NationService } from '@app/core';

@Component({
  selector: 'mobile-number-picker',
  templateUrl: './mobile-number-picker.component.html',
})
export class MobileNumberPickerComponent implements OnInit {

  // Khai báo
  @Input()
  public property: FormControl;
  @Input()
  public propertyNation = new BaseControl();
  @Input()
  public nation: number;
  @Input()
  public disabled: boolean;
  @Input()
  public maxLength: number;
  @ViewChildren('btnItem')
  public btnItems;

  public nationObj: any;
  
  @ViewChildren('inputSearch')
  public inputSearch;
  @ViewChildren('displayName')
  public displayName;
  public nationFilter = '';
  public nationSelected: any;
  @Output() onKeyUp = new EventEmitter<any>();
  @Output()
  public onThisInput: EventEmitter<any> = new EventEmitter<any>();

  listNation: [];
  

  @ViewChild('op') overlayPanel: any;

  //Main
  constructor(
    public translation: TranslationService,
    private nationService: NationService,
  ) {
    this.nationObj = {};
  }

  ngOnChanges(changes: SimpleChanges) {
    const nation: SimpleChange = changes.nation;
    if (nation) {
      this.propertyNation.setValue(nation.currentValue);
    }
  }

  ngOnInit() {
    this.nationService.getNationList()
      .subscribe(res => { this.listNation = res.data });
  }

  public getTitleChooseNation() {
    return this.nationObj.name || this.translation.translate('button.title.selectNation');
  }

  public choseNation(nation) {
    this.nationObj = nation;
    this.propertyNation.setValue(this.nationObj.nationId);
    let phoneAreaCode = '';
    if (this.nationObj.phoneAreaCode) {
      phoneAreaCode += '+' + this.nationObj.phoneAreaCode + ' ';
    }
    this.property.setValue(phoneAreaCode);
  }
  changeIndex(index) { // tính index của element đang focus
    let currentIndex = -1; // chưa có item nào được focus
    for (let i = 0; i < this.btnItems._results.length; i++) {
      const item = this.btnItems._results[i];
      if (item.nativeElement.classList.contains('focus')) {
        item.nativeElement.classList.remove('focus');
        currentIndex = i;
        break;
      }
    }
    if (currentIndex <= -1) { // không tìm thấy item focus
      this.btnItems.first.nativeElement.focus();
      this.btnItems.first.nativeElement.classList.add('focus');
      this.inputSearch.first.nativeElement.focus();
      return;
    }
    let nextIndex = currentIndex += index;
    nextIndex = (nextIndex <= 0) ? 0 : (nextIndex >= this.btnItems._results.length ? this.btnItems._results.length - 1 : nextIndex);
    this.btnItems._results[nextIndex].nativeElement.classList.add('focus');
  }

  focusElement() { // focus vào element để scroll down up
    for (const item of this.btnItems._results) {
      if (item.nativeElement.className.includes('focus')) {
        item.nativeElement.focus();
        this.inputSearch.first.nativeElement.focus();
        return;
      }
    }
  }

  focusInputSearch() {
    setTimeout(() => {
      this.inputSearch.first.nativeElement.focus();
    }, 400);
  }

  choseFocusNation() {
    // chọn element đang focus
    for (const item of this.btnItems._results) {
      if (item.nativeElement.className.includes('focus')) {
        item.nativeElement.click();
        this.inputSearch.first.nativeElement.focus();
        return;
      }
    }
    // đóng pupop
    for (const item of this.btnItems._results) {
      if (item.nativeElement.className.includes('selected')) {
        item.nativeElement.click();
        return;
      }
    }
  }

  searchNationByCode(event) {
    const value = event.target.value;
    // Kiểm tra có phải mã vùng không (phải có định dạng +n)
    if(value.indexOf('+') >= 0 && value.indexOf(' ') >= 0) { 
      const data0 = value.trim().split('+');
      const data1 = data0[1].split(' ');
      // trường hợp không nhập hoặc nhập quá 5 ký tự sẽ ko xử lý tiếp tìm kiếm icon quốc gia
      if(!data1[0] || data1[0].length > 4 ) {
        this.nationObj = {};
        return;
      }
      for (let nation of this.listNation) {
        let nationNew = {nationId : '', phoneAreaCode : '', icon : ''};
        nationNew = nation;
        const phoneAreaCode = nationNew.phoneAreaCode;
        if (data1[0] === phoneAreaCode) {
          this.nationObj = nation;
          return;
        } else {
          this.nationObj = {};
        }
      }
    } else {
      this.nationObj = {};
    }
  }
}
