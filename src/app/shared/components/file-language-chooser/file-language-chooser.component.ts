import { FileControl, FileAttachment } from '@app/core/models/file.control';
import { Component, OnInit, Input, Output, EventEmitter, ViewChild, OnChanges } from '@angular/core';
import { BaseControl } from '@app/core/models/base.control';
import { SysLanguageService } from '@app/core/services/sys-language/sys-language.service';
import { CommonUtils } from '@app/shared/services';
import { AppComponent } from '@app/app.component';
import { FileStorageService } from '@app/core/services/file-storage.service';
import { TranslationService } from 'angular-l10n';
import { HrStorage } from '@app/core/services/HrStorage';
import { saveAs } from 'file-saver';

@Component({
  selector: 'file-language-chooser',
  templateUrl: './file-language-chooser.component.html',
  styles: []
})
export class FileLanguageChooserComponent implements OnInit, OnChanges {
  @Input()
  property: FileControl;
  @Input()
  private validMaxSize = -1;
  @Input()
  private validType: string;
  @Input()
  private maxFile = -1;
  @Output()
  public onFileChange: EventEmitter<any> = new EventEmitter<any>();
  public emptyFile = new File([''], '-', {type: 'vhr_stored_file'});

  @ViewChild('op') overlayPanel: any;
  listLanguage: [];
  lang: string;
  constructor(private app: AppComponent
            , private fileStorage: FileStorageService
            , private translation: TranslationService) {
    this.lang = CommonUtils.getCurrentLanguage().code;
    this.listLanguage = HrStorage.getListLang();
  }


  ngOnInit() {
    // tạo giá trị mặc định cho fileAttachment

    if (this.property) {
      if (!this.property.fileAttachment || this.property.fileAttachment.length === 0) {
        this.property.fileAttachment = [this.makeEmptyFileAttach()];
      } else {
        for (const fileAttr of this.property.fileAttachment ) {
          if (!fileAttr.isTemp) {
            fileAttr.file = this.emptyFile;
            fileAttr.languageObj = this.findLanguage(fileAttr.sysLanguageCode);
          }
        }
        this.onFileChanged();
      }
    }
  }
  /**
   * ngOnChanges
   */
  ngOnChanges() {
    this.ngOnInit();
  }
  public chooseLanguage(fileAttr, language) {
    fileAttr.languageObj = language;
    fileAttr.sysLanguageCode = language.code;
    this.onFileChanged();
  }
  private isValidLanguage(fileAttr): boolean {
    if (!fileAttr.sysLanguageCode) {
      fileAttr.errorKey = 'controls.requiredLanguage';
      return false;
    }
    if (this.property.fileAttachment && this.property.fileAttachment.length > 0) {
      let _countLang = 0;
      for (const item of this.property.fileAttachment) {
        if (item.sysLanguageCode === fileAttr.sysLanguageCode) {
          _countLang ++ ;
        }
      }
      if (_countLang > 1) {
        fileAttr.errorKey = 'controls.duplicateLanguage';
        return false;
      }
    }
    fileAttr.errorKey = null;
    return true;
  }
  /** */
  /**
   * onChange
   */
  public onChange(event, fileAttr: FileAttachment) {
    // const files = this.multiple ? event.target.files : event.target.files[0];
    const file = event.target.files[0];
    fileAttr.target = event.target;
    if (!this.isValidFile(file, fileAttr)) {
      fileAttr.fileName = this.translation.translate('controls.noFile');
      fileAttr.target.value = '';
      return;
    } else {
      this.setFileTemp(fileAttr, file);
      this.onFileChanged();
    }
  }
  /**
   * setFileTemp
   */
  private setFileTemp(fileAttr, file) {
    fileAttr.file = file;
    if (file === null) {
      fileAttr.fileName = this.translation.translate('controls.noFile');
      if (fileAttr.target) {
        fileAttr.target.value = '';
      }
    } else {
      fileAttr.fileName = file.name;
    }
  }
  /**
   * onFileChanged
   */
  public onFileChanged() {
    const files = [];
    for (const fileAttr of this.property.fileAttachment ) {
      if ( fileAttr.file && this.isValidLanguage(fileAttr)) {
        files.push(fileAttr.file);
      }
    }
    if (files.length > 0) {
      this.property.setValue( files );
      this.onFileChange.emit(files);
      this.property.setErrors(null);
  } else {
      this.property.setValue(null);
      this.onFileChange.emit(null);
    }
  }
  /**
   * delete
   */
  public delete(fileAttr: FileAttachment) {
    if (fileAttr.isTemp === false || fileAttr.isTemp === undefined) {
      this.app.confirmDelete('common.message.confirm.deleteFile', () => {// on accepted
        this.fileStorage.deleteFile(fileAttr.id)
        .subscribe(res => {
          if (this.fileStorage.requestIsSuccess(res)) {
            this.setFileTemp(fileAttr, null);
            fileAttr.id = null;
            fileAttr.length = null;
            fileAttr.chunkSize = null;
            fileAttr.uploadDate = null;
            fileAttr.file = null;
            fileAttr.isTemp = true;
            this.onFileChanged();
          }
        });
      }, () => {// on rejected

      });
    } else {
      this.setFileTemp(fileAttr, null);
      this.onFileChanged();
    }
  }
  /**
   * isValidFile
   */
  public isValidFile(file, fileAttr): boolean {
    if (!file) {
      return true;
    }
    if (this.validMaxSize > 0) {
      if (CommonUtils.tctGetFileSize(file) > this.validMaxSize) {
        fileAttr.errorKey = 'controls.maxFileSize';
        return false;
      }
    }
    if (!CommonUtils.isNullOrEmpty(this.validType)) {
      const fileName = file.name;
      const temp = fileName.split('.');
      const ext = temp[temp.length - 1].toLowerCase();
      const ruleFile = ',' + this.validType;
      if (ruleFile.toLowerCase().indexOf(ext) === -1) {
        fileAttr.errorKey = 'controls.fileType';
        return false;
      }
    }
    fileAttr.errorKey = null;
    return true;
  }
  /**
   * downloadFile
   */
  public downloadFile(file) {
    this.fileStorage.downloadFile(file.id)
        .subscribe(res => {
          saveAs(res, file.fileName);
        });
  }

  /**
   * makeEmptyFileAttach
   */
  private makeEmptyFileAttach(): FileAttachment {
    const _file = {
      id: null,
      fileName: this.translation.translate('controls.noFile'),
      length: null,
      chunkSize: null,
      uploadDate: null,
      isTemp: true,
      file: null,
      target: null,
      errorKey: null,
      sysLanguageCode: null,
      languageObj: null
    };
    // Do it later...
    // if (!this.property || !this.property.fileAttachment || this.property.fileAttachment.length === 0) {
    //   _file.sysLanguageCode = CommonUtils.getCurrentLanguage().code;
    //   _file.languageObj = this.findLanguage(_file.sysLanguageCode);
    // }
    return _file;
  }
  private findLanguage(sysLanguageCode) {
    return this.listLanguage.find(x => x['code'] === sysLanguageCode);
  }
  /**
   * addItem
   */
  public addItem(item) {
    if (this.maxFile > 0 && this.property.fileAttachment.length >= this.maxFile) {
      return;
    }
    this.property.fileAttachment.push(this.makeEmptyFileAttach());
    this.onFileChanged();
  }
  /**
   * removeItem
   */
  public removeItem(item) {
    this.property.fileAttachment.splice(this.property.fileAttachment.indexOf(item), 1);
    if (this.property.fileAttachment.length === 0) {
      this.property.fileAttachment.push(this.makeEmptyFileAttach());
    }
    this.onFileChanged();
  }
}
