import { Component, OnInit, Input, ViewChildren, Output, EventEmitter, SimpleChanges, SimpleChange } from '@angular/core';
import { BaseControl } from '@app/core/models/base.control';
import { SelectItem } from 'primeng/api';
import { TranslationService } from 'angular-l10n';
import { NationService, LocationService } from '@app/core';

@Component({
  selector: 'province-select-filter',
  templateUrl: './province-select-filter.component.html'
})
export class ProvinceSelectFilterComponent implements OnInit {

  @Input()
  public property: BaseControl;
  @Input()
  public isMultiLanguage: boolean;
  @Input()
  public placeHolder: string;
  @Input()
  public disabled = false;
  @Input()
  public autofocus = '';
  @Input()
  public appendTo = '';
  // Start fix request select by nation
  public nationSelected: any;
  public listNation;
  public nationFilter = '';
  @ViewChildren('btnItem')
  public btnItems;
  @ViewChildren('inputSearch')
  public inputSearch;
  // End
  emptyFilterMessage: any;
  selectedValue: any;
  checkShowClear = false;
  listData: SelectItem[];
  @Output()
  public onChange: EventEmitter<any> = new EventEmitter<any>();
  constructor(
    public translation: TranslationService
    , private nationService: NationService
    , private locationService: LocationService
  ) {
    this.listData = [];
    this.nationSelected = {};
    this.emptyFilterMessage = this.translation.translate('selectFilter.emptyFilterMessage');
  }

  ngOnChanges(changes: SimpleChanges) {
    const property: SimpleChange = changes.property;
    if (property && property.currentValue) {
      this.initNationFlag(property.currentValue);
    }
    this.selectedValue = this.property.value;
    this.checkShowClear = (this.selectedValue === null || this.selectedValue === '') ? false : true;
    this.validateSeletedValue();
  }

  ngOnInit() {
    this.property.valueChanges.subscribe(
      (value) => {
        this.selectedValue = value;
        this.checkShowClear = (this.selectedValue === null || this.selectedValue === '') ? false : true;
      }
    );
    this.initNationConfig();
  }
  selectedChange() {
    this.checkShowClear = (this.selectedValue === null || this.selectedValue === '') ? false : true;
    this.property.setValue(this.selectedValue);
    this.onChange.emit(this.selectedValue);
  }
  // Start fix by nation
  choseNation(nation) {
    if (this.disabled) {
      return;
    }
    this.nationSelected = nation;
    this.locationService.getProvincesByNationId(nation.nationId).subscribe(
      res => {
        this.listData = [];
        for (const item of res) {
          this.listData.push({label: item.name, value: item.locationId});
        }
      }
    );
  }
  initNationConfig(nationConfigObj?) {
    if (!this.listNation || this.listNation.length === 0) {
      this.nationService.getNationList().subscribe(
        res => {
          this.listNation = res.data;
          if (nationConfigObj) {
            for (const nation of this.listNation) {
              if (nation.nationId === nationConfigObj.nationId) {
                this.nationSelected = nation;
              }
            }
          }
        }
      );
    } else {
      if (nationConfigObj) {
        for (const nation of this.listNation) {
          if (nation.nationId === nationConfigObj.nationId) {
            this.nationSelected = nation;
          }
        }
      }
    }
    if (!nationConfigObj) {
      return;
    }
    this.locationService.getProvincesByNationId(nationConfigObj.nationId).subscribe(
      res => {
        this.listData = [];
        for (const item of res) {
          this.listData.push({label: item.name, value: item.locationId});
        }
      }
    );
  }
  initNationFlag(property) {
    if (!property || !property.value ) {
      this.nationSelected = {};
      return;
    }
    if (property.value) {
      this.locationService.findOne(property.value).subscribe(
        res => {
          this.initNationConfig(res.data);
        }
      )
    }
  }
  changeIndex(index) { // tính index của element đang focus
    let currentIndex = -1; // chưa có item nào được focus
    for (let i = 0; i < this.btnItems._results.length; i++) {
      const item = this.btnItems._results[i];
      if (item.nativeElement.classList.contains('focus')) {
        item.nativeElement.classList.remove('focus');
        currentIndex = i;
        break;
      }
    }
    if (currentIndex <= -1) { // không tìm thấy item focus
      this.btnItems.first.nativeElement.focus();
      this.btnItems.first.nativeElement.classList.add('focus');
      this.inputSearch.first.nativeElement.focus();
      return;
    }
    let nextIndex = currentIndex += index;
    nextIndex = (nextIndex <= 0) ? 0 : (nextIndex >= this.btnItems._results.length ? this.btnItems._results.length - 1 : nextIndex);
    this.btnItems._results[nextIndex].nativeElement.classList.add('focus');
  }
  focusElement() { // focus vào element để scroll down up
    for (const item of this.btnItems._results) {
      if (item.nativeElement.className.includes('focus')) {
        item.nativeElement.focus();
        this.inputSearch.first.nativeElement.focus();
        return;
      }
    }
  }
  choseFocusNation() {
    // chọn element đang focus
    for (const item of this.btnItems._results) {
      if (item.nativeElement.className.includes('focus')) {
        item.nativeElement.click();
        this.inputSearch.first.nativeElement.focus();
        return;
      }
    }
    // đóng pupop
    for (const item of this.btnItems._results) {
      if (item.nativeElement.className.includes('selected')) {
        item.nativeElement.click();
        return;
      }
    }
  }
  focusInputSearch() {
    setTimeout(() => {
      this.inputSearch.first.nativeElement.focus();
    }, 400);
  }
  // Validate gia tri combobox co ton tai hay khong
  private validateSeletedValue() {
    if (!this.selectedValue && this.listData.length === 0) {
      return;
    }

    if (this.listData.length > 0) {
      const obj = this.listData.find(x => x.value === this.selectedValue);
      if (!obj) {
        this.checkShowClear = false;
        this.property.setValue(null);
        this.onChange.emit(null);
      }
    }
  }
}
