import { HrStorage } from './../../../core/services/HrStorage';
import { Component, OnInit, Input } from '@angular/core';
import { TranslationService } from 'angular-l10n';
import { EmpSystemConfigService } from '@app/core/services/sys-language/emp-system-config.service';

@Component({
  selector: 'system-intro',
  templateUrl: './system-intro.component.html'
})
export class SystemIntroComponent implements OnInit {
  @Input()
  screenCode: string;
  constructor(public translation: TranslationService
            , public empSystemConfigService: EmpSystemConfigService) {
  }

  ngOnInit() {
    setTimeout(() => {
      if (!HrStorage.introIsDone(this.screenCode)) {
        this.startTour();
      }
    }, 3000);
  }
  startTour() {
    const IntroJs = require('../../../../../node_modules/intro.js/intro.js');
    const intro = IntroJs();
    // Initialize steps
    intro.setOptions({
      steps: this.CONFIG[this.screenCode]
    });

    // Start tutorial
    intro.start();
    intro.oncomplete(() => {
      const submitData = {};
      submitData['screenCode'] = this.screenCode;
      this.empSystemConfigService.saveIntro(submitData).subscribe(
        res => {}
      );
      HrStorage.introSetDone(this.screenCode);
    });
  }
  // tslint:disable-next-line:member-ordering
  private CONFIG = {
    ORG_PLAN_ADD_NEW : [ // lập kế hoạch đơn vị - loại tạo mới kế hoạch
      {
        intro: this.translation.translate('intro.orgPlan.intro'),
        position: 'left'
      }, {
        element: '#btnCloneTree',
        intro: this.translation.translate('intro.orgPlan.cloneOrg'),
        position: 'right'
      }, {
        element: '#btnImportTree',
        intro: this.translation.translate('intro.orgPlan.importOrg'),
        position: 'right'
      }, {
        element: '#btnSaveTree',
        intro: this.translation.translate('intro.orgPlan.saveOrg'),
        position: 'right'
      }, {
        intro: this.translation.translate('intro.thanks'),
        position: 'right'
      },
    ],
    ORG_PLAN_RENAME : [ // lập kế hoạch đơn vị - loại kế hoạch đổi tên
      {
        intro: this.translation.translate('intro.orgPlan.intro'),
        position: 'left'
      }, {
        element: '#btnCloneTreeChangeName',
        intro: this.translation.translate('intro.orgPlan.selectOrgChangeName'),
        position: 'right'
      }, {
        element: '#btnSaveTreeChangeName',
        intro: this.translation.translate('intro.orgPlan.saveOrg'),
        position: 'right'
      }, {
        intro: this.translation.translate('intro.thanks'),
        position: 'right'
      },
    ],
    PERSONAL_INFORMATION : [
      {
        intro: this.translation.translate('intro.employee.intro'),
        position: 'left'
      }, {
        element: '#settingsTabs',
        intro: this.translation.translate('intro.employee.settingTab'),
        position: 'right'
      }, {
        element: '#changeEmployee',
        intro: this.translation.translate('intro.employee.changeEmployee'),
        position: 'right'
      }, {
        element: '#tranferEmployee',
        intro: this.translation.translate('intro.employee.tranferEmployee'),
        position: 'right'
      }, {
        element: '#leaveJobEmployee',
        intro: this.translation.translate('intro.employee.leaveJobEmployee'),
        position: 'right'
      }, {
        element: '#debtEmployee',
        intro: this.translation.translate('intro.employee.debtEmployee'),
        position: 'right'
      }, {
        intro: this.translation.translate('intro.thanks'),
        position: 'right'
      },
    ],
    ORGANIZATION_MANAGE : [
      {
        intro: this.translation.translate('intro.organization.intro'),
        position: 'left'
      }, {
        element: '#showByLineOrg',
        intro: this.translation.translate('intro.organization.showByLineOrg'),
        position: 'right'
      }, {
        element: '#showOrgExpried',
        intro: this.translation.translate('intro.organization.showExpiredOrg'),
        position: 'right'
      }, {
        element: '#orgTree',
        intro: this.translation.translate('intro.organization.dropButton'),
        position: 'right'
      }, {
        element: '#orgTree',
        intro: this.translation.translate('intro.organization.clickRightMouse'),
        position: 'right'
      }, {
        element: '#orgTree',
        intro: this.translation.translate('intro.organization.clickLeftMouse'),
        position: 'right'
      }, {
        element: '#orgTable',
        intro: this.translation.translate('intro.organization.orgTable'),
        position: 'right'
      }, {
        intro: this.translation.translate('intro.thanks'),
        position: 'right'
      },
    ],
    SYS_CAT : [
      {
        intro: this.translation.translate('intro.sysCat.intro'),
        position: 'left'
      }, {
        element: '#addSysCatType',
        intro: this.translation.translate('intro.sysCat.addSysCatType'),
        position: 'right'
      }, {
        element: '#searchSysCatType',
        intro: this.translation.translate('intro.sysCat.searchSysCatType'),
        position: 'right'
      }, {
        element: '#sysCatTypeTable',
        intro: this.translation.translate('intro.sysCat.clickSysCatType'),
        position: 'right'
      }, {
        element: '#sysCatTable',
        intro: this.translation.translate('intro.sysCat.sysCatTable'),
        position: 'left'
      }, {
        element: '#editSysCatType',
        intro: this.translation.translate('intro.sysCat.editSysCatType'),
        position: 'right'
      }, {
        element: '#deleteSysCatType',
        intro: this.translation.translate('intro.sysCat.deleteSysCatType'),
        position: 'right'
      }, {
        element: '#transfer',
        intro: this.translation.translate('intro.sysCat.transferSysCatType'),
        position: 'right'
      }, {
        element: '#addSysCat',
        intro: this.translation.translate('intro.sysCat.addSysCat'),
        position: 'left'
      }, {
        element: '#searchSysCat',
        intro: this.translation.translate('intro.sysCat.searchSysCat'),
        position: 'left'
      }, {
        element: '#editSysCat',
        intro: this.translation.translate('intro.sysCat.editSysCat'),
        position: 'right'
      }, {
        element: '#deleteSysCat',
        intro: this.translation.translate('intro.sysCat.deleteSysCat'),
        position: 'right'
      }, {
        intro: this.translation.translate('intro.thanks'),
        position: 'right'
      },
    ],
    NATION_CONFIG : [
      {
        intro: this.translation.translate('intro.sysCat.intro'),
        position: 'left'
      }, {
        element: '#addNationConfigType',
        intro: this.translation.translate('intro.sysCat.addSysCatType'),
        position: 'right'
      }, {
        element: '#searchNationConfigType',
        intro: this.translation.translate('intro.sysCat.searchSysCatType'),
        position: 'right'
      }, {
        element: '#nationConfigTypeTable',
        intro: this.translation.translate('intro.sysCat.clickSysCatType'),
        position: 'right'
      }, {
        element: '#nationConfigTable',
        intro: this.translation.translate('intro.sysCat.sysCatTable'),
        position: 'left'
      }, {
        element: '#editNationConfigType',
        intro: this.translation.translate('intro.sysCat.editSysCatType'),
        position: 'right'
      }, {
        element: '#deleteNationConfigType',
        intro: this.translation.translate('intro.sysCat.deleteSysCatType'),
        position: 'right'
      }, {
        element: '#addNationConfig',
        intro: this.translation.translate('intro.sysCat.addSysCat'),
        position: 'left'
      }, {
        element: '#searchNationConfig',
        intro: this.translation.translate('intro.sysCat.searchSysCat'),
        position: 'left'
      }, {
        element: '#editNationConfig',
        intro: this.translation.translate('intro.sysCat.editSysCat'),
        position: 'right'
      }, {
        element: '#deleteNationConfig',
        intro: this.translation.translate('intro.sysCat.deleteSysCat'),
        position: 'right'
      }, {
        intro: this.translation.translate('intro.thanks'),
        position: 'right'
      },
    ],
  };
}
