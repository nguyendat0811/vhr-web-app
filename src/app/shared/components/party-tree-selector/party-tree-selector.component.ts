import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { OrgSelectorService } from '@app/core';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { TreeNode, MenuItem } from 'primeng/api';
import { TranslationService } from 'angular-l10n';
import { ContextMenu } from 'primeng/contextmenu';
import { element } from '@angular/core/src/render3';

@Component({
  selector: 'party-tree-selector',
  templateUrl: './party-tree-selector.component.html',
  styleUrls: ['./party-tree-selector.component.css']
})

export class PartyTreeSelectorComponent implements OnInit {
  public nodes: TreeNode[];
  public selectedNode: TreeNode[];
  public isShowContextMenu = true;
  showOrgExpried = false;
  showOrgByLineOrg = false;
  items: MenuItem[];
  @Input()
  public onNodeSelect: Function;
  @Input()
  public height: string;
  @Input()
  public rootId: string;
  @Output()
  public treeSelectNode: EventEmitter<TreeNode> = new EventEmitter<TreeNode>();
  @Output()
  public nodeContextMenuBuilder: EventEmitter<TreeNode> = new EventEmitter<TreeNode>();
  @Output()
  public nodeContextMenuBuilder2: EventEmitter<TreeNode> = new EventEmitter<TreeNode>();
  @Output()
  public onSelectedNodeChange: EventEmitter<any> = new EventEmitter<any>();
  showCm2 = true;
  @Input()
  public operationKey: string;
  @Input()
  public adResourceKey: string;
  @Input()
  public filterCondition: string;
  @Input()
  public requestId: Number;
  @Input()
  public allowShowLineOrg = true;
  @ViewChild("cm2")
  cm2: ContextMenu;

  constructor( private service: OrgSelectorService, private translation: TranslationService) {
  }

  /**
   * ngOnInit
   */
  ngOnInit() {

    if (!this.showOrgByLineOrg) {
      this.actionInitAjax();
    }
    this.height = this.height || '800';
  }
  /**
   * getParams
   */
  private getParams() {
    return { operationKey: this.operationKey,
             adResourceKey: this.adResourceKey,
             filterCondition: (this.filterCondition ? this.filterCondition : ''),
             nodeId: '',
             rootId: this.rootId,
             showOrgExpried: this.showOrgExpried
           };
  }
  /**
   * set root node
   */
  public setRootNode(rootId: string) {
    this.rootId = rootId;
    this.actionInitAjax();
  }
  /**
   * action init ajax
   */
  private actionInitAjax() {
    this.service.actionInitAjax(this.getParams())
        .subscribe((res) => {
          this.nodes = CommonUtils.toTreeNode(res);
        });
  }
  /**
   * actionLazyRead
   * @ param event
   */
  public actionLazyRead(event) {
    const params = this.getParams();
    params.nodeId = event.node.nodeId;
    if(event.node.children && event.node.children.length > 0){
      return;
    }
    var isSelected = false;
    if(this.selectedNode && this.selectedNode.length >0){
      this.selectedNode.forEach(element => {
        if(element.data == params.nodeId){
          isSelected = true;
        }
      });
    }


    this.service.actionLazyRead(params)
        .subscribe((res) => {
          if(isSelected){
            this.selectedNode = this.selectedNode.concat(res);
          }
          event.node.children = CommonUtils.toTreeNode(res);

          // Them su kien khi co su thay doi selectedNode
          this.onSelectedNodeChange.emit(1);
        });
  }

  /**
   * actionLazyRead
   * @ param event
   */
  public actionLazyReadWhenAddChild(nodeCurrent, node) {
    const params = this.getParams();
    params.nodeId = nodeCurrent.nodeId;
    this.service.actionLazyRead(params)
        .subscribe((res) => {
          nodeCurrent.children = CommonUtils.toTreeNode(res);
          nodeCurrent.children.push(node);
          nodeCurrent.expanded = true;
        });
  }
  /**
   * nodeSelect
   * @ param event
   */
  public nodeSelect(event) {
    // TH neu click vao nganh tren cay don vi nganh doc thi khong su ly gi
    if (event.node.lineOrg) {
      return;
    }

    this.treeSelectNode.emit(event);
    // Them su kien khi co su thay doi selectedNode
    this.onSelectedNodeChange.emit(1);
  }
// for context menu
  /**
  * action create Context Menu
  */
  nodeContextMenuSelect(event) {
    this.nodeContextMenuBuilder.emit(event.node);
  }

  // Xu ly hien thi cay don vi theo nganh doc

  /**
   * action init ajax by line org
   */
  private actionInitAjaxByLineOrg() {
    this.selectedNode=[];
    this.service.actionInitAjaxByLineOrg(this.getParams())
        .subscribe((res) => {
          this.nodes = CommonUtils.toTreeNode(res);
        });
  }
  /**
   * actionLazyRead by line org
   * @ param event
   */
  public actionLazyReadByLineOrg(event) {
    if (!event.node.parent) {
      this.actionInitAjaxByLineOrg();
      return;
    }
    const params = this.getParams();
    params.nodeId = event.node.nodeId;
    params['lineOrg'] = event.node.lineOrg;
    params['lineOrgId'] = event.node.lineOrgId;
    params['orgLevelManage'] = event.node.orgLevelManage;
    params['lineOrgRootId'] = event.node.lineOrgRootId;
    this.service.actionLazyReadByLineOrg(params)
        .subscribe((res) => {
          event.node.children = CommonUtils.toTreeNode(res);
        });
  }

  // for context menu
  /**
  * action create Context Menu
  */
  nodeContextMenuSelect2(event) {
    if (event.node.lineOrg) {
      this.cm2.hide();
      return;
    }
    this.cm2.show();
    this.nodeContextMenuBuilder2.emit(event.node);
  }

  showOrgByLineOrgChange(e) {
    if (!this.showOrgByLineOrg) {
      this.actionInitAjax();
    } else {
      this.actionInitAjaxByLineOrg();
    }
  }

  onNodeCollapse(e) {
    // Them su kien khi co su thay doi selectedNode
    this.onSelectedNodeChange.emit(1);
  }
}



