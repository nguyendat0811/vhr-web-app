import { DisplayDateMonthPipe } from './pipes/display-date-month.pipe';
import { MultiSelectModule } from 'primeng/multiselect';
import { MenuTreeComponent } from './components/menu-tree/menu-tree.component';
import { PropertyResolver } from './services/property.resolver';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { LayoutModule } from './layout/layout.module';
import { TableModule } from 'primeng/table';
import { PaginatorModule } from 'primeng/paginator';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService } from 'primeng/api';
import { TreeModule } from 'primeng/tree';
import { CalendarModule } from 'primeng/calendar';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { DialogModule } from 'primeng/dialog';
import { ContextMenuModule } from 'primeng/contextmenu';
import { StepsModule } from 'primeng/steps';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { TabMenuModule } from 'primeng/tabmenu';
import { TabViewModule } from 'primeng/tabview';
import { DropdownModule } from 'primeng/dropdown';
import { FieldsetModule } from 'primeng/fieldset';
import { SelectButtonModule } from 'primeng/selectbutton';
import { OrganizationChartModule } from 'primeng/organizationchart';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { PickListModule } from 'primeng/picklist';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { OrderListModule } from 'primeng/orderlist';
import { CheckboxModule } from 'primeng/checkbox';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { ProgressBarModule } from 'primeng/progressbar';
import { SplitButtonModule } from 'primeng/splitbutton';

import { InternationalPhoneModule } from 'ng4-intl-phone';

import { ControlMessagesComponent } from './components/control-messages/control-messages.component';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { HavePermissionComponent } from './components/have-permission/have-permission.component';
import { HaveNotPermissionComponent } from './components/have-not-permission/have-not-permission.component';
import { OrgSelectorComponent } from './components/org-selector/org-selector.component';

import { TranslationModule } from 'angular-l10n';
import { OrgSelectorModalComponent } from './components/org-selector/org-selector-modal/org-selector-modal.component';
import { SelectFilterComponent } from './components/select-filter/select-filter.component';
import { DataPickerComponent } from './components/data-picker/data-picker.component';
import { DataPickerModalComponent } from './components/data-picker/data-picker-modal/data-picker-modal.component';
import { DatePickerComponent } from './components/date-picker/date-picker.component';
import { OrgTreeComponent } from './components/org-tree/org-tree.component';
import { LocationPickerComponent } from './components/location-picker/location-picker.component';
import { DataNationComponent } from './components/location-picker/data-nation.component';
import { FileChoserComponent } from './components/file-choser/file-choser.component';
import { DisplayHelperPipe } from './pipes/display-helper.pipe';
import { DynamicInputComponent } from './components/dynamic-input/dynamic-input.component';
import { InputSpecialDirective } from './directive/input-special.directive';
import { InputTrimDirective } from './directive/input-trim.directive';
import { MultiLangInputComponent } from './components/multi-lang-input/multi-lang-input.component';
// import { BaseComponent } from './components/base-component/base-component.component';
import { MultilFileChoserComponent } from './components/file-choser/multil-file-choser.component';
import { AutoFocusDirective } from './directive/auto-focus.directive';
import { RemoveWrapperDirective } from './directive/remove-wrapper';
import { DisplayDatePipe } from './pipes/display-date.pipe';
import { ButtonBackComponent } from './components/button-back/button-back.component';
// import { DiagramAllModule, SymbolPaletteAllModule, OverviewAllModule } from '@syncfusion/ej2-ng-diagrams';
import { ColorPickerModule } from 'primeng/colorpicker';
// import { DiagramModule } from '@syncfusion/ej2-ng-diagrams';

import { MultiSelectFilterComponent } from './components/multi-select-filter/multi-select-filter.component';

import { CalendarMonthComponent } from './components/calendar-month/calendar-month.component';
// import { FullCalendarModule } from 'primeng/fullcalendar';

import { LocationPickerModalComponent } from './components/location-picker/location-picker-modal/location-picker-modal.component';

// start thanhlq6 bo sung gon song khi click
// Doanvd turning ko su dung RippleModule
// import { RippleModule } from '@progress/kendo-angular-ripple';
import { PayrollCalendarComponent } from './components/payroll-calendar/payroll-calendar.component';
import { DiagramDashboardDialogComponent } from './components/diagram-dashboard/diagram-dashboard-dialog/diagram-dashboard-dialog.component';
// end thanhlq6

import { ImportErrorComponent } from './components/import-error/import-error.component';
import { HasPermissionDirective } from './directive/has-permission.directive';
import { HasNotPermissionDirective } from './directive/has-not-permission.directive';
import { FormatCurrencyPipe, FindByPipe } from './pipes/format-currency.pipe';
import { PayrollCalendarSaturdayComponent } from './components/payroll-calendar-saturday/payroll-calendar-saturday.component';
import { AttendanceTypeTableComponent } from './components/attendance-type-table/attendance-type-table.component';
import { TableFooterComponent } from './components/table-footer/table-footer.component';
import { FileLanguageChooserComponent } from './components/file-language-chooser/file-language-chooser.component';
import { ProvinceSelectFilterComponent } from './components/province-select-filter/province-select-filter.component';
import { SystemIntroComponent } from './components/system-intro/system-intro.component';
import { WorkScheduleCalendarComponent } from './components/work-schedule-calendar/work-schedule-calendar.component';
import { AutoCompleteComponent } from './components/auto-complete/auto-complete.component';
import { OrgMultiSelectorComponent } from './components/org-multi-selector/org-multi-selector.component';
import { OrgMultiSelectorModalComponent } from './components/org-multi-selector/org-multi-selector-modal/org-multi-selector-modal.component';
import { MobileNumberPickerComponent } from './components/mobile-number-picker/mobile-number-picker.component';
import { FormatCurrencyDirective } from '@app/shared/directive/format-currency.directive';
import { OrgSelectorPayrollComponent } from './components/org-selector-payroll/org-selector-payroll.component';
import { OrgSelectorPayrollModalComponent } from './components/org-selector-payroll/org-selector-payroll-modal/org-selector-payroll-modal.component';

//start GiangTH
import { InputSwitchModule } from 'primeng/inputswitch';
import { ButtonOrgSelectorComponent } from './components/button-org-selector/button-org-selector.component';
import { ButtonOrgSelectorModalComponent } from './components/button-org-selector/button-org-selector-modal/button-org-selector-modal.component';
//end GiangTH
import { InputTextComponent } from '@app/modules/reports/employee-list/entry-components/input-text.component';
import { InputComboboxComponent } from '@app/modules/reports/employee-list/entry-components/input-combobox.component';
import { InputDateRangeComponent } from '@app/modules/reports/employee-list/entry-components/input-date-range.component';
import { InputMultiComboboxComponent } from '@app/modules/reports/employee-list/entry-components/input-multi-selectbox.component';
import { InputNumberRangeComponent } from '@app/modules/reports/employee-list/entry-components/input-number-range.component';
import { MultiSelectAreaComponent } from './components/multi-select-area/multi-select-area.component';
import { PartyTreeSelectorComponent } from './components/party-tree-selector/party-tree-selector.component';
import { OrgMultiSelectorPayrollComponent } from './components/org-multi-selector-payroll/org-multi-selector-payroll.component';
import { ButtonDataPickerComponent } from './components/button-data-picker/button-data-picker.component';
import { ButtonDataPickerModalComponent } from './components/button-data-picker/button-data-picker-modal/button-data-picker-modal.component';
import { OrgMultiSelectorPayrollModalComponent } from './components/org-multi-selector-payroll/org-multi-selector-modal-payroll/org-multi-selector-payroll-modal.component';
import { MessageDialogComponent } from './components/message-dialog/message-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    /**
     * prime
     */
    LayoutModule,
    TableModule,
    PaginatorModule,
    ConfirmDialogModule,
    TreeModule,
    CalendarModule,
    ScrollPanelModule,
    StepsModule,
    BreadcrumbModule,
    TabMenuModule,
    DropdownModule,
    TabViewModule,
    FieldsetModule,
    ContextMenuModule,
    ColorPickerModule,
    MultiSelectModule,
    SplitButtonModule,
    // FullCalendarModule,
    /**
     * EJ2
     */
    // DiagramAllModule,
    // SymbolPaletteAllModule,
    // OverviewAllModule,
    // DiagramModule,
    /**
     * Others
     */
    OrganizationChartModule,
    InternationalPhoneModule,
    TranslationModule,
    NgbModule.forRoot(),
    OverlayPanelModule,
    PickListModule,
    // Doanvd turning ko su dung RippleModule
    // RippleModule,
    DragDropModule,
    OrderListModule,
    CheckboxModule,
    AutoCompleteModule,
    ProgressBarModule,
    DialogModule,
    InputSwitchModule,
  ],
  providers: [
    ConfirmationService,
    PropertyResolver,
    FormatCurrencyPipe
  ],
  declarations: [
    ControlMessagesComponent,
    SpinnerComponent,
    HavePermissionComponent,
    HaveNotPermissionComponent,
    OrgSelectorComponent,
    SystemIntroComponent,
    OrgSelectorModalComponent,
    SelectFilterComponent,
    DataPickerComponent,
    DataPickerModalComponent,
    DatePickerComponent,
    PayrollCalendarComponent,
    OrgTreeComponent,

    LocationPickerComponent,
    DataNationComponent,

    FileChoserComponent,
    MultilFileChoserComponent,
    DisplayHelperPipe,
    MenuTreeComponent,
    DynamicInputComponent,
    InputSpecialDirective,
    InputTrimDirective,
    MultiLangInputComponent,
    // BaseComponent,
    AutoFocusDirective,
    RemoveWrapperDirective,
    DisplayDatePipe,
    DisplayDateMonthPipe,
    ButtonBackComponent,

    MultiSelectFilterComponent,

    CalendarMonthComponent,

    LocationPickerModalComponent,


    // DiagramDashboardComponent,

    DiagramDashboardDialogComponent,

    ImportErrorComponent,

    HasPermissionDirective,

    HasNotPermissionDirective,

    FormatCurrencyDirective,
    FormatCurrencyPipe, FindByPipe,

    PayrollCalendarSaturdayComponent,
    AttendanceTypeTableComponent,
    TableFooterComponent,
    FileLanguageChooserComponent,
    ProvinceSelectFilterComponent,
    WorkScheduleCalendarComponent,
    AutoCompleteComponent,
    OrgMultiSelectorComponent,
    OrgMultiSelectorModalComponent,
    MobileNumberPickerComponent,
    OrgSelectorPayrollComponent,
    OrgSelectorPayrollModalComponent,
    ButtonOrgSelectorComponent,
    ButtonOrgSelectorModalComponent,
    PartyTreeSelectorComponent,
    MultiSelectAreaComponent,
    InputTextComponent,
    InputComboboxComponent,
    InputDateRangeComponent,
    InputMultiComboboxComponent,
    InputTextComponent,
    InputNumberRangeComponent,
    OrgMultiSelectorPayrollComponent,
    OrgMultiSelectorPayrollModalComponent,
    ButtonDataPickerComponent,
    ButtonDataPickerModalComponent,
    MessageDialogComponent
  ],
  exports: [
    /**
     * Shared module
     */
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    LayoutModule,

    NgbModule,
    FontAwesomeModule,

    TranslationModule,
    /**
     * prime
     */
    TableModule,
    PaginatorModule,
    ConfirmDialogModule,
    CalendarModule,
    StepsModule,
    BreadcrumbModule,
    TabMenuModule,
    DropdownModule,
    TabViewModule,
    FieldsetModule,
    SelectButtonModule,
    ContextMenuModule,
    OrganizationChartModule,
    OverlayPanelModule,
    ScrollPanelModule,
    PickListModule,
    TreeModule,
    ColorPickerModule,
    MultiSelectModule,
    DragDropModule,
    OrderListModule,
    CheckboxModule,
    AutoCompleteModule,
    ProgressBarModule,
    SplitButtonModule,
    // FullCalendarModule,
    /**
     * EJ2
     */
    // DiagramAllModule,
    // SymbolPaletteAllModule,
    // OverviewAllModule,
    // DiagramModule,
    /**
     * Shared Component
     */
    HavePermissionComponent,
    HaveNotPermissionComponent,
    ControlMessagesComponent,
    SpinnerComponent,
    OrgSelectorComponent,
    SystemIntroComponent,
    SelectFilterComponent,
    DataPickerComponent,
    DatePickerComponent,
    PayrollCalendarComponent,
    OrgTreeComponent,
    PartyTreeSelectorComponent,
    LocationPickerComponent,
    DataNationComponent,
    FileChoserComponent,
    MultilFileChoserComponent,
    DisplayHelperPipe,
    MenuTreeComponent,
    InternationalPhoneModule,
    DynamicInputComponent,
    InputSpecialDirective,
    InputTrimDirective,
    MultiLangInputComponent,
    AutoFocusDirective,
    // BaseComponent,
    DisplayDatePipe,
    DisplayDateMonthPipe,
    ButtonBackComponent,
    MultiSelectFilterComponent,
    CalendarMonthComponent,
    AutoCompleteComponent,

    // DiagramDashboardComponent,

    DiagramDashboardDialogComponent,
    // Doanvd turning ko su dung RippleModule
    // RippleModule,
    ImportErrorComponent,
    HasPermissionDirective,
    HasNotPermissionDirective,
    FormatCurrencyDirective,
    FormatCurrencyPipe, FindByPipe,
    PayrollCalendarSaturdayComponent,
    AttendanceTypeTableComponent,
    TableFooterComponent,
    FileLanguageChooserComponent,
    ProvinceSelectFilterComponent,
    WorkScheduleCalendarComponent,
    OrgMultiSelectorComponent,
    DialogModule,
    MobileNumberPickerComponent,
    OrgSelectorPayrollComponent,
    InputSwitchModule,
    ButtonOrgSelectorComponent,
    ButtonOrgSelectorModalComponent,
    MultiSelectAreaComponent,
    OrgMultiSelectorPayrollComponent,
    OrgMultiSelectorPayrollModalComponent,
    ButtonDataPickerComponent,
    ButtonDataPickerModalComponent
  ],
  entryComponents: [
    OrgSelectorModalComponent,
    DataPickerModalComponent,
    LocationPickerModalComponent,
    DiagramDashboardDialogComponent,
    OrgMultiSelectorModalComponent,
    OrgSelectorPayrollModalComponent,
    InputTextComponent,
    InputComboboxComponent,
    InputDateRangeComponent,
    InputMultiComboboxComponent,
    InputTextComponent,
    InputNumberRangeComponent,
    ButtonOrgSelectorModalComponent,
    OrgMultiSelectorPayrollComponent,
    OrgMultiSelectorPayrollModalComponent,
    ButtonDataPickerModalComponent,
    MessageDialogComponent
  ]
})
export class SharedModule {
}
