import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslationModule } from 'angular-l10n';
import { SlideMenuModule } from 'primeng/slidemenu';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { PanelMenuModule } from 'primeng/panelmenu';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { DialogModule } from 'primeng/dialog';

import { NavComponent } from './nav/nav.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { HeaderSearchComponent } from './header/header-search.component';


@NgModule({
    declarations: [
        NavComponent,
        FooterComponent,
        HeaderComponent,
        HeaderSearchComponent,
    ],
    imports: [
        CommonModule,
        FormsModule, ReactiveFormsModule,
        RouterModule,
        TranslationModule,
        PanelMenuModule, SlideMenuModule,
        NgbModule,
        AutoCompleteModule,
        ScrollPanelModule,
        DialogModule
    ],
    exports: [
        HeaderComponent,
        NavComponent,
        FooterComponent,
    ],
})
export class LayoutModule {
  constructor() {}
}
