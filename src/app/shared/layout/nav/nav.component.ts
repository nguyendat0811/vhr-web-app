import { VPS_MENU } from './../../../core/app-config';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { Component, OnInit} from '@angular/core';
import { MenuItem } from 'primeng/api';
import { environment, Constants } from '@env/environment';
import { FormGroup, FormControl } from '@angular/forms';
import { HrStorage } from '@app/core/services/HrStorage';
import { TranslationService } from 'angular-l10n';

@Component({
    selector: 'app-nav',
    templateUrl: './nav.component.html',
    styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  version = environment.version;
  // isBack = false;
  isShowSearch = false;
  isSearching = false;
  items: MenuItem[];
  vpsItems: MenuItem[];
  formSearch: FormGroup;

  constructor(public translation: TranslationService) {
    this.formSearch = new FormGroup({
      keyword: new FormControl(''),
    });
  }
  ngOnInit(): void {
    this.buildDefaultMenu();
    this.buildVpsMenus();
  }
  buildDefaultMenu() {
    this.items = [ // icon-mng-model
      {
        label: this.translation.translate('navMenu.home'),
        icon: 'fa fa-home',
        title: this.translation.translate('navMenu.home'),
        routerLink: '/home'
      },
      {
        label: this.translation.translate('navMenu.recruitmentManager'),
        icon: 'fas fa-bullseye',
        title: this.translation.translate('navMenu.recruitmentManager'),
        routerLink: '/dashboard/recruitment'
      },
      {
        label: this.translation.translate('navMenu.employeeManager'),
        icon: 'fa fa-chalkboard-teacher',
        title: this.translation.translate('navMenu.employeeManager'),
        routerLink: '/dashboard/employee'
      }, {
        label: this.translation.translate('navMenu.organizationManager'),
        icon: 'fa fa-sitemap',
        title: this.translation.translate('navMenu.organizationManager'),
        routerLink: '/dashboard/organization'
      }, {
        label: this.translation.translate('navMenu.timeKeepingManager'),
        icon: 'fa fa-clock',
        title: this.translation.translate('navMenu.timeKeepingManager'),
        routerLink: '/dashboard/time_keeping'
      }, {
        label: this.translation.translate('navMenu.transferManager'),
        icon: 'fas fa-walking',
        title: this.translation.translate('navMenu.transferManager'),
        routerLink: '/dashboard/transfer'
      }, {
        label: this.translation.translate('navMenu.rejectManager'),
        icon: 'fa fa-frown',
        title: this.translation.translate('navMenu.rejectManager'),
        routerLink: '/dashboard/reject'
      }, {
        label: this.translation.translate('navMenu.salaryManager'),
        icon: 'fa fa-money-bill-alt',
        title: this.translation.translate('navMenu.salaryManager'),
        routerLink: '/dashboard/salary'
      }, {
        label: this.translation.translate('navMenu.reportManager'),
        icon: 'fa fa-chart-pie',
        title: this.translation.translate('navMenu.reportManager'),
        routerLink: '/dashboard/report'
      }, {
        label: this.translation.translate('navMenu.settingsManager'),
        icon: 'fa fa-cogs',
        title: this.translation.translate('navMenu.settingsManager'),
        routerLink: '/dashboard/system'
      }
    ];
  }
  toogleSearch(isShowSearch) {
    this.isShowSearch = isShowSearch;
    if (isShowSearch) {
      setTimeout( () => {
        document.getElementById('navSearchText').focus();
      }, 500);
    } else {
      this.formSearch.get('keyword').setValue('');
      this.onNavSearch();
    }
  }
  onNavSearch() {
    const value = this.formSearch.value;
    const keyword = value.keyword;
    if (CommonUtils.isNullOrEmpty(keyword)) {
      this.findMenu(null);
      this.isSearching = false;
    } else {
      this.findMenu(keyword.trim().toLowerCase());
      this.isSearching = true;
    }
  }
  findMenu(keyword) {
    if (keyword === null) {
      this.deepClearStyle(this.vpsItems);
      this.clearHighlight();
      return;
    }
    this.deepApplyStyle(this.vpsItems, keyword);
    this.highlight(keyword);
  }
  deepApplyStyle(items: any, keyword: string) {
    for (const item of items) {
      let styleClass = '';
      if (this.isMatched(item, keyword)) {
        styleClass += ' matched';
      }
      if (this.isParentMatched(item, keyword)) {
        styleClass += ' parents-matched';
      }
      item.styleClass = styleClass;
      if (item.items && item.items.length > 0) {
        this.deepApplyStyle(item.items, keyword);
      }
    }
  }
  isMatched(item: any, keyword: string): boolean {
    const text = item.label.toLowerCase();
    return text.includes(keyword);
  }
  isParentMatched(item: any, keyword: string): boolean {
    if (item.items && item.items.length > 0) {
      for (const child of item.items) {
        if (this.isMatched(child, keyword) || this.isParentMatched(child, keyword)) {
          return true;
        }
      }
    } else {
      return false;
    }
  }
  highlight(text) {
    const nav = document.getElementById('nav-container');
    const menuItems = nav.getElementsByClassName('ui-menuitem-text');
    this.clearHighlight();
    Array.prototype.forEach.call(menuItems, function(el) {
      let innerHTML = el.innerHTML;
      innerHTML = innerHTML.replace(new RegExp(text, 'gi'), match => {
        return '<span class="highlightText">' + match + '</span>';
      });
      el.innerHTML = innerHTML;
    });
  }
  clearHighlight() {
    const nav = document.getElementById('nav-container');
    const menuItems = nav.getElementsByClassName('ui-menuitem-text');
    Array.prototype.forEach.call(menuItems, function(el) {
      let innerHTML = el.innerHTML;
      innerHTML = innerHTML.replace(new RegExp('<span class="highlightText">', 'gi'), match => {
        return '';
      });
      innerHTML = innerHTML.replace(new RegExp('</span>', 'gi'), match => {
        return '';
      });
      el.innerHTML = innerHTML;
    });
  }
  deepClearStyle(items: any) {
    for (const item of items) {
      item.styleClass = '';
      if (item.items && item.items.length > 0) {
        this.deepClearStyle(item.items);
      }
    }
  }
  /**
   * build vps menu
   */
  buildVpsMenus() {
    const userToken = HrStorage.getUserToken();
    if (userToken == null) {
      this.vpsItems = this.convertVpsMenus(null);
      return null;
    }
    this.vpsItems = this.convertVpsMenus(userToken.userMenuList);
  }
  /**
   * convert vpsMenu => menuItem
   */
  convertVpsMenus(userMenuList): MenuItem[] {
    if (!userMenuList || userMenuList.length === 0) {
      return null;
    }
    for (const item of userMenuList) {
      item.label = item.name;
      item.routerLink = item.url;
      if (!item.parentId) {
        if (VPS_MENU.hasOwnProperty(item.code)) {
          item.icon = VPS_MENU[item.code];
        }
      }
    }
    const converted = CommonUtils.convertVpsMenus(userMenuList, 'sysMenuId');

    return this.translateMenu(converted);
  }

  /*
  * DNN Menu VPS
  */
  translateMenu(menuList): MenuItem[] {
    for (const item of menuList) {
      const label = this.translation.translate('vpsNavMenu.' + item.code);
      if (label !== 'No key') {
        item.label = label;
      }
      if (item.items) {
        item.items = this.translateMenu(item.items);
      }
    }
    return menuList;
  }
  /****************** CAC HAM COMMON DUNG CHUNG ****/
  /**
   * f
   */
  get f () {
    return this.formSearch.controls;
  }
}
