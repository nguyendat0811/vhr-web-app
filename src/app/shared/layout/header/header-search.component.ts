import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Constants } from '@env/environment';
import { CommonUtils } from '@app/shared/services';
import { HomeEmployeeService } from '@app/core/services/home/home-employee.service';
import { AppComponent } from '@app/app.component';
import { Router } from '@angular/router';
import { HrStorage } from '@app/core/services/HrStorage';

@Component({
  selector: 'header-search',
  templateUrl: './header-search.component.html',
})
export class HeaderSearchComponent implements OnInit {
  results: string[];
  formSearch: FormGroup;
  constructor(private formBuilder: FormBuilder
            , private homeEmployeeService: HomeEmployeeService
            , private app: AppComponent
            , private router: Router
             ) {
    this.buildForm();
  }
  /**
   * buildForm
   */
  private buildForm(): void {
    const employeeCode = this.findState('employeeCode');
    const fullName = this.findState('fullName');
    const email = this.findState('email');
    this.formSearch = this.formBuilder.group({
      keyword: [''],
      employeeCode: [employeeCode === null ? true : employeeCode],
      fullName: [fullName === null ? true : fullName],
      email: [email === null ? true : email],
      mobileNumber: [this.findState('mobileNumber')],
      cmt: [this.findState('cmt')],
      tax: [this.findState('tax')],
      // bh: [this.findState('bh')],
    });
  }
  findState(key: string): boolean {
    const searchState = HrStorage.getSearchState();
    if (searchState == null) {
      return null;
    }
    return searchState[key] === true ? true : false;
  }
  search(event) {
    const params = this.formSearch ? this.formSearch.value : null;
    if (!params.employeeCode && !params.fullName && !params.email
      && !params.cmt && !params.mobileNumber && !params.tax
      ) {
      this.app.warningMessage('quickSearch.mustChose');
      this.results = [];
      return;
    }
    this.homeEmployeeService.search(params).subscribe(res => {
      this.results = res;
    });
  }
  onSelect(event) {
    this.router.navigate(['/employees/personal/' + event.employeeId + '/personal-information']);
    this.formSearch.get('keyword').setValue(event.fullName);
  }
  /**
   * ngOnInit
   */
  ngOnInit(): void {
  }
 /**
   * ngOnInit
   */
  changeCodition(): void {
    const value = this.formSearch.value;
    HrStorage.setSearchState(value);
  }
}
