import { CommonUtils } from '@app/shared/services/common-utils.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { SysLanguageService } from '@app/core/services/sys-language/sys-language.service';
import { AppComponent } from '@app/app.component';
import { HomeEmployeeService } from '@app/core/services/home/home-employee.service';
import { HrStorage } from '@app/core/services/HrStorage';
import { ThemeService } from '@app/shared/services';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  public listMarketCompany: any;
  public selectedMarket: any;
  public selectedLanguage: any;
  public listSysLanguage: any;
  public isMinium: boolean;
  public isBack: boolean;
  public userInfo: any;
  public selectedTheme = 0;
  constructor(private homeEmployeeService: HomeEmployeeService
            , private sysLanguageService: SysLanguageService
            , private app: AppComponent
            , private theme: ThemeService) {
    this.isMinium = HrStorage.getNavState();
    this.navViewChange.emit(this.isMinium);
    this.initMarketCompany();
    const userToken = HrStorage.getUserToken();
    this.userInfo = userToken ? userToken.userInfo : {};
  }
  @Output()
  public navViewChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output()
  public navFlipChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  /**
   * ngOnInit
   */
  ngOnInit(): void {
    let theme = HrStorage.getTheme() ? HrStorage.getTheme() : 1;
    this.fSelectedTheme(theme);
  }
  /**
   * onMinium
   */
  onMinium() {
    this.isMinium = !this.isMinium;
    this.navViewChange.emit(this.isMinium);
    HrStorage.setNavState(this.isMinium);
  }
  /**
   * onMinium
   */
  flipMenus() {
    this.isBack = !this.isBack;
    this.navFlipChange.emit(this.isBack);
    HrStorage.setNavFlipState(this.isBack);
  }

  initSelectedLanguage() {
    this.listSysLanguage = HrStorage.getListLang();

    if (this.listSysLanguage == null) {
      /**
       * Lấy danh sách ngôn ngữ hỗ trợ của hệ thống
       */
      this.sysLanguageService.findAll().subscribe(res => {
        HrStorage.setListLang(res);
        this.listSysLanguage = HrStorage.getListLang();
        this.selectedMarket = HrStorage.getSelectedMarket();
        for (const lang of this.listSysLanguage) {
          if (lang.code === this.selectedMarket.language) {
            HrStorage.setSelectedLang(lang);
            this.selectedLanguage = HrStorage.getSelectedLang();
            return;
          }
        }
      });
    } else {
      this.selectedLanguage = HrStorage.getSelectedLang();
      this.app.selectLanguage(this.selectedLanguage.code);
    }
  }
  setCurrentLang(lang) {
    HrStorage.setSelectedLang(lang);
    this.selectedLanguage = HrStorage.getSelectedLang();
    this.app.selectLanguage(this.selectedLanguage.code);
    location.reload();
  }
  /************************* */
  /**
   * Lấy danh sách ngôn ngữ hỗ trợ của hệ thống
   */
  initMarketCompany() {
    this.listMarketCompany = HrStorage.getListMarket();
    if (this.listMarketCompany == null) {
      this.homeEmployeeService.getMarketCompany().subscribe(res => {
        HrStorage.setListMarket(res.data);
        this.listMarketCompany = HrStorage.getListMarket();
        this.selectedMarket = HrStorage.getSelectedMarket();
        this.initSelectedLanguage();
      });
    } else {
      this.selectedMarket = HrStorage.getSelectedMarket();
      this.initSelectedLanguage();
    }
  }

  setCurrentMarket(market) {
    HrStorage.setSelectedMarket(market);
    this.selectedMarket = HrStorage.getSelectedMarket();
    if (this.selectedMarket.language) {
      const _langs = this.listSysLanguage.filter(x => x.code === this.selectedMarket.language);
      if (_langs && _langs.length > 0) {
        HrStorage.setSelectedLang(_langs[0]);
        this.selectedLanguage = HrStorage.getSelectedLang();
        this.app.selectLanguage(this.selectedLanguage.code);
      }
    }
    location.reload();
  }

  logout() {
    CommonUtils.logoutAction();
  }

  fSelectedTheme(value){
    if(this.selectedTheme != value){
      HrStorage.setTheme(value);
      this.selectedTheme = value;
      switch(value){
        case 1:
          this.theme.toggleBlueViolet();
          break;
        case 2:
          this.theme.toggleLightGreen();
          break;
        case 3:
          this.theme.toggleGreen();
          break;
        case 4:
          this.theme.toggleBlueSky();
          break;
        default:
          this.theme.toggleBlueViolet();
          break;
      }
    }
  }
}
