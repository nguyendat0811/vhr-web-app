import { Routes } from '@angular/router';

export const CONTENT_ROUTES: Routes = [
  {
    path: 'home',
    loadChildren: './modules/home/home.module#HomeModule'
  }
  , {
    path: 'follows',
    loadChildren: './modules/follows/follows.module#FollowsModule'
  }, {
    path: 'employees',
    loadChildren: './modules/employees/employees.module#EmployeesModule'
  }, {
    path: 'evaluation',
    loadChildren: './modules/evaluation/evaluation.module#EvaluationModule'
  }, {
    path: 'organizations',
    loadChildren: './modules/organizations/organizations.module#OrganizationsModule'
  }, {
    path: 'positions',
    loadChildren: './modules/positions/positions.module#PositionsModule'
  }, {
    path: 'timekeeping',
    loadChildren: './modules/timekeeping/timekeeping.module#TimekeepingModule'
  }, {
    path: 'reports',
    loadChildren: './modules/reports/reports.module#ReportsModule'
  }, {
    path: 'settings',
    loadChildren: './modules/settings/settings.module#SettingsModule'
  }
  , {
    path: 'dashboard',
    loadChildren: './modules/flows-dashboard/flows-dashboard.module#FlowsDashboardModule'
  }
  , {
    path: 'payroll',
    loadChildren: './modules/payroll/payroll.module#PayrollModule'
  }
  , {
    path: 'timesheet',
    loadChildren: './modules/timesheet/timesheet.module#TimeSheetModule'
  }
  , {
    path: 'schedular',
    loadChildren: './modules/scheduler/scheduler.module#SchedulerModule'
  }
];
