import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HelperService {
  public ORGANIZATION = new BehaviorSubject<any>([]);
  public APP_TOAST_MESSAGE = new BehaviorSubject<any>([]);
  public APP_CONFIRM_DELETE = new BehaviorSubject(null);
  public APP_SHOW_HIDE_LEFT_MENU = new BehaviorSubject<any>([]);
  public APP_SHOW_PROCESSING = new BehaviorSubject<any>([]);
  public EMPLOYEE_URL = new BehaviorSubject<any>([]);
  constructor() { }
  /**
   * afterSaveOrganization
   * param data
   */
  afterSaveOrganization(data) {
    this.ORGANIZATION.next(data);
  }
  /**
   * createMessage
   * param data
   */
  processReturnMessage(data) {
    this.APP_TOAST_MESSAGE.next(data);
  }
  /**
   * processing
   * param data
   */
  isProcessing(isProcessing: boolean) {
    this.APP_SHOW_PROCESSING.next(isProcessing);
  }
  /**
   * confirmDelete
   * param data
   */
  confirmDelete(data) {
    this.APP_CONFIRM_DELETE.next(data);
    // this.APP_CONFIRM_DELETE.pipe(data);
  }

  refreshConfirmDelete() {
    this.APP_CONFIRM_DELETE = new BehaviorSubject({});
  }
  /**
   * Load lại cây đơn vị
   */
  reloadTreeOrganization(data) {
    this.ORGANIZATION.next({type: 'RELOAD_TREE', data: data});
  }

  reloadGridOrganization(data) {
    this.ORGANIZATION.next({type: 'RELOAD_GRID', data: data});
  }

  resolveUrlEmployee(data){
    this.EMPLOYEE_URL.next(data);
  }
}
