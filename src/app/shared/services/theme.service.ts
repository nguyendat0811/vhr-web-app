import { Injectable } from '@angular/core';

export const blueVioletTheme = {
    'background-color-header': '#3f51b5',
    'background-color-nav': '#222d32',
    'background-color-expand' : '#2c3b41',
    'color-nav': 'rgba(158,158,158,.9)',
    'primary-color': '#3f51b5',
    'active-arrow-color': '#ffffff',
    'primary-color-hover': '#3F51FD'
};

export const lightGreenTheme = {
    'background-color-header': '#31c0be',
    'background-color-nav': '#eee',
    'background-color-expand' : '#f4f4f5',
    'color-nav': '#444',
    'primary-color': '#31c0be',
    'active-arrow-color': '#000000',
    'primary-color-hover': '#31C0F6',
};

export const greenTheme = {
    'background-color-header': '#609e3b',
    'background-color-nav': '#4d394b',
    'background-color-expand' : '#3e313c',
    'color-nav': '#ddd',
    'primary-color': '#609e3b',
    'active-arrow-color': '#ffffff',
    'primary-color-hover': '#609E72'
};

export const blueSkyTheme = {
    'background-color-header': '#00adca',
    'background-color-nav': '#222d32',
    'background-color-expand' : '#2c3b41',
    'color-nav': '#b8c7ce',
    'primary-color': '#00adca',
    'active-arrow-color': '#ffffff',
    'primary-color-hover': '#00ADEA'
};


@Injectable({ providedIn: 'root' })
export class ThemeService {

  toggleBlueViolet() {
    this.setTheme(blueVioletTheme);
  }

  toggleLightGreen() {
    this.setTheme(lightGreenTheme);
  }

  toggleBlueSky() {
    this.setTheme(blueSkyTheme);
  }

  toggleGreen() {
    this.setTheme(greenTheme);
  }

  private setTheme(theme: {}) {
    Object.keys(theme).forEach(k =>
      document.documentElement.style.setProperty(`--${k}`, theme[k])
    );
  }
}