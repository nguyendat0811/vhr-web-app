import { Router, NavigationEnd } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { LocaleService, TranslationService, Language } from 'angular-l10n';
import { HelperService } from '@app/shared/services/helper.service';
import { HrStorage } from '@app/core/services/HrStorage';
import { MenuItem } from 'primeng/api';
import { UserToken, UserMenu, FIXED_MENU } from '@app/core';

@Component({
  selector: 'app-content-layout',
  templateUrl: './content-layout.component.html',
  styleUrls: ['./content-layout.component.css']
})
export class ContentLayoutComponent implements OnInit {
  public items: MenuItem[];
  home: MenuItem;
  public isMinium = false;
  public isBack = true;
  private listUrlEmployee = [];
  public disableBreadCrumb = false;
  private userToken: UserToken;
  private currentUrl: string = '';

  constructor(
    public locale: LocaleService,
    public router: Router,
    public translation: TranslationService,
    public helperService: HelperService) {
      this.helperService.EMPLOYEE_URL.subscribe(data => {
        if(data) {
          let that = this;
          data.forEach(function (item) {
            let tmp = {
              url: that.buildUrl(item.routerLink),
              label: item.label,
              type: 'emp_url'
            }
            that.listUrlEmployee.push(tmp);
            that.buildBreadcrumb(that.currentUrl)
          });
        }
      });
      this.userToken = HrStorage.getUserToken();
      this.items = [
        { label: 'Categories', url: '/home' }
      ];
      this.home = { icon: 'pi pi-home', url: '/home' };

      this.router.events.subscribe(e => {
        if (e instanceof NavigationEnd) {
          this.currentUrl = e.url;
          this.buildBreadcrumb(this.currentUrl);
        }
      });

      this.helperService.APP_SHOW_HIDE_LEFT_MENU.subscribe(data => {
        this.navViewChange(!data);
      });
  }
  selectLanguage(language: string): void {
    this.locale.setCurrentLanguage(language);
  }
  public navViewChange(isMinium) {
    this.isMinium = isMinium;
  }
  public navFlipChange(isBack) {
    this.isBack = isBack;
  }
  ngOnInit() {
    this.isMinium = (true === HrStorage.getNavState());
    this.isBack = (true === HrStorage.getNavFlipState());
  }

  private buildUrl(arrUrl: string[]): string {
    let result = '';
    for (let i = 0; i < arrUrl.length - 1; i++) {
      result += arrUrl[i] + "/";
    }
    result += arrUrl[arrUrl.length - 1];
    return result;
  }

  private buildBreadcrumb(currentUrl) {
    if (currentUrl == '/home') {
      this.disableBreadCrumb = true;
    }
    else {
      let lstMenu = [];
      this.disableBreadCrumb = false;
      if (this.userToken != null) {
        if (currentUrl.includes("employees/personal") && this.listUrlEmployee.length > 0) {
          let last = this.listUrlEmployee.find(x => x.url == currentUrl);
          let second = this.userToken.userMenuList.find(x=>x.code == "app.employee.menu.updateEmployee");
          let first = this.userToken.userMenuList.find(x=>x.code == "QLNV");
          lstMenu = [last, second, first];
        }
        else {
          let last =this.userToken.userMenuList && this.userToken.userMenuList.find(x => currentUrl == x.url);
          if (last) {
            lstMenu = this.getListMenuParent(last.parentId);
            lstMenu.unshift(last);
          } else {
            let url = currentUrl;
            while(true) {
              if(!url) break;
              let item =this.userToken.userMenuList && this.userToken.userMenuList.find(x => url == x.url);
              if (item) {
                lstMenu = this.getListMenuParent(item.parentId);
                lstMenu.unshift(item);
                break;
              }
              url = this.sliceUrl(url);
            }
          }
        }
      }
      this.items = [];
      for (let i = lstMenu.length - 1; i >= 0; i--) {
        let breadCrumItem = {};
        if (lstMenu[i]) {
          if (lstMenu[i].type) {
            breadCrumItem = lstMenu[i];
          }
          else {
            breadCrumItem = { label: lstMenu[i].name, url: lstMenu[i].url };
          }
        }
        this.items.push(breadCrumItem);
      }
    }
  }


  private getListMenuParent(menuId: any): UserMenu[] {
    let parentId = menuId;
    let result = [];
    while (parentId != 0) {
      let parent = this.userToken.userMenuList.find(x => x.sysMenuId === parentId);
      parentId = parent.parentId ? parent.parentId : 0;
      if (parent) {
        result.push(parent);
      }
    }
    return result;
  }
  private sliceUrl(url:string): string {
    let result = '';
    if(!url) return result;
    let lastIdx = url.lastIndexOf('/');
    result = url.slice(0, lastIdx);
    return result;
  }
}
