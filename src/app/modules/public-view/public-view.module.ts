import {CarouselModule} from 'primeng/carousel';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PublicViewRoutingModule } from './public-view-routing.module';
import { ViewDetailPayslipComponent } from './view-detail-payslip/view-detail-payslip.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { PanelModule } from 'primeng/panel';
import { ButtonModule } from 'primeng/button';

@NgModule({
  declarations: [
    ViewDetailPayslipComponent
  ],
  imports: [
    CommonModule,
    //SharedModule,
    PublicViewRoutingModule,
    PdfViewerModule,
    CarouselModule,
    PanelModule,
    ButtonModule
  ]
})
export class PublicViewModule { }
