import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewDetailPayslipComponent } from './view-detail-payslip/view-detail-payslip.component';

const routes: Routes = [
  {
    path: 'view-detail-pay-slip',
    component: ViewDetailPayslipComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicViewRoutingModule { }
