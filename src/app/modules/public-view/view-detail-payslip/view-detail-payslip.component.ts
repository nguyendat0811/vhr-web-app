import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PayslipService } from '@app/core';
import { HelperService } from '@app/shared/services/helper.service';
import { CommonUtils } from '@app/shared/services';

(<any>window).pdfWorkerSrc = '../../../../assets/api/pdf.worker.min.js';

@Component({
  selector: 'view-detail-payslip',
  templateUrl: './view-detail-payslip.component.html'
})
export class ViewDetailPayslipComponent implements OnInit, OnDestroy  {
  params: string;
  pdfSrc: any;
  listPayslip: any = [];
  payrollGeneralId = 0;
  isLoadding = false;
  styleHeight = '480px';
  private sub: any;
  constructor(
    private route: ActivatedRoute
    , private payslipService: PayslipService
    , private helperService: HelperService
    ) {
    this.sub = this.route.queryParams.subscribe(params => {
      this.params = params['params'];
      this.getData();
   });
  }
  ngOnInit() {
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  getData(){
    this.payslipService.processGetDetail(this.params).subscribe(res=>{
      if(res && res.length > 0){
        if(res.length == 1){
          this.styleHeight = '540px';
        }
        else{
          this.styleHeight = '480px';
        }
        this.listPayslip = res;
        this.payrollGeneralId = res[0].payrollGeneralId;
        this.pdfSrc = this.payslipService.buildUrlViewDetail(this.params, res[0].payrollGeneralId);
      }
    });
  }

  changePayslip(index){
    this.payrollGeneralId = this.listPayslip[index].payrollGeneralId;
    this.pdfSrc = this.payslipService.buildUrlViewDetail(this.params, this.listPayslip[index].payrollGeneralId);
  }

  onComplete(ev){
  }

  downloadFile(){
    this.payslipService.downloadPDF(this.params, this.payrollGeneralId).subscribe(res=>{
      var blob = new Blob([res], {type: "application/pdf"});
      var link = document.createElement('a');
      link.href = window.URL.createObjectURL(blob);
      var fileName = 'Chi_Tiet_Phieu_Luong.pdf';
      link.download = fileName;
      link.click();
    })
  }
}