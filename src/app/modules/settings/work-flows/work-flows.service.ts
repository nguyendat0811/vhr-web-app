import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BasicService } from '@app/core/services/basic.service';
import { environment } from '@env/environment';
import { Observable } from 'rxjs';
import { CONFIG } from '@app/core';
import { HelperService } from '@app/shared/services/helper.service';

@Injectable({
  providedIn: 'root'
})
export class WfMenuMappingService extends BasicService {
  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('sys', 'wfMenuMapping', httpClient, helperService);
  }
  public findNodes(): Observable<any> {
    const url = `${this.serviceUrl}/find-nodes/`;
    return this.httpClient.get(url).pipe();
  }
  public findNodesByWorkFlowId(id): Observable<any> {
    const url = `${this.serviceUrl}/work-flows/${id}`;
    return this.httpClient.get(url).pipe();
  }
  public findNodesByWorkFlowCode(code): Observable<any> {
    const url = `${this.serviceUrl}/work-flows-code/${code}`;
    return this.httpClient.get(url).pipe();
  }
}
@Injectable({
  providedIn: 'root'
})
export class WorkFlowsService extends BasicService {
  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('sys', 'workFlows', httpClient, helperService);
  }
  public findByCode(code): Observable<any> {
    const url = `${this.serviceUrl}/work-flows-code/${code}`;
    return this.httpClient.get(url).pipe();
  }
  //
}
