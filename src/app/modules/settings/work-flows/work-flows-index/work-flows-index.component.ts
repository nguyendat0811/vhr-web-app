import { CommonUtils } from './../../../../shared/services/common-utils.service';
import { TreeNode } from 'primeng/api';
import { Component, OnInit, ViewChild, OnChanges, SimpleChanges, OnDestroy } from '@angular/core';
import { WfMenuMappingService } from '../work-flows.service';
import { AuthService } from '@app/core/services/auth.service';

@Component({
  selector: 'work-flows-index',
  templateUrl: './work-flows-index.component.html',
  styles: []
})
export class WorkFlowsIndexComponent implements OnInit {
  // list data từ API, chưa build
  dataSource: any = [];
  dataDest: any = [];
  // list data đã build tree
  listSource: any = [];
  listDest: any = [];
  // list data đc tích chọn
  selectedSrcNode: any[] = [];
  selectedDestNode: any[] = [];

  workFlowId: number;
  constructor(private wfMenuMappingService: WfMenuMappingService,
              private authService: AuthService
    ) { }

  ngOnInit() {
    this.authService.findAllMenus().subscribe(res => {
      this.dataSource = res.data;
      this.listSource = this.buildTree(this.dataSource);
    });
  }

  buildTree(data: any): any {
    const dataMap = data.reduce((m, d) => {
      m[d.nodeId] = Object.assign({}, d);
      return m;
    }, {});
    const listTemp = data.filter(d => {
      if (d.parentId !== null) { // assign child to its parent
        const parentNode = dataMap[d.parentId];
        if (parentNode['children'] === undefined || parentNode['children'] === null ) {
          parentNode['children'] = [];
        }
        parentNode.children.push(dataMap[d.nodeId]);
        parentNode.expanded = true;
        return false;
      }
      return true; // root node, do nothing
    }).map(d => dataMap[d.nodeId]);
    return listTemp;
  }
  public onChangeWorkFlow(wf: any) {
    this.workFlowId = wf;
    this.refreshNode();
    this.wfMenuMappingService.findNodesByWorkFlowId(this.workFlowId).subscribe(res => {
      if (res.data && res.data.length > 0) {
        this.dataDest = CommonUtils.pureDataToTreeNode(this.dataSource, res.data);
        console.log('this.dataDest', this.dataDest);
        this.listDest = this.buildTree(this.dataDest);
        const lstTmpSource = [];
        this.convertTreeToList(this.listSource, lstTmpSource);
        const lstTmpDest = [];
        this.convertTreeToList(this.listDest, lstTmpDest);
        for (const node of this.dataDest) {
          const _node = lstTmpSource.filter(x => x.nodeId === node.nodeId)[0];
          if (_node && !this.isExist(this.selectedSrcNode, _node)) {
            this.selectedSrcNode.push(_node);
          }
          const _node2 = lstTmpDest.filter(x => x.nodeId === node.nodeId)[0];
          if ( node.isMainAction && _node && !this.isExist(this.selectedDestNode, _node2)) {
            this.selectedDestNode.push(_node2);
          }
        }
      }
    });
  }
  /** Source Tree Action */
  public onNodeSourceSelect($event: any) {
    this.dataDest = this.addNodes(this.dataDest, $event.node);
    this.selectedDestNode = [];
    this.listDest = this.buildTree(this.dataDest);
  }
  public onNodeSourceUnSelect($event: any) {
    this.dataDest = this.removeNodes(this.dataDest, $event.node);
    const arrChild = [];
    this.getChildren(this.dataSource, arrChild, $event.node.nodeId);
    if (arrChild && arrChild.length > 0) {
      this.dataDest = this.removeNodes(this.dataDest, arrChild);
    }
    const arrParent = [];
    this.getParent(this.dataSource, arrParent, $event.node.nodeId);
    while (arrParent.length > 0) {
      for (const _parent of arrParent) {
        const count = this.dataDest.filter(x => x.parentId === _parent.nodeId).length;
        if (count === 0) {
          this.dataDest = this.removeNodes(this.dataDest, _parent);
        }
        this.getParent(this.dataSource, arrParent, _parent.nodeId);
        arrParent.splice(arrParent.findIndex(x => x.nodeId === _parent.nodeId), 1);
      }
    }
    this.selectedDestNode = [];
    this.listDest = this.buildTree(this.dataDest);
  }
  private refreshNode() {
    this.dataDest = [];
    this.listDest = [];
    this.selectedSrcNode = [];
    this.selectedDestNode = [];
  }
  private addNodes(dataDest: any, node: any) {
    if (!this.isExist(dataDest, node.nodeId)) {
      dataDest.push(this.dataSource.filter(x => x.nodeId === node.nodeId)[0]);
    }
    this.getChildren(this.dataSource, dataDest, node.nodeId);
    this.getParent(this.dataSource, dataDest, node.parentId);
    return dataDest;
  }
  private removeNodes(dataDest: any, nodes: any) {
    if (typeof nodes === 'object') {
      if (this.isExist(dataDest, nodes.nodeId)) {
        dataDest.splice(dataDest.findIndex(x => x.nodeId === nodes.nodeId), 1);
      }
    }
    if (Array.isArray(nodes)) {
      for (const node of nodes) {
        dataDest.splice(dataDest.findIndex(x => x.nodeId === node.nodeId), 1);
      }
    }
    // this.getChildren(this.dataSource, dataDest, node.nodeId);
    // this.getParent(this.dataSource, dataDest, node.parentId);
    return dataDest;
  }
  private getChildren(arrSource, arrDest, nodeId) {
    for (const i in arrSource) {
      if (arrSource[i].parentId === nodeId) {
        this.getChildren(arrSource, arrDest, arrSource[i].nodeId);
        if (!this.isExist(arrDest, arrSource[i].nodeId)) {
          arrDest.push(arrSource[i]);
        }
      }
    }
  }
  private getParent(arr, arrDest, parentId) {
    for (const i in arr) {
      if (arr[i].nodeId === parentId) {
        this.getParent(arr, arrDest, arr[i].parentId);
        if (!this.isExist(arrDest, arr[i].nodeId)) {
          arrDest.push(arr[i]);
        }
      }
    }
  }
  private isExist(arr, nodeId) {
    const item = arr.filter(x => x.nodeId === nodeId);
    if (item && item.length > 0) {
      return true;
    }
    return false;
  }
  private convertTreeToList(arrSource, arrDest) {
    for (const i in arrSource) {
      if (arrSource[i].children && arrSource[i].children.length > 0 ) {
        this.convertTreeToList(arrSource[i].children, arrDest);
        if (!this.isExist(arrDest, arrSource[i].nodeId)) {
          arrDest.push(arrSource[i]);
        }
      } else {
        arrDest.push(arrSource[i]);
      }
    }
  }

  /** Dest Tree Action */

  processSaveOrUpdate() {
    for (const node of this.dataDest) {
      node.workFlowId = this.workFlowId;
      if (this.selectedDestNode && this.selectedDestNode.length > 0) {
        const nodeSelected = this.selectedDestNode.filter(x => x.nodeId === node.nodeId)[0];
        if (nodeSelected) {
          node.isMainAction = true;
        } else {
          node.isMainAction = false;
        }
      }
    }
    this.wfMenuMappingService.saveOrUpdate(this.dataDest).subscribe(res => {
      this.wfMenuMappingService.requestIsSuccess(res);
    });
  }

}
