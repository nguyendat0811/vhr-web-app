import { SysCatService } from './../../../../core/services/hr-system/sys-cat.service';
import { APP_CONSTANTS, DEFAULT_MODAL_OPTIONS, LARGE_MODAL_OPTIONS } from './../../../../core/app-config';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { Component, OnInit, OnChanges, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { WfMenuMappingService } from '../work-flows.service';
import { AppComponent } from '@app/app.component';
import { WorkFlowsFormComponent } from '../work-flows-form/work-flows-form.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { WorkFlowsDiagramComponent } from '../work-flows-diagram/work-flows-diagram.component';

@Component({
  selector: 'work-flows-search',
  templateUrl: './work-flows-search.component.html',
  styles: []
})
export class WorkFlowsSearchComponent extends BaseComponent implements OnInit {

  @Output()
  public onChangeWorkFlow: EventEmitter<any> = new EventEmitter<any>();
  formSearch: FormGroup;
  workFlows: any = [];
  formConfig = {
    workFlowId: ['']
  };
  constructor(public actr: ActivatedRoute
            , private modalService: NgbModal
            , public wfMenuMappingService: WfMenuMappingService
            , private sysCatService: SysCatService
            , private app: AppComponent) {
        super();
        this.formSearch = this.buildForm({}, this.formConfig);
        this.loadReference();
  }
  ngOnInit() {
  }
  private loadReference(): void {
    this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.WORK_FLOW)
    .subscribe(res => this.workFlows = res.data);
  }
  get f() {
    return this.formSearch.controls;
  }
  public onChangeWf($event) {
    this.onChangeWorkFlow.emit($event);
  }
  public config(): void {
    const modalRef = this.modalService.open(WorkFlowsDiagramComponent, LARGE_MODAL_OPTIONS);
    modalRef.componentInstance.setWorkFlowId(this.formSearch.get('workFlowId').value);

    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
    });
  }
}
