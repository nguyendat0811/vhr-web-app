import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WorkFlowsRoutingModule } from './work-flows-routing.module';
import { WorkFlowsIndexComponent } from './work-flows-index/work-flows-index.component';
import { WorkFlowsSearchComponent } from './work-flows-search/work-flows-search.component';
import { WorkFlowsFormComponent } from './work-flows-form/work-flows-form.component';
import { SharedModule } from '@app/shared';
import { ButtonModule, CheckBoxModule } from '@syncfusion/ej2-angular-buttons';
import { NumericTextBoxModule, ColorPickerModule, UploaderModule, TextBoxModule} from '@syncfusion/ej2-angular-inputs';
import { ToolbarModule } from '@syncfusion/ej2-angular-navigations';
import { DropDownButtonModule } from '@syncfusion/ej2-angular-splitbuttons';
import { DiagramModule, HierarchicalTreeService, MindMapService, RadialTreeService, ComplexHierarchicalTreeService,
   DataBindingService, SnappingService, PrintAndExportService, BpmnDiagramsService, SymmetricLayoutService, ConnectorBridgingService,
   UndoRedoService, LayoutAnimationService, DiagramContextMenuService, ConnectorEditingService } from '@syncfusion/ej2-ng-diagrams';
   import { DiagramAllModule, SymbolPaletteAllModule, OverviewAllModule } from '@syncfusion/ej2-ng-diagrams';
import { WorkFlowsDiagramComponent } from './work-flows-diagram/work-flows-diagram.component';
import { StyleConfigComponent } from './work-flows-diagram/modals/style-config/style-config.component';
import { UrlConfigComponent } from './work-flows-diagram/modals/url-config/url-config.component';

@NgModule({
  declarations: [WorkFlowsIndexComponent, WorkFlowsSearchComponent, WorkFlowsFormComponent, WorkFlowsDiagramComponent, StyleConfigComponent, UrlConfigComponent],
  imports: [
    CommonModule,
    SharedModule,
    WorkFlowsRoutingModule,
    ButtonModule,
    CheckBoxModule,
    DropDownButtonModule,
    DiagramModule,
    ToolbarModule,
    UploaderModule,
    SymbolPaletteAllModule,
  ],
  providers: [HierarchicalTreeService, MindMapService, RadialTreeService, ComplexHierarchicalTreeService, DataBindingService,
          SnappingService, PrintAndExportService, BpmnDiagramsService, SymmetricLayoutService, ConnectorBridgingService, UndoRedoService,
          LayoutAnimationService, DiagramContextMenuService, ConnectorEditingService, BpmnDiagramsService ],
  entryComponents: [WorkFlowsFormComponent, StyleConfigComponent, UrlConfigComponent]
})
export class WorkFlowsModule { }
