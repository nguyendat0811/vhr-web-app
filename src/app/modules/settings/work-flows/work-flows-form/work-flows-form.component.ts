import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit} from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AppComponent } from '@app/app.component';

@Component({
  selector: 'work-flows-form',
  templateUrl: './work-flows-form.component.html',
  styles: [],
})
export class WorkFlowsFormComponent extends BaseComponent implements OnInit  {
  workFlowId: number;
  formSave: FormGroup;
  formConfig = {
    url: [''],
    resource: ['']
  };
  constructor(public actr: ActivatedRoute
            , public activeModal: NgbActiveModal
            , private app: AppComponent) {
        super();
        this.formSave = this.buildForm({}, this.formConfig);
  }
  ngOnInit() {
  }
  get f() {
    return this.formSave.controls;
  }
  public setFormValue(data: any) {
    if (data) {
      this.formSave = this.buildForm(data, this.formConfig);
    }
  }
  processSaveOrUpdate() {
    this.app.confirmMessage(null, () => {// on accepted
      this.activeModal.close(this.formSave.value);
    }, () => {// on rejected

    });
  }
}
