import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from '@app/app.component';
import { CommonUtils } from '@app/shared/services';
import { NodeModel, TextStyleModel } from '@syncfusion/ej2-diagrams';
import { DiagramComponent } from '@syncfusion/ej2-ng-diagrams';

@Component({
  selector: 'style-config',
  templateUrl: './style-config.component.html',
})
export class StyleConfigComponent implements OnInit {
  workFlowId: number;
  formSave: FormGroup;
  diagram: DiagramComponent;
  nodes: NodeModel[];
  colorInp: '';
  colorTextInp: '';
  formConfig = {
    color: [''],
    colorText: ['']
  };
  constructor(public actr: ActivatedRoute
            , public activeModal: NgbActiveModal
            , private app: AppComponent) {
        this.formSave = CommonUtils.createForm({}, this.formConfig);
  }
  ngOnInit() {
  }
  get f() {
    return this.formSave.controls;
  }
  public setFormValue(data: any, diagram: any) {
    this.diagram = diagram;
    if (data) {
      this.nodes = data;
      this.formSave.get('color').setValue(this.nodes[0].style['fill'] ? (this.nodes[0].style['fill'] === 'white' ? '#ffffff' : this.nodes[0].style['fill']) : '');
      this.formSave.get('colorText').setValue(this.nodes[0].annotations[0].style.color? (this.nodes[0].annotations[0].style.color === 'white' ? '#ffffff' : this.nodes[0].annotations[0].style.color) : '');
      this.colorInp = this.formSave.get('color').value;
      this.colorTextInp = this.formSave.get('colorText').value;
    }
  }
  public changeColorInp(): void {
    this.formSave.get('color').setValue(this.colorInp);
    this.changeColor();
  }
  public changeColor(): void {
    this.colorInp = this.formSave.get('color').value;
    for (const node of this.nodes) {
      node['controlParent'].isProtectedOnChange = false;
      node.style['fill'] = this.formSave.get('color').value;
    }
    this.diagram.dataBind();
  }
  public changeColorTextInp(): void {
    this.formSave.get('colorText').setValue(this.colorTextInp);
    this.changeColorText();
  }
  public changeColorText(): void {
    this.colorTextInp = this.formSave.get('colorText').value;
    for (const node of this.nodes) {
      for (let j = 0; j < node.annotations.length; j++) {
        const textStyle: TextStyleModel = node.annotations[j].style;
        textStyle.color = this.formSave.get('colorText').value;
      }
    }

    this.diagram.dataBind();
  }
  processSaveOrUpdate() {
    this.activeModal.close(this.formSave.value);
  }
}
