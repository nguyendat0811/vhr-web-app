import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from '@app/app.component';
import { CommonUtils } from '@app/shared/services';

@Component({
  selector: 'url-config',
  templateUrl: './url-config.component.html',
  styles: []
})
export class UrlConfigComponent implements OnInit {
  workFlowId: number;
  formSave: FormGroup;
  formConfig = {
    url: [''],
    resource: ['']
  };
  constructor(public actr: ActivatedRoute
            , public activeModal: NgbActiveModal
            , private app: AppComponent) {
        this.formSave = CommonUtils.createForm({}, this.formConfig);
  }
  ngOnInit() {
  }
  get f() {
    return this.formSave.controls;
  }
  public setFormValue(data: any) {
    if (data) {
      this.formSave = CommonUtils.createForm({}, this.formConfig);
    }
  }
  processSaveOrUpdate() {
    this.app.confirmMessage(null, () => {// on accepted
      this.activeModal.close(this.formSave.value);
    }, () => {// on rejected

    });
  }
}
