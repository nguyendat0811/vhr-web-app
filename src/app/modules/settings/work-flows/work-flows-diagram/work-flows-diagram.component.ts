// tslint:disable:max-line-length
import { HelperService } from './../../../../shared/services/helper.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit, ViewEncapsulation, ViewChild, Input, SimpleChanges, SimpleChange, OnChanges, OnDestroy, Inject } from '@angular/core';
import { PointPortModel, NodeModel, SymbolInfo, PaletteModel, ConnectorModel, MarginModel, IDragEnterEventArgs, UndoRedo, StrokeStyleModel, Diagram,
  SnapSettingsModel, Connector, FlowShapeModel, TextStyleModel, OrthogonalSegmentModel, BpmnGatewayModel, NodeConstraints, SymbolPalette, BpmnDiagrams, ContextMenuSettingsModel, DiagramContextMenu, DiagramBeforeMenuOpenEventArgs, SnapConstraints, IPropertyChangeEventArgs, ITextEditEventArgs  } from '@syncfusion/ej2-diagrams';
import { ExpandMode, ClickEventArgs, MenuEventArgs } from '@syncfusion/ej2-navigations';
import { DiagramComponent } from '@syncfusion/ej2-ng-diagrams';
import { paletteIconClick, showPaletteIcon } from 'lib/ejs-diagrams/common';
import { AsyncSettingsModel } from '@syncfusion/ej2-inputs';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { WfMenuMappingService, WorkFlowsService } from '../work-flows.service';
import { SysCatService, SMALL_MODAL_OPTIONS, MEDIUM_MODAL_OPTIONS } from '@app/core';
import { CommonUtils } from '@app/shared/services';
import { DOCUMENT } from '@angular/platform-browser';
import { UrlConfigComponent } from './modals/url-config/url-config.component';
import { StyleConfigComponent } from './modals/style-config/style-config.component';
Diagram.Inject(UndoRedo);
SymbolPalette.Inject(BpmnDiagrams);
Diagram.Inject(BpmnDiagrams);
Diagram.Inject(DiagramContextMenu);
@Component({
  selector: 'work-flows-diagram',
  templateUrl: './work-flows-diagram.component.html',
  styleUrls: ['./material.css'],
  encapsulation: ViewEncapsulation.None
})
export class WorkFlowsDiagramComponent implements OnChanges, OnInit, OnDestroy  {
  @ViewChild('diagram')
  public diagram: DiagramComponent;
  @ViewChild('diagramTmp')
  private diagramTmp: DiagramComponent;
  @Input()
  public workFlowId: number;
  private _workFlowId: number;
  formConfig = {
    workFlowId: [''],
    code: [''],
    name: [''],
    status: [''],
    diagram: ['']
  };
  formSave: FormGroup;
  public contextMenu: ContextMenuSettingsModel;
  constructor(public actr: ActivatedRoute
    , public wfMenuMappingService: WfMenuMappingService
    , public workFlowsService: WorkFlowsService
    , private sysCatService: SysCatService
    , private modalService: NgbModal
    , private helperService: HelperService
    , @Inject(DOCUMENT) document) {
      this.formSave = CommonUtils.createForm({}, this.formConfig);
      this.loadReference();
  }
  ngOnChanges(changes: SimpleChanges) {
    const workFlowId: SimpleChange = changes.workFlowId;
    this._workFlowId = workFlowId.currentValue;
    this.formSave.get('workFlowId').setValue(this._workFlowId);
    this.loadNewDiagram();
  }
  ngOnInit() {
   this.contextMenu = {
    show: true
    , items: [
      {
        text: 'Delete',
        id: 'delete',
      },
      {
        text: 'Config URL',
        id: 'url',
      },
      {
        text: 'Config Style',
        id: 'styleConfig',
      }]
    // Hides the default context menu items
    , showCustomMenuOnly: false
  };
  }
  ngOnDestroy() {
    if (!this.diagram.isDestroyed) {
      this.diagram.destroy();
    }
  }
  private loadReference(): void {
  }
  private loadNewDiagram(): void {
    this.sysCatService.findOne(this._workFlowId).subscribe(res => {
      if (res.data) {
        this.formSave.get('workFlowId').setValue(res.data.sysCatId);
        this.formSave.get('code').setValue(res.data.code);
        this.formSave.get('name').setValue(res.data.name);
      }
    });
    this.initDiagram();
    this.workFlowsService.findOne(this._workFlowId).subscribe(res => {
      this.formSave.get('diagram').setValue('');
      if (res.data && res.data.diagram) {
        this.formSave.get('diagram').setValue(res.data.diagram);
        this.helperService.isProcessing(true);
        setTimeout(() => {
          this.diagramTmp.loadDiagram(res.data.diagram);
          this.diagram.nodes = Object.assign([], this.diagramTmp.nodes);
          this.diagram.connectors = Object.assign([], this.diagramTmp.connectors);
          this.diagram.dataBind();
          this.helperService.isProcessing(false);
        }, 500);
      }
    });
  }
  private initDiagram(): void {
    this.diagram.ngOnInit();
    this.diagram.clear();
    this.diagramTmp.ngOnInit();
    this.diagramTmp.clear();
  }
  get f() {
    return this.formSave.controls;
  }

 // #region "Diagram Properties"

 // tslint:disable: member-ordering

 public interval: number[] = [
   1, 9, 0.25, 9.75, 0.25, 9.75, 0.25, 9.75, 0.25, 9.75, 0.25,
   9.75, 0.25, 9.75, 0.25, 9.75, 0.25, 9.75, 0.25, 9.75
 ];
 public snapSettings: SnapSettingsModel = {
  horizontalGridlines: { lineColor: '#e0e0e0', lineIntervals: this.interval },
  verticalGridlines: { lineColor: '#e0e0e0', lineIntervals: this.interval }
  };
  public asyncSettings: AsyncSettingsModel = {
    saveUrl: 'https://aspnetmvc.syncfusion.com/services/api/uploadbox/Save',
    removeUrl: 'https://aspnetmvc.syncfusion.com/services/api/uploadbox/Remove'
  };
 // SymbolPalette Properties
 public symbolMargin: MarginModel = { left: 15, right: 15, top: 15, bottom: 15 };
 public expandMode: ExpandMode = 'Multiple';
 // Initialize the flowshapes for the symbol palatte
 private flowshapes: NodeModel[] = [
  { id: 'Text', shape: { type: 'Text', content: 'Label' }, style: {strokeColor: 'transparent', fill: 'transparent'} },
   { id: 'Terminator', shape: { type: 'Flow', shape: 'Terminator'}},
   { id: 'Process', shape: { type: 'Flow', shape: 'Process' } },
   { id: 'Decision', shape: { type: 'Flow', shape: 'Decision' } },
   { id: 'Document', shape: { type: 'Flow', shape: 'Document' } },
   { id: 'PreDefinedProcess', shape: { type: 'Flow', shape: 'PreDefinedProcess' } },
   { id: 'PaperTap', shape: { type: 'Flow', shape: 'PaperTap' } },
   { id: 'DirectData', shape: { type: 'Flow', shape: 'DirectData' } },
   { id: 'SequentialData', shape: { type: 'Flow', shape: 'SequentialData' } },
   { id: 'Sort', shape: { type: 'Flow', shape: 'Sort' } },
   { id: 'MultiDocument', shape: { type: 'Flow', shape: 'MultiDocument' } },
   { id: 'Collate', shape: { type: 'Flow', shape: 'Collate' } },
   { id: 'SummingJunction', shape: { type: 'Flow', shape: 'SummingJunction' } },
   { id: 'Or', shape: { type: 'Flow', shape: 'Or' } },
   {
     id: 'InternalStorage',
     shape: { type: 'Flow', shape: 'InternalStorage' }
   },
   { id: 'Extract', shape: { type: 'Flow', shape: 'Extract' } },
   {
     id: 'ManualOperation',
     shape: { type: 'Flow', shape: 'ManualOperation' }
   },
   { id: 'Merge', shape: { type: 'Flow', shape: 'Merge' } },
   {
     id: 'OffPageReference',
     shape: { type: 'Flow', shape: 'OffPageReference' }
   },
   {
     id: 'SequentialAccessStorage',
     shape: { type: 'Flow', shape: 'SequentialAccessStorage' }
   },
   { id: 'Annotation', shape: { type: 'Flow', shape: 'Annotation' } },
   { id: 'Annotation2', shape: { type: 'Flow', shape: 'Annotation2' } },
   { id: 'Data', shape: { type: 'Flow', shape: 'Data' } },
   { id: 'Card', shape: { type: 'Flow', shape: 'Card' } },
   { id: 'Delay', shape: { type: 'Flow', shape: 'Delay' } }
 ];
 public textShapes: NodeModel[] = [
  { id: 'Text', shape: { type: 'Text', shape: 'Rectangle' }, style: {strokeColor: 'transparent'} }
 ];
 // Initializes connector symbols for the symbol palette
 private connectorSymbols: ConnectorModel[] = [
   {
     id: 'Link1',
     type: 'Orthogonal',
     sourcePoint: { x: 0, y: 0 },
     targetPoint: { x: 60, y: 60 },
     targetDecorator: { shape: 'Arrow' },
     style: { strokeWidth: 1 }
   },
   {
     id: 'link3',
     type: 'Orthogonal',
     sourcePoint: { x: 0, y: 0 },
     targetPoint: { x: 60, y: 60 },
     style: { strokeWidth: 1 },
     targetDecorator: { shape: 'None' }
   },
   {
     id: 'Link21',
     type: 'Straight',
     sourcePoint: { x: 0, y: 0 },
     targetPoint: { x: 60, y: 60 },
     targetDecorator: { shape: 'Arrow' },
     style: { strokeWidth: 1 }
   },
   {
     id: 'link23',
     type: 'Straight',
     sourcePoint: { x: 0, y: 0 },
     targetPoint: { x: 60, y: 60 },
     style: { strokeWidth: 1 },
     targetDecorator: { shape: 'None' }
   },
   {
     id: 'link33',
     type: 'Bezier',
     sourcePoint: { x: 0, y: 0 },
     targetPoint: { x: 60, y: 60 },
     style: { strokeWidth: 1 },
     targetDecorator: { shape: 'None' }
   },
   {
    id: 'link4', sourcePoint: { x: 0, y: 0 }, targetPoint: { x: 60, y: 60 }, type: 'Orthogonal',
    shape: {
        type: 'Bpmn',
        flow: 'Association',
        association: 'Directional'
    }, style: {
        strokeDashArray: '2,2'
    },
  },
 ];
 // Initializes bpmnShapeModel symbols for the symbol palette
 public bpmnShapes: NodeModel[] = [
  {
    id: 'Start', width: 35, height: 35, shape: {
        type: 'Bpmn', shape: 'Event',
        event: { event: 'Start' }
    }
  },
  {
    id: 'NonInterruptingIntermediate', width: 35, height: 35, shape: {
        type: 'Bpmn', shape: 'Event',
        event: { event: 'NonInterruptingIntermediate' }
    },
  },
  {
      id: 'End', width: 35, height: 35, offsetX: 665, offsetY: 230, shape: {
          type: 'Bpmn', shape: 'Event',
          event: { event: 'End' }
      },
  },
  {
      id: 'Task', width: 35, height: 35, offsetX: 700, offsetY: 700,
      shape: {
          type: 'Bpmn', shape: 'Activity', activity: {
              activity: 'Task',
          },
      }
  },
  {
      id: 'Transaction', width: 35, height: 35, offsetX: 300, offsetY: 100,
      constraints: NodeConstraints.Default | NodeConstraints.AllowDrop,
      shape: {
          type: 'Bpmn', shape: 'Activity',
          activity: {
              activity: 'SubProcess', subProcess: {
                  type: 'Transaction', transaction: {
                      cancel: { visible: false }, failure: { visible: false }, success: { visible: false }
                  }
              }
          }
      }
  }, {
      id: 'Task_Service', width: 35, height: 35, offsetX: 700, offsetY: 700,
      shape: {
          type: 'Bpmn', shape: 'Activity', activity: {
              activity: 'Task', task: { type: 'Service' }
          },
      },

  },
  {
      id: 'Gateway', width: 35, height: 35, offsetX: 100, offsetY: 100,
      shape: { type: 'Bpmn', shape: 'Gateway', gateway: { type: 'Exclusive' } as BpmnGatewayModel },
  },
  {
      id: 'DataObject', width: 35, height: 35, offsetX: 500, offsetY: 100,
      shape: { type: 'Bpmn', shape: 'DataObject', dataObject: { collection: false, type: 'None' } }
  },
  {
      id: 'subProcess', width: 520, height: 250, offsetX: 355, offsetY: 230,
      constraints: NodeConstraints.Default | NodeConstraints.AllowDrop,
      shape: {
          shape: 'Activity', type: 'Bpmn',
          activity: {
              activity: 'SubProcess', subProcess: {
                  type: 'Transaction', collapsed: false,
                  processes: [], transaction: {
                      cancel: { visible: false }, failure: { visible: false }, success: { visible: false }
                  }
              }
          }
      }
  },
  {
    id: 'Message', width: 35, height: 35, shape: {
        type: 'Bpmn', shape: 'Event',
        event: { event: 'Start', trigger: 'Message'  }
    }
  },
  {
    id: 'DataSource', width: 35, height: 35, offsetX: 500, offsetY: 100, shape: {
        type: 'Bpmn', shape: 'DataSource'    }
  },
];
 public palettes: PaletteModel[] = [
  //  {
  //   id: 'text',
  //   expanded: true,
  //   symbols: this.textShapes,
  //   iconCss: 'shapes',
  //   title: 'Text Shapes'
  //  },
   {
     id: 'flow',
     expanded: true,
     symbols: this.flowshapes,
     iconCss: 'shapes',
     title: 'Flow Shapes'
   },
   {
     id: 'connectors',
     expanded: true,
     symbols: this.connectorSymbols,
     iconCss: 'shapes',
     title: 'Connectors'
   },
   {
     id: 'bpmn',
     expanded: true,
     symbols: this.bpmnShapes,
     iconCss: 'shapes',
     title: 'BPMN Shapes'
   }
 ];


 public nodeDefaults(node: NodeModel): NodeModel {
   const obj: NodeModel = {};
   if (obj.width === undefined) {
     obj.width = 145;
   } else {
     const ratio: number = 100 / obj.width;
     obj.width = 100;
     obj.height *= ratio;
   }
   obj.style = { fill: '#fff', strokeColor: '#58666E' };
   obj.annotations = [{ style: { color: '#58666E', fill: 'transparent' } }];
   obj.ports = getPorts(node);
   return obj;
 }
 public connDefaults(obj: Connector): void {
   if (obj.id.indexOf('connector') !== -1) {
     obj.type = 'Orthogonal';
     obj.targetDecorator = { shape: 'Arrow', width: 10, height: 10 };
   }
 }

 public dragEnter(args: IDragEnterEventArgs): void {
   let obj: NodeModel = args.element as NodeModel;
    console.log(obj);
   if (obj instanceof Node) {
     let oWidth: number = obj.width;
     let oHeight: number = obj.height;
     let ratio: number = 100 / obj.width;
     obj.width = 100;
     obj.height *= ratio;
     obj.offsetX += (obj.width - oWidth) / 2;
     obj.offsetY += (obj.height - oHeight) / 2;
     obj.style = { fill: '#fff', strokeColor: '58666E' };
   }
 }

 public getSymbolInfo(symbol: NodeModel): SymbolInfo {
   return { fit: true };
 }

 public getSymbolDefaults(symbol: NodeModel): void {
   if (symbol.id === 'Terminator' || symbol.id === 'Process') {
     symbol.width = 80;
     symbol.height = 40;
   } else if (
     symbol.id === 'Decision' ||
     symbol.id === 'Document' ||
     symbol.id === 'PreDefinedProcess' ||
     symbol.id === 'PaperTap' ||
     symbol.id === 'DirectData' ||
     symbol.id === 'MultiDocument' ||
     symbol.id === 'Data'
   ) {
     symbol.width = 50;
     symbol.height = 40;
   } else {
     symbol.width = 50;
     symbol.height = 50;
   }
 }


 public onClicked(args: ClickEventArgs): void {
    if (args.item.text === 'New') {
        this.diagram.clear();
    } else if (args.item.text === 'Load') {
        document.getElementsByClassName('e-file-select-wrap')[0].querySelector('button').click();
    } else if (args.item.id === 'palette-icon') {
        showPaletteIcon();
    } else if (args.item.text === 'Export') {
      this.download(this.diagram.saveDiagram());
    } else {
      const diagram_config = document.getElementById('diagram_diagramLayer');
      this.diagram.height = diagram_config.getBoundingClientRect().height;
      // this.diagram.width = diagram_config.getBoundingClientRect().width;
      this.saveOrUpdate(this.diagram.saveDiagram());
    }
  }

  public onUploadSuccess(args: { [key: string]: Object }): void {
    const file1: { [key: string]: Object } = args.file as { [key: string]: Object };
    const file: Blob = file1.rawFile as Blob;
    const reader: FileReader = new FileReader();
    reader.readAsText(file);

    reader.onloadend = this.loadManualDiagram.bind(this);
  }

  public loadManualDiagram(event: ProgressEvent): void {
    this.helperService.isProcessing(true);
    setTimeout(() => {
      this.diagramTmp.loadDiagram((event.target as FileReader).result.toString());
      this.diagram.nodes = Object.assign([], this.diagramTmp.nodes);
      this.diagram.connectors = Object.assign([], this.diagramTmp.connectors);
      this.helperService.isProcessing(false);
    }, 500);
  }

public saveOrUpdate(data: string): void {
  this.formSave.get('diagram').setValue(data);
  this.workFlowsService.saveOrUpdate(this.formSave.value).subscribe(res => {
    this.diagram.height = 700;
  });
}
public download(data: string): void {
  if (window.navigator.msSaveBlob) {
      const blob: Blob = new Blob([data], { type: 'data:text/json;charset=utf-8,' });
      window.navigator.msSaveOrOpenBlob(blob, 'Diagram.json');
  } else {
      const dataStr: string = 'data:text/json;charset=utf-8,' + encodeURIComponent(data);
      const a: HTMLAnchorElement = document.createElement('a');
      a.href = dataStr;
      a.download = 'Diagram.json';
      document.body.appendChild(a);
      a.click();
      a.remove();
  }
}
 public diagramCreate(args: Object): void {
  this.diagram.fitToPage();
   paletteIconClick();
 }
// #endregion "Diagram Properties"
 // ...
 public contextMenuOpen(args: DiagramBeforeMenuOpenEventArgs) {
  console.log(this.diagram.nodes);
    for (const item of args.items) {
      if (item.id === 'delete') {
        if (!this.diagram.selectedItems.nodes.length && !this.diagram.selectedItems.connectors.length) {
          args.hiddenItems.push(item.text);
        }
      }
    }
  }
  public contextMenuClick(args: MenuEventArgs): void {
    if (args.item.id === 'delete') {
      if ((this.diagram.selectedItems.nodes.length + this.diagram.selectedItems.connectors.length) > 0) {
        this.diagram.cut();
      }
    }
    if (args.item.id === 'url') {
      const modalRef = this.modalService.open(UrlConfigComponent, SMALL_MODAL_OPTIONS);
      modalRef.componentInstance.setFormValue(this.diagram.selectedItems.nodes[0].data);
      modalRef.result.then((result) => {
        if (result !== undefined) {
          for (const node of this.diagram.selectedItems.nodes) {
            node.data = result;
          }
        }
      });
    }
    if (args.item.id === 'styleConfig') {
      const modalRef = this.modalService.open(StyleConfigComponent, MEDIUM_MODAL_OPTIONS);
      modalRef.componentInstance.setFormValue(this.diagram.selectedItems.nodes, this.diagram);
      modalRef.result.then((result) => {
      });
    }
  }
}
function getPorts(obj: NodeModel): PointPortModel[] {
 const ports: PointPortModel[] = [
   { id: 'port1', shape: 'Circle', offset: { x: 0, y: 0.5 } },
   { id: 'port2', shape: 'Circle', offset: { x: 0.5, y: 1 } },
   { id: 'port3', shape: 'Circle', offset: { x: 1, y: 0.5 } },
   { id: 'port4', shape: 'Circle', offset: { x: 0.5, y: 0 } }
 ];
 return ports;
}
