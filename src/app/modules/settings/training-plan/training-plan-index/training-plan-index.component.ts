import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RESOURCE } from '@app/core';

@Component({
  selector: 'training-plan-index',
  templateUrl: './training-plan-index.component.html'
})
export class TrainingPlanIndexComponent extends BaseComponent implements OnInit {

  constructor(public act: ActivatedRoute) { 
    super(act, RESOURCE.TRAINING_PLAN);
  }

  ngOnInit() {
  }

}
