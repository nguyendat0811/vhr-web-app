import { TrainingPlanService } from './../../../../core/services/training-plan/training-plan.service';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { ACTION_FORM, SysCatService } from '@app/core';
import { RESOURCE, APP_CONSTANTS } from './../../../../core/app-config';
import { BaseComponent } from './../../../../shared/components/base-component/base-component.component';
import { AppComponent } from '@app/app.component';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ValidationService } from '@app/shared/services';

@Component({
  selector: 'training-plan-form',
  templateUrl: './training-plan-form.component.html'
})
export class TrainingPlanFormComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  trainingTypeList: any = {};
  formConfig = {
    trainingPlanId: [null],
    name: [null, [ValidationService.required, ValidationService.maxLength(200)]],
    documentNumber: [null, [ValidationService.required, ValidationService.maxLength(20)]],
    organization: [null, [ValidationService.required, ValidationService.maxLength(200)]],
    trainingType: [null, [ValidationService.required]],
    workProcessDate: [null, [ValidationService.required]],
    numOfEmployee: [null, [ValidationService.required, Validators.min(1), ValidationService.maxLength(10), ValidationService.integer]],
  };
  constructor(public actr: ActivatedRoute
    , public activeModal: NgbActiveModal
    , private trainingPlanService: TrainingPlanService
    , private sysCatService: SysCatService
    , private app: AppComponent
    ) {
      super(actr, RESOURCE.TRAINING_PLAN, ACTION_FORM.INSERT);
      this.formSave = this.buildForm({}, this.formConfig);
      this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.TRAINING_TYPE).subscribe(
        res => {
          this.trainingTypeList = res.data;

        }
      );
    }
  ngOnInit() {
  }

  get f () {
    return this.formSave.controls;
  }

  // public onTrainingType($event) {
  //   this.formSave.value.trainingType = $event;
  // }
    /**
   * processSaveOrUpdate
   */
  processSaveOrUpdate() {
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    } else {
      this.app.confirmMessage(null, () => {// on rejected
        console.log('this.formSave.value', this.formSave.value);
        this.trainingPlanService.saveOrUpdate(this.formSave.value)
        .subscribe(res => {
          if (this.trainingPlanService.requestIsSuccess(res)) {
            this.activeModal.close(res);
          }
        });
      }, () => {// on rejected

      });
    }
  }

  /**
   * setFormValue
   * param: Array propertyConfigs
   * param data
   */
  public setFormValue(propertyConfigs: any, data?: any) {
    this.propertyConfigs = propertyConfigs;
    if (data && data.trainingPlanId > 0) {
      this.formSave = this.buildForm(data, this.formConfig, ACTION_FORM.UPDATE);
    } else {
      this.formSave = this.buildForm({}, this.formConfig, ACTION_FORM.INSERT);
    }
  }
}
