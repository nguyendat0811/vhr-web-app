import { TrainingPlanIndexComponent } from './training-plan-index/training-plan-index.component';
import { CommonUtils } from './../../../shared/services/common-utils.service';
import { RESOURCE } from '@app/core';
import { PropertyResolver } from '@app/shared/services/property.resolver';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

const routes: Routes = [
  {
    path: '',
    component: TrainingPlanIndexComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.TRAINING_PLAN,
      nationId: CommonUtils.getNationId()
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TrainingPlanRoutingModule { }
