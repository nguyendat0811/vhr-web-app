import { TrainingPlanService } from './../../../../core/services/training-plan/training-plan.service';
import { DEFAULT_MODAL_OPTIONS } from '@app/core/app-config';
import { AppComponent } from '@app/app.component';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from './../../../../shared/components/base-component/base-component.component';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RESOURCE, ACTION_FORM, SysCatService, APP_CONSTANTS } from '@app/core';
import { Validators } from '@angular/forms';
import { ValidationService } from '@app/shared/services';
import { TrainingPlanFormComponent } from '../training-plan-form/training-plan-form.component';

@Component({
  selector: 'training-plan-search',
  templateUrl: './training-plan-search.component.html'
})
export class TrainingPlanSearchComponent extends BaseComponent implements OnInit {
  trainingTypeList: any = {};
  formConfig = {
    name: ['' , [ValidationService.maxLength(200)]],
    documentNumber: ['', [ValidationService.maxLength(20)]],
    organization: ['', [ValidationService.maxLength(200)]],
    trainingType: [''],
    workProcessDate: [''],
    numOfEmployee: ['', [Validators.min(1), ValidationService.maxLength(10), ValidationService.integer]],
  };
  constructor(private modalService: NgbModal
    , public actr: ActivatedRoute
    , private app: AppComponent
    , private sysCatService: SysCatService
    , private trainingPlanService: TrainingPlanService
    ) {
      super(actr, RESOURCE.TRAINING_PLAN, ACTION_FORM.SEARCH);
      this.setMainService(trainingPlanService);
      this.formSearch = this.buildForm({}, this.formConfig);
      this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.TRAINING_TYPE).subscribe(
        res => {
          this.trainingTypeList = res.data;
        }
      );
    }

  ngOnInit() {
    this.processSearch();
  }

  get f () {
    return this.formSearch.controls;
  }

  /**
   * prepareUpdate
   * param item
   */
  prepareSaveOrUpdate(item) {
    const modalRef = this.modalService.open(TrainingPlanFormComponent, DEFAULT_MODAL_OPTIONS);
    if (item && item.trainingPlanId > 0) {
      this.trainingPlanService.findOne(item.trainingPlanId)
        .subscribe(res => {
          modalRef.componentInstance.setFormValue(this.propertyConfigs, res.data);
        });
    } else {
      modalRef.componentInstance.setFormValue(this.propertyConfigs);
    }
    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
      if (this.trainingPlanService.requestIsSuccess(result)) {
        this.processSearch(null);
      }
      });
    }

    /**
     * prepareDelete
     * param item
     */
    processDelete(item) {
      if (item && item.trainingPlanId > 0) {
        this.app.confirmDelete(null, () => {// on accepted
          this.trainingPlanService.deleteById(item.trainingPlanId)
          .subscribe(res => {
            if (this.trainingPlanService.requestIsSuccess(res)) {
              this.processSearch(null);
            }
          });
        }, () => {// on rejected

        });
      }
    }
    public onChangeType (event) {
      console.log(event);
    }
}
