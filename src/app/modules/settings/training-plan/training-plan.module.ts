import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@app/shared';
import { TrainingPlanFormComponent } from './training-plan-form/training-plan-form.component';
import { TrainingPlanRoutingModule } from './training-plan-routing.module';
import { TrainingPlanSearchComponent } from './training-plan-search/training-plan-search.component';
import { TrainingPlanIndexComponent } from './training-plan-index/training-plan-index.component';

@NgModule({
  declarations: [
    TrainingPlanIndexComponent,
    TrainingPlanSearchComponent,
    TrainingPlanFormComponent
  ],
  imports: [
    CommonModule,
    SharedModule,

    TrainingPlanRoutingModule,
  ],
  entryComponents: [TrainingPlanFormComponent]
})
export class TrainingPlanModule { }
