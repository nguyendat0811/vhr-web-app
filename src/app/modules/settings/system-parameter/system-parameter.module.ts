import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@app/shared';

import { SystemParameterRoutingModule } from './system-parameter-routing.module';
import { SystemParameterPageComponent } from './pages/system-parameter-index/system-parameter-index.component';
import { SystemParameterSearchComponent } from './pages/system-parameter-search/system-parameter-search.component';
import { SystemParameterFormComponent } from './pages/system-parameter-form/system-parameter-form.component';

@NgModule({
  declarations: [SystemParameterPageComponent, SystemParameterSearchComponent, SystemParameterFormComponent],
  imports: [
    CommonModule,
    SharedModule,

    SystemParameterRoutingModule,
  ],
  entryComponents: [SystemParameterFormComponent]
})
export class SystemParameterModule { }
