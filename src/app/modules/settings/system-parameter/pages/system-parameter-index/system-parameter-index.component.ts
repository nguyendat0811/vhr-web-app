import { Component, OnInit } from '@angular/core';
import { SystemParameterService, RESOURCE } from '@app/core';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';

@Component({
  selector: 'system-parameter-page',
  templateUrl: './system-parameter-index.component.html'
})
export class SystemParameterPageComponent extends BaseComponent implements OnInit {
  resultList: any = {};
  constructor(public act: ActivatedRoute) { 
    super(act, RESOURCE.SYSTEM_PARAMETER);
  }

  ngOnInit() {
  }
}
