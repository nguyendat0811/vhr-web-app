import { ACTION_FORM } from '../../../../../core/app-config';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SystemParameterFormComponent } from '../system-parameter-form/system-parameter-form.component';
import { SystemParameterService } from '@app/core';
import { DEFAULT_MODAL_OPTIONS, RESOURCE } from '@app/core/app-config';
import { AppComponent } from '@app/app.component';
import { ValidationService, CommonUtils } from '@app/shared/services';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';

@Component({
  selector: 'system-parameter-search',
  templateUrl: './system-parameter-search.component.html',
})
export class SystemParameterSearchComponent extends BaseComponent implements OnInit {
  credentials: any = {};
  listParamUsed: any = [];
  @ViewChild('ptable') dataTable: any;
  formConfig = {
    code: ['', [ValidationService.maxLength(100)]],
    name: ['', [ValidationService.maxLength(100)]],
    value: ['', [ValidationService.maxLength(400)]],
    description: ['', [ValidationService.maxLength(200)]],
    effectiveStartDate: [''],
    effectiveEndDate: [''],
  };

  constructor(
      public actr: ActivatedRoute
    , private modalService: NgbModal
    , private systemParameterService: SystemParameterService
    , private app: AppComponent) {
    super(actr, RESOURCE.SYSTEM_PARAMETER, ACTION_FORM.SEARCH);
    this.formSearch = this.buildForm({}, this.formConfig, ACTION_FORM.SEARCH
      , [ ValidationService.notAffter('effectiveStartDate', 'effectiveEndDate', 'app.system-parameter.endDate')]);
    this.setMainService(systemParameterService);
    // Kiểm tra xem tham số nào đang được sử dụng , nếu được sử dụng thì lock lại phần delete!
    systemParameterService.listParamUsed()
      .subscribe(arg => {
        this.listParamUsed = arg.data;
      });
  }

  ngOnInit() {
    this.processSearch();
  }

  get f () {
    return this.formSearch.controls;
  }

  /**
   * prepareUpdate
   * param item
   */
  prepareSaveOrUpdate(item) {
    const modalRef = this.modalService.open(SystemParameterFormComponent, DEFAULT_MODAL_OPTIONS);
    if (item && item.systemParameterId > 0) {
      this.systemParameterService.findOne(item.systemParameterId)
        .subscribe(res => {
          modalRef.componentInstance.setFormValue(this.propertyConfigs, res.data);
        });
    } else {
      modalRef.componentInstance.setFormValue(this.propertyConfigs);
    }

    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
      if (this.systemParameterService.requestIsSuccess(result)) {
        this.processSearch(null);
      }
    });
  }
  /**
   * prepareDelete
   * param item
   */
  processDelete(item) {
    if (item && item.systemParameterId > 0) {
      if (this.listParamUsed.includes(item.code)) {
        this.systemParameterService.deleteById(item.systemParameterId)
        .subscribe(res => {
          if (this.systemParameterService.requestIsSuccess(res)) {
            this.processSearch(null);
          }
        });
      } else {
        this.app.confirmDelete(null, () => {// on accepted
          this.systemParameterService.deleteById(item.systemParameterId)
          .subscribe(res => {
            if (this.systemParameterService.requestIsSuccess(res)) {
              this.processSearch(null);
            }
          });
        }, () => {// on rejected
        });
      }
    }
  }


}
