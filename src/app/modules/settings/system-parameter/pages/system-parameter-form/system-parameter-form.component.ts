import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SystemParameterService, RESOURCE, ACTION_FORM } from '@app/core';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { AppComponent } from '@app/app.component';
import { ValidationService } from '@app/shared/services';

@Component({
  selector: 'system-parameter-form',
  templateUrl: './system-parameter-form.component.html',
})
export class SystemParameterFormComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  isDisabled: any;
  @Input() item: any;
  formConfig = {
      systemParameterId: [''],
      code: ['', [ValidationService.required, ValidationService.maxLength(100)]],
      name: ['', [ValidationService.required, ValidationService.maxLength(100)]],
      effectiveStartDate: [''],
      effectiveEndDate: [''],
      value: ['', [ValidationService.required, ValidationService.maxLength(400)]],
      description : ['', [ValidationService.maxLength(200)]]
  };
  constructor(public activeModal: NgbActiveModal
            , public actr: ActivatedRoute
            , private systemParameterService: SystemParameterService
            , private app: AppComponent) {
      super(actr, RESOURCE.SYSTEM_PARAMETER, ACTION_FORM.INSERT);
    //  this.formSave = this.buildForm({}, this.formConfig);
      this.buildFormOuterProcess({});
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
  }

  private buildFormOuterProcess(data?: any, actionForm?: ACTION_FORM) {
    this.formSave = this.buildForm(data, this.formConfig, actionForm,
      [ ValidationService.notAffter('effectiveStartDate', 'effectiveEndDate', 'app.system-parameter.endDate')]);
  }
  /**
   * processSaveOrUpdate
   */
  processSaveOrUpdate() {
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    } else {
      this.app.confirmMessage(null, () => {// on accepted
        this.systemParameterService.saveOrUpdate(this.formSave.value)
          .subscribe(res => {
            if (this.systemParameterService.requestIsSuccess(res)) {
              this.activeModal.close(res);
            }
          });
      }, () => {// on rejected
      });
    }
  }

  /****************** CAC HAM COMMON DUNG CHUNG ****/
  get f () {
    return this.formSave.controls;
  }

  /**
   * setFormValue
   * param data
   */
  public setFormValue(propertyConfigs: any, data?: any) {
    this.propertyConfigs = propertyConfigs;
    console.log('data in setFormValue', data);
    if (data && data.systemParameterId > 0) {
      this.isDisabled = true;
      console.log('data', data);
      this.buildFormOuterProcess(data, ACTION_FORM.UPDATE);
    //  this.formSave = this.buildForm(data, this.formConfig, ACTION_FORM.UPDATE);
    } else {
      this.isDisabled = false;
      this.buildFormOuterProcess({}, ACTION_FORM.INSERT);
    //  this.formSave = this.buildForm({}, this.formConfig, ACTION_FORM.INSERT);
    }
  }

}
