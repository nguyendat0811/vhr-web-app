import { CommonUtils } from '@app/shared/services/common-utils.service';
import { RESOURCE } from '../../../core/app-config';
import { PropertyResolver } from '@app/shared/services/property.resolver';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SystemParameterPageComponent } from './pages/system-parameter-index/system-parameter-index.component';

const routes: Routes = [
  {
    path: '',
    component: SystemParameterPageComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.SYSTEM_PARAMETER,
      nationId: CommonUtils.getNationId()
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SystemParameterRoutingModule { }
