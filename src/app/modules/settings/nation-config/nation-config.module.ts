import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@app/shared';

import { SysCatService, SysCatTypeService, NationService } from '@app/core';

import { IndexComponent } from './pages/index/index.component';
import { NationConfigSearchComponent} from './pages/nation-config-search/nation-config-search.component';
import { NationConfigAddComponent } from './pages/nation-config-add/nation-config-add.component';
import { NationConfigTypeComponent, FilterPipe } from './pages/nation-config-type/nation-config-type.component';
import { NationConfigTypeAddComponent} from './pages/nation-config-type-add/nation-config-type-add.component';
import { NationConfigRoutingModule } from './nation-config-routing.module';
import { NationConfigService } from '@app/core/services/nation/nation-config.service';
import { NationConfigTypeService } from '@app/core/services/nation/nation-config-type.service';

@NgModule({
  declarations: [
    IndexComponent, NationConfigSearchComponent, NationConfigAddComponent,
    NationConfigTypeComponent, NationConfigTypeAddComponent, FilterPipe, NationConfigSearchComponent
  ],
  imports: [
    /**
     * Required
     */
    CommonModule,
    SharedModule,

    NationConfigRoutingModule,
  ],
  entryComponents: [NationConfigAddComponent, NationConfigTypeAddComponent],
  providers: [
    NationConfigService, NationConfigTypeService, NationService
  ]

})
export class NationConfigModule { }
