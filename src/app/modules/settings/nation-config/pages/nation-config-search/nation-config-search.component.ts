import { BaseComponent } from './../../../../../shared/components/base-component/base-component.component';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { AppComponent } from '@app/app.component';
import { Component, OnInit, setTestabilityGetter, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
// import { NationConfigService, NationConfigTypeService } from '@app/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NationConfigAddComponent } from '../nation-config-add/nation-config-add.component';
import { ValidationService } from '@app/shared/services';
import { ActivatedRoute } from '@angular/router';
import { NationConfigTypeService } from '@app/core/services/nation/nation-config-type.service';
import { NationConfigService } from '@app/core/services/nation/nation-config.service';
import { RESOURCE, ACTION_FORM, NationService } from '@app/core';

@Component({
  selector: 'nation-config-search',
  templateUrl: './nation-config-search.component.html'
})
export class NationConfigSearchComponent extends BaseComponent implements OnInit {
  public commonUtils =  CommonUtils;
  listNation = [];
  constructor(
    public actr: ActivatedRoute,
    private modalService: NgbModal,
    private nationConfigService: NationConfigService,
    private nationConfigTypeService: NationConfigTypeService,
    private app: AppComponent,
    private nationService: NationService,
  ) {
    super(actr, RESOURCE.NATION_CONFIG, ACTION_FORM.SEARCH);
    this.setMainService(nationConfigService);
    this.formSearch = this.buildForm({}, {
      code: ['', [ValidationService.maxLength(50)]],
      name: ['', [ValidationService.maxLength(500)]],
      nationId: [''],
    });
    this.nationService.getNationList().subscribe(
      res => {
        this.listNation = res.data;
      },
    );
  }

  ngOnInit() {
    this.processSearch();
  }
  get f () {
    return this.formSearch.controls;
  }
  /**
   * prepare insert/update
   */
  prepareSaveOrUpdate(item): void {
    if (item && item.nationConfigId > 0) {
      this.nationConfigService.findOne(item.nationConfigId)
        .subscribe(res => {
          this.nationConfigTypeService.findOne(res.data.nationConfigTypeId)
            .subscribe(resSCType => {
              res.data.nationConfigTypeName = resSCType.data.name;
              this.activeFormModal(this.modalService, NationConfigAddComponent, res.data);
          });
        });

    } else {
      if (!this.formSearch.get('nationConfigTypeId')) {
        this.app.warningMessage('nationConfig.chooseNationConfigType', '');
        return;
      }
      this.nationConfigTypeService.findOne(this.formSearch.get('nationConfigTypeId').value)
        .subscribe(resSCType => {
          const data = {
            nationConfigTypeId  : resSCType.data.nationConfigTypeId,
            nationConfigTypeName: resSCType.data.name
          };
          this.activeFormModal(this.modalService, NationConfigAddComponent, data);
      });
    }
  }
}
