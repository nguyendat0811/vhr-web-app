import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from './../../../../../shared/components/base-component/base-component.component';
import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ValidationService } from '@app/shared/services';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { AppComponent } from '@app/app.component';
import { RESOURCE, ACTION_FORM, NationService } from '@app/core';
import { NationConfigService } from '@app/core/services/nation/nation-config.service';

@Component({
  selector: 'app-nation-config-add',
  templateUrl: './nation-config-add.component.html'
})
export class  NationConfigAddComponent extends BaseComponent implements OnInit {
  listNation = [];
  formSave: FormGroup;
  formConfig = {
    nationConfigId: [''],
    code: ['', [ValidationService.required, ValidationService.maxLength(50)]],
    name: ['', [ValidationService.required, ValidationService.maxLength(500)]],
    nationConfigTypeName: [''],
    nationConfigTypeId: [''],
    sortOrder: ['', [ValidationService.positiveInteger, Validators.max(1000)]],
    status: [''],
    description: ['', [ValidationService.maxLength(4000)]],
    nationId: ['', [ValidationService.required]]
  };
  constructor(
    public actr: ActivatedRoute,
    public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder,
    private nationConfigService: NationConfigService,
    private nationService: NationService,
    private app: AppComponent
  ) {
    super(actr, RESOURCE.NATION_CONFIG, ACTION_FORM.INSERT);
    this.formSave = this.buildForm({}, this.formConfig);
    this.nationService.getNationList().subscribe(
      res => {
        this.listNation = res.data;
      },
    );
  }

  ngOnInit() { }

  /**
   * processSaveOrUpdate
   */
  processSaveOrUpdate() {
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    }

    this.app.confirmMessage(null, () => {// on accepted
      console.log(this.formSave.value);
      this.nationConfigService.saveOrUpdate(this.formSave.value)
      .subscribe(res => {
        if (this.nationConfigService.requestIsSuccess(res)) {
          this.activeModal.close(res);
        }
      });
      }, () => {// on rejected
    });
  }

  /****************** CAC HAM COMMON DUNG CHUNG ****/
  get f () {
    return this.formSave.controls;
  }
  /**
   * setFormValue
   * param data
   */
  public setFormValue(propertyConfigs: any, data?: any) {
    this.propertyConfigs = propertyConfigs;
    if (data && data.nationConfigId > 0) {
      this.formSave = this.buildForm(data, this.formConfig, ACTION_FORM.UPDATE);
    } else {
      this.formSave = this.buildForm(data, this.formConfig, ACTION_FORM.INSERT);
    }
  }
}
