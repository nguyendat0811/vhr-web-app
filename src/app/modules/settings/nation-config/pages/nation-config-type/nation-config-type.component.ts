import { NationConfigTypeAddComponent} from '../nation-config-type-add/nation-config-type-add.component';
import { AppComponent } from '@app/app.component';
import { NationConfigSearchComponent } from '../nation-config-search/nation-config-search.component';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { Component, OnInit, Input, Pipe, PipeTransform } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SysCatTypeService, NationService } from '@app/core';
import { DEFAULT_MODAL_OPTIONS, RESOURCE } from '@app/core/app-config';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { NationConfigTypeService } from '@app/core/services/nation/nation-config-type.service';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-nation-config-type',
  templateUrl: './nation-config-type.component.html'
})
export class NationConfigTypeComponent extends BaseComponent implements OnInit {

  @Input()
  NationConfigSearch: NationConfigSearchComponent;
  resultList = [];
  paramUsedList = [];
  formSave: FormGroup;
  scTypeFillter = '';
  scTypeId: number;

  constructor(
    public actr: ActivatedRoute,
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
    private nationConfigTypeService: NationConfigTypeService,
    private nationService: NationService,
    private app: AppComponent
  ) {
    super(actr, RESOURCE.NATION_CONFIG);
    nationConfigTypeService.listParamUsed()
     .subscribe(arg => {
       this.paramUsedList = arg.data;
     });
  }


  ngOnInit() {
    this.processSearch();
  }

  processSearch(): void {
    this.nationConfigTypeService.searchAll().subscribe(res => {
      this.resultList = res.data;
      this.onSelectNationConfigType(res.data[0]);
    });
  }

  prepareSaveOrUpdate(item): void {
    if (item && item.nationConfigTypeId > 0) {
      this.nationConfigTypeService.findOne(item.nationConfigTypeId)
        .subscribe(res => {
          this.activeModelSave(res.data);
      });
    } else {
      this.activeModelSave();
    }
  }

  onSelectNationConfigType(item): void {
    this.scTypeId = item.nationConfigTypeId;
    this.NationConfigSearch.formSearch.removeControl('nationConfigTypeId');
    this.NationConfigSearch.formSearch.addControl('nationConfigTypeId', new FormControl(item.nationConfigTypeId));
    this.NationConfigSearch.processSearch(null);
  }
  processDelete(item) {
    if (item && item.nationConfigTypeId > 0) {
      if (this.paramUsedList.includes(item.code)) {
        this.nationConfigTypeService.deleteById(item.nationConfigTypeId)
        .subscribe(res => {
          if (this.nationConfigTypeService.requestIsSuccess(res)) {
            this.processSearch();
            this.NationConfigSearch.processSearch(null);
          }
        });
      } else {
        this.app.confirmDelete(null, () => {// on accepted
          this.nationConfigTypeService.deleteById(item.nationConfigTypeId)
          .subscribe(res => {
            if (this.nationConfigTypeService.requestIsSuccess(res)) {
              this.processSearch();
              this.NationConfigSearch.processSearch(null);
            }
          });
        }, () => {// on rejected
        });
      }
    }
  }

  private activeModelSave(data?: any) {
    const modalRef = this.modalService.open(NationConfigTypeAddComponent, DEFAULT_MODAL_OPTIONS);
    if (data) {
      modalRef.componentInstance.setFormValue(data);
    }
    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
      if (this.nationConfigTypeService.requestIsSuccess(result)) {
        this.processSearch();
      }
    });
  }
}

@Pipe({ name: 'filter' })
export class FilterPipe implements PipeTransform {
  transform(items: any[], searchText: string): any[] {
    if (!items) { return []; }
    if (!searchText) { return items; }
    searchText = searchText.toLowerCase();
    return items.filter( it => {
      return it.name.toLowerCase().includes(searchText);
    });
   }
}
