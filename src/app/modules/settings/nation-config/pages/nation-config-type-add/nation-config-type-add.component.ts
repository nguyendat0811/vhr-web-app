import { ValidationService } from '../../../../../shared/services/validation.service';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { AppComponent } from '../../../../../app.component';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { SysCatTypeService, RESOURCE, ACTION_FORM } from '@app/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { NationConfigTypeService } from '@app/core/services/nation/nation-config-type.service';

@Component({
  selector: 'app-nation-config-type-add',
  templateUrl: './nation-config-type-add.component.html'
})
export class NationConfigTypeAddComponent extends BaseComponent implements OnInit {

  formSave: FormGroup;
  formConfig = {
    nationConfigTypeId: [''],
    code: ['', [ValidationService.required, ValidationService.maxLength(20)]],
    name: ['', [ValidationService.required, ValidationService.maxLength(200)]]
  };
  constructor(
    public actr: ActivatedRoute,
    public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder,
    private app: AppComponent,
    private nationConfigTypeService: NationConfigTypeService,
  ) {
    super(actr, RESOURCE.NATION_CONFIG, ACTION_FORM.INSERT);
    this.formSave = this.buildForm({}, this.formConfig);
  }

  ngOnInit() {
  }

  /**
   * processSaveOrUpdate
   */
  processSaveOrUpdate() {
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    }
    this.app.confirmMessage(null, () => {// on accepted
      this.nationConfigTypeService.saveOrUpdate(this.formSave.value)
      .subscribe(res => {
        if (this.nationConfigTypeService.requestIsSuccess(res) ) {
          this.activeModal.close(res);
        }
      });
      }, () => {// on rejected
    });
  }

  /****************** CAC HAM COMMON DUNG CHUNG ****/
  get f () {
    return this.formSave.controls;
  }
  // /**
  //  * buildForm
  //  */
  // private buildForm(): void {
  //   this.formSave = this.formBuilder.group({
  //     sysCatTypeId: [''],
  //     code: ['', [ValidationService.required, ValidationService.maxLength(50)]],
  //     name: ['', [ValidationService.required, ValidationService.maxLength(200)]]
  //   });
  // }
  /**
   * setFormValue
   * param data
   */
  public setFormValue(data: any) {
    this.formSave.setValue(CommonUtils.copyProperties(this.formSave.value, data));
  }
}
