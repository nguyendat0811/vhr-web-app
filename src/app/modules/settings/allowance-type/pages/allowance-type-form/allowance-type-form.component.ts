import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AllowanceTypeService, RESOURCE, ACTION_FORM } from '@app/core';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { AppComponent } from '@app/app.component';
import { ValidationService } from '@app/shared/services';

@Component({
  selector: 'allowance-type-form',
  templateUrl: './allowance-type-form.component.html',
})
export class AllowanceTypeFormComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  @Input() item: any;
  formConfig = {
    allowanceTypeId: [''],
      code: ['', [ValidationService.required, ValidationService.maxLength(50)]],
      name: ['', [ValidationService.required, ValidationService.maxLength(200)]],
      description : ['', [ValidationService.maxLength(1000)]]
  };
  constructor(public activeModal: NgbActiveModal
            , public actr: ActivatedRoute
            , private allowanceTypeService: AllowanceTypeService
            , private app: AppComponent) {
      super(actr, RESOURCE.ALLOWANCE_TYPE, ACTION_FORM.INSERT);
      this.formSave = this.buildForm({}, this.formConfig);
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
  }

  /**
   * processSaveOrUpdate
   */
  processSaveOrUpdate() {
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    } else {
      this.app.confirmMessage(null, () => {// on accepted
        this.allowanceTypeService.saveOrUpdate(this.formSave.value)
          .subscribe(res => {
            if (this.allowanceTypeService.requestIsSuccess(res)) {
              this.activeModal.close(res);
            }
          });
      }, () => {// on rejected
      });
    }
  }

  /****************** CAC HAM COMMON DUNG CHUNG ****/
  get f () {
    return this.formSave.controls;
  }

  /**
   * setFormValue
   * param data
   */
  public setFormValue(propertyConfigs: any, data?: any) {
    this.propertyConfigs = propertyConfigs;
    if (data && data.allowanceTypeId > 0) {
      this.formSave = this.buildForm(data, this.formConfig, ACTION_FORM.UPDATE);
    } else {
      this.formSave = this.buildForm({}, this.formConfig, ACTION_FORM.INSERT);
    }
  }

}
