import { ACTION_FORM } from './../../../../../core/app-config';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AllowanceTypeFormComponent } from '../allowance-type-form/allowance-type-form.component';
import { AllowanceTypeService } from '@app/core';
import { DEFAULT_MODAL_OPTIONS, RESOURCE } from '@app/core/app-config';
import { AppComponent } from '@app/app.component';
import { ValidationService, CommonUtils } from '@app/shared/services';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';

@Component({
  selector: 'allowance-type-search',
  templateUrl: './allowance-type-search.component.html',
})
export class AllowanceTypeSearchComponent extends BaseComponent implements OnInit {
  credentials: any = {};
  formConfig = {
    code: ['', [ValidationService.maxLength(50)]],
    name: ['', [ValidationService.maxLength(200)]],
    description: ['', [ValidationService.maxLength(1000)]],
  };

  constructor(
      public actr: ActivatedRoute
    , private modalService: NgbModal
    , private allowanceTypeService: AllowanceTypeService
    , private app: AppComponent) {
    super(actr, RESOURCE.ALLOWANCE_TYPE, ACTION_FORM.SEARCH);
    this.setMainService(allowanceTypeService);
    this.formSearch = this.buildForm({}, this.formConfig);
  }

  ngOnInit() {
    this.processSearch();
  }

  get f () {
    return this.formSearch.controls;
  }

  /**
   * prepareUpdate
   * param item
   */
  prepareSaveOrUpdate(item) {
    const modalRef = this.modalService.open(AllowanceTypeFormComponent, DEFAULT_MODAL_OPTIONS);
    if (item && item.allowanceTypeId > 0) {
      this.allowanceTypeService.findOne(item.allowanceTypeId)
        .subscribe(res => {
          modalRef.componentInstance.setFormValue(this.propertyConfigs, res.data);
        });
    } else {
      modalRef.componentInstance.setFormValue(this.propertyConfigs);
    }

    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
      if (this.allowanceTypeService.requestIsSuccess(result)) {
        this.processSearch(null);
      }
    });
  }
  /**
   * prepareDelete
   * param item
   */
  processDelete(item) {
    console.log(item);
    if (item && item.allowanceTypeId > 0) {
      this.app.confirmDelete(null, () => {// on accepted
        this.allowanceTypeService.deleteById(item.allowanceTypeId)
        .subscribe(res => {
          if (this.allowanceTypeService.requestIsSuccess(res)) {
            this.processSearch(null);
          }
        });
      }, () => {// on rejected

      });
    }
  }
}
