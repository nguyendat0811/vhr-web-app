import { Component, OnInit } from '@angular/core';
import { AllowanceTypeService, RESOURCE } from '@app/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'allowance-type-page',
  templateUrl: './allowance-type-index.component.html'
})
export class AllowanceTypePageComponent extends BaseComponent implements OnInit {
  resultList: any = {};
  constructor(public act: ActivatedRoute) { 
    super(act, RESOURCE.ALLOWANCE_TYPE);
  }

  ngOnInit() {
  }
}
