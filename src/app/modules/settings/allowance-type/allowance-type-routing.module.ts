import { CommonUtils } from '@app/shared/services/common-utils.service';
import { RESOURCE } from './../../../core/app-config';
import { PropertyResolver } from '@app/shared/services/property.resolver';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllowanceTypePageComponent } from './pages/allowance-type-index/allowance-type-index.component';

const routes: Routes = [
  {
    path: '',
    component: AllowanceTypePageComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.ALLOWANCE_TYPE,
      nationId: CommonUtils.getNationId()
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AllowanceTypeRoutingModule { }
