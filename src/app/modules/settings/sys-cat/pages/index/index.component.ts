import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { RESOURCE } from '@app/core';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html'
})
export class IndexComponent extends BaseComponent implements OnInit {

  constructor(public act: ActivatedRoute) { 
    super(act, RESOURCE.SYS_CAT);
  }

  ngOnInit() {
  }

}
