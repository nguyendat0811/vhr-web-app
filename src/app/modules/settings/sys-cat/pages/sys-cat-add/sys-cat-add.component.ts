import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from './../../../../../shared/components/base-component/base-component.component';
import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SysCatService } from '@app/core/services';
import { ValidationService } from '@app/shared/services';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { AppComponent } from '@app/app.component';
import { RESOURCE, ACTION_FORM } from '@app/core';

@Component({
  selector: 'app-sys-cat-add',
  templateUrl: './sys-cat-add.component.html'
})
export class SysCatAddComponent extends BaseComponent implements OnInit {

  formSave: FormGroup;
  formConfig = {
    sysCatId: [''],
    code: ['', [ValidationService.required, ValidationService.maxLength(50)]],
    name: ['', [ValidationService.required, ValidationService.maxLength(500)]],
    sysCatTypeName: [''],
    sysCatTypeId: [''],
    sortOrder: ['', [ValidationService.positiveInteger, Validators.max(1000)]],
    status: [''],
    description: ['', [ValidationService.maxLength(4000)]]
  };
  constructor(
    public actr: ActivatedRoute,
    public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder,
    private sysCatService: SysCatService,
    private app: AppComponent
  ) {
    super(actr, RESOURCE.SYS_CAT, ACTION_FORM.INSERT);
    this.formSave = this.buildForm({}, this.formConfig);
  }

  ngOnInit() { }

  /**
   * processSaveOrUpdate
   */
  processSaveOrUpdate() {
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    }

    this.app.confirmMessage(null, () => {// on accepted
      console.log(this.formSave.value);
      this.sysCatService.saveOrUpdate(this.formSave.value)
      .subscribe(res => {
        if (this.sysCatService.requestIsSuccess(res)) {
          this.activeModal.close(res);
        }
      });
      }, () => {// on rejected
    });
  }

  /****************** CAC HAM COMMON DUNG CHUNG ****/
  get f () {
    return this.formSave.controls;
  }
  /**
   * setFormValue
   * param data
   */
  public setFormValue(propertyConfigs: any, data?: any) {
    this.propertyConfigs = propertyConfigs;
    if (data && data.sysCatId > 0) {
      this.formSave = this.buildForm(data, this.formConfig, ACTION_FORM.UPDATE);
    } else {
      this.formSave = this.buildForm(data, this.formConfig, ACTION_FORM.INSERT);
    }
  }
}
