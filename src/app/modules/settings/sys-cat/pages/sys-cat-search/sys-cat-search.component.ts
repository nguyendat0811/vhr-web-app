import { BaseComponent } from './../../../../../shared/components/base-component/base-component.component';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { AppComponent } from '@app/app.component';
import { Component, OnInit, setTestabilityGetter, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SysCatService, SysCatTypeService } from '@app/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SysCatAddComponent } from '../sys-cat-add/sys-cat-add.component';
import { DEFAULT_MODAL_OPTIONS, RESOURCE, ACTION_FORM } from '@app/core/app-config';
import { ValidationService } from '@app/shared/services';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'sys-cat-search',
  templateUrl: './sys-cat-search.component.html'
})
export class SysCatSearchComponent extends BaseComponent implements OnInit {
  public commonUtils =  CommonUtils;
  constructor(
    public actr: ActivatedRoute,
    private modalService: NgbModal,
    private sysCatService: SysCatService,
    private sysCatTypeService: SysCatTypeService,
    private app: AppComponent
  ) {
    super(actr, RESOURCE.SYS_CAT, ACTION_FORM.SEARCH);
    this.setMainService(sysCatService);
    this.formSearch = this.buildForm({}, {
      code: ['', [ValidationService.maxLength(50)]],
      name: ['', [ValidationService.maxLength(500)]],
    });
  }

  ngOnInit() {
    this.processSearch();
  }
  get f () {
    return this.formSearch.controls;
  }
  /**
   * prepare insert/update
   */
  prepareSaveOrUpdate(item): void {
    if (item && item.sysCatId > 0) {
      this.sysCatService.findOne(item.sysCatId)
        .subscribe(res => {
          this.sysCatTypeService.findOne(res.data.sysCatTypeId)
            .subscribe(resSCType => {
              res.data.sysCatTypeName = resSCType.data.name;
              this.activeFormModal(this.modalService, SysCatAddComponent, res.data);
          });
        });
    } else {
      if (!this.formSearch.get('sysCatTypeId')) {
        this.app.warningMessage('sysCat.chooseSysCatType', '');
        return;
      }
      this.sysCatTypeService.findOne(this.formSearch.get('sysCatTypeId').value)
        .subscribe(resSCType => {
          const data = {
            sysCatTypeId  : resSCType.data.sysCatTypeId,
            sysCatTypeName: resSCType.data.name
          };
          this.activeFormModal(this.modalService, SysCatAddComponent, data);
      });
    }
  }
}
