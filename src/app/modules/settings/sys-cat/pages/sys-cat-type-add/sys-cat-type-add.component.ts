import { ValidationService } from './../../../../../shared/services/validation.service';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { AppComponent } from './../../../../../app.component';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { SysCatTypeService, RESOURCE, ACTION_FORM } from '@app/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-sys-cat-type-add',
  templateUrl: './sys-cat-type-add.component.html'
})
export class SysCatTypeAddComponent extends BaseComponent implements OnInit {

  formSave: FormGroup;
  formConfig = {
    sysCatTypeId: [''],
    code: ['', [ValidationService.required, ValidationService.maxLength(50)]],
    name: ['', [ValidationService.required, ValidationService.maxLength(200)]]
  };
  constructor(
    public actr: ActivatedRoute,
    public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder,
    private sysCatTypeService: SysCatTypeService,
    private app: AppComponent
  ) {
    super(actr, RESOURCE.SYS_CAT, ACTION_FORM.SYS_CAT_TYPE_INSERT);
    this.formSave = this.buildForm({}, this.formConfig);
  }

  ngOnInit() {
  }

  /**
   * processSaveOrUpdate
   */
  processSaveOrUpdate() {
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    }

    this.app.confirmMessage(null, () => {// on accepted
      this.sysCatTypeService.saveOrUpdate(this.formSave.value)
      .subscribe(res => {
        if (this.sysCatTypeService.requestIsSuccess(res) ) {
          this.activeModal.close(res);
        }
      });
      }, () => {// on rejected
    });
  }

  /****************** CAC HAM COMMON DUNG CHUNG ****/
  get f () {
    return this.formSave.controls;
  }
  // /**
  //  * buildForm
  //  */
  // private buildForm(): void {
  //   this.formSave = this.formBuilder.group({
  //     sysCatTypeId: [''],
  //     code: ['', [ValidationService.required, ValidationService.maxLength(50)]],
  //     name: ['', [ValidationService.required, ValidationService.maxLength(200)]]
  //   });
  // }

    /**
  * buildForms
  */
  private buildForms(data?: any, actionForm?: ACTION_FORM) {
    this.formSave = this.buildForm(data, this.formConfig, actionForm);
  }
  /**
   * setFormValue
   * param data
   */
  public setFormValue(propertyConfigs: any, data: any) {
    this.propertyConfigs = propertyConfigs;
    if (data && data.sysCatTypeId > 0) {
      this.buildForms(data, ACTION_FORM.SYS_CAT_TYPE_UPDATE);
    } else {
      this.buildForms(data, ACTION_FORM.SYS_CAT_TYPE_INSERT);
    }
    // this.formSave.setValue(CommonUtils.copyProperties(this.formSave.value, data));
  }
}
