import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './pages/index/index.component';
import { PropertyResolver } from '@app/shared/services/property.resolver';
import { RESOURCE } from '@app/core';
import { CommonUtils } from '@app/shared/services';

const routes: Routes = [
  {
    path: '',
    component: IndexComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.SYS_CAT,
      nationId: CommonUtils.getNationId()
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SysCatRoutingModule { }
