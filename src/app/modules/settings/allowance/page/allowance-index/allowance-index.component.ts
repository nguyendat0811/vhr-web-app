import { Component, OnInit } from '@angular/core';
import { AllowanceService, RESOURCE } from '@app/core';
import { CommonUtils } from '@app/shared/services';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'allowance',
  templateUrl: './allowance-index.component.html'
})
export class AllowanceIndexComponent  extends BaseComponent  implements OnInit {
  resultList: any = {};
  constructor(public act: ActivatedRoute) { 
    super(act, RESOURCE.ALLOWANCE);
  }

  ngOnInit() {

  }

}
