import { ACTION_FORM } from './../../../../../core/app-config';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AllowanceFormComponent } from '../allowance-form/allowance-form.component';
import { AllowanceService } from '@app/core';
import { DEFAULT_MODAL_OPTIONS, APP_CONSTANTS, RESOURCE } from '@app/core/app-config';
import { AppComponent } from '@app/app.component';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { SysCatService } from '@app/core';
import { ValidationService } from '@app/shared/services';
import { CommonUtils } from '@app/shared/services/common-utils.service';

@Component({
  selector: 'allowance-search',
  templateUrl: './allowance-search.component.html',
})
export class AllowanceSearchComponent extends BaseComponent implements OnInit {
  credentials: any = {};
  fundList: any = {};
  typeIdList: any = {};
  checkFactor: boolean;
  checkMoney: boolean;
  allowanceTypeList: any = {};

  formConfig = {
    typeCode: [null],
    typeName: [null],
    code: ['', [ValidationService.maxLength(20)]],
    name: ['', [ValidationService.maxLength(100)]],
    typeId: [null],
    allowanceTypeId: [null],
    factor: [null, [ValidationService.positiveNumber, Validators.minLength(0), ValidationService.maxLength(6)]],
    amountMoney: [null, [ValidationService.positiveNumber, Validators.minLength(0), ValidationService.maxLength(12)]],
  };

  constructor(
      public actr: ActivatedRoute
    , private modalService: NgbModal
    , private allowanceService: AllowanceService
    , private app: AppComponent
    , private sysCatService: SysCatService
      ) {
    super(actr, RESOURCE.ALLOWANCE, ACTION_FORM.SEARCH);
    this.setMainService(allowanceService);
    this.formSearch = this.buildForm({}, this.formConfig);
    this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.ALLOWANCE)
    .subscribe( res =>
        this.fundList = res.data);
    this.allowanceService.getAllowanceType().subscribe(data => {
        this.allowanceTypeList = data.data;
      });
    this.checkFactor = true;
    this.checkMoney = true;
    this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.ALLOWANCE).subscribe(
      res => this.typeIdList = res.data
    );
  }
  ngOnInit() {
    this.processSearch();
  }

  get f () {
    return this.formSearch.controls;
  }

  convertFromIdToName(typeId: number) {
    this.fundList.array.forEach(element => {
      if (element.code === typeId) { return element.name; }
    });
  }

  /**
   * prepareUpdate
   * param item
   */
  prepareSaveOrUpdate(item) {
    const modalRef = this.modalService.open(AllowanceFormComponent, DEFAULT_MODAL_OPTIONS);
    if (item && item.allowanceId > 0) {
      this.allowanceService.findOne(item.allowanceId)
        .subscribe(res => {
          modalRef.componentInstance.setFormValue(this.propertyConfigs, res.data);
        });
    } else {
      modalRef.componentInstance.setFormValue(this.propertyConfigs);
    }

    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
      if (this.allowanceService.requestIsSuccess(result)) {
        this.processSearch();
      }
    });
  }
  public onAllowanceTypeChange($event) {
    this.formSearch.value.allowanceTypeId = $event;
  }

  /**
   * prepareDelete
   * param item
   */
  processDelete(item) {
    console.log(item);
    if (item && item.allowanceId > 0) {
      this.app.confirmDelete(null, () => {// on accepted
        this.allowanceService.deleteById(item.allowanceId)
        .subscribe(res => {
          if (this.allowanceService.requestIsSuccess(res)) {
            this.processSearch(null);
          }
        });
      }, () => {// on rejected

      });
    }
  }
}
