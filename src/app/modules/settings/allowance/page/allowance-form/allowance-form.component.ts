import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AllowanceService, RESOURCE, ACTION_FORM, SysCatService, APP_CONSTANTS } from '@app/core';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { AppComponent } from '@app/app.component';
import { ValidationService } from '@app/shared/services';
@Component({
  selector: 'allowance-form',
  templateUrl: './allowance-form.component.html',
})
export class AllowanceFormComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  allowanceTypeList: any = {};
  typeIdList: any = {};
  checkFactor: boolean;
  checkMoney: boolean;
  isInsOrUp: boolean;
  @Input() item: any;
  formConfig = {
      allowanceId: [''],
      typeId: ['', [ValidationService.required]],
      allowanceTypeId: ['', [ValidationService.required]],
      code: ['', [ValidationService.required, ValidationService.maxLength(20)]],
      name: ['', [ValidationService.required, ValidationService.maxLength(100)]],
      factor: [null, [ValidationService.number, Validators.min(0), ValidationService.maxLength(6)]],
      amountMoney: [null, [ValidationService.number, Validators.min(0), ValidationService.maxLength(12)]],
  };
  constructor(public activeModal: NgbActiveModal
            , public actr: ActivatedRoute
            , private allowanceService: AllowanceService
            , private app: AppComponent
            , private sysCatService: SysCatService
            ) {
      super(actr, RESOURCE.ALLOWANCE, ACTION_FORM.INSERT);
      this.buildForms({}, ACTION_FORM.INSERT);
      this.allowanceService.getAllowanceType().subscribe(data => {
          this.allowanceTypeList = data.data;
        });
      this.checkFactor = true;
      this.checkMoney = true;
      this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.ALLOWANCE).subscribe(
        res => {
          this.typeIdList = res.data;
        }
      );
  }
  /**
   * ngOnInit
   */
  ngOnInit() {
  }
  public onAllowanceTypeChange($event) {
    this.formSave.value.allowanceTypeId = $event;
  }

  public onTypeIdChange($event) {
    let codeType = ' ';
    this.checkFactor = true;
    this.checkMoney = true;
    this.typeIdList.forEach(element => {
      if (element.sysCatId === $event) {
        codeType = element.code;
      }
    });
    if (codeType === APP_CONSTANTS.ALLOWANCE.THEOSOTIEN) {
      this.checkMoney = false;
      let moneyValue = this.formSave.get('amountMoney').value;
      if (moneyValue === 0) {
        moneyValue = '0';
      }
      this.formSave.get('factor').setValue(null);
      this.formSave.removeControl('amountMoney');
      this.formSave.removeControl('factor');
      this.formSave.addControl('amountMoney',
      CommonUtils.createControl(this.actionForm, 'amountMoney', moneyValue,
      [ValidationService.required, ValidationService.number, Validators.min(0), ValidationService.maxLength(12)], this.propertyConfigs));
      this.formSave.addControl('factor',
      CommonUtils.createControl(this.actionForm, 'factor', null, null, this.propertyConfigs));
    } else if (codeType === APP_CONSTANTS.ALLOWANCE.THEOSOTIEN_LH) {
      this.checkFactor = true;
      this.checkMoney = true;
      this.formSave.get('amountMoney').setValue(null);
      this.formSave.get('factor').setValue(null);
      this.formSave.removeControl('amountMoney');
      this.formSave.removeControl('factor');
      this.formSave.addControl('amountMoney',
       CommonUtils.createControl(this.actionForm, 'amountMoney', null, null, this.propertyConfigs));
      this.formSave.addControl('factor', CommonUtils.createControl(this.actionForm, 'factor', null, null, this.propertyConfigs));
    } else if (codeType === APP_CONSTANTS.ALLOWANCE.LUONGBAOHIEM || codeType === APP_CONSTANTS.ALLOWANCE.LUONGCHUCDANH) {
      this.checkFactor = false;
      let factorValue = this.formSave.get('factor').value;
      if (factorValue === 0) {
        factorValue = '0';
      }
      this.formSave.get('amountMoney').setValue(null);
      this.formSave.removeControl('amountMoney');
      this.formSave.removeControl('factor');
      this.formSave.addControl('factor',
      CommonUtils.createControl(this.actionForm, 'factor', factorValue,
      [ValidationService.required, ValidationService.number, Validators.min(0), ValidationService.maxLength(6)], this.propertyConfigs));
      this.formSave.addControl('amountMoney',
      CommonUtils.createControl(this.actionForm, 'amountMoney', null, null, this.propertyConfigs));
    } else {
      this.checkFactor = true;
      this.checkMoney = true;
      this.formSave.get('amountMoney').setValue(null);
      this.formSave.get('factor').setValue(null);
      this.formSave.removeControl('amountMoney');
      this.formSave.removeControl('factor');
      this.formSave.addControl('amountMoney', CommonUtils.createControl(this.actionForm, 'amountMoney', null, null, this.propertyConfigs));
      this.formSave.addControl('factor', CommonUtils.createControl(this.actionForm, 'factor', null, null, this.propertyConfigs));
    }
    this.formSave.value.typeId = $event;
  }

  insert(str, index, value) {
    return str.substr(0, index) + value + str.substr(index);
}
  /**
  * buildFormInnerProcess
  */
 private buildForms(data?: any, actionForm?: ACTION_FORM) {
  this.formSave = this.buildForm(data, this.formConfig, actionForm);
}
  /**
   * processSaveOrUpdate
   */
  processSaveOrUpdate() {
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    } else {
      this.app.confirmMessage(null, () => {// on accepted
        // if (!this.formSave.value.factor.includes('.') && this.formSave.value.factor.length > 3) {
        //   this.formSave.value.factor = this.formSave.value.factor.substr(0, this.formSave.value.factor.length - 3) + ',' +
        //    this.formSave.value.factor.substr(this.formSave.value.factor.length - 3;
        //   console.log('this.formSave.value.factor', this.formSave.value.factor);
        // }
        this.allowanceService.saveOrUpdateFormFile(this.formSave.value)
          .subscribe(res => {
            if (this.allowanceService.requestIsSuccess(res)) {
              this.activeModal.close(res);
            }
          });
      }, () => {// on rejected
      });
    }
  }

  /****************** CAC HAM COMMON DUNG CHUNG ****/
  get f () {
    return this.formSave.controls;
  }

  /**
   * setFormValue
   * param data
   */
  public setFormValue(propertyConfigs: any, data?: any) {
    this.propertyConfigs = propertyConfigs;
    if (data && data.allowanceId > 0) {
      this.buildForms(data, ACTION_FORM.UPDATE);
      this.isInsOrUp = true;
      if (data.typeId) {
        this.onTypeIdChange(data.typeId);
      }
    } else {
      this.buildForms({}, ACTION_FORM.INSERT);
      this.isInsOrUp = false;
    }
  }

}
