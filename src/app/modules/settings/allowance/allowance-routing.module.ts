import { CommonUtils } from '../../../shared/services/common-utils.service';
import { RESOURCE } from '@app/core/app-config';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllowanceIndexComponent } from './page/allowance-index/allowance-index.component';
import { PropertyResolver } from '@app/shared/services/property.resolver';


const routes: Routes = [
  {
    path: '',
    component: AllowanceIndexComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.ALLOWANCE,
      nationId: CommonUtils.getNationId()
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AllowanceRoutingModule { }
