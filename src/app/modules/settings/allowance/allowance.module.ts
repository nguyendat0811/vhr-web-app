import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@app/shared';
import { AllowanceIndexComponent } from './page/allowance-index/allowance-index.component';
import { AllowanceSearchComponent } from './page/allowance-search/allowance-search.component';
import { AllowanceFormComponent } from './page/allowance-form/allowance-form.component';
import {AllowanceRoutingModule} from './allowance-routing.module';
 @NgModule({
  declarations: [AllowanceIndexComponent, AllowanceSearchComponent, AllowanceFormComponent],
  imports: [
    CommonModule,
    SharedModule,

    AllowanceRoutingModule
  ],
  entryComponents: [AllowanceFormComponent],

})
export class AllowanceModule { }
