import { NationPropertyService } from './../../../../core/services/nation/nation-property.service';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { Component, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NationService } from '@app/core';
import { NationFormComponent } from '../nation-form/nation-form.component';
import { DEFAULT_MODAL_OPTIONS, RESOURCE, ACTION_FORM } from '@app/core/app-config';
import { ValidationService } from '@app/shared/services';
import { AppComponent } from '@app/app.component';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { NationConfigComponent } from '../nation-config/nation-config.component';
import { HrStorage } from '@app/core/services/HrStorage';

@Component({
    selector: 'nation-search',
    templateUrl: './nation-search.component.html'
})

export class NationSearchComponent extends BaseComponent implements OnInit {
  credentials: any = {};
  formConfig = {
    code: [''],
    name: [''],
    isDefault: [''],
    requirePersionalId: [''],
    description: [''],
    phoneAreaCode: [''],
    icon: ['']
  };
  constructor(public actr: ActivatedRoute,
              private modalService: NgbModal,
              private nationService: NationService,
              private nationPropertyService: NationPropertyService,
              private app: AppComponent) {
          super(actr, RESOURCE.NATION, ACTION_FORM.SEARCH);
          this.setMainService(nationService);
          this.formSearch = this.buildForm({}, this.formConfig);
  }
  ngOnInit() {
    this.processSearch();
  }

  get f () {
    return this.formSearch.controls;
  }

  // Thêm mới hoặc update nation
  prepareInsertOrUpdate(item) {
    // Update nation
    if (item && item.nationId > 0) {
      this.nationService.findOne(item.nationId)
        .subscribe(res => {
          const modalRef = this.modalService.open(NationFormComponent, DEFAULT_MODAL_OPTIONS);
          modalRef.componentInstance.setFormValue(this.propertyConfigs, res.data);
          modalRef.result.then((result) => {
            if (!result) {
              return;
            }
            if (this.nationService.requestIsSuccess(result)) {
              this.processSearch(null);
            }
          });
        });
    } else {    // Insert nation
      const modalRef = this.modalService.open(NationFormComponent, DEFAULT_MODAL_OPTIONS);
      modalRef.componentInstance.setFormValue(this.propertyConfigs);
      modalRef.result.then((result) => {
        if (!result) {
          return;
        }
        if (this.nationService.requestIsSuccess(result)) {
          this.processSearch(null);
        }
      });
    }
  }
  /**
   * Cau hinh Nation
   */
  config(item) {
    const modalRef = this.modalService.open(NationConfigComponent, DEFAULT_MODAL_OPTIONS);
    this.nationService.findOne(item.nationId)
      .subscribe(res => {
        modalRef.componentInstance.setFormValue(this.propertyConfigs, res.data);
      });

    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
      if (this.nationService.requestIsSuccess(result)) {
        this.processSearch(null);
      }
    });
  }

  processDelete(item) {
    if (item && item.nationId > 0) {
      this.app.confirmDelete(null, () => {
        // on accepted
        this.nationService.deleteById(item.nationId)
        .subscribe(
          res => {
            if (this.nationService.requestIsSuccess(res)) {
              this.processSearch(null);
            }
          },
          error => {
            this.app.requestIsError();
          },
          () => {
            // No errors, route to new page
          }
        );
      }, () => {// on rejected

      });
    }
  }
}
