import { Component, OnInit } from '@angular/core';
import { NationService, RESOURCE } from '@app/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'nation-index',
  templateUrl: './nation-index.component.html'
})
export class NationIndexComponent extends BaseComponent  implements OnInit {
  resultList: any = {};
  constructor(public act: ActivatedRoute) { 
    super(act, RESOURCE.NATION);
  }

  ngOnInit() {
  }
}
