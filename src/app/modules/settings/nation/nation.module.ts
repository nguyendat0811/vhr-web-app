import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@app/shared';
import { ReactiveFormsModule } from '@angular/forms';

import { NationRoutingModule } from './nation-routing.module';
import { NationIndexComponent } from './nation-index/nation-index.component';
import { NationSearchComponent } from './nation-search/nation-search.component';
import { NationFormComponent } from './nation-form/nation-form.component';
import { NationConfigComponent } from './nation-config/nation-config.component';

@NgModule({
    declarations: [
        NationIndexComponent
      , NationSearchComponent
      , NationFormComponent
      , NationConfigComponent
    ],
    imports: [
      CommonModule,
      SharedModule,
      ReactiveFormsModule,
      NationRoutingModule,
    ],
    entryComponents: [NationFormComponent, NationConfigComponent]
  })
  export class NationModule { }
