import { LanguageService } from '@app/core/services/hr-system/language.service';
import { ValidationService } from '@app/shared/services/validation.service';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { NationPropertyService } from './../../../../core/services/nation/nation-property.service';
import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from '@app/app.component';
import { CommonUtils } from '@app/shared/services';
import { RESOURCE, ACTION_FORM } from '@app/core';

@Component({
  selector: 'nation-config',
  templateUrl: './nation-config.component.html',
})
export class NationConfigComponent extends BaseComponent implements OnInit {
  langs: any = [];
  formConfig: FormGroup;
  config = {
    nationPropertyId: [''],
    nationName: [''],
    nationId: ['', [ValidationService.required, ValidationService.maxLength(50)]],
    numberFormat: ['', [ValidationService.maxLength(50), ValidationService.positiveNumber]],
    dateFormat: [''],
    currencyFormat: ['', [ValidationService.maxLength(50), ValidationService.positiveNumber]],
    language: [''],
  };
  @Input() item: any;
  constructor(public actr: ActivatedRoute
    , public activeModal: NgbActiveModal
    , private nationPropertyService: NationPropertyService
    , private languageService: LanguageService
    , private app: AppComponent) {
      super(actr, RESOURCE.NATION, ACTION_FORM.CONFIG);
      this.formConfig = this.buildForm({}, this.config);
      this.loadReference();
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
  }
  private loadReference() {
    this.languageService.findAll().subscribe(res => {
      this.langs = res.data;
    });
  }
  /**
   * processSaveOrUpdate
   */
  processSaveOrUpdate() {
    if (!this.validateBeforeSave()) {
      return;
    }
    this.app.confirmMessage(null, () => {// on accepted
      this.nationPropertyService.saveOrUpdate(this.formConfig.value)
        .subscribe(res => {
            this.activeModal.close(res);
        });
    }, () => {
      // on rejected
    });
  }

  get f () {
    return this.formConfig.controls;
  }
  /**
   * validate save
   */
  private validateBeforeSave(): boolean {
    return CommonUtils.isValidForm(this.formConfig);
  }

    /**
   * setFormValue
   * param: Array propertyConfigs
   * param data
   */
  public setFormValue(propertyConfigs: any, data?: any) {
    this.propertyConfigs = propertyConfigs;
    let formValue: any = {};
    if (data && data.nationId > 0) {
      this.nationPropertyService.findByNationId(data.nationId).subscribe(res => {
        if (res.data) {
          formValue = res.data;
        }
        formValue.nationId = data.nationId;
        formValue.nationName = data.name;
        this.formConfig =  this.buildForm(formValue, this.config);
      });
    }
  }

}
