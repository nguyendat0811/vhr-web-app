import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, Validators } from '@angular/forms';
import { AppComponent } from '@app/app.component';
import { ValidationService } from '@app/shared/services/validation.service';
import { NationService, RESOURCE, ACTION_FORM } from '@app/core';

@Component({
  selector: 'nation-form',
  templateUrl: './nation-form.component.html',
})
export class NationFormComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  @Input() item: any;
  formConfig = {
      nationId: [''],
      code: ['', [ValidationService.required, ValidationService.maxLength(50)]],
      name: ['', [ValidationService.required, ValidationService.maxLength(200)]],
      isDefault: [''],
      requirePersionalId: [''],
      icon: [''],
      description: ['', [ValidationService.maxLength(1000)]]
  };
  constructor(public actr: ActivatedRoute,
              public activeModal: NgbActiveModal,
              private nationService: NationService,
              private app: AppComponent) {
    super(actr, RESOURCE.NATION, ACTION_FORM.INSERT);
    this.formSave = this.buildForm({}, this.formConfig);
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
  }

  /**
   * processSaveOrUpdate
   */
  processSaveOrUpdate() {
    if (!this.validateBeforeSave()) {
      return;
    }
    this.app.confirmMessage(null, () => {// on accepted
      this.nationService.saveOrUpdate(this.formSave.value)
        .subscribe(res => {
          if (this.nationService.requestIsSuccess(res)) {
            this.activeModal.close(res);
          }
        });
    }, () => {
      // on rejected
    });
  }

  get f () {
    return this.formSave.controls;
  }

  /**
   * validate before save
   */
  private validateBeforeSave(): boolean {
    return CommonUtils.isValidForm(this.formSave);
  }

  /**
   * setFormValue
   * param: Array propertyConfigs
   * param data
   */
  public setFormValue(propertyConfigs: any, data?: any) {
    this.propertyConfigs = propertyConfigs;
    if (data && data.nationId > 0) {
      this.formSave = this.buildForm(data, this.formConfig, ACTION_FORM.UPDATE);
    } else {
      this.formSave = this.buildForm({}, this.formConfig, ACTION_FORM.INSERT);
    }
  }
}

