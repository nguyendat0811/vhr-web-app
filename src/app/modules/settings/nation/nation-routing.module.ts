import { RESOURCE } from '@app/core';
import { PropertyResolver } from '@app/shared/services/property.resolver';
import { CommonUtils } from '@app/shared/services';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NationIndexComponent } from './nation-index/nation-index.component';

const routes: Routes = [
  {
    path: '',
    component: NationIndexComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.NATION,
    }
  }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class NationRoutingModule {}
