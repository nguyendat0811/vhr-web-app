import { SysCatService } from '@app/core';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from './../../../../shared/components/base-component/base-component.component';
import { ACTION_FORM, APP_CONSTANTS, RESOURCE } from './../../../../core/app-config';
import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from '@app/app.component';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { ValidationService } from '@app/shared/services';
import { SysPropertyService } from '../sys-property.service';

@Component({
  selector: 'app-sys-property-form',
  templateUrl: './sys-property-form.component.html'
})
export class SysPropertyFormComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  @Input() item: any;
  resources: any = [];
  actionForms: any = [];
  tables: any = [];
  columns: any = [];
  formConfig = {
    propertyId: [''],
    code: ['', [ValidationService.required, ValidationService.maxLength(50)]],
    name: ['', [ValidationService.required, ValidationService.maxLength(200)]],
    startDate: ['', [ValidationService.required]],
    endDate: [''],
    resourceCode: ['', [ValidationService.required]],
    actionForm: ['', [ValidationService.required]],
    tableName: [''],
    columnName: [''],
    isValidator: [1]
  };
  constructor(public actr: ActivatedRoute
            , public activeModal: NgbActiveModal
            , public sysPropertyService: SysPropertyService
            , public sysCatService: SysCatService
            , private app: AppComponent) {
    super(actr, RESOURCE.SYS_PROPERTY, ACTION_FORM.INSERT);
    this.formSave = this.buildForm({}, this.formConfig);
    this.loadReference();
  }
  ngOnInit() { }
  get f() {
    return this.formSave.controls;
  }
  private loadReference(): void {
    this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.TABLE)
    .subscribe(res => this.tables = res.data);
    this.resources = CommonUtils.convertEnumToChoiceArray(RESOURCE);
    this.actionForms = CommonUtils.convertEnumToChoiceArray(ACTION_FORM);
  }
  /**
   * processSaveOrUpdate
   */
  processSaveOrUpdate() {
    if (CommonUtils.isValidForm(this.formSave)) {
      this.app.confirmMessage(null, () => {// on accepted
        this.sysPropertyService.saveOrUpdate(this.formSave.value)
          .subscribe(res => {
            if (this.sysPropertyService.requestIsSuccess(res)) {
              this.activeModal.close(res);
            }
          });
      }, () => {// on rejected

      });
    }
  }

  /****************** CAC HAM COMMON DUNG CHUNG ****/

  /**
   * validate save
   */
  private validateBeforeSave(): boolean {
    return true;
  }
  /**
   * setFormValue
   * param: Array propertyConfigs
   * param data
   */
  public setFormValue(propertyConfigs: any, data?: any) {
    this.propertyConfigs = propertyConfigs;
    if (data && data.propertyId > 0) {
      this.formSave = this.buildForm(data, this.formConfig, ACTION_FORM.UPDATE,
                      [ValidationService.notAffter('startDate', 'endDate', 'app.sys_property.endDate')]);
      this.findColumnsByTableName(this.formSave.get('tableName').value);
    } else {
      this.formSave = this.buildForm({}, this.formConfig, ACTION_FORM.INSERT,
                      [ValidationService.notAffter('startDate', 'endDate', 'app.sys_property.endDate')]);
    }
  }
  onChangeFieldTableName(tableName): void {
    this.formSave.get('columnName').setValue('');
    this.findColumnsByTableName(tableName);
  }
  private findColumnsByTableName(tableName: string): void {
    this.sysPropertyService.findColumns(tableName).subscribe(res => {
      this.columns = [];
      if (res.data && res.data.length > 0) {
        for (const item of res.data) {
          this.columns.push({
            code: item,
            name: item
          });
        }
      }
    });
  }
}
