import { MarketCompany } from './../../../../core/models/market-company.model';
import { AppComponent } from './../../../../app.component';
import { Component, OnInit, Input, ViewChild, ElementRef, OnChanges, SimpleChanges,  } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { SysPropertyDetailBean, APP_CONSTANTS } from '@app/core';
import { SysPropertyService } from '../sys-property.service';
import { HrStorage } from '@app/core/services/HrStorage';

@Component({
  selector: 'sys-property-config',
  templateUrl: './sys-property-config.component.html',
  styles: []
})
export class SysPropertyConfigComponent implements OnInit, OnChanges {
  formConfig: FormGroup;
  @Input() selectedProperty: any;
  @ViewChild('cssForm') cssElement: ElementRef;
  positions = [
    { label: 'Trái', value: 'left', icon: 'fa fa-align-left' },
    { label: 'Giữa', value: 'center', icon: 'fa fa-align-center' },
    { label: 'Phải', value: 'right', icon: 'fa fa-align-right' }
  ];
  marketCompanies: any = [];
  menus: any = [];
  numberFormats: any = [];
  propDetails: any = [];
  selectedMarketCompany: MarketCompany;
  selectedMarketCompanyCode: string;
  constructor(private sysPropertyService: SysPropertyService
            , private formBuilder: FormBuilder
            , private app: AppComponent) {
    this.loadReference();
    this.buildForm();
  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes.selectedProperty.currentValue) {
      this.selectedProperty = changes.selectedProperty.currentValue;
      this.findPropertyDetails(this.selectedProperty.propertyId);
    }
  }
  ngOnInit() {
    this.findPropertyDetails(this.selectedProperty.propertyId);
  }
  private loadReference(): void {
    this.marketCompanies = HrStorage.getListMarket();
    this.numberFormats = APP_CONSTANTS.NUMBER_VALIDATE;
  }
  private buildForm(): void {
    this.formConfig = this.formBuilder.group({
      marketCompanyName: [''],
      marketCompanyId: [''],
      marketCompanyCode: [''],
      propertyId: [''],
      propertyName: [''],
      propertyCode: [''],
      isHide: [''],
      isTranslation: [''],
      align: [''],
      dateFormat: [''],
      numberFormat: [''],
      moneyFormat: [''],
      css: [''],
      js: [''],
      isRequire: [''],
      isUrl: [''],
      isNumber: [''],
      isEmail: [''],
      isIp: [''],
      minLength: [''],
      maxLength: [''],
      numberMin: [''],
      numberMax: [''],
      password: [''],
      phone: [''],
      mobileNumber: [''],
      personalIdNumber: [''],
      beforeCurrentDate: [''],
      numberType: ['']
    });
  }
  private findPropertyDetails(propertyId): void {
    this.selectedProperty.dataDetails = [];
    this.propDetails = this.selectedProperty.dataDetails;
    this.sysPropertyService.findPropertyDetails(propertyId).subscribe(res => {
      if (res.data.length > 0) {
        for (const item of res.data) {
          this.propDetails[item.marketCompanyBO.code] = new SysPropertyDetailBean(item.sysPropertyBO, item.marketCompanyBO);
          this.propDetails[item.marketCompanyBO.code] = CommonUtils.cloneObject(this.propDetails[item.marketCompanyBO.code], item);
        }
      }
      if (!this.selectedMarketCompanyCode) {
        this.changePropertyMarketCompany(this.marketCompanies[0]);
      }
      this.setValueForm();
    });
  }
  get f() {
    return this.formConfig.controls;
  }
  private setValueForm(): void {
    if (this.propDetails[this.selectedMarketCompanyCode] === undefined || this.propDetails[this.selectedMarketCompanyCode] === null) {
      this.propDetails[this.selectedMarketCompanyCode] = new SysPropertyDetailBean(this.selectedProperty, this.selectedMarketCompany);
    }
    this.buildForm();
    this.formConfig.setValue(CommonUtils.copyProperties(this.formConfig.value, this.propDetails[this.selectedMarketCompanyCode]));
  }
  private onChangeMarketCompany(event: any): void {
    this.propDetails[this.selectedMarketCompanyCode] = CommonUtils.cloneObject(this.propDetails[this.selectedMarketCompanyCode], this.formConfig.value);
    this.changePropertyMarketCompany(this.marketCompanies[event.index]);
  }
  private changePropertyMarketCompany(item: MarketCompany): void {
    if (!this.selectedProperty) {
      // toaster.pop("error","","Vui lòng chọn thuộc tính cần cấu hình ");
      return;
    }
    this.selectedMarketCompany = item;
    this.selectedMarketCompanyCode = item.code;
    this.setValueForm();
  }
  processSaveOrUpdate() {
    if (!CommonUtils.isValidForm(this.formConfig)) {
      return;
    }
    this.app.confirmMessage(null, () => {// on accepted
      this.propDetails[this.selectedMarketCompanyCode] = CommonUtils.cloneObject(this.propDetails[this.selectedMarketCompanyCode], this.formConfig.value);
      const data = [];
      for (const key in this.propDetails) {
        const obj = this.propDetails[key];
          if (obj.propertyId && obj.marketCompanyId) {
            data.push(obj);
          }
      }
      this.sysPropertyService.savePropertyDetail(data).subscribe(res => {
        if (this.sysPropertyService.requestIsSuccess(res)) {
          this.findPropertyDetails(this.selectedProperty.propertyId);
        }
      });
    }, () => {// on rejected

    });
  }
  refreshPropertyDetails(): void {
    this.findPropertyDetails(this.selectedProperty.propertyId);
  }
  cancelPropertyDetails(): void {
    this.selectedProperty = {};
  }
  validateCss(): void{
    try {
      if(this.formConfig.controls.css){
        JSON.parse(this.formConfig.controls.css.value);
      }
    } catch (e) {
      this.app.messError('ERROR', 'sys.property.invalid.syntax');
      this.cssElement.nativeElement.focus();
      this.formConfig.controls.css.setValue('{}');
    }
  }
}
