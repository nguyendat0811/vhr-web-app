import { BaseComponent } from './../../../../shared/components/base-component/base-component.component';
import { Component, OnInit, Input, NgModuleRef } from '@angular/core';
import { SysPropertyFormComponent } from '../sys-property-form/sys-property-form.component';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { DEFAULT_MODAL_OPTIONS, RESOURCE, ACTION_FORM } from '@app/core/app-config';
import { AppComponent } from '@app/app.component';
import { SysPropertyService } from '../sys-property.service';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-sys-property-index',
  templateUrl: './sys-property-index.component.html'
})
export class SysPropertyIndexComponent extends BaseComponent implements OnInit {
  /* Variables */
  resultList: any = {};
  resources = [];
  formSearch: FormGroup;
  selectedProperty: any = {};
  @Input()
  formConfig: any = {
    code: [''],
    name: [''],
    startDate: [''],
    endDate: [''],
    resourceCode: [''],
  };
  /* Constructor */
  constructor(public actr: ActivatedRoute
    , private formBuilder: FormBuilder
    , private modalService: NgbModal
    , public sysPropertyService: SysPropertyService
    , private app: AppComponent) {
    super(actr, RESOURCE.SYS_PROPERTY, ACTION_FORM.SEARCH);
    this.formSearch = this.buildForm({}, this.formConfig);
    this.setMainService(this.sysPropertyService);
    this.loadReference();
  }
  ngOnInit() {
    this.processSearch();
  }
  get f() {
    return this.formSearch.controls;
  }

  /**
   * Load cac list du lieu lien quan
   */
  private loadReference(): void {
    this.resources = CommonUtils.convertEnumToChoiceArray(RESOURCE);
  }

  /**
  * prepareUpdate
  * param item
  */
  prepareSaveOrUpdate(item) {
    const modalRef = this.modalService.open(SysPropertyFormComponent, DEFAULT_MODAL_OPTIONS);
    if (item && item.propertyId > 0) {
      this.sysPropertyService.findOne(item.propertyId)
        .subscribe(res => {
          modalRef.componentInstance.setFormValue(this.propertyConfigs, res.data);
        });
    } else {
      modalRef.componentInstance.setFormValue(this.propertyConfigs);
    }

    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
      if (this.sysPropertyService.requestIsSuccess(result)) {
        this.processSearch(null);
      }
    });
  }
  /**
  * prepareDelete
  * param item
  */
  processDelete(item) {
    if (item && item.propertyId > 0) {
      this.app.confirmDelete(null, () => {// on accepted
        this.sysPropertyService.deleteById(item.propertyId)
          .subscribe(res => {
            if (this.sysPropertyService.requestIsSuccess(res)) {
              this.processSearch(null);
            }
          });
      }, () => {// on rejected

      });
    }
  }
  /**
   * showProperty
   *
   */
  showProperty(item: any): void {
    if (item && item.propertyId > 0) {
      this.sysPropertyService.findOne(item.propertyId)
        .subscribe(res => {
            this.selectedProperty = res.data;
          }
        );
    }
  }

}
