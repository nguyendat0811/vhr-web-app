import { Component, OnInit } from '@angular/core';
import { LabourContractTypeService, RESOURCE } from '@app/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-labour-contract-type',
  templateUrl: './labour-contract-type.component.html'
})
export class LabourContractTypeComponent extends BaseComponent implements OnInit {

  constructor(public act: ActivatedRoute) { 
    super(act, RESOURCE.LABOUR_CONTRACT_TYPE);
  }

  ngOnInit() {
  }

}
