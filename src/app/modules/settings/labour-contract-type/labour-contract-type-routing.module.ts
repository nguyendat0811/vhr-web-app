import { RESOURCE } from './../../../core/app-config';
import { PropertyResolver } from '@app/shared/services/property.resolver';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LabourContractTypeComponent } from './labour-contract-type.component';
import { CommonUtils } from '@app/shared/services';

const routes: Routes = [
  {
    path: '',
    component: LabourContractTypeComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.LABOUR_CONTRACT_TYPE,
      nationId: CommonUtils.getNationId()
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LabourContractTypeRoutingModule { }
