import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from './../../../../shared/components/base-component/base-component.component';
import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder , Validators, FormArray} from '@angular/forms';
import { LabourContractTypeService, RESOURCE, ACTION_FORM } from '@app/core';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { AppComponent } from '@app/app.component';
import { ValidationService } from '@app/shared/services';
import { LabourContractDetailService } from '@app/core/services/labour-contract-detail/labour-contract-detail.service';

@Component({
    selector: 'labour-contract-type-form',
    templateUrl: './labour-contract-type-form.component.html',
  })
export class LabourContractTypeFormComponent extends BaseComponent implements OnInit {
  formlabourContractDetail: FormArray;
  listIdDelete = [];
  // formSave: FormGroup;
  formLabourContractType: FormGroup;

    @Input() item: any;
    formConfig = {
      labourContractTypeId: [''],
      code: ['' , [ValidationService.required, ValidationService.maxLength(50)]],
      name: ['' , [ValidationService.required, ValidationService.maxLength(200)]],
      status: [''],
    };
    formConfigDetail = {
      labourContractDetailId: [''],
      code: [''],
      name: ['' , [ValidationService.required, ValidationService.maxLength(200)]],
      effectiveDuration: ['', [ValidationService.positiveInteger, ValidationService.maxLength(5), Validators.min(1), Validators.max(99)]],
    };
  constructor(public actr: ActivatedRoute
            , public activeModal: NgbActiveModal
            , private labourContractTypeService: LabourContractTypeService
            , private labourContractDetailService: LabourContractDetailService
            , private app: AppComponent) {
    super(actr, RESOURCE.LABOUR_CONTRACT_TYPE, ACTION_FORM.INSERT);
    this.formLabourContractType = this.buildForm({}, this.formConfig, ACTION_FORM.INSERT);

    this.buildFormLabourContractDetail();
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
  }
  /**
   * processSaveOrUpdate
   */
  processSaveOrUpdate() {
    if (!this.validateBeforeSave()) {
      return;
    } else {
      const formSave = {};
      formSave['labourContractType'] = this.formLabourContractType.value;
      formSave['labourContractDetail'] = this.formlabourContractDetail.value;
      formSave['listIdDelete'] = this.listIdDelete;
      this.app.confirmMessage(null, () => {// on rejected
        this.labourContractTypeService.saveOrUpdate(formSave)
        .subscribe(res => {
          if (this.labourContractTypeService.requestIsSuccess(res)) {
            this.activeModal.close(res);
          }
        });
      }, null);
    }
  }

  private validateBeforeSave(): boolean {
    const formLabourContractType = CommonUtils.isValidForm(this.formLabourContractType);
    const formlabourContractDetail = CommonUtils.isValidForm(this.formlabourContractDetail);
    return formLabourContractType
          && formlabourContractDetail;
  }

  /****************** CAC HAM COMMON DUNG CHUNG ****/
  get f () {
    return this.formLabourContractType.controls;
  }

   /**
   * makeLabourContractForm
   */
  private makeLabourContractDetailForm(): FormGroup {
    const formGroup = this.buildForm({}, this.formConfigDetail, ACTION_FORM.ORG_RELATION_UPDATE);
    return formGroup;
  }

  private buildFormLabourContractDetail(listLabourContractDetail?: any) {
    const controls = new FormArray([]);
      if (listLabourContractDetail) {
      for (const i in listLabourContractDetail) {
        const contractDetail = listLabourContractDetail[i];
        const group = this.makeLabourContractDetailForm();
        group.patchValue(contractDetail);
        controls.push(group);
      }
    }
    this.formlabourContractDetail = controls;
  }


  public addLabourContractDetail( item: FormGroup) {
    const controls = this.formlabourContractDetail as FormArray;
    controls.insert(controls.length, this.makeLabourContractDetailForm());
  }

  /**
   * removeOrgRelation
   * param index
   * param item
   */
  public removeLabourContractDetail(index: number, item: FormGroup) {
    if (item && item.get('labourContractDetailId').value) {
      this.app.confirmDelete(null, () => {// on accepted
        this.labourContractDetailService.deleteById(item.get('labourContractDetailId').value)
        .subscribe(res => {
          if (this.labourContractDetailService.requestIsSuccess(res)) {
            const controls = this.formlabourContractDetail as FormArray;
            controls.removeAt(index);
          }
        });
      }, null);
    } else {
      const controls = this.formlabourContractDetail as FormArray;
      controls.removeAt(index);
    }
  }


  /**
   * setFormValue
   * param: Array propertyConfigs
   * param data
   */
  public setFormValue(propertyConfigs: any, data?: any) {
    this.propertyConfigs = propertyConfigs;
    if (data && data.labourContractTypeId > 0) {
      this.formLabourContractType = this.buildForm(data, this.formConfig, ACTION_FORM.UPDATE);
      this.labourContractTypeService.findByLabourContractTypeId(data.labourContractTypeId).subscribe(
        res => {
          if (!res) {
            this.buildFormLabourContractDetail();
          } else {
          this.buildFormLabourContractDetail(res);
        }
      });
    } else {
      this.formLabourContractType = this.buildForm({}, this.formConfig, ACTION_FORM.INSERT);
    }
  }
}
