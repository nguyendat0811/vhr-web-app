import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/dashboard/settings',
    pathMatch: 'full'
  },
  {
    path: 'emp-type',
    loadChildren: './emp-type/emp-type.module#EmpTypeModule'
  },
  {
    path: 'nation',
    loadChildren: './nation/nation.module#NationModule'
  },
  {
    path: 'sys-cat',
    loadChildren: './sys-cat/sys-cat.module#SysCatModule'
  },
  {
    path: 'sys-property',
    loadChildren: './sys-property/sys-property.module#SysPropertyModule'
  },
  {
    path: 'allowance',
    loadChildren: './allowance/allowance.module#AllowanceModule'
  },
  {
    path: 'allowance-type',
    loadChildren: './allowance-type/allowance-type.module#AllowanceTypeModule'
  },
  {
    path: 'labour_contract_type',
    loadChildren: './labour-contract-type/labour-contract-type.module#LabourContractTypeModule'
  },
  {
    path: 'work-flows',
    loadChildren: './work-flows/work-flows.module#WorkFlowsModule'
  },
  {
    path: 'position-salary',
    loadChildren: './position-salary/position-salary.module#PositionSalaryModule'
  },
  {
    path: 'long-leave-reason',
    loadChildren: './long-leave-reason/long-leave-reason.module#LongLeaveReasonModule'
  },
  {
    path: 'document-type',
    loadChildren: './document-type/document-type.module#DocumentTypeModule'
  },
  {
    path: 'system-parameter',
    loadChildren: './system-parameter/system-parameter.module#SystemParameterModule'
  },
  {
    path: 'training-plan',
    loadChildren: './training-plan/training-plan.module#TrainingPlanModule'
  },
  {
    path: 'nation-config',
    loadChildren: './nation-config/nation-config.module#NationConfigModule'
  },
  {
    path: 'job-requirement',
    loadChildren: './job-requirement/job-requirement.module#JobRequirementModule'
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule { }
