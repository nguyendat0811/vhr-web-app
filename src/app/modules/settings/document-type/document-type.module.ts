import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {  } from './document-type-routing.module';
import { SharedModule } from '@app/shared';
import {  DocumentTypeIndexComponent } from './page/document-type-index/document-type-index.component';
import { DocumentTypeSearchComponent } from './page/document-type-search/document-type-search.component';
import { DocumentTypeFormComponent } from './page/document-type-form/document-type-form.component';
import {DocumentTypeRoutingModule} from './document-type-routing.module';

 @NgModule({
  declarations: [DocumentTypeIndexComponent, DocumentTypeSearchComponent, DocumentTypeFormComponent],
  imports: [
    CommonModule,
    SharedModule,

    DocumentTypeRoutingModule
  ],
  entryComponents: [DocumentTypeFormComponent],

})
export class DocumentTypeModule { }
