import { CommonUtils } from '../../../shared/services/common-utils.service';
import { RESOURCE } from '@app/core/app-config';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DocumentTypeIndexComponent } from './page/document-type-index/document-type-index.component';
import { PropertyResolver } from '@app/shared/services/property.resolver';


const routes: Routes = [
  {
    path: '',
    component: DocumentTypeIndexComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.DOCUMENT_TYPE,
      nationId: CommonUtils.getNationId()
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentTypeRoutingModule { }
