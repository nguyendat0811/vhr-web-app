import { Component, OnInit } from '@angular/core';
import { DocumentTypeService, RESOURCE } from '@app/core';
import { CommonUtils } from '@app/shared/services';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
@Component({
  selector: 'document-type',
  templateUrl: './document-type-index.component.html'
})
export class DocumentTypeIndexComponent extends BaseComponent implements OnInit {
  resultList: any = {};
  constructor(public act: ActivatedRoute) { 
    super(act, RESOURCE.DOCUMENT_TYPE);
  }
  ngOnInit() {

  }

}
