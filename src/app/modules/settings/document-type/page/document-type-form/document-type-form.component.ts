import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { DocumentTypeService, DocumentReasonService, RESOURCE, ACTION_FORM, SysCatService, APP_CONSTANTS, WorkProcessService } from '@app/core';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { AppComponent } from '@app/app.component';
import { ValidationService } from '@app/shared/services';
@Component({
  selector: 'document-type-form',
  templateUrl: './document-type-form.component.html',
})
export class DocumentTypeFormComponent extends BaseComponent implements OnInit {
  formDocumentReason: FormArray;
  // formSave: FormGroup;
  formDocumentType: FormGroup;
  checked: any;
  isUsed : boolean;
  checkedFlag: any;
  @Input() item: any;
  formConfig = {
    documentTypeId: [''],
    code: ['', [ValidationService.required, ValidationService.maxLength(50)]],
    name: ['', [ValidationService.required, ValidationService.maxLength(200)]],
    used: [null],
    inoutFlag: [null]
  };
  formConfigReason = {
    documentReasonId: [''],
    name: ['', [ValidationService.required, ValidationService.maxLength(100)]]
  };
  constructor(public activeModal: NgbActiveModal
            , public actr: ActivatedRoute
            , private documentTypeService: DocumentTypeService
            , private app: AppComponent
            , private workProcessService: WorkProcessService
            ) {
      super(actr, RESOURCE.DOCUMENT_TYPE, ACTION_FORM.INSERT);
      this.formDocumentType = this.buildForm({}, this.formConfig, ACTION_FORM.INSERT);
      this.buildFormDocumentReason();
      this.isUsed = false;
    //  this.buildForms({}, ACTION_FORM.INSERT);
  }
  /**
   * ngOnInit
   */
  ngOnInit() {
  }



  /**
   * processSaveOrUpdate
   */
  processSaveOrUpdate() {

    if (!this.validateBeforeSave()) {
      return;
    } else {
      const formSave = {};
      if (this.formDocumentType.value.used === null ) {
        this.formDocumentType.controls['used'].setValue(1);
      }
      formSave['documentType'] = this.formDocumentType.value;
      formSave['documentReason'] = this.formDocumentReason.value;
      this.app.confirmMessage(null, () => {// on accepted
        this.documentTypeService.saveOrUpdate(formSave)
          .subscribe(res => {
            if (this.documentTypeService.requestIsSuccess(res)) {
              this.activeModal.close(res);
            }
          });
      }, () => {// on rejected
      });
    }
  }

  private validateBeforeSave(): boolean {
    const formDocumentType = CommonUtils.isValidForm(this.formDocumentType);
    const formDocumentReason = CommonUtils.isValidForm(this.formDocumentReason);
    return formDocumentType && formDocumentReason;
  }
  /****************** CAC HAM COMMON DUNG CHUNG ****/
  get f () {
    return this.formDocumentType.controls;
  }

  private makeDocumentReasonForm(): FormGroup {
    const formGroup = this.buildForm({}, this.formConfigReason, ACTION_FORM.ORG_RELATION_UPDATE);
    return formGroup;
  }
  private buildFormDocumentReason(listDocumentReason?: any) {
    const controls = new FormArray([]);
    if (listDocumentReason && listDocumentReason.length > 0) {
      for (const i in listDocumentReason) {
        const contractDetail = listDocumentReason[i];
        const group = this.makeDocumentReasonForm();
        group.patchValue(contractDetail);
        controls.push(group);
      }
    }
    this.formDocumentReason = controls;
  }

  public addDocumentReason(item: FormGroup) {
    const controls = this.formDocumentReason as FormArray;
    // controls.insert(index + 1, this.makeDocumentReasonForm());
    controls.insert(controls.length, this.makeDocumentReasonForm());
  }

  /**
   * removeOrgRelation
   * param index
   * param item
   */
  public removeDocumentReason(index: number, item: FormGroup) {
    const controls = this.formDocumentReason as FormArray;
    // if (controls.length === 1) {
    //   this.buildFormDocumentReason({});
    //   const group = this.makeDocumentReasonForm();
    //   controls.push(group);
    //   this.formDocumentReason = controls;
    // }
    controls.removeAt(index);
  }
  /**
   * setFormValue
   * param data
   */
  public setFormValue(propertyConfigs: any, data?: any) {
    this.propertyConfigs = propertyConfigs;
    if (data && data.documentTypeId > 0) {
      if (data.used === 1 ) {
        this.checked = true;
      } else {
        this.checked = false;
      }
      this.formDocumentType = this.buildForm(data, this.formConfig, ACTION_FORM.UPDATE);
      this.documentTypeService.findReasonByDocumentTypeId(data.documentTypeId)
        .subscribe(res => {
          this.buildFormDocumentReason(res);
        });
      this.workProcessService.getProcessByDocumentTypeId(data.documentTypeId).subscribe(ress => {
        if (ress.data.length > 0) {
          this.isUsed = true;
        } else {
          this.isUsed = false;
        }
      });
    // this.buildForms(data, ACTION_FORM.UPDATE);
    } else {
      this.checked = true;
      this.checkedFlag = true;
    //  this.buildForms({}, ACTION_FORM.INSERT);
      this.formDocumentType = this.buildForm({}, this.formConfig, ACTION_FORM.INSERT);
      this.formDocumentType.controls['inoutFlag'].setValue(3);
    }
  }
  OnChangeCB(e) {
    if (e.target.checked) {
      this.formDocumentType.controls['used'].setValue(1);
      this.checked = true;
    } else {
      this.formDocumentType.controls['used'].setValue(0);
      this.checked = false;
    }
  }
}
