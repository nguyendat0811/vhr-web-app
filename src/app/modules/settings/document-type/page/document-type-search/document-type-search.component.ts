import { ACTION_FORM } from '../../../../../core/app-config';
import { Component, OnInit, ViewChild, OnChanges } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DocumentTypeFormComponent } from '../document-type-form/document-type-form.component';
import { DocumentTypeService } from '@app/core';
import { DEFAULT_MODAL_OPTIONS, APP_CONSTANTS, RESOURCE } from '@app/core/app-config';
import { AppComponent } from '@app/app.component';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { SysCatService } from '@app/core';
import { ValidationService } from '@app/shared/services';
import { CommonUtils } from '@app/shared/services/common-utils.service';

@Component({
  selector: 'document-type-search',
  templateUrl: './document-type-search.component.html',
})
export class DocumentTypeSearchComponent extends BaseComponent implements OnInit {
  checked = false;
  // @ViewChild('ptable') dataTable: any;
  formSave: FormGroup;
  formConfig = {
    code: ['', [ ValidationService.maxLength(50)]],
    name: ['', [ ValidationService.maxLength(200)]],
    used: [''],
    inoutFlag: [''],
  };

  constructor(
      public actr: ActivatedRoute
    , private modalService: NgbModal
    , private documentTypeService: DocumentTypeService
    , private app: AppComponent
    // , private sysCatService: SysCatService
      ) {
    super(actr, RESOURCE.DOCUMENT_TYPE, ACTION_FORM.SEARCH);
    this.setMainService(documentTypeService);
    this.formSearch = this.buildForm({}, this.formConfig);
  }
  ngOnInit() {
    this.processSearch();
  }

  get f () {
    return this.formSearch.controls;
  }

  /**
   * prepareUpdate
   * param item
   */
  prepareSaveOrUpdate(item) {
    const modalRef = this.modalService.open(DocumentTypeFormComponent, DEFAULT_MODAL_OPTIONS);
    if (item && item.documentTypeId > 0) {
      this.documentTypeService.findOne(item.documentTypeId)
        .subscribe(res => {
          modalRef.componentInstance.setFormValue(this.propertyConfigs, res.data);
        });
    } else {
      modalRef.componentInstance.setFormValue(this.propertyConfigs);
    }

    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
      if (this.documentTypeService.requestIsSuccess(result)) {
        this.processSearch();
      }
    });
  }

  /**
   * prepareDelete
   * param item
   */
  processDelete(item) {
    console.log(item);
    if (item && item.documentTypeId > 0) {
      this.app.confirmDelete(null, () => {// on accepted
        this.documentTypeService.deleteById(item.documentTypeId)
        .subscribe(res => {
          if (this.documentTypeService.requestIsSuccess(res)) {
            this.processSearch(null);
          }
        });
      }, () => {// on rejected

      });
    }
  }
  OnChangeCB(e) {
    if (e.target.checked) {
      this.formSearch.controls['used'].setValue(1);
      this.checked = true;
    } else {
      this.formSearch.controls['used'].setValue(null);
      this.checked = false;
    }

  }
}

