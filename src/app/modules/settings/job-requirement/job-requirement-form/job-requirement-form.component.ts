import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { RESOURCE, ACTION_FORM, SysCatService, APP_CONSTANTS } from '@app/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from '@app/app.component';
import { RequirementDetailService } from '@app/core/services/hr-organization/requirement-detail.service';
import { FormGroup, Validators } from '@angular/forms';
import { CommonUtils, ValidationService } from '@app/shared/services';
import { TranslationService } from 'angular-l10n';
import { SelectItemGroup } from 'primeng/api';

@Component({
  selector: 'job-requirement-form',
  templateUrl: './job-requirement-form.component.html',
})
export class JobRequirementFormComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  formConfig = {
    requirementDetailId: [null],
    requirementId: [null, [ValidationService.required]],
    code: [null, [ValidationService.required, ValidationService.maxLength(100)]],
    name: [null, [ValidationService.required, ValidationService.maxLength(200)]],
    description: [null, [ValidationService.maxLength(1000)]],
  };
  isDisable = false;
  group: [];
  constructor(public actr: ActivatedRoute,
              public activeModal: NgbActiveModal,
              private app: AppComponent,
              private requirementDetailService: RequirementDetailService,
              private sysCatService: SysCatService,
              ) {
    super(actr, RESOURCE.JOB_REQUIREMENT, ACTION_FORM.INSERT);
    this.buildForms({}, ACTION_FORM.INSERT);
  }

  ngOnInit() {
    this.sysCatService.getGroupRequirement().subscribe(res => {
      this.group = res.data;
    });
  }

  get f () {
    return this.formSave.controls;
  }

  processSaveOrUpdate() {
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    } else {
      this.app.confirmMessage(null, () => {// on accepted
        this.requirementDetailService.saveOrUpdate(this.formSave.value)
          .subscribe(res => {
            if (this.requirementDetailService.requestIsSuccess(res)) {
              this.activeModal.close(res);
            }
          });
      }, () => {// on rejected

      });
    }

  }

  /**
   * buildForm
   */
  private buildForms(data?: any, actionForm?: ACTION_FORM): void {
    this.formSave = this.buildForm(data, this.formConfig, actionForm);
  }

  setFormValue(propertyConfigs: any, data?: any) {
    this.propertyConfigs = propertyConfigs;
    if (data && data.requirementDetailId > 0) {
      this.buildForms(data, ACTION_FORM.UPDATE);
      this.requirementDetailService.checkExistRequirement(data.requirementDetailId)
      .subscribe(res => {
        if (!CommonUtils.isNullOrEmpty(res.data)) {
          this.isDisable = true;
        }
      });
    } else {
      this.buildForms({}, ACTION_FORM.INSERT);
    }
  }
}
