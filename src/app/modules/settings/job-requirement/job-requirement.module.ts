import { SharedModule } from './../../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JobRequiremetIndexComponent } from './job-requiremet-index/job-requiremet-index.component';
import { JobRequirementSearchComponent } from './job-requirement-search/job-requirement-search.component';
import { JobRequirementFormComponent } from './job-requirement-form/job-requirement-form.component';
import { JobRequirementRoutingModule } from './job-requirement-routing.module';

@NgModule({
  declarations: [JobRequiremetIndexComponent, JobRequirementSearchComponent, JobRequirementFormComponent],
  imports: [
    CommonModule,
    SharedModule,

    JobRequirementRoutingModule
  ],
  entryComponents: [JobRequirementFormComponent]
})
export class JobRequirementModule { }
