import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { RESOURCE } from '@app/core';

@Component({
  selector: 'job-requiremet-index',
  templateUrl: './job-requiremet-index.component.html',
})
export class JobRequiremetIndexComponent extends BaseComponent implements OnInit {

  constructor(public act: ActivatedRoute) { 
    super(act, RESOURCE.JOB_REQUIREMENT);
  }

  ngOnInit() {
  }

}
