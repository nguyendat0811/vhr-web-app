import { DEFAULT_MODAL_OPTIONS, APP_CONSTANTS } from './../../../../core/app-config';
import { Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { ACTION_FORM, RESOURCE, SysCatService } from '@app/core';
import { RequirementDetailService } from '@app/core/services/hr-organization/requirement-detail.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { JobRequirementFormComponent } from '../job-requirement-form/job-requirement-form.component';
import { TranslationService } from 'angular-l10n';
import { AppComponent } from '@app/app.component';
import { ValidationService } from '@app/shared/services';

@Component({
  selector: 'job-requirement-search',
  templateUrl: './job-requirement-search.component.html',
})
export class JobRequirementSearchComponent extends BaseComponent implements OnInit {
  formConfig = {
    code: [null, [ValidationService.maxLength(100)]],
    name: [null, [ValidationService.maxLength(200)]],
    requirementId: [null],
  };
  group = [];
  constructor(public actr: ActivatedRoute,
             private app: AppComponent,
              private modalService: NgbModal,
              private requirementDetailService: RequirementDetailService,
              private sysCatService: SysCatService,) {
    super(actr, RESOURCE.JOB_REQUIREMENT, ACTION_FORM.SEARCH);
    this.setMainService(requirementDetailService);
    this.formSearch = this.buildForm({}, this.formConfig);
    this.sysCatService.getGroupRequirement().subscribe(res => {
      this.group = res.data;
    });
  }

  ngOnInit() {
    this.processSearch();
  }

  get f () {
    return this.formSearch.controls;
  }

  /**
   * prepareUpdate
   * param item
   */
  prepareSaveOrUpdate(item) {
    const modalRef = this.modalService.open(JobRequirementFormComponent, DEFAULT_MODAL_OPTIONS);
    if (item && item.requirementDetailId > 0) {
      this.requirementDetailService.findOne(item.requirementDetailId)
        .subscribe(res => {
          modalRef.componentInstance.setFormValue(this.propertyConfigs, res.data);
        });
    } else {
      modalRef.componentInstance.setFormValue(this.propertyConfigs);
    }
    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
      if (this.requirementDetailService.requestIsSuccess(result)) {
        this.processSearch(null);
      }
    });
  }

   /**
   * prepareDelete
   * param item
   */
  processDelete(item) {
    if (item && item.requirementDetailId > 0) {
      this.app.confirmDelete(null, () => {// on accepted
        this.requirementDetailService.deleteById(item.requirementDetailId)
        .subscribe(res => {
          if (this.requirementDetailService.requestIsSuccess(res)) {
            this.processSearch(null);
          }
        });
      }, () => {// on rejected

      });
    }
  }
}
