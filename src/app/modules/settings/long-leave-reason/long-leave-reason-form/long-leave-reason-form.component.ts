import { WorkdayTypeService } from '../../../../core/services/hr-timekeeping/workday-type.service';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { ACTION_FORM, SysCatService } from '@app/core';
import { RESOURCE, APP_CONSTANTS } from './../../../../core/app-config';
import { BaseComponent } from './../../../../shared/components/base-component/base-component.component';
import { AppComponent } from '@app/app.component';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { LongLeaveReasonService } from '@app/core/services/hr-timekeeping/long-leave-reason.service';
import { ValidationService } from '@app/shared/services';

@Component({
  selector: 'long-leave-reason-form',
  templateUrl: './long-leave-reason-form.component.html'
})
export class LongLeaveReasonFormComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  workdayTypeList: any = {};
  formConfig = {
    longLeaveReasonId: [null],
    name: [null, [ValidationService.required, ValidationService.maxLength(200)]],
    workdayTypeCode: [null, [ValidationService.required, ValidationService.maxLength(50)]],
  };
  constructor(public actr: ActivatedRoute
    , public activeModal: NgbActiveModal
    , private longLeaveReasonService: LongLeaveReasonService
    , private app: AppComponent
    , private workdayTypeService: WorkdayTypeService
    ) {
      super(actr, RESOURCE.LONG_LEAVE_REASON, ACTION_FORM.INSERT);
      this.formSave = this.buildForm({}, this.formConfig);
      this.workdayTypeService.findAllWorkdayTypeByMarketId().subscribe(
        res => {
          this.workdayTypeList = res.data;
        });
    }
  ngOnInit() {
  }

  get f () {
    return this.formSave.controls;
  }
    /**
   * processSaveOrUpdate
   */
  processSaveOrUpdate() {
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    } else {
      this.app.confirmMessage(null, () => {// on rejected
        this.longLeaveReasonService.saveOrUpdate(this.formSave.value)
        .subscribe(res => {
          if (this.longLeaveReasonService.requestIsSuccess(res)) {
            this.activeModal.close(res);
          }
        });
      }, () => {// on rejected

      });
    }
  }

  /**
   * setFormValue
   * param: Array propertyConfigs
   * param data
   */
  public setFormValue(propertyConfigs: any, data?: any) {
    this.propertyConfigs = propertyConfigs;
    if (data && data.longLeaveReasonId > 0) {
      this.formSave = this.buildForm(data, this.formConfig, ACTION_FORM.UPDATE);
    } else {
      this.formSave = this.buildForm({}, this.formConfig, ACTION_FORM.INSERT);
    }
  }
}
