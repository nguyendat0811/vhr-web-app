import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { RESOURCE } from '@app/core';

@Component({
  selector: 'long-leave-reason-index',
  templateUrl: './long-leave-reason-index.component.html'
})
export class LongLeaveReasonIndexComponent extends BaseComponent implements OnInit {

  constructor(public act: ActivatedRoute) { 
    super(act, RESOURCE.LONG_LEAVE_REASON);
  }

  ngOnInit() {
  }

}
