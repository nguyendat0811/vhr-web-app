import { LongLeaveReasonIndexComponent } from './long-leave-reason-index/long-leave-reason-index.component';
import { LongLeaveReasonRoutingModule } from './long-leave-reason-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@app/shared';
import { LongLeaveReasonSearchComponent } from './long-leave-reason-search/long-leave-reason-search.component';
import { LongLeaveReasonFormComponent } from './long-leave-reason-form/long-leave-reason-form.component';

@NgModule({
  declarations: [
    LongLeaveReasonIndexComponent,
    LongLeaveReasonSearchComponent,
    LongLeaveReasonFormComponent
  ],
  imports: [
    CommonModule,
    SharedModule,

    LongLeaveReasonRoutingModule,
  ],
  entryComponents: [LongLeaveReasonFormComponent]
})
export class LongLeaveReasonModule { }
