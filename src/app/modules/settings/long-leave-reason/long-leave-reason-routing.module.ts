import { LongLeaveReasonIndexComponent } from './long-leave-reason-index/long-leave-reason-index.component';
import { CommonUtils } from './../../../shared/services/common-utils.service';
import { RESOURCE } from '@app/core';
import { PropertyResolver } from '@app/shared/services/property.resolver';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

const routes: Routes = [
  {
    path: '',
    component: LongLeaveReasonIndexComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.LONG_LEAVE_REASON,
      nationId: CommonUtils.getNationId()
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LongLeaveReasonRoutingModule { }
