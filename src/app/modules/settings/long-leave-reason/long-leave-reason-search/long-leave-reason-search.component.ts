import { WorkdayTypeService } from '../../../../core/services/hr-timekeeping/workday-type.service';
import { LongLeaveReasonFormComponent } from './../long-leave-reason-form/long-leave-reason-form.component';
import { DEFAULT_MODAL_OPTIONS } from '@app/core/app-config';
import { AppComponent } from '@app/app.component';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from './../../../../shared/components/base-component/base-component.component';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RESOURCE, ACTION_FORM, SysCatService, APP_CONSTANTS } from '@app/core';
import { Validators } from '@angular/forms';
import { ValidationService } from '@app/shared/services';
import { LongLeaveReasonService } from '@app/core/services/hr-timekeeping/long-leave-reason.service';

@Component({
  selector: 'long-leave-reason-search',
  templateUrl: './long-leave-reason-search.component.html'
})
export class LongLeaveReasonSearchComponent extends BaseComponent implements OnInit {
  workdayTypeList: any;
  formConfig = {
    name: ['' , [ValidationService.maxLength(200)]],
    workdayTypeCode: [''],
  };
  constructor(private modalService: NgbModal
    , public actr: ActivatedRoute
    , private app: AppComponent
    , private longLeaveReasonService: LongLeaveReasonService
    , private workdayTypeService: WorkdayTypeService
    ) {
      super(actr, RESOURCE.LONG_LEAVE_REASON, ACTION_FORM.SEARCH);
      this.setMainService(longLeaveReasonService);
      this.formSearch = this.buildForm({}, this.formConfig);
      this.workdayTypeService.findAllWorkdayTypeByMarketId().subscribe(
        res => {
          this.workdayTypeList = res.data;
        });
    }

  ngOnInit() {
    this.processSearch();
  }

  get f () {
    return this.formSearch.controls;
  }

  /**
   * prepareUpdate
   * param item
   */
  prepareSaveOrUpdate(item) {
    const modalRef = this.modalService.open(LongLeaveReasonFormComponent, DEFAULT_MODAL_OPTIONS);
    if (item && item.longLeaveReasonId > 0) {
      this.longLeaveReasonService.findOne(item.longLeaveReasonId)
        .subscribe(res => {
          modalRef.componentInstance.setFormValue(this.propertyConfigs, res.data);
        });
    } else {
      modalRef.componentInstance.setFormValue(this.propertyConfigs);
    }
    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
      if (this.longLeaveReasonService.requestIsSuccess(result)) {
        this.processSearch(null);
      }
      });
    }

    /**
     * prepareDelete
     * param item
     */
    processDelete(item) {
      if (item && item.longLeaveReasonId > 0) {
        this.app.confirmDelete(null, () => {// on accepted
          this.longLeaveReasonService.deleteById(item.longLeaveReasonId)
          .subscribe(res => {
            if (this.longLeaveReasonService.requestIsSuccess(res)) {
              this.processSearch(null);
            }
          });
        }, () => {// on rejected

        });
      }
    }

}
