import { Component, OnInit } from '@angular/core';
import { EmpTypeService, RESOURCE } from '@app/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'emp-type-page',
  templateUrl: './emp-type-page.component.html',
})
export class EmpTypePageComponent extends BaseComponent implements OnInit {
  resultList: any = {};
  constructor(public act: ActivatedRoute) { 
    super(act, RESOURCE.EMP_TYPE);
  }

  ngOnInit() {
  }

}
