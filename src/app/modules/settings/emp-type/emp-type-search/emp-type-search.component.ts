import { CommonUtils } from '@app/shared/services/common-utils.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EmpTypeFormComponent } from '../emp-type-form/emp-type-form.component';
import { EmpTypeService, ACTION_FORM } from '@app/core';
import { DEFAULT_MODAL_OPTIONS, RESOURCE } from '@app/core/app-config';
import { AppComponent } from '@app/app.component';
import { ValidationService } from '@app/shared/services';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'emp-type-search',
  templateUrl: './emp-type-search.component.html'
})
export class EmpTypeSearchComponent extends BaseComponent implements OnInit {
  credentials: any = {};
  formConfig = {
    organizationId: [''],
      empTypeId: ['51'],
      code: ['', [ValidationService.maxLength(50)]],
      name: ['', [ValidationService.maxLength(50)]],
      isUsed: [''],
      isInFlag: [''],
      hasSoldierInfo: [''],
      hasLabourContractInfo: [''],
  };
  constructor(public actr: ActivatedRoute
            , private modalService: NgbModal
            , private empTypeService: EmpTypeService
            , private app: AppComponent) {
    super(actr, RESOURCE.EMP_TYPE, ACTION_FORM.SEARCH);
    this.setMainService(empTypeService);
    this.formSearch = this.buildForm({}, this.formConfig);
  }

  ngOnInit() {
    this.processSearch();
  }

  get f () {
    return this.formSearch.controls;
  }

  public onChangeDate() {
    console.log('onDateChanged');
  }

  /**
   * prepareUpdate
   * param item
   */
  prepareSaveOrUpdate(item) {
    const modalRef = this.modalService.open(EmpTypeFormComponent, DEFAULT_MODAL_OPTIONS);
    if (item && item.empTypeId > 0) {
      this.empTypeService.findOne(item.empTypeId)
        .subscribe(res => {
          modalRef.componentInstance.setFormValue(this.propertyConfigs, res.data);
        });
    } else {
      modalRef.componentInstance.setFormValue(this.propertyConfigs);
    }
    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
      if (this.empTypeService.requestIsSuccess(result)) {
        this.processSearch(null);
      }
    });
  }

  /**
   * prepareDelete
   * param item
   */
  processDelete(item) {
    if (item && item.empTypeId > 0) {
      this.app.confirmDelete(null, () => {// on accepted
        this.empTypeService.deleteById(item.empTypeId)
        .subscribe(res => {
          if (this.empTypeService.requestIsSuccess(res)) {
            this.processSearch(null);
          }
        });
      }, () => {// on rejected

      });
    }
  }

  onChange(event) {
    console.log(event);
    console.log(this.formSearch.value);
  }
}
