import { CommonUtils } from './../../../shared/services/common-utils.service';
import { RESOURCE } from '@app/core/app-config';
import { PropertyResolver } from './../../../shared/services/property.resolver';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmpTypePageComponent } from './emp-type-page.component';

const routes: Routes = [
  {
    path: '',
    component: EmpTypePageComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.EMP_TYPE,
      nationId: CommonUtils.getNationId()
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmpTypeRoutingModule { }
