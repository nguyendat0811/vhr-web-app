import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EmpTypeService, RESOURCE, ACTION_FORM } from '@app/core';
import { AppComponent } from '@app/app.component';
import { ValidationService } from '@app/shared/services';

@Component({
  selector: 'emp-type-form',
  templateUrl: './emp-type-form.component.html',
})
export class EmpTypeFormComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  @Input() item: any;
  formConfig = {
    empTypeId: [null],
    code: [null, [ValidationService.required, ValidationService.maxLength(50)]],
    name: [null, [ValidationService.required, ValidationService.maxLength(50)]],
    isUsed: [null],
    isInFlag: [null],
    hasSoldierInfo: [null],
    hasLabourContractInfo: [null],
  };
  constructor(public actr: ActivatedRoute
            , public activeModal: NgbActiveModal
            , private empTypeService: EmpTypeService
            , private app: AppComponent) {
    super(actr, RESOURCE.EMP_TYPE, ACTION_FORM.INSERT);
    this.formSave = this.buildForm({}, this.formConfig);

  }

  /**
   * ngOnInit
   */
  ngOnInit() {
  }

  /**
   * processSaveOrUpdate
   */
  processSaveOrUpdate() {
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    } else {
      if ((!this.formSave.value.hasSoldierInfo && !this.formSave.value.hasLabourContractInfo)
            || (this.formSave.value.hasSoldierInfo && this.formSave.value.hasLabourContractInfo)) {
        this.empTypeService.processReturnMessage({ type: 'WARNING', code: 'emptype.isSolderOrContract'});
        return;
      }
      this.app.confirmMessage(null, () => {// on accepted
        this.empTypeService.saveOrUpdate(this.formSave.value)
          .subscribe(res => {
            if (this.empTypeService.requestIsSuccess(res)) {
              this.activeModal.close(res);
            }
          });
      }, () => {// on rejected

      });
    }

  }

  /****************** CAC HAM COMMON DUNG CHUNG ****/
  /**
   * f
   */
  get f () {
    return this.formSave.controls;
  }

  /**
   * setFormValue
   * param: Array propertyConfigs
   * param data
   */
  public setFormValue(propertyConfigs: any, data?: any) {
    this.propertyConfigs = propertyConfigs;
    if (data && data.empTypeId > 0) {
      this.formSave = this.buildForm(data, this.formConfig, ACTION_FORM.UPDATE);
    } else {
      this.formSave = this.buildForm({}, this.formConfig, ACTION_FORM.INSERT);
    }

  }

}
