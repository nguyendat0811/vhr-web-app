import { PositionSalaryIndexComponent } from './position-salary-index/position-salary-index.component';
import { PositionSalaryRoutingModule } from './position-salary-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@app/shared';
import { PositionSalarySearchComponent } from './position-salary-search/position-salary-search.component';
import { PositionSalaryFormComponent } from './position-salary-form/position-salary-form.component';

@NgModule({
  declarations: [PositionSalaryIndexComponent, PositionSalarySearchComponent, PositionSalaryFormComponent],
  imports: [
    CommonModule,
    SharedModule,

    PositionSalaryRoutingModule,
  ],
  entryComponents: [PositionSalaryFormComponent]
})
export class PositionSalaryModule { }
