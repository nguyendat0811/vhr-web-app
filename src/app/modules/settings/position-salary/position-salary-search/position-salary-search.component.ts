import { PositionSalaryFormComponent } from './../position-salary-form/position-salary-form.component';
import { DEFAULT_MODAL_OPTIONS } from '@app/core/app-config';
import { AppComponent } from '@app/app.component';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from './../../../../shared/components/base-component/base-component.component';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RESOURCE, ACTION_FORM, SysCatService, APP_CONSTANTS } from '@app/core';
import { Validators } from '@angular/forms';
import { ValidationService } from '@app/shared/services';
import { PositionSalaryService } from '@app/core/services/position-salary/position-salary.service';

@Component({
  selector: 'position-salary-search',
  templateUrl: './position-salary-search.component.html'
})
export class PositionSalarySearchComponent extends BaseComponent implements OnInit {
  positionTypeList: any = {};
  formConfig = {
    code: ['' , [ValidationService.maxLength(50)]],
    name: ['' , [ValidationService.maxLength(50)]],
    positionTypeId: [''],
  };

  constructor(private modalService: NgbModal
    , public actr: ActivatedRoute
    , private sysCatService: SysCatService
    , private app: AppComponent
    , private positionSalaryService: PositionSalaryService
    ) {
      super(actr, RESOURCE.POSITION_SALARY, ACTION_FORM.SEARCH);
      this.setMainService(positionSalaryService);

      this.formSearch = this.buildForm({}, this.formConfig);

      this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.POSITION_TYPY_LIST).subscribe(
        res => this.positionTypeList = res.data
      );
     }

  ngOnInit() {
    this.processSearch();
  }

  get f () {
    return this.formSearch.controls;
  }

  /**
   * prepareUpdate
   * param item
   */
  prepareSaveOrUpdate(item) {
    const modalRef = this.modalService.open(PositionSalaryFormComponent, DEFAULT_MODAL_OPTIONS);
    if (item && item.positionSalaryId > 0) {
      this.positionSalaryService.findOne(item.positionSalaryId)
        .subscribe(res => {
          modalRef.componentInstance.setFormValue(this.propertyConfigs, res.data);
        });
    } else {
      modalRef.componentInstance.setFormValue(this.propertyConfigs);
    }
    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
      if (this.positionSalaryService.requestIsSuccess(result)) {
        this.processSearch(null);
      }
      });
    }

    /**
     * prepareDelete
     * param item
     */
    processDelete(item) {
      if (item && item.positionSalaryId > 0) {
        this.app.confirmDelete(null, () => {// on accepted
          this.positionSalaryService.deleteById(item.positionSalaryId)
          .subscribe(res => {
            if (this.positionSalaryService.requestIsSuccess(res)) {
              this.processSearch(null);
            }
          });
        }, () => {// on rejected

        });
      }
    }

}
