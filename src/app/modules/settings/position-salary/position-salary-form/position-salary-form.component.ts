import { CommonUtils } from '@app/shared/services/common-utils.service';
import { ACTION_FORM, SysCatService } from '@app/core';
import { RESOURCE, APP_CONSTANTS } from './../../../../core/app-config';
import { BaseComponent } from './../../../../shared/components/base-component/base-component.component';
import { AppComponent } from '@app/app.component';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { PositionSalaryService } from '@app/core/services/position-salary/position-salary.service';
import { ValidationService } from '@app/shared/services';

@Component({
  selector: 'position-salary-form',
  templateUrl: './position-salary-form.component.html'
})
export class PositionSalaryFormComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  positionTypeList: any = {};

  formConfig = {
    positionSalaryId: [null],
    code: [null, [ValidationService.required, ValidationService.maxLength(50)]],
    name: [null, [ValidationService.required, ValidationService.maxLength(50)]],
    positionTypeId: [null, [ValidationService.required]],
  };
  constructor(public actr: ActivatedRoute
    , public activeModal: NgbActiveModal
    , private sysCatService: SysCatService
    , private positionSalaryService: PositionSalaryService
    , private app: AppComponent) {
      super(actr, RESOURCE.POSITION_SALARY, ACTION_FORM.INSERT);
      this.formSave = this.buildForm({}, this.formConfig);

      this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.POSITION_TYPY_LIST).subscribe(
        res => this.positionTypeList = res.data
      );
     }

  ngOnInit() {
  }

  get f () {
    return this.formSave.controls;
  }
    /**
   * processSaveOrUpdate
   */
  processSaveOrUpdate() {
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    } else {
      this.app.confirmMessage(null, () => {// on rejected
        this.positionSalaryService.saveOrUpdate(this.formSave.value)
        .subscribe(res => {
          if (this.positionSalaryService.requestIsSuccess(res)) {
            this.activeModal.close(res);
          }
        });
      }, () => {// on rejected

      });
    }
  }

  /**
   * setFormValue
   * param: Array propertyConfigs
   * param data
   */
  public setFormValue(propertyConfigs: any, data?: any) {
    this.propertyConfigs = propertyConfigs;
    if (data && data.positionSalaryId > 0) {
      this.formSave = this.buildForm(data, this.formConfig, ACTION_FORM.UPDATE);
    } else {
      this.formSave = this.buildForm({}, this.formConfig, ACTION_FORM.INSERT);
    }
  }
}
