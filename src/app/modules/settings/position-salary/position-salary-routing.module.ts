import { PositionSalaryIndexComponent } from './position-salary-index/position-salary-index.component';
import { CommonUtils } from './../../../shared/services/common-utils.service';
import { RESOURCE } from './../../../core/app-config';
import { PropertyResolver } from '@app/shared/services/property.resolver';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

const routes: Routes = [
  {
    path: '',
    component: PositionSalaryIndexComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.POSITION_SALARY,
      nationId: CommonUtils.getNationId()
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PositionSalaryRoutingModule { }
