import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { RESOURCE } from '@app/core';

@Component({
  selector: 'position-salary-index',
  templateUrl: './position-salary-index.component.html'
})
export class PositionSalaryIndexComponent extends BaseComponent implements OnInit {

  constructor(public act: ActivatedRoute) { 
    super(act, RESOURCE.POSITION_SALARY);
  }

  ngOnInit() {
  }

}
