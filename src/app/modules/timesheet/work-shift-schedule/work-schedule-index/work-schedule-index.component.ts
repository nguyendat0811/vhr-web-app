import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { RESOURCE } from '@app/core';

@Component({
  selector: 'work-schedule-index',
  templateUrl: './work-schedule-index.component.html'
})
export class WorkScheduleIndexComponent extends BaseComponent implements OnInit {

  constructor(public act: ActivatedRoute) { 
    super(act, RESOURCE.WORK_SCHEDULE);
  }

  ngOnInit() {
  }

}
