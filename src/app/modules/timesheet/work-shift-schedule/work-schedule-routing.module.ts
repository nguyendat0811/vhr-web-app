import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PropertyResolver } from '@app/shared/services/property.resolver';
import { RESOURCE } from '@app/core';
import { CommonUtils } from '@app/shared/services';
import { WorkScheduleIndexComponent } from './work-schedule-index/work-schedule-index.component';
import { WorkScheduleFormComponent } from './work-schedule-form/work-schedule-form.component';

const routes: Routes = [
  {
    path: '',
    component: WorkScheduleIndexComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.WORK_SCHEDULE,
      nationId: CommonUtils.getNationId()
    }
  },
  {
    path: 'add',
    component: WorkScheduleFormComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.WORK_SCHEDULE,
      nationId: CommonUtils.getNationId()
    }
  },
  {
    path: 'edit/:workScheduleId',
    component: WorkScheduleFormComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.WORK_SCHEDULE,
      nationId: CommonUtils.getNationId()
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WorkScheduleRoutingModule { }
