import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { ACTION_FORM, RESOURCE, SysCatService, APP_CONSTANTS } from '@app/core';
import { Validators, FormGroup, FormArray } from '@angular/forms';
import { ValidationService, CommonUtils } from '@app/shared/services';
import { AppComponent } from '@app/app.component';
import { HelperService } from '@app/shared/services/helper.service';
import {WorkdayTypeService} from "@app/core/services/hr-timesheet/workday-type.service";

@Component({
  selector: 'work-schedule',
  templateUrl: './work-schedule.component.html'
})
export class WorkScheduleComponent extends BaseComponent implements OnInit {
  list: any;
  formConfig = {
    code: ['' , [ValidationService.maxLength(50)]],
    name: ['' , [ValidationService.maxLength(200)]],
    attenanceType: [''],
    lstAttenanceType: [this.list],
    marketCompanyId: [CommonUtils.getCurrentMarketCompanyId()],
  };
  formSaveConfig = {
    marketCompanyId: [CommonUtils.getCurrentMarketCompanyId()],
    workdayTypeId: [''],
    code: ['' , [ValidationService.required, ValidationService.maxLength(50)]],
    name: ['' , [ValidationService.required, ValidationService.maxLength(200)]],
    attenanceType: [''],
    note: [''],
    sysCatId: [''],
    modifiedBy: [''],
    modifiedDate: [''],
    createdBy: [''],
    createdDate: [''],
    lstAttenanceType: ['', [ValidationService.required]],
  };
  formSave: FormArray;
  attenanceTypeList: any;
  constructor(public actr: ActivatedRoute
    , private app: AppComponent
    , private helperService: HelperService
    , private workdayTypeService: WorkdayTypeService
    , public sysCatService: SysCatService) {
      super(actr, RESOURCE.ATTENDANCE_TYPE, ACTION_FORM.SEARCH);
      this.setMainService(workdayTypeService);
      this.formSearch = this.buildForm({}, this.formConfig);
     // this.buildFormSave({});
      this.attenanceTypeList = APP_CONSTANTS.ATTENDANCE_TYPE;
}

  ngOnInit() {
  }

}
