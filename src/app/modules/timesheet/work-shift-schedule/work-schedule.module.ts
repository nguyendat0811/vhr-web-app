import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '@app/shared';
import {WorkScheduleRoutingModule} from './work-schedule-routing.module';
import {WorkScheduleIndexComponent} from './work-schedule-index/work-schedule-index.component';
import {WorkScheduleSearchComponent} from './work-schedule-search/work-schedule-search.component';
import {WorkScheduleFormComponent} from './work-schedule-form/work-schedule-form.component';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {WorkScheduleApplyComponent} from './work-schedule-apply/work-schedule-apply.component';
import {SaturdayCalendarComponent} from './saturday-calendar/saturday-calendar.component';
import {WorkScheduleComponent} from './work-schedule-defintion/work-schedule.component';
import {WorkScheduleEmployeeComponent} from './work-schedule-employee/work-schedule-employee.component';

@NgModule({
// tslint:disable-next-line: max-line-length
  declarations: [WorkScheduleIndexComponent, WorkScheduleComponent, WorkScheduleSearchComponent, WorkScheduleFormComponent,
    WorkScheduleApplyComponent, SaturdayCalendarComponent, WorkScheduleEmployeeComponent],
  imports: [
    CommonModule,
    SharedModule,
    WorkScheduleRoutingModule,
  ],
  entryComponents: [WorkScheduleSearchComponent, WorkScheduleApplyComponent, SaturdayCalendarComponent, WorkScheduleEmployeeComponent],
  providers: [NgbActiveModal]
})
export class WorkScheduleModule {
}
