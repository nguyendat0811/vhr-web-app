import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {
  ACTION_FORM,
  APP_CONSTANTS,
  DEFAULT_MODAL_OPTIONS,
  MEDIUM_MODAL_OPTIONS,
  RESOURCE,
  SysCatService
} from '@app/core';
import {CommonUtils, ValidationService} from '@app/shared/services';
import {FormArray, FormGroup} from '@angular/forms';
import {BaseComponent} from '@app/shared/components/base-component/base-component.component';
import * as moment from 'moment';
import {AppComponent} from '@app/app.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {WorkScheduleApplyComponent} from '../work-schedule-apply/work-schedule-apply.component';
import {WorkScheduleEmployeeComponent} from '../work-schedule-employee/work-schedule-employee.component';
import {WorkScheduleService} from "@app/core/services/hr-timesheet/work-schedule.service";

@Component({
  selector: 'work-schedule-search',
  templateUrl: './work-schedule-search.component.html'
})
export class WorkScheduleSearchComponent extends BaseComponent implements OnInit {
  formSearch: FormGroup;
  listFormType: any;
  listAttendanceConfig: any;
  listWorkTypeConfig: any;
  listInsuranceConfig: any;
  formConfig = {
    attendanceTypeId: [''],
    effectiveDate: [''],
    expiredDate: [''],
    type: [''],
    saturdayType: [''],
    insuranceType: [''],
    attendanceType: [''],
    keyword: [''],
    isDecrease: [1]
  };

  constructor(public actr: ActivatedRoute
    , private router: Router
    , private workScheduleService: WorkScheduleService
    , private sysCatService: SysCatService
    , private app: AppComponent
    , private modalService: NgbModal
    , private scheduleService: WorkScheduleService) {
    super(actr, RESOURCE.WORK_SCHEDULE, ACTION_FORM.SEARCH);
    this.setMainService(workScheduleService);
    this.listAttendanceConfig = APP_CONSTANTS.ATTENDANCE_CONFIGURATION;
    this.listWorkTypeConfig = APP_CONSTANTS.WORK_TYPE_CONFIGURATION;
    this.listInsuranceConfig = APP_CONSTANTS.INSURANCE_TYPE_CONFIGURATION;

    this.formSearch = this.buildForm({}, this.formConfig, ACTION_FORM.SEARCH
      , [ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.detailReport.toDate')]);
    this.processSearch();
  }

  public processSearch(event?): void {
    if (!CommonUtils.isValidForm(this.formSearch)) {
      return;
    }
    const params = this.formSearch ? this.formSearch.value : null;
    console.log("--------------- event tìm kiếm là:  " + JSON.stringify(params) + " ------------------------------");
    this.workScheduleService.search(params, event).subscribe(res => {
      const results = JSON.stringify(res);
      console.log('ghi log thong tin tim kiem ca cong' + results);
      this.resultList = res;

    });
  }

  ngOnInit() {
  }


  get f() {
    return this.formSearch.controls;
  }

  checkEffectiveDate(item) {
    const now = new Date();
    const expiredDate = new Date(item.expiredDate);
    now.setHours(0, 0, 0, 0);
    if (item.expiredDate === null) {
      return true;
    }
    return expiredDate.getTime() < now.getTime() ? false : true;
  }

  processDelete(item) {
    if (item && item.id > 0) {
      this.app.confirmDelete(null, () => {// on accepted
        this.workScheduleService.deleteById(item.id)
          .subscribe(res => {
            this.processSearch(null);

            /*if (this.workScheduleService.requestIsSuccess(res)) {
              this.processSearch(null);
            }*/
          });
      }, () => {// on rejected

      });
    }
  }

  prepareSaveOrUpdate(item) {
    if (item && item.id > 0) {
      this.router.navigate(['/timesheet/work-shift-schedule/edit', item.id]).then(r => item);
    }
  }

  printSelectedDate(formArray: FormArray) {
    const list = [];
    if (formArray && formArray.controls) {
      for (const control of formArray.controls) {
        list.push(moment(control.value).format('DD/MM/YYYY'));
      }
    }
    return list.join(', ');
  }

  //bao cao

  processExportDetail() {
    if (!CommonUtils.isValidForm(this.formSearch)) {
      return;
    }
    // khoi tao gia tri cho truong null khi export
    if (this.formSearch.get('saturdayType').value == null) {
      this.formSearch.controls['saturdayType'].setValue('');
    }
    if (this.formSearch.get('type').value == null) {
      this.formSearch.controls['type'].setValue('');
    }
    //
    const params = this.formSearch.value;
    this.workScheduleService.processDetailReport(params).subscribe(res => {
      saveAs(res, 'work_schedule_report.xls');
    });
  }

  // kiem tra nut apply
  checkApply(data?: any) {
    if (data == null) {
      return false;
    }
    const dateNow = new Date().getTime();
    if (data < dateNow) {
      if (dateNow - data < 99999999) {
        return false;
      }
      return true;
    }
    return false;
  }

  // hien thu cau hinh thu 7
  changeHasSatConf() {
    const type = this.f.type.value;
    if (type === 2) {
      this.f.saturdayType.setValue(1);
    } else {
      this.f.saturdayType.setValue(null);
    }
  }

  processApplyEmployee(data) {
    const modalRef = this.modalService.open(WorkScheduleEmployeeComponent, MEDIUM_MODAL_OPTIONS);
    if (data) {
      modalRef.componentInstance.setFormValue(data);
    }
    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
      if (this.workScheduleService.requestIsSuccess(result)) {
        this.processSearch();
      }
    });
  }

  processCheckApply(item) {
    this.workScheduleService.processCheckApply(item.workScheduleId).subscribe(
      res => {
        console.log('-------------xư lý thong tim kiếm phân ca: ---------------');
        if (this.workScheduleService.requestIsSuccess(res)) {
          if (res.data && res.data.length > 0) {
            const modalRef = this.modalService.open(WorkScheduleApplyComponent, DEFAULT_MODAL_OPTIONS);
            modalRef.componentInstance.setFormValue(item.workScheduleId, res);
            modalRef.result.then((result) => {
              if (!result) {
                return;
              }
              if (this.workScheduleService.requestIsSuccess(result)) {
                this.processSearch();
              }
            });
          } else {
            setTimeout(() => {
              this.app.confirmMessage('workSchedule.apply.defaultMessageComfirm', () => {// on accepted
                this.workScheduleService.processApply(item.workScheduleId, {applyData: null}).subscribe(
                  resApply => {
                    if (this.workScheduleService.requestIsSuccess(resApply)) {
                      this.processSearch();
                    }
                  }
                );
              }, () => {// on rejected

              });
            }, 500);
          }
        } else if (this.workScheduleService.requestIsConfirm(res) && res.message === 'empWorkScheduleExist') {
          this.app.confirmMessage('workSchedule.apply.empWorkScheduleExist',
            () => {// on accepted
              this.workScheduleService.processApply(item.workScheduleId, {applyData: null}).subscribe(
                resApply => {
                  if (this.workScheduleService.requestIsSuccess(resApply)) {
                    this.processSearch();
                  }
                }
              );
            }, () => {// on rejected
            });
        }
      }
    );
  }

}
