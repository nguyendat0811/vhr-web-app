import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ACTION_FORM, APP_CONSTANTS, DEFAULT_MODAL_OPTIONS, RESOURCE, WorkdayTypeService} from '@app/core';
import {CommonUtils, ValidationService} from '@app/shared/services';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {BaseComponent} from '@app/shared/components/base-component/base-component.component';
import * as moment from 'moment';
import {AppComponent} from '@app/app.component';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {SaturdayCalendarComponent} from '../saturday-calendar/saturday-calendar.component';
import {TranslationService} from 'angular-l10n';
import {HelperService} from "@app/shared/services/helper.service";
import {MessageService} from "primeng/api";
import {WorkScheduleService} from "@app/core/services/hr-timesheet/work-schedule.service";

@Component({
  selector: 'work-schedule-form',
  templateUrl: './work-schedule-form.component.html',
})

export class WorkScheduleFormComponent extends BaseComponent implements OnInit {
  applyCount: any;
  mapDateTimekeepingToTal: any = {};
  lstDateTimekeepingToTal: any = {};
  workdayTypeList: any = {};
  list: any;
  listFormType: any;
  listAttendanceConfig: any;
  formWorkSchedule: FormGroup;
  formTableConfig: FormArray;
  formTableWeekendConfig: FormArray;
  isHidden: boolean;
  hasSatConf: any;
  checkedFlag: any;
  id: null;
  public mapDayChosen = {};
  listDayError: any;
  formConfig = {
    formType: [''],
    note: [''],
    workTypeId: [1, [ValidationService.required, ValidationService.maxLength(50)]],
    effectiveDate: [new Date(), [ValidationService.afterCurrentDate], [ValidationService.required]],
    expiredDate: [null, [ValidationService.afterCurrentDate]],
    saturdayType: [null],
    applyCount: [''],
    isDecrease: [1],
    workCode: ['', [ValidationService.required]],
    workName: ['', [ValidationService.required]],
    workFrom: '',
    workTo: '',
    freeTime: [2],
    totalWorkTime: [0],
    insuranceId: [null, [ValidationService.required]]
  };

  time: string;

  setNow() {
    let now = new Date();
    let hours = ("0" + now.getHours()).slice(-2);
    let minutes = ("0" + now.getMinutes()).slice(-2);
    let str = hours + ':' + minutes;
    this.formConfig.workFrom = str;
  }

  formSaveWeekendTableConfig = {
    workFrom: [''],
    workTo: [''],
    hours: ['', [ValidationService.required, ValidationService.positiveInteger, Validators.max(10), Validators.min(1)]],
    workdayTypeCode: [null, [ValidationService.required]],
  };
  dayOfWeekList: any;
  insuranceFormList: any;
  type: any;

  constructor(public actr: ActivatedRoute
    , public translationService: TranslationService
    , public helperService: HelperService
    , public translation: TranslationService
    , private router: Router
    , private workScheduleService: WorkScheduleService
    , private modalService: NgbModal
    , public activeModal: NgbActiveModal
    , private workdayTypeService: WorkdayTypeService
    , private messageService: MessageService
    , private app: AppComponent) {
    super(actr, RESOURCE.WORK_SCHEDULE, ACTION_FORM.SEARCH);
    this.setMainService(WorkScheduleService);
    this.formWorkSchedule = this.buildForm({saturdayType: 1}, this.formConfig, ACTION_FORM.INSERT
      , [ValidationService.notBefore('effectiveDate', 'expiredDate', 'app.contractProcess.expiredDate')]);

    this.dayOfWeekList = APP_CONSTANTS.DAY_OF_WEEK;
    this.insuranceFormList = APP_CONSTANTS.INSURANCE_TYPE_CONFIGURATION;
    this.listAttendanceConfig = APP_CONSTANTS.ATTENDANCE_CONFIGURATION;
    this.workdayTypeService.findAllWorkdayTypeByMarketId().subscribe(
      res => {
        this.workdayTypeList = res.data;
      });
    this.isHidden = true;
    this.setNow();
  }

  ngOnInit() {
    const params = this.actr.snapshot.params;
    this.formConfig.workTypeId = null;

    if (params && CommonUtils.isValidId(params.workScheduleId)) {
      this.formConfig.workTypeId = params.workScheduleId;
    }

    if (!this.formConfig.workTypeId) {
      return;
    }

    // load thong tin chinh
    this.workScheduleService.findOne(params.workScheduleId).subscribe(res => {
      if (this.workScheduleService.requestIsSuccess(res)) {
        this.formWorkSchedule = this.buildForm(res, this.formConfig, ACTION_FORM.UPDATE);
      }
    });
  }

  get f() {
    return this.formWorkSchedule.controls;
  }

  get f1() {
    return this.formTableConfig.controls;
  }

  get f2() {
    return this.formTableWeekendConfig.controls;
  }

  // BiWeekend\
  hide() {
    this.isHidden = true;
  }

  show() {
    this.isHidden = false;
  }

  private clearData() {
    this.formTableWeekendConfig.reset();
    this.isHidden = true;
  }

  private validateBeforeSave(): boolean {
    const formWorkScheduleState = CommonUtils.isValidForm(this.formWorkSchedule);
    return formWorkScheduleState;
  }

  processCancel() {
    this.app.confirmMessage('common.message.confirm.cancel', () => {
      // on accepted
      this.router.navigate(['/timesheet/work-shift-schedule']);
    }, () => {
      // on rejected
    });
  }

  processSaveOrUpdate() {
    if (!this.validateBeforeSave()) {
      return;
    }

    const formSave = this.formWorkSchedule.value;
    formSave.id = null;

    // @ts-ignore
    if (this.formConfig.workTypeId !== null && !isNaN(parseInt(this.formConfig.workTypeId))) {
      formSave.id = this.formConfig.workTypeId;
    }

    this.app.confirmMessage('', () => {// on accept
      this.workScheduleService.saveOrUpdateExtend(formSave, this.app)
        .subscribe(res => {
          if (this.workScheduleService.requestIsSuccess(res)) {
            this.activeModal.close(res);
            const modalRef = this.router.navigate(['/timesheet/work-shift-schedule']);
          }
        });
    }, () => {// on rejected

    });
  }

  prepareCalendar(lstDateTimekeeping) {
    if (!this.formWorkSchedule.value.effectiveDate) {
      this.app.errorMessage('workSchedule', 'requireEffectiveDate');
      return;
    }
    if (!CommonUtils.isNullOrEmpty(this.outOfRangeDay(2))) {
      this.app.confirmMessageError('app.payrolls.workSchedule.outOfRangeComfirmDelete', () => { // on accepted
        this.actionActiveModal(lstDateTimekeeping);
      }, () => { // on rejected
      }, this.outOfRangeDay(2));
    } else {
      this.actionActiveModal(lstDateTimekeeping);
    }
  }

  actionActiveModal(lstDateTimekeeping) {
    const modalRef = this.modalService.open(SaturdayCalendarComponent, DEFAULT_MODAL_OPTIONS);
    modalRef.componentInstance.setLstDateTimekeeping(lstDateTimekeeping);
    modalRef.componentInstance.setLstDateTimekeepingToTal(this.mapDateTimekeepingToTal);

    // tslint:disable-next-line: max-line-length
    modalRef.componentInstance.setYearList(this.formWorkSchedule.get('effectiveDate').value, this.formWorkSchedule.get('expiredDate').value);
    modalRef.result.then((result) => {
    });
  }

  printSelectedDate(formArray: FormArray) {
    const list = [];
    if (formArray && formArray.controls) {
      for (const control of formArray.controls) {
        list.push(moment(control.value).format('DD/MM/YYYY'));
      }
    }
    return list.join(', ');
  }

  validateDayDuplicate(): Boolean {
    for (let j in this.formTableConfig.value) {
      const form = this.formTableConfig.value[j];
      for (let i in form.lstDayOfWeek) {
        if (this.mapDayChosen[form.lstDayOfWeek[i]]) {
          this.mapDayChosen = {};
          this.app.messError('ERROR', 'app.payrolls.workSchedule.isDuplicated');
          return true;
        } else {
          this.mapDayChosen[form.lstDayOfWeek[i]] = form.lstDayOfWeek[i];
        }
      }
    }
    this.mapDayChosen = {};
    return false;
  }

  validateChooseDay(): Boolean {
    for (let j in this.formTableWeekendConfig.value) {
      const form = this.formTableWeekendConfig.value[j];
      // tslint:disable-next-line: no-shadowed-variable
      if (!form.lstDateTimekeeping || form.lstDateTimekeeping.length === 0) {
        this.app.messError('ERROR', 'app.payrolls.workSchedule.notCalendar');
        return true;
      }
    }
    return false;
  }

  // hien thu cau hinh thu 7
  ChangeHasSatConf() {
    const type = this.formWorkSchedule.get('type').value;
    this.hasSatConf = type;
    if (this.hasSatConf === 2) {
      if (this.formWorkSchedule.controls['saturdayType'].value == null) {
        this.formWorkSchedule.controls['saturdayType'].setValue(1);
      }
      if (this.formWorkSchedule.controls['saturdayType'].value === 2) {
        this.isHidden = false;
      }
    } else {
      this.isHidden = true;
    }
  }

  // kiem tra ngay chon trong lich nam ngoai khoang
  outOfRangeDay(type?: any) {
    const effect = this.formWorkSchedule.get('effectiveDate').value;
    const expired = this.formWorkSchedule.get('expiredDate').value;
    const list = this.formTableWeekendConfig.value;
    this.listDayError = [];
    for (const i in list) {
      const formTableWeekendConfig = list[i];
      if (formTableWeekendConfig.lstDateTimekeeping && formTableWeekendConfig.lstDateTimekeeping.length > 0) {
        for (let i = formTableWeekendConfig.lstDateTimekeeping.length - 1; i >= 0; i--) {
          const day = formTableWeekendConfig.lstDateTimekeeping[i];
          if (day < effect || (day > expired && expired != null)) {
            const dayError = moment(day).format('DD/MM/YYYY');
            this.listDayError.push(dayError);
          }
        }
        if (type === 1) {
          if (this.listDayError.length > 0) {
            this.app.messError('ERROR', 'app.payrolls.workSchedule.outOfRange', this.listDayError);
            //this.app.messError('ERROR', 'app.payrolls.workSchedule.outOfRange' , this.listDayError);
            return true;
          }
        } else {
          return this.listDayError;
        }
      }
    }
    return false;
  }

  // load map total trong lich
  initMapTotal(list) {
    for (const i in list) {
      const formTableWeekendConfig = list[i];
      if (formTableWeekendConfig.lstDateTimekeeping && formTableWeekendConfig.lstDateTimekeeping.length > 0) {
        for (let i = formTableWeekendConfig.lstDateTimekeeping.length - 1; i >= 0; i--) {
          const control = new FormControl(formTableWeekendConfig.lstDateTimekeeping[i]);
          this.mapDateTimekeepingToTal[control.value] = control;
        }
      }
    }
  }

  weekendConfig() {
    if (this.isHidden) {
      this.isHidden = false;
    } else {   // If expired area is shown
      this.clearData();
      this.isHidden = true;
    }
  }

  public messageError(severity: string, text: string, value: any) {
    //const message = this.translation.translate(text);
    const message = text;
    const textDetail = message + ' ' + value;
    this.messageService.add({
      severity: severity.toLowerCase()
      , summary: this.translation.translate(`app.messageSummary`)
      , detail: textDetail
    });
  }

  /**
   * process return message
   * param serviceResponse
   */
  public processReturnMessage(serviceResponse: any) {
    if (!serviceResponse) {
      return;
    }
    if (serviceResponse.status === 500 || serviceResponse.status === 0) {
      this.errorMessage('haveError');
      return;
    }
    if (serviceResponse.code) {
      this.toastMessage(serviceResponse.type, serviceResponse.code, serviceResponse.message);
      return;
    }
  }

  /**
   * toastMessage
   * param severity
   * param errorType
   * param errorCode
   */
  public toastMessage(severity: string, code: string, message?: string) {
    let detail;
    message = severity === 'CONFIRM' ? null : message;
    severity = severity === 'CONFIRM' ? 'WARNING' : severity;
    if (!message) {
      detail = this.translation.translate(`${severity}.${code}`);
    } else {
      detail = this.translation.translate(`${severity}.${code}.${message}`);
    }
    severity = severity === 'WARNING' ? 'WARN' : severity;
    const summary = this.translation.translate(`app.messageSummary`);
    this.messageService.add({severity: severity.toLowerCase(), summary: summary, detail: detail});
  }

  /**
   * errorMessage
   * param errorType
   * param errorCode
   */
  errorMessage(code: string, message?: string) {
    this.toastMessage('ERROR', code, message);
  }
}
