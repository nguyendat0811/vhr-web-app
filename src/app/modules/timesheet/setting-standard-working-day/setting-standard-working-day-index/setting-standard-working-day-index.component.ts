import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { RESOURCE } from '@app/core';

@Component({
  selector: 'setting-standard-working-day-index',
  templateUrl: './setting-standard-working-day-index.component.html'
})
export class SettingStandardWorkingDayIndexComponent extends BaseComponent implements OnInit {

  constructor(public act: ActivatedRoute) {
    super(act, RESOURCE.WORK_SCHEDULE);
  }

  ngOnInit() {
  }

}
