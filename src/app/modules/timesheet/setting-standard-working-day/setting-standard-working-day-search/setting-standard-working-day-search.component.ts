import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {
  ACTION_FORM,
  APP_CONSTANTS,
  DEFAULT_MODAL_OPTIONS,
  EmpTypeService,
  MEDIUM_MODAL_OPTIONS,
  RESOURCE,
  SysCatService
} from '@app/core';
import {CommonUtils, ValidationService} from '@app/shared/services';
import {FormArray, FormGroup} from '@angular/forms';
import {BaseComponent} from '@app/shared/components/base-component/base-component.component';
import {SettingStandardWorkingDayService} from '@app/core/services/hr-timesheet/setting-standard-workingday.service';
import * as moment from 'moment';
import {AppComponent} from '@app/app.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {SettingStandardWorkingDayEmployeeComponent} from "@app/modules/timesheet/setting-standard-working-day/setting-standard-working-day-employee/setting-standard-working-day-employee.component";
import {SettingStandardWorkingDayApplyComponent} from "@app/modules/timesheet/setting-standard-working-day/setting-standard-working-day-apply/setting-standard-working-day-apply.component";
import {ImportSettingStandardWorkingDayComponent} from "@app/modules/timesheet/setting-standard-working-day/import-setting-standard-working-day/import-setting-standard-working-day.component";

@Component({
  selector: 'setting-standard-working-day-search',
  templateUrl: './setting-standard-working-day-search.component.html'
})
export class SettingStandardWorkingDaySearchComponent extends BaseComponent implements OnInit {
  formSearch: FormGroup;
  listAttendanceConfig: any;
  listMarketCompany: [];
  listEmpType: [];
  listAllEmpType: [];
  formConfig = {
    organizationId: ['', [ValidationService.required]],
    effectiveDate: [''],
    expiredDate: [''],
    type: [''],
    employeeCode: [''],
    fullName: [''],
  };

  constructor(public actr: ActivatedRoute
    , private router: Router
    , private settingStandardWorkingDayService: SettingStandardWorkingDayService
    , private sysCatService: SysCatService
    , private app: AppComponent, private empTypeService: EmpTypeService
    , private modalService: NgbModal) {
    super(actr, RESOURCE.WORK_SCHEDULE, ACTION_FORM.SEARCH);
    this.setMainService(settingStandardWorkingDayService);
    this.listAttendanceConfig = APP_CONSTANTS.ATTENDANCE_CONFIGURATION;
    this.formSearch = this.buildForm({}, this.formConfig, ACTION_FORM.SEARCH
      , [ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.detailReport.toDate')]);
    this.processSearch();
  }

  ngOnInit() {
  }

  get f() {
    return this.formSearch.controls;
  }

  processDelete(item) {
    if (item && item.id > 0) {
      this.app.confirmDelete(null, () => {// on accepted
        this.settingStandardWorkingDayService.deleteById(item.id)
          .subscribe(res => {
            if (this.settingStandardWorkingDayService.requestIsSuccess(res)) {
              this.processSearch(null);
            }
          });
      }, () => {// on rejected

      });
    }
  }

  prepareSaveOrUpdate(item) {
    //prompt("please press ctrl+c to copy the text below", JSON.stringify(item))

    if (item && item.id > 0) {
      this.settingStandardWorkingDayService.findOne(item.id)
        .subscribe(res => {
          //prompt("please press ctrl+c to copy the text below", JSON.stringify(res))
          const modalRef = this.router.navigate(['/timesheet/setting-standard-working-day/edit', item.id]);

        });
    }
  }

  printSelectedDate(formArray: FormArray) {
    const list = [];
    if (formArray && formArray.controls) {
      for (const control of formArray.controls) {
        list.push(moment(control.value).format('DD/MM/YYYY'));
      }
    }
    return list.join(', ');
  }

  processApplyEmployee(data) {
    const modalRef = this.modalService.open(SettingStandardWorkingDayEmployeeComponent, MEDIUM_MODAL_OPTIONS);
    if (data) {
      modalRef.componentInstance.setFormValue(data);
    }
    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
      if (this.settingStandardWorkingDayService.requestIsSuccess(result)) {
        this.processSearch();
      }
    });
  }

  processCheckApply(item) {
    this.settingStandardWorkingDayService.processCheckApply(item.workScheduleId).subscribe(
      res => {
        if (this.settingStandardWorkingDayService.requestIsSuccess(res)) {
          if (res.data && res.data.length > 0) {
            const modalRef = this.modalService.open(SettingStandardWorkingDayApplyComponent, DEFAULT_MODAL_OPTIONS);
            modalRef.componentInstance.setFormValue(item.workScheduleId, res);
            modalRef.result.then((result) => {
              if (!result) {
                return;
              }
              if (this.settingStandardWorkingDayService.requestIsSuccess(result)) {
                this.processSearch();
              }
            });
          } else {
            setTimeout(() => {
              this.app.confirmMessage('workSchedule.apply.defaultMessageComfirm', () => {// on accepted
                this.settingStandardWorkingDayService.processApply(item.workScheduleId, {applyData: null}).subscribe(
                  resApply => {
                    if (this.settingStandardWorkingDayService.requestIsSuccess(resApply)) {
                      this.processSearch();
                    }
                  }
                );
              }, () => {// on rejected

              });
            }, 500);
          }
        } else if (this.settingStandardWorkingDayService.requestIsConfirm(res) && res.message === 'empWorkScheduleExist') {
          this.app.confirmMessage('workSchedule.apply.empWorkScheduleExist',
            () => {// on accepted
              this.settingStandardWorkingDayService.processApply(item.workScheduleId, {applyData: null}).subscribe(
                resApply => {
                  if (this.settingStandardWorkingDayService.requestIsSuccess(resApply)) {
                    this.processSearch();
                  }
                }
              );
            }, () => {// on rejected
            });
        }
      }
    );
  }

  processImportOpenCloseMarket() {
    const modalRef = this.modalService.open(ImportSettingStandardWorkingDayComponent, DEFAULT_MODAL_OPTIONS);
    modalRef.componentInstance.setFormValue(this.f.id.value);
    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
      this.processSearch(null);
    });
  }

  public processSearch(event?): void {
    if (!CommonUtils.isValidForm(this.formSearch)) {
      return;
    }
    const params = this.formSearch ? this.formSearch.value : null;
    if (!event) {
      event = {
        "first": 0,
        "rows": 1,
        "sortOrder": 1,
        "filters": {},
        "globalFilter": null
      };
    }

    this.settingStandardWorkingDayService.search(params, event).subscribe(res => {
      this.resultList = res;
    });
    if (!event) {
      if (this.dataTable) {
        this.dataTable.first = 0;
      }
    }
  }
}
