import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PropertyResolver} from '@app/shared/services/property.resolver';
import {RESOURCE} from '@app/core';
import {CommonUtils} from '@app/shared/services';
import {SettingStandardWorkingDayIndexComponent} from "@app/modules/timesheet/setting-standard-working-day/setting-standard-working-day-index/setting-standard-working-day-index.component";
import {SettingStandardWorkingDayFormComponent} from "@app/modules/timesheet/setting-standard-working-day/setting-standard-working-day-form/setting-standard-working-day-form.component";

const routes: Routes = [
  {
    path: '',
    component: SettingStandardWorkingDayIndexComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.WORK_SCHEDULE,
      nationId: CommonUtils.getNationId()
    }
  },
  {
    path: 'add',
    component: SettingStandardWorkingDayFormComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.WORK_SCHEDULE,
      nationId: CommonUtils.getNationId()
    }
  },
  {
    path: 'edit/:id',
    component: SettingStandardWorkingDayFormComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.WORK_SCHEDULE,
      nationId: CommonUtils.getNationId()
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
/*WorkScheduleRoutingModule*/
export class SettingStandardWorkingDayRoutingModule {
}
