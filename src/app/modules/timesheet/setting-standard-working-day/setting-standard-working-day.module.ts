import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '@app/shared';
import {SettingStandardWorkingDayRoutingModule} from './setting-standard-working-day-routing';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {SaturdayCalendarComponent} from './saturday-calendar/saturday-calendar.component';
import {SettingStandardWorkingDayIndexComponent} from "@app/modules/timesheet/setting-standard-working-day/setting-standard-working-day-index/setting-standard-working-day-index.component";
import {SettingStandardWorkingDayApplyComponent} from "@app/modules/timesheet/setting-standard-working-day/setting-standard-working-day-apply/setting-standard-working-day-apply.component";
import {SettingStandardWorkingDayFormComponent} from "@app/modules/timesheet/setting-standard-working-day/setting-standard-working-day-form/setting-standard-working-day-form.component";
import {SettingStandardWorkingDayComponent} from "@app/modules/timesheet/setting-standard-working-day/setting-standard-working-day-defintion/setting-standard-working-day.component";
import {SettingStandardWorkingDayEmployeeComponent} from "@app/modules/timesheet/setting-standard-working-day/setting-standard-working-day-employee/setting-standard-working-day-employee.component";
import {SettingStandardWorkingDaySearchComponent} from "@app/modules/timesheet/setting-standard-working-day/setting-standard-working-day-search/setting-standard-working-day-search.component";
import {ImportSettingStandardWorkingDayComponent} from "@app/modules/timesheet/setting-standard-working-day/import-setting-standard-working-day/import-setting-standard-working-day.component";

@NgModule({
// tslint:disable-next-line: max-line-length
  declarations: [SettingStandardWorkingDayIndexComponent, SettingStandardWorkingDayComponent, SettingStandardWorkingDaySearchComponent, SettingStandardWorkingDayFormComponent,
    SettingStandardWorkingDayApplyComponent, SaturdayCalendarComponent, SettingStandardWorkingDayEmployeeComponent, ImportSettingStandardWorkingDayComponent],
  imports: [
    CommonModule,
    SharedModule,
    SettingStandardWorkingDayRoutingModule,
  ],
  entryComponents: [SettingStandardWorkingDaySearchComponent, SettingStandardWorkingDayApplyComponent, SaturdayCalendarComponent, SettingStandardWorkingDayEmployeeComponent, ImportSettingStandardWorkingDayComponent],
  providers: [NgbActiveModal]
})
export class SettingStandardWorkingDayModule {
}
