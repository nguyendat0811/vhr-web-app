import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute} from '@angular/router';
import {AppComponent} from '@app/app.component';
import {FormGroup} from '@angular/forms';
import {ACTION_FORM, RESOURCE} from '@app/core/app-config';
import {BaseComponent} from '@app/shared/components/base-component/base-component.component';
import {FileControl} from '@app/core/models/file.control';
import {CommonUtils, ValidationService} from '@app/shared/services';
import {OrgPlanService} from '@app/core';
import {saveAs} from 'file-saver';
import {SettingStandardWorkingDayService} from '@app/core/services/hr-timesheet/setting-standard-workingday.service';

@Component({
  selector: 'import-setting-standard-working-day',
  templateUrl: './import-setting-standard-working-day.component.html'
})
export class ImportSettingStandardWorkingDayComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  dataError: any;
  formConfig = {
    orgPlanId: ['']
  };

  constructor(
    public activatedRoute: ActivatedRoute,
    private app: AppComponent,
    public activeModal: NgbActiveModal,
    private orgPlanService: OrgPlanService,
    private settingStandardWorkingDayService: SettingStandardWorkingDayService
  ) {
    super(activatedRoute, RESOURCE.ORGANIZATION_PLAN, ACTION_FORM.INSERT);
    this.formSave = this.buildForm({}, this.formConfig, ACTION_FORM.INSERT);
    this.formSave.addControl('fileImport', new FileControl(null, ValidationService.required));
  }

  ngOnInit() {
  }

  get f() {
    return this.formSave.controls;
  }

  setFormValue(planId: number) {
    this.formSave.get('orgPlanId').setValue(planId);
  }

  actionSubmitForm() {
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    }
    this.app.confirmMessage(null, () => {
      this.settingStandardWorkingDayService.importOpenCloseMarket(this.formSave.value).subscribe(
        res => {
          if (this.orgPlanService.requestIsSuccess(res)) {
            const fileName = res.data;
            this.settingStandardWorkingDayService.downloadResultsImport(res.data).subscribe(res => {
              saveAs(res, fileName);
            });

            this.activeModal.close(res);
          } else {
            this.dataError = res.data;
          }
        }
      );
    }, null);
  }

  actionDownloadTemplate() {
    this.settingStandardWorkingDayService.downloadTemplateImport().subscribe(res => {
      saveAs(res, 'TemplateWorkingDayStandardSetting.xlsx');
    });
  }
}
