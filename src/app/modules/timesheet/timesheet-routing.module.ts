import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { RESOURCE } from "@app/core";
import { CommonUtils } from "@app/shared/services";

const routes: Routes = [
  {
    path: '',
    redirectTo: '/dashboard/work-shift-schedule',
    pathMatch: 'full'
  }, {
    path: 'work-shift-schedule',
    loadChildren: './work-shift-schedule/work-schedule.module#WorkScheduleModule'
  },
 /* {
    path: 'open-close',
    loadChildren: './open-close-schedule/open-close-schedule.module#OpenCloseScheduleModule'
  },*/
  {
    path: 'open-close',
    loadChildren: './setting-standard-working-day/setting-standard-working-day.module#SettingStandardWorkingDayModule'
  },
  {
    path: 'setting-standard-working-day',
    loadChildren: './setting-standard-working-day/setting-standard-working-day.module#SettingStandardWorkingDayModule'
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TimeSheetRoutingModule {
}
