import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ACTION_FORM, APP_CONSTANTS, DEFAULT_MODAL_OPTIONS, RESOURCE, WorkdayTypeService} from '@app/core';
import {CommonUtils, ValidationService} from '@app/shared/services';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {BaseComponent} from '@app/shared/components/base-component/base-component.component';
import {OpenCloseScheduleService} from '@app/core/services/hr-timesheet/open-close-schedule.service';
import * as moment from 'moment';
import {AppComponent} from '@app/app.component';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {SaturdayCalendarComponent} from '../saturday-calendar/saturday-calendar.component';
import {TranslationService} from 'angular-l10n';

@Component({
  selector: 'work-schedule-form',
  templateUrl: './open-close-schedule-form.component.html',
})
export class OpenCloseScheduleFormComponent extends BaseComponent implements OnInit {
  applyCount: any;
  mapDateTimekeepingToTal: any = {};
  lstDateTimekeepingToTal: any = {};
  workdayTypeList: any = {};
  list: any;
  listFormType: any;
  listAttendanceConfig: any;
  formWorkSchedule: FormGroup;
  formTableWeekendConfig: FormArray;
  isHidden: boolean;
  hasSatConf: any;
  checkedFlag: any;
  openCloseScheduleId: any;
  public mapDayChosen = {};  listDayError: any;
  formConfig = {
    id: [null],
    formType: [''],
    type: [null],
    organizationId: ['', [ValidationService.required]],
    effectiveDate: [null, [ValidationService.required]],
    expiredDate: [null, [ValidationService.afterCurrentDate]],
    saturdayType: [null],
    applyCount: [''],
    workFrom: ['', [ValidationService.required]],
    workTo: ['', [ValidationService.required]],
    workFromWeekend: ['', [ValidationService.required]],
    workToWeekend: ['', [ValidationService.required]],
  };

  formSaveWeekendTableConfig = {
    workdayTypeId: [''],
    hours: ['', [ValidationService.required, ValidationService.positiveInteger, Validators.max(10), Validators.min(1)]],
    workdayTypeCode: [null, [ValidationService.required]],
  };
  dayOfWeekList: any;
  attendanceFormList: any;
  type: any;

  constructor(public actr: ActivatedRoute
    , public translationService: TranslationService
    , private router: Router
    , private workScheduleService: OpenCloseScheduleService
    , private modalService: NgbModal
    , public activeModal: NgbActiveModal
    , private workdayTypeService: WorkdayTypeService
    , private app: AppComponent) {
    super(actr, RESOURCE.WORK_SCHEDULE, ACTION_FORM.SEARCH);
    this.setMainService(OpenCloseScheduleService);
    this.formWorkSchedule = this.buildForm({saturdayType: 1}, this.formConfig, ACTION_FORM.INSERT
      , [ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.contractProcess.expiredDate')]);
    this.buildFormSaveWeekendConfig();
    this.dayOfWeekList = APP_CONSTANTS.DAY_OF_WEEK;
    this.attendanceFormList = APP_CONSTANTS.ATTENDANCE_FORM;
    this.listAttendanceConfig = APP_CONSTANTS.ATTENDANCE_CONFIGURATION;
    this.isHidden = true;
  }

  ngOnInit() {
    const params = this.actr.snapshot.params;

    if (params && CommonUtils.isValidId(params.id)) {
      this.openCloseScheduleId = params.id;
    }
    if (!this.openCloseScheduleId) {
      return;
    }

    // load thong tin chinh
    this.workScheduleService.findOne(this.openCloseScheduleId).subscribe(res => {
      if (this.workScheduleService.requestIsSuccess(res)) {
        this.formWorkSchedule = this.buildForm(res
          , this.formConfig
          , ACTION_FORM.UPDATE
          , [ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.contractProcess.expiredDate')]);
        // kiem tra xem da apply chua
        this.applyCount = this.formWorkSchedule.get('applyCount').value;

        // kiem tra xem co cau hinh thu 7 khong
        this.hasSatConf = this.formWorkSchedule.get('type').value;
        if (this.formWorkSchedule.controls['saturdayType'].value === 2) {
          this.show();
        }
      }
    });
  }

  get f() {
    return this.formWorkSchedule.controls;
  }

  get f2() {
    return this.formTableWeekendConfig.controls;
  }

  // BiWeekend\
  hide() {
    this.isHidden = true;
  }

  show() {
    this.isHidden = false;
    this.isHidden = false;
  }

  private buildFormSaveWeekendConfig(list?: any) {
    if (!list || list.length <= 0) {
      this.formTableWeekendConfig = new FormArray([this.makeDefaultFormSaveWeekendConfig()]);
    } else {
      const controls = new FormArray([]);
      for (const i in list) {
        const group = this.makeDefaultFormSaveWeekendConfig(1);
        const formTableWeekendConfig = list[i];
        group.patchValue(formTableWeekendConfig);
        const lstDateTimekeeping = new FormArray([]);
        if (formTableWeekendConfig.lstDateTimekeeping && formTableWeekendConfig.lstDateTimekeeping.length > 0) {
          for (let i = formTableWeekendConfig.lstDateTimekeeping.length - 1; i >= 0; i--) {
            const control = new FormControl(formTableWeekendConfig.lstDateTimekeeping[i]);
            lstDateTimekeeping.insert(0, control);
            // this.lstDateTimekeepingToTal.insert(0, control);
          }
        }
        group.addControl('lstDateTimekeeping', lstDateTimekeeping);
        controls.push(group);
      }
      this.formTableWeekendConfig = controls;
    }
  }

  private makeDefaultFormSaveWeekendConfig(num ?: any): FormGroup {
    const formGroup = this.buildForm({}, this.formSaveWeekendTableConfig, ACTION_FORM.UPDATE);
    if (!num) {
      const lstDateTimekeeping = new FormArray([]);
      formGroup.addControl('lstDateTimekeeping', lstDateTimekeeping);
    }
    return formGroup;
  }

  private validateBeforeSave(): boolean {
    if (!this.isHidden) {
      const formWorkScheduleState = CommonUtils.isValidForm(this.formWorkSchedule);
      const formTableWeekendConfigState = CommonUtils.isValidForm(this.formTableWeekendConfig);
      return formWorkScheduleState && !this.validateChooseDay()
        && formTableWeekendConfigState && !this.outOfRangeDay(1);
    } else {
      const formWorkScheduleState = CommonUtils.isValidForm(this.formWorkSchedule);
      return formWorkScheduleState
    }
  }

  processSaveOrUpdate() {
    if (!this.validateBeforeSave()) {
      return;
    }
    // gan gia tri mac dinh cho saturdayType khi chon tu thu 2 den thu 6
    if (this.hasSatConf === 1) {
      this.formWorkSchedule.controls['saturdayType'].setValue(null);
    }
    // kiem tra type
    const formSave = this.formWorkSchedule.value;
    formSave.id = null;
    if (this.openCloseScheduleId !== null && !isNaN(parseInt(this.openCloseScheduleId))) {
      formSave.id = this.openCloseScheduleId;
    }

    this.app.confirmMessage('', () => {// on accept
      this.workScheduleService.saveOrUpdateExtend(formSave, this.app)
        .subscribe(res => {
          if (this.workScheduleService.requestIsSuccess(res)) {
            this.activeModal.close(res);
            const modalRef = this.router.navigate(['/timesheet/open-close']);
          }
        });
    }, () => {// on rejected

    });
  }

  actionActiveModal(lstDateTimekeeping) {
    const modalRef = this.modalService.open(SaturdayCalendarComponent, DEFAULT_MODAL_OPTIONS);
    modalRef.componentInstance.setLstDateTimekeeping(lstDateTimekeeping);
    modalRef.componentInstance.setLstDateTimekeepingToTal(this.mapDateTimekeepingToTal);

    // tslint:disable-next-line: max-line-length
    modalRef.componentInstance.setYearList(this.formWorkSchedule.get('effectiveDate').value, this.formWorkSchedule.get('expiredDate').value);
    modalRef.result.then((result) => {
    });
  }

  printSelectedDate(formArray: FormArray) {
    const list = [];
    if (formArray && formArray.controls) {
      for (const control of formArray.controls) {
        list.push(moment(control.value).format('DD/MM/YYYY'));
      }
    }
    return list.join(', ');
  }

  validateChooseDay(): Boolean {
    for (let j in this.formTableWeekendConfig.value) {
      const form = this.formTableWeekendConfig.value[j];

      if (!form.lstDateTimekeeping || form.lstDateTimekeeping.length === 0) {
        this.app.messError('ERROR', 'app.payrolls.workSchedule.notCalendar');
        return true;
      }
    }
    return false;
  }

  // hien thu cau hinh thu 7
  ChangeHasSatConf() {
    const type = this.formWorkSchedule.get('type').value;
    this.hasSatConf = type;
    if (this.hasSatConf === 2) {
      if (this.formWorkSchedule.controls['saturdayType'].value == null) {
        this.formWorkSchedule.controls['saturdayType'].setValue(1);
      }
      if (this.formWorkSchedule.controls['saturdayType'].value === 2) {
        this.isHidden = false;
      }
    } else {
      this.isHidden = true;
    }
  }


  // kiem tra ngay chon trong lich nam ngoai khoang
  outOfRangeDay(type?: any) {
    const effect = this.formWorkSchedule.get('effectiveDate').value;
    const expired = this.formWorkSchedule.get('expiredDate').value;
    const list = this.formTableWeekendConfig.value;
    this.listDayError = [];
    for (const i in list) {
      const formTableWeekendConfig = list[i];
      if (formTableWeekendConfig.lstDateTimekeeping && formTableWeekendConfig.lstDateTimekeeping.length > 0) {
        for (let i = formTableWeekendConfig.lstDateTimekeeping.length - 1; i >= 0; i--) {
          const day = formTableWeekendConfig.lstDateTimekeeping[i];
          if (day < effect || (day > expired && expired != null)) {
            const dayError = moment(day).format('DD/MM/YYYY');
            this.listDayError.push(dayError);
          }
        }
        if (type === 1) {
          if (this.listDayError.length > 0) {
            this.app.messError('ERROR', 'app.payrolls.workSchedule.outOfRange', this.listDayError);
            //this.app.messError('ERROR', 'app.payrolls.workSchedule.outOfRange' , this.listDayError);
            return true;
          }
        } else {
          return this.listDayError;
        }
      }
    }
    return false;
  }

  // load map total trong lich
  initMapTotal(list) {
    for (const i in list) {
      const formTableWeekendConfig = list[i];
      if (formTableWeekendConfig.lstDateTimekeeping && formTableWeekendConfig.lstDateTimekeeping.length > 0) {
        for (let i = formTableWeekendConfig.lstDateTimekeeping.length - 1; i >= 0; i--) {
          const control = new FormControl(formTableWeekendConfig.lstDateTimekeeping[i]);
          this.mapDateTimekeepingToTal[control.value] = control;
          // this.lstDateTimekeepingToTal.insert(0, control);
        }
      }
    }
  }
}
