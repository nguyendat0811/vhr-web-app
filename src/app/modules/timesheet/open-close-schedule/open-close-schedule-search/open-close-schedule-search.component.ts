import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {
  ACTION_FORM,
  APP_CONSTANTS,
  DEFAULT_MODAL_OPTIONS,
  MEDIUM_MODAL_OPTIONS,
  RESOURCE,
  SysCatService
} from '@app/core';
import {ValidationService} from '@app/shared/services';
import {FormArray, FormGroup} from '@angular/forms';
import {BaseComponent} from '@app/shared/components/base-component/base-component.component';
import {OpenCloseScheduleService} from '@app/core/services/hr-timesheet/open-close-schedule.service';
import * as moment from 'moment';
import {AppComponent} from '@app/app.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {OpenCloseScheduleEmployeeComponent} from "@app/modules/timesheet/open-close-schedule/open-close-schedule-employee/open-close-schedule-employee.component";
import {OpenCloseScheduleApplyComponent} from "@app/modules/timesheet/open-close-schedule/open-close-schedule-apply/open-close-schedule-apply.component";
import {ImportOpenCloseComponent} from "@app/modules/timesheet/open-close-schedule/import-open-close-time/import-open-close.component";

@Component({
  selector: 'work-schedule-search',
  templateUrl: './open-close-schedule-search.component.html'
})
export class OpenCloseScheduleSearchComponent extends BaseComponent implements OnInit {
  formSearch: FormGroup;
  listAttendanceConfig: any;
  formConfig = {
    organizationId: [''],
    effectiveDate: [''],
    expiredDate: [''],
    type: [''],
    saturdayType: ['']
  };

  constructor(public actr: ActivatedRoute
    , private router: Router
    , private openCloseScheduleService: OpenCloseScheduleService
    , private sysCatService: SysCatService
    , private app: AppComponent
    , private modalService: NgbModal) {
    super(actr, RESOURCE.WORK_SCHEDULE, ACTION_FORM.SEARCH);
    this.setMainService(openCloseScheduleService);
    this.listAttendanceConfig = APP_CONSTANTS.ATTENDANCE_CONFIGURATION;
    this.formSearch = this.buildForm({}, this.formConfig, ACTION_FORM.SEARCH
      , [ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.detailReport.toDate')]);
    this.processSearch();
  }

  ngOnInit() {
  }


  get f() {
    return this.formSearch.controls;
  }

  processDelete(item) {
    if (item && item.id > 0) {
      this.app.confirmDelete(null, () => {// on accepted
        this.openCloseScheduleService.deleteById(item.id)
          .subscribe(res => {
            if (this.openCloseScheduleService.requestIsSuccess(res)) {
              this.processSearch(null);
            }
          });
      }, () => {// on rejected

      });
    }
  }

  prepareSaveOrUpdate(item) {
    if (item && item.id > 0) {
      this.openCloseScheduleService.findOne(item.id)
        .subscribe(res => {
          //prompt("please press ctrl+c to copy the text below", JSON.stringify(res))
          const modalRef = this.router.navigate(['/timesheet/open-close/edit', item.id]);

        });
    }
  }

  printSelectedDate(formArray: FormArray) {
    const list = [];
    if (formArray && formArray.controls) {
      for (const control of formArray.controls) {
        list.push(moment(control.value).format('DD/MM/YYYY'));
      }
    }
    return list.join(', ');
  }

  processApplyEmployee(data) {
    const modalRef = this.modalService.open(OpenCloseScheduleEmployeeComponent, MEDIUM_MODAL_OPTIONS);
    if (data) {
      modalRef.componentInstance.setFormValue(data);
    }
    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
      if (this.openCloseScheduleService.requestIsSuccess(result)) {
        this.processSearch();
      }
    });
  }

  processCheckApply(item) {
    this.openCloseScheduleService.processCheckApply(item.workScheduleId).subscribe(
      res => {
        if (this.openCloseScheduleService.requestIsSuccess(res)) {
          if (res.data && res.data.length > 0) {
            const modalRef = this.modalService.open(OpenCloseScheduleApplyComponent, DEFAULT_MODAL_OPTIONS);
            modalRef.componentInstance.setFormValue(item.workScheduleId, res);
            modalRef.result.then((result) => {
              if (!result) {
                return;
              }
              if (this.openCloseScheduleService.requestIsSuccess(result)) {
                this.processSearch();
              }
            });
          } else {
            setTimeout(() => {
              this.app.confirmMessage('workSchedule.apply.defaultMessageComfirm', () => {// on accepted
                this.openCloseScheduleService.processApply(item.workScheduleId, {applyData: null}).subscribe(
                  resApply => {
                    if (this.openCloseScheduleService.requestIsSuccess(resApply)) {
                      this.processSearch();
                    }
                  }
                );
              }, () => {// on rejected

              });
            }, 500);
          }
        } else if (this.openCloseScheduleService.requestIsConfirm(res) && res.message === 'empWorkScheduleExist') {
          this.app.confirmMessage('workSchedule.apply.empWorkScheduleExist',
            () => {// on accepted
              this.openCloseScheduleService.processApply(item.workScheduleId, {applyData: null}).subscribe(
                resApply => {
                  if (this.openCloseScheduleService.requestIsSuccess(resApply)) {
                    this.processSearch();
                  }
                }
              );
            }, () => {// on rejected
            });
        }
      }
    );
  }

  processImportOpenCloseMarket() {
    const modalRef = this.modalService.open(ImportOpenCloseComponent, DEFAULT_MODAL_OPTIONS);
    modalRef.componentInstance.setFormValue(this.f.id.value);
    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
      this.processSearch(null);
    });
  }
}
