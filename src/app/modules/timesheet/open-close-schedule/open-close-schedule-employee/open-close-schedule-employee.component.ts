import { Component, OnInit } from '@angular/core';
import { FormArray, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AppComponent } from '@app/app.component';
import { ACTION_FORM, RESOURCE } from '@app/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { CommonUtils, ValidationService } from '@app/shared/services';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {WorkScheduleService} from "@app/core/services/hr-timesheet/work-schedule.service";

@Component({
  selector: 'work-schedule-employee',
  templateUrl: './open-close-schedule-employee.component.html'
})
export class OpenCloseScheduleEmployeeComponent extends BaseComponent implements OnInit {
  formEmpRelation: FormArray;
  workScheduleId;
  conditionByOrganization = ' ';
  formConfigEmployee = {
    employeeId: [null, [ValidationService.required]]
  };
  constructor(public activeModal: NgbActiveModal
            , public actr: ActivatedRoute
            , private app: AppComponent
            , private workScheduleService: WorkScheduleService ) {
    super(actr, RESOURCE.WORK_SCHEDULE, ACTION_FORM.SEARCH);

    this.setMainService(workScheduleService);
    this.buildFormEmpRelation();

   }

  ngOnInit() {}
  private buildFormEmpRelation() {
    this.formEmpRelation = new FormArray([]);
    this.formEmpRelation.push(this.makeDefaultRelationForm());
    this.formEmpRelation.setValidators(ValidationService.duplicateArray(
      ['employeeId'], 'employeeId', 'app.employee.employee'));
  }

  /**
   * processSaveOrUpdate
   */
  processApplyEmp() {

    if (!this.validateBeforeSave()) {
      return;
    } else {
      const formSave = {};
      formSave['lstEmp'] = this.formEmpRelation.value;
      this.app.confirmMessage(null, () => {// on accepted
        this.workScheduleService.processApplyEmp(this.workScheduleId, formSave)
          .subscribe(res => {
            if (this.workScheduleService.requestIsSuccess(res)) {
              this.activeModal.close(res);
            }
          });
      }, () => {// on rejected
      });
    }
  }

  private validateBeforeSave(): boolean {
    return CommonUtils.isValidForm(this.formEmpRelation);
  }
    /**
   * makeDefaultRelationForm
   */
  public makeDefaultRelationForm(): FormGroup {
    const formGroup = this.buildForm({}, this.formConfigEmployee);
    return formGroup;
  }

   /**
   * addEmpRelation
   * param index
   * param item
   */
  addEmpRelation(index: number) {
    const controls = this.formEmpRelation as FormArray;
    controls.insert(index + 1, this.makeDefaultRelationForm());
  }

    /**
   * removeEmpRelation
   * param index
   * param item
   */
  removeEmpRelation(index: number) {
    const controls = this.formEmpRelation as FormArray;
    if (controls.length === 1) {
      this.buildFormEmpRelation();
    }
    controls.removeAt(index);
  }

   /**
   * setFormValue
   * param data
   */
  public setFormValue(data) {
     if (data && data.organizationId > 0) {
       this.workScheduleId = data.workScheduleId;
    // tslint:disable-next-line: max-line-length
       this.conditionByOrganization =  ' AND  obj.organization_id IN ( SELECT org.organization_id FROM organization org WHERE org.path LIKE \'' + '%' + data.organizationId + '%' + '\')'
        + ' AND ( obj.status <> 2 OR obj.`status` IS NULL )';
     }
  }
}
