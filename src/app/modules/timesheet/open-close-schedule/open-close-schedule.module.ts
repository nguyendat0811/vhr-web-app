import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '@app/shared';
import {OpenCloseScheduleRoutingModule} from './open-close-schedule-routing.module';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {SaturdayCalendarComponent} from './saturday-calendar/saturday-calendar.component';
import {OpenCloseScheduleIndexComponent} from "@app/modules/timesheet/open-close-schedule/open-close-schedule-index/open-close-schedule-index.component";
import {OpenCloseScheduleApplyComponent} from "@app/modules/timesheet/open-close-schedule/open-close-schedule-apply/open-close-schedule-apply.component";
import {OpenCloseScheduleFormComponent} from "@app/modules/timesheet/open-close-schedule/open-close-schedule-form/open-close-schedule-form.component";
import {OpenCloseScheduleComponent} from "@app/modules/timesheet/open-close-schedule/open-close-schedule-defintion/open-close-schedule.component";
import {OpenCloseScheduleEmployeeComponent} from "@app/modules/timesheet/open-close-schedule/open-close-schedule-employee/open-close-schedule-employee.component";
import {OpenCloseScheduleSearchComponent} from "@app/modules/timesheet/open-close-schedule/open-close-schedule-search/open-close-schedule-search.component";
import {ImportOpenCloseComponent} from "@app/modules/timesheet/open-close-schedule/import-open-close-time/import-open-close.component";

@NgModule({
// tslint:disable-next-line: max-line-length
  declarations: [OpenCloseScheduleIndexComponent, OpenCloseScheduleComponent, OpenCloseScheduleSearchComponent, OpenCloseScheduleFormComponent,
    OpenCloseScheduleApplyComponent, SaturdayCalendarComponent, OpenCloseScheduleEmployeeComponent, ImportOpenCloseComponent],
  imports: [
    CommonModule,
    SharedModule,
    OpenCloseScheduleRoutingModule,
  ],
  entryComponents: [OpenCloseScheduleSearchComponent, OpenCloseScheduleApplyComponent, SaturdayCalendarComponent, OpenCloseScheduleEmployeeComponent, ImportOpenCloseComponent],
  providers: [NgbActiveModal]
})
export class OpenCloseScheduleModule {
}
