import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PropertyResolver } from '@app/shared/services/property.resolver';
import { RESOURCE } from '@app/core';
import { CommonUtils } from '@app/shared/services';
import {OpenCloseScheduleIndexComponent} from "@app/modules/timesheet/open-close-schedule/open-close-schedule-index/open-close-schedule-index.component";
import {OpenCloseScheduleFormComponent} from "@app/modules/timesheet/open-close-schedule/open-close-schedule-form/open-close-schedule-form.component";

const routes: Routes = [
  {
    path: '',
    component: OpenCloseScheduleIndexComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.WORK_SCHEDULE,
      nationId: CommonUtils.getNationId()
    }
  },
  {
    path: 'add',
    component: OpenCloseScheduleFormComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.WORK_SCHEDULE,
      nationId: CommonUtils.getNationId()
    }
  },
  {
    path: 'edit/:id',
    component: OpenCloseScheduleFormComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.WORK_SCHEDULE,
      nationId: CommonUtils.getNationId()
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OpenCloseScheduleRoutingModule { }
