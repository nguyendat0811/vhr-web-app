import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from "@app/shared";
import {TimeSheetRoutingModule} from "@app/modules/timesheet/timesheet-routing.module";

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SharedModule,
    TimeSheetRoutingModule
  ]
})
export class TimeSheetModule {
}
