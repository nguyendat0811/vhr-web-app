import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrganizationHistoryComponent } from './organization-history.component';
import { OrganizationHistorySearchComponent } from './organization-history-search/organization-history-search.component';
import { PropertyResolver } from '@app/shared/services/property.resolver';
import { RESOURCE } from '@app/core';
import { CommonUtils } from '@app/shared/services';

const routes: Routes = [
  {
    path: '',
    component: OrganizationHistoryComponent,
    children: [
      {
        path: 'search',
        component: OrganizationHistorySearchComponent,
        resolve: {
          props: PropertyResolver
        },
        data: {
          resource: RESOURCE.ORGANIZATION_PLAN,
          nationId: CommonUtils.getNationId()
        }
      }
      , {
        path: 'search/:orgId',
        component: OrganizationHistorySearchComponent,
        resolve: {
          props: PropertyResolver
        },
        data: {
          resource: RESOURCE.ORGANIZATION,
          nationId: CommonUtils.getNationId()
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrganizationHistoryRoutingModule { }
