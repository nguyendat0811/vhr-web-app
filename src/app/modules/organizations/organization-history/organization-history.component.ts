import { Component, OnInit, ViewChild } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';
import { TranslationService } from 'angular-l10n';
import { AppComponent } from '@app/app.component';
import { OrganizationService, RESOURCE, ACTION_FORM } from '@app/core';
import { HelperService } from '@app/shared/services/helper.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { OrgTreeComponent } from '@app/shared/components/org-tree/org-tree.component';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';

@Component({
  selector: 'organization-history',
  templateUrl: './organization-history.component.html',
})
export class OrganizationHistoryComponent extends BaseComponent implements OnInit {
  resultList: any = {};
  currentNode: any;
  @ViewChild('orgTreeHistory')
  orgTreeHistory: OrgTreeComponent;
  constructor(
      private router: Router
    , private translation: TranslationService
    , private app: AppComponent
    , private organizationService: OrganizationService
    , private helperService: HelperService
    , private modalService: NgbModal
    , public act: ActivatedRoute) { 
      super(act, RESOURCE.ORGANIZATION, ACTION_FORM.SEARCH);
    }

  ngOnInit() {
    this.helperService.ORGANIZATION.subscribe(res => {
      if (!res.type) {
        if (this.currentNode) {
          this.currentNode.label = res.name;
          this.currentNode.nodeId = res.organizationId;
          this.currentNode.data = res.organizationId;
          if (this.currentNode.parent.nodeId !== res.orgParentId) {
            this.orgTreeHistory.ngOnInit();
          }
        }
      } else if (res.type === 'RELOAD_TREE') {
        this.orgTreeHistory.ngOnInit();
      }
    });
  }
  /**
   * treeSelectNode
   * param event
   */
  public treeSelectNode(event) {
    if ( event.node.nodeId > 0 ) {
        this.router.navigate(['/organizations/history/search', event.node.nodeId]);
    } else {
      this.editNode(event.node);
    }
  }
  /**
   * onEditNode
   * @ param currentNode
   */
  private editNode(currentNode) {
    this.currentNode = currentNode;
    const params = {
      name: currentNode.label,
      orgParentId: currentNode.parentId,
    };
    if (currentNode.nodeId > 0) {
      this.router.navigate(['/organizations/manage/edit', currentNode.nodeId]);
    } else {
      this.router.navigate(['/organizations/manage/edit', currentNode.nodeId], { queryParams: params });
    }
  }
}
