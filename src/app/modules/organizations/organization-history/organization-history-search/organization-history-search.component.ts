import { Component, OnInit, ViewChild } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { CommonUtils, ValidationService } from '@app/shared/services';
import { Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { OrganizationService, RESOURCE, ACTION_FORM } from '@app/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from '@app/app.component';
import { HelperService } from '@app/shared/services/helper.service';
import { OrgHistoryService } from '@app/core/services/hr-organization/organization/org-history.service';

@Component({
  selector: 'organization-history-search',
  templateUrl: './organization-history-search.component.html',
})
export class OrganizationHistorySearchComponent extends BaseComponent implements OnInit {
  resultList: any = {};
  organization_id: number;
  @ViewChild('pTable') dataTable: any;
  formConfig = {
    changeType: [null , [ValidationService.maxLength(1000)]],
    changedFields: [null , [ValidationService.maxLength(50)]],
    changeTimeFrom: [null],
    changeTimeTo: [null],
    createdBy: [null, [ValidationService.maxLength(50)]],
    path: ['']
  };
  constructor(
    public actr: ActivatedRoute,
    private router: Router,
    private orgHistoryService: OrgHistoryService,
    private activatedRoute: ActivatedRoute,
    private modalService: NgbModal,
    private app: AppComponent,
    private helperService: HelperService
    ) {
    super(actr, RESOURCE.ORGANIZATION, ACTION_FORM.SEARCH);
    this.formSearch = this.buildForm({ orgHistoryId: null }, this.formConfig, ACTION_FORM.SEARCH);
    this.activatedRoute.params.subscribe(params => {
      if (params && params.orgId) {
        this.formSearch.controls['path'].setValue(`${params.orgId}`);
      }
        this.processSearch();
    });

    this.helperService.ORGANIZATION.subscribe(res => {
      if (res.type && res.type === 'RELOAD_GRID') {
        this.processSearch();
      }
    });
  }
  ngOnInit() {
    this.processSearch();
  }
  get f () {
    return this.formSearch.controls;
  }

  // Tìm kiếm organization
  processSearch(event?) {
    if (CommonUtils.isValidForm(this.formSearch)) {
        this.orgHistoryService.search(this.formSearch.value, event).subscribe(res => {
          this.resultList = res;
          console.log(res);
        });
      }
  }
}
