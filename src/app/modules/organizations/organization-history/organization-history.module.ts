import { NgModule } from '@angular/core';
import { OrganizationHistoryRoutingModule } from './organization-history-routing.module';
import { OrganizationHistoryComponent } from './organization-history.component';
import { CommonModule } from '@angular/common';
import { OrganizationHistorySearchComponent } from './organization-history-search/organization-history-search.component';
import { SharedModule } from '@app/shared';



@NgModule({
  declarations: [
    OrganizationHistoryComponent
    ,OrganizationHistorySearchComponent
  ],
  imports: [
    OrganizationHistoryRoutingModule
    ,CommonModule
    ,SharedModule
  ],
  entryComponents: [
    
  ]
})
export class OrganizationHistoryModule { }
