import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/dashboard/organization',
    pathMatch: 'full'
  }, {
    path: 'manage',
    loadChildren: './organization-manage/organization-manage.module#OrganizationManageModule'
  }, {
    path: 'plan',
    loadChildren: './organization-plan/organization-plan.module#OrganizationPlanModule'
  }, {
    path: 'history',
    loadChildren: './organization-history/organization-history.module#OrganizationHistoryModule'
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrganizationsRoutingModule { }
