import { NgModule } from '@angular/core';
import { SharedModule } from '@app/shared';
import { CommonModule } from '@angular/common';
import { OrganizationsRoutingModule } from './organizations-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { TreeDragDropService } from 'primeng/api';
import { OrganizationResolver } from '@app/shared/services/organization.resolver';
@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    SharedModule,
    OrganizationsRoutingModule,
    ReactiveFormsModule,
  ],
  entryComponents: [],
  providers: [TreeDragDropService
              ,OrganizationResolver]
})
export class OrganizationsModule { }
