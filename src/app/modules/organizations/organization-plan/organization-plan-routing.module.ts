import { OrganizationPlanSearchComponent } from './search/organization-plan-search.component';
import { NgModule } from '@angular/core';
import { RESOURCE } from '@app/core/app-config';
import { CommonUtils } from '@app/shared/services';
import { Routes, RouterModule } from '@angular/router';
import { OrganizationPlanComponent } from './organization-plan.component';
import { OrganizationPlanAddComponent } from './add/organization-plan-add.component';
import { PropertyResolver } from '@app/shared/services/property.resolver';

const routes: Routes = [
  {
    path: '',
    component: OrganizationPlanComponent,
    children: [
      {
        path: 'search',
        component: OrganizationPlanSearchComponent,
        resolve: {
          props: PropertyResolver
        },
        data: {
          resource: RESOURCE.ORGANIZATION_PLAN,
          nationId: CommonUtils.getNationId()
        }
      }, {
        path: 'add',
        component: OrganizationPlanAddComponent,
        resolve: {
          props: PropertyResolver
        },
        data: {
          resource: RESOURCE.ORGANIZATION_PLAN,
          nationId: CommonUtils.getNationId()
        }
      }, {
        path: 'edit/:orgPlanId',
        component: OrganizationPlanAddComponent,
        resolve: {
          props: PropertyResolver
        },
        data: {
          resource: RESOURCE.ORGANIZATION_PLAN,
          nationId: CommonUtils.getNationId()
        }
      }
    ],
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.ORGANIZATION_PLAN,
      nationId: CommonUtils.getNationId()
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrganizationPlanRoutingModule { }
