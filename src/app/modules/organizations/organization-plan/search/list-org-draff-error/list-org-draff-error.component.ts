import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'list-org-draff-error',
  templateUrl: './list-org-draff-error.component.html'
})
export class ListOrgDraffErrorComponent implements OnInit {

  data: any;
  type: any;
  constructor(
    public activeModal: NgbActiveModal
  ) {
  }

  ngOnInit() {
  }
}
