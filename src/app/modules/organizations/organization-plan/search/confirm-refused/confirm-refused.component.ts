import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, Validators } from '@angular/forms';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { OrgPlanService, RESOURCE, ACTION_FORM } from '@app/core';
import { AppComponent } from '@app/app.component';
import { CommonUtils, ValidationService } from '@app/shared/services';

@Component({
  selector: 'confirm-refused',
  templateUrl: './confirm-refused.component.html'
})
export class ConfirmRefusedComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  orgPlanId: any;

  formConfig = {
    orgPlanId: [null],
    reason: ['', [ ValidationService.required, ValidationService.maxLength(1000) ]]
  };

  constructor(
    public actr: ActivatedRoute,
    private orgPlanService: OrgPlanService,
    private app: AppComponent,
    public activeModal: NgbActiveModal
  ) {
    super(actr, RESOURCE.ORGANIZATION_PLAN, ACTION_FORM.ORG_PLAN_INSERT);
    this.formSave = this.buildForm({}, this.formConfig);
  }

  ngOnInit() {
  }

  get f() {
    return this.formSave.controls;
  }

  processSaveOrUpdate() {
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    }
    this.app.confirmMessage('common.message.confirm.refused', () => { // on accepted
      this.orgPlanService.unapproveSign(this.formSave.value.orgPlanId, this.formSave.value).subscribe(res => {
        if (this.orgPlanService.requestIsSuccess(res)) {
          this.activeModal.close(res);
        }
      });
    }, null);
  }

  initForm() {
    this.formSave = this.buildForm({orgPlanId: this.orgPlanId}, this.formConfig);
  }
}
