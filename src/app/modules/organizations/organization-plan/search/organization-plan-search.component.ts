import { ViewDetailComponent } from './../view-detail/view-detail.component';
import { APP_CONSTANTS, DEFAULT_MODAL_OPTIONS, RESOURCE, ACTION_FORM } from '@app/core/app-config';
import { OrgPlanService } from '@app/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit } from '@angular/core';
import { AppComponent } from '@app/app.component';
import { OrganizationPlanFormComponent } from '../add/org-plan-form/org-plan-form.component';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ListOrgDraffErrorComponent } from './list-org-draff-error/list-org-draff-error.component';
import { ConfirmRefusedComponent } from './confirm-refused/confirm-refused.component';
import { ValidationService } from '@app/shared/services';

@Component({
  selector: 'organization-plan-search',
  templateUrl: './organization-plan-search.component.html',
})
export class OrganizationPlanSearchComponent extends BaseComponent implements OnInit {
  resultList: any = {};
  listPlanType = APP_CONSTANTS.PLAN_TYPE;
  listPlanStatus = APP_CONSTANTS.PLAN_STATUS;

  constructor(
              public actr: ActivatedRoute,
              private orgPlanService: OrgPlanService,
              private modalService: NgbModal,
              private app: AppComponent) {
    super(actr, RESOURCE.ORGANIZATION_PLAN, ACTION_FORM.ORG_PLAN_SEARCH);
    this.setMainService(orgPlanService);
    this.buildFormSearch();
  }

  ngOnInit() {
    this.processSearch();
  }

  get f () {
    return this.formSearch.controls;
  }

  private buildFormSearch(): void {
    this.formSearch = this.buildForm({}, {
      orgParentId: [''],
      planName: ['', [ ValidationService.maxLength(200)]],
      type: [''],
      status: ['']
    }, ACTION_FORM.ORG_PLAN_SEARCH);
  }

  prepareInsertOrUpdate(item) {
    const modalRef = this.modalService.open(OrganizationPlanFormComponent, DEFAULT_MODAL_OPTIONS);
    if (item && item.orgPlanId > 0) {
      this.orgPlanService.findOne(item.orgPlanId)
        .subscribe( res => {
          modalRef.componentInstance.setFormValue(this.propertyConfigs, res.data);
        });
    } else {
      modalRef.componentInstance.setFormValue(this.propertyConfigs, {});
    }

    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
      if (this.orgPlanService.requestIsSuccess(result)) {
        this.processSearch(null);
      }
    });
  }

  signPlan(item) {
    if (item && item.orgPlanId > 0) {
      this.app.confirmMessage('common.message.confirm.sign', () => { // on accepted
        this.orgPlanService.signPlan(item.orgPlanId)
        .subscribe(res => {
          if (this.orgPlanService.requestIsSuccess(res)) {
            this.processSearch(null);
          } else if (this.orgPlanService.requestIsConfirm(res)) {
            const showOrgDraffNotEnoughSign = () => {
              const modalRef = this.modalService.open(ListOrgDraffErrorComponent, DEFAULT_MODAL_OPTIONS);
              modalRef.componentInstance.data = res.data;
              modalRef.componentInstance.type = 'SIGN';
            };
            if (res.message) {
              eval(res.message);
            }
          }
        });
      }, null);
    }
  }

  cancelSign(item) {
    if (item && item.orgPlanId > 0) {
      this.app.confirmMessage('common.message.confirm.cancelSign', () => { // on accepted
        this.orgPlanService.cancelSign(item.orgPlanId)
        .subscribe(res => {
          if (this.orgPlanService.requestIsSuccess(res)) {
            this.processSearch(null);
          }
        });
      }, null);
    }
  }

  processDeletePlan(item) {
    if (item && item.orgPlanId > 0) {
      this.app.confirmDelete(null, () => {
        // on accepted
        this.orgPlanService.deleteById(item.orgPlanId)
        .subscribe(
          res => {
            if (this.orgPlanService.requestIsSuccess(res)) {
              this.processSearch(null);
            }
          },
          error => {
            this.app.requestIsError();
          }, null);
      }, null);
    }
  }

  processViewDetail(item) {
    const modalRef = this.modalService.open(ViewDetailComponent, {
      size: 'lg',
      keyboard : false,
      backdrop: true,
    });

    modalRef.componentInstance.orgParentId = item.orgParentId;
    modalRef.componentInstance.orgPlanId = item.orgPlanId;

    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
    });
  }

  actionApprove(item) {
    if (item && item.orgPlanId > 0) {
      this.app.confirmMessage('common.message.confirm.approve', () => { // on accepted
        this.orgPlanService.approveSign(item.orgPlanId).subscribe(res => {
          if (this.orgPlanService.requestIsSuccess(res)) {
            this.processSearch(null);
          } else if (this.orgPlanService.requestIsConfirm(res)) {
            const showOrgDraffNotEnoughSign = () => {
              const modalRef = this.modalService.open(ListOrgDraffErrorComponent, DEFAULT_MODAL_OPTIONS);
              modalRef.componentInstance.data = res.data;
              modalRef.componentInstance.type = 'APPROVE';
            };
            if (res.message) {
              eval(res.message);
            }
          }
        });
      }, null);
    }
  }

  actionUnApprove(item) {
    if (item && item.orgPlanId > 0) {
      const modalRef = this.modalService.open(ConfirmRefusedComponent, DEFAULT_MODAL_OPTIONS);
      modalRef.componentInstance.orgPlanId = item.orgPlanId;
      modalRef.componentInstance.initForm();
      modalRef.result.then((result) => {
        if (!result) {
          return;
        }
        if (this.orgPlanService.requestIsSuccess(result)) {
          this.processSearch(null);
        }
      });
    }
  }
}
