import { BaseComponent } from './../../../../../shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { FormGroup, FormArray, Validators, FormControl } from '@angular/forms';
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ValidationService } from '@app/shared/services';
import { OrgBoundaryDraffService, RESOURCE, ACTION_FORM, OrgBoundaryService } from '@app/core';
import { DataPickerModalComponent } from '@app/shared/components/data-picker/data-picker-modal/data-picker-modal.component';
import { AppComponent } from '@app/app.component';
import { ListEmployeeModalComponent } from './list-employee-modal.component';

@Component({
  selector: 'boundary',
  templateUrl: './boundary.component.html',
})
export class BoundaryComponent extends BaseComponent implements OnInit, OnDestroy {
  // @Input()
  // isView: boolean;
  _nodeSelected: any;
  _orgPlan: any;
  fieldReadOnly = false;
  showSaveButton = true;
  isPlanTypeChangeName = false;

  formOrgBoundaryDraff: FormArray;
  listOrgBoundary = [];
  private navigationSubscription;
  // private _orgDraffId: number;
  // private _orgPlanId: number;
  constructor(
    private activedRoute: ActivatedRoute,
    private modalService: NgbModal,
    private orgBoundaryDraffService: OrgBoundaryDraffService,
    private orgBoundaryService: OrgBoundaryService,
    private app: AppComponent
  ) {
    super(activedRoute, RESOURCE.ORGANIZATION_PLAN, ACTION_FORM.ORG_DRAFF_BOUNDARY_UPDATE);
    this.buildFormOrgBoundaryDraff({});

  }
  // get set tham so truyen vao component
  get orgPlan(): any {
    return this._orgPlan;
  }

  @Input()
  set orgPlan(orgPlan: any) {
    this._orgPlan = orgPlan;
    if (this.orgPlan && this.orgPlan.type === 1 ) {
      this.isPlanTypeChangeName = true;
    }
  }
  get nodeSelected(): any {
    return this._nodeSelected;
  }

  @Input()
  set nodeSelected(nodeSelected: any) {
    this._nodeSelected = nodeSelected;
    this.prepareSaveOrUpdate(nodeSelected);
  }


  ngOnInit() {
  }

  public prepareSaveOrUpdate(e) {
    if (!e) {
      return;
    }
    if (e.isRoot) {
      // Load all list org boundary
      if (!this.isPlanTypeChangeName) {
        this.orgBoundaryDraffService.findAllOrgBoundaryByOrg(this.orgPlan.orgPlanId, null)
        .subscribe(res => {
          this.buildFormOrgBoundaryDraff(res.data);
          this.actionToggleAllowEdit(e);
        });
      }
      // Load list org boundary du lieu that
      this.orgBoundaryService.findOrgBoundaryByOrg(e.data)
      .subscribe(res => {
        this.listOrgBoundary = res.data;
      });
    } else {
      if (!e.nodeId) {
        this.buildFormOrgBoundaryDraff([]);
        this.listOrgBoundary = [];
        return;
      }
      // Load all list org boundary
      if (!this.isPlanTypeChangeName) {
        this.orgBoundaryDraffService.findAllOrgBoundaryByOrg(this.orgPlan.orgPlanId, e.nodeId)
        .subscribe(res => {
          this.buildFormOrgBoundaryDraff(res.data);
          this.actionToggleAllowEdit(e);
        });
        // Load list org boundary du lieu nhap
        this.orgBoundaryDraffService.findOrgBoundaryByOrg(e.nodeId)
        .subscribe(res => {
          this.listOrgBoundary = res.data;
        });
      } else {
        // Load list org boundary du lieu that
        this.orgBoundaryService.findOrgBoundaryByOrg(e.data)
        .subscribe(res => {
          this.listOrgBoundary = res.data;
        });
      }
    }
  }

  public buildFormOrgBoundaryDraff(listOrg: any) {
    if (!listOrg) {
      this.formOrgBoundaryDraff = new FormArray([]);
    } else {
      const controls = new FormArray([]);
      for (const i in listOrg) {
        const org = listOrg[i];
        const group = this.makeOrgForm(org);
        controls.push(group);
      }
      this.formOrgBoundaryDraff = controls;
    }
  }

  public makeOrgForm(formData: any): FormGroup {
    const formGroupOrg = CommonUtils.createForm(formData, {
      orgDraffId: [null],
      organizationName: [null],
      isUpdate: [null],
      isAllowAddEmloyee: [null],
      lstOrgBoundaryDraff: [new FormArray([])],
      listOrgBoundaryDraffDelete: [[]]
    });
    if (formData.lstOrgBoundaryDraff && formData.lstOrgBoundaryDraff.length > 0) {
      const controls = new FormArray([]);
      for (const i in formData.lstOrgBoundaryDraff) {
        const orgBoundary = formData.lstOrgBoundaryDraff[i];
        const group = this.makeOrgBoundaryForm(orgBoundary);
        controls.push(group);
      }
      controls.setValidators(ValidationService.duplicateArray(
        ['orgDraffId', 'positionId']
        , 'positionId', 'app.duplicate.orgAndPosition'));

      formGroupOrg.setControl('lstOrgBoundaryDraff', controls);
    }
    return formGroupOrg;
  }

  public makeOrgBoundaryForm(formData: any): FormGroup {
    const formGroup = this.buildForm(formData, {
      orgBoundaryDraffId: [null],
      orgDraffId: [null],
      positionId: [null, [ValidationService.isRequiredIfHaveOne('boundaryNumber'), ValidationService.isRequiredIfHaveOne('listEmployee')]],
      boundaryNumber: [null,
        [ValidationService.positiveInteger, ValidationService.maxLength(10), Validators.max(999999999),
          ValidationService.ifHaveOneIsRequired('positionId')]],
      listEmployee: [[], [ValidationService.ifHaveOneIsRequired('positionId')]]
    }, ACTION_FORM.ORG_DRAFF_BOUNDARY_UPDATE);
    return formGroup;
  }

  /**
   * addOrgBoundary
   * param index
   * param item
   */
  public addOrgBoundary(index: number, item: FormGroup) {
    const controls = item.get('lstOrgBoundaryDraff') as FormArray;
    controls.insert(index + 1, this.makeOrgBoundaryForm(item.value));
  }

  /**
   * removeOrgBoundary
   * param index
   * param item
   */
  public removeOrgBoundary(index: number, item: FormGroup) {
    const controls = item.get('lstOrgBoundaryDraff') as FormArray;
    const arrayDelete = item.get('listOrgBoundaryDraffDelete').value;
    const itemOrgBoundary = controls.controls[index] as FormGroup;
    if (itemOrgBoundary.get('orgBoundaryDraffId').value) {
      arrayDelete.push(itemOrgBoundary.get('orgBoundaryDraffId').value);
      item.get('listOrgBoundaryDraffDelete').setValue(arrayDelete);
    }
    if (controls.length === 1) {
      controls.removeAt(index);
      controls.push(this.makeOrgBoundaryForm({ orgDraffId: itemOrgBoundary.get('orgDraffId').value }));
      return;
    }
    controls.removeAt(index);
  }

  public validate() {
    CommonUtils.isValidForm(this.formOrgBoundaryDraff);
    this.validateTotalBoundary();
  }
  /**
   * check sum boundary = sum sub by nation
   */
  public validateTotalBoundary(): boolean {
    let isError = false;
    for (const group of this.formOrgBoundaryDraff.controls) {
      const boundaryNumberControl = group.get('boundaryNumber') as FormControl;
      const listDetailArray = group.get('listDetailDraff') as FormArray;
      if (listDetailArray === null || listDetailArray.length === 0) {
        continue;
      }
      if (! CommonUtils.isRealNumber(boundaryNumberControl.value)) {
        continue;
      }
      const sum = this.getSumByNation(listDetailArray);
      if (! CommonUtils.isRealNumber(sum)) {
        continue;
      }
      if (CommonUtils.compareNumber(sum, boundaryNumberControl.value) !== 0) {
        boundaryNumberControl.setErrors({boundaryNumberNotMatch: {}});
        boundaryNumberControl.markAsTouched();
        isError = true;
      }
    }

    return isError;
  }

  public getSumByNation(listDetailArray): number {
    return 0;
  }

  public onChose(objectForm) {
    const modalRef = this.modalService.open(DataPickerModalComponent, {
      backdrop: 'static',
    });
    modalRef.componentInstance.setInitValue({
          operationKey: 'action.view'
        , adResourceKey: 'resource.employee'
        , objectBO: 'EmployeeBO'
        , codeField: 'email'
        , nameField: 'fullName'
        , selectField: 'employeeId'
        , filterCondition: ' and status = 1 '
    });
    modalRef.result.then((item) => {
      if (!item) {
        return;
      }

      // add id nhan vien vao form
      const arrayEmployee = objectForm.get('listEmployee').value;
      if (this.containsObjectInList(item, arrayEmployee)) {
        this.orgBoundaryDraffService.processReturnMessage({type: 'WARNING', code: 'boundary.employeeHasAssign'});
        return;
      }
      arrayEmployee.push(item);
      objectForm.get('listEmployee').setValue(arrayEmployee);
    });
  }

  public showListEmployee(objectForm) {
    const modalRef = this.modalService.open(ListEmployeeModalComponent, {
      backdrop: 'static',
    });
    modalRef.componentInstance.setListEmployee(objectForm.get('listEmployee').value);
    objectForm.get('listEmployee').setValue(modalRef.componentInstance.resultList);

    modalRef.result.then((item) => {
      if (!item) {
        return;
      }
      objectForm.get('listEmployee').updateValueAndValidity();
    });
  }

  private validateBeforeSave(): boolean {
    return CommonUtils.isValidForm(this.formOrgBoundaryDraff);
  }

  public actionSave() {
    if (!this.validateBeforeSave()) {
      return;
    }
    this.app.confirmMessage(null, () => {// on accepted
      this.orgBoundaryDraffService.saveOrUpdate(this.formOrgBoundaryDraff.value).subscribe(
        result => {
          if (this.orgBoundaryDraffService.requestIsSuccess(result)) {
            this.prepareSaveOrUpdate(this.nodeSelected);
          }
        }
      );
    }, null);
  }
  public containsObjectInList(obj, list): boolean {
    let i;
    for (i = 0; i < list.length; i++) {
      if (list[i].selectField === obj.selectField) {
        return true;
      }
    }
    return false;
  }
  public createStringListEmailEmployee(objectForm): string {
    const arrayEmployee = objectForm.get('listEmployee').value;
    let arrayEmployeeEmail = '';
    for (let i = 0; i < arrayEmployee.length; i++) {
      if (i + 1 === arrayEmployee.length) {
        arrayEmployeeEmail += arrayEmployee[i].codeField;
      } else {
        arrayEmployeeEmail += arrayEmployee[i].codeField + ', ';
      }
    }
    return arrayEmployeeEmail;
  }
  ngOnDestroy() {
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }
  }

  private actionToggleAllowEdit(e?: any) {
    if (!e) {
      this.showSaveButton = false;
      this.fieldReadOnly = true;
      return;
    }
    if (!e.nodeId) {
      this.showSaveButton = false;
      this.fieldReadOnly = true;
      return;
    }
    setTimeout(() => {
      // TH thêm mới và từ chối phê duyệt thì đc phép sửa
      if (this.orgPlan.status === 0 || this.orgPlan.status === 3) {
        this.showSaveButton = true;
        this.fieldReadOnly = false;
        if (this.isPlanTypeChangeName) {
          this.showSaveButton = false;
          this.fieldReadOnly = true;
        }
      } else {
        this.showSaveButton = false;
        this.fieldReadOnly = true;
      }
    }, 500);
  }
}
