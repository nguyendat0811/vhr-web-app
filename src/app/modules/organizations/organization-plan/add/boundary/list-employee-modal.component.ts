import { AppComponent } from '@app/app.component';
import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { FormGroup} from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'list-boundary-employee-draff',
  templateUrl: './list-employee-modal.component.html',
})
export class ListEmployeeModalComponent implements OnInit {
  formSearch: FormGroup;
  listEmployee: any;
  listTemp: any;
  filterText: any;

  constructor(
    private app: AppComponent,
    public activeModal: NgbActiveModal) {
      this.filterText = '';
  }

  ngOnInit() {
  }

  get f () {
    return this.formSearch.controls;
  }

  get resultList () {
    return this.listEmployee;
  }

  public setListEmployee(data: any) {
    this.listEmployee = data;
    this.listTemp = data;
  }

  public actionClose() {
    this.activeModal.close({});
  }

  public chose(item: any) {
    const index = this.listEmployee.indexOf(item);
    this.listEmployee.splice(index, 1);
    // Sau khi click xoa mot nhan vien thi thuc hien tim kiem lai
    this.onSearchChange();
  }

  public onSearchChange() {
    if (this.filterText) {
      this.listTemp = this.listEmployee.filter(
        item => item['codeField'].toLowerCase().includes(this.filterText.toLowerCase())
        || item['nameField'].toLowerCase().includes(this.filterText.toLowerCase())
      );
    } else {
      this.listTemp = this.listEmployee;
    }
  }
}
