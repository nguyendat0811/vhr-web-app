import { ImportOrgComponent } from './import-org/import-org.component';
import { DEFAULT_MODAL_OPTIONS, APP_CONSTANTS, RESOURCE, ACTION_FORM } from '@app/core/app-config';
import { Component, OnInit, OnDestroy, ViewChild, AfterViewInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { OrganizationTypeService, OrgPlanService, OrgBoundaryDraffService } from '@app/core';
import { LineOrgService } from '@app/core';
import { OrganizationPlanCloneComponent } from '../clone/organization-plan-clone.component';
import { CommonUtils } from '@app/shared/services';
import { TreeNode } from 'primeng/api';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { OrganizationPlanTreeComponent } from '../tree/organization-plan-tree.component';
import { BoundaryComponent } from './boundary/boundary.component';
import { AppComponent } from '@app/app.component';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { OrgSelectorModalComponent } from '@app/shared/components/org-selector/org-selector-modal/org-selector-modal.component';
import { TranslationService } from 'angular-l10n';
import { AdditionInfoComponent } from './addition-info/addition-info.component';
import { ValidationService } from '../../../../shared/services/validation.service';

@Component({
  selector: 'organization-plan-add',
  templateUrl: './organization-plan-add.component.html',
})
export class OrganizationPlanAddComponent extends BaseComponent implements OnInit, OnDestroy, AfterViewInit {
  curentNode: TreeNode[];
  form: FormGroup;
  // listOrgType: any = {};
  // lineOrgList: any;
  typeList = APP_CONSTANTS.PLAN_TYPE;
  private navigationSubscription;
  @ViewChild('tree')
  tree: OrganizationPlanTreeComponent;

  @ViewChild('additionInfoTab')
  additionInfoTab: AdditionInfoComponent;
  @ViewChild('boundaryTab')
  boundaryTab: BoundaryComponent;
  // isDisabled = true;
  isPlanTypeChangeName = false;
  // orgDraffId: number;

  orgPlanObject: any;
  nodeSelected: any;
  isView: boolean;

  // isShowCommonTab = true;
  // isShowAdditionTab = false;
  // isShowBoundaryTab = false;

  tabIndex = 0;

  checkPermission = true;
  formConfig = {
    orgPlanId: [''],
    orgParentId: ['', ValidationService.required],
    type: ['', ValidationService.required],
    planName: ['', ValidationService.required],
    effectiveDate: ['', ValidationService.required],
    organizationId: [''],
    cloneType: [1, ValidationService.required]
  };
  /**
   * constructor
   */
  constructor(
      private orgTypeService: OrganizationTypeService
    , private lineOrgService: LineOrgService
    , private activatedRoute: ActivatedRoute
    , private modalService: NgbModal
    , private orgPlanService: OrgPlanService
    , private orgBoundaryDraffService: OrgBoundaryDraffService
    , private router: Router
    , private app: AppComponent
    , public translation: TranslationService
    ) {
      super(activatedRoute, RESOURCE.ORGANIZATION_PLAN, ACTION_FORM.ORG_PLAN_VIEW);
      this.navigationSubscription = this.router.events.subscribe((e: any) => {
        // If it is a NavigationEnd event re-initalise the component
        if (e instanceof NavigationEnd) {
          // this.orgDraffId = null;
          this.createForm({});
          const params = this.activatedRoute.snapshot.params;
          if (params && CommonUtils.isValidId(params.orgPlanId)) {
            this.f.orgPlanId.setValue(params.orgPlanId);
          }
          this.prepareInsertOrUpdate();
          // this.orgTypeService.getListOrgType().subscribe(res => {
          //   this.listOrgType = res;
          // });
          // this.lineOrgService.getListLineCareer().subscribe(res => {
          //   this.lineOrgList = res.data;
          // });
        }
      });
  }
  ngOnDestroy() {
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }
  }

  /**
   * ngOnInit
   */
  ngOnInit() {

  }

  /**
   * controls
   */
  get f () {
    return this.form.controls;
  }

  /**
   * build form
   */
  private createForm(formData: any): void {
    this.form = this.buildForm(formData, this.formConfig, ACTION_FORM.ORG_PLAN_VIEW);
  }

  public prepareInsertOrUpdate() {
    if (!CommonUtils.isValidId(this.f.orgPlanId.value)) {
      return;
    }
    this.orgPlanService.findOne(this.f.orgPlanId.value)
      .subscribe(res => {
        if (this.orgPlanService.requestIsSuccess(res)) {
          this.createForm(res.data);
          this.orgPlanObject = res.data;

          // TH thêm mới và từ chối phê duyệt thì đc phép sửa
          if (res.data.status === 0 || res.data.status === 3) {
            this.isView = false;
            if (this.hasPermission('action.update')) {
              this.checkPermission = true;
            } else {
              this.checkPermission = false;
            }
          } else {
            this.checkPermission = true;
            this.isView = true;
            // this.infoTab.showSaveButton = false;
          }

          if (this.f.type.value === 1) {
            this.isPlanTypeChangeName = true;
            // this.infoTab.isPlanTypeChangeName = true;
            // this.infoTab.showSaveButton = false;
            this.tree.isPlanTypeChangeName = true;
          }
          // this.infoTab.formInfo.controls['orgPlanId'].setValue(this.f.orgPlanId.value);
          // this.infoTab.planId = this.f.orgPlanId.value;

          // Xu ly cay don vi
          this.orgPlanService.findTreeOrgDraffById(res.data.orgPlanId)
          .subscribe(nodes => {
            this.tree.setDataNodes(res.data, nodes);
            // xu ly focus mac dinh vao cay goc
            this.tree.selectedNode = this.tree.rootNode;
            // set node selected for child companent
            this.nodeSelected = this.tree.rootNode;
          });

        }
    });
  }

  /**
   * bắt đầu sửa thông tin chung đơn vị
   * param orgDraffId
   */
  public editDraff(event) {
    // set node selected for child companent
    this.nodeSelected = event;
    // this.orgDraffId = event.nodeId === -1 ? null : event.nodeId;
    // // Xu ly active cac tab thong tin
    // if (event.nodeId === -1 && !this.isPlanTypeChangeName) {
    //   // TH Chon don vi root
    //   this.tabIndex = 2;
    //   this.isShowBoundaryTab = true;
    //   this.isShowCommonTab = this.isShowAdditionTab = false;
    //   // Load all list org boundary
    //   this.orgBoundaryDraffService.findAllOrgBoundaryByOrg(this.f.orgPlanId.value, this.orgDraffId)
    //   .subscribe(res => {
    //     // this.boundaryTab.buildFormOrgBoundaryDraff(res.data);
    //   });
    // } else if (event.nodeId === 0 || this.isPlanTypeChangeName) {
    //   // Doi ten hoac don vi moi
    //   this.tabIndex = 0;
    //   this.isShowCommonTab = true;
    //   this.isShowAdditionTab = this.isShowBoundaryTab = false;
    // } else {
    //   this.isShowCommonTab = this.isShowAdditionTab = this.isShowBoundaryTab = true;
    // }
    // // Load thong tin tab Thong tin chung
    // if (this.tabIndex === 0) {
    //   // this.infoTab.prepareSaveOrUpdate(event);
    //   // this.infoTab.isPlanTypeChangeName = this.isPlanTypeChangeName;
    //   // this.infoTab.formInfo.controls['orgPlanId'].setValue(this.f.orgPlanId.value);
    // }
  }

  /**
   * Lưu thông tin cây đơn vị
   */
  public saveTree() {
    const formSave = {};
    formSave['nodes'] = this.convertTreeNodeToValue(this.tree.dataNodes);
    this.app.confirmMessage(null, () => {
      this.orgPlanService.saveTreeOrgDraffById(this.f.orgPlanId.value, formSave)
      .subscribe((res) => {
        if (this.orgPlanService.requestIsSuccess(res)) {
          this.rebuildAfterSaveTree();
        }
      });
    }, null);
  }

  /**
   * clone
   */
  public clone() {
    if (!CommonUtils.isValidForm(this.form)) {
      return;
    }
    if (this.form.controls['orgParentId'].value  > 0) {
      this.form.controls['organizationId'].setValue(this.form.controls['orgParentId'].value);
      const modalRef = this.modalService.open(OrganizationPlanCloneComponent, DEFAULT_MODAL_OPTIONS);
      modalRef.componentInstance.setFormValue(this.form.value);
      modalRef.result.then((result) => {
        if (!result) {
          return;
        }
        if (this.orgPlanService.requestIsSuccess(result)) {
          this.rebuildAfterSaveTree();
        }
      });
    }
  }

  /**
   * Xu ly hien thi tab commonInfo
   * Sau khi save tree hoac clone
   */
  rebuildAfterSaveTree() {
    // xu ly focus mac dinh vao cay goc
    // this.tree.selectedNode = this.tree.rootNode;
    // set node selected for child companent
    // this.nodeSelected = this.tree.rootNode;
    // this.prepareInsertOrUpdate();
    // this.infoTab.createForm({}, ACTION_FORM.ORG_DRAFF_COMMON_INFO_INSERT);
    this.tabIndex = 0;
    // this.isShowCommonTab = true;
    // this.isShowAdditionTab = this.isShowBoundaryTab = false;

    // Xu ly cay don vi
    this.orgPlanService.findTreeOrgDraffById(this.orgPlanObject.orgPlanId)
    .subscribe(nodes => {
      this.tree.setDataNodes(this.orgPlanObject, nodes);
      // xu ly focus mac dinh vao cay goc
      this.tree.selectedNode = this.tree.rootNode;
      // set node selected for child companent
      this.nodeSelected = this.tree.rootNode;
    });
  }

  /**
   * convertTreeNodeToValue
   * param nodes
   */
  private convertTreeNodeToValue(nodes: any) {
    if (!nodes) {
      return null;
    }
    const values = [];
    for ( const node of nodes ) {
      const data = {nodeId: node.nodeId
                  , data: node.data
                  , expanded: node.expanded ? 1 : 0
                  , label: node.label
                  , children: this.convertTreeNodeToValue(node.children)
                };
      values.push(data);
    }
    return values;
  }

  callBackAffterSaveTabInfo(res) {
    if (this.orgPlanService.requestIsSuccess(res)) {
      this.rebuildTree(res.data);
    }

    if (this.additionInfoTab) {
      this.additionInfoTab.prepareSaveOrUpdate(this.nodeSelected);
    }
    if (this.boundaryTab) {
      this.boundaryTab.prepareSaveOrUpdate(this.nodeSelected);
    }
  }

  loadImportModal() {
    const modalRef = this.modalService.open(ImportOrgComponent, DEFAULT_MODAL_OPTIONS);
    modalRef.componentInstance.setFormValue(this.f.orgPlanId.value);
    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
      this.orgPlanService.findTreeOrgDraffById(this.f.orgPlanId.value)
          .subscribe(nodes => {
            this.tree.setDataNodes(this.orgPlanObject, nodes);
            this.rebuildAfterSaveTree();
          });
    });
  }

  /**
   * onChose
   */
  onChoseOrg() {
    const modalRef = this.modalService.open(OrgSelectorModalComponent, DEFAULT_MODAL_OPTIONS);
      modalRef
      .componentInstance
      .setInitValue({operationKey: 'action.insert'
        , adResourceKey: 'resource.organization'
        , filterCondition: ' ( org_parent_id = ' + this.form.controls['orgParentId'].value
          + ' or organization_id = ' + this.form.controls['orgParentId'].value + ' )'
        , rootId: this.form.controls['orgParentId'].value
        });

    modalRef.result.then((node) => {
      if (!node) {
        return;
      }
      if (node.organizationId === this.form.controls['orgParentId'].value) {
        this.orgPlanService.processReturnMessage({type: 'WARNING', code: 'orgPlan.dontChooseOriginOrg'});
      } else {
        const childNode = this.tree.getDefaultNode();
        childNode.data = node.organizationId;
        childNode.label = node.name;
        // Chi cho phep doi ten 1 don vi
        this.tree.dataNodes = [];
        this.tree.dataNodes.push(childNode);
        this.tree.rootNode.children = [];
        this.tree.rootNode.children.push(childNode);
      }
    });
  }

  ngAfterViewInit() {
  }

  /**
   * rebuildTree
   * @ param updatedNode
   */
  public rebuildTree(updatedNode) {
    // step 1 tìm và xóa node được update khỏi cây đơn vị
    const updatedId = updatedNode.orgDraffId;
    let currentNode = this.findNodeByNodeId(updatedId, this.tree.nodes, false);
    const isCreated = !currentNode;
    const isChanged = this.isChangeParent(currentNode, updatedNode);
    if (isChanged) {
      currentNode = this.findNodeByNodeId(updatedId, this.tree.nodes, true); // xoa khoi cay
    }
    if (isCreated) { // truong hop tao moi
      currentNode = {
          leaf: true
        , nodeId: updatedId
        , data: updatedId + ''
        , expanded: false
        , collapsedIcon: 'glyphicons glyphicons-folder-closed'
        , expandedIcon: 'glyphicons glyphicons-folder-open'
        , icon: 'glyphicons glyphicons-list-alt'
      };
    }
    currentNode.label = updatedNode.name;
    const orgParentId = updatedNode.orgParentId || this.tree.rootNode.nodeId;
    const newParent = this.findNodeByNodeId(orgParentId, this.tree.nodes, false);
    newParent.children = newParent.children || [];
    newParent.leaf = false;
    delete newParent.icon;
    newParent.collapsedIcon = 'glyphicons glyphicons-folder-closed';
    newParent.expandedIcon = 'glyphicons glyphicons-folder-open';
    newParent.expanded = true;
    if (isChanged || isCreated) {
      newParent.children.push(currentNode);
    }
    // xu ly focus mac dinh vao cay
    this.tree.selectedNode = currentNode;
    this.nodeSelected = currentNode;
  }

  private isChangeParent(curentNode, newNode) {
    if (!curentNode) {
      return false;
    }
    const curentParentId = curentNode.parent ? curentNode.parent.nodeId : this.tree.rootNode.nodeId;
    const newParentId = newNode.orgParentId ? newNode.orgParentId : this.tree.rootNode.nodeId;
    return curentParentId !== newParentId;
  }
  private findNodeByNodeId(nodeId: number, listNode, remove: boolean): any {
    for (let i = 0; i < listNode.length; i++) {
      const node = listNode[i];
      if (node.nodeId === nodeId) {
        if (remove) {
          listNode.splice(i, 1);
        }
        return node;
      } else if (node.children && node.children.length > 0) {
        const find = this.findNodeByNodeId(nodeId, node.children, remove);
        if (find) {
          return find;
        }
      }
    }
  }
}
