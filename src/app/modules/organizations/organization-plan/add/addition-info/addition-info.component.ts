import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ValidationService } from '@app/shared/services/validation.service';
import { AppComponent } from '@app/app.component';
import { HelperService } from '@app/shared/services/helper.service';
import { APP_CONSTANTS, RESOURCE, ACTION_FORM, NationConfigService, OrganizationService, OrgDutyService } from '@app/core';
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { NationService, LocationService, OrgDraffService } from '@app/core';
import { FileControl } from '@app/core/models/file.control';

@Component({
  selector: 'addition-info',
  templateUrl: './addition-info.component.html',
})
export class AdditionInfoComponent extends BaseComponent implements OnInit {

  _nodeSelected: any;
  _orgPlan: any;
  fieldReadOnly = false;
  showSaveButton = true;
  isPlanTypeChangeName = false;

  listNation: any;
  listLocation: any;
  listBank: any;
  listGender = APP_CONSTANTS.GENDERS;
  listStatus = APP_CONSTANTS.STATUS;
  formOtherInfoDraff: FormGroup;
  formOrgDutyDraff: FormGroup;
  formOrgBusinessDraff: FormArray;
  formOrgDocumentDraff: FormArray;
  @ViewChild('phonePrefix') phonePrefix;
  constructor(
    private activatedRoute: ActivatedRoute,
    private app: AppComponent,
    private nationService: NationService,
    private orgDraffService: OrgDraffService,
    private organizationService: OrganizationService,
    private orgDutyService: OrgDutyService,
    private locationService: LocationService,
    private helperService: HelperService,
    private nationConfigService: NationConfigService
  ) {
    super(activatedRoute, RESOURCE.ORGANIZATION_PLAN, ACTION_FORM.ORG_DRAFF_ADDITIONAL_INFO_UPDATE);
    this.buildFormOtherInfo({});
    this.buildFormOrgDutyDraff({});
    this.buildFormOrgBusinessDraff({});
    this.buildFormOrgDocumentDraff({});

    this.nationService.getNationList()
    .subscribe(res => {
      this.listNation = res.data;
    });
  }

  // get set tham so truyen vao component
  get orgPlan(): any {
    return this._orgPlan;
  }

  @Input()
  set orgPlan(orgPlan: any) {
    this._orgPlan = orgPlan;
    if (this.orgPlan && this.orgPlan.type === 1 ) {
      this.isPlanTypeChangeName = true;
    }
  }

  get nodeSelected(): any {
    return this._nodeSelected;
  }

  @Input()
  set nodeSelected(nodeSelected: any) {
    this._nodeSelected = nodeSelected;
    this.prepareSaveOrUpdate(nodeSelected);
  }

  ngOnInit() {
  }

  get f () {
    return this.formOtherInfoDraff.controls;
  }

  get fDutyDraff () {
    return this.formOrgDutyDraff.controls;
  }

  getProvincesByNationId() {
    if (this.formOtherInfoDraff.controls['nationId'].value) {
      this.locationService.getProvincesByNationId(this.formOtherInfoDraff.controls['nationId'].value)
      .subscribe(res => {
        this.listLocation = res;
        if (this.listLocation.length === 0) {
          this.formOtherInfoDraff.controls['locationId'].setValue('');
        }
      });
    } else {
      this.listLocation = [];
      this.formOtherInfoDraff.controls['locationId'].setValue('');
    }
  }

  getBanksByNationId() {
    const nationId = this.formOtherInfoDraff.controls['nationId'].value;
    if (nationId) {
      this.nationConfigService.getNationConfigList(APP_CONSTANTS.NATION_CONFIG_TYPE.BANK, nationId)
      .subscribe(res => {
        this.listBank = [];
        for (const item of res.data) {
          this.listBank.push({label: item.name, value: item.nationConfigId});
        }
        if (this.listBank.length === 0) {
          this.formOtherInfoDraff.controls['bankId'].setValue('');
        }
      });
    } else {
      this.listBank = [];
      this.formOtherInfoDraff.controls['bankId'].setValue('');
    }
  }

  buildFormOtherInfo(formData: any) {
    this.formOtherInfoDraff = this.buildForm(formData, {
      orgDraffId: [''],
      nationId: [''],
      locationId: [''],
      bankAccount: ['', [ValidationService.maxLength(50)]],
      bankId: [''],
      bankBranch: ['', [ValidationService.maxLength(50)]],
      phoneNumber: ['', [ValidationService.maxLength(50), ValidationService.mobileNumber]],
      isOrgSalary: [null],
      isOrgRecruitment: [null],
      isOrgSign: [null],
      isOrgModerator: [null],
      isOrgTax: [null],
      isOrgInsurance: [null],
      isOrgReward: [null],
    }, ACTION_FORM.ORG_DRAFF_ADDITIONAL_INFO_UPDATE);
  }

  buildFormOrgDutyDraff(formData: any) {
    this.formOrgDutyDraff = this.buildForm(formData, {
      orgDutyDraffId: [''],
      orgDraffId: [''],
      effectiveDate: [null],
      target: ['', [ValidationService.maxLength(1000)]],
      mechanism: ['', [ValidationService.maxLength(1000)]],
      mainDuty: ['', [ValidationService.maxLength(1000)]],
      jurisdiction: ['', [ValidationService.maxLength(1000)]],
      responsibility: ['', [ValidationService.maxLength(1000)]],
      innerRelationship: ['', [ValidationService.maxLength(1000)]],
      outerRelationship: ['', [ValidationService.maxLength(1000)]],
    }, ACTION_FORM.ORG_DRAFF_DUTY_UPDATE);
    this.formOrgDutyDraff.addControl('orgDutyDraffFile', new FileControl(null));
  }

  /**
   * make Form Business Registration Draff
   */
  private makeFormOrgBusinessDraff(): FormGroup {
    const formGroup = this.buildForm({}, {
      orgBusinessRegistrationDraffId: [null],
      orgDraffId: [null],
      issuedDate: [null],
      address: [null, [ValidationService.maxLength(1000)]],
      leaderName: [null, [ValidationService.maxLength(200)]],
      leaderGender: [null],
      leaderBirthDay: [null, [ValidationService.beforeCurrentDate]],
      reason: [null, [ValidationService.maxLength(500)]],
      documentNumber: [null, [ValidationService.maxLength(100)]],
      signedBy: [null, [ValidationService.maxLength(100)]],
      orgSigned: [null, [ValidationService.maxLength(200)]]
    }, ACTION_FORM.ORG_DRAFF_BUSINESS_REGIS_UPDATE);

    formGroup.addControl('orgBusinessDraffFile', new FileControl(null));
    return formGroup;
  }

  buildFormOrgBusinessDraff(listOrgBusinessDraff: any) {
    if (!listOrgBusinessDraff) {
      this.formOrgBusinessDraff = new FormArray([]);
    } else {
      const controls = new FormArray([]);
      for (const i in listOrgBusinessDraff) {
        const orgBusinessDraff = listOrgBusinessDraff[i];
        const group = this.makeFormOrgBusinessDraff();
        group.patchValue(orgBusinessDraff);
        if (orgBusinessDraff.fileAttachment) {
          let fileData;
          if (this.isPlanTypeChangeName || this.nodeSelected.isRoot) {
            fileData = orgBusinessDraff.fileAttachment.orgBusinessFile;
          } else {
            fileData = orgBusinessDraff.fileAttachment.orgBusinessDraffFile;
          }
          (group.controls['orgBusinessDraffFile'] as FileControl)
          .setFileAttachment(fileData);
        }
        controls.push(group);
      }
      this.formOrgBusinessDraff = controls;
    }
  }

  /**
   * make Form Org Document Draff
   */
  private makeFormOrgDocumentDraff(): FormGroup {
    const formGroup = this.buildForm({}, {
      orgDocumentManagerDraffId: [null],
      orgDraffId: [null],
      orgFileManagerId: [null],
      effectiveDate: [null],
      expiredDate: [null],
      employeeId: [null],
      status: [null]
    }, ACTION_FORM.ORG_DRAFF_DOCUMENT_UPDATE,
    [
      ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.organization.expiredDate')
    ]);
    return formGroup;
  }

  buildFormOrgDocumentDraff(listOrgDocument: any) {
    if (!listOrgDocument) {
      this.formOrgDocumentDraff = new FormArray([]);
    } else {
      const controls = new FormArray([]);
      for (const i in listOrgDocument) {
        const orgDocument = listOrgDocument[i];
        const group = this.makeFormOrgDocumentDraff();
        group.patchValue(orgDocument);
        controls.push(group);
      }
      this.formOrgDocumentDraff = controls;
    }
  }

  public prepareSaveOrUpdate(e) {
    if (!e) {
      return;
    }
    let orgDraffId = e.nodeId;
    this.actionToggleAllowEdit(e);
    if (e.isRoot) {
      orgDraffId = e.data;
      this.loadDataByReal(orgDraffId);
    } else {
      if (!this.isPlanTypeChangeName) {
        this.loadDataByDraff(orgDraffId);
      }
    }
  }

  private loadDataByDraff(orgDraffId) {
    if (CommonUtils.isValidId(orgDraffId)) {
      this.orgDraffService.findOrgOtherInfoDraff(orgDraffId)
        .subscribe(res => {
          if (this.orgDraffService.requestIsSuccess(res)) {
            this.buildFormOtherInfo(res.data);
            this.getProvincesByNationId();
            this.getBanksByNationId();
          }
        });

      this.orgDraffService.findOrgDutyDraff(orgDraffId)
        .subscribe(res => {
          if (this.orgDraffService.requestIsSuccess(res)) {
            this.buildFormOrgDutyDraff(res.data);
            if (res.data.fileAttachment) {
              (this.formOrgDutyDraff.controls['orgDutyDraffFile'] as FileControl)
                .setFileAttachment(res.data.fileAttachment.orgDutyDraffFile);
            }
            if (res.data.orgDraffId == null) {
              this.formOrgDutyDraff.controls['orgDraffId'].setValue(orgDraffId);
            }
          }
        });

      this.orgDraffService.findOrgBusinessDraff(orgDraffId)
        .subscribe(res => {
          this.buildFormOrgBusinessDraff(res);
        });

      this.orgDraffService.findOrgDocumentDraff(orgDraffId)
        .subscribe(res => {
          this.buildFormOrgDocumentDraff(res);
        });
    } else {
      this.buildFormOtherInfo({});
      this.buildFormOrgDutyDraff({});
      this.buildFormOrgBusinessDraff({});
      this.buildFormOrgDocumentDraff({});
    }
  }

  private loadDataByReal(orgId) {
    // Load data to form other info
    this.organizationService.findOrgAdditionalInfo(orgId)
    .subscribe(res => {
      if (this.organizationService.requestIsSuccess(res)) {
        this.buildFormOtherInfo(res.data);
        this.getProvincesByNationId();
        this.getBanksByNationId();
      }
    });

    // Load data to orgDuty
    this.orgDutyService.findOrgDuty(orgId)
    .subscribe(res => {
      if (res.data != null) {
        this.buildFormOrgDutyDraff(res.data);
        if (res.data.fileAttachment) {
          (this.formOrgDutyDraff.controls['orgDutyDraffFile'] as FileControl)
            .setFileAttachment(res.data.fileAttachment.orgDutyFile);
        }
        if (res.data.orgDraffId == null) {
          this.formOrgDutyDraff.controls['orgDraffId'].setValue(orgId);
        }
      }
    });

    // Load list business registration to table
    this.organizationService.findOrgBusiness(orgId)
    .subscribe(res => {
      this.buildFormOrgBusinessDraff(res);
    });

    // Load list org document manager to table
    this.organizationService.findOrgDocument(orgId)
    .subscribe(res => {
      this.buildFormOrgDocumentDraff(res);
    });
  }

  processSaveOrUpdate() {
    if (!this.validateBeforeSave()) {
      return;
    }

    const formSave = {};
    formSave['otherInfoDraff'] = this.formOtherInfoDraff.value;
    formSave['orgDutyDraff'] = this.formOrgDutyDraff.value;
    formSave['lstOrgBusinessDraff'] = this.formOrgBusinessDraff.value;
    formSave['lstDocumentDraff'] = this.formOrgDocumentDraff.value;
    this.app.confirmMessage(null, () => { // on accepted
      this.orgDraffService.saveOrUpdateOtherInfoDraff(formSave, this.nodeSelected.nodeId)
        .subscribe(res => {
          if (this.orgDraffService.requestIsSuccess(res)) {
            this.helperService.afterSaveOrganization(res.data);
            this.prepareSaveOrUpdate(this.nodeSelected);
          }
        });
      }, null);
  }

  /**
   * addOrgBusinessDraff
   * param index
   * param item
   */
  public addOrgBusinessDraff(index: number, item: FormGroup) {
    const controls = this.formOrgBusinessDraff as FormArray;
    controls.insert(index + 1, this.makeFormOrgBusinessDraff());
  }

  /**
   * removeOrgBusinessDraff
   * param index
   * param item
   */
  public removeOrgBusinessDraff(index: number, item: FormGroup) {
    const controls = this.formOrgBusinessDraff as FormArray;
    if (controls.length === 1) {
      this.buildFormOrgBusinessDraff({});
      const group = this.makeFormOrgBusinessDraff();
      controls.push(group);
      this.formOrgBusinessDraff = controls;
    }
    controls.removeAt(index);
  }

  /**
   * addOrgDocument
   * param index
   * param item
   */
  public addOrgDocumentDraff(index: number, item: FormGroup) {
    const controls = this.formOrgDocumentDraff as FormArray;
    controls.insert(index + 1, this.makeFormOrgDocumentDraff());
  }

  /**
   * removeOrgDocument
   * param index
   * param item
   */
  public removeOrgDocumentDraff(index: number, item: FormGroup) {
    const controls = this.formOrgDocumentDraff as FormArray;
    if (controls.length === 1) {
      this.buildFormOrgDocumentDraff({});
      const group = this.makeFormOrgDocumentDraff();
      controls.push(group);
      this.formOrgDocumentDraff = controls;
    }
    controls.removeAt(index);
  }

  private validateBeforeSave(): boolean {
    const formOtherInfoDraff =CommonUtils.isValidForm(this.formOtherInfoDraff);
    const formOrgDutyDraff = CommonUtils.isValidForm(this.formOrgDutyDraff);
    const formOrgBusinessDraff = CommonUtils.isValidForm(this.formOrgBusinessDraff);
    const formOrgDocumentDraff = CommonUtils.isValidForm(this.formOrgDocumentDraff);
    return formOtherInfoDraff
          && formOrgDutyDraff
          && formOrgBusinessDraff
          && formOrgDocumentDraff;
  }

  private actionToggleAllowEdit(e?: any) {
    if (!e) {
      this.showSaveButton = false;
      this.fieldReadOnly = true;
      if (this.phonePrefix) {
        this.phonePrefix.disabled = true;
      }
      return;
    }

    if (e.isRoot || !e.nodeId) {
      this.showSaveButton = false;
      this.fieldReadOnly = true;
      if (this.phonePrefix) {
        this.phonePrefix.disabled = true;
      }
    } else {
      setTimeout(() => {
        // TH thêm mới và từ chối phê duyệt thì đc phép sửa
        if (this.orgPlan.status === 0 || this.orgPlan.status === 3) {
          this.showSaveButton = true;
          this.fieldReadOnly = false;
          if (this.phonePrefix && !this.isPlanTypeChangeName) {
            if (this.isPlanTypeChangeName) {
              this.phonePrefix.disabled = true;
            } else {
              this.phonePrefix.disabled = false;
            }
          }
        } else {
          this.showSaveButton = false;
          this.fieldReadOnly = true;
          if (this.phonePrefix) {
            this.phonePrefix.disabled = true;
          }
        }
      }, 500);
    }
  }
}


