import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { AppComponent } from '@app/app.component';
import { FormGroup, Validators } from '@angular/forms';
import { RESOURCE, ACTION_FORM } from '@app/core/app-config';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { FileControl } from '@app/core/models/file.control';
import { CommonUtils, ValidationService } from '@app/shared/services';
import { OrgPlanService } from '@app/core';
import { saveAs } from 'file-saver';

@Component({
  selector: 'import-org',
  templateUrl: './import-org.component.html'
})
export class ImportOrgComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  dataError: any;
  formConfig = {
    orgPlanId: ['']
  };
  constructor(
    public activatedRoute: ActivatedRoute,
    private app: AppComponent,
    public activeModal: NgbActiveModal,
    private orgPlanService: OrgPlanService
  ) {
    super(activatedRoute, RESOURCE.ORGANIZATION_PLAN, ACTION_FORM.INSERT);
    this.formSave = this.buildForm({}, this.formConfig, ACTION_FORM.INSERT);
    this.formSave.addControl('fileImport', new FileControl(null, ValidationService.required));
  }

  ngOnInit() {
  }

  get f () {
    return this.formSave.controls;
  }

  setFormValue(planId: number) {
    this.formSave.get('orgPlanId').setValue(planId);
  }

  actionSubmitForm() {
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    }
    this.app.confirmMessage(null, () => {
      this.orgPlanService.importOrganizationTree(this.formSave.get('orgPlanId').value, this.formSave.value).subscribe(
        res => {
          if (this.orgPlanService.requestIsSuccess(res)) {
            this.activeModal.close(res);
          } else {
            this.dataError = res.data;
          }
        }
      );
    }, null);
  }

  actionDownloadTemplate() {
    this.orgPlanService.downloadTemplateImport().subscribe(res => {
      saveAs(res, 'import_organization.xls');
    });
  }
}
