import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { HelperService } from '@app/shared/services/helper.service';
import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { CommonUtils, ValidationService } from '@app/shared/services';
import { OrganizationTypeService, LineOrgService, OrgDraffService, RESOURCE, ACTION_FORM, OrganizationService } from '@app/core';
import { AppComponent } from '@app/app.component';
import { OrgDraffSelectorComponent } from '../../org-draff-selector/org-draff-selector.component';

@Component({
  selector: 'common-info',
  templateUrl: './common-info.component.html',
})
export class CommonInfoComponent extends BaseComponent implements OnInit {

  _nodeSelected: any;
  _orgPlan: any;

  formInfo: FormGroup;

  listOrgType: any;
  lineOrgList: any;
  isPlanTypeChangeName = false;
  isRoot = false;
  // planId: number;
  // @ViewChild('draffSelector')
  // draffSelector: OrgDraffSelectorComponent;
  defaultOrgParentName = '';

  @Output() callBackAffterSave: EventEmitter<any> = new EventEmitter<any>();
  defaultParentName = '';
  codeFieldReadOnly = true;
  fieldReadOnly = false;
  showSaveButton = true;
  // nodeSelect: any;
  constructor(
      private activatedRoute: ActivatedRoute,
      private orgTypeService: OrganizationTypeService,
      private orgDraffservice: OrgDraffService,
      private orgService: OrganizationService,
      private helperService: HelperService,
      private app: AppComponent,
      private lineOrgService: LineOrgService
    ) {
    super(activatedRoute, RESOURCE.ORGANIZATION_PLAN, ACTION_FORM.ORG_DRAFF_COMMON_INFO_INSERT);
    this.createForm({}, ACTION_FORM.ORG_DRAFF_COMMON_INFO_INSERT);
    this.orgTypeService.getListOrgType().subscribe(res => {
      this.listOrgType = res;
    });
    this.lineOrgService.getListLineCareer().subscribe(res => {
      this.lineOrgList = res.data;
    });
  }

  // get set tham so truyen vao component
  get nodeSelected(): any {
    return this._nodeSelected;
  }

  @Input()
  set nodeSelected(nodeSelected: any) {
    this._nodeSelected = nodeSelected;
    this.prepareSaveOrUpdate(this.nodeSelected);
  }

  get orgPlan(): any {
    return this._orgPlan;
  }

  @Input()
  set orgPlan(orgPlan: any) {
    this._orgPlan = orgPlan;
    if (this.orgPlan && this.orgPlan.type === 1 ) {
      this.isPlanTypeChangeName = true;
    }
  }

  get f () {
    return this.formInfo.controls;
  }

  ngOnInit() {
  }

  /**
   * build form
   */
  public createForm(formData: any, actionForm: any): void {
    this.formInfo = this.buildForm(formData, {
      orgDraffId: [null],
      organizationId: [null],
      isOrgLevelManage: [1],
      orgParentId: [null],
      orgPlanId: [null],
      orgOrder: [null,
        [
          ValidationService.positiveInteger,
          Validators.min(1),
          Validators.max(99999999999),
          ValidationService.maxLength(11)]
        ],
      code: ['', [ValidationService.required, ValidationService.maxLength(50)]],
      name: ['', [ValidationService.required, ValidationService.maxLength(500)]],
      shortName: ['', [ValidationService.maxLength(500)]],
      orgTypeId: [null, [ValidationService.required]],
      orgManager: [null],
      lineOrgId: [null],
      address: [''],
      establishReason: ['', ValidationService.maxLength(500)],
      description: ['', ValidationService.maxLength(500)]
    }, actionForm);

    // Xu ly chon don vi co cap quan ly
    if (!this.isPlanTypeChangeName) {
      this.showHideCodeField(this.formInfo.get('isOrgLevelManage').value);
      this.formInfo.get('isOrgLevelManage').valueChanges.subscribe(
        (value) => {
          this.showHideCodeField(value);
        }
      );
    }
  }

  private showHideCodeField(isOrgLevelManage) {
    this.codeFieldReadOnly = isOrgLevelManage;
    const codeDefault = this.formInfo.get('code').value;
    this.formInfo.removeControl('code');
    if (isOrgLevelManage !== 1) {
      this.formInfo.addControl('code', CommonUtils.createControl(this.actionForm, 'code', codeDefault,
      [ValidationService.required, ValidationService.maxLength(50)], this.propertyConfigs));
    } else {
      this.formInfo.addControl('code', CommonUtils.createControl(this.actionForm, 'code', codeDefault,
      [ValidationService.maxLength(50)], this.propertyConfigs));
    }
  }

  /**
   * prepareSaveOrUpdate
   * param orgDraffId
   */
  public prepareSaveOrUpdate(e) {
    if (!e) {
      return;
    }
    let orgDraffId = e.nodeId;
    let serviceOrg: any;
    if (this.isPlanTypeChangeName || e.isRoot) {
      orgDraffId = e.data;
      serviceOrg = this.orgService;
    } else {
      serviceOrg = this.orgDraffservice;
    }
    if (CommonUtils.isValidId(orgDraffId)) {
      serviceOrg.findOneWithPlan(orgDraffId, this.orgPlan.orgPlanId)
        .subscribe((res) => {
          if (serviceOrg.requestIsSuccess(res)) {
            this.actionToggleAllowEdit(e);
            this.createForm(res.data, ACTION_FORM.ORG_DRAFF_COMMON_INFO_UPDATE);
            if (!res.data.orgParentId) {
              // setTimeout(() => {
                this.formInfo.removeControl('orgParentId');
                this.formInfo.addControl('orgParentId', new FormControl(null));
                this.defaultOrgParentName = e.parent.label;
                // this.draffSelector.setDefaultName(e.parent.label);
              // }, 100);

            }
          }
        });
    } else if (e.nodeId === 0) {
      this.actionToggleAllowEdit(e);
      // Xu ly khi chon node moi
      this.createForm({}, ACTION_FORM.ORG_DRAFF_COMMON_INFO_INSERT);
      if (this.orgPlan) {
        this.f.orgPlanId.setValue(this.orgPlan.orgPlanId);
      }
      this.f.isOrgLevelManage.setValue(1);
      this.f.orgOrder.setValue(e.index);
      this.formInfo.removeControl('orgParentId');
      // this.formInfo.controls['lineOrgId'].clearValidators();
      if (e.parent && !e.parent.isRoot) {
        this.formInfo.addControl('orgParentId', new FormControl(e.parent.nodeId));
      } else {
        this.formInfo.addControl('orgParentId', new FormControl(null));
        this.defaultOrgParentName = e.parent.label;
        // this.draffSelector.setDefaultName(e.parent.label);
      }
    }
  }

  private actionToggleAllowEdit(e?: any) {
    if (!e) {
      this.showSaveButton = false;
      this.fieldReadOnly = true;
      return;
    }

    if (e.isRoot) {
      this.showSaveButton = false;
      this.fieldReadOnly = true;
      this.isRoot = true;
    } else {
      this.isRoot = false;
      setTimeout(() => {
        // TH thêm mới và từ chối phê duyệt thì đc phép sửa
        if (this.orgPlan.status === 0 || this.orgPlan.status === 3) {
          this.showSaveButton = true;
          this.fieldReadOnly = false;
        } else {
          this.showSaveButton = false;
          this.fieldReadOnly = true;
        }
      }, 500);
    }
  }

  public processSaveOrUpdate() {
    if (!this.validateBeforeSave()) {
      return;
    }
    this.f.orgPlanId.setValue(this.orgPlan.orgPlanId);
    this.app.confirmMessage(null, () => { // on accepted
      this.orgDraffservice.saveOrUpdate(this.formInfo.value)
        .subscribe(res => {
          if (this.orgDraffservice.requestIsSuccess(res)) {
            this.createForm(res.data, ACTION_FORM.ORG_DRAFF_COMMON_INFO_UPDATE);
            this.f.orgPlanId.setValue(this.orgPlan.orgPlanId);
            this.f.orgDraffId.setValue(res.data.orgDraffId);
            if (this.nodeSelected) {
              this.nodeSelected.nodeId = res.data.orgDraffId;
            }
            this.callBackAffterSave.emit(res);
          }
        });
      }, null);
  }

  public validateBeforeSave(): boolean {
    return CommonUtils.isValidForm(this.formInfo);
  }
  public checkDuplicate() {
    const orgParentId = this.f['orgParentId'].value;
    const orgDraffId = this.f['orgDraffId'].value;
    if (orgParentId === orgDraffId) {
      this.orgDraffservice.processReturnMessage({type: 'WARNING', code: 'plan.invalidOrgParentId'});
      // this.f['orgParentId'].setValue(null);
      // this.draffSelector.onChangeOrgId();
      this.formInfo.removeControl('orgParentId');
      this.formInfo.addControl('orgParentId', new FormControl(null));
    }
  }
}
