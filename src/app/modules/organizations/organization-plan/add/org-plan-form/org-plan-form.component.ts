import { AppComponent } from '@app/app.component';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { APP_CONSTANTS, RESOURCE, ACTION_FORM } from '@app/core/app-config';
import { FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OrgPlanService } from '@app/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ValidationService } from '@app/shared/services';

@Component({
  selector: 'org-plan-form',
  templateUrl: './org-plan-form.component.html',
})
export class OrganizationPlanFormComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  isCreatedDraffTree = false;
  typeList = APP_CONSTANTS.PLAN_TYPE;
  formConfig = {
    orgPlanId: [null],
    orgParentId: ['', [ ValidationService.required ]],
    type: ['', [ ValidationService.required ]],
    planName: ['', [ ValidationService.required, ValidationService.maxLength(200) ]],
    effectiveDate: ['', [ ValidationService.required ]],
    organizationId: ['']
  };
  constructor(
    public actr: ActivatedRoute,
    private orgPlanService: OrgPlanService,
    private app: AppComponent,
    public activeModal: NgbActiveModal
  ) {
    super(actr, RESOURCE.ORGANIZATION_PLAN, ACTION_FORM.ORG_PLAN_INSERT);
    this.formSave = this.buildForm({}, this.formConfig);
  }

  ngOnInit() {
  }

  get f() {
    return this.formSave.controls;
  }

  processSaveOrUpdate() {
    if (!this.validateBeforeSave()) {
      return;
    }
    const formOrgPlan = {};
    this.formSave.removeControl('orgParentEffectiveDate');
    this.formSave.removeControl('orgParentExpiredDate');
    this.formSave.controls['organizationId'].setValue(this.formSave.controls['orgParentId'].value);
    formOrgPlan['info'] = this.formSave.value;
    this.app.confirmMessage(null, () => { // on accepted
      this.orgPlanService.saveOrUpdate(formOrgPlan)
        .subscribe(res => {
          if (this.orgPlanService.requestIsSuccess(res)) {
            this.activeModal.close(res);
          }
        });
    }, () => {
      // on rejected
    });
  }

  private validateBeforeSave(): boolean {
    return CommonUtils.isValidForm(this.formSave);
  }

  private setFormValue(propertyConfigs: any, data?: any): void {
    this.propertyConfigs = propertyConfigs;
    if (data && data.orgPlanId > 0) {
      this.orgPlanService.checkIsCreatedDraffTree(data.orgPlanId)
      .subscribe(res => {
        this.isCreatedDraffTree = res.data;
      });
      this.formSave = this.buildForm(data, this.formConfig, ACTION_FORM.ORG_PLAN_UPDATE);
    } else {
      this.formSave = this.buildForm({}, this.formConfig, ACTION_FORM.ORG_PLAN_INSERT);
    }
  }
}



