import { OrgPlanService, OrganizationService, OrgDraffService, EmployeeInfoService } from '@app/core';
import { Component, OnInit, HostListener } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TreeNode } from 'primeng/api';
import { forEach } from '@angular/router/src/utils/collection';
import { TranslationService } from 'angular-l10n';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'view-detail',
  templateUrl: './view-detail.component.html'
})
export class ViewDetailComponent implements OnInit {
  orgPlanId: number;
  orgParentId: number;
  rootOrg: TreeNode;
  data: TreeNode[];
    // Animation Variable
    paneDragging = false;
    paneTransform;
    zoom = 1;
    paneX = 0;
    paneY = 0;

  constructor(
    public activeModal: NgbActiveModal,
    private orgService: OrganizationService,
    private orgPlanService: OrgPlanService,
    private translationService: TranslationService,
    private sanitizer: DomSanitizer
  ) {
    this.data = [{}];
  }

  ngOnInit() {
    if (this.orgParentId) {
      this.orgService.findOne(this.orgParentId)
      .subscribe(res => {
        // Set root node of org tree
        this.rootOrg = {
          label: res.data.name,
          styleClass: 'ui-org',
          data: {
            orgId: res.data.organizationId,
            name: res.data.name,
            orgManager: '',
            employeeAmount: '',
            childUnit: '' },
          type: 'rootNode',
          expanded: true,
          children: []
        };
        this.data[0] = this.rootOrg;

        // Find list orgDraff parent
        this.orgPlanService.findListParentViewDetail(this.orgPlanId)
        .subscribe(result => {
          if (result.data.length > 0) {
            const orgDraffParentNodes: TreeNode[] = [{}];
            // Set list child
            let i = 0;
            result.data.forEach(function (value) {
              let node: TreeNode = {};
              node = {
                label: value.name,
                styleClass: 'ui-org',
                data: value,
                type: 'node',
                expanded: true,
                children: []
              };

              orgDraffParentNodes[i] = node;
              i++;
            });
            this.data[0].children = orgDraffParentNodes;
          }
        });
      });
    }
  }

  onNodeSelect(event) {
    if (event['node']['children'].length > 0) {
      return;
    } else {
      this.searchTree(this.data[0], event['node']['data']);
    }
  }

  searchTree(element, matchingData) {
    if (element.data.orgId === matchingData.orgId) {
      this.orgPlanService.findListChildViewDetail(matchingData.orgId)
      .subscribe(result => {
        if (result.data.length > 0) {
          const orgDraffParentNodes: TreeNode[] = [{}];
          let i = 0;
          result.data.forEach(function (value) {
            let node: TreeNode;
            node = {
              label: value.name,
              styleClass: 'ui-org',
              data: value,
              type: 'node',
              expanded: true,
              children: []
            };

            orgDraffParentNodes[i] = node;
            i++;
          });
          element.children = orgDraffParentNodes;
        }
      });
      return;
    } else if (element.children) {
      let i: number;
      let result = null;
      for ( i = 0; result == null && i < element.children.length; i++) {
        result = this.searchTree(element.children[i], matchingData);
      }
      return result;
    }
    return;
  }


    // Host Listener for Browser
    @HostListener('mousewheel', ['$event']) onMouseWheelChrome(event: any) {
      this.onmousewheel(event);
    }

    @HostListener('DOMMouseScroll', ['$event']) onMouseWheelFirefox(event: any) {
      this.onmousewheel(event);
    }

    @HostListener('onmousewheel', ['$event']) onMouseWheelIE(event: any) {
      this.onmousewheel(event);
    }

    // Animation Function
    public onmousewheel(event) {
      let delta;
      event.preventDefault();
      delta = event.detail || event.wheelDelta;
      this.zoom += delta / 1000 / 2;
      this.zoom = Math.min(Math.max(this.zoom, 0.2), 3);
      this.makeTransform();
    }

    public onmousemove(event) {
      if (this.paneDragging) {
          const {movementX, movementY} = event;
          this.paneX += movementX;
          this.paneY += movementY;
          this.makeTransform();
      }
    }

    public onmouseup(event) {
      this.paneDragging = false;
    }

    public preventMouse(event) {
      event.stopPropagation();
    }

    public onmousedown(event) {
      this.paneDragging = true;
    }

    public makeTransform() {
      this.paneTransform = this.sanitizer.bypassSecurityTrustStyle(`translate(${this.paneX}px, ${this.paneY}px) scale(${this.zoom})`);
    }
}
