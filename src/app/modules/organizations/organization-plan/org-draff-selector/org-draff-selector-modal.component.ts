import { FormGroup, FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { TreeNode } from 'primeng/api';
import { OrgDraffSelectorService } from '@app/core';

@Component({
  selector: 'org-draff-selector-modal',
  templateUrl: './org-draff-selector-modal.component.html',
})
export class OrgDraffSelectorModalComponent implements OnInit {
  nodes: TreeNode[];
  public orgPlanId: number;
  selectedNode: TreeNode;
  resultList: any = {};
  form: FormGroup;
  params: any;
  placeholdercode: string;
  placeholdername: string;
  constructor(
      public activeModal: NgbActiveModal
    , private formBuilder: FormBuilder
    , private service: OrgDraffSelectorService
    ) { }

  ngOnInit() {
    this.buildForm();
  }
  /**
   * set init value
   */
  setInitValue(params: {operationKey: '', adResourceKey: '', filterCondition: '', orgPlanId: number}) {
    this.params = params;
    this.placeholdercode = 'orgSelectorPlaceholder.codeOrgPlaceholder';
    this.placeholdername = 'orgSelectorPlaceholder.nameOrgPlaceholder';
    this.orgPlanId = params.orgPlanId;
    this.actionInitAjax();
  }
  /**
   * action init ajax
   */
  private actionInitAjax() {
    this.service.actionInitAjax(this.orgPlanId, this.params)
        .subscribe((res) => {
          this.nodes = CommonUtils.toTreeNode(res);
          this.processSearch(null);
        });
  }
  /**
   * actionLazyRead
   * @ param event
   */
  public actionLazyRead(event) {
    const params = this.params;
    params.nodeId = event.node.nodeId;
    this.service.actionLazyRead(this.orgPlanId, params)
      .subscribe((res) => {
        event.node.children = CommonUtils.toTreeNode(res);
      });
  }
/****************** CAC HAM COMMON DUNG CHUNG ****/
  /**
   * buildForm
   */
  private buildForm(): void {
    this.form = this.formBuilder.group({
      code: [''],
      name: [''],
      orgPlanId: [this.orgPlanId],
    });
  }
  /**
   * processSearch
   * @ param event
   */
  public processSearch(event) {
    if (CommonUtils.isValidForm(this.form)) {
      const params = this.form.value;
      if (this.selectedNode) {
        params.nodeId = this.selectedNode.data;
      }
      this.service.search(params, event).subscribe(res => {
        this.resultList = res;
      });
    }
  }
  /**
   * chose
   * @ param item
   */
  public chose(item) {
    this.activeModal.close(item);
  }
  /**
   * nodeSelect
   * @ param event
   */
  public nodeSelect(event) {
    this.selectedNode = event.node;
    this.processSearch(null);
  }
}
