import { FormControl } from '@angular/forms';
import { Component, OnInit, Input, ViewChildren, AfterViewInit, Output, EventEmitter, OnChanges } from '@angular/core';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { OrgDraffSelectorModalComponent } from './org-draff-selector-modal.component';
import { DEFAULT_MODAL_OPTIONS } from '@app/core/app-config';
import { OrgDraffService } from '@app/core';

@Component({
  selector: 'org-draff-selector',
  templateUrl: './org-draff-selector.component.html',
})
export class OrgDraffSelectorComponent implements OnInit, AfterViewInit, OnChanges {
  @Input()
  public property: FormControl;
  @Input()
  public orgPlanId: number;
  @Input()
  public isRequiredField = false;
  @Input()
  public operationKey: string;
  @Input()
  public adResourceKey: string;
  @Input()
  public filterCondition: string;
  @Output()
  public onChange: EventEmitter<any> = new EventEmitter<any>();
  @Input()
  public disabled = false;
  @Input()
  public defaultName = '';

  @ViewChildren('displayName')
  public displayName;
  @ViewChildren('buttonChose')
  public buttonChose;

  constructor(
        private service: OrgDraffService
      , private modalService: NgbModal
  ) {
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
  }
  /**
   * ngAfterViewInit
   */
  ngAfterViewInit() {
    this.onChangeOrgId();
  }
  /**
   * delete
   */
  delete() {
    this.property.setValue('');
    this.onChangeOrgId();
  }
  /**
   * onChange orgId then load org name
   */
  public onChangeOrgId() {
    if (CommonUtils.isNullOrEmpty(this.property.value)) {
      if (this.displayName) {
        this.displayName.first.nativeElement.value = '' + this.defaultName;
      }
      return;
    }
    // thuc hien lay ten don vi de hien thi
    this.service.findOne(this.property.value)
        .subscribe((res) => {
          const data = res.data;
          if (data) {
            this.displayName.first.nativeElement.value = data.name;
            this.defaultName = '';
          }
        });
  }
  /**
   * ngOnChanges
   */
  ngOnChanges() {
    this.onChangeOrgId();
  }
  /**
   * onFocus
   */
  public onFocus() {
    this.buttonChose.first.nativeElement.focus();
    this.buttonChose.first.nativeElement.click();
  }
  /**
   * onChose
   */
  public onChose() {
    const modalRef = this.modalService.open(OrgDraffSelectorModalComponent, DEFAULT_MODAL_OPTIONS);
    modalRef
      .componentInstance
      .setInitValue({operationKey: this.operationKey, adResourceKey: this.adResourceKey
                   , filterCondition: this.filterCondition, orgPlanId: this.orgPlanId});

    modalRef.result.then((node) => {
      if (!node) {
        return;
      }
      this.property.setValue(node.organizationId);
      this.displayName.first.nativeElement.value = node.name;
      this.defaultName = '';
      // callback on chose item
      this.onChange.emit(node);
    });
  }

  public setDefaultName(name) {
    this.defaultName = name;
  }
}
