import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@app/shared';
import { TreeModule } from 'primeng/tree';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { StepsModule } from 'primeng/steps';

import { OrganizationPlanRoutingModule } from './organization-plan-routing.module';
import { OrganizationPlanComponent } from './organization-plan.component';
import { OrganizationPlanSearchComponent } from './search/organization-plan-search.component';
import { OrganizationPlanAddComponent } from './add/organization-plan-add.component';
import { OrganizationPlanTreeComponent } from './tree/organization-plan-tree.component';
import { OrganizationPlanCloneComponent } from './clone/organization-plan-clone.component';
import { CommonInfoComponent } from './add/common-info/common-info.component';
import { AdditionInfoComponent } from './add/addition-info/addition-info.component';
import { BoundaryComponent } from './add/boundary/boundary.component';
import { OrganizationPlanFormComponent } from './add/org-plan-form/org-plan-form.component';
import { OrgDraffSelectorComponent } from './org-draff-selector/org-draff-selector.component';
import { OrgDraffSelectorModalComponent } from './org-draff-selector/org-draff-selector-modal.component';
import { ViewDetailComponent } from './view-detail/view-detail.component';
import { ListEmployeeModalComponent } from './add/boundary/list-employee-modal.component';
import { ImportOrgComponent } from './add/import-org/import-org.component';
import { ListOrgDraffErrorComponent } from './search/list-org-draff-error/list-org-draff-error.component';
import { ConfirmRefusedComponent } from './search/confirm-refused/confirm-refused.component';

@NgModule({
  declarations: [
    OrganizationPlanComponent
    , OrganizationPlanSearchComponent
    , OrganizationPlanFormComponent
    , OrganizationPlanAddComponent
    , OrganizationPlanTreeComponent
    , OrgDraffSelectorComponent
    , OrgDraffSelectorModalComponent
    , OrganizationPlanCloneComponent
    , CommonInfoComponent
    , AdditionInfoComponent
    , BoundaryComponent
    , ViewDetailComponent
    , ListEmployeeModalComponent
    , ImportOrgComponent, ListOrgDraffErrorComponent, ConfirmRefusedComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    TreeModule,
    ScrollPanelModule,
    StepsModule,
    OrganizationPlanRoutingModule
  ],
  entryComponents: [
    OrganizationPlanCloneComponent,
    OrganizationPlanFormComponent,
    OrgDraffSelectorModalComponent,
    ViewDetailComponent,
    ListEmployeeModalComponent,
    ImportOrgComponent,
    ListOrgDraffErrorComponent,
    ConfirmRefusedComponent
  ]
})
export class OrganizationPlanModule { }
