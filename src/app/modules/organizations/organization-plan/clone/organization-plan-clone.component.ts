import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from '@app/app.component';
import { OrgPlanService, RESOURCE, ACTION_FORM, SystemParameterService, SYSTEM_PARAMETER_CODE } from '@app/core';
import { ActivatedRoute } from '@angular/router';
import { OrgTreeComponent } from '@app/shared/components/org-tree/org-tree.component';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ValidationService } from '../../../../shared/services/validation.service';

@Component({
  selector: 'organization-plan-clone',
  templateUrl: './organization-plan-clone.component.html',
})
export class OrganizationPlanCloneComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  @ViewChild('tree')
  tree: OrgTreeComponent;
  @Input() item: any;
  orgRootId: string;
  constructor(
    private activatedRoute: ActivatedRoute
  , public activeModal: NgbActiveModal
  , private service: OrgPlanService
  , private app: AppComponent
  , private systemParameterService: SystemParameterService
  ) {
    super(activatedRoute, RESOURCE.ORGANIZATION_PLAN, ACTION_FORM.ORG_PLAN_CLONE);
    this.createForm({});
    // Lay id don vi goc
    this.systemParameterService.getValueByCode(SYSTEM_PARAMETER_CODE.ORGANIZATION_ROOT).subscribe(
      res => this.orgRootId = res.data
    )
  }

  private createForm(formData: any): void {
    this.formSave = this.buildForm(formData, {
      orgPlanId: [''],
      orgParentId: ['', ValidationService.required],
      type: ['', ValidationService.required],
      planName: ['', ValidationService.required],
      effectiveDate: ['', ValidationService.required],
      organizationId: ['', ValidationService.required],
      cloneType: [1, ValidationService.required],
    }, ACTION_FORM.ORG_PLAN_CLONE);
  }
  /**
   * setFormValue
   * param data
   */
  public setFormValue(data: any) {
    this.createForm(data);
  }
  /**
   * controls
   */
  get f () {
    return this.formSave.controls;
  }
  ngOnInit() {
  }

  save() {
    const formSave = {};
    formSave['info'] = this.formSave.value;
    this.app.confirmMessage(null, () => {
      this.service.saveOrUpdateTree(this.f['orgPlanId'].value, formSave)
      .subscribe((res) => {
        if (this.service.requestIsSuccess(res)) {
          this.activeModal.close(res);
        }
      });
    }, null);
  }
}
