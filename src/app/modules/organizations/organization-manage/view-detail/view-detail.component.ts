import { Component, OnInit, HostListener } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { APP_CONSTANTS, LineOrgService, OrganizationService, EmployeeInfoService } from '@app/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'view-detail',
  templateUrl: './view-detail.component.html'
})
export class ViewDetailComponent implements OnInit {
  organizationId: number;
  listExpiredType: Array<Object> = APP_CONSTANTS.ORG_EXPIRED_TYPE;
  listRelationType: Array<Object> = APP_CONSTANTS.ORG_RELATION_TYPE;
  orgInfo = {
    organizationId: '',
    code: '',
    name: '',
    shortName: '',
    orgManager: '',
    orgParent: '',
    lineUnit: '',
    decisionNumber: '',
    decisionSigner: '',
    decisionDate: '',
    totalBoundary: '',
    effectiveDate: '',
    expiredDate: '',
    address: '',
    phoneNumber: '',
    description: '',
  };
  // Animation Variable
  paneDragging = false;
  paneTransform;
  zoom = 1;
  paneX = 0;
  paneY = 0;

  constructor(
    public activeModal: NgbActiveModal,
    private lineOrgService: LineOrgService,
    private empService: EmployeeInfoService,
    private orgService: OrganizationService,
    private sanitizer: DomSanitizer
    ) {
      this.buildOrgInfo({});
  }

  ngOnInit() {
    this.orgService.findOrgMultiLanguageById(this.organizationId)
    .subscribe(res => {
      if (this.orgService.requestIsSuccess(res)) {
        this.buildOrgInfo(res.data);

        if (res.data.lineOrgId) {
          this.lineOrgService.findOne(res.data.lineOrgId)
          .subscribe(result => {
            if (this.lineOrgService.requestIsSuccess(result)) {
              this.orgInfo.lineUnit = result.data.name;
            }
          });
        }

        // Get parent org if orgParentId is not null
        if (res.data.orgParentId) {
          this.orgService.findOne(res.data.orgParentId)
          .subscribe(result => {
            if (this.orgService.requestIsSuccess(result)) {
              this.orgInfo.orgParent = result.data.name;
            }
          });
        }

        // Get employee info if orgManager is not null
        if (res.data.orgManager) {
          this.empService.getEmployeeInfo(res.data.orgManager)
          .subscribe(result => {
            if (result.data) {
              this.orgInfo.orgManager = result.data.fullName;
            }
          });
        }

        // Get total boundary
        this.orgService.getTotalBoundary(this.organizationId)
        .subscribe(result => {
          this.orgInfo.totalBoundary = result.data;
        });

        this.orgService.findOrgAdditionalInfo(this.organizationId)
        .subscribe(result => {
          if (this.orgService.requestIsSuccess(result)) {
            this.orgInfo.phoneNumber = result.data.phoneNumber;
          }
        });
      }
    });
  }

  public buildOrgInfo(data: any) {
    this.orgInfo.organizationId = data.organizationId;
    this.orgInfo.code = data.code;
    this.orgInfo.name = data.name;
    this.orgInfo.shortName = data.shortName;
    this.orgInfo.decisionNumber = data.decisionNumber;
    this.orgInfo.decisionSigner = data.decisionSigner;
    this.orgInfo.decisionDate = data.decisionDate;
    this.orgInfo.effectiveDate = data.effectiveDate;
    this.orgInfo.expiredDate = data.expiredDate;
    this.orgInfo.address = data.address;
    this.orgInfo.phoneNumber = data.phoneNumber;
    this.orgInfo.description = data.description;
  }

  // Host Listener for Browser
  @HostListener('mousewheel', ['$event']) onMouseWheelChrome(event: any) {
    this.onmousewheel(event);
  }

  @HostListener('DOMMouseScroll', ['$event']) onMouseWheelFirefox(event: any) {
    this.onmousewheel(event);
  }

  @HostListener('onmousewheel', ['$event']) onMouseWheelIE(event: any) {
    this.onmousewheel(event);
  }

  // Animation Function
  public onmousewheel(event) {
    let delta;
    event.preventDefault();
    delta = event.detail || event.wheelDelta;
    this.zoom += delta / 1000 / 2;
    this.zoom = Math.min(Math.max(this.zoom, 0.2), 3);
    this.makeTransform();
  }

  public onmousemove(event) {
    if (this.paneDragging) {
        const {movementX, movementY} = event;
        this.paneX += movementX;
        this.paneY += movementY;
        this.makeTransform();
    }
  }

  public onmouseup(event) {
    this.paneDragging = false;
  }

  public preventMouse(event) {
    event.stopPropagation();
  }

  public onmousedown(event) {
    this.paneDragging = true;
  }

  public makeTransform() {
    this.paneTransform = this.sanitizer.bypassSecurityTrustStyle(`translate(${this.paneX}px, ${this.paneY}px) scale(${this.zoom})`);
  }
}
