import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@app/shared';
import { ReactiveFormsModule } from '@angular/forms';
import { AccordionModule } from 'primeng/accordion';

import { OrganizationManageRoutingModule } from './organization-manage-routing.module';
import { OrganizationManageComponent } from './organization-manage.component';
import { OrganizationAddComponent } from './organization-add/organization-add.component';
import { OrganizationSearchComponent } from './organization-search/organization-search.component';
import { AdditionalInfoComponent } from './organization-add/additional-info/additional-info.component';
import { BoundaryComponent } from './organization-add/boundary/boundary.component';
import { ViewDetailComponent } from './view-detail/view-detail.component';
import { TreeViewComponent } from '../organization-tree-detail/tree-view/tree-view.component';
import { ListOrgErrorComponent } from './organization-search/list-org-error/list-org-error.component';
import { ListEmployeeComponent } from './organization-add/boundary/list-employee/list-employee.component';

@NgModule({
  declarations: [
      OrganizationManageComponent,
      OrganizationSearchComponent,
      OrganizationAddComponent,
      AdditionalInfoComponent,
      BoundaryComponent,
      ViewDetailComponent,
      TreeViewComponent,
      ListOrgErrorComponent,
      ListEmployeeComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    AccordionModule,
    OrganizationManageRoutingModule
  ], entryComponents: [ViewDetailComponent, ListEmployeeComponent, ListOrgErrorComponent]
})
export class OrganizationManageModule { }
