import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './../../../app.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslationService } from 'angular-l10n';
import { OrgTreeComponent } from '@app/shared/components/org-tree/org-tree.component';
import { HelperService } from '@app/shared/services/helper.service';
import { OrganizationService, DEFAULT_MODAL_OPTIONS, RESOURCE, ACTION_FORM } from '@app/core';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { ListOrgErrorComponent } from './organization-search/list-org-error/list-org-error.component';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';

@Component({
  selector: 'organization-manage',
  templateUrl: './organization-manage.component.html'
})
export class OrganizationManageComponent extends BaseComponent implements OnInit {
  resultList: any = {};
  currentNode: any;
  @ViewChild('orgTree')
  orgTree: OrgTreeComponent;
  // permissions = CommonUtils.getPermissionByResourceCode(RESOURCE.ORGANIZATION);
  constructor(
     private router: Router
    , private translation: TranslationService
    , private app: AppComponent
    , private organizationService: OrganizationService
    , private helperService: HelperService
    , private modalService: NgbModal
    , public act: ActivatedRoute
  ) {
    super(act, RESOURCE.ORGANIZATION, ACTION_FORM.SEARCH);
  }

  ngOnInit() {
    this.helperService.ORGANIZATION.subscribe(res => {
      if (!res.type) {
        if (this.currentNode) {
          this.currentNode.label = res.name;
          this.currentNode.nodeId = res.organizationId;
          this.currentNode.data = res.organizationId;
          if (this.currentNode.parent.nodeId !== res.orgParentId) {
            this.orgTree.ngOnInit();
          }
        }
      } else if (res.type === 'RELOAD_TREE') {
        this.orgTree.ngOnInit();
      }
    });
  }
  /**
   * treeSelectNode
   * param event
   */
  public treeSelectNode(event) {
    if ( event.node.nodeId > 0 ) {
      if (this.router.url.includes('/search')) {
        this.router.navigate(['/organizations/manage/search', event.node.nodeId]);
      } else {
        this.editNode(event.node);
        // this.router.navigate(['/organizations/manage/edit', event.node.nodeId]);
      }
    } else {
      this.editNode(event.node);
    }
  }
  /**
   * nodeContextMenuBuilder
   * param event
   */
  public nodeContextMenuBuilder(node) {
    this.orgTree.items = [];
    const haveInsertPermission = this.hasPermission('action.insert');
    const haveUpdatePermission = this.hasPermission('action.update');
    const haveDeletePermission = this.hasPermission('action.delete');
    const labelAdd = this.translation.translate('MenuContext.add');
    const labelAddBefore = this.translation.translate('MenuContext.addBefore');
    const labelAddAfter = this.translation.translate('MenuContext.addAfter');
    const labelEdit = this.translation.translate('MenuContext.edit');
    const labelDelete = this.translation.translate('MenuContext.delete');
    const menuAddChild  = { icon: 'fa info fa-plus', command: () => this.addChild(node), label: labelAdd };
    const menuAddBefore = { icon: 'fa info fa-plus', command: () => this.treeAddSibling(node, true), label: labelAddBefore };
    const menuAddAfter =  { icon: 'fa info fa-plus', command: () => this.treeAddSibling(node, false), label: labelAddAfter };
    const menuEdit =      { icon: 'fa info fa-edit', command: () => this.editNode(node), label: labelEdit };
    const menuDelete =    { icon: 'fa danger fa-trash-alt', command: () => this.processRemoveNode(node), label: labelDelete};
    // const menuImport =    { icon: 'fa info fa-trash-alt', command: () => this.processRemoveNode(event.node), label: labelDelete};
    // const menuViewHistory =    { icon: 'fa info fa-trash-alt', command: () => this.processRemoveNode(event.node), label: labelDelete};

    // Trường hợp không có quyền nào
    if (haveInsertPermission === false && haveUpdatePermission === false && haveDeletePermission === false) {
      this.orgTree.items = [];
      return;
    }
    // Trường hợp node là cây đơn vị chưa được lưu và có quyền insert, update, delete
    if (node.nodeId === 0) {
      // this.orgTree.items = [ menuAddBefore, menuAddAfter, menuDelete, menuEdit ];
      if (haveInsertPermission === true) {
        this.orgTree.items = [ menuAddBefore, menuAddAfter ];
      }
      if (haveUpdatePermission === true) {
        this.orgTree.items.push(menuEdit);
      }
      if (haveDeletePermission === true) {
        this.orgTree.items.push(menuDelete);
      }
      return;
    }

    if (haveInsertPermission === true) {
      this.orgTree.items = [ menuAddBefore, menuAddAfter, menuAddChild ];
    }
    if (haveUpdatePermission === true) {
      this.orgTree.items.push(menuEdit);
    }
    if (haveDeletePermission === true && this.checkOrganizationEffect(node)) {
      this.orgTree.items.push(menuDelete);
    }
  }
  /**
   * nodeContextMenuBuilder2 su dung cho cay don vi nganh doc
   * param event
   */
  public nodeContextMenuBuilder2(node) {
    this.orgTree.items = [];
    const haveInsertPermission = this.hasPermission('action.insert');
    const haveUpdatePermission = this.hasPermission('action.update');
    const haveDeletePermission = this.hasPermission('action.delete');
    const labelEdit = this.translation.translate('MenuContext.edit');
    const labelDelete = this.translation.translate('MenuContext.delete');
    const menuEdit =      { icon: 'fa info fa-edit', command: () => this.editNode(node), label: labelEdit };
    // const menuDelete =    { icon: 'fa danger fa-trash-alt', command: () => this.processRemoveNode(node), label: labelDelete};

    // Trường hợp không có quyền nào
    if (haveInsertPermission === false && haveUpdatePermission === false && haveDeletePermission === false) {
      this.orgTree.items = [];
      return;
    }
    // Trường hợp node là cây đơn vị chưa được lưu và có quyền insert, update, delete
    if (node.nodeId === 0) {
      if (haveUpdatePermission === true) {
        this.orgTree.items.push(menuEdit);
      }
      // if (haveDeletePermission === true) {
      //   this.orgTree.items.push(menuDelete);
      // }
      return;
    }

    if (haveUpdatePermission === true) {
      this.orgTree.items.push(menuEdit);
    }
    // if (haveDeletePermission === true && this.checkOrganizationEffect(node)) {
    //   this.orgTree.items.push(menuDelete);
    // }
  }
  /**
   * getDefaultNode
   * param parentNode
   */
  private getDefaultNode (parentNode) {
    return {
        data: '0'
      , label: 'New Node'
      , leaf: true
      , icon: 'glyphicons glyphicons-list-alt'
      , nodeId: 0
      , parentId: parentNode ? parentNode.nodeId : ''
    };
  }
  /**
   * addchild
   * @ param event
   * @ param type
   * @ param isDefault
   */
  private addChild(parentNode: any) {
    const childNode = this.getDefaultNode(parentNode);
    parentNode.children = parentNode.children || [];
    if (parentNode.leaf) {
      parentNode.expanded = true;
      parentNode.children.push(childNode);
    } else {
      this.orgTree.actionLazyReadWhenAddChild(parentNode, childNode);
    }

  }

  /**
   * treeAddSibling
   * param currentNode
   * param isBefore
   */
  private treeAddSibling(currentNode, isBefore) {
    const parentNode = currentNode.parent;
    let children;
    const newNode = this.getDefaultNode(parentNode);
    if (parentNode) {
      children = parentNode.children;
    } else {
      children = this.orgTree.nodes;
    }
    const index = children.indexOf(currentNode);
    if (isBefore) {
      children.splice(index, 0, newNode);
    } else {
      if (index < children.length - 1) {
        children.splice(index + 1, 0, newNode);
      } else {
        children.push(newNode);
      }
    }
  }
  /**
   * onEditNode
   * @ param currentNode
   */
  private editNode(currentNode) {
    this.currentNode = currentNode;
    const params = {
      name: currentNode.label,
      orgParentId: currentNode.parentId,
    };
    if (currentNode.nodeId > 0) {
      this.router.navigate(['/organizations/manage/edit', currentNode.nodeId]);
    } else {
      this.router.navigate(['/organizations/manage/edit', currentNode.nodeId], { queryParams: params });
    }
  }
  /**
   * Xử lý khi xóa node
   * param node
   */
  private processRemoveNode(currentNode) {
		console.log('TCL: OrganizationManageComponent -> privateprocessRemoveNode -> currentNode', currentNode);
    if (currentNode.nodeId === 0) {
      const parentNode = currentNode.parent;
      let children;
      if (parentNode) {
        children = parentNode.children || [];
      } else {
        children = this.orgTree.nodes;
      }
      const index = children.indexOf(currentNode);
      children.splice(index, 1);
      return;
    } else {
      this.app.confirmDelete(null, () => {// on accepted
        this.organizationService.deleteById(currentNode.data)
          .subscribe(res => {
            if (this.organizationService.requestIsSuccess(res)) {
              this.helperService.reloadTreeOrganization(res);
              this.helperService.reloadGridOrganization(res);
              this.router.navigate(['/organizations/manage/search']);
            } else if (this.organizationService.requestIsConfirm(res)) {
              const showListOrgError = () => {
                const modalRef = this.modalService.open(ListOrgErrorComponent, DEFAULT_MODAL_OPTIONS);
                modalRef.componentInstance.data = res.data;
              };
              eval(res.message);
            }
          });
        }, null);
    }
  }

  public checkOrganizationEffect(item): boolean {
    const today = new Date();
    const day = today.getDate(), month = today.getMonth() + 1, year = today.getFullYear();
    const currentDate = new Date(month + '/' + day + '/' + year).getTime();
    const startDate = item.effectiveDate || new Date(month + '/' + day + '/' + year).getTime();
    const endDate = item.expiredDate || new Date(month + '/' + day + '/' + year).getTime();
    return startDate <= currentDate && currentDate <= endDate;
  }

  // private hasPermission(operationKey: string): boolean {
  //   if (!this.permissions || this.permissions.length <= 0) {
  //     return false;
  //   }
  //   const rsSearch = this.permissions.findIndex(x => x.operationCode === CommonUtils.getPermissionCode(operationKey));
  //   if (rsSearch < 0) {
  //     return false;
  //   }
  //   return true;
  // }
}
