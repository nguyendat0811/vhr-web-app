import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'list-org-error',
  templateUrl: './list-org-error.component.html'
})
export class ListOrgErrorComponent implements OnInit {
  data: any;
  constructor(
    public activeModal: NgbActiveModal
  ) { }

  ngOnInit() {
  }

}
