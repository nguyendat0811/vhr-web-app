import { ViewDetailComponent } from './../view-detail/view-detail.component';
import { ActivatedRoute } from '@angular/router';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Validators } from '@angular/forms';
import { OrganizationService, RESOURCE, ACTION_FORM, DEFAULT_MODAL_OPTIONS } from '@app/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ValidationService } from '@app/shared/services/validation.service';
import { Router } from '@angular/router';
import { AppComponent } from '@app/app.component';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { HelperService } from '@app/shared/services/helper.service';
import { ListOrgErrorComponent } from './list-org-error/list-org-error.component';

@Component({
  selector: 'organization-search',
  templateUrl: './organization-search.component.html'
})
export class OrganizationSearchComponent extends BaseComponent implements OnInit {
  resultList: any = {};
  @ViewChild('pTable') dataTable: any;

  formConfig = {
    code: ['', [ValidationService.maxLength(50)]],
    name: ['', [ValidationService.maxLength(500)]],
    status: [''],
    path: ['']
  };
  constructor(
    public actr: ActivatedRoute,
    private router: Router,
    private organizationService: OrganizationService,
    private activatedRoute: ActivatedRoute,
    private modalService: NgbModal,
    private app: AppComponent,
    private helperService: HelperService
    ) {
    super(actr, RESOURCE.ORGANIZATION, ACTION_FORM.SEARCH);
    this.formSearch = this.buildForm({ status: -1 }, this.formConfig, ACTION_FORM.SEARCH);
    this.activatedRoute.params.subscribe(params => {
      if (params && params.orgId) {
        this.formSearch.controls['path'].setValue(`/${params.orgId}/`);
      }
      this.processSearch();
    });

    this.helperService.ORGANIZATION.subscribe(res => {
      if (res.type && res.type === 'RELOAD_GRID') {
        this.processSearch();
      }
    });
  }

  ngOnInit() {
    // this.processSearch();
  }
  get f () {
    return this.formSearch.controls;
  }

  // Tìm kiếm organization
  processSearch(event?) {
    if (CommonUtils.isValidForm(this.formSearch)) {
      if (!CommonUtils.isValidForm(this.formSearch)) {
        return;
      }
      this.organizationService.search(this.formSearch.value, event).subscribe(res => {
        this.resultList = res;
      });

      if (!event) {
        if (this.dataTable) {
          this.dataTable.first = 0;
        }
      }
    }
  }

  /**
   * prepareDelete
   * param item
   */
  processDelete(item) {
    if (item && item.organizationId > 0) {
      this.app.confirmDelete(null, () => {// on accepted
        this.organizationService.deleteById(item.organizationId)
          .subscribe(res => {
            if (this.organizationService.requestIsSuccess(res)) {
              this.helperService.reloadTreeOrganization(res);
              this.processSearch();
            } else if (this.organizationService.requestIsConfirm(res)) {
              const showListOrgError = () => {
                const modalRef = this.modalService.open(ListOrgErrorComponent, DEFAULT_MODAL_OPTIONS);
                modalRef.componentInstance.data = res.data;
              };
              eval(res.message);
            }
          });
        }, null);
    }
  }

  processViewDetail(organizationId: number) {
    const modalRef = this.modalService.open(ViewDetailComponent, {
      size: 'lg',
      keyboard : false,
      backdrop: true,
    });
    modalRef.componentInstance.organizationId = organizationId;

    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
    });
  }

  edit(item) {
    this.router.navigate(['/organizations/manage/edit', item.organizationId]);
  }

  public checkOrganizationEffect(item): boolean {
    const today = new Date();
    const day = today.getDate(), month = today.getMonth() + 1, year = today.getFullYear();
    const currentDate = new Date(month + '/' + day + '/' + year).getTime();
    const startDate = item.effectiveDate || new Date(month + '/' + day + '/' + year).getTime();
    const endDate = item.expiredDate || new Date(month + '/' + day + '/' + year).getTime();
    return startDate <= currentDate && currentDate <= endDate;
  }
}
