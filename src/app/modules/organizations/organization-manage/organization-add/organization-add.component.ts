import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { AdditionalInfoComponent } from './additional-info/additional-info.component';
import { APP_CONSTANTS } from '@app/core/app-config';
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, Validators, FormArray } from '@angular/forms';
import { OrganizationService, LineOrgService, OrganizationTypeService, RESOURCE, ACTION_FORM } from '@app/core';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { ValidationService } from '@app/shared/services';
import { AppComponent } from '@app/app.component';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { OrgSelectorComponent } from '@app/shared/components/org-selector/org-selector.component';
import { HelperService } from '@app/shared/services/helper.service';
import { FileControl } from '@app/core/models/file.control';
import { BoundaryComponent } from './boundary/boundary.component';
import { AccordionTab } from 'primeng/accordion';

@Component({
  selector: 'organization-add',
  templateUrl: './organization-add.component.html'
})
export class OrganizationAddComponent extends BaseComponent implements OnInit, OnDestroy {
  isHidden: boolean;
  isDisabled = false;
  isExpanedFormInfo = true;
  isExpandedFormRelation = true;
  lineOrgList: any;
  listOrgType: any = {};
  resultList: any = {};
  formInfo: FormGroup;
  formOrgRelation: FormArray;
  listExpiredType: Array<Object> = APP_CONSTANTS.ORG_EXPIRED_TYPE;
  listRelationType: Array<Object> = APP_CONSTANTS.ORG_RELATION_TYPE;
  codeFieldReadOnly = true;
  // ... your class variables here
  navigationSubscription;
  @ViewChild('orgParentId') vcOrgParentId: OrgSelectorComponent;
  @ViewChild('additionalInfo') additionalInfoCom: AdditionalInfoComponent;
  @ViewChild('boundary') boundaryCom: BoundaryComponent;
  @ViewChild('commonInfoTab') commonInfoTab: AccordionTab;
  @ViewChild('relationTab') relationTab: AccordionTab;

  itemsBreadcrumb = [];

  formInfoConfig = {
    organizationId: [null],
    isOrgLevelManage: [''],
    orgOrder: ['',
      [
        ValidationService.positiveInteger,
        Validators.min(1),
        Validators.max(2147483648),
        ValidationService.maxLength(10)]
      ],
    orgParentId: ['', ValidationService.required],
    orgTypeId: ['', ValidationService.required],
    code: ['',
      [
        ValidationService.required,
        ValidationService.maxLength(50)
      ]],
    name: ['', [ ValidationService.required, ValidationService.maxLength(500)]],
    shortName: ['', [ ValidationService.maxLength(500)]],
    decisionNumber: ['', [ ValidationService.maxLength(500)]],
    decisionDate: [null],
    orgDecision: ['', [ ValidationService.maxLength(50)]],
    decisionSigner: ['', [ ValidationService.maxLength(50)]],
    orgManager: [''],
    lineOrgId: [''],
    address: ['', [ ValidationService.maxLength(500) ]],
    effectiveDate: ['', ValidationService.required],
    expiredDate: ['', [ ValidationService.beforeCurrentDate ]],
    expiredDecision: ['', [ ValidationService.maxLength(100)]],
    expiredType: [''],
    expiredDescription: ['', [ ValidationService.maxLength(200) ]],
    description: ['', [ ValidationService.maxLength(200) ]],
    establishReason: ['', [ ValidationService.maxLength(500) ]]
  };

  formOrgRelationConfig = {
    orgRelationId: [''],
    relativeOrganizationId: [''],
    relationTypeId: [''],
    eventDate: ['', [ ValidationService.beforeCurrentDate ]],
    description: ['', [ ValidationService.maxLength(200)]],
  };

  constructor(
    private router: Router,
    private app: AppComponent,
    private helperService: HelperService,
    private lineOrgService: LineOrgService,
    private orgTypeService: OrganizationTypeService,
    private activatedRoute: ActivatedRoute,
    private organizationService: OrganizationService,
  ) {
    super(activatedRoute, RESOURCE.ORGANIZATION, ACTION_FORM.ORG_COMMON_INFO_INSERT);
    this.navigationSubscription = this.router.events.subscribe((event: any) => {
      // If it is a NavigationEnd event re-initalise the component
      if (event instanceof NavigationEnd) {
        this.ngOnInit();
      }
    });
  }

  ngOnInit() {
    this.formInfo = this.buildForm({}, this.formInfoConfig, ACTION_FORM.ORG_COMMON_INFO_INSERT,
      [ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.organization.expiredDate')]);
    this.formInfo.controls['isOrgLevelManage'].setValue(1);
    this.buildFormOrgRelation({});
    const params = this.activatedRoute.snapshot.params;
    if (params && CommonUtils.isValidId(params.orgId)) {
      this.formInfo.controls['organizationId'].setValue(params.orgId);
    }
    const queryParams = this.activatedRoute.snapshot.queryParams;
    if (queryParams && queryParams.name) {
      this.formInfo.controls['name'].setValue(queryParams.name);
      this.formInfo.controls['orgParentId'].setValue(queryParams.orgParentId);
    }
    this.onParamsChange();
    this.checkIsDisabled();
  }

  ngOnDestroy() {
    // avoid memory leaks here by cleaning up after ourselves. If we
    // don't then we will continue to run our initialiseInvites()
    // method on every navigationEnd event.
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }
  }

  canReuse(): boolean {
    return false;
  }

  get f() {
    return this.formInfo.controls;
  }

  private onParamsChange() {
    this.orgTypeService.getListOrgType().subscribe(res => {
      this.listOrgType = res;
    });
    this.lineOrgService.getListLineCareer().subscribe(res => {
      this.lineOrgList = res.data;
    });
    this.prepareInsertOrUpdate();
    this.createBreadcrumb();
    this.loadHistoryByOrgId();
    this.isHidden = true;
  }

  public prepareInsertOrUpdate() {
    if (!CommonUtils.isValidId(this.formInfo.controls['organizationId'].value)) {
      this.formInfo = this.buildForm(
        {
          isOrgLevelManage: 1,
          orgParentId: this.formInfo.controls['orgParentId'].value
        },
        this.formInfoConfig, ACTION_FORM.ORG_COMMON_INFO_INSERT,
        [ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.organization.expiredDate')]
      );
      this.showHideCodeField(this.formInfo.get('isOrgLevelManage').value);
      this.formInfo.get('isOrgLevelManage').valueChanges.subscribe(
        (value) => {
          this.showHideCodeField(value);
        }
      );
      return;
    }
    // Load common info
    this.organizationService.findOne(this.formInfo.controls['organizationId'].value)
      .subscribe(res => {
        if (this.organizationService.requestIsSuccess(res)) {
          this.formInfo = this.buildForm(res.data, this.formInfoConfig,
            ACTION_FORM.ORG_COMMON_INFO_UPDATE,
            [ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.organization.expiredDate')]);
          this.showHideCodeField(this.formInfo.get('isOrgLevelManage').value);
          this.formInfo.get('isOrgLevelManage').valueChanges.subscribe(
            (value) => {
              this.showHideCodeField(value);
            }
          );
        }
      });
    // Load data to list history of establistment
    this.loadHistoryByOrgId();
  }

  private buildFormOrgRelation(listOrgRelation: any) {
    if (!listOrgRelation) {
      this.formOrgRelation = new FormArray([]);
    } else {
      const controls = new FormArray([]);
      for (const i in listOrgRelation) {
        const orgRelation = listOrgRelation[i];
        const group = this.makeDefaultRalationForm();
        group.patchValue(orgRelation);
        if (orgRelation.fileAttachment) {
          (group.controls['file'] as FileControl).setFileAttachment(orgRelation.fileAttachment.relationFile);
        }
        controls.push(group);
      }
      this.formOrgRelation = controls;
    }
  }

  processSaveOrUpdate() {
    if (!this.validateBeforeSave()) {
      return;
    }

    this.app.confirmMessage(null, () => {// on accepted
      const formSave = {};
      formSave['info'] = this.formInfo.value;
      formSave['lstOrgRelation'] = this.formOrgRelation.value;
      this.organizationService.saveOrUpdateFormFile(formSave)
        .subscribe(res => {
          if (this.organizationService.requestIsSuccess(res)) {
            this.helperService.afterSaveOrganization(res.data);
            // vao trang edit
            this.router.navigate(['/organizations/manage/edit', res.data.organizationId]);
            // set id don vi vao form sau khi luu
            // this.formInfo.controls['organizationId'].setValue(res.data.organizationId);
            // this.prepareInsertOrUpdate();
          }
        });
    }, () => {
      // on rejected
    });
  }

  private validateBeforeSave(): boolean {
    if (!CommonUtils.isValidForm(this.formInfo)) {
      this.commonInfoTab.selected = true;
    }
    if (!CommonUtils.isValidForm(this.formOrgRelation)) {
      this.relationTab.selected = true;
    }
    const formInfo = CommonUtils.isValidForm(this.formInfo);
    const formOrgRelation = CommonUtils.isValidForm(this.formOrgRelation);
    return formInfo && formOrgRelation;
  }

  // If click on clear ExpiredDate button
  public clearExpiredDate() {
    this.formInfo.controls['expiredDate'].setValue(null);
    this.formInfo.controls['expiredType'].setValue('');
    this.formInfo.controls['expiredDecision'].setValue('');
    this.formInfo.controls['expiredDescription'].setValue('');
    this.isHidden = true;
    this.checkIsDisabled();
    console.log('ExpiredDate', this.formInfo.controls['expiredDate'].value);
  }

  public checkIsDisabled(): void {
    const expiredDate = this.formInfo.controls['expiredDate'].value;
    this.isDisabled = CommonUtils.isNullOrEmpty(expiredDate);
  }

  // xử lý load danh sách history
  loadHistoryByOrgId() {
    this.organizationService.findOrgRelationId(this.f['organizationId'].value)
    .subscribe((res) => {
      this.buildFormOrgRelation(res);
    });
  }

  /**
   * makeDefaultRalationForm
   */
  private makeDefaultRalationForm(): FormGroup {
    const formGroup = this.buildForm({}, this.formOrgRelationConfig, ACTION_FORM.ORG_RELATION_UPDATE);
    formGroup.addControl('file', new FileControl(null));
    return formGroup;
  }

  /**
   * addOrgRelation
   * param index
   * param item
   */
  public addOrgRelation(index: number, item: FormGroup) {
    const controls = this.formOrgRelation as FormArray;
    controls.insert(index + 1, this.makeDefaultRalationForm());
  }

  /**
   * removeOrgRelation
   * param index
   * param item
   */
  public removeOrgRelation(index: number, item: FormGroup) {
    const controls = this.formOrgRelation as FormArray;
    if (controls.length === 1) {
      this.buildFormOrgRelation({});
      const group = this.makeDefaultRalationForm();
      controls.push(group);
      this.formOrgRelation = controls;
    }
    controls.removeAt(index);
  }

  private createBreadcrumb() {
    const orgId = this.formInfo.controls['organizationId'].value || this.formInfo.controls['orgParentId'].value;
    const mediateItem = {
      label: '...',
      title: ''
    };
    this.organizationService.findAllParrent(orgId).subscribe(
      res => {
        this.itemsBreadcrumb = [];
        const listOrg = res.data;
        if (!listOrg || listOrg.length === 0) {
          return;
        }
        if (this.formInfo.controls['organizationId'].value) {
          if (listOrg.length > 3) {
            this.itemsBreadcrumb.push({label: listOrg[0].name, title: listOrg[0].name});
            for (let i = 1; i <= listOrg.length - 3; i++) {
              if (i === listOrg.length - 3) {
                mediateItem.title = mediateItem.title + listOrg[i].name;
              } else {
                mediateItem.title = mediateItem.title + listOrg[i].name + ' > ';
              }
            }
            this.itemsBreadcrumb.push(mediateItem);
            this.itemsBreadcrumb.push({label: listOrg[listOrg.length - 2].name, title: listOrg[listOrg.length - 2].name});
            this.itemsBreadcrumb.push({label: listOrg[listOrg.length - 1].name, title: listOrg[listOrg.length - 1].name});
          } else {
            for (const obj of listOrg) {
              this.itemsBreadcrumb.push({label: obj.name, title: obj.name});
            }
          }
        } else {
          if (listOrg.length > 2) {
            this.itemsBreadcrumb.push({label: listOrg[0].name, title: listOrg[0].name});
            for (let i = 1; i <= listOrg.length - 2; i++) {
              if (i === listOrg.length - 2) {
                mediateItem.title = mediateItem.title + listOrg[i].name;
              } else {
                mediateItem.title = mediateItem.title + listOrg[i].name + ' > ';
              }
            }
            this.itemsBreadcrumb.push({label: listOrg[listOrg.length - 1].name, title: listOrg[listOrg.length - 1].name});
          } else {
            for (const obj of listOrg) {
              this.itemsBreadcrumb.push({label: obj.name, title: obj.name});
            }
          }
          this.itemsBreadcrumb.push({label: this.formInfo.controls['name'].value, title: this.formInfo.controls['name'].value});
        }
      }
    );
  }

  private showHideCodeField(isOrgLevelManage) {
    this.codeFieldReadOnly = isOrgLevelManage;
    const codeDefault = this.formInfo.get('code').value;
    this.formInfo.removeControl('code');
    if (isOrgLevelManage !== 1) {
      this.formInfo.addControl('code', CommonUtils.createControl(this.actionForm, 'code', codeDefault,
      [ValidationService.required, ValidationService.maxLength(50)], this.propertyConfigs));
    } else {
      this.formInfo.addControl('code', CommonUtils.createControl(this.actionForm, 'code', codeDefault,
      [ValidationService.maxLength(50)], this.propertyConfigs));
    }
  }
}

