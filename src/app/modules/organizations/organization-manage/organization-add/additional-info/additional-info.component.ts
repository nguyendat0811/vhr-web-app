import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { APP_CONSTANTS, RESOURCE, ACTION_FORM } from '@app/core/app-config';
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, Validators, FormArray } from '@angular/forms';
import { ValidationService } from '@app/shared/services';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { AppComponent } from '@app/app.component';
import { NationService, SysCatService, LocationService, OrganizationService, OrgDutyService, NationConfigService } from '@app/core';
import { FileControl } from '@app/core/models/file.control';
import { AccordionTab } from 'primeng/accordion';

@Component({
  selector: 'additional-info',
  templateUrl: './additional-info.component.html'
})
export class AdditionalInfoComponent extends BaseComponent implements OnInit, OnDestroy {
  orgId: number;
  formAdditionalInfo: FormGroup;
  formOrgDuty: FormGroup;
  listNation: any;
  listBank: any;
  listLocation: any;
  listGender = APP_CONSTANTS.GENDERS;
  listStatus = APP_CONSTANTS.STATUS;
  formOrgBusiness: FormArray;
  formOrgBusinessDelete = [];
  formOrgDocument: FormArray;
  formOrgDocumentDelete = [];
  navigationSubscription;
  @ViewChild('additionalInfoTab') additionalInfoTab: AccordionTab;
  @ViewChild('orgDutyTab') orgDutyTab: AccordionTab;
  @ViewChild('orgBusinessTab') orgBusinessTab: AccordionTab;
  @ViewChild('orgDocumentTab') orgDocumentTab: AccordionTab;

  formAdditionalInfoConfig = {
    organizationId: [''],
    nationId: [''],
    locationId: [''],
    bankAccount: ['', [ ValidationService.maxLength(50)]],
    bankId: [''],
    bankBranch: ['', [ ValidationService.maxLength(50) ]],
    phoneNumber: ['', [ ValidationService.maxLength(50), ValidationService.mobileNumber ]],
    isOrgSalary: [null],
    isOrgRecruitment: [null],
    isOrgSign: [null],
    isOrgModerator: [null],
    isOrgTax: [null],
    isOrgInsurance: [null],
    isOrgReward: [null]
  };

  formOrgDutyConfig = {
    orgDutyId: [''],
    organizationId: [''],
    effectiveDate: [null, [ ValidationService.required ]],
    target: ['', [ ValidationService.maxLength(1000) ]],
    mechanism: ['', [ ValidationService.maxLength(1000) ]],
    mainDuty: ['', [ ValidationService.maxLength(1000) ]],
    jurisdiction: ['', [ ValidationService.maxLength(1000) ]],
    responsibility: ['', [ ValidationService.maxLength(1000) ]],
    innerRelationship: ['', [ ValidationService.maxLength(1000) ]],
    outerRelationship: ['', [ ValidationService.maxLength(1000) ]]
  };

  formOrgBusinessConfig = {
    orgBusinessRegistrationId: [null],
    organizationId: [null],
    issuedDate: [null],
    address: [null, ValidationService.maxLength(1000)],
    leaderName: [null, ValidationService.maxLength(200)],
    leaderGender: [null],
    leaderBirthDay: [null, [ ValidationService.beforeCurrentDate ]],
    reason: [null, ValidationService.maxLength(500)],
    documentNumber: [null, ValidationService.maxLength(100)],
    signedBy: [null, ValidationService.maxLength(100)],
    orgSigned: [null, ValidationService.maxLength(200)]
  };

  formOrgDocumentConfig = {
    orgDocumentManagerId: [null],
    organizationId: [null],
    orgFileManagerId: [null],
    effectiveDate: [null],
    expiredDate: [null],
    employeeId: [null],
    status: ['']
  };

  constructor(
    private nationService: NationService,
    private sysCatService: SysCatService,
    private locationService: LocationService,
    private activatedRoute: ActivatedRoute,
    private organizationService: OrganizationService,
    private orgDutyService: OrgDutyService,
    private app: AppComponent,
    private router: Router,
    private nationConfigService: NationConfigService
  ) {
    super(activatedRoute, RESOURCE.ORGANIZATION, ACTION_FORM.ORG_ADDITIONAL_INFO_UPDATE);
    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      if (e instanceof NavigationEnd) {
        this.ngOnInit();
      }
    });
  }

  ngOnInit() {
    const params = this.activatedRoute.snapshot.params;
    if (params && CommonUtils.isValidId(params.orgId)) {
      this.orgId = params.orgId;
    }

    this.buildFormAdditionalInfo({});
    this.buildFormOrgDuty({});
    this.buildFormOrgBusiness({});
    this.buildFormOrgDocument({});
    this.loadDataToForm();
  }

  ngOnDestroy() {
    // avoid memory leaks here by cleaning up after ourselves. If we
    // don't then we will continue to run our initialiseInvites()
    // method on every navigationEnd event.
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }
  }

  get f () {
    return this.formAdditionalInfo.controls;
  }

  get fDuty () {
    return this.formOrgDuty.controls;
  }

  private loadDataToForm() {
    this.nationService.getNationList().subscribe(res => {
      this.listNation = res.data;
    });
    // this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.BANK_CODE).subscribe(res => {
    //   this.listBank = res.data;
    // });
    this.prepareInsertOrUpdate();
  }

  public prepareInsertOrUpdate() {
    // Load data to form other info
    this.organizationService.findOrgAdditionalInfo(this.orgId)
    .subscribe(res => {
      if (this.organizationService.requestIsSuccess(res)) {
        this.buildFormAdditionalInfo(res.data);
        this.getProvincesByNationId();
        this.getBanksByNationId();
      }
    });

    // Load data to orgDuty
    this.orgDutyService.findOrgDuty(this.orgId)
    .subscribe(res => {
      if (res.data != null) {
        this.buildFormOrgDuty(res.data);
        if (res.data.fileAttachment) {
          (this.formOrgDuty.controls['orgDutyFile'] as FileControl).setFileAttachment(res.data.fileAttachment.orgDutyFile);
        }
      }
    });

    // Load list business registration to table
    this.organizationService.findOrgBusiness(this.orgId)
    .subscribe(res => {
      this.buildFormOrgBusiness(res);
    });

    // Load list org document manager to table
    this.organizationService.findOrgDocument(this.orgId)
    .subscribe(res => {
      this.buildFormOrgDocument(res);
    });
  }

  buildFormAdditionalInfo(formData: any) {
    this.formAdditionalInfo = this.buildForm(
      formData, this.formAdditionalInfoConfig, ACTION_FORM.ORG_ADDITIONAL_INFO_UPDATE
    );
  }

  buildFormOrgDuty(formData: any) {
    this.formOrgDuty = this.buildForm(formData, this.formOrgDutyConfig, ACTION_FORM.ORG_DUTY_UPDATE);
    this.formOrgDuty.addControl('orgDutyFile', new FileControl(null));
  }

  /**
   * makeBusinessRegistrationForm
   */
  private makeOrgBusinessForm(): FormGroup {
    const formGroup = this.buildForm({}, this.formOrgBusinessConfig, ACTION_FORM.ORG_BUSINESS_REGIS_UPDATE);
    formGroup.addControl('orgBusinessFile', new FileControl(null));
    return formGroup;
  }

  buildFormOrgBusiness(listOrgBusiness: any) {
    if (!listOrgBusiness) {
      this.formOrgBusiness = new FormArray([]);
    } else {
      const controls = new FormArray([]);
      for (const i in listOrgBusiness) {
        const orgBusiness = listOrgBusiness[i];
        const group = this.makeOrgBusinessForm();
        group.patchValue(orgBusiness);
        if (orgBusiness.fileAttachment) {
          (group.controls['orgBusinessFile'] as FileControl).setFileAttachment(orgBusiness.fileAttachment.orgBusinessFile);
        }
        controls.push(group);
      }
      this.formOrgBusiness = controls;
    }
  }

  buildFormOrgDocument(listOrgDocument: any) {
    if (!listOrgDocument) {
      this.formOrgDocument = new FormArray([]);
    } else {
      const controls = new FormArray([]);
      for (const i in listOrgDocument) {
        const orgDocument = listOrgDocument[i];
        const group = this.buildForm({}, this.formOrgDocumentConfig, ACTION_FORM.ORG_DOCUMENT_UPDATE,
          [ ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.organization.expiredDate') ]);
        group.patchValue(orgDocument);
        controls.push(group);
      }
      this.formOrgDocument = controls;
    }
  }

  getProvincesByNationId() {
    if (this.formAdditionalInfo.controls['nationId'].value) {
      this.locationService.getProvincesByNationId(this.formAdditionalInfo.controls['nationId'].value)
      .subscribe(res => {
        this.listLocation = res;
        if (this.listLocation.length === 0) {
          this.formAdditionalInfo.controls['locationId'].setValue('');
        }
      });
    } else {
      this.listLocation = [];
      this.formAdditionalInfo.controls['locationId'].setValue('');
    }
  }

  getBanksByNationId() {
    const nationId = this.formAdditionalInfo.controls['nationId'].value;
    if (nationId) {
      this.nationConfigService.getNationConfigList(APP_CONSTANTS.NATION_CONFIG_TYPE.BANK, nationId)
      .subscribe(res => {
        this.listBank = [];
        for (const item of res.data) {
          this.listBank.push({label: item.name, value: item.nationConfigId});
        }
        if (this.listBank.length === 0) {
          this.formAdditionalInfo.controls['bankId'].setValue('');
        }
      });
    } else {
      this.listBank = [];
      this.formAdditionalInfo.controls['bankId'].setValue('');
    }
  }

  processSaveOrUpdate() {
    if (!this.validateBeforeSave()) {
      return;
    }

    this.app.confirmMessage(null, () => { // on accepted
        const formSave = {};
        formSave['otherInfo'] = this.formAdditionalInfo.value;
        formSave['orgDutyInfo'] = this.formOrgDuty.value;
        formSave['lstOrgBusinessRegistration'] = this.formOrgBusiness.value;
        formSave['lstOrgBusinessRegistrationDelete'] = this.formOrgBusinessDelete;
        formSave['lstOrgDocument'] = this.formOrgDocument.value;
        formSave['lstOrgDocumentDelete'] = this.formOrgDocumentDelete;
        this.organizationService.saveOrUpdateOtherInfo(formSave, this.formAdditionalInfo.controls['organizationId'].value)
        .subscribe(res => {
          if (this.organizationService.requestIsSuccess(res)) {
            this.prepareInsertOrUpdate();
          }
        });
      }, () => {
      // on rejected
    });
  }

  private validateBeforeSave(): boolean {
    if (!CommonUtils.isValidForm(this.formAdditionalInfo)) {
      this.additionalInfoTab.selected = true;
    }
    if (!CommonUtils.isValidForm(this.formOrgDuty)) {
      this.orgDutyTab.selected = true;
    }
    if (!CommonUtils.isValidForm(this.formOrgBusiness)) {
      this.orgBusinessTab.selected = true;
    }
    if (!CommonUtils.isValidForm(this.formOrgDocument)) {
      this.orgDocumentTab.selected = true;
    }
    const formAdditionalInfo = CommonUtils.isValidForm(this.formAdditionalInfo);
    const formOrgDuty = CommonUtils.isValidForm(this.formOrgDuty);
    const formOrgBusiness = CommonUtils.isValidForm(this.formOrgBusiness);
    const formOrgDocument = CommonUtils.isValidForm(this.formOrgDocument);
    return formAdditionalInfo && formOrgDuty && formOrgBusiness && formOrgDocument;
  }

  /**
   * addOrgBusiness
   * param index
   * param item
   */
  public addOrgBusiness(index: number, item: FormGroup) {
    const controls = this.formOrgBusiness as FormArray;
    controls.insert(index + 1, this.makeOrgBusinessForm());
  }

  /**
   * param index
   * param item
   */
  public removeOrgBusiness(index: number, item: FormGroup) {
    const controls = this.formOrgBusiness as FormArray;
    if (controls.length === 1) {
      this.buildFormOrgBusiness({});
      const group = this.makeOrgBusinessForm();
      controls.push(group);
      this.formOrgBusiness = controls;
    }
    const orgBusinessRegistrationId = item.get('orgBusinessRegistrationId').value;
    if (orgBusinessRegistrationId) {
      this.formOrgBusinessDelete.push(orgBusinessRegistrationId);
    }
    controls.removeAt(index);
  }

  /**
   * addOrgDocument
   * param index
   * param item
   */
  public addOrgDocument(index: number, item: FormGroup) {
    const controls = this.formOrgDocument as FormArray;
    controls.insert(index + 1, this.buildForm({}, this.formOrgDocumentConfig, ACTION_FORM.ORG_DOCUMENT_UPDATE,
      [ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.organization.expiredDate')]));
  }

  /**
   * removeOrgDocument
   * param index
   * param item
   */
  public removeOrgDocument(index: number, item: FormGroup) {
    const controls = this.formOrgDocument as FormArray;
    if (controls.length === 1) {
      this.buildFormOrgDocument({});
      const group = this.buildForm({}, this.formOrgDocumentConfig, ACTION_FORM.ORG_DOCUMENT_UPDATE,
        [ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.organization.expiredDate')]);
      controls.push(group);
      this.formOrgDocument = controls;
    }
    const orgDocumentManagerId = item.get('orgDocumentManagerId').value;
    if (orgDocumentManagerId) {
      this.formOrgDocumentDelete.push(orgDocumentManagerId);
    }
    controls.removeAt(index);
  }
}

