import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { FormGroup, FormArray, Validators, FormControl } from '@angular/forms';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { RESOURCE, ACTION_FORM } from '@app/core/app-config';
import { OrgBoundaryService } from '@app/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ValidationService } from '@app/shared/services';
import { DataPickerModalComponent } from '@app/shared/components/data-picker/data-picker-modal/data-picker-modal.component';
import { AppComponent } from '@app/app.component';
import { ListEmployeeComponent } from './list-employee/list-employee.component';

@Component({
  selector: 'boundary',
  templateUrl: './boundary.component.html'
})
export class BoundaryComponent extends BaseComponent implements OnInit, OnDestroy {
  orgId: number;
  formOrgBoundary: FormArray;
  listOrgBoundary = [];
  navigationSubscription;
  constructor(
    private modalService: NgbModal,
    private activatedRoute: ActivatedRoute,
    private orgBoundaryService: OrgBoundaryService,
    private router: Router,
    private app: AppComponent
  ) {
    super(activatedRoute, RESOURCE.ORGANIZATION);
    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      if (e instanceof NavigationEnd) {
        this.ngOnInit();
      }
    });
   }

  ngOnInit() {
    const params = this.activatedRoute.snapshot.params;
    if (params && CommonUtils.isValidId(params.orgId)) {
      this.orgId = params.orgId;
    }
    this.buildFormOrgBoundary({});
    this.prepareSaveOrUpdate();
  }

  get f() {
    return this.formOrgBoundary.controls;
  }

  private prepareSaveOrUpdate() {
    // Load all list org boundary
    this.orgBoundaryService.findAllOrgBoundaryByOrg(this.orgId)
    .subscribe(res => {
      this.buildFormOrgBoundary(res.data);
    });

    // Load list org boundary
    this.orgBoundaryService.findOrgBoundaryByOrg(this.orgId)
    .subscribe(res => {
      this.listOrgBoundary = res.data;
    });
  }

  private buildFormOrgBoundary(listOrg: any) {
    if (!listOrg) {
      this.formOrgBoundary = new FormArray([]);
    } else {
      const controls = new FormArray([]);
      for (const i in listOrg) {
        const org = listOrg[i];
        const group = this.makeOrgForm(org);
        controls.push(group);
      }
      this.formOrgBoundary = controls;
    }
  }

  private makeOrgForm(formData: any): FormGroup {
    const formGroupOrg = this.buildForm(formData, {
      organizationId: [null],
      organizationName: [null],
      isUpdate: [null],
      lstOrgBoundary: [new FormArray([])],
      listOrgBoundaryDelete: [[]]
    }, ACTION_FORM.ORG_BOUNDARY_UPDATE);
    if (formData.lstOrgBoundary && formData.lstOrgBoundary.length > 0) {
      const controls = new FormArray([]);
      for (const i in formData.lstOrgBoundary) {
        const orgBoundary = formData.lstOrgBoundary[i];
        const group = this.makeOrgBoundaryForm(orgBoundary);
        controls.push(group);
      }
      controls.setValidators(ValidationService.duplicateArray(
        ['organizationId', 'positionId']
        , 'positionId', 'app.duplicate.orgAndPosition'));

      formGroupOrg.setControl('lstOrgBoundary', controls);
    }
    return formGroupOrg;
  }

  private makeOrgBoundaryForm(formData: any): FormGroup {
    const formGroup = this.buildForm(formData, {
      orgBoundaryId: [null],
      organizationId: [null],
      positionId: [null, [ValidationService.isRequiredIfHaveOne('boundaryNumber'), ValidationService.isRequiredIfHaveOne('listEmployee')]],
      boundaryNumber: [null,
        [ValidationService.positiveInteger, ValidationService.maxLength(10), Validators.max(999999999),
          ValidationService.ifHaveOneIsRequired('positionId')]],
      listEmployee: [[], [ValidationService.ifHaveOneIsRequired('positionId')]]
    }, ACTION_FORM.ORG_BOUNDARY_UPDATE);
    return formGroup;
  }

  /**
   * addOrgBoundary
   * param index
   * param item
   */
  public addOrgBoundary(index: number, item: FormGroup) {
    const controls = item.get('lstOrgBoundary') as FormArray;
    controls.insert(index + 1, this.makeOrgBoundaryForm(item.value));
  }

  /**
   * removeOrgBoundary
   * param index
   * param item
   */
  public removeOrgBoundary(index: number, item: FormGroup) {
    const controls = item.get('lstOrgBoundary') as FormArray;
    const arrayDelete = item.get('listOrgBoundaryDelete').value;
    const itemOrgBoundary = controls.controls[index] as FormGroup;
    if (itemOrgBoundary.get('orgBoundaryId').value) {
      arrayDelete.push(itemOrgBoundary.get('orgBoundaryId').value);
      item.get('listOrgBoundaryDelete').setValue(arrayDelete);
    }
    if (controls.length === 1) {
      controls.removeAt(index);
      controls.push(this.makeOrgBoundaryForm({ organizationId: itemOrgBoundary.get('organizationId').value }));
      return;
    }
    controls.removeAt(index);
  }

  private validateBeforeSave(): boolean {
    return CommonUtils.isValidForm(this.formOrgBoundary);
  }

  /**
   * check sum boundary = sum sub by nation
   */
  public validateTotalBoundary(): boolean {
    let isError = false;
    for (const group of this.formOrgBoundary.controls) {
      const boundaryNumberControl = group.get('boundaryNumber') as FormControl;
      const listDetailArray = group.get('listDetail') as FormArray;
      if (listDetailArray === null || listDetailArray.length === 0) {
        continue;
      }
      if (! CommonUtils.isRealNumber(boundaryNumberControl.value)) {
        continue;
      }
      const sum = this.getSumByNation(listDetailArray);
      if (! CommonUtils.isRealNumber(sum)) {
        continue;
      }
      if (CommonUtils.compareNumber(sum, boundaryNumberControl.value) !== 0) {
        boundaryNumberControl.setErrors({boundaryNumberNotMatch: {}});
        boundaryNumberControl.markAsTouched();
        isError = true;
      }
    }

    return isError;
  }

  public getSumByNation(listDetailArray): number {
    return 0;
  }

  /**
   * Xu ly popup chon nhan vien
   */

  // Hien thi popup chon nhan vien
  public onChose(objectForm) {
    const modalRef = this.modalService.open(DataPickerModalComponent, {
      backdrop: 'static',
    });
    modalRef.componentInstance.setInitValue({
        operationKey: 'action.view'
      , adResourceKey: 'resource.employee'
      , objectBO: 'EmployeeBO'
      , codeField: 'email'
      , nameField: 'fullName'
      , selectField: 'employeeId'
      , filterCondition: ' and status = 1 '
    });

    modalRef.result.then((item) => {
      if (!item) {
        return;
      }
      // add id nhan vien vao form
      const arrayEmployee = objectForm.get('listEmployee').value;
      if (this.containsObjectInList(item, arrayEmployee)) {
        this.orgBoundaryService.processReturnMessage({type: 'WARNING', code: 'boundary.employeeHasAssign'});
        return;
      }
      arrayEmployee.push(item);
      objectForm.get('listEmployee').setValue(arrayEmployee);

      // // add id nhan vien vao form
      // const arrayEmployeeId = objectForm.get('listEmployeeId').value;
      // arrayEmployeeId.push(item.selectField);
      // objectForm.get('listEmployeeId').setValue(arrayEmployeeId);
      // // add email nhan vien vao form
      // const arrayEmployeeEmail = objectForm.get('lstEmployeeEmail').value;
      // arrayEmployeeId.push(item.codeField);
      // objectForm.get('lstEmployeeEmail').setValue(arrayEmployeeEmail);
    });
  }
  public showListEmployee(objectForm) {
    const modalRef = this.modalService.open(ListEmployeeComponent, {
      backdrop: 'static',
    });
    modalRef.componentInstance.setListEmployee(objectForm.get('listEmployee').value);
    objectForm.get('listEmployee').setValue(modalRef.componentInstance.resultList);

    modalRef.result.then((item) => {
      if (!item) {
        return;
      }
      objectForm.get('listEmployee').updateValueAndValidity();
    });
  }
  public actionSave() {
    if (!this.validateBeforeSave()) {
      return;
    }
    this.app.confirmMessage(null, () => {// on accepted
      this.orgBoundaryService.saveOrUpdate(this.formOrgBoundary.value).subscribe(
        res => {
          if (this.orgBoundaryService.requestIsSuccess(res)) {
            this.prepareSaveOrUpdate();
          }
        }
      );
    }, null);
  }

  private containsObjectInList(obj, list): boolean {
    let i;
    for (i = 0; i < list.length; i++) {
      if (list[i].selectField === obj.selectField) {
        return true;
      }
    }
    return false;
  }

  public createStringListEmailEmployee(objectForm): string {
    const arrayEmployee = objectForm.get('listEmployee').value;
    let arrayEmployeeEmail = '';
    for (let i = 0; i < arrayEmployee.length; i++) {
      if (i + 1 === arrayEmployee.length) {
        arrayEmployeeEmail += arrayEmployee[i].codeField;
      } else {
        arrayEmployeeEmail += arrayEmployee[i].codeField + ', ';
      }
    }
    return arrayEmployeeEmail;
  }
  ngOnDestroy() {}
}
