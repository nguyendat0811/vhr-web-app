import { RESOURCE } from '@app/core/app-config';
import { PropertyResolver } from '@app/shared/services/property.resolver';
import { CommonUtils } from '@app/shared/services';
import { OrganizationSearchComponent } from './organization-search/organization-search.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrganizationAddComponent } from './organization-add/organization-add.component';
import { OrganizationManageComponent } from './organization-manage.component';

const routes: Routes = [
  {
    path: '',
    component: OrganizationManageComponent,
    children: [
      {
        path: 'search',
        component: OrganizationSearchComponent,
        resolve: {
          props: PropertyResolver
        },
        data: {
          resource: RESOURCE.ORGANIZATION,
          nationId: CommonUtils.getNationId()
        }
      }, {
        path: 'search/:orgId',
        component: OrganizationSearchComponent,
        resolve: {
          props: PropertyResolver
        },
        data: {
          resource: RESOURCE.ORGANIZATION,
          nationId: CommonUtils.getNationId()
        }
      }, {
        path: 'add',
        component: OrganizationAddComponent,
        resolve: {
          props: PropertyResolver
        },
        data: {
          resource: RESOURCE.ORGANIZATION,
          nationId: CommonUtils.getNationId()
        }
      }, {
        path: 'edit/:orgId',
        component: OrganizationAddComponent,
        resolve: {
          props: PropertyResolver
        },
        data: {
          resource: RESOURCE.ORGANIZATION,
          nationId: CommonUtils.getNationId()
        }
      }
    ],
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.ORGANIZATION,
      nationId: CommonUtils.getNationId()
    }
  }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class OrganizationManageRoutingModule {}
