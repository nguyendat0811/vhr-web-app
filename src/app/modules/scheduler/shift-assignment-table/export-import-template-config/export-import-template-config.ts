import { IFixedTitle, IHeaderConfig } from "@app/core/services/schedular/read-file-schedular.service";
import * as moment from 'moment';

export class ExportImportTemplateConfig {
  static getTitleConfig(reportTitle: string, organizationName: string, startDate: Date, endDate: Date): IFixedTitle[] {
    return [
      { start: 1, tittle: "CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM", },
      { start: 2, tittle: "Độc lập - Tự do - Hạnh phúc", },
      { start: 5, tittle: reportTitle.toUpperCase() },
      { start: 6, tittle: `Đơn vị: ${organizationName}`, },
      { start: 7, tittle: `Từ ngày: ${moment(startDate).format("DD/MM/YYYY")} đến ngày: ${moment(endDate).format("DD/MM/YYYY")}`, },
    ]
  }
  static getHeaderConfig(startDate: Date, endDate: Date): IHeaderConfig[] {
    return [
      { index: 1, title: 'STT', key: 'index', width: 5, fixed: true, parentHeader: 0 },
      { index: 2, title: 'Mã nhân viên', key: 'employeeCode', width: 10, fixed: true, parentHeader: 0 },
      { index: 3, title: 'Họ tên', key: 'employeeName', width: 20, fixed: true, parentHeader: 0 },
      { index: 4, title: 'Chức danh', key: 'title', width: 15, fixed: true, parentHeader: 0 },
      { index: 5, title: 'Mã đơn vị', key: 'organizationCode', width: 10, fixed: true, parentHeader: 0 },
      { index: 6, title: 'Ngày trong tháng', key: null, width: 6, fixed: true, parentHeader: 0 },
    ].concat(this.getDynamicHeader(6, startDate, endDate), [
      { index: 7, title: 'Tổng công tiêu chuẩn (h)', key: 'hoursShift', width: 10, fixed: true, parentHeader: 0 },
      { index: 8, title: 'Trạng thái nhân viên', key: 'status', width: 10, fixed: true, parentHeader: 0 },
    ]);
  }

  static getImportErrorHeaderConfig(startDate: Date, endDate: Date): IHeaderConfig[] {
    return [
      { index: 1, title: 'STT', key: 'index', width: 5, fixed: true, parentHeader: 0 },
      { index: 2, title: 'Mã nhân viên', key: 'employeeCode', width: 10, fixed: true, parentHeader: 0 },
      { index: 3, title: 'Họ tên', key: 'employeeName', width: 20, fixed: true, parentHeader: 0 },
      { index: 4, title: 'Chức danh', key: 'title', width: 15, fixed: true, parentHeader: 0 },
      { index: 5, title: 'Mã đơn vị', key: 'organizationCode', width: 10, fixed: true, parentHeader: 0 },
      { index: 6, title: 'Ngày trong tháng', key: null, width: 6, fixed: true, parentHeader: 0 },
    ].concat(this.getDynamicHeader(6, startDate, endDate), [
      { index: 7, title: 'Tổng công tiêu chuẩn (h)', key: 'hoursShift', width: 10, fixed: true, parentHeader: 0 },
      { index: 8, title: 'Trạng thái nhân viên', key: 'status', width: 10, fixed: true, parentHeader: 0 },
      { index: 9, title: 'Thông tin lỗi', key: 'errorMessage', width: 20, fixed: true, parentHeader: 0 },
    ]);
  }

  static getExportHeaderConfig(startDate: Date, endDate: Date): IHeaderConfig[] {
    return [
      { index: 1, title: 'STT', key: 'index', width: 5, fixed: true, parentHeader: 0 },
      { index: 2, title: 'Mã nhân viên', key: 'employeeCode', width: 10, fixed: true, parentHeader: 0 },
      { index: 3, title: 'Họ tên', key: 'employeeName', width: 20, fixed: true, parentHeader: 0 },
      { index: 4, title: 'Chức danh', key: 'title', width: 15, fixed: true, parentHeader: 0 },
      { index: 5, title: 'Mã đơn vị', key: 'organizationCode', width: 10, fixed: true, parentHeader: 0 },
      { index: 6, title: 'Ngày trong tháng', key: null, width: 6, fixed: true, parentHeader: 0 },
    ].concat(this.getDynamicHeader(6, startDate, endDate),
      [
        { index: 7, title: 'Tổng công thực tế (h)', key: 'hoursWork', width: 10, fixed: true, parentHeader: 0 },
        { index: 8, title: 'Tổng công tiêu chuẩn (h)', key: 'hoursShift', width: 10, fixed: true, parentHeader: 0 },
        { index: 9, title: 'Trạng thái nhân viên', key: 'status', width: 15, fixed: true, parentHeader: 0 },
      ]);
  }

  static getReportInvalidDataHeaderConfig(): IHeaderConfig[] {
    return [
      { index: 1, title: 'STT', key: 'index', width: 5, fixed: true, parentHeader: 0 },
      { index: 2, title: 'Mã nhân viên', key: 'employeeCode', width: 10, fixed: true, parentHeader: 0 },
      { index: 3, title: 'Họ tên', key: 'employeeName', width: 20, fixed: true, parentHeader: 0 },
      { index: 4, title: 'Ngày', key: 'date', width: 20, fixed: true, parentHeader: 0 },
      { index: 5, title: 'Phân ca', key: 'shift', width: 10, fixed: true, parentHeader: 0 },
      { index: 6, title: 'Thời gian tương ứng với phân ca', key: 'shiftTime', width: 20, fixed: true, parentHeader: 0 },
      { index: 7, title: 'Thời gian vào/ra', key: 'actualTime', width: 60, fixed: true, parentHeader: 0 },
    ];
  }
  static getReportWarningShiftDataHeaderConfig() {
    return {
      employeeReportHeader: [
        { index: 1, title: 'STT', key: 'index', width: 5, fixed: true, parentHeader: 0 },
        { index: 2, title: 'Mã nhân viên', key: 'employeeCode', width: 10, fixed: true, parentHeader: 0 },
        { index: 3, title: 'Họ tên', key: 'employeeName', width: 20, fixed: true, parentHeader: 0 },
        { index: 4, title: 'Cảnh báo', key: 'warningMessage', width: 80, fixed: true, parentHeader: 0, spanCol: 2 },
      ],
      organizationReportHeader: [
        { index: 1, title: 'STT', key: 'index', width: 5, fixed: true, parentHeader: 0 },
        { index: 2, title: 'Mã siêu thị', key: 'organizationCode', width: 10, fixed: true, parentHeader: 0 },
        { index: 3, title: 'Tên siêu thị', key: 'organizationName', width: 20, fixed: true, parentHeader: 0 },
        { index: 4, title: 'Quân số siêu thị', key: 'totalEmployee', width: 20, fixed: true, parentHeader: 0 },
        { index: 5, title: 'Cảnh báo', key: 'warningMessage', width: 60, fixed: true, parentHeader: 0 },
      ],
    };
  }

  static getDynamicHeader(parentHeader, startDate: Date, endDate: Date): IHeaderConfig[] {
    const dynamicHeaders = [];
    let index = 1;
    for (let currentDate = new Date(startDate); currentDate <= endDate; currentDate.setDate(currentDate.getDate() + 1)) {
      const day = currentDate.getDay();
      dynamicHeaders.push({
        index,
        title: `${currentDate.getDate()}(${day == 0 ? "CN" : "T" + (day + 1)})`,
        key: "" + (index + startDate.getDate() - 1),
        width: 6,
        fixed: false,
        parentHeader
      });
      index++;
    }
    return dynamicHeaders;
  }

  static getMessengerError(errorCode: number): string {
    switch (errorCode) {
      case 1: return "noData";
      case -1: return "Locked";
      case -2: return "systemError";
      case -3: return "haveError";
      case -4: return "notValidate";
      case -5: return "notExistEmployee";
      case -6: return "notExistOrganization";
      case -7: return "notMapperEmployOrg";
      case -8: return "doubleEmployee";
      case -9: return "doubleChangeShift";
      case -10: return "notExistChangeShift";
      case -11: return "doubleInMonthLockShift";
      case -12: return "notExistShiftEmploy";
      case -13: return "notExistWorkType";
      case -14: return "notExistUser";
      case -15: return "doubleShiftTime";
      case -16: return "notMapperEmployOrg";
      case -17: return "wrongWorkTime";
      case -18: return "wrongWorkTotalHours";
      case -19: return "noShiftExisted";
      case -20: return "emptyTimeShift";
      case -21: return "timeShiftNotInRange";
      case -22: return "timeShiftNotDayOfWeek";
      case -23: return "timeShiftOverTime";
      case -24: return "orgNoEmployee";
      case -25: return "notExistedGDST";
      case -26: return "notExistedTC";
      case -27: return "notExistedNVKT";
      case -28: return "workUnder48Hours";
      case -29: return "workOver48Hours";
      case -30: return "restUnder4Days";
      case -31: return "restLastDayMonth";
      case -32: return "orgNoTstOrQlstIn1Day";
      case -33: return "orgEmployeeRestOver20Percent";
      case -34: return "noPermission";
      case -35: return "maCongCheDo";
      default: return "haveError"
    }
  }
}
