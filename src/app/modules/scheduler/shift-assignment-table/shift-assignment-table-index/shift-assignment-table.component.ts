import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ACTION_FORM, RESOURCE } from '@app/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ValidationService } from '@app/shared/services';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonUtils } from '@app/shared/services';
import { SchedularService } from '@app/core/services/schedular/schedular.service';
import * as moment from 'moment';
import { WorkEmployee } from '@app/core/models/employee.model';
import { BaseControl, DataPickerResult } from "app/core/models/base.control";
import { AppComponent } from '@app/app.component';
import { ReadFileSchedularService, IParamExport, IFixedTitle } from '@app/core/services/schedular/read-file-schedular.service';
import { ExportImportTemplateConfig } from '../export-import-template-config/export-import-template-config';
import { HrStorage } from '@app/core/services/HrStorage';
import { TranslationService } from 'angular-l10n';

@Component({
  selector: 'shift-assignment-table',
  templateUrl: './shift-assignment-table.component.html',
  styleUrls: ['./shift-assignment-table.component.css']
})
export class ShiftAssignmentTableComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  selectedEmployee: DataPickerResult;
  selectedOrg: OrgSelectorData;
  searchStartDate: Date;
  searchEndDate: Date;
  formConfig = {
    orgParentId: ['', [ValidationService.required]],
    employeeId: [''],
    startDate: ['', [ValidationService.required]],
    endDate: ['', [ValidationService.required]],
  };
  workEmployees: WorkEmployee[] = [];
  totalRecords = 0;


  exportOptions = []


  constructor(
    public activatedRoute: ActivatedRoute,
    public activeModal: NgbActiveModal,
    private schedularService: SchedularService,
    private router: Router,
    private app: AppComponent,
    private readFileSchedularService: ReadFileSchedularService,
    private translation: TranslationService,
  ) {
    super(activatedRoute, RESOURCE.SHIFT_ASSIGN, ACTION_FORM.INTERIOR_SEARCH);
    this.formSave = this.buildForm({}, this.formConfig, ACTION_FORM.INTERIOR_SEARCH, [
      ValidationService.notAffter('startDate', 'endDate', 'app.empTimekeeping.endDate'),
      ValidationService.notOnMonth('startDate', 'endDate', 'validate.dateNotSameMonth'),
    ]);
  }

  get orgParentId() {
    return this.formSave.get("orgParentId") as BaseControl;
  }
  get employeeId() {
    return this.formSave.get("employeeId") as BaseControl;
  }
  get startDate() {
    return this.formSave.get("startDate") as BaseControl;
  }

  get endDate() {
    return this.formSave.get("endDate") as BaseControl;
  }

  ngOnInit() {
    this.exportOptions = [
      {
        label: this.translation.translate('app.schedular.shiftAssignmentTable.invalidShiftData'), command: () => {
          this.onExportReportInvalidData();
        },
      },
      {
        label: this.translation.translate('app.schedular.shiftAssignmentTable.warningShiftData'), command: () => {
          this.onExportReportWarningShiftData();
        },
      }
    ]
  }

  onLoadWorkEmployeePage(event) {
    const { page, size } = event;
    this.onSearch(page, size);
  }

  onSearch(page = 0, size = 0) {
    if (!CommonUtils.isValidForm(this.formSave))
      return;

    this.searchStartDate = new Date(this.formSave.value["startDate"]);
    this.searchEndDate = new Date(this.formSave.value["endDate"]);

    // to UTC
    this.searchStartDate.setMinutes(-this.searchStartDate.getTimezoneOffset());
    this.searchEndDate.setMinutes(-this.searchEndDate.getTimezoneOffset());

    const params = {
      fromDate: moment(this.searchStartDate).format("YYYY-MM-DD"),
      toDate: moment(this.searchEndDate).format("YYYY-MM-DD"),
      organizationId: this.selectedOrg.organizationId,
      orgenizationId: this.selectedOrg.organizationId,
      employeeId: this.selectedEmployee && this.selectedEmployee.codeField,
      page,
      size
    };

    return this.schedularService.searchEmployeeWorkTime(params).subscribe(res => {
      this.workEmployees = res.data.workEmployees;
      this.totalRecords = this.workEmployees.length; // turn off paging
    });
  }

  selectOrg(data: OrgSelectorData) {
    this.selectedOrg = data;
  }

  selectEmployee(data: DataPickerResult) {
    this.selectedEmployee = data;
  }

  onClickImport() {
    this.router.navigate(["import"], { relativeTo: this.activatedRoute });
  }

  onUpdateWorkEmployee(params) {
    this.schedularService.updateEmployeeWorkTime({
      ...params,
      employeeIdEdit: HrStorage.getUserToken().employeeCode,
      fromDate: moment(this.searchStartDate).format("YYYY-MM-DD"),
      toDate: moment(this.searchEndDate).format("YYYY-MM-DD"),
    }).subscribe(res => {
      if (res.code) {
        const firstErrorWork = res.data.workEmployeesUpdateErrors.dataWorks.find(x => x && x.works && x.works.length);
        const error = firstErrorWork.works[0];
        this.app.errorMessage(`schedular.${ExportImportTemplateConfig.getMessengerError(error.errorCode)}`);
        return;
      }
      this.onSearch();
      this.app.successMessage("updateSuccess");
    });
  }
  onClickExport() {
    if (!CommonUtils.isValidForm(this.formSave))
      return;

    this.searchStartDate = new Date(this.formSave.value["startDate"]);
    this.searchEndDate = new Date(this.formSave.value["endDate"]);

    // to UTC
    this.searchStartDate.setMinutes(-this.searchStartDate.getTimezoneOffset());
    this.searchEndDate.setMinutes(-this.searchEndDate.getTimezoneOffset());

    const params = {
      fromDate: moment(this.searchStartDate).format("YYYY-MM-DD"),
      toDate: moment(this.searchEndDate).format("YYYY-MM-DD"),
      organizationId: this.selectedOrg.organizationId,
      orgenizationId: this.selectedOrg.organizationId,
      employeeId: this.selectedEmployee && this.selectedEmployee.codeField,
      page: 0,
      size: 0
    };

    this.schedularService.searchEmployeeWorkTime(params).subscribe(res => {
      const source = res.data.workEmployees as WorkEmployee[];
      const exportData = source.map(x => {
        return {
          employeeCode: x.employee.employeeCode,
          employeeName: x.employee.fullName,
          fullName: x.employee.fullName,
          organizationCode: x.employee.organization.code,
          organizationId: x.employee.organizationId,
          organizationName: x.employee.organization.name,
          hoursWork: x.hoursWork,
          hoursShift: x.hoursShift,
          workDay: x.works.map(w => ({
            date: new Date(w.workDay).getDate(), // column index for month days
            shiftCodes: w.totalHours
          })),
          status: "Đang làm việc"
        };
      });

      const reportTitle = "Bảng công";
      const exportConfig = {
        reportTitle: reportTitle,
        orgMaster: this.selectedOrg.name,
        month: this.searchStartDate,
        headers: ExportImportTemplateConfig.getExportHeaderConfig(this.searchStartDate, this.searchEndDate),
        tittleConfig: ExportImportTemplateConfig.getTitleConfig(reportTitle, this.selectedOrg.name, this.searchStartDate, this.searchEndDate),
        fromDate: this.searchStartDate,
        toDate: this.searchEndDate,
      } as IParamExport;
      this.readFileSchedularService.exportAsExcelFile(exportData, reportTitle, exportConfig);
    });
  }

  getErrorMessage(errors: ReportErrorMessageDetail[]): ReportErrorMessage[] {
    const groupedErrorMessages = new Map<number, ReportErrorMessage>();
    const errorMapping = {
      "-28": "app.schedular.workUnder48Hours",
      "-29": "app.schedular.workOver48Hours",
      "-30": "app.schedular.restUnder4Days",
      "-31": "app.schedular.restOnLastDayOfMonth",
      "-32": "app.schedular.noManagerOnWorkDay",
      "-33": "app.schedular.restOver20Percent",
    }

    const formatErrorMessage = (errorMessage: ReportErrorMessage): string => {
      const formatDisplayDate = (d: string) => moment(new Date(d)).format("DD/MM/YYYY");
      let formatErrorDetails;
      switch (errorMessage.code) {
        case -28:
        case -29:
          formatErrorDetails = errorMessage.errorDetails.map(e => `${e.hours}h (${formatDisplayDate(e.fromDate)}-${formatDisplayDate(e.toDate)})`)
          return `${errorMessage.messageByType}: ${formatErrorDetails.join(", ")}`;
        case -30:
          const countDayRest = errorMessage.errorDetails[0].countDayRest;
          return `${errorMessage.messageByType}: ${countDayRest} ${this.translation.translate("app.schedular.day")}`;
        case -32:
        case -33:
          formatErrorDetails = errorMessage.errorDetails.map(e => `${this.translation.translate("app.schedular.day")} ${formatDisplayDate(e.date)}`)
          return `${errorMessage.messageByType}: ${formatErrorDetails.join(", ")}`;
        default:
          return errorMessage.messageByType;
      }
    }

    errors.forEach(e => {
      if (!groupedErrorMessages.has(e.errorCode)) {
        const errorMessage = {
          code: e.errorCode,
          messageByType: this.translation.translate(errorMapping[e.errorCode]),
          errorDetails: [{
            countDayRest: e.countDayRest,
            fromDate: e.fromDate,
            hours: e.hours,
            toDate: e.toDate,
            date: e.date,
          }]
        } as ReportErrorMessage;
        groupedErrorMessages.set(e.errorCode, errorMessage);
      }
      else {
        const errorMessage = groupedErrorMessages.get(e.errorCode);
        errorMessage.errorDetails.push({
          countDayRest: e.countDayRest,
          fromDate: e.fromDate,
          hours: e.hours,
          toDate: e.toDate,
          date: e.date,
          errorCode: e.errorCode
        });
        groupedErrorMessages.set(e.errorCode, errorMessage)
      }
    });

    groupedErrorMessages.forEach((v, k, m) => {
      v.warningMessage = formatErrorMessage(v);
      m.set(k, v)
    });

    return Array.from(groupedErrorMessages.values());
  }

  onExportReportWarningShiftData() {
    if (!CommonUtils.isValidForm(this.formSave))
      return;

    this.searchStartDate = new Date(this.formSave.value["startDate"]);
    this.searchEndDate = new Date(this.formSave.value["endDate"]);

    // to UTC
    this.searchStartDate.setMinutes(-this.searchStartDate.getTimezoneOffset());
    this.searchEndDate.setMinutes(-this.searchEndDate.getTimezoneOffset());

    const params = {
      fromDate: moment(this.searchStartDate).format("YYYY-MM-DD"),
      toDate: moment(this.searchEndDate).format("YYYY-MM-DD"),
      organizationId: this.selectedOrg.organizationId,
      employeeId: this.selectedEmployee && this.selectedEmployee.codeField
    };

    this.schedularService.getWarningTimeworkReport(params).subscribe(res => {
      if (res.code) {
        // error
        this.app.errorMessage("error");
        return;
      }

      const employeeReports: ReportWarningShiftEmployeeResult[] = [];
      const organizationReports: ReportWarningShiftOrganizationResult[] = [];
      const employeeWarningReports = res.data.warningReport.employeeWarningReports as EmployeeWarningReport[];
      const organizationWarningReports = res.data.warningReport.organizationWarningReport as OrganizationWarningReport[];

      employeeWarningReports.forEach(er => {
        const reportErrorMessages = this.getErrorMessage(er.warningReports);
        reportErrorMessages.forEach(m => {
          employeeReports.push({
            code: m.code,
            employeeCode: er.employee.employeeCode,
            employeeName: er.employee.fullName,
            messageByType: m.messageByType,
            organizationName: er.employee.organization.name,
            warningMessage: m.warningMessage
          });
        })
      });
      organizationWarningReports.forEach(organizationWarningReport => {
        const organizationReportErrorMessages = this.getErrorMessage(organizationWarningReport.warningReports);

        organizationReports.push({
          headcount: organizationWarningReport.totalEmployee,
          organizationCode: organizationWarningReport.organization.code,
          organizationId: organizationWarningReport.organization.id,
          organizationName: organizationWarningReport.organization.name,
          warningMessage: "'" + organizationReportErrorMessages.map(x => `- ${x.warningMessage}`).join("\r\n"), // add ' to escape excel formula
          parentOrganizationCode: this.selectedOrg.code,
          parentOrganizationName: organizationWarningReport.organization.orgParentName,
          parentOrganizationId: this.selectedOrg.organizationId,
        });
      });


      const reportTitle = "Báo cáo cảnh báo dữ liệu công hợp lệ";
      const { organizationReportHeader, employeeReportHeader } = ExportImportTemplateConfig.getReportWarningShiftDataHeaderConfig();
      const exportConfigs = [
        {
          reportTitle: reportTitle,
          orgMaster: this.selectedOrg.name,
          headers: employeeReportHeader,
          tittleConfig: ExportImportTemplateConfig.getTitleConfig(reportTitle, this.selectedOrg.name, this.searchStartDate, this.searchEndDate),
          dataSource: employeeReports,
          headerStart: 10,
          tittleStartDataSource: 'Danh sách cảnh báo nhân viên:'
        },
        {
          reportTitle: reportTitle,
          orgMaster: this.selectedOrg.name,
          headers: organizationReportHeader,
          tittleConfig: [],
          dataSource: organizationReports,
          groupBy: "parentOrganizationName",
          headerStart: employeeReports.length + 14,
          tittleStartDataSource: 'Danh sách cảnh báo nhân viên:'
        }
      ] as IParamExport[];
      this.readFileSchedularService.exportExcelMultipleSource(reportTitle, exportConfigs);
    });

  }

  onExportReportInvalidData() {
    if (!CommonUtils.isValidForm(this.formSave))
      return;
    this.searchStartDate = new Date(this.formSave.value["startDate"]);
    this.searchEndDate = new Date(this.formSave.value["endDate"]);

    // to UTC
    this.searchStartDate.setMinutes(-this.searchStartDate.getTimezoneOffset());
    this.searchEndDate.setMinutes(-this.searchEndDate.getTimezoneOffset());

    const params = {
      fromDate: moment(this.searchStartDate).format("YYYY-MM-DD"),
      toDate: moment(this.searchEndDate).format("YYYY-MM-DD"),
      organizationId: this.selectedOrg.organizationId,
      employeeId: this.selectedEmployee && this.selectedEmployee.codeField
    };

    this.schedularService.getBreachTimeworkReport(params).subscribe(res => {
      if (res.code) {
        // error
        this.app.errorMessage("error");
        return;
      }
      const employeeReports = res.data.warningReport.employeeWarningReports as EmployeeWarningReport[];

      const data: ReportInvalidDataResult[] = [];
      employeeReports.forEach(r => {
        r.warningReports.forEach(w => {
          data.push({
            organizationName: r.employee.organization.name,
            employeeCode: r.employee.employeeCode,
            employeeName: r.employee.fullName,
            date: moment(new Date(w.date)).format("DD/MM/YYYY"),
            shift: w.workCode,
            shiftTime: `${w.shiftStart} - ${w.shiftEnd}`,
            actualTime: `${w.checkIn} - ${w.checkOut}`,
          });
        });
      });

      const reportTitle = "Báo cáo ngày công không hợp lệ";
      const exportConfig = {
        reportTitle: reportTitle,
        orgMaster: this.selectedOrg.name,
        headers: ExportImportTemplateConfig.getReportInvalidDataHeaderConfig(),
        tittleConfig: ExportImportTemplateConfig.getTitleConfig(reportTitle, this.selectedOrg.name, this.searchStartDate, this.searchEndDate)
      } as IParamExport;
      this.readFileSchedularService.exportAsExcelFile(data, reportTitle, exportConfig);
    });
  }
}

interface OrgSelectorData {
  organizationId: number,
  code: string,
  name: string
}

interface ReportInvalidDataResult {
  employeeCode: string,
  employeeName: string,
  organizationName: string,
  date: string,
  shift: string,
  shiftTime: string,
  actualTime: string,
}

interface ReportWarningShiftEmployeeResult extends ReportErrorMessage {
  employeeCode: string,
  employeeName: string,
  organizationName: string
}

interface ReportWarningShiftOrganizationResult {
  organizationCode: string,
  organizationId: number,
  organizationName: string,
  headcount: number,
  parentOrganizationId?: number,
  parentOrganizationName?: string,
  parentOrganizationCode?: string,
  warningMessage?: string,
}

interface ReportErrorMessage {
  code: number,
  messageByType: string,
  errorDetails?: ReportErrorMessageDetail[],
  warningMessage?: string,
}

interface ReportErrorMessageDetail {
  errorCode: number,
  countDayRest?: number,
  fromDate?: string,
  hours?: number,
  toDate?: string,
  date?: string,
  workCode?: string,
  shiftStart?: string,
  shiftEnd?: string,
  checkIn?: string,
  checkOut?: string,
}

interface OrganizationWarningReport {
  organization: OrganizationInfo,
  totalEmployee: number,
  warningReports: ReportErrorMessageDetail[]
}

interface EmployeeWarningReport {
  employee: EmployeeInfo,
  warningReports: ReportErrorMessageDetail[]
}

interface EmployeeInfo {
  id: number,
  employeeCode: string,
  fullName: string,
  organizationId: number,
  positionId?: number,
  organization: OrganizationInfo,
  position?: string
}

interface OrganizationInfo {
  id: number,
  organizationTypeId?: number,
  code: string,
  name: string,
  orgLevel: number,
  shortName: string,
  orgParentId: number,
  orgParentName?: string,
}
