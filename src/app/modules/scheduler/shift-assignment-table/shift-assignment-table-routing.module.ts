import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { RESOURCE } from "@app/core";
import { CommonUtils } from "@app/shared/services";
import { ShiftAssignmentTableComponent } from "./shift-assignment-table-index/shift-assignment-table.component";
import { ImportWorkEmployeeComponent } from "./import-work-employee/import-work-employee.component";


const routes: Routes = [
  {
    path: '',
    component: ShiftAssignmentTableComponent,
    data: {
      resource: RESOURCE.WORK_SCHEDULE,
      nationId: CommonUtils.getNationId()
    },
    pathMatch: 'full'
  },
  {
    path: 'import',
    component: ImportWorkEmployeeComponent,
    data: {
      resource: RESOURCE.WORK_SCHEDULE,
      nationId: CommonUtils.getNationId()
    },
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShiftAssignmentTableRoutingModule { }
