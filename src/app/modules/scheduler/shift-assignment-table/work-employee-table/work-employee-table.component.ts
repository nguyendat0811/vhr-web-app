import { Component, Input, OnInit, OnChanges, SimpleChanges, EventEmitter, Output } from '@angular/core';
import { AppComponent } from '@app/app.component';
import { WorkEmployee, WorkDay, HrWorkEmployee } from '@app/core/models/employee.model';
import { TranslationService } from 'angular-l10n';
import * as moment from "moment";
@Component({
  selector: 'work-employee-table',
  templateUrl: './work-employee-table.component.html',
  styleUrls: ['./work-employee-table.component.css']
})
export class WorkEmployeeTableComponent implements OnInit, OnChanges {

  constructor(private appComponent: AppComponent, private translationService: TranslationService) { }

  @Input() public month: Date;
  @Input() public searchStartDate?: Date;
  @Input() public searchEndDate?: Date;
  @Output() public onUpdateWorkEmployee: EventEmitter<any> = new EventEmitter<any>();
  @Output() public onLoadWorkEmployeePage: EventEmitter<any> = new EventEmitter<any>();
  @Input() public totalRecords: number = 0;
  @Input() public workEmployees: WorkEmployee[] = [];
  @Input() public selectorOrganizationId: number;
  first = 0;
  rows = 10;
  workEmployeeInitialData = new Map<string, HrWorkEmployee[]>();
  setSearchDays() {
    this.searchDays = []
    for (let currentDay = new Date(this.searchStartDate); currentDay <= this.searchEndDate; currentDay.setDate(currentDay.getDate() + 1)) {
      this.searchDays.push(new WorkDay(currentDay.getDate(), "", "", currentDay.getDay(), `app.schedular.day.${currentDay.getDay() + 1}`, true, false));
    }
  }

  ngOnInit() {
    this.month = this.month || new Date();
    const date = new Date(this.month.valueOf()), y = date.getFullYear(), m = date.getMonth();
    const firstDay = new Date(y, m, 1);
    const lastDay = new Date(y, m + 1, 0);

    this.searchStartDate = this.searchStartDate || new Date(firstDay);
    this.searchEndDate = this.searchEndDate || new Date(lastDay);

    this.setSearchDays();

  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.searchStartDate || changes.searchEndDate) { this.setSearchDays(); }
    if (!this.workEmployeeInitialData.values.length && changes.workEmployees) {
      this.workEmployees.forEach(workEmployee => {
        this.workEmployeeInitialData.set(workEmployee.employee.employeeCode, workEmployee.works.map(x => Object.assign({}, x)));
      });
    }
  }

  getWorkHours(day: WorkDay, works: HrWorkEmployee[]): string {
    const workTime = works.find(work => {
      const date = new Date(work.workDay);
      return date.getDate() == day.date;
    });
    return workTime ? `${workTime.totalHours}` : ""
  }

  isDisplayOrganization(index: number, workEmployee: WorkEmployee): boolean {
    const tableIndex = index - this.first;
    if (tableIndex < 1) {
      return true;
    }
    const prevWorkEmployee = this.workEmployees[tableIndex - 1];
    return prevWorkEmployee.employee.organizationId != workEmployee.employee.organizationId;
  }

  onUpdateCellData(data) {
    if (data.event.type == "keydown" && data.event.keyCode != 13)
      return;
    const totalHours = data.event.target.value == "" ? 0 : parseFloat(data.event.target.value);
    if (isNaN(totalHours) || totalHours < 0 || totalHours > 8) {
      this.appComponent.message('ERROR', this.translationService.translate('ERROR.valueInvalid'));
      return;
    }

    const workEmployee = this.workEmployees.find(x => x.employee.employeeCode == data.employeeId);
    const work = workEmployee.works.find(x => new Date(x.workDay).getDate() == data.workDay.date);
    if (work) {
      work.totalHours = totalHours;
    } else {

      const workDay = new Date(this.searchStartDate);
      workDay.setDate(data.workDay.date);
      workEmployee.works.push({
        employeeId: workEmployee.employee.employeeCode,
        totalHours: totalHours,
        workDay: this.formatDateParam(workDay)
      } as HrWorkEmployee);
    }
  }

  formatDateParam(date: Date): string {
    return moment(date).format("YYYY-MM-DD");
  }

  onClickUpdate() {
    const dataWorks = [];
    this.workEmployees.filter(x => x.works.length).forEach(workEmployee => {
      const initValues = this.workEmployeeInitialData.get(workEmployee.employee.employeeCode) || [];
      const initValuesDictionary = new Map();
      initValues.forEach(x => initValuesDictionary.set(x.workDay, x.totalHours));
      const works = initValuesDictionary.size > 0 ? workEmployee.works.filter(x => x.totalHours != initValuesDictionary.get(x.workDay)) : workEmployee.works;
      if (works.length) {
        dataWorks.push({
          employeeId: workEmployee.employee.employeeCode,
          works
        });
      }
    });

    if (dataWorks.length) {
      const params = {
        organizationId: this.selectorOrganizationId,
        dataWorks,
      };
      this.onUpdateWorkEmployee.emit(params);
      this.workEmployeeInitialData.clear();
    }
    else {
      this.appComponent.successMessage("noChange");
    }
  }

  loadWorkEmployees(event) {
    const { first, rows } = event;
    this.first = first;
    this.onLoadWorkEmployeePage.emit({ page: first / rows, size: rows });
  }

  searchDays: WorkDay[] = [];
}

