import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShiftAssignmentTableComponent } from './shift-assignment-table-index/shift-assignment-table.component';
import { ShiftAssignmentTableRoutingModule } from './shift-assignment-table-routing.module';
import { SharedModule } from '@app/shared';
import { WorkEmployeeTableComponent } from './work-employee-table/work-employee-table.component';
import { ImportWorkEmployeeComponent } from './import-work-employee/import-work-employee.component';

@NgModule({
  declarations: [ShiftAssignmentTableComponent, WorkEmployeeTableComponent, ImportWorkEmployeeComponent],
  imports: [
    CommonModule,
    SharedModule,
    ShiftAssignmentTableRoutingModule,
  ],
  entryComponents: [ShiftAssignmentTableComponent, ImportWorkEmployeeComponent]
})
export class ShiftAssignmentTableModule { }
