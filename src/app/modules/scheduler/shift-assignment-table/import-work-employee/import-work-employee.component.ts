import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ACTION_FORM, RESOURCE } from '@app/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { CommonUtils, ValidationService } from '@app/shared/services';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SchedularService } from '@app/core/services/schedular/schedular.service';
import { BaseControl } from "app/core/models/base.control";
import { IParamExport, ReadFileSchedularService } from '@app/core/services/schedular/read-file-schedular.service';
import { WorkDay, WorkEmployee } from '@app/core/models/employee.model';
import { ExportImportTemplateConfig } from '../export-import-template-config/export-import-template-config';
import * as moment from 'moment';
import { HrStorage } from '@app/core/services/HrStorage';
import { AppComponent } from '@app/app.component';
import { TranslationService } from 'angular-l10n';


@Component({
  selector: 'import-work-employee',
  templateUrl: './import-work-employee.component.html',
  styleUrls: ['./import-work-employee.component.css']
})
export class ImportWorkEmployeeComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  formFile: FormGroup;
  dataError: any;
  selectedOrg: OrgSelectorData;
  searchStartDate: Date;
  searchEndDate: Date;
  selectedFile: any;
  formConfig = {
    orgParentId: ['', [ValidationService.required]],
    startDate: ['', [ValidationService.required]],
    endDate: ['', [ValidationService.required]],
  };
  formFileConfig = {
    importFile: ['', [ValidationService.required]],
  }
  isImportError = false;
  importDataDictionary = new Map();
  errorFileName: string = "";

  constructor(
    public activatedRoute: ActivatedRoute,
    public activeModal: NgbActiveModal,
    private schedularService: SchedularService,
    private readFileService: ReadFileSchedularService,
    private appComponent: AppComponent,
    private translation: TranslationService
  ) {
    super(activatedRoute, RESOURCE.SHIFT_ASSIGN, ACTION_FORM.INTERIOR_SEARCH);
    this.formSave = this.buildForm({}, this.formConfig, ACTION_FORM.INTERIOR_SEARCH, [
      ValidationService.notAffter('startDate', 'endDate', 'app.empTimekeeping.endDate'),
      ValidationService.notOnMonth('startDate', 'endDate', 'validate.dateNotSameMonth'),
    ]);
    this.formFile = this.buildForm({}, this.formFileConfig, ACTION_FORM.INTERIOR_SEARCH);
  }

  get orgParentId() {
    return this.formSave.get("orgParentId") as BaseControl;
  }
  get startDate() {
    return this.formSave.get("startDate") as BaseControl;
  }

  get endDate() {
    return this.formSave.get("endDate") as BaseControl;
  }

  get importFile() {
    return this.formFile.get("importFile") as BaseControl;
  }

  ngOnInit() {
  }
  selectOrg(data: OrgSelectorData) {
    this.selectedOrg = data;
  }

  onClickImport() {
    this.isImportError = false;
    const isFormValid = CommonUtils.isValidForm(this.formSave);
    const isFileValid = CommonUtils.isValidForm(this.formFile);

    if (!isFormValid || !isFileValid)
      return;
    this.searchStartDate = new Date(this.formSave.value["startDate"]);
    this.searchEndDate = new Date(this.formSave.value["endDate"]);

    // to UTC
    this.searchStartDate.setMinutes(-this.searchStartDate.getTimezoneOffset());
    this.searchEndDate.setMinutes(-this.searchEndDate.getTimezoneOffset());

    const params = {
      fromDate: moment(this.searchStartDate).format("YYYY-MM-DD"),
      toDate: moment(this.searchEndDate).format("YYYY-MM-DD"),
      organizationId: this.selectedOrg.organizationId,
      employeeIdEdit: HrStorage.getUserToken().employeeCode,
      dataWorks: []
    };

    const headerConfig = ExportImportTemplateConfig.getHeaderConfig(this.searchStartDate, this.searchEndDate);

    const exportParams = {
      headers: headerConfig,
      month: this.searchStartDate,
      fromDate: this.searchStartDate,
      toDate: this.searchEndDate,
    } as IParamExport;

    this.readFileService.readFile<WorkEmployeeImportModel>(this.formFile.value.importFile, exportParams).subscribe(data => {
      const rawDataWorks = data.Datasource;
      console.log(rawDataWorks);

      params.dataWorks = rawDataWorks.map(emp => ({
        employeeId: emp.employeeCode,
        works: emp.workDay
          .filter(d => d.date >= this.searchStartDate.getDate()
            && d.date <= this.searchEndDate.getDate())
          .map(d => ({
            totalHours: parseInt(d.shiftCodes),
            workDay: moment(new Date(this.searchEndDate).setDate(d.date)).format("YYYY-MM-DD")
          }))
      })).filter(param => param.works.length > 0);

      if (!params.dataWorks.length) {
        this.appComponent.errorMessage("importNoData");
        return;
      }

      this.schedularService.updateEmployeeWorkTime(params).subscribe(res => {
        if (res.code) {
          // error
          this.appComponent.errorMessage("error");
          this.isImportError = true;
          this.errorFileName = `Importbangcongca_Canhbao`
          // fill error code
          rawDataWorks.forEach(data => {
            this.importDataDictionary.set(data.employeeCode, data);
          });
          const errorDetails = res.data.workEmployeesUpdateErrors;
          errorDetails.dataWorks.forEach(dataWork => {
            const employeeData = this.importDataDictionary.get(dataWork.employeeId);
            employeeData.errorMessage = dataWork.works
              .filter(work => work.errorCode)
              .map(work => `${moment(new Date(work.workDay)).format("DD/MM/YYYY")}: ${this.translation.translate(`ERROR.schedular.${ExportImportTemplateConfig.getMessengerError(work.errorCode)}`)}`)
              .join("\r\n");
            this.importDataDictionary.set(dataWork.employeeId, employeeData);
          });
          console.log(Array.from(this.importDataDictionary.values()));

        }
        else {
          // success
          this.appComponent.successMessage("success");
        }
      });
    });


  }

  onDownloadImportErrorDetails(e) {

    this.searchStartDate = new Date(this.formSave.value["startDate"]);
    this.searchEndDate = new Date(this.formSave.value["endDate"]);

    // to UTC
    this.searchStartDate.setMinutes(-this.searchStartDate.getTimezoneOffset());
    this.searchEndDate.setMinutes(-this.searchEndDate.getTimezoneOffset());

    const source = Array.from(this.importDataDictionary.values()) as WorkEmployeeImportModel[];
    const exportData = source;

    const reportTitle = "Bảng công";
    const exportConfig = {
      reportTitle: reportTitle,
      orgMaster: this.selectedOrg.name,
      month: this.searchStartDate,
      headers: ExportImportTemplateConfig.getImportErrorHeaderConfig(this.searchStartDate, this.searchEndDate),
      tittleConfig: ExportImportTemplateConfig.getTitleConfig(reportTitle, this.selectedOrg.name, this.searchStartDate, this.searchEndDate),
      fromDate: this.searchStartDate,
      toDate: this.searchEndDate,
    } as IParamExport;
    console.log(exportConfig);
    this.readFileService.exportAsExcelFile(exportData, this.errorFileName, exportConfig);
  }

  onDownloadTemplateImport(e) {
    e.preventDefault();
    if (!CommonUtils.isValidForm(this.formSave))
      return;

    this.searchStartDate = new Date(this.formSave.value["startDate"]);
    this.searchEndDate = new Date(this.formSave.value["endDate"]);

    // to UTC
    this.searchStartDate.setMinutes(-this.searchStartDate.getTimezoneOffset());
    this.searchEndDate.setMinutes(-this.searchEndDate.getTimezoneOffset());

    const params = {
      fromDate: moment(this.searchStartDate).format("YYYY-MM-DD"),
      toDate: moment(this.searchEndDate).format("YYYY-MM-DD"),
      organizationId: this.selectedOrg.organizationId,
    };

    this.schedularService.searchEmployeeWorkTime(params).subscribe(res => {

      const source = res.data.workEmployees as WorkEmployee[];
      const exportData = source.map(x => {
        return {
          employeeCode: x.employee.employeeCode,
          employeeName: x.employee.fullName,
          fullName: x.employee.fullName,
          organizationCode: x.employee.organization.code,
          organizationId: x.employee.organizationId,
          organizationName: x.employee.organization.name,
          hoursShift: x.hoursShift,
          workDay: x.works.map(w => ({
            date: new Date(w.workDay).getDate(), // column index for month days
            shiftCodes: w.totalHours
          })),
          status: "Đang làm việc"
        };
      });

      const reportTitle = "Bảng công";
      const exportConfig = {
        reportTitle: reportTitle,
        orgMaster: this.selectedOrg.name,
        month: this.searchStartDate,
        headers: ExportImportTemplateConfig.getHeaderConfig(this.searchStartDate, this.searchEndDate),
        tittleConfig: ExportImportTemplateConfig.getTitleConfig(reportTitle, this.selectedOrg.name, this.searchStartDate, this.searchEndDate),
        fromDate: this.searchStartDate,
        toDate: this.searchEndDate,
      } as IParamExport;
      console.log(exportConfig);
      this.readFileService.exportAsExcelFile(exportData, reportTitle, exportConfig);
    });
  }
}

interface OrgSelectorData {
  organizationId: number,
  code: string,
  name: string
}

interface WorkEmployeeImportModel {
  employeeCode: string,
  employeeName: string,
  title?: string,
  organizationCode?: string;
  organizationName?: string;
  hourShift?: number;
  status?: string;
  errorCode?: number;
  errorMessage?: string;
  workDay: WorkDay[],
}
