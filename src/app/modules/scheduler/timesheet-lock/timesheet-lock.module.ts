import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TimeSheetLockComponent } from './timesheet-lock-index/timesheet-lock.component';
import { TimeSheetLockRoutingModule } from './timesheet-lock-routing.module';
import { SharedModule } from '@app/shared';
import { TimeSheetLockGridComponent } from './timesheet-lock-grid/timesheet-lock-grid.component';
@NgModule({
  declarations: [TimeSheetLockComponent, TimeSheetLockGridComponent],
  imports: [
    CommonModule,
    SharedModule,
    TimeSheetLockRoutingModule
  ],
  entryComponents: [TimeSheetLockComponent]
})
export class TimeSheetLockModule { }
