import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ACTION_FORM, OrgSelectorService, RESOURCE } from '@app/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { BaseControl } from '@app/core/models/base.control';
import { ValidationService } from '@app/shared/services';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonUtils } from '@app/shared/services';
import { SchedularService } from '@app/core/services/schedular/schedular.service';
import { LockWorkOrg } from '@app/core/models/employee.model';
import * as moment from "moment";
import { AppComponent } from "@app/app.component";
import { ExportImportTemplateConfig } from '../../../scheduler/shift-assignment-table/export-import-template-config/export-import-template-config';

@Component({
  selector: 'timesheet-lock',
  templateUrl: './timesheet-lock.component.html',
  styleUrls: ['./timesheet-lock.component.css']
})
export class TimeSheetLockComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  dataError: any;
  selectedOrg: OrgSelectorData;
  searchStartDate: Date;
  searchEndDate: Date;
  formConfig = {
    orgParentId: ['', [ValidationService.required]],
    startDate: ['', [ValidationService.required]],
    endDate: ['', [ValidationService.required]],
  };
  lockTimeSheetOrgs: LockWorkOrg[];
  constructor(
    public activatedRoute: ActivatedRoute,
    public activeModal: NgbActiveModal,
    private schedularService: SchedularService,
    private orgSelectorService: OrgSelectorService,
    private app: AppComponent
  ) {
    super(activatedRoute, RESOURCE.SHIFT_ASSIGN, ACTION_FORM.INTERIOR_SEARCH);
    this.formSave = this.buildForm({}, this.formConfig, ACTION_FORM.INTERIOR_SEARCH, [
      ValidationService.notAffter('startDate', 'endDate', 'app.empTimekeeping.endDate'),
      ValidationService.notOnMonth('startDate', 'endDate', 'validate.dateNotSameMonth'),
    ]);
  }

  get orgParentId() {
    return this.formSave.get("orgParentId") as BaseControl;
  }

  get startDate() {
    return this.formSave.get("startDate") as BaseControl;
  }

  get endDate() {
    return this.formSave.get("endDate") as BaseControl;
  }

  ngOnInit() {
  }
  onSearch() {
    if (!CommonUtils.isValidForm(this.formSave))
      return;

    this.searchStartDate = new Date(this.formSave.value["startDate"]);
    this.searchEndDate = new Date(this.formSave.value["endDate"]);

    // to UTC
    this.searchStartDate.setMinutes(-this.searchStartDate.getTimezoneOffset());
    this.searchEndDate.setMinutes(-this.searchEndDate.getTimezoneOffset());

    const params = {
      fromDate: moment(this.searchStartDate).format("YYYY-MM-DD"),
      toDate: moment(this.searchEndDate).format("YYYY-MM-DD"),
      organizationId: this.selectedOrg.organizationId
    };

    this.schedularService.getLock(params).subscribe(res => {
      if (res.code) {
        this.app.errorMessage(`schedular.${ExportImportTemplateConfig.getMessengerError(res.code)}`);
        return;
      }

      const lockTimeSheets = res.data;
      this.orgSelectorService.search({
        showOrgExpried: 0,
        nodeId: params.organizationId,
        operationKey: "action.insert",
        adResourceKey: "resource.organization"
      }, { rows: lockTimeSheets.lockTimeSheetOrgs && (lockTimeSheets.lockTimeSheetOrgs.length + 1) }).subscribe(res => {
        const organizationWorkLockDict = new Map();
        (lockTimeSheets.lockTimeSheetOrgs as LockWorkOrg[]).forEach(lockWork => {
          organizationWorkLockDict.set(lockWork.id, lockWork.data);
        });
        this.lockTimeSheetOrgs = [];
        const orgData = res.data as OrgSelectorData[];
        const rootOrganizationLevel = (orgData[0].path.match(/\//g) || []).length;
        orgData.forEach((organization, index) => {
          const lockWorkData = organizationWorkLockDict.get(organization.organizationId);
          const organizationLevel = (organization.path.match(/\//g) || []).length;
          this.lockTimeSheetOrgs.push({
            id: organization.organizationId,
            organizationCode: organization.code,
            organizationName: organization.name,
            hasChild: orgData.length > 1 && (index < (orgData.length - 1) && orgData[index + 1].path.includes(organization.path)),
            data: lockWorkData,
            relativeLevel: organizationLevel - rootOrganizationLevel
          } as LockWorkOrg);
        });
      })
    });
  }

  selectOrg(data: OrgSelectorData) {
    this.selectedOrg = data;
  }

  onClickUpdateLock(isLock: boolean) {
    if (!CommonUtils.isValidForm(this.formSave) || !this.lockTimeSheetOrgs.length)
      return;
    this.onUpdateLock({ isLock, organizationId: this.selectedOrg.organizationId, fromDate: this.searchStartDate, toDate: this.searchEndDate });
  }

  onUpdateLock(data: any) {
    data = { ...data, isLock: Number(data.isLock) }
    this.schedularService.updateLock(data).subscribe((res) => {
      if (res.code) {
        this.app.errorMessage(`schedular.${ExportImportTemplateConfig.getMessengerError(res.code)}`);
        return;
      }
      this.app.successMessage(data.isLock ? "lockTimeSheet" : "unlockTimeSheet");
      this.onSearch();
    });
  }
}

interface OrgSelectorData {
  organizationId: number,
  code: string,
  name: string,
  path?: string
}

