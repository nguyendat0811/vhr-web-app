import { Component, Input, OnInit, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { LockWorkOrg, WorkDay } from '@app/core/models/employee.model';
import { TranslationService } from 'angular-l10n';


@Component({
  selector: 'timesheet-lock-grid',
  templateUrl: './timesheet-lock-grid.component.html',
  styleUrls: ['./timesheet-lock-grid.component.css']
})
export class TimeSheetLockGridComponent implements OnInit, OnChanges {

  constructor(private translation: TranslationService) { }

  @Input() public lockWorkOrgs: LockWorkOrg[];
  @Input() public month: Date;
  @Input() public searchStartDate?: Date;
  @Input() public searchEndDate?: Date;
  @Output() public onUpdateLock: EventEmitter<any> = new EventEmitter<any>();
  totalRecordMessage: string;
  setSearchDays() {
    this.searchDays = []
    for (let currentDay = new Date(this.searchStartDate); currentDay <= this.searchEndDate; currentDay.setDate(currentDay.getDate() + 1)) {
      this.searchDays.push(new WorkDay(currentDay.getDate(), "", "", currentDay.getDay(), `app.schedular.day.${currentDay.getDay() + 1}`, true, false));
    }
  }

  ngOnInit() {
    this.month = this.month || new Date();
    const date = new Date(this.month.valueOf()), y = date.getFullYear(), m = date.getMonth();
    const firstDay = new Date(y, m, 1);
    const lastDay = new Date(y, m + 1, 0);

    this.searchStartDate = this.searchStartDate || new Date(firstDay);
    this.searchEndDate = this.searchEndDate || new Date(lastDay);

    this.setSearchDays();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.searchStartDate || changes.searchEndDate) { this.setSearchDays(); }
    if (changes.lockWorkOrgs && this.lockWorkOrgs && this.lockWorkOrgs.length) {
      this.totalRecordMessage = this.translation.translate("app.timesheet.findRecords").replace("{n}", this.lockWorkOrgs.length);
    }
  }

  searchDays: WorkDay[] = [];

  isLocked(day: WorkDay, lockWorkOrg: LockWorkOrg): boolean {
    return lockWorkOrg.data.some(lockWork => {
      const lockWorkDate = new Date(lockWork.shiftsDate);
      return lockWork.isLocked && lockWorkDate.getDate() == day.date
    });
  }

  onClickUpdateLock(organizationId: number, isLock: boolean, date = null) {
    const fromDate = new Date(this.searchStartDate);
    const toDate = new Date(this.searchEndDate);
    if (date) {
      fromDate.setDate(date);
      toDate.setDate(date);
    }
    const data = { organizationId, isLock, fromDate, toDate };
    this.onUpdateLock.emit(data);
  }

  formatOrganizationName(lockWorkOrg: LockWorkOrg): string {
    if (!lockWorkOrg.relativeLevel)
      return lockWorkOrg.organizationName;
    return `${"--".repeat(lockWorkOrg.relativeLevel)} ${lockWorkOrg.organizationName}`;
  }
}

