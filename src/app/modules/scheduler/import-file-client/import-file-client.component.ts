import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from '@app/app.component';
import { FormGroup } from '@angular/forms';
import { ACTION_FORM, RESOURCE } from '@app/core/app-config';
import { FileControl } from '@app/core/models/file.control';
import { CommonUtils, ValidationService } from '@app/shared/services';
import { saveAs } from 'file-saver';
import { OpenCloseScheduleService } from '@app/core/services/hr-timesheet/open-close-schedule.service';
import { ImportData, ReadFileSchedularService } from '@app/core/services/schedular/read-file-schedular.service';
import { ShiftEmploy } from '@app/core/models/employee.model';
@Component({
  selector: 'import-file-client',
  templateUrl: './import-file-client.component.html',
  styleUrls: ['./import-file-client.component.css']
})
export class ImportFileClientComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  dataError: any;
  formConfig = {
    orgPlanId: ['']
  };
  @Output()
  public onSubmit: EventEmitter<ImportData> = new EventEmitter<ImportData>();
  @Input()
  public HeaderStart: number = 8;
  @Input()
  public HeaderCountHeight: number = 0;
  constructor(
    public activatedRoute: ActivatedRoute,
    private app: AppComponent,
    public activeModal: NgbActiveModal,
    private openCloseScheduleService: OpenCloseScheduleService,
    public readFileSchedularService: ReadFileSchedularService
  ) {
    super(activatedRoute, RESOURCE.ORGANIZATION_PLAN, ACTION_FORM.IMPORT);
    this.formSave = this.buildForm({}, this.formConfig, ACTION_FORM.IMPORT);
    this.formSave.addControl('fileImport', new FileControl(null, ValidationService.required));
  }

  disableSubmit = true;
  ngOnInit() {

  }

  get f() {
    return this.formSave.controls;
  }

  setFormValue(planId: number) {
    this.formSave.get('orgPlanId').setValue(planId);
  }


  actionSubmitForm() {
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    }

    if (!this.formSave || !this.formSave.value["fileImport"])
      return;
    this.readFileSchedularService.readFile<ShiftEmploy>(this.formSave.value["fileImport"], {month : new Date()})
      .subscribe(data => {
        this.onSubmit.emit(data);
      })
  }


  actionDownloadTemplate() {
    this.openCloseScheduleService.downloadTemplateImport().subscribe(res => {
      saveAs(res, 'TemplateCloseOpenTime.xlsx');
    });
  }
}
