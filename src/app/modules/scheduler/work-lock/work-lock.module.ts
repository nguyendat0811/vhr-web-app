import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WorkLockComponent } from './work-lock-index/work-lock.component';
import { WorkLockRoutingModule } from './work-lock-routing.module';
import { SharedModule } from '@app/shared';
import { WorkLockGridComponent } from './work-lock-grid/work-lock-grid.component';
@NgModule({
  declarations: [WorkLockComponent, WorkLockGridComponent],
  imports: [
    CommonModule,
    SharedModule,
    WorkLockRoutingModule
  ],
  entryComponents: [WorkLockComponent]
})
export class WorkLockModule { }
