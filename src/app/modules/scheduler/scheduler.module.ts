import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from "@app/shared";
import { SchedulerRoutingModule } from './scheduler-routing.module';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ImportFileClientComponent } from './import-file-client/import-file-client.component';
// import { ShiftSchedularModule } from './shift-schedular/shift-schedular.module';
// import { ShiftLockModule } from './shift-lock/shift-lock.module';

// import { ShiftAssignComponent } from './shift-lock/shift-assign/shift-assign.component';
// import { ShiftFormComponent } from './shift-lock/shift-form/shift-form.component';

@NgModule({

  declarations: [ImportFileClientComponent],
  imports: [
    CommonModule,
    SharedModule,
    SchedulerRoutingModule
  ],
  entryComponents: [ImportFileClientComponent],
  providers: [NgbActiveModal]
})
export class SchedulerModule { }
