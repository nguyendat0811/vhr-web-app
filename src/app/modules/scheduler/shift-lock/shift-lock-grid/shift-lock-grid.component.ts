import { Component, Input, OnInit, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { LockShiftOrg, WorkDay } from '@app/core/models/employee.model';
@Component({
  selector: 'shift-lock-grid',
  templateUrl: './shift-lock-grid.component.html',
  styleUrls: ['./shift-lock-grid.component.css']
})
export class ShiftLockGridComponent implements OnInit, OnChanges {

  constructor() { }

  @Input() public lockShiftOrgs: LockShiftOrg[];
  @Input() public month: Date;
  @Input() public searchStartDate?: Date;
  @Input() public searchEndDate?: Date;
  @Output() public onUpdateLock: EventEmitter<any> = new EventEmitter<any>();

  setSearchDays() {
    this.searchDays = []
    for (let currentDay = new Date(this.searchStartDate); currentDay <= this.searchEndDate; currentDay.setDate(currentDay.getDate() + 1)) {
      this.searchDays.push(new WorkDay(currentDay.getDate(), "", "", currentDay.getDay(), `app.schedular.day.${currentDay.getDay() + 1}`, true, false));
    }
  }

  ngOnInit() {
    this.month = this.month || new Date();
    const date = new Date(this.month.valueOf()), y = date.getFullYear(), m = date.getMonth();
    const firstDay = new Date(y, m, 1);
    const lastDay = new Date(y, m + 1, 0);

    this.searchStartDate = this.searchStartDate || new Date(firstDay);
    this.searchEndDate = this.searchEndDate || new Date(lastDay);

    this.setSearchDays();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.searchStartDate || changes.searchEndDate) { this.setSearchDays(); }
  }

  searchDays: WorkDay[] = [];

  isLocked(day: WorkDay, lockShiftOrg: LockShiftOrg): boolean {
    return lockShiftOrg.data.some(lockShift => {
      const lockShiftDate = new Date(lockShift.shiftsDate);
      return lockShift.isLocked && lockShiftDate.getDate() == day.date
    });
  }

  onClickUpdateLock(organizationId: number, isLock: boolean) {
    const data = { organizationId, isLock, fromDate: this.searchStartDate, toDate: this.searchEndDate };
    this.onUpdateLock.emit(data);
  }

  formatOrganizationName(lockShiftOrg: LockShiftOrg): string {
    if (!lockShiftOrg.relativeLevel)
      return lockShiftOrg.organizationName;
    return `${"--".repeat(lockShiftOrg.relativeLevel)} ${lockShiftOrg.organizationName}`;
  }
}

