import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { RESOURCE } from "@app/core";
import { CommonUtils } from "@app/shared/services";
import { ShiftAssignComponent } from "./shift-assign/shift-assign.component";


const routes: Routes = [
  {
    path: '',
    component: ShiftAssignComponent,
    data: {
      resource: RESOURCE.WORK_SCHEDULE,
      nationId: CommonUtils.getNationId()
    },
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShiftLockRoutingModule { }
