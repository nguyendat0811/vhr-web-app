import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShiftAssignComponent } from './shift-assign/shift-assign.component';
import { ShiftLockRoutingModule } from './shift-lock-routing.module';
import { SharedModule } from '@app/shared';
import { ShiftLockGridComponent } from './shift-lock-grid/shift-lock-grid.component';
@NgModule({
  declarations: [ShiftAssignComponent, ShiftLockGridComponent],
  imports: [
    CommonModule,
    SharedModule,
    ShiftLockRoutingModule
  ],
  entryComponents: [ShiftAssignComponent]
})
export class ShiftLockModule { }
