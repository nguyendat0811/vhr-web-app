import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { RESOURCE } from "@app/core";
import { CommonUtils } from "@app/shared/services";
// import { ShiftAssignComponent } from "./shift-lock/shift-assign/shift-assign.component";

const routes: Routes = [
  {
    path: '',
    loadChildren: './shift-schedular/shift-schedular.module#ShiftSchedularModule'
  },
  {
    path: 'shift-schedular',
    loadChildren: './shift-schedular/shift-schedular.module#ShiftSchedularModule'
  },
  {
    path: 'shift-lock',
    loadChildren: './shift-lock/shift-lock.module#ShiftLockModule'
  },
  {
    path: 'shift-assignment-table',
    loadChildren: './shift-assignment-table/shift-assignment-table.module#ShiftAssignmentTableModule'
  },
  {
    path: 'shift-lock/form',
    loadChildren: './work-lock/work-lock.module#WorkLockModule'
  },
  {
    path: 'timekeeping/lock',
    loadChildren: './timesheet-lock/timesheet-lock.module#TimeSheetLockModule'
  },
  // {
  //   path:'shift-schedular',
  //   component:ShiftSchedularComponent,
  //   data: {
  //     resource: RESOURCE.WORK_SCHEDULE,
  //     nationId: CommonUtils.getNationId()
  //   },
  //   pathMatch: 'full'
  // },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SchedulerRoutingModule { }
