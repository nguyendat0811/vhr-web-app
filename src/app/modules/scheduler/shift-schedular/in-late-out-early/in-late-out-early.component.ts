import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AppComponent } from '@app/app.component';
import { ACTION_FORM, RESOURCE } from '@app/core';
import { SchedularService } from '@app/core/services/schedular/schedular.service';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { CommonUtils, ValidationService } from '@app/shared/services';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import * as moment from "moment";
@Component({
  selector: 'in-late-out-early',
  templateUrl: './in-late-out-early.component.html',
  styleUrls: ['./in-late-out-early.component.css']
})
export class InLateOutEarlyComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  dataError: any;
  formConfig = {
    employeeId: ['',],
    organizationId: ['',],// [ValidationService.required]],
    fromDate: ['', [ValidationService.required]],
    toDate: ['', [ValidationService.required]],
    isCurrent: [0],
  };
  currentDate = new Date();

  constructor(
    public activatedRoute: ActivatedRoute,
    private app: AppComponent,
    public activeModal: NgbActiveModal,
    public schedularService: SchedularService,
  ) {
    super(activatedRoute, RESOURCE.SHIFT_ASSIGN, ACTION_FORM.INTERIOR_SEARCH);
    this.formSave = this.buildForm({
      fromDate: this.currentDate,
      toDate: new Date(this.currentDate.getFullYear(), this.currentDate.getMonth() + 1, 0, 59, 59, 59),
    }
      , this.formConfig
      , ACTION_FORM.INTERIOR_SEARCH
      , [
        ValidationService.notAffter('fromDate', 'toDate', 'app.salaryTable.steps.expiredDate'),
        // ValidationService.notOnMonth('toDate', 'fromDate', 'common.label.error.onMonth'),
      ]);

  }
  labourContractDetailList: any;
  get organizationId(): AbstractControl {
    return this.formSave.controls["organizationId"];
  }
  get f() {
    return this.formSave.controls;
  }
  get employeeId(): AbstractControl {
    return this.formSave.controls["employeeId"];
  }
  get fromDate(): AbstractControl {
    return this.formSave.controls["fromDate"];
  }
  get toDate(): AbstractControl {
    return this.formSave.controls["toDate"];
  }
  shiftEmploys = [];
  selectedEmployees = []
  visiblePopup = false;
  ngOnInit() {

  }
  employeeSelected: any;
  onEmployeeChange(value) {
    this.employeeSelected = value;
  }
  getParam() {
    let param = {...this.formSave.value};
    param.employeeId = this.employeeSelected?this.employeeSelected.codeField: param.employeeId ;
    param.fromDate = moment(param.fromDate).format("yyyy-MM-dd");
    param.toDate = moment(param.toDate).format("yyyy-MM-dd");
    return param;
  }
  onSearch() {
    if (!CommonUtils.isValidForm(this.formSave))
      return;
    var param = this.getParam();
    this.shiftEmploys.push({



    })
  }
  getReport() {

  }
  view(value) {
    console.log(value);

  }
}
