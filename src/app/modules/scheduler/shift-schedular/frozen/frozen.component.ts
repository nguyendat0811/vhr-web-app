import { Component, OnInit } from '@angular/core';
import { LazyLoadEvent } from 'primeng/api';
import { SelectItem } from 'primeng/api';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'frozen',
  templateUrl: './frozen.component.html',
  styleUrls: ['./frozen.component.css']
})
export class FrozenComponent implements OnInit {

  constructor() { }


// import { Component } from '@angular/core';
// import { CustomerService } from './customerservice';
// import { Customer } from './customer';
// import { FilterUtils } from 'primeng/utils';



  customers: Customer[];

  frozenValue: Customer[];

  dialogVisible: boolean;

  scrollableCols: any[];

  frozenCols: any[];
  card: any;


  ngOnInit() {
    this.card = [
      {
        rule: 'ABC',
        selected_tab: 'tab1',
        proj_id: 123,
        attributes: [
          {
            name: 'key',
            isPK: true,
            type: 'string',
            required: 'true',
            maxLength: 10,
            minLength: 4
          },
          {
            name: 'id',
            isPK: true,
            type: 'string',
            required: 'true',
            maxLength: 10,
            minLength: 4
          },
          {
            name: 'type',
            isPK: false,
            length: 10,
            values: ['test1', 'test2']
          },
          {
            name: 'currency',
            isPK: false,
            length: 10,
            values: ['cad', 'inr', 'usd']
          },
          {
            name: 'code',
            isPK: false,
            length: 10,
            values: ['aa', 'bc', 'cd'],
            required: 'true'
          }
        ],
        TABS: [
          {
            name: 'tab1',
            tab_id: 'tab1'
          },
          {
            name: 'tab2',
            tab_id: 'tab2'
          },
          {
            name: 'tab3',
            tab_id: 'tab3'
          },
          {
            name: 'tab4',
            tab_id: 'tab4'
          }
        ]
      }
    ];



    this.frozenValue = [
      {
        id: 1255,
        name: 'James McAdams',
        country: {
          name: 'United States',
          code: 'us'
        },
        company: 'McAdams Consulting Ltd',
        date: '2014-02-13',
        status: 'qualified',
        activity: 23,
        representative: {
          name: 'Ioni Bowcher',
          image: 'ionibowcher.png'
        }
      },
      {
        id: 5135,
        name: 'Geraldine Bisset',
        country: {
          name: 'France',
          code: 'fr'
        },
        company: 'Bisset Group',
        status: 'proposal',
        date: '2019-05-05',
        activity: 0,
        representative: {
          name: 'Amy Elsner',
          image: 'amyelsner.png'
        }
      }
    ];

    this.frozenCols = [
      { field: 'name', header: 'Name' },
      { field: 'id', header: 'Id' }
    ];

    this.scrollableCols = [
      { field: 'date', header: 'Date' },
      { field: 'company', header: 'Company' },
      { field: 'status', header: 'Status' },
      { field: 'activity', header: 'Activity' }
    ];
  }

  showDialog() {
    this.dialogVisible = true;
  }
  isSubmitted = false;
  onAddRow(card) {
   
  }
  onChange($event, i, j, card){
    
  }
}

export interface Country {
  name?: string;
  code?: string;
}

export interface Representative {
  name?: string;
  image?: string;
}

export interface Customer {
  id?: number;
  name?: string;
  country?: Country;
  company?: string;
  activity: number;
  date?: string;
  status?: string;
  representative?: Representative;
}