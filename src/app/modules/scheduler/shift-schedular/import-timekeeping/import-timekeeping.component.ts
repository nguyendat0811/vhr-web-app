import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AppComponent } from '@app/app.component';
import { ACTION_FORM, RESOURCE } from '@app/core';
import { ShiftEmploy, WorkDay } from '@app/core/models/employee.model';
import { HrShiftsEmployeeDetail, RepositoryPaging, timekeeping } from '@app/core/models/org-model';
import { HrStorage } from '@app/core/services/HrStorage';
import { IHeaderConfig, IParamExport, ReadFileSchedularService } from '@app/core/services/schedular/read-file-schedular.service';
import { SchedularService } from '@app/core/services/schedular/schedular.service';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { CommonUtils, ValidationService } from '@app/shared/services';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { ExportImportTemplateConfig } from '../../shift-assignment-table/export-import-template-config/export-import-template-config';

@Component({
  selector: 'import-timekeeping',
  templateUrl: './import-timekeeping.component.html',
  styleUrls: ['./import-timekeeping.component.css']
})
export class ImportTimekeepingComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  dataError: any;
  formConfig = {
    fileImport: ['', [ValidationService.required]],
    organizationId: ['', [ValidationService.required]],
    fromDate: ['', [ValidationService.required]],
    toDate: ['', [ValidationService.required]],
    isCurrent: [0],
  };
  constructor(
    public activatedRoute: ActivatedRoute,
    private app: AppComponent,
    public activeModal: NgbActiveModal,
    public schedularService: SchedularService,
    public clientDataReader: ReadFileSchedularService
  ) {
    super(activatedRoute, RESOURCE.SHIFT_ASSIGN, ACTION_FORM.INTERIOR_SEARCH);
    this.formSave = this.buildForm(
      {
        fromDate: this.currentDate,
        toDate: new Date(this.currentDate.getFullYear(), this.currentDate.getMonth() + 1, 0, 23, 59, 59),
      }
      , this.formConfig
      , ACTION_FORM.INTERIOR_SEARCH
      , [
        ValidationService.notAffter('fromDate', 'toDate', 'app.salaryTable.steps.expiredDate'),
        // ValidationService.notOnMonth('endDate', 'startDate', 'common.label.error.onMonth'),
      ]);

  }
  currentDate = new Date();
  disableSubmit: boolean;
  organizationSelected: any;
  organizationOnChange(value) {
    this.organizationSelected = value;
  };
  get f() {
    return this.formSave.controls;
  }
  cancel() {
    this.sourceError = [];
  }
  sourceError: Array<any>;
  handlerError(code: number, value?: any): string {
    let key = 'ERROR.schedular.' + ExportImportTemplateConfig.getMessengerError(code);
    return this.app.validateMessenger(key, value);
  }
  actionSubmitForm() {
    if (!CommonUtils.isValidForm(this.formSave))
      return;
    const config = this.getReportConfig();
    this.clientDataReader.readFile<ShiftEmploy>(this.formSave.value["fileImport"], config)
      .subscribe((data) => {
        let source = this.transformDataImport(data.Datasource);
        this.schedularService.updateTimeKeeping(source).subscribe((res: RepositoryPaging<timekeeping>) => {
          if (res && res.code == 0)
            return this.app.successMessage("updateSuccess");

          console.log(res, 'source : ', source);
          if (res && res.code != -2)
            return this.app.message("error", this.handlerError(res.code, res))

          let data = res.data && res.data.timeSheetsUpdateErrors.dataTimeSheets
          for (const iterator of data) {
            if (iterator.errorCode)
              return this.app.message('error', this.getErrorMessenger(iterator.errorCode, iterator));
            for (const day of iterator.timeSheets) {
              if (day.errorCode)
                return this.app.message('error', this.getErrorMessenger(day.errorCode, day));
            }
          }
        }, error => {
          this.app.requestIsError();
        });

      });

  }
  fileName = "ImportChamcongCTV4";
  downloadFileError() {
    let config = this.getReportConfig([{ index: 5, title: 'Thông tin lỗi', key: 'error', width: 20, fixed: true, parentHeader: 0 }]);

    let sheet = this.clientDataReader.getWorkSheetExport(this.sourceError, this.fileName, config)

    this.clientDataReader.exportFile(sheet, this.fileName + "_error");

  }
  buttonClick() {
    this.openPopup = !this.openPopup;
  }
  openPopup = false;
  transformDataImport(data: ShiftEmploy[]) {
    let param = { ...this.getParam() };
    const month = new Date(this.formSave.value['fromDate']).getMonth();
    const year = new Date(this.formSave.value['fromDate']).getFullYear();
    param.dataTimeSheets = Array<any>();

    for (const iterator of data) {
      param.dataTimeSheets.push({
        employeeId: iterator.employeeCode,
        timeSheets: iterator.workDay.map(x => {
          return {
            workDay: moment(new Date(year, month, x.date)).format("YYYY-MM-DD"),
            timesheetDetails: x.shiftCodes
          }
        })
      });
    }
    return param;
  }
  ngOnInit() {
  }
  getReportConfig(header?: IHeaderConfig[]): IParamExport {
    let param = this.getParam()
    let configReport = <IParamExport>{
      fromDate: new Date(param["fromDate"]),
      toDate: new Date(param["toDate"]),
      orgMaster: this.organizationSelected && this.organizationSelected.name || '',
      tittleConfig: [],
      headerStart: 1,

    };
    configReport.headers = [{ index: 1, title: 'STT', key: 'index', width: 5, fixed: true, parentHeader: 0 },
    { index: 2, title: 'Mã nhân viên', key: 'employeeCode', width: 10, fixed: true, parentHeader: 0 },
    { index: 3, title: 'Họ tên', key: 'employeeName', width: 20, fixed: true, parentHeader: 0 },
    { index: 4, title: 'Thông tin chấm công', key: null, width: 6, fixed: true, parentHeader: 0 }]
      .concat(this.clientDataReader.getDynamicHeader(configReport.fromDate, configReport.toDate, 4));
    configReport.headers = header ? configReport.headers.concat(header) : configReport.headers;

    return configReport;
  }
  getParam() {
    var param = this.formSave.value;
    param.fromDate = moment(param["fromDate"]).format("YYYY-MM-DD");
    param.toDate = moment(param["toDate"]).format("YYYY-MM-DD");
    param.orgenizationId = param.organizationId;
    param.organizationId = param.organizationId;
    param.page = 0;
    param.size = 100;
    return param;
  }
  getErrorMessenger(code: number, value?: any): string {
    let key = 'ERROR.schedular.' + ExportImportTemplateConfig.getMessengerError(code);
    return this.app.validateMessenger(key, value);
  }
  actionDownloadTemplate() {
    var param = this.getParam()
    var config = this.getReportConfig()

    if (!param.organizationId) {
      this.clientDataReader.exportAsExcelFile([], this.fileName, config);
      return;
    }

    this.schedularService.getTimeKeeping(param).subscribe((res: RepositoryPaging<timekeeping>) => {
      if (!res) return this.app.errorMessage("haveError")
      if (res.code) return this.app.message('error', this.getErrorMessenger(res.code, param));
      let data = res.data.timeSheets;

      let source = data.map(x => {
        return <ShiftEmploy>{
          employeeCode: x.employee.employeeCode,
          fullName: x.employee.fullName,
          organizationId: x.employee.organizationId,
          organizationCode: x.employee.organization.code,
          organizationName: x.employee.organization.name,
          employeeName: x.employee.fullName,
          workDayControls: {},
          workDay: x.sheets.map(s => {
            const day = new Date(s.workDay);
            return <WorkDay>{
              date: day.getDate(),
              dayOfWeek: day.getDay(),
              shiftCodes: s.hrMdWorkType.workCode,
              shiftCodeRaw: s.hrMdWorkType.workCode,
              shiftDate: moment(day).format('YYYY-MM-DD')
            }
          }),
        }
      })
      this.clientDataReader.exportAsExcelFile(source, this.fileName, config);

    });
  }

}

