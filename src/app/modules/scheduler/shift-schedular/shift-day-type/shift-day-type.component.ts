import { Component, Input, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { SchedularService } from '@app/core/services/schedular/schedular.service';
import { ACTION_FORM, RESOURCE } from '@app/core';
import { ActivatedRoute } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'shift-day-type',
  templateUrl: './shift-day-type.component.html',
})
export class ShiftDayTypeComponent  implements OnInit {

  @Input() public workTypeSource: Array<any>;
  @Input() public page: number;
  @Input() public workTypeId: number;
  @Input() public pageTotal: number;
  constructor(

  ) {
  }
  ngOnInit() {
  }
  formatTime(value) {
    return value ? value.substr(0, 5) : '';
  }
  // customers = [];
}
