import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from "@app/shared";
import { ShiftSchedularRoutingModule } from './shift-schedular-routing.module';
import { ShiftSchedularIndexComponent } from './shift-schedular-index/shift-schedular-index.component';
import { ShiftDayTypeComponent } from './shift-day-type/shift-day-type.component';
import { ShiftSchedularTableComponent } from './shift-schedular-table/shift-schedular-table.component';
import { ImportShiftSchedularComponent } from './import-shift-schedular/import-shift-schedular.component';
import { HistoryAssignmentComponent } from './history-assignment/history-assignment.component';
import { InLateOutEarlyComponent } from './in-late-out-early/in-late-out-early.component';
import { TimeKeepingComponent } from './time-keeping/time-keeping.component';
import { EmpTimekeepingComponent } from './emp-timekeeping/emp-timekeeping.component';
import { ImportTimekeepingComponent } from './import-timekeeping/import-timekeeping.component';
import { TimeKeepingTableComponent } from './time-keeping/time-keeping-table/time-keeping-table.component';
@NgModule({
  declarations: [ShiftSchedularIndexComponent, ShiftDayTypeComponent, ShiftSchedularTableComponent, ImportShiftSchedularComponent, HistoryAssignmentComponent, InLateOutEarlyComponent, TimeKeepingComponent, EmpTimekeepingComponent, ImportTimekeepingComponent, TimeKeepingTableComponent],
  imports: [
    CommonModule,
    ShiftSchedularRoutingModule,
    SharedModule
  ],
  entryComponents:[ShiftSchedularIndexComponent]
})
export class ShiftSchedularModule { }
