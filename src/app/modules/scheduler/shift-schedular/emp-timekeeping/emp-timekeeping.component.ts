import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AppComponent } from '@app/app.component';
import { ACTION_FORM, RESOURCE } from '@app/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ValidationService, CommonUtils } from '@app/shared/services';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SchedularService } from '@app/core/services/schedular/schedular.service';
import { ShiftEmploy, WorkDay } from '@app/core/models/employee.model';
import { RepositoryPaging, timekeeping, warningReportEmployee, warningReportOrganization } from '@app/core/models/org-model';
import { formatDate } from '@angular/common';
import { ExportImportTemplateConfig } from '../../shift-assignment-table/export-import-template-config/export-import-template-config';
import { ShiftSchedularTableComponent } from '../shift-schedular-table/shift-schedular-table.component';
import * as moment from 'moment';
import { ReadFileSchedularService } from '@app/core/services/schedular/read-file-schedular.service';

@Component({
  selector: 'emp-timekeeping',
  templateUrl: './emp-timekeeping.component.html',
  styleUrls: ['./emp-timekeeping.component.css']
})
export class EmpTimekeepingComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  dataError: any;
  formConfig = {
    employeeId: ['',],
    organizationId: ['',],
    fromDate: ['', [ValidationService.required]],
    toDate: ['', [ValidationService.required]],
    isCurrent: [0],
  };
  currentDate = new Date();
  public shiftEmploy: ShiftEmploy[];
  @ViewChild('table') table: ShiftSchedularTableComponent;
  constructor(
    public activatedRoute: ActivatedRoute,
    private app: AppComponent,
    public activeModal: NgbActiveModal,
    public schedularService: SchedularService,
    public clientDataReader: ReadFileSchedularService,
  ) {
    super(activatedRoute, RESOURCE.SHIFT_ASSIGN, ACTION_FORM.INTERIOR_SEARCH);
    this.formSave = this.buildForm({
      fromDate: this.currentDate,
      toDate: new Date(this.currentDate.getFullYear(), this.currentDate.getMonth() + 1, 0),
    }
      , this.formConfig
      , ACTION_FORM.INTERIOR_SEARCH
      , [
        ValidationService.notAffter('fromDate', 'toDate', 'common.label.toDate'),
        // ValidationService.notOnMonth('toDate', 'fromDate', 'common.label.error.onMonth'),
      ]);

  }
  ngOnInit(): void {
    this.workTypeSource = this.workTypeSource || [];
    this.schedularService.getWorkType()
      .subscribe((data: Array<any>) => {
        this.workTypeSource = data.filter(x => x.workTypeId == 0);
        console.log("workType ", data, "workTypeCheck ", this.workTypeSource);

      });
  }
  get f() {
    return this.formSave.controls;
  }
  getErrorMessenger(code: number, value?: any): string {
    let key = 'ERROR.schedular.' + ExportImportTemplateConfig.getMessengerError(code);
    return this.app.validateMessenger(key, value);
  }
  employeeSelected: any;
  onEmployeeChange(value) {
    this.employeeSelected = value;
  }
  getParam() {
    let param = { ...this.formSave.value };
    param.fromDate = formatDate(new Date(param["fromDate"]), "yyyy-MM-dd", 'en');
    param.toDate = formatDate(new Date(param["toDate"]), "yyyy-MM-dd", 'en');
    param.orgenizationId = param.organizationId;
    param.organizationId = param.organizationId;
    param.employeeId = this.employeeSelected ? this.employeeSelected.codeField : param.employeeId;
    param.page = 0;
    param.size = 100;
    return param;
  }
  public notOnMonth() {
    const target = this.f["toDate"];
    const toMatch = this.f["fromDate"];
    if (target.hasError('dateNotSameMonth')) {
      target.setErrors(null);
      target.markAsUntouched();
    }

    if (target.value && toMatch.value) {
      const fromDate = new Date(target.value);
      const toDate = new Date(toMatch.value);
      const isCheck = fromDate.getFullYear() === toDate.getFullYear()
        && fromDate.getMonth() === toDate.getMonth();
      // set equal value error on dirty controls
      if (!isCheck && target.valid && toMatch.valid) {
        target.setErrors({ dateNotSameMonth: { dateNotSameMonth: "common.label.error.onMonth" } });
        target.markAsTouched();
        return false;
      }
    }
    return true;
  }
  workTypeSource = [];
  onSearch() {
    if (!CommonUtils.isValidForm(this.formSave) || !this.notOnMonth())
      return;
    this.shiftEmploy = [];
    var param = this.getParam();
    this.schedularService.getTimeKeeping(param).subscribe((res: RepositoryPaging<timekeeping>) => {
      console.log(res);

      if (!res) return this.app.errorMessage("haveError")
      if (res.code) return this.app.message('error', this.getErrorMessenger(res.code, param));

      let data = res.data.timeSheets;
      this.shiftEmploy = data.map(x => {
        return <ShiftEmploy>{
          employeeCode: x.employee.employeeCode,
          fullName: x.employee.fullName,
          organizationId: x.employee.organizationId,
          organizationCode: x.employee.organization.code,
          organizationName: x.employee.organization.name,
          employeeName: x.employee.fullName,
          position : x.position? x.position.name : '',
          workDayControls: {},
          workDay: x.sheets.map(s => {
            const day = new Date(s.workDay);
            return <WorkDay>{
              date: day.getDate(),
              dayOfWeek: day.getDay(),
              shiftCodes: s.hrMdWorkType.workCode,
              shiftCodeRaw: s.hrMdWorkType.workCode,
              shiftDate: formatDate(day, 'yyyy-MM-dd', 'en')
            }
          }),
        }
      })
      this.table.onLoadData(this.shiftEmploy, param.fromDate, param.toDate);
    });
  }
  onSubmit(data: ShiftEmploy[]) {
    if (!data || !data.length) {
      this.app.successMessage("noChange");
      return;
    }
    let param = this.getParam();
    param.dataTimeSheets = Array<any>();

    for (const iterator of data) {
      param.dataTimeSheets.push({
        employeeId: iterator.employeeCode,
        timeSheets: iterator.workDay.map(x => {
          return {
            workDay: x.shiftDate,
            timesheetDetails: x.shiftCodes
          }
        })
      });
    }

    this.schedularService.updateTimeKeeping(param).subscribe((res: RepositoryPaging<timekeeping>) => {
      if (res && res.code == 0) {
        this.app.successMessage("updateSuccess");
        this.onSearch();
        return;
      }
      console.log(res);
      if (res && res.code != -2)
        return this.app.message('error', this.getErrorMessenger(res.code, res));

      let data = res.data && res.data.timeSheetsUpdateErrors.dataTimeSheets
      for (const iterator of data) {
        if (iterator.errorCode)
          return this.app.message('error', this.getErrorMessenger(iterator.errorCode, iterator));
        for (const day of iterator.timeSheets) {
          if (day.errorCode)
            return this.app.message('error', this.getErrorMessenger(day.errorCode, day));
        }
      }
    }, error => {
      this.app.requestIsError();
    });

  }

}
