import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AppComponent } from '@app/app.component';
import { ACTION_FORM, RESOURCE } from '@app/core';
import { ShiftEmploy, WorkDay } from '@app/core/models/employee.model';
import { HrShiftsEmployeeDetail, RepositoryPaging, ShiftEmployOrg } from '@app/core/models/org-model';
import { HrStorage } from '@app/core/services/HrStorage';
import { IParamExport, ReadFileSchedularService } from '@app/core/services/schedular/read-file-schedular.service';
import { SchedularService } from '@app/core/services/schedular/schedular.service';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { CommonUtils, ValidationService } from '@app/shared/services';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { ExportImportTemplateConfig } from '../../shift-assignment-table/export-import-template-config/export-import-template-config';
@Component({
  selector: 'import-shift-schedular',
  templateUrl: './import-shift-schedular.component.html',
  styleUrls: ['./import-shift-schedular.component.css']
})
export class ImportShiftSchedularComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  dataError: any;
  formConfig = {
    fileImport: ['', [ValidationService.required]],
    organizationId: ['', [ValidationService.required]],
    fromDate: ['', [ValidationService.required]],
    toDate: ['', [ValidationService.required]],
    isCurrent: [0],
  };
  constructor(
    public activatedRoute: ActivatedRoute,
    private app: AppComponent,
    public activeModal: NgbActiveModal,
    public schedularService: SchedularService,
    public clientDataReader: ReadFileSchedularService
  ) {
    super(activatedRoute, RESOURCE.SHIFT_ASSIGN, ACTION_FORM.INTERIOR_SEARCH);
    this.formSave = this.buildForm(
      {
        fromDate: this.currentDate,
        toDate: new Date(this.currentDate.getFullYear(), this.currentDate.getMonth() + 1, 0, 23, 59, 59),
      }
      , this.formConfig
      , ACTION_FORM.INTERIOR_SEARCH
      , [
        ValidationService.notAffter('fromDate', 'toDate', 'app.salaryTable.steps.expiredDate'),
        // ValidationService.notOnMonth('endDate', 'startDate', 'common.label.error.onMonth'),
      ]);

  }
  currentDate = new Date();
  disableSubmit: boolean;
  organizationSelected: any;
  organizationOnChange(value) {
    this.organizationSelected = value;
  };
  get f() {
    return this.formSave.controls;
  }
  cancel() {
    this.sourceError = [];
  }
  sourceError: Array<any>;
  handlerError(code: number, value?: any): string {
    let key = 'ERROR.schedular.' + ExportImportTemplateConfig.getMessengerError(code);
    return this.app.validateMessenger(key, value);
  }
  actionSubmitForm() {
    if (!CommonUtils.isValidForm(this.formSave))
      return;

    this.clientDataReader.readFile<ShiftEmploy>(this.formSave.value["fileImport"], {
      fromDate: this.formSave.value["fromDate"],
      toDate: this.formSave.value["toDate"],
      orgMaster: this.organizationSelected && this.organizationSelected.name || '',
    })
      .subscribe((data) => {
        let source = this.transformDataImport(data.Datasource);
        this.schedularService.importShiftEmploy(source).subscribe(res => {
          if (res && res.code == 0)
            this.app.successMessage("updateSuccess");
          else {
            // let mess = ExportImportTemplateConfig.getMessengerError(res.code);
            console.log(res, data.Datasource);
            if (res && res.code != -2) {
              return this.app.message("error", this.handlerError(res.code, res))
            }
            let error = res.data && res.data.shiftEmployeeImportErrors as ShiftEmployeeImportModel;
            for (const item of error.dataShifts) {
              var employee = data.Datasource.find(x => x.employeeCode == item.employeeId);
              employee.error = '';
              if (item.errorCode)
                employee.error = this.handlerError(item.errorCode, employee);

              if (employee) {
                for (const dayShift of item.shift) {
                  if (dayShift.errorCode == 0)
                    continue;
                  employee.error += `Ngày: ${dayShift.shiftDate}: \n Ca ${dayShift.shiftDetails} vi phạm
                    ${this.handlerError(dayShift.errorCode, dayShift)}\n`;
                }
              }

            }
            this.sourceError = data.Datasource;
            //Show popup
            this.openPopup = !this.openPopup;

          }

        }, error => {
          this.app.requestIsError();
        });

      });

  }
  downloadFile() {
    const config = this.getReportConfig()
    config.headers = this.clientDataReader.getHeader(config)
      .concat({ index: 9, title: 'Thông tin lỗi', key: 'error', width: 20, fixed: true, parentHeader: 0 });

    const sheet = this.clientDataReader.getWorkSheetExport(this.sourceError, this.fileName + "_CanhBao", config);
    this.clientDataReader.exportFile(sheet, this.fileName);

  }
  buttonClick() {
    this.openPopup = !this.openPopup;
  }
  openPopup = false;
  fileName = "Lập lịch phân ca";
  transformDataImport(data: ShiftEmploy[]): ShiftEmployeeImportModel {
    var dataShift = Array<shift>();
    const fromDate = new Date(this.formSave.value['fromDate']).getDate();
    const toDate = new Date(this.formSave.value['toDate']).getDate();
    const month = new Date(this.formSave.value['fromDate']).getMonth();
    const year = new Date(this.formSave.value['fromDate']).getFullYear();
    const user = HrStorage.getUserToken();

    for (const item of data) {
      let employeeDay = {
        employeeId: item.employeeCode,
        shift: [],
      };
      for (let index = 0; index < item.workDay.length; index++) {
        const element = item.workDay[index];
        if (element.date < fromDate || element.date > toDate)
          continue;

        employeeDay.shift.push({
          shiftDate: moment(new Date(year, month, element.date)).format("YYYY-MM-DD"),
          dayCode: element.title && element.title.slice(element.title.indexOf("(") + 1, element.title.length - 1),
          shiftDetails: element.shiftCodes,
        });
      }
      dataShift.push(employeeDay);
    }
    var obj = {
      employeeIdEdit: user.employeeCode, //hash code
      organizationId: +this.formSave.value["organizationId"],
      dataShifts: dataShift,
      fromDate: moment(this.formSave.value['fromDate']).format("YYYY-MM-DD"),
      toDate: moment(this.formSave.value['toDate']).format("YYYY-MM-DD"),
    }

    return obj;
  }
  ngOnInit() {
  }

  getParam() {
    var param = this.formSave.value;
    param.fromDate = moment(param["fromDate"]).format("YYYY-MM-DD");
    param.toDate = moment(param["toDate"]).format("YYYY-MM-DD");
    param.orgenizationId = param.organizationId;
    param.organizationId = param.organizationId;
    param.page = 0;
    param.size = 100;
    return param;
  }
  getReportConfig(): IParamExport {
    let param = this.getParam()
    let configReport = <IParamExport>{
      fromDate: new Date(param["fromDate"]),
      toDate: new Date(param["toDate"]),
      orgMaster: this.organizationSelected && this.organizationSelected.name || '',
    };

    return configReport;
  }
  modelMapping(data: ShiftEmployOrg[]): ShiftEmploy[] {
    return data.map(x => {
      return <ShiftEmploy>{
        employeeCode: x.employee.employeeCode,
        fullName: x.employee.fullName,
        organizationId: x.employee.organizationId,
        organizationCode: x.employee.organization.code,
        organizationName: x.employee.organization.name,
        employeeName: x.employee.fullName,
        position: x.position ? x.position.name : '',
        workDayControls: {},
        verified: true,
        workDay: x.shiftsEmployees.map(s => {
          const day = new Date(s.shiftsDate);
          return <WorkDay>{
            date: day.getDate(),
            dayOfWeek: day.getDay(),
            shiftCodes: s.workCode,
            shiftCodeRaw: s.workCode,
          }
        }),
      }
    })
  }
  actionDownloadTemplate() {
    const param = this.getParam();
    const config = this.getReportConfig();
    if (!param.organizationId) {
      this.clientDataReader.exportAsExcelFile([], this.fileName, config);
      return;
    }
    this.schedularService.getShiftEmployee(param).subscribe((res: RepositoryPaging<HrShiftsEmployeeDetail>) => {
      if (!res) return;
      let source = this.modelMapping(res.data.shifts);
      this.clientDataReader.exportAsExcelFile(source, this.fileName, config);
    });
  }

}
//Cái này phải sửa lại này
export interface shift {
  employeeId: string;
  shift: dayShift[];
  errorCode?: number;
  errorMessage?: string;
}

export interface dayShift {
  shiftDate: string
  shiftDetails: string;
  errorCode?: number,
  errorMessage?: string,
}
export interface ShiftEmployeeImportModel {

  employeeIdEdit: string;
  fromDate: string,
  toDate: string,
  organizationId: number;
  dataShifts: shift[]
  errorCode?: number
  errorMessage?: string
}
