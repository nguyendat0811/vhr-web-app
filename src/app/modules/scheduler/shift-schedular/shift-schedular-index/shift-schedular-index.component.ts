import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AppComponent } from '@app/app.component';
import { ACTION_FORM, RESOURCE } from '@app/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ValidationService, CommonUtils } from '@app/shared/services';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SchedularService } from '@app/core/services/schedular/schedular.service';
import { ShiftEmploy, WorkDay } from '@app/core/models/employee.model';
import { employeeWarningReport, HrShiftsEmployeeDetail, organizationWarningReport, RepositoryPaging, warningReportEmployee, warningReportOrganization } from '@app/core/models/org-model';
import { ShiftEmployeeImportModel, shift } from '../import-shift-schedular/import-shift-schedular.component';
import { HrStorage } from '@app/core/services/HrStorage';
import { formatDate } from '@angular/common';
import { ExportImportTemplateConfig } from '../../shift-assignment-table/export-import-template-config/export-import-template-config';
import { ShiftSchedularTableComponent } from '../shift-schedular-table/shift-schedular-table.component';
import * as moment from 'moment';
import { ReadFileSchedularService } from '@app/core/services/schedular/read-file-schedular.service';
@Component({
  selector: 'shift-schedular-index',
  templateUrl: './shift-schedular-index.component.html',
  styleUrls: ["./shift-schedular-index.component.css"],
})
export class ShiftSchedularIndexComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  dataError: any;
  formConfig = {
    employeeId: ['',],
    organizationId: ['',],// [ValidationService.required]],
    fromDate: ['', [ValidationService.required]],
    toDate: ['', [ValidationService.required]],
    isCurrent: [0],
  };
  currentDate = new Date();
  public shiftEmploy: ShiftEmploy[];
  @ViewChild('table') table: ShiftSchedularTableComponent;
  constructor(
    public activatedRoute: ActivatedRoute,
    private app: AppComponent,
    public activeModal: NgbActiveModal,
    public schedularService: SchedularService,
    public clientDataReader: ReadFileSchedularService,
  ) {
    super(activatedRoute, RESOURCE.SHIFT_ASSIGN, ACTION_FORM.INTERIOR_SEARCH);
    this.formSave = this.buildForm({
      fromDate: this.currentDate,
      toDate: new Date(this.currentDate.getFullYear(), this.currentDate.getMonth() + 1, 0),
    }
      , this.formConfig
      , ACTION_FORM.INTERIOR_SEARCH
      , [
        ValidationService.notAffter('fromDate', 'toDate', 'app.salaryTable.steps.expiredDate'),
        // ValidationService.notOnMonth('toDate', 'fromDate', 'common.label.error.onMonth'),
      ]);

  }


  public notOnMonth() {
    const target = this.f["toDate"];
    const toMatch = this.f["fromDate"];
    if (target.hasError('dateNotSameMonth')) {
      target.setErrors(null);
      target.markAsUntouched();
    }

    if (target.value && toMatch.value) {
      const fromDate = new Date(target.value);
      const toDate = new Date(toMatch.value);
      const isCheck = fromDate.getFullYear() === toDate.getFullYear()
        && fromDate.getMonth() === toDate.getMonth();
      // set equal value error on dirty controls
      if (!isCheck && target.valid && toMatch.valid) {
        target.setErrors({ dateNotSameMonth: { dateNotSameMonth: "common.label.error.onMonth" } });
        target.markAsTouched();
        return false;
      }
    }
    return true;
  }
  get f() {
    return this.formSave.controls;
  }
  public workTypeSource: Array<any>;
  public maxCol: number = 3;
  public pagingTotal: number = 3;
  public dataSourcePaging: Array<any>;

  ngOnInit() {
    this.currentDate = new Date();
    this.maxCol = this.getMaxCol(window.innerWidth);
    this.workTypeSource = this.workTypeSource || [];
    this.schedularService.getWorkType()
      .subscribe(data => {
        this.workTypeSource = data;
        this.reBuildData();
      });

  }
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    let maxCol = this.getMaxCol(window.innerWidth);
    if (this.maxCol != maxCol) {
      this.maxCol = maxCol;
      this.reBuildData();
    }
  }
  reBuildData() {
    this.pagingTotal = Math.trunc(this.workTypeSource.length / this.maxCol) + 1;
    this.dataSourcePaging = [];
    for (let index = 0; index < this.maxCol; index++) {
      let temp = this.workTypeSource.slice(index * this.pagingTotal, (index + 1) * this.pagingTotal);
      this.dataSourcePaging.push(temp);
    }
  }
  getMaxCol(value: number) {
    if (value >= 1200)
      return 3;
    if (value >= 800)
      return 2;
    return 1;
  }
  customRequire(): boolean {
    var require = this.formSave.value["organizationId"] || this.formSave.value["employeeId"];
    if (!require) {
      let target = this.f["organizationId"];
      target.setErrors({ oneOfRequire: { oneOfRequire: "common.label.error.onMonth" } });
      return false;
    }
    return true;
  }
  onSearch() {
    if (!CommonUtils.isValidForm(this.formSave) || !this.notOnMonth() || !this.customRequire())
      return;

    this.shiftEmploy = [];
    var param = this.getParam();
    this.schedularService.getShiftEmployee(param).subscribe((res: RepositoryPaging<HrShiftsEmployeeDetail>) => {
      if (!res) return this.app.errorMessage("haveError")
      if (res.code) return this.handlerError(res.code, param);

      console.log(res);
      let data = res.data.shifts;
      this.shiftEmploy = data.map(x => {
        return <ShiftEmploy>{
          employeeCode: x.employee.employeeCode,
          fullName: x.employee.fullName,
          organizationId: x.employee.organizationId,
          organizationCode: x.employee.organization.code,
          organizationName: x.employee.organization.name,
          employeeName: x.employee.fullName,
          position: x.position ? x.position.name : '',
          workDayControls: {},
          workDay: x.shiftsEmployees.map(s => {
            const day = new Date(s.shiftsDate);
            return <WorkDay>{
              date: day.getDate(),
              dayOfWeek: day.getDay(),
              shiftCodes: s.workCode,
              shiftCodeRaw: s.workCode,
              shiftDate: formatDate(day, 'yyyy-MM-dd', 'en')
            }
          }),
        }
      })
      this.table.onLoadData(this.shiftEmploy, param.fromDate, param.toDate);
    });
  }
  getReport() {
    let config = {};
    // this.schedularService.getShiftEmploy(config);
    console.log(this.shiftEmploy);

  }

  employeeSelected: any;
  onEmployeeChange(value) {
    this.employeeSelected = value;
  }
  getParam() {
    let param = { ...this.formSave.value };
    param.employeeId = this.employeeSelected ? this.employeeSelected.codeField : param.employeeId;
    param.fromDate = formatDate(new Date(param["fromDate"]), "yyyy-MM-dd", 'en');
    param.toDate = formatDate(new Date(param["toDate"]), "yyyy-MM-dd", 'en');
    param.orgenizationId = param.organizationId;
    param.organizationId = param.organizationId;
    param.page = 0;
    param.size = 100;
    return param;
  }
  organizationSelected: any;
  organizationOnChange(e) {
    this.organizationSelected = e
  }
  waringReport() {
    var param = this.getParam();
    this.schedularService.shiftReport(param).subscribe((res: RepositoryPaging<HrShiftsEmployeeDetail>) => {
      if (res.code != 0)
        return this.handlerError(res.code);
      let data = res.data.warningReport;
      console.log(data);

      var a = data.employeeWarningReports.map((x: employeeWarningReport) => {
        return <warningReportEmployee>{
          employeeName: x.employee.fullName,
          employeeCode: x.employee.employeeCode,
          organizationName: x.employee.organization.name,
          warningMessage: x.warningReports.map(w => {
            return w.errorCode && this.getErrorMessenger(w.errorCode, w);
          }).join(", \n"),
        }
      });
      var b = data.organizationWarningReport.map((x: organizationWarningReport) => {
        return <warningReportOrganization>{
          organizationName: x.organization.name,
          organizationCode: x.organization.code,
          totalEmployee: x.totalEmployee,
          warningMessage: x.warningReports.map(w => {
            return w.errorCode && this.getErrorMessenger(w.errorCode, w);
          }).join(", \n"),
        }
      });
      const headers = ExportImportTemplateConfig.getReportWarningShiftDataHeaderConfig()
      this.clientDataReader.exportExcelMultipleSource("Báo cáo cảnh báo", [{
        headers: headers.employeeReportHeader,
        tittleStartDataSource: "Danh sách cảnh báo nhân viên",
        dataSource: a,
        headerStart: 10,
        tittleConfig: [{ start: 1, tittle: "CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM", style: null },
        { start: 2, tittle: "Độc lập - Tự do - Hạnh phúc", style: null },
        { start: 5, tittle: "CẢNH BÁO DỮ LIỆU LẬP LỊCH PHÂN CA", style: null },
        { start: 6, tittle: this.organizationSelected.name, style: null },
        { start: 7, tittle: this.app.validateMessenger("app.schedular.fromDateToDate", param), style: { font: { bold: false } } }],
      }, {
        headers: headers.organizationReportHeader,
        tittleStartDataSource: "Danh sách cảnh báo đơn vị",
        dataSource: b,
      }])
    })
  }

  getErrorMessenger(code: number, value?: any): string {
    let key = 'ERROR.schedular.' + ExportImportTemplateConfig.getMessengerError(code);
    return this.app.validateMessenger(key, value);
  }
  handlerError(code: number, value?: any) {
    this.app.message('error', this.getErrorMessenger(code, value));
  }
  onSubmit(data: ShiftEmploy[]) {
    if (!data || !data.length) {
      this.app.successMessage("noChange");
      return;
    }
    let source = data && this.transformData(data);


    this.schedularService.importShiftEmploy(source).subscribe((res: RepositoryPaging<any>) => {
      if (res && res.code == 0) {
        this.app.successMessage("updateSuccess");
        this.onSearch();
        return;
      }
      console.log(res);
      if (res && res.code != -2)
        return this.app.message('error', this.getErrorMessenger(res.code, res));

      let data = res.data && res.data.shiftEmployeeImportErrors as ShiftEmployeeImportModel
      if (data.errorCode)
        return this.app.message('error', this.getErrorMessenger(data.errorCode, data));

      for (const item of data.dataShifts) {
        if (item.errorCode)
          return this.handlerError(item.errorCode, item);

        for (const dayShift of item.shift) {
          if (dayShift.errorCode == 0)
            continue;
          let error = `Ngày: ${dayShift.shiftDate} \n
                        Ca ${dayShift.shiftDetails} vi phạm:
                ${this.getErrorMessenger(dayShift.errorCode, dayShift)}\n`;
          this.app.message('error', error);
          return;
        }
      }
    }, error => {
      this.app.requestIsError();
    });

  }

  transformData(data: ShiftEmploy[]): ShiftEmployeeImportModel {
    var dataShift = Array<shift>();
    const fromDate = new Date(this.formSave.value['fromDate']).getDate();
    const toDate = new Date(this.formSave.value['toDate']).getDate();
    const month = new Date(this.formSave.value['fromDate']).getMonth();
    const year = new Date(this.formSave.value['fromDate']).getFullYear();
    const user = HrStorage.getUserToken();

    for (const item of data) {
      let employeeDay = {
        employeeId: item.employeeCode,
        shift: [],
      };
      for (let index = 0; index < item.workDay.length; index++) {
        const element = item.workDay[index];
        if (element.date < fromDate || element.date > toDate)
          continue;
        employeeDay.shift.push({

          shiftDate: moment(new Date(year, month, element.date)).format("YYYY-MM-DD"),
          dayCode: element.title && element.title.slice(element.title.indexOf("(") + 1, element.title.length - 1),
          shiftDetails: element.shiftCodes,
        });
      }
      dataShift.push(employeeDay);
    }
    var obj = {
      employeeIdEdit: user.employeeCode, //hash code
      organizationId: +this.formSave.value["organizationId"],
      dataShifts: dataShift,
      fromDate: moment(this.formSave.value['fromDate']).format("YYYY-MM-DD"),
      toDate: moment(this.formSave.value['toDate']).format("YYYY-MM-DD"),
    }
    return obj;
  }
}
