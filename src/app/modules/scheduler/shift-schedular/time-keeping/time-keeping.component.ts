import { Component, HostListener, OnInit, Testability, ViewChild } from '@angular/core';
import { FormGroup, ValidatorFn } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AppComponent } from '@app/app.component';
import { ACTION_FORM, RESOURCE } from '@app/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ValidationService, CommonUtils } from '@app/shared/services';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SchedularService } from '@app/core/services/schedular/schedular.service';
import { RepositoryPaging, timekeeping, timeSheetModelView } from '@app/core/models/org-model';
import { formatDate } from '@angular/common';
import { ExportImportTemplateConfig } from '../../shift-assignment-table/export-import-template-config/export-import-template-config';
import { ShiftSchedularTableComponent } from '../shift-schedular-table/shift-schedular-table.component';
import * as moment from 'moment';
import { ReadFileSchedularService } from '@app/core/services/schedular/read-file-schedular.service';

@Component({
  selector: 'time-keeping',
  templateUrl: './time-keeping.component.html',
  styleUrls: ['./time-keeping.component.css']
})
export class TimeKeepingComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  dataError: any;
  formConfig = {
    employeeId: ['',],
    organizationId: ['',],// [ValidationService.required]],
    fromDate: ['', [ValidationService.required]],
    page: [0],
    size: [0],
  };
  currentDate = new Date();
  public shiftEmploy = Array<timeSheetModelView>();
  @ViewChild('table') table: ShiftSchedularTableComponent;
  constructor(
    public activatedRoute: ActivatedRoute,
    private app: AppComponent,
    public activeModal: NgbActiveModal,
    public schedularService: SchedularService,
    public clientDataReader: ReadFileSchedularService,
  ) {
    super(activatedRoute, RESOURCE.SHIFT_ASSIGN, ACTION_FORM.INTERIOR_SEARCH);
    this.formSave = this.buildForm({
      fromDate: this.currentDate,
      page: 0,
      size: 100,
    }
      , this.formConfig
      , ACTION_FORM.INTERIOR_SEARCH
      , [
        // this.customRequire('organizationId', 'employeeId','')
        //  ValidationService.notOnMonth('toDate', 'fromDate', 'common.label.error.onMonth'),
      ]);
  }
  customValidate() {

    const target = this.f['organizationId'];
    const toMatch = this.f['employeeId'];
    // if (target.hasError('oneOfRequire')) {
    //   target.setErrors(null);
    //   target.markAsUntouched();
    // }
    var require = target.value || toMatch.value;
    if (!require) {
      target.setErrors({ oneOfRequire: { oneOfRequire: "common.label.error.onMonth" } });
      target.markAsTouched();
      return false
    }
    return true;
  }
  customRequire(targetKey: string, toMatchKey: string, labelMatchCode: string): ValidatorFn {
    return (group: FormGroup): { [key: string]: any } => {
      const target = group.controls[targetKey]
      const toMatch = group.controls[toMatchKey];
      // if (target.hasError('oneOfRequire')) {
      //   target.setErrors(null);
      //   target.markAsUntouched();
      // }
      var require = target.value || toMatch.value;
      if (!require) {
        target.setErrors({ oneOfRequire: { oneOfRequire: "common.label.error.onMonth" } });
        target.markAsTouched();
      }
      return null;
    };
  }
  get f() {
    return this.formSave.controls;
  }
  submit() {

  }
  ngOnInit() {
    this.dataSourcePaging = {};
    this.getWorkType(null);
  }
  workTypeSource = []
  dataSourcePaging: {
    [key: string]: Array<any>;
  }
  showWorkType: boolean;
  showSourceType() {
    if (this.workTypeSource && this.workTypeSource.length)
      return this.showWorkType = !this.showWorkType;
    this.getWorkType(() => { this.showWorkType = !this.showWorkType });
  }
  getWorkType(callback: () => any) {
    this.schedularService.getWorkType()
      .subscribe(data => {
        this.workTypeSource = data;
        this.dataSourcePaging = this.clientDataReader.groupBy(data, 'workTypeId');
        console.log(data);
        callback && callback();
      });
  }

  employeeSelected: any;
  onEmployeeChange(value) {
    this.employeeSelected = value;
  }
  getParam() {
    let param = { ...this.formSave.value };
    param.fromDate = formatDate(new Date(param["fromDate"]), "yyyy-MM-dd", 'en');
    param.toDate = param.fromDate;//formatDate(new Date(param["toDate"]), "yyyy-MM-dd", 'en');
    param.orgenizationId = param.organizationId;
    param.organizationId = param.organizationId;
    param.employeeId = this.employeeSelected ? this.employeeSelected.codeField : param.employeeId;
    return param;
  }
  getErrorMessenger(code: number, value?: any) {
    let key = 'ERROR.schedular.' + ExportImportTemplateConfig.getMessengerError(code);
    return this.app.validateMessenger(key, value);
  }
  onSearch() {
    if (!CommonUtils.isValidForm(this.formSave) || !this.customValidate())
      return;
    var param = this.getParam();
    this.schedularService.getTimeKeeping(param).subscribe((res: RepositoryPaging<timekeeping>) => {
      if (res.code != 0)
        return this.app.message('error', this.getErrorMessenger(res.code))
      const data = res.data.timeSheets;
      this.shiftEmploy = data.map(x => {
        const sheet = x.sheets[0];
        return <timeSheetModelView>{
          employeeCode: x.employee.employeeCode,
          fullName: x.employee.fullName,
          workCode: sheet ? sheet.hrMdWorkType.workCode : '',
          workCodeBefore: sheet ? sheet.hrMdWorkType.workCode : '',
          totalWorkTime: sheet ? sheet.hrMdWorkType.totalWorkTime : '',
          workTypeId: sheet ? sheet.hrMdWorkType.workTypeId : -1,
        }
      })
      console.log(data, 'source', this.shiftEmploy);

    })
  }
  validateCell(value) {
    return new RegExp("^X[4|8]").test(value);
  }
  actionSubmitForm() {
    if (!CommonUtils.isValidForm(this.formSave) || !this.customValidate())
      return;
    var param = this.getParam();
    param.dataTimeSheets = Array<any>();

    for (const item of this.shiftEmploy) {
      if (item.workCode != item.workCodeBefore) {
        param.dataTimeSheets.push({
          employeeId: item.employeeCode,
          timeSheets: [{
            workDay: param.fromDate,
            timesheetDetails: item.workCode,
          }]
        });
      }
    }
    if (!param.dataTimeSheets.length) {
      return this.app.successMessage("noChange");
    }
    this.schedularService.updateTimeKeeping(param).subscribe((res: RepositoryPaging<timekeeping>) => {
      if (res.code == 0)
        return this.app.successMessage("updateSuccess");
      if (res.code !== -2)
        return this.app.message('error', this.getErrorMessenger(res.code));
      let data = res.data.timeSheetsUpdateErrors.dataTimeSheets;
      for (const iterator of data) {
        if (iterator.errorCode)
          return this.app.message('error', this.getErrorMessenger(iterator.errorCode, iterator));
        for (const day of iterator.timeSheets) {
          if (day.errorCode)
            return this.app.message('error', this.getErrorMessenger(day.errorCode, day));
        }
      }
      // return this.app.message('error', mess);
    })

  }
}
