import { Component, Input, OnInit } from '@angular/core';
import { timeSheetModelView } from '@app/core/models/org-model';
import { faLeaf } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'time-keeping-table',
  templateUrl: './time-keeping-table.component.html',
  styleUrls: ['./time-keeping-table.component.css']
})
export class TimeKeepingTableComponent implements OnInit {

  constructor() { }
  @Input() workTypeSource: Array<any>;
  @Input() workTypeId: number;
  @Input() shiftEmploy = Array<timeSheetModelView>();
  ngOnInit() {
    this.popupDetail = {};
  }
  onBlur(e) {
  }
  popupDetail: any;
  popupDialog = false;
  cancel() {
    this.popupDetail = {};
  }
  showPopup(item: timeSheetModelView) {
    this.popupDetail = { ...item };
    let workType = this.workTypeSource.find(x => x.workCode == item.workCode);
    this.popupDetail.workTypeName = workType ? workType.workName : "Không hợp lệ"
    this.popupDialog = true
  }
  validate() {
    for (const item of this.shiftEmploy) {
      if (item.workCode != item.workCodeBefore && !this.validateCell(item.workCode))
        return false;
    }
    return true;
  }
  validateCell(value: string): boolean {
    if (!this.workTypeSource)
      return true;
    if (!value) return true;
    if (value.split(",").some(x => !this.workTypeSource.find(y => y.workCode == x)))
      return false;
    return true;
    // return new RegExp("^X[4|8]").test(value);
  }

}
