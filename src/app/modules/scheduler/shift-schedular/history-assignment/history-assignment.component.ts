import { Component, HostListener, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AppComponent } from '@app/app.component';
import { ACTION_FORM, RESOURCE } from '@app/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ValidationService, CommonUtils } from '@app/shared/services';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SchedularService } from '@app/core/services/schedular/schedular.service';
import { ShiftEmploy, WorkDay } from '@app/core/models/employee.model';
import { LogShiftEmployee, LogShiftEmployeeDetail, RepositoryPaging } from '@app/core/models/org-model';
import * as moment from 'moment'
import { ExportImportTemplateConfig } from '../../shift-assignment-table/export-import-template-config/export-import-template-config';
@Component({
  selector: 'history-assignment',
  templateUrl: './history-assignment.component.html',
  styleUrls: ['./history-assignment.component.css']
})
export class HistoryAssignmentComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  dataError: any;
  formConfig = {
    employeeId: ['',],
    organizationId: ['', [ValidationService.required]],
    fromDate: ['',],
    toDate: ['',],
    isCurrent: [0],
  };
  currentDate = new Date();
  public shiftLogEmploy: LogShiftEmployeeDetail[];

  constructor(
    public activatedRoute: ActivatedRoute,
    private app: AppComponent,
    public activeModal: NgbActiveModal,
    public schedularService: SchedularService,
  ) {
    super(activatedRoute, RESOURCE.SHIFT_ASSIGN, ACTION_FORM.INTERIOR_SEARCH);
    this.formSave = this.buildForm({
      fromDate: this.currentDate,
      toDate: new Date(this.currentDate.getFullYear(), this.currentDate.getMonth() + 1, 0),
    }
      , this.formConfig
      , ACTION_FORM.INTERIOR_SEARCH
      , [
        ValidationService.notAffter('fromDate', 'toDate', 'app.salaryTable.steps.expiredDate'),
        // ValidationService.notOnMonth('toDate', 'fromDate', 'common.label.error.onMonth'),
      ]);

  }
  resultList: any;

  formatDate(date: Date) {
    return date.toISOString().split('T')[0]
  }

  public notOnMonth() {
    const target = this.f["toDate"];
    const toMatch = this.f["fromDate"];
    if (target.hasError('dateNotSameMonth')) {
      target.setErrors(null);
      target.markAsUntouched();
    }

    if (target.value && toMatch.value) {
      const fromDate = new Date(target.value);
      const toDate = new Date(toMatch.value);
      const isCheck = fromDate.getFullYear() === toDate.getFullYear()
        && fromDate.getMonth() === toDate.getMonth();
      // set equal value error on dirty controls
      if (!isCheck && target.valid && toMatch.valid) {
        target.setErrors({ dateNotSameMonth: { dateNotSameMonth: "common.label.error.onMonth" } });
        target.markAsTouched();
        return false;
      }
    }
    return true;
  }
  get f() {
    return this.formSave.controls;
  }


  ngOnInit() {
    this.currentDate = new Date();
  }
  employeeSelected: any;
  onEmployeeChange(value) {
    this.employeeSelected = value;
  }
  getParam(): any {
    let param = this.formSave.value;
    param.fromDate = moment(param["fromDate"]).format("YYYY-MM-DD");
    param.toDate = moment(param["toDate"]).format("YYYY-MM-DD");
    param.orgenizationId = param.organizationId;
    param.organizationId = param.organizationId ;
    param.employeeId = this.employeeSelected?this.employeeSelected.codeField:param.employeeId;
    param.page = 0;
    param.size = 100;
    return param;
  }
  totalPage: number;
  handlerError(code: number, value?: any) {
    let key = 'ERROR.schedular.' + ExportImportTemplateConfig.getMessengerError(code);
    this.app.message('error', this.app.validateMessenger(key, value));
  }
  onSearch() {
    if (!CommonUtils.isValidForm(this.formSave) || !this.notOnMonth())
      return;

    this.shiftLogEmploy = [];
    var param = this.getParam()
    this.schedularService.searchHistory(param).subscribe((res: RepositoryPaging<LogShiftEmployee>) => {
      if (!res || res.code != 0)
        return this.handlerError(res.code, param);
      this.totalPage = res.totalPage;
      console.log(res.data);

      this.shiftLogEmploy = res.data.logs;
    });


  }

}
