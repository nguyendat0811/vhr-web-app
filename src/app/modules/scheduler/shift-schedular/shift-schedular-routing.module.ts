import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { RESOURCE } from "@app/core";
import { CommonUtils } from "@app/shared/services";
import { EmpTimekeepingComponent } from "./emp-timekeeping/emp-timekeeping.component";
import { HistoryAssignmentComponent } from "./history-assignment/history-assignment.component";
import { ImportShiftSchedularComponent } from "./import-shift-schedular/import-shift-schedular.component";
import { ImportTimekeepingComponent } from "./import-timekeeping/import-timekeeping.component";
import { InLateOutEarlyComponent } from "./in-late-out-early/in-late-out-early.component";
import { ShiftSchedularIndexComponent } from "./shift-schedular-index/shift-schedular-index.component";
import { TimeKeepingComponent } from "./time-keeping/time-keeping.component";

const routes: Routes = [
  {
    path: '',
    component:ShiftSchedularIndexComponent,
    data: {
      resource: RESOURCE.WORK_SCHEDULE,
      nationId: CommonUtils.getNationId()
    },
    pathMatch: 'full'
  },
  {
    path: 'import',
    component:ImportShiftSchedularComponent,
    data: {
      resource: RESOURCE.WORK_SCHEDULE,
      nationId: CommonUtils.getNationId()
    },
    pathMatch: 'full'
  },
  {
    path: 'history',
    component:HistoryAssignmentComponent,
    data: {
      resource: RESOURCE.WORK_SCHEDULE,
      nationId: CommonUtils.getNationId()
    },
    pathMatch: 'full'
  },
  {
    path: 'in-out',
    component:InLateOutEarlyComponent,
    data: {
      resource: RESOURCE.WORK_SCHEDULE,
      nationId: CommonUtils.getNationId()
    },
    pathMatch: 'full'
  },
  {
    path: 'timekeeping',
    component:TimeKeepingComponent,
    data: {
      resource: RESOURCE.WORK_SCHEDULE,
      nationId: CommonUtils.getNationId()
    },
    pathMatch: 'full'
  },
  {
    path: 'emp-timekeeping',
    component:EmpTimekeepingComponent,
    data: {
      resource: RESOURCE.WORK_SCHEDULE,
      nationId: CommonUtils.getNationId()
    },
    pathMatch: 'full'
  },
  {
    path: 'emp-timekeeping/import',
    component:ImportTimekeepingComponent,
    data: {
      resource: RESOURCE.WORK_SCHEDULE,
      nationId: CommonUtils.getNationId()
    },
    pathMatch: 'full'
  },
  {
    path: 'timekeeping/import',
    component:ImportTimekeepingComponent,
    data: {
      resource: RESOURCE.WORK_SCHEDULE,
      nationId: CommonUtils.getNationId()
    },
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShiftSchedularRoutingModule { }
