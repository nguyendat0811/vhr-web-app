import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges, ViewEncapsulation } from '@angular/core';
import { AppComponent } from '@app/app.component';
import { ShiftEmploy, WorkDay } from '@app/core/models/employee.model';
import { ReadFileSchedularService } from '@app/core/services/schedular/read-file-schedular.service';
import * as moment from 'moment'
@Component({
  selector: 'shift-schedular-table',
  templateUrl: './shift-schedular-table.component.html',
  styleUrls: ['./shift-schedular-table.component.css']
})
export class ShiftSchedularTableComponent implements OnInit {
  constructor(
    private app: AppComponent,
    public readFile: ReadFileSchedularService
  ) { }

  public shiftEmploys: ShiftEmploy[];
  // @Input() public month: Date;
  @Input() public fromDate?: Date;
  @Input() public toDate?: Date;
  @Input() public allowSave: boolean;
  @Input() workTypeSource: Array<any>;
  @Output() public onSubmit: EventEmitter<ShiftEmploy[]> = new EventEmitter<ShiftEmploy[]>();
  // workDayConfig: WorkDay[]  =[];
  ngOnInit() {
    this.buildCol();
    this.shiftEmploys = []
  }

  dateTran: { [key: string]: string } = {};
  currentMonth: string;
  buildCol() {
    let key = moment(this.fromDate).format("YYYYMMDD") + moment(this.toDate).format("YYYYMMDD");
    if (key === this.currentMonth)
      return;
    this.currentMonth = key;
    let headers = this.readFile.getDynamicHeader(this.fromDate || new Date(), this.toDate || new Date(), 0);
    this.daysInMonth = [];
    for (const header of headers) {
      this.daysInMonth.push(new WorkDay(+header.key, "", "", header.extend, header.title, true, false, header['date']));
    }

  }
  daysInMonth: WorkDay[] = [];

  isWorkingDay(shiftEmploy: ShiftEmploy, date: WorkDay): Boolean {
    const workDays = shiftEmploy.workDay.map(day => day.date);
    return workDays.some(workDay => workDay == date.date);
  }
  organizationId: number

  isDisplayOrganization(index: number, workEmployee: ShiftEmploy): boolean {
    if (index < 1) {
      return true;
    }
    const prevWorkEmployee = this.shiftEmploys[index - 1];
    return prevWorkEmployee.organizationId != workEmployee.organizationId;
  }
  validate() {
    for (const row of this.shiftEmploys) {
      let item = { ...row };
      for (const day in item.workDayControls) {
        const wd = item.workDayControls[day];
        if (wd.shiftCodeRaw != wd.shiftCodes && !this.validateCell(wd.shiftCodes)) {
          this.app.message('error', this.app.validateMessenger("app.schedular.timekeeping.error", { day: wd.title }));
          return false;
        }
      }
    }
    return true;
  }
  submit() {
    if (!this.validate())
      return;

    let shiftEmploysChange = new Array<ShiftEmploy>();
    for (const row of this.shiftEmploys) {
      row.workDay = Object.keys(row.workDayControls).map(key => <WorkDay>row.workDayControls[key]);
      let item = { ...row };
      item.workDay = item.workDay.filter(x => x.shiftCodes != x.shiftCodeRaw);
      item.workDayControls = []

      if (item.workDay.length)
        shiftEmploysChange.push(item);

    }
    this.onSubmit.emit(shiftEmploysChange);

  }

  transformData() {
    if (!this.shiftEmploys || !this.shiftEmploys.length)
      return;
    console.log("source binding ", this.shiftEmploys);

    for (const row of this.shiftEmploys) {
      for (const day of row.workDay) {
        day.shiftCodeRaw = day.shiftCodes;
        row.workDayControls[day.date] = day;
      }
      for (const day of this.daysInMonth) {
        row.workDayControls[day.date] = row.workDayControls[day.date] || new WorkDay(day.date, day.shiftCodes || "", "", day.dayOfWeek, day.title, day.isShow, day.locked, day.shiftDate);
      }
    }
  }
  validateCell(values: string) {
    values = values || '';
    if (!this.workTypeSource) return true;
    if (!values) return true;
    for (const value of values.split(',')) {
      let item = this.workTypeSource.find(x => x.workCode == value);
      if (!item) return false;
    }
    return true;
  }
  allowEdit(day: number) {
    let currentDate = new Date();
    if (day < currentDate.getDate())
      return false;

    return true;
  }
  onUpdateCellData(e: Event, data) {
    if (e.type != "change" || !data)
      return;
    var input = e.target as HTMLElement;

    console.log(e, data);
  }
  onLoadData(data: ShiftEmploy[], fromDate: Date, toDate: Date): void {
    this.shiftEmploys = data;
    this.fromDate = new Date(fromDate);
    this.toDate = new Date(toDate);

    this.buildCol();
    this.transformData();
  }



}

