import { Component, OnInit, AfterViewInit, AfterViewChecked } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { CommonUtils, ValidationService } from '@app/shared/services';
import { FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { EmployeeKiService } from '@app/core/services/evaluation/employee-ki.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from '@app/app.component';
import { RESOURCE, ACTION_FORM, APP_CONSTANTS, SysCatService } from '@app/core';
import * as moment from 'moment';
import { HelperService } from '@app/shared/services/helper.service';

@Component({
  selector: 'employee-ki-search',
  templateUrl: './employee-ki-search.component.html',
  styleUrls: ['./employee-ki-search.component.css']
})
export class EmployeeKiSearchComponent extends BaseComponent implements OnInit {
  public commonUtils = CommonUtils;
  arrDisable: Array<any>;
  resultList: any = {};
  formSearch: FormGroup;
  disabled = false;
  private expiredDate: string;
  private currentEvent: any;
  private navigationSubscription;
  checkFirstSearch = false;
  listYear: Array<any>;
  listQuarter: Array<any>;
  listMonth: Array<any>;
  listType = APP_CONSTANTS.EVALUATION_PERIOD;
  listKiRank: Array<any>;
  filterCondition: any;
  START_DATE: any;
  END_DATE: any;
  flagAffterImport = false;
  formConfig = {
    type: [1, [ValidationService.required]],
    month: [parseInt(moment(new Date()).format('MM')), [ValidationService.required]],
    quarter: [parseInt(moment(new Date()).format('Q')), [ValidationService.required]],
    year: [parseInt(moment(new Date()).format('YYYY')), [ValidationService.required]],
    employeeCode: ['',[ValidationService.maxLength(100)]],
    employeeName: ['', [ValidationService.maxLength(100)]],
    organizationId: ['', [ValidationService.required]],
    jobTitle: [''],
    status: [0],
    numOfRecord: [10, [ValidationService.required, Validators.min(1), Validators.max(1000)]]
  };
  formSaveConfig = {
    employeeKiProcessId: [''],
    employeeCode:[''],
    employeeId: ['', [ValidationService.required]],
    organizationId: [''],
    empKiPoint: ['', [ValidationService.required, ValidationService.number, Validators.max(1000), Validators.min(1)]],
    empKi: ['', [ValidationService.required]],
    isRate: [''],
    note: ['',[ValidationService.maxLength(100)]],
    modifiedBy: [''],
    modifiedDate: [''],
    createdBy: [''],
    createdDate: [''],
    year: [''],
    quarter: [''],
    month: [''],
    type: [''],
    isLock:[''],
    orgPath:['']
  };
  formSave: FormArray;
  constructor(public act: ActivatedRoute
    , private helperService: HelperService
    , private modalService: NgbModal
    , private router: Router
    , public emplpoyeeKiService: EmployeeKiService
    , private sysCatService: SysCatService
    , private app: AppComponent) {
    super(act, RESOURCE.EMPLOYEE_KI_PROCESS, ACTION_FORM.SEARCH);   //change resource
    this.setMainService(emplpoyeeKiService);

    this.formSearch = this.buildForm({}, this.formConfig, ACTION_FORM.SEARCH);
    this.listYear = this.emplpoyeeKiService.getYearList();
    this.listQuarter = this.emplpoyeeKiService.getQuaterList();
    this.listMonth = this.emplpoyeeKiService.getMonthList();

    this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.KI_RANK).subscribe(res => {
      this.listKiRank = res.data;
    });

    // lay idOrg để tìm kiếm khi import xong
    const stateDataSearch = this.router.getCurrentNavigation().extras.state;
    if (stateDataSearch && stateDataSearch.data) {
      this.formSearch = this.buildForm(stateDataSearch.data, this.formConfig, ACTION_FORM.SEARCH);
      this.flagAffterImport = true;
    }

    this.buildFormSave2([]);
  }

  ngOnInit() {
    if (this.flagAffterImport) {
      this.processSearchEmployeeKi();
      this.flagAffterImport = false;
    }
    this.setDate();
  }

  setDate(){
    let startMonth, endMonth,year;
    if (this.formSearch.controls['type'].value == 1) {
      startMonth = this.formSearch.controls['month'].value;
      endMonth = this.formSearch.controls['month'].value;
    } else {
      if (this.formSearch.controls['type'].value == 2) {
        switch (this.formSearch.controls['quarter'].value) {
          case 1:
            startMonth = 1;
            endMonth = 3;
            break;
          case 2:
            startMonth = 4;
            endMonth = 6;
            break;
          case 3:
            startMonth = 7;
            endMonth = 9;
            break;
          case 4:
            startMonth = 10;
            endMonth = 12;
            break;
        }
      } else {
        startMonth = 1;
        endMonth = 12;
      }
    }
    year = this.formSearch.controls['year'].value;
    this.START_DATE = moment(new Date(year,startMonth-1)).startOf('day').startOf('month').toDate().getTime();
    this.END_DATE = moment(new Date(year,endMonth-1)).startOf('day').endOf('month').toDate().getTime();
    this.START_DATE = this.formatDate(this.START_DATE);
    this.END_DATE = this.formatDate(this.END_DATE);
  }
  formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('/');
}

  /**
    * makeDefaultFormSave
    */
  private makeDefaultFormSave(): FormGroup {
    const formGroup = this.buildForm({}, this.formSaveConfig, ACTION_FORM.UPDATE);
    return formGroup;
  }
  /**
   * buildFormSave
   */
  
  private buildFormSave2(list?: any) {
    if (!list) {
      this.formSave = new FormArray([this.makeDefaultFormSave()]);
    } else {
      var organizationId = "";
      const controls = new FormArray([]);
      for (const i in list.data) {
        let item = list.data[i];
        if (organizationId == "" || organizationId != item.organizationId) {
          const empKiProcess = this.getDefaultValue();
          const group = this.makeDefaultFormSave();
          group.patchValue(empKiProcess);
          controls.push(group);
          organizationId = item.organizationId;
        }
        let group2 = this.makeDefaultFormSave();
        group2.patchValue(item);
        group2.controls['empKi'].setValue((item.empKi));
        if (item.isRate == 1) {
          group2.removeControl('empKi');
          group2.addControl('empKi', new FormControl(null));
          group2.removeControl('empKiPoint');
          group2.addControl('empKiPoint', new FormControl(null));
          group2.controls['empKiPoint'].disable({});
        } else {
          group2.removeControl('empKi');
          group2.addControl('empKi', new FormControl((item.empKi), [ValidationService.required]));
          group2.removeControl('empKiPoint');
          group2.addControl('empKiPoint', new FormControl(item.empKiPoint, [ValidationService.required, ValidationService.number, Validators.max(1000), Validators.min(1)]));
        }

        if(item.isLock == 1){
          group2.removeControl('empKi');
          group2.addControl('empKi', new FormControl(item.empKi));
          group2.removeControl('empKiPoint');
          group2.addControl('empKiPoint', new FormControl(item.empKiPoint));
          group2.controls['empKiPoint'].disable({});
          group2.controls['note'].disable({});
        } 
        controls.push(group2);
      };
      this.formSave = controls;
    }
  }
  processSearchEmployeeKi(event?) {
    if (!CommonUtils.isValidForm(this.formSearch)) {
      return;
    }
    this.currentEvent = event;
    const params = this.formSearch ? this.formSearch.value : null;

    this.emplpoyeeKiService.search(params, this.currentEvent).subscribe(res => {
      this.resultList = this.groupItemByProperty(res, 'organizationId');
      if (this.resultList.data) {
        this.buildFormSave2(this.resultList);

      } else {
        this.formSave = new FormArray([this.makeDefaultFormSave()]);
      }
    });
    if (!this.currentEvent) {
      if (this.dataTable) {
        this.dataTable.first = 0;
      }
    }
  }

  getDefaultValue() {
    var obj = {
      employeeKiProcessId: -1,
      employeeCode:'',
      employeeId: -1,
      organizationId: -1,
      empKiPoint: 1,
      empKi: 0,
      isRate: 1,
      note: -1,
      modifiedBy: -1,
      modifiedDate: -1,
      createdBy: -1,
      createdDate: -1,
      year: -1,
      quarter: -1,
      month: -1,
      type: -1,
      isLock: 1
    };
    return obj;
  }
  /**
   * groupItemByProperty
   * @ param res
   * @ param propertyName
   */
  groupItemByProperty(res, propertyName: string): any {
    const groupItem = [];
    if (!res.data) {
      res.groupItem = groupItem;
      return res;
    }
    let currId = 0;
    let index = parseInt(res.first);
    for (let item of res.data) {
      item.index = ++index;
      if (currId !== item[propertyName]) {
        currId = item[propertyName];
        const group = [];
        group.push(item);
        groupItem.push(group);
      } else {
        groupItem[groupItem.length - 1].push(item);
      }
    }
    res.groupItem = groupItem;
    return res;
  }
  changeType() {
    var value = this.formSearch.controls['type'].value;
    if (value == 1) {
      this.elementRequired('month', parseInt(moment(new Date()).format('MM')));
      this.elementNotRequired('quarter', 1);
    } else {
      if (value == 2) {
        this.elementRequired('quarter', parseInt(moment(new Date()).format('Q')));
        this.elementNotRequired('month', 1);
      } else {
        if (value == 3) {
          this.elementNotRequired('month', 1);
          this.elementNotRequired('quarter', 1);
        } else {

        }
      }
    }
    this.setDate();
  }

  elementRequired(item, value) {
    this.formSearch.removeControl(item);
    this.formSearch.addControl(item, new FormControl(value, [ValidationService.required]));
  }
  elementNotRequired(item, value) {
    this.formSearch.removeControl(item);
    this.formSearch.addControl(item, new FormControl(value));
  }
  private validateBeforeSave(): boolean {
    return CommonUtils.isValidForm(this.formSave);
  }

  processSaveOrUpdate() {
    if (!this.validateBeforeSave()) {
      return;
    }
    this.app.confirmMessage(null, () => {// on accepted
      this.emplpoyeeKiService.saveOrUpdate(this.formSave.value)
        .subscribe(res => {
          if (this.emplpoyeeKiService.requestIsSuccess(res)) {
            this.helperService.isProcessing(res.data);
            this.processSearchEmployeeKi(this.currentEvent);
          } else{
            this.processSearchEmployeeKi(this.currentEvent);
          }
        },()=>{
         
        });
    }, () => {
      // on rejected
      
    });
  }
  get f() {
    return this.formSearch.controls;
  }
  get fs() {
    return this.formSave.controls;
  }

  changeIsRate(item) {

    if (!item.controls['isRate'].value) {
      item.controls['empKi'].setValue(null);
      item.controls['empKiPoint'].setValue(null);
      item.removeControl('empKiPoint');
      item.removeControl('empKi');
      item.addControl('empKiPoint', new FormControl(null));
      item.addControl('empKi', new FormControl(null));
      const element = document.getElementsByClassName(`empKi_${item.value.employeeId}`);
      element[0].getElementsByTagName('input')[0].disabled = true;

    } else {
      item.removeControl('empKi');
      item.addControl('empKi', new FormControl(null, [ValidationService.required]));
      item.removeControl('empKiPoint');
      item.addControl('empKiPoint', new FormControl(null, [ValidationService.required, ValidationService.number, Validators.max(1000), Validators.min(1)]));
      const element = document.getElementsByClassName(`empKi_${item.value.employeeId}`);
      element[0].getElementsByTagName('input')[0].disabled = false;
    }
  }

  updateEmpKiChange(employeeId) {
    for (let i in this.formSave.controls) {
      if (this.formSave.controls[i].get('employeeId').value == employeeId) {
        this.formSave.controls[i].get('empKiPoint').setValue(0);
      }
    }
  }

  prepareExport() {
    if (!CommonUtils.isValidForm(this.formSearch)) {
      return;
    }
    const params = this.formSearch.value;
    this.emplpoyeeKiService.prepareExport(params).subscribe(res => {
      if (this.emplpoyeeKiService.requestIsSuccess(res)) {
        this.processExport();
      }

    }, () => {

    });
  }

  processExport() {
    if (!CommonUtils.isValidForm(this.formSearch)) {
      return;
    }
    const params = this.formSearch.value;
    this.emplpoyeeKiService.processExport(params, this.currentEvent).subscribe(res => {
      saveAs(res, 'export_employee_ki.xls');
    });
  }

  changeNumOfRecord(){
    // this.formSearch.controls['numOfRecord'].setValue(this.formSearch.controls['numOfRecord'].value.replace(/^\s+|\s+$/g,""));
     this.formSearch.controls['numOfRecord'].setValue(this.formSearch.controls['numOfRecord'].value.replace(/[^0-9 \,]/, ''));
  }

  changePoint(item){
    const element = document.getElementsByClassName(`empKi_${item.value.employeeId}`);
    var str = element[0].getElementsByTagName('input')[0].value
    var point =parseFloat(str); //parseFloat(item.controls['empKiPoint'].value);
    if(isNaN(point)){
      element[0].getElementsByTagName('input')[0].value='';
      return;
    }
    if(point.toString()==point.toFixed(1)){
      element[0].getElementsByTagName('input')[0].value = point.toFixed(1);
    } else{
      if(point==parseInt(point.toFixed(2))){
        element[0].getElementsByTagName('input')[0].value = parseInt(point.toFixed(2)).toString();
      } else{
        element[0].getElementsByTagName('input')[0].value = point.toFixed(2);
      }
    }
  }
}
