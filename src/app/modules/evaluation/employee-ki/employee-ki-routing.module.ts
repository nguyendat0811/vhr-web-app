import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeeKiSearchComponent } from './employee-ki-search/employee-ki-search.component';
import { EmployeeKiImportComponent } from './employee-ki-import/employee-ki-import.component';
import { PropertyResolver } from '@app/shared/services';
import { RESOURCE } from '@app/core';

const routes: Routes = [
  {
    path:'',
    component:EmployeeKiSearchComponent
  },
  {
    path:'import',
    component:EmployeeKiImportComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeeKiRoutingModule { }
