import { Component, OnInit, AfterViewInit } from '@angular/core';
import { CommonUtils, ValidationService } from '@app/shared/services';
import { FormGroup, FormControl } from '@angular/forms';
import * as moment from 'moment';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { HelperService } from '@app/shared/services/helper.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EmployeeKiService } from '@app/core/services/evaluation/employee-ki.service';
import { AppComponent } from '@app/app.component';
import { RESOURCE, ACTION_FORM } from '@app/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { FileControl } from '@app/core/models/file.control';

@Component({
  selector: 'employee-ki-import',
  templateUrl: './employee-ki-import.component.html',
  styleUrls: ['./employee-ki-import.component.css']
})
export class EmployeeKiImportComponent  extends BaseComponent  implements OnInit {
  public commonUtils = CommonUtils;
  public dataError: any;
  formImport: FormGroup;
  listYear: Array<any>;
  listQuarter: Array<any>;
  private navigationSubscription;
  listMonth: Array<any>;
  formConfig = {
    type: [1, [ValidationService.required]],
    month: [parseInt(moment(new Date()).format('MM')), [ValidationService.required]],
    quarter: [parseInt(moment(new Date()).format('Q')), [ValidationService.required]],
    year: [parseInt(moment(new Date()).format('YYYY')), [ValidationService.required]],
    organizationId: ['', [ValidationService.required]],
    numOfRecord:[10],
    status:[0]
  };
  constructor(
    public act: ActivatedRoute
    , private helperService: HelperService
    , private modalService: NgbModal
    , private router: Router
    , public emplpoyeeKiService: EmployeeKiService
    , private app: AppComponent
  ) { 
    super(act, RESOURCE.EMPLOYEE_KI_PROCESS, ACTION_FORM.SEARCH);  
    this.setMainService(emplpoyeeKiService);
    this.formImport = this.buildForm({}, this.formConfig, ACTION_FORM.SEARCH);
    this.formImport.addControl('fileImport', new FileControl(null, ValidationService.required));   
    this.listYear=this.emplpoyeeKiService.getYearList();
    this.listQuarter=this.emplpoyeeKiService.getQuaterList();
    this.listMonth=this.emplpoyeeKiService.getMonthList();
  }

  ngOnInit() {
    
  }
  
  get f() {
    return this.formImport.controls;
  }

  changeType(value){
    value=this.formImport.value.type;
    if(value==1){      
      this.elementRequired('month',parseInt(moment(new Date()).format('MM')));
      this.elementNotRequired('quarter',1);

    } else{
      if(value==2){
        this.elementNotRequired('month',1);
        this.elementRequired('quarter',parseInt(moment(new Date()).format('Q')));
      } else{
        if(value==3){
          this.elementNotRequired('month',1);
          this.elementNotRequired('quarter',1);
        } else{
          
        }
      }
    } 
  }
  
  elementRequired(item,value){
    this.formImport.removeControl(item);
    this.formImport.addControl(item, new FormControl(value,[ValidationService.required]));
  }
  elementNotRequired(item,value){
    this.formImport.removeControl(item);
    this.formImport.addControl(item, new FormControl(value));
  }
  processImportEmployeeKi() {
    this.formImport.controls['fileImport'].updateValueAndValidity();
    if (!CommonUtils.isValidForm(this.formImport)) {
      return;
    }
    this.app.confirmMessage(null, () => {// on accepted
      
      this.dataError = null;
      this.emplpoyeeKiService.processImport(this.formImport.value).subscribe(res => {
        if (!this.emplpoyeeKiService.requestIsSuccess(res)) {
          this.dataError = res.data;
          this.dataError.forEach(element => {
            if(element.row == 8 && element.column == 1) {
              element.row  = null;
              element.columnLabel = null;
            }
          });
        } else {
          this.dataError = null;
          this.router.navigate(['/evaluation/employee-ki'], {state: {data : this.formImport.value}});
        }
      });
    },null);
  }

  processDownloadTemplate() {
    
    const params = this.formImport.value;
    delete params['fileImport'];
    this.formImport.removeControl('fileImport');
    this.formImport.addControl('fileImport', new FormControl(null));
    if (!CommonUtils.isValidForm(this.formImport)) {
      return;
    }
    this.emplpoyeeKiService.downloadTemplateImport(params).subscribe(res => {
      saveAs(res, 'employee_ki_process.xls');
    });
    this.formImport.controls['fileImport'].setValidators(ValidationService.required);
  }
}
