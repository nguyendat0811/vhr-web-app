import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmployeeKiRoutingModule } from './employee-ki-routing.module';
import { EmployeeKiSearchComponent } from './employee-ki-search/employee-ki-search.component';
import { EmployeeKiAddComponent } from './employee-ki-add/employee-ki-add.component';
import { EmployeeKiImportComponent } from './employee-ki-import/employee-ki-import.component';
import { SharedModule } from '@app/shared';


@NgModule({
  declarations: [ EmployeeKiSearchComponent, EmployeeKiAddComponent, EmployeeKiImportComponent], 
  imports: [
    CommonModule,
    SharedModule,
    EmployeeKiRoutingModule
  ]
})
export class EmployeeKiModule { }
  