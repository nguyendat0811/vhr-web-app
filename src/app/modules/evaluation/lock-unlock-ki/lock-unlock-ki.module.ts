import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LockUnlockKiIndexComponent } from './lock-unlock-ki-index/lock-unlock-ki-index.component';
import { LockUnlockKiSearchComponent } from './lock-unlock-ki-search/lock-unlock-ki-search.component';
import { SharedModule } from '@app/shared';
import { LockUnlockKiRoutingModule } from './lock-unlock-ki-routing.module';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  // tslint:disable-next-line: max-line-length
    declarations: [LockUnlockKiIndexComponent, LockUnlockKiSearchComponent],
    imports: [
      CommonModule,
      SharedModule,
      LockUnlockKiRoutingModule,
    ],
    entryComponents: [LockUnlockKiIndexComponent, LockUnlockKiSearchComponent],
    providers: [NgbActiveModal]
  })
export class LockUnlockKiModule { }
