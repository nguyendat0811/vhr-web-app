import { Component, OnInit } from '@angular/core';
import { LockUnlockKiService } from '@app/core/services/evaluation/lock-unlock-ki.service';
import { Routes, RouterModule, ActivatedRoute } from '@angular/router';
import { RESOURCE, ACTION_FORM, APP_CONSTANTS, SysCatService, DEFAULT_MODAL_OPTIONS, MEDIUM_MODAL_OPTIONS } from '@app/core';
import { CommonUtils, ValidationService } from '@app/shared/services';
import { Validators, FormGroup, FormArray, FormControl } from '@angular/forms';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { AppComponent } from '@app/app.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'lock-unlock-ki-search',
  templateUrl: './lock-unlock-ki-search.component.html',
  styleUrls: ['./lock-unlock-ki-search.component.css']
})
export class LockUnlockKiSearchComponent extends BaseComponent implements OnInit {
  typeColumn: any;
  typeYear: any;
  typeQuarter: any;
  typeMonth: any;
  listEvaluationPeriod: any;
  listMonthsOfYear: any;
  listQuarterOfYear: any;
  listYears: any;
  formConfig = {
    organizationId: ['', [ValidationService.required]],
    status: ['', [ValidationService.required]],
    typeLock: ['', [ValidationService.required]],
    periodType: ['', [ValidationService.required]],
    month: [''],
    quarter: [''],
    year: ['']
  };
  formSearch: FormGroup;
  constructor(public actr: ActivatedRoute
    , private router: Router
    , private lockUnlockService: LockUnlockKiService
    , private app: AppComponent
    , private modalService: NgbModal) {
    super(actr, RESOURCE.LOCK_UNLOCK_KI, ACTION_FORM.SEARCH);
    this.setMainService(lockUnlockService);
    this.typeMonth = 1;
    this.typeYear = 3;
    this.typeQuarter = 2;
    this.listMonthsOfYear = APP_CONSTANTS.MONTHS_OF_YEAR;
    this.listEvaluationPeriod = APP_CONSTANTS.EVALUATION_PERIOD;
    this.listQuarterOfYear = APP_CONSTANTS.QUARTER_OF_YEAR;
    this.listYears = APP_CONSTANTS.YEARS;
    this.formSearch = this.buildForm({}, this.formConfig, ACTION_FORM.SEARCH);
    // this.buildFormSave({});
    // this.buildSearchBasicFormGroup();
  }
  ngOnInit() {
    const CurrentYear = new Date().getFullYear();
    const CurrentMonth = new Date().getMonth();
    this.f.year.setValue(CurrentYear);
    this.f.month.setValue(CurrentMonth + 1);
    this.f.periodType.setValue(this.typeMonth);
    this.f.typeLock.setValue(1);
    this.f.status.setValue(2);
    // khoi tao listYears
    this.listYears = [
      { id: CurrentYear - 1, name: (CurrentYear - 1).toString() },
      { id: CurrentYear, name: (CurrentYear).toString() },
      { id: CurrentYear + 1, name: (CurrentYear + 1).toString() }
    ];
  }

  get f() {
    return this.formSearch.controls;
  }

  // thay doi loai thoi gian danh gia Evaluation_Period
  changeType() {
    const typePeriod = this.f.periodType.value;
    this.listMonthsOfYear = [];

    if (typePeriod === this.typeMonth) {
      this.f.month.setValue(parseInt(moment(new Date()).format('MM')));
      this.listMonthsOfYear = APP_CONSTANTS.MONTHS_OF_YEAR;
    } else if (typePeriod === this.typeQuarter) {
      this.f.month.setValue(parseInt(moment(new Date()).format('Q')));
      this.listMonthsOfYear = APP_CONSTANTS.QUARTER_OF_YEAR;

    }
  }

  public confirm(isLock?: number): void {
    // set validate cho 3 loai type_lock
    const typePeriod = this.f.periodType.value;
    if (typePeriod === this.typeMonth || typePeriod === this.typeQuarter) {
      const month = this.formSearch.value.month;
      this.formSearch.removeControl('month');
      this.formSearch.addControl('month', new FormControl(month, [ValidationService.required]));
    } else if (typePeriod === this.typeYear) {
      const month = this.formSearch.value.month;
      this.formSearch.removeControl('month');
      this.formSearch.addControl('month', new FormControl(month));
    }
    // truong hop loai khoa theo nam va cho tat ca
    const year = this.formSearch.value.year;
    this.formSearch.removeControl('year');
    this.formSearch.addControl('year', new FormControl(year, [ValidationService.required]));
    this.formSearch.controls['year'].setValidators(ValidationService.required);

    if (!this.validateBeforeSave()) {
      // reset lai validate control time
      // this.resetControlsTime();
      return;
    }
    if (isLock === 0) {
      this.processLockUnLock(isLock, 'app.evaluation.unlock');
    } else if (isLock === 1) {
      this.processLockUnLock(isLock, 'app.evaluation.lock');
    }
  }

  public processLockUnLock(type?: number, message?: string) {
    // reset lai validate control time
    this.resetControlsTime();
    // set gia tri khóa
    const isLock = this.f.status.value;
    this.f.status.setValue(type);
    const formSubmit = this.formSearch.value;
    // tra lai gia tri
    this.f.status.setValue(isLock);
    // bat dau xu li
    this.app.confirmMessage(message, () => {
      this.lockUnlockService.saveOrUpdate(formSubmit)
        .subscribe(res => {
          if (this.lockUnlockService.requestIsSuccess(res)) {
            setTimeout(() => {
              this.processSearchLockUnlock();
            }, 500);
          }
        });
    }, () => {
    });
  }

  /**
 * validateBeforeSave
 */
  private validateBeforeSave(): boolean {
    return CommonUtils.isValidForm(this.formSearch);
  }

  private resetControlsTime() {
    const year = this.formSearch.value.year;
    this.formSearch.removeControl('year');
    this.formSearch.addControl('year', new FormControl(year));

    const quarter = this.formSearch.value.quarter;
    this.formSearch.removeControl('quarter');
    this.formSearch.addControl('quarter', new FormControl(quarter));

    const month = this.formSearch.value.month;
    this.formSearch.removeControl('month');
    this.formSearch.addControl('month', new FormControl(month));

  }

  //
  public processSearchLockUnlock(event?): void {
    this.resetControlsTime();
    if (!CommonUtils.isValidForm(this.formSearch)) {
      return;
    }
    const params = this.formSearch ? this.formSearch.value : null;
    this.lockUnlockService.search(params, event).subscribe(res => {
      this.resultList = res;
      this.formatPeriod();
      this.typeColumn = this.resultList.extendData.typeColumn;
    });
    if (!event) {
      if (this.dataTable) {
        this.dataTable.first = 0;
      }
    }
  }

  public formatPeriod() {
    this.resultList.data.forEach(item => {
      if (item.month <= 9) {
        item.month = '0' + item.month;
      }
      switch (item.quarter) {
        case 1:
          item.quarter = 'I';
          break;
        case 2:
          item.quarter = 'II';
          break;
        case 3:
          item.quarter = 'III';
          break;
        case 4:
          item.quarter = 'IV';
          break;

      }
    });
  }

  // export

  processExportDetail() {
    if (!CommonUtils.isValidForm(this.formSearch)) {
      return;
    }
    // khoi tao gia tri cho truong null khi export
    if (this.formSearch.get('month').value == null) {
      this.formSearch.controls['month'].setValue('');
    }
    if (this.formSearch.get('year').value == null) {
      this.formSearch.controls['year'].setValue('');
    }
    const params = this.formSearch.value;
    this.lockUnlockService.processDetailReport(params).subscribe(res => {
      saveAs(res, 'lock_unlock_evaluation.xls');
    });
  }
}
