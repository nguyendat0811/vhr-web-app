import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PropertyResolver, CommonUtils } from '@app/shared/services';
import { RESOURCE } from '@app/core';
import { LockUnlockKiIndexComponent } from './lock-unlock-ki-index/lock-unlock-ki-index.component';

const routes: Routes = [
  {
    path: '',
    component: LockUnlockKiIndexComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.LOCK_UNLOCK_KI,
      nationId: CommonUtils.getNationId()
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LockUnlockKiRoutingModule { }
