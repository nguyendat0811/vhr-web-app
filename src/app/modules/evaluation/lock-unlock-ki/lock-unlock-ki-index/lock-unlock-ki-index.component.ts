import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { RESOURCE } from '@app/core';

@Component({
  selector: 'lock-unlock-ki-index',
  templateUrl: './lock-unlock-ki-index.component.html',
  styleUrls: ['./lock-unlock-ki-index.component.css']
})
export class LockUnlockKiIndexComponent extends BaseComponent implements OnInit {

  constructor(public act: ActivatedRoute) { 
    super(act, RESOURCE.LOCK_UNLOCK_KI);
  }

  ngOnInit() {
  }

}
