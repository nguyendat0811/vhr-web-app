import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { FormGroup, Validators, FormArray, MaxLengthValidator, FormControl } from '@angular/forms';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';

import { AppComponent } from '@app/app.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RESOURCE, ACTION_FORM, SysCatService, APP_CONSTANTS } from '@app/core';
import { OrganizationKiService } from '@app/core/services/evaluation/organization-ki.service';

import * as moment from 'moment';
import { CommonUtils, ValidationService } from '@app/shared/services';
import { HelperService } from '@app/shared/services/helper.service';


@Component({
  selector: 'organization-ki-search',
  templateUrl: './organization-ki-search.component.html',
  styleUrls: ['./organization-ki-search.component.css']
})
export class OrganizationKiSearchComponent extends BaseComponent implements OnInit {
  sysCatList: Array<any>;
  resultList: any = {};
  private navigationSubscription;
  listYear: Array<any>;
  listQuarter: Array<any>;
  private currentEvent: any;
  listMonth: Array<any>;
  listType = APP_CONSTANTS.EVALUATION_PERIOD;
  flagAffterImport = false;
  formConfig = {
    organizationId: ['', [ValidationService.required]],
    numberRecord: [10, [ValidationService.required, Validators.min(1), Validators.max(1000)]],
    status: [3, [ValidationService.required]],
    year: [parseInt(moment(new Date()).format('YYYY')), [ValidationService.required]],
    quarter: [parseInt(moment(new Date()).format('Q')), [ValidationService.required]],
    month: [parseInt(moment(new Date()).format('MM')), [ValidationService.required]],
    type: [1, [ValidationService.required]]

  };
  formSearch: FormGroup;
  //
  formSaveConfig = {
    organizationId: [''],
    organizationKiProcessId: [''],
    unitCode: [''],
    unitName: [''],
    isRate: [''],
    orgKiPoint: ['', [ValidationService.required, ValidationService.number, Validators.max(1000), Validators.min(1)]],
    orgKi: ['', [ValidationService.required]],
    note: ['', [ValidationService.maxLength(100)]],
    sysCatId: [''],
    modifiedBy: [''],
    modifiedDate: [''],
    createdBy: [''],
    createdDate: [''],
    type: [''],
    year: [parseInt(moment(new Date()).format('YYYY'))],
    quarter: [parseInt(moment(new Date()).format('Q'))],
    month: [parseInt(moment(new Date()).format('MM'))],
    isLock:[''],
    orgPath:['']

  };
  formSave: FormArray;

  changeType() {
    var value = this.formSearch.controls['type'].value;
    if (value == 1) {
      this.elementRequired('month', parseInt(moment(new Date()).format('MM')));
      this.elementNotRequired('quarter', 1);
    } else {
      if (value == 2) {
        this.elementRequired('quarter', parseInt(moment(new Date()).format('Q')));
        this.elementNotRequired('month', 1);
      } else {
        if (value == 3) {
          this.elementNotRequired('month', 1);
          this.elementNotRequired('quarter', 1);
        }
      }
    }
  }

  elementRequired(item, value) {
    this.formSearch.removeControl(item);
    this.formSearch.addControl(item, new FormControl(value, [ValidationService.required]));
  }
  elementNotRequired(item, value) {
    this.formSearch.removeControl(item);
    this.formSearch.addControl(item, new FormControl(value));
  }
  constructor(public actr: ActivatedRoute
    , private router: Router
    , private helperService: HelperService
    , private organizationKiService: OrganizationKiService
    , private app: AppComponent
    , private sysCatService: SysCatService
    , private modalService: NgbModal) {
    super(actr, RESOURCE.ORGANIZATION_KI_PROCESS, ACTION_FORM.SEARCH);
    this.setMainService(organizationKiService);
    this.formSearch = this.buildForm({}, this.formConfig, ACTION_FORM.SEARCH);
    this.listYear = this.organizationKiService.getYearList();
    this.listQuarter = this.organizationKiService.getQuaterList();
    this.listMonth = this.organizationKiService.getMonthList();
    this.buildFormSave([]);
    this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.KI_RANK)
      .subscribe(res =>{
        this.sysCatList= res.data;
      });
    // lay idOrg để tìm kiếm khi import xong
    const stateDataSearch = this.router.getCurrentNavigation().extras.state;
    if (stateDataSearch && stateDataSearch.data) {
      this.formSearch = this.buildForm(stateDataSearch.data, this.formConfig, ACTION_FORM.SEARCH);
      this.flagAffterImport = true;
    }
  }
  ngOnInit() {
    if (this.flagAffterImport) {
      this.processSearch();
      this.flagAffterImport = false;
    }
  }
  get f() {
    return this.formSearch.controls;
  }
  get fa() {
    return this.formSave.controls;
  }

  processSearch(event?) {
    if (CommonUtils.isValidForm(this.formSearch)) {
      this.currentEvent = event;
      const params = this.formSearch ? this.formSearch.value : null;
      this.organizationKiService.search(params, this.currentEvent).subscribe(res => {
        this.resultList = res;
        if (this.resultList) {
          this.buildFormSave(res.data);
        }
      });
      if (!this.currentEvent) {
        if (this.dataTable) {
          this.dataTable.first = 0;
        }
      }
    }
  }
  private makeDefaultFormSave(): FormGroup {
    const formGroup = this.buildForm({}, this.formSaveConfig, ACTION_FORM.UPDATE);
    return formGroup;
  }
  private buildFormSave(list?: any) {
    if (list == null) {
      this.formSave = new FormArray([this.makeDefaultFormSave()]);
    } else {
      const controls = new FormArray([]);
      for (const i in list) {
        const item = list[i];
        const group = this.makeDefaultFormSave();
        group.patchValue(item);

        group.controls['type'].setValue(this.formSearch.controls['type'].value);
        group.controls['year'].setValue(this.formSearch.controls['year'].value);
        group.controls['quarter'].setValue(this.formSearch.controls['quarter'].value);

        group.controls['month'].setValue(this.f['month'].value);
        // hien thi danh sach tim kiem
        group.controls['orgKi'].setValue((item.orgKi));
        group.controls['note'].setValue(item.note);
        group.controls['createdBy'].setValue(item.createdBy);
        group.controls['createdDate'].setValue(item.createdDate);
        group.controls['modifiedBy'].setValue(item.modifiedBy);
        group.controls['modifiedDate'].setValue(item.modifiedDate);
        if (item.isRate == 1) {
          group.removeControl('orgKiPoint')
          group.addControl('orgKiPoint', new FormControl(null))
          group.removeControl('orgKi')
          group.addControl('orgKi', new FormControl(null))
          
        }else {
          group.removeControl('orgKi');
          group.addControl('orgKi', new FormControl((item.orgKi), [ValidationService.required]));
          group.removeControl('orgKiPoint');
          group.addControl('orgKiPoint', new FormControl(item.orgKiPoint, [ValidationService.required, ValidationService.number, Validators.max(1000), Validators.min(1)]));
        }
        if(item.isLock == 1){
          group.removeControl('orgKi');
          group.addControl('orgKi', new FormControl(item.orgKi));
          group.removeControl('orgKiPoint');
          group.addControl('orgKiPoint', new FormControl(item.orgKiPoint));
          
          
        } 
        
        controls.push(group);
      }
      controls.setValidators(ValidationService.duplicateArray(
        ['unitCode'], 'unitCode', 'app.detailReport.attendanceCode'));
      this.formSave = controls;
    }
  }
  private validateBeforeSave(): boolean {

    return CommonUtils.isValidForm(this.formSave);
  }
  processSaveOrUpdate() {
    if (!this.validateBeforeSave()) {
      return;
    }
    this.app.confirmMessage(null, () => {// on accepted
      this.organizationKiService.saveOrUpdate(this.formSave.value)
        .subscribe(res => {
          if (this.organizationKiService.requestIsSuccess(res)) {
            this.helperService.isProcessing(res.data);
            this.processSearch(this.currentEvent);
            
          } else{
            this.processSearch(this.currentEvent);
          }
        });
    }, () => {
      // on rejected
    });


  }
  prepareExport() {
    if (!CommonUtils.isValidForm(this.formSearch)) {
      return;
    }
    const params = this.formSearch.value;
    this.organizationKiService.prepareExport(params).subscribe(res => {
      if (this.organizationKiService.requestIsSuccess(res)) {
        this.processExport();
      }

    }, () => {
    });
  }

  processExport() {

    if (!CommonUtils.isValidForm(this.formSearch)) {
      return;
    }
    const params = this.formSearch.value;
    this.organizationKiService.processExport(params).subscribe(res => {
      saveAs(res, 'export_organization_ki.xls');
    });
  }

  changeIsRate(item) {

    if (!item.controls['isRate'].value) {
      item.controls['orgKi'].setValue(null);
      item.controls['orgKiPoint'].setValue(null);
      item.removeControl('orgKiPoint');
      item.removeControl('orgKi');
      item.addControl('orgKiPoint', new FormControl(null));
      item.addControl('orgKi', new FormControl(null));
     
    } else {
      item.removeControl('orgKi');
      item.addControl('orgKi', new FormControl(null, [ValidationService.required]));
      item.removeControl('orgKiPoint');
      item.addControl('orgKiPoint', new FormControl(null, [ValidationService.required, ValidationService.number, Validators.max(1000), Validators.min(0)]));
     
    }
  }

  changeNumberRecord(){
    this.formSearch.controls['numberRecord'].setValue(this.formSearch.controls['numberRecord'].value.replace(/[^0-9 \,]/, ''));  
    
  }

}
