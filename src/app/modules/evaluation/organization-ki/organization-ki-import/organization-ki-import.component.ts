import { Component, OnInit } from '@angular/core';
import { CommonUtils, ValidationService } from '@app/shared/services';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { HelperService } from '@app/shared/services/helper.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from '@app/app.component';
import { ACTION_FORM, RESOURCE } from '@app/core';
import { FileControl } from '@app/core/models/file.control';
import * as moment from 'moment';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { OrganizationKiService } from '@app/core/services/evaluation/organization-ki.service';

@Component({
  selector: 'organization-ki-import',
  templateUrl: './organization-ki-import.component.html',
  styleUrls: ['./organization-ki-import.component.css']
})
export class OrganizationKiImportComponent  extends BaseComponent  implements OnInit {

  public commonUtils = CommonUtils;
  public dataError: any;
  formImport: FormGroup;
  listYear: Array<any>;
  listQuarter: Array<any>;
  listMonth: Array<any>;
  formConfig = {
    type: [1, [ValidationService.required]],
    month: [parseInt(moment(new Date()).format('MM')), [ValidationService.required]],
    quarter: [parseInt(moment(new Date()).format('Q')), [ValidationService.required]],
    year: [parseInt(moment(new Date()).format('YYYY')), [ValidationService.required]],
    organizationId: ['', [ValidationService.required]],
    numberRecord:[10],
    status:[3]
  };
  constructor(
    public act: ActivatedRoute
    , private helperService: HelperService
    , private modalService: NgbModal
    , private router: Router
    , public organizationKiService: OrganizationKiService
    , private app: AppComponent
    
  ) { 
    super(act, RESOURCE.ORGANIZATION_KI_PROCESS, ACTION_FORM.SEARCH);   //change resource
    this.setMainService(organizationKiService);
    this.formImport = this.buildForm({}, this.formConfig, ACTION_FORM.SEARCH);
    this.formImport.addControl('fileImport', new FileControl(null, ValidationService.required));
    
    this.listYear=this.organizationKiService.getYearList();
    this.listQuarter=this.organizationKiService.getQuaterList();
    this.listMonth=this.organizationKiService.getMonthList();

    
  }
  ngOnInit() {
    
  }
  
  get f() {
    return this.formImport.controls;
  }

  changeType(value){
    value=this.formImport.value.type;
    if(value==1){      
      this.elementRequired('month',parseInt(moment(new Date()).format('MM')));
      this.elementNotRequired('quarter',1);

    } else{
      if(value==2){
        this.elementNotRequired('month',1);
        this.elementRequired('quarter',parseInt(moment(new Date()).format('Q')));
      } else{
        if(value==3){
          this.elementNotRequired('month',1);
          this.elementNotRequired('quarter',1);
        } else{
          
        }
      }
    } 
  }
  
  elementRequired(item,value){
    this.formImport.removeControl(item);
    this.formImport.addControl(item, new FormControl(value,[ValidationService.required]));
  }
  elementNotRequired(item,value){
    this.formImport.removeControl(item);
    this.formImport.addControl(item, new FormControl(value));
  }
  processImportOrg() {
    this.formImport.controls['fileImport'].updateValueAndValidity();
    if (!CommonUtils.isValidForm(this.formImport)) {
      return;
    }
    this.app.confirmMessage(null, () => {// on accepted
      this.dataError = null;
      this.organizationKiService.processImport(this.formImport.value).subscribe(res => {
        if (!this.organizationKiService.requestIsSuccess(res)) {
          this.dataError = res.data;
          // this.dataError.forEach(element => {
          //   if(element.row == 8 && element.column == 1) {
          //     element.row  = null;
          //     element.columnLabel = null;
          //   }
          // });
        } else {
          this.dataError = null;
          this.router.navigate(['/evaluation/organization-ki'], {state: {data: this.formImport.value}});
          // this.router.navigate(['/evaluation/organization-ki/search'
          //                 , this.formImport.get('organizationId').value
          //                 , this.formImport.get('type').value
          //                 , this.formImport.get('year').value
          //                 , this.formImport.get('quarter').value
          //                 , this.formImport.get('month').value]);
        }
      });
    }, null);
  }

  processDownloadTemplate() {
    const params = this.formImport.value;
    delete params['fileImport'];
    this.formImport.removeControl('fileImport');
    this.formImport.addControl('fileImport', new  FormControl(null));
    if (!CommonUtils.isValidForm(this.formImport)) {
      return;
    }
    this.organizationKiService.downloadTemplateImport(params).subscribe(res => {
      saveAs(res, 'template-import-OrganizationKI.xls');
    });
    this.formImport.controls['fileImport'].setValidators(ValidationService.required);
  }
}
