import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PropertyResolver, CommonUtils } from '@app/shared/services';
import { RESOURCE } from '@app/core';
import { Routes, RouterModule } from '@angular/router';
import { OrganizationKiImportComponent } from './organization-ki-import/organization-ki-import.component';
import { OrganizationKiSearchComponent } from './organization-ki-search/organization-ki-search.component';

const routes: Routes = [
  {
    path: '',
    component: OrganizationKiSearchComponent,
  },
  {
    path:  'import',
    component: OrganizationKiImportComponent
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrganizationKiRoutingModule { }
