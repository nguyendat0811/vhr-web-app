import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrganizationKiIndexComponent } from './organization-ki-index/organization-ki-index.component';
import { OrganizationKiSearchComponent } from './organization-ki-search/organization-ki-search.component';
import { OrganizationKiRoutingModule } from './organization-ki-routing.module';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { OrganizationKiImportComponent } from './organization-ki-import/organization-ki-import.component';

@NgModule({
  declarations: [OrganizationKiIndexComponent, OrganizationKiSearchComponent, OrganizationKiImportComponent],
  imports: [
    CommonModule,
    OrganizationKiRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule
  ]
})
export class OrganizationKiModule {

 
  }

