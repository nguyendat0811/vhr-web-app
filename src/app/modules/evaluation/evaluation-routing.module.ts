import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/dashboard/evaluation',
    pathMatch: 'full'
  }, {
    path: 'lock-unlock-ki',
    loadChildren: './lock-unlock-ki/lock-unlock-ki.module#LockUnlockKiModule'
  }, 
  {
    path: 'employee-ki',
    loadChildren: './employee-ki/employee-ki.module#EmployeeKiModule'
  }, {
    path: 'organization-ki',
    loadChildren: './organization-ki/organization-ki.module#OrganizationKiModule'
  }
];
 
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EvaluationRoutingModule { }
