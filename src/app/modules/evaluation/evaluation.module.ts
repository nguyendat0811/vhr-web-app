import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@app/shared';
import { EvaluationRoutingModule } from './evaluation-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SharedModule,
    EvaluationRoutingModule
  ]
})
export class EvaluationModule { }
