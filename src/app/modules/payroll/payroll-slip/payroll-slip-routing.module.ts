import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'config',
    loadChildren: './payroll-slip-config/payroll-slip-config.module#PayrollSlipConfigModule'
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PayrollSlipRoutingModule { }
