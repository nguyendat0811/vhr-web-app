import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from './../../../../../shared/components/base-component/base-component.component';
import { Component, OnInit } from '@angular/core';
import { RESOURCE } from '@app/core';

@Component({
  selector: 'payroll-slip-config-index',
  templateUrl: './payroll-slip-config-index.component.html',
})
export class PayrollSlipConfigIndexComponent extends BaseComponent implements OnInit {

  constructor(public actr: ActivatedRoute) {
    super(actr, RESOURCE.PAYSLIP_CONFIG);
  }

  ngOnInit() {
  }

}
