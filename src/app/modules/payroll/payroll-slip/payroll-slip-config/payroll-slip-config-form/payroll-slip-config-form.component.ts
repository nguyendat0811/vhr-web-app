import { APP_CONSTANTS } from '@app/core/app-config';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { RESOURCE, ACTION_FORM, } from '@app/core';
import { FormGroup } from '@angular/forms';
import { ValidationService, CommonUtils } from '@app/shared/services';
import { AppComponent } from '@app/app.component';
import { PayslipConfigService } from '@app/core/services/hr-payroll/payslip-config.service';
import { FileControl } from '@app/core/models/file.control';
import { SalaryTypeOrganizationMappingService } from '@app/core';

@Component({
  selector: 'payroll-slip-config-form',
  templateUrl: './payroll-slip-config-form.component.html',
})
export class PayrollSlipConfigFormComponent extends BaseComponent implements OnInit {
  resultList: any = {};
  formSave: FormGroup;
  payslipId: number;
  salaryTypeList: any = {};
  statusList: any = {};
  formConfig = {
    salaryTypeName: [''],
    payslipId: [null],
    organizationId: ['', [ValidationService.required]],
    salaryTypeId: ['', [ValidationService.required]],
    dataQuery: ['', [ValidationService.required, ValidationService.maxLength(8000)]],
    status: ['', [ValidationService.required]]
  };
  constructor(
    public actr: ActivatedRoute
    , private app: AppComponent
    , public activeModal: NgbActiveModal
    , public payslipConfigService: PayslipConfigService
    , private salaryTypeOrganizationMappingService: SalaryTypeOrganizationMappingService
    ) {
    super(actr, RESOURCE.PAYSLIP_CONFIG, ACTION_FORM.INSERT);
    this.buildForms({}, ACTION_FORM.INSERT);
    this.statusList = APP_CONSTANTS.PAYSLIP_CONFIG_STATUS;
  }

  get f () {
    return this.formSave.controls;
  }

  ngOnInit() {
  }

  /**
   * buildForm
   */
  private buildForms(data?: any, actionForm?: ACTION_FORM): void {
    this.formSave = this.buildForm(data, this.formConfig, actionForm );
    this.formSave.addControl('file', new FileControl(null, ValidationService.required));
    if (data.fileAttachment) {
      (this.formSave.controls['file'] as FileControl).setFileAttachment(data.fileAttachment.payslipConfigFile);
    }

    // xu ly lang nghe thay doi don vi
    this.f.organizationId.valueChanges.subscribe(id => {
      this.f.salaryTypeId.setValue(null);
      this.initSalaryType(id);
    });
  }

  /**
   * setFormValue
   * param data
   */
  public setFormValue(propertyConfigs: any, data?: any) {
    this.propertyConfigs = propertyConfigs;
    if (data && data.payslipId > 0) {
      this.buildForms(data, ACTION_FORM.UPDATE);
      this.initSalaryType(data.organizationId);
    }
    this.f.organizationId.valueChanges.subscribe(id => {
      this.initSalaryType(id);
    });
  }

  processSaveOrUpdate() {
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    } else {
      this.app.confirmMessage(null, () => {// on accepted
        this.payslipConfigService.saveOrUpdateFormFile(this.formSave.value)
          .subscribe(res => {
            if (this.payslipConfigService.requestIsSuccess(res)) {
              this.activeModal.close(res);
              this.processSearch();
            }
          });
        }, () => {

        });
      }
    }

  /*
  * onChangeOrganization
  * Lay danh sach loai luong theo cau hinh mapping don vi
  */
  initSalaryType(id?) {
  if (id) {
    this.salaryTypeOrganizationMappingService.findByOrgId(id).subscribe(res => {
      this.salaryTypeList = res.data;
    });
  } else {
    this.salaryTypeList = null;
  }
}
}
