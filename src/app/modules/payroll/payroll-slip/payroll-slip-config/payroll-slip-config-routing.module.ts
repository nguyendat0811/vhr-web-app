import { CommonUtils } from './../../../../shared/services/common-utils.service';
import { PropertyResolver } from './../../../../shared/services/property.resolver';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PayrollSlipConfigIndexComponent } from './payroll-slip-config-index/payroll-slip-config-index.component';
import { RESOURCE } from '@app/core';

const routes: Routes = [
  {
    path: '',
    component: PayrollSlipConfigIndexComponent,
    resolve: {
      props: PropertyResolver
      },
      data: {
        resource: RESOURCE.PAYSLIP_CONFIG,
        nationId: CommonUtils.getNationId()
      }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PayrollSlipConfigRoutingModule { }
