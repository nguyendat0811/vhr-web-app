import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@app/shared';
import { PayrollSlipConfigFormComponent } from './payroll-slip-config-form/payroll-slip-config-form.component';
import { PayrollSlipConfigSearchComponent } from './payroll-slip-config-search/payroll-slip-config-search.component';
import { PayrollSlipConfigIndexComponent } from './payroll-slip-config-index/payroll-slip-config-index.component';
import { PayrollSlipConfigRoutingModule } from './payroll-slip-config-routing.module';

@NgModule({
  declarations: [PayrollSlipConfigFormComponent, PayrollSlipConfigSearchComponent, PayrollSlipConfigIndexComponent],
  imports: [
    CommonModule,
    SharedModule,
    PayrollSlipConfigRoutingModule,
  ],
  entryComponents: [PayrollSlipConfigFormComponent]
})
export class PayrollSlipConfigModule { }
