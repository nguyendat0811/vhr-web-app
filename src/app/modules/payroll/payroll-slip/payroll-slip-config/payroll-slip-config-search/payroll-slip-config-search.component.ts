import { FileStorageService } from '@app/core/services/file-storage.service';
import { PayrollSlipConfigFormComponent } from './../payroll-slip-config-form/payroll-slip-config-form.component';
import { APP_CONSTANTS, DEFAULT_MODAL_OPTIONS } from './../../../../../core/app-config';
import { CommonUtils } from './../../../../../shared/services/common-utils.service';
import { AppComponent } from './../../../../../app.component';
import { ValidationService } from './../../../../../shared/services/validation.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RESOURCE, ACTION_FORM } from '@app/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { PayslipConfigService } from '@app/core/services/hr-payroll/payslip-config.service';
import { SalaryTypeService } from './../../../../../core/services/hr-payroll/salary-type.service';

@Component({
  selector: 'payroll-slip-config-search',
  templateUrl: './payroll-slip-config-search.component.html',
})
export class PayrollSlipConfigSearchComponent extends BaseComponent implements OnInit {
  formSearch: FormGroup;
  public commonUtils = CommonUtils;
  salaryTypeList: any = {};
  statusList: any = {};
  resultList: any = {};
  formConfig = {
    salaryTypeId: [null, [ValidationService.maxLength(20)]],
    status: [null, [ValidationService.maxLength(1)]]
  };
  constructor(public actr: ActivatedRoute
    , private modalService: NgbModal
    , public payslipConfigService: PayslipConfigService
    , private app: AppComponent
    , private salaryTypeService: SalaryTypeService
    , public fileStorage: FileStorageService
    ) {
      super(actr, RESOURCE.PAYSLIP_CONFIG,  ACTION_FORM.SEARCH);
      this.setMainService(payslipConfigService);
      this.salaryTypeService.getList().subscribe(res => {
        this.salaryTypeList = res.data;
      });
      this.formSearch = this.buildForm({}, this.formConfig, ACTION_FORM.SEARCH);
      this.statusList = APP_CONSTANTS.PAYSLIP_CONFIG_STATUS;
      this.payslipConfigService.search().subscribe(res => {
        this.resultList = res;
      });
  }
  ngOnInit() {
    this.processSearch();
  }

  get f () {
    return this.formSearch.controls;
  }

  prepareSaveOrUpdate(item) {
    if (item && item.payslipId > 0) {
      this.payslipConfigService.findOne(item.payslipId)
        .subscribe(res => {
          this.activeFormModal(this.modalService, PayrollSlipConfigFormComponent, res.data);
        });
    } else {
        this.activeFormModal(this.modalService, PayrollSlipConfigFormComponent, {});
    }
  }

  public downloadFile(fileData) {
    this.fileStorage.downloadFile(fileData.id).subscribe(res => {
        saveAs(res , fileData.fileName);
    });
  }

  processDelete(item) {
    if (item && item.payslipId > 0) { // on accepted
      this.app.confirmDelete(null, () => {
        this.payslipConfigService.deleteById(item.payslipId)
          .subscribe(res => {
            if (this.payslipConfigService.requestIsSuccess(res)) {
              this.processSearch(null);
            }
          });
      }, () => { // on rejected

      });
    }
  }
}
