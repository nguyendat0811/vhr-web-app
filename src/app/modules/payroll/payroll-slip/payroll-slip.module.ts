import { SharedModule } from '@app/shared';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PayrollSlipRoutingModule } from './payroll-slip-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SharedModule,
    PayrollSlipRoutingModule,
  ]
})
export class PayrollSlipModule { }
