import { ProgressIndexComponent } from './payroll/progress/progress-index/progress-index.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonUtils, PropertyResolver } from '@app/shared/services';
import { RESOURCE } from '@app/core';
import { ConfigIndexComponent } from './payroll/config/config-index/config-index.component';
import { CalculatorIndexComponent } from './payroll/calculator/calculator-index/calculator-index.component';
import { RecalculateIndexComponent } from './payroll/calculator/recalculate-index/recalculate-index.component';

const routes: Routes = [
  {
    path: 'data-input',
    loadChildren: './data-input/data-input.module#DataInputModule'
  }
  , {
    path: 'setting',
    loadChildren: './setting/setting.module#SettingModule'
  }
  , {
    path: 'formula',
    loadChildren: './formula/formula.module#FormulaModule'
  }, {
    path: 'payroll-slip',
    loadChildren: './payroll-slip/payroll-slip.module#PayrollSlipModule'
  }, {
    path: 'config',
    component: ConfigIndexComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.PAYROLL_TABLE_CONFIG,
      nationId: CommonUtils.getNationId()
    }
  }, {
    path: 'calculate',
    component: CalculatorIndexComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      isSearchOnly: false,
      resource: RESOURCE.PAYROLL_GENERAL,
      nationId: CommonUtils.getNationId()
    }
  }, {
    path: 'search',
    component: CalculatorIndexComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      isSearchOnly: true,
      resource: RESOURCE.PAYROLL_GENERAL,
      nationId: CommonUtils.getNationId()
    }
  }
  , {
    path: 'recalculate',
    component: RecalculateIndexComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.PAYROLL_GENERAL,
      nationId: CommonUtils.getNationId()
    }
  }
  , {
    path: 'progress',
    component: ProgressIndexComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.PAYROLL_MASTER,
      nationId: CommonUtils.getNationId()
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PayrollRoutingModule { }
