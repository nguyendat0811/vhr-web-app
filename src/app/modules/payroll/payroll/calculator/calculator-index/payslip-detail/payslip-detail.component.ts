import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DomSanitizer } from '@angular/platform-browser';
import { PayslipService } from '@app/core';

@Component({
  selector: 'payslip-detail',
  templateUrl: './payslip-detail.component.html'
})
export class PayslipDetailComponent implements OnInit {

  public data: any;

  constructor(public activeModal: NgbActiveModal,
    public sanitizer: DomSanitizer,
    public payslipService: PayslipService) {
  }

  ngOnInit() {
  }

  setData(form) {
    const url = this.payslipService.getLinkViewDetail(form);
    this.data = this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
}
