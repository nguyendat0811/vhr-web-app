import { RESOURCE, ACTION_FORM } from '@app/core/app-config';
import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { PayrollCalculateService } from '@app/core';
import * as moment from 'moment';
import { AppComponent } from '@app/app.component';

@Component({
  selector: 'payment-sync',
  templateUrl: './payment-sync.component.html',
})
export class PaymentSyncComponent extends BaseComponent implements OnInit {
  listPaymentItem = [];
  listPeriod = [];
  formSync: FormGroup;
  formConfig = {
    paymentItemId: [null],
    paymentMonth: [null],
    organizationId: [null],
    salaryTypeId: [null],
    year: [parseInt(moment(new Date()).format('YYYY'))],
    periodId: [null],
    employeeCode: [''],
    employeeName: [''],
    payrollGeneralId: ['']
  };
  constructor(
    public actr: ActivatedRoute,
    public activeModal: NgbActiveModal,
    private payrollCalculateService: PayrollCalculateService,
    private app: AppComponent,
  ) {
    super(actr, RESOURCE.PAYROLL_GENERAL, ACTION_FORM.PAYROLL_GENERAL_SEARCH);
    this.formSync = this.buildForm({}, this.formConfig, ACTION_FORM.PAYROLL_GENERAL_SEARCH);
    // this.payrollCalculateService.getListIncomeItem().subscribe(
    //   res => {
    //     this.listIncomeItem = res.data;
    //   }
    // );
  }

  ngOnInit() {
    this.formSync.get("paymentMonth").valueChanges.subscribe(value => {
      if (value && this.listPeriod.length > 0) {
        let tmp = this.listPeriod.find(x => x.startDate == value);
        if (tmp) {
          this.prepareForm(tmp.periodId);
        } else {
          this.listPaymentItem = [];
        }
      }
    })
  }

  prepareForm(periodId) {
    if (periodId) {
      this.payrollCalculateService.getListPaymentItem(periodId).subscribe(
        res => {
          this.listPaymentItem = res.data;
        }
      );
    }
  }

  get f() {
    return this.formSync.controls;
  }

  processPaymentSync() {
    this.payrollCalculateService.prepareTaxSync(this.formSync.value).subscribe(res => {
      if (res && res.code === "error") {
        this.activeModal.close(res);
        return;
      }
      else{
        this.app.confirmMessage('payroll.calculate.confirmSyncPayment', () => {// on accepted
          this.payrollCalculateService.processSyncPayment(this.formSync.value).subscribe(res => {
            if (this.payrollCalculateService.requestIsSuccess(res)) {
              this.app.successMessage('sync.payment', 'message.success');
              this.activeModal.close(res);
            } else {
              console.log("res", res);
            }
          });
        }, null);
      }
    });
  }

  cancelPaymentSync() {
    this.payrollCalculateService.prepareTaxSync(this.formSync.value).subscribe(res => {
      if (res && res.code === "error" && res.type === "payroll.isLocked") {
        this.activeModal.close(res);
        return;
      }
      else{
        this.app.confirmMessage('payroll.calculate.confirmCancel', () => {// on accepted
          this.payrollCalculateService.cancelSyncPayment(this.formSync.value).subscribe(res => {
            if (this.payrollCalculateService.requestIsSuccess(res)) {
              this.activeModal.close(res);
            } else {
              console.log("res", res);
            }
          });
        }, null);
      }
    });
  }

  public setFormValue(propertyConfigs: any, data?: any) {
    this.propertyConfigs = propertyConfigs;
    if (data) {
      this.listPeriod = data.listPeriod;
      delete data.listPeriod;
      this.formSync = this.buildForm(data, this.formConfig, ACTION_FORM.PAYROLL_GENERAL_SEARCH);
      this.prepareForm(data.periodId);
      let tmp = this.listPeriod.find(x => x.periodId == data.periodId);
      if (tmp) {
        this.formSync.controls.paymentMonth.setValue(tmp.startDate);
      }
    }
  }
}
