import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'error-detail',
  templateUrl: './error-detail.component.html'
})
export class ErrorDetailComponent implements OnInit {

  public data: any;
  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

  setData(data) {
    console.log(data);
    this.data = data;
  }
}
