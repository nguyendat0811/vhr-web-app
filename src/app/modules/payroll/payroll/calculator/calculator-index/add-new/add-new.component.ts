import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { RESOURCE, APP_CONSTANTS, ACTION_FORM, PayrollTableConfigService, PayrollCalculateService } from '@app/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, Validators } from '@angular/forms';
import { ValidationService } from '@app/shared/services';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { AppComponent } from '@app/app.component';

@Component({
  selector: 'add-new',
  templateUrl: './add-new.component.html'
})
export class AddNewComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  formConfigNew = {};
  formConfig = {
    organizationId: ['', [ValidationService.required]],
    organizationRootId: ['', [ValidationService.required]],
    orgId: [''],
    payrollGeneralId: [''],
    salaryTypeId: [''],
    periodId: [''],
    startDate: ['', [ValidationService.required]],
    endDate: ['', [ValidationService.required]],
    employeeCode: ['', [ValidationService.required, ValidationService.maxLength(45)]],
    employeeName: ['', [ValidationService.required, ValidationService.maxLength(100)]],
    createBy: [''],
    createDate: [''],
  };
  listConfig: any;
  listMasterConfig: any;
  constructor(public activeModal: NgbActiveModal
            , public actr: ActivatedRoute
            , public payrollCalculateService: PayrollCalculateService
            , public payrollTableConfigService: PayrollTableConfigService
            , private app: AppComponent) {
    super(actr, RESOURCE.SYSTEM_PARAMETER, ACTION_FORM.INSERT);
    this.buildFormAdd({},  ACTION_FORM.INSERT, this.formConfig);
  }

  ngOnInit() {
  }

  get f () {
    return this.formSave.controls;
  }

  /**
   * buildFormAdd
   * @ param data
   * @ param actionForm
   * @ param form
   */
  private buildFormAdd(data?: any, actionForm?: ACTION_FORM, form?) {
    this.formSave = this.buildForm(data, form, actionForm,
      [ ValidationService.notAffter('startDate', 'endDate', 'app.system-parameter.endDate')]);
  }

  /**
   * set dữ liệu cho form
   */
  setData(propertyConfigs: any, data, dataForm?) {
    console.log('data:', data);
    data['organizationRootId'] = data.organizationId;
    if (dataForm) {
      data['orgId'] = dataForm.orgId;
    } else {
      data['orgId'] = data.orgId;
    }
    this.propertyConfigs = propertyConfigs;
    if (data && data.payrollGeneralId > 0) {
      this.handForm(data, ACTION_FORM.UPDATE);
    } else {
      this.handForm(data, ACTION_FORM.INSERT);
    }
  }

  /**
   * Xử lý hiện các trường trên form
   * @ param data
   * @ param actionForm
   */
  handForm(data?, actionForm?) {
    this.payrollCalculateService.getListConfigIgnored(data.orgId, data.organizationRootId, data.salaryTypeId)
    .subscribe(res => { this.listConfig = res;
      console.log('res: ', res)
      if (this.listConfig.length > 0) {
        for (let i = 0; i < this.listConfig.length; i++) {
          const obj = this.listConfig[i];
          const elements = this.formConfig;
          const  term: string =  obj.field ;
          elements[term] = [''];
          if (obj.attributeType === '5') {
            elements[term] = ['', [ValidationService.integer, Validators.min(0)]];
          } else if (obj.attributeType === '4') {
            elements[term] = ['', [ValidationService.number, Validators.min(0)]];
          }
          this.formConfigNew = elements;
        }
        this.tranferListConfig();
        this.buildFormAdd(data, actionForm, this.formConfigNew);
      } else {
        this.app.warningMessage('payroll', 'calculate.payrollItemNotConfig');
        this.buildFormAdd(data, actionForm, this.formConfig);
      }
    });
  }

  checkAttributeType(type) {
    let arrayValidate;
    if (type === 5) {
      arrayValidate = ',' + [ValidationService.positiveNumber];
    }
    return arrayValidate;
  }

  /**
   *  THực hiện lưu dữ liệu
   */
  processSaveOrUpdate() {
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    }
    this.app.confirmMessage(null, () => {// on accepted
      this.payrollCalculateService.processSaveOrUpdatePayrolGeneral(this.formSave.value)
        .subscribe(res => {
          if (this.payrollCalculateService.requestIsSuccess(res)) {
            this.activeModal.close(res);
          }
        });
      }, () => {// on rejected
    });
  }

  private tranferListConfig() {
    this.listMasterConfig = [];
    for (let i = 0; i < this.listConfig.length; i += 2) {
      const obj = {
        listConfig: [this.listConfig[i], this.listConfig[i + 1]]
      };
      this.listMasterConfig.push(obj);
    }
  }
}
