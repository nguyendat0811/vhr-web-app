import { FileStorageService } from './../../../../../core/services/file-storage.service';
import { ImportPayrollGeneralComponent } from './import-payroll-general/import-payroll-general.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { ACTION_FORM, RESOURCE, SalaryTypeService, PayrollCalculateService, DataTypeService, PayrollTableConfigService, OrgSelectorService, DEFAULT_MODAL_OPTIONS, PayrollProgressService } from '@app/core';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import * as moment from 'moment';
import { PeriodService } from '@app/core/services/hr-payroll/period.serviece';
import { ValidationService, CommonUtils } from '@app/shared/services';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import { AppComponent } from '@app/app.component';
import { OrgTreeComponent } from '@app/shared/components/org-tree/org-tree.component';
import { environment } from '@env/environment';
import { TreeNode, MenuItem } from 'primeng/api';
import { SalaryTypeOrganizationMappingService } from '@app/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AddNewComponent } from './add-new/add-new.component';
import { TranslationService } from 'angular-l10n';
import { PayslipService } from '@app/core/services/hr-payroll/payslip.service';
import { PayslipDetailComponent } from './payslip-detail/payslip-detail.component';
import { HrStorage } from '@app/core/services/HrStorage';
import { PayrollReportTypeService } from '@app/core/services/hr-payroll/payroll-report-type.service';
import { TaxSyncComponent } from './tax-sync/tax-sync.component';
import { PaymentSyncComponent } from './payment-sync/payment-sync.component';
import { HelperService } from '@app/shared/services/helper.service';
import { flatMap } from 'rxjs/operators';
import { XnkNotificationService } from '@app/core/services/hr-payroll/xnk-notification.service';
import { MessageDialogComponent } from '@app/shared/components/message-dialog/message-dialog.component';

@Component({
  selector: 'calculator-index',
  templateUrl: './calculator-index.component.html'
})
export class CalculatorIndexComponent extends BaseComponent implements OnInit {
  formSearch: FormGroup;
  periodBeanForm: FormGroup;
  @ViewChild('tree')
  tree: OrgTreeComponent;
  organizationIdSelected = null;
  orgId;
  formConfig = {
    organizationId: [null, [ValidationService.required]],
    salaryTypeId: [null, [ValidationService.required]],
    year: [parseInt(moment(new Date()).format('YYYY'))],
    periodId: [null, [ValidationService.required]],
    employeeCode: ['', [ValidationService.maxLength(100)]],
    employeeName: ['', [ValidationService.maxLength(100)]],
    payrollGeneralId: [''],
  };

  periodBean = {
    organizationId: [null],
    organizationIds: [null],
    year: [null],
  };

  checkSingle = false;
  display = false;
  contentMessage: string[];

  salaryTypeList: any;
  yearList: any;
  periodList: any;
  listPayrollReportType: any;
  isShowDelete: any = false;

  listDataType = [];
  listPayrollConfig = [];
  listPayrollPayload = [];

  private ws: any;
  finishedProcess = true;
  errorProcess = false;

  finishedProcessCal = true;
  errorProcessCal = false;
  valueCal = null;
  empCal = '';
  messageError = '';
  isSearchOnly = false;

  titlePopup = 'payroll.process.listError.title';

  listMessageCal: any;

  // Cay don vi
  public nodes: TreeNode[];
  public selectedNode: TreeNode;

  // contextmenu
  menuItems: MenuItem[];
  selectedObject: any;

  constructor(public actr: ActivatedRoute,
    public salaryTypeService: SalaryTypeService,
    public periodService: PeriodService,
    public payrollCalculateService: PayrollCalculateService,
    public dataTypeService: DataTypeService,
    private payrollMasterService: PayrollProgressService
    , private modalService: NgbModal
    , private app: AppComponent
    , private payrollTableConfigService: PayrollTableConfigService
    , private orgSelectorService: OrgSelectorService
    , private router: ActivatedRoute
    , private translation: TranslationService, private salaryTypeOrganizationMappingService: SalaryTypeOrganizationMappingService
    , private payslipService: PayslipService
    , private payrollReportTypeService: PayrollReportTypeService
    , private helperService: HelperService
    , private notificationService: XnkNotificationService
    , public fileStorage: FileStorageService) {
    super(actr, RESOURCE.PAYROLL_GENERAL, ACTION_FORM.PAYROLL_GENERAL_SEARCH);
    if (!this.hasPermission('action.view')) {
      return;
    }
    this.formSearch = this.buildForm({}, this.formConfig, ACTION_FORM.PAYROLL_GENERAL_SEARCH);

    this.yearList = this.getYearList();
    this.periodBeanForm = this.buildForm({}, this.periodBean, ACTION_FORM.PAYROLL_GENERAL_SEARCH);

    this.initSocket();

    // xu ly lang nghe thay doi loai luong
    this.f.salaryTypeId.valueChanges.subscribe(data => {
      this.initDataTypeInput();
      this.initPayrollItem();
      this.initPayrollReportType();
    });

    // xu ly lang nghe thay doi don vi
    this.f.organizationId.valueChanges.subscribe(data => {
      if (data) {
        this.initSalaryType();
        this.getPeriod(data, this.f.year.value);
        this.initPayrollItem();
        this.actionInitAjax();
        this.orgId = data;
      }
    });

    // xu ly lang nghe thay doi nam
    this.f.year.valueChanges.subscribe(data => {
      if (this.f.organizationId.value) {
        this.getPeriod(this.f.organizationId.value, data);
      }
    });

    this.formSearch.statusChanges.subscribe(data => {
      this.checkSingleSelect();
    })

    // Xu ly tao context menu
    this.initContextMenu();

    this.notificationService.getActiveNotification().subscribe(rs => {
      if (rs.data) {
        const modalRef = this.modalService.open(MessageDialogComponent, DEFAULT_MODAL_OPTIONS);
        modalRef.componentInstance.title = rs.data.title;
        modalRef.componentInstance.message = rs.data.content;
      }
    });
  }

  ngOnInit() {
    this.router.data.subscribe((data: any) => {
      if (data && data.isSearchOnly) {
        this.isSearchOnly = data.isSearchOnly;
      }
    });
  }

  get f() {
    return this.formSearch.controls;
  }

  private getPeriod(organizationId?, year?): void {
    this.periodBeanForm.controls['organizationIds'].setValue(organizationId);
    this.periodBeanForm.controls['year'].setValue(year);
    this.periodService.getDataByOrgIdsAndYear(this.periodBeanForm.value).subscribe(res => {

      res.data.forEach(x => {
        if (x.isCurrent == 1) {
          this.formSearch.controls['periodId'].setValue(x.periodId);
        }
        x.periodName = x.periodName + '( ' +
          moment(new Date(x.startDate)).format(CommonUtils.getDateFormat().toUpperCase()) + ' - ' +
          moment(new Date(x.endDate)).format(CommonUtils.getDateFormat().toUpperCase()) + ' )';
      });
      this.periodList = res.data;
    });
  }

  private getYearList() {
    this.yearList = [];
    const currentYear = new Date().getFullYear();
    for (let i = (currentYear - 50); i <= (currentYear + 50); i++) {
      const obj = {
        year: i
      };
      this.yearList.push(obj);
    }
    return this.yearList;
  }

  actionSearchCalculate(event?) {
    if (!CommonUtils.isValidForm(this.formSearch)) {
      return;
    }

    this.finishedProcess = true;
    this.errorProcess = false;

    this.finishedProcessCal = true;
    this.errorProcessCal = false;

    let arrSalaryType = this.f.salaryTypeId.value;
    let param = Object.assign({}, this.formSearch.value);
    // Click button search, khong lay don vi tren cay
    if (!event) {
      this.organizationIdSelected = null;
      this.selectedNode = null;
    }
    // Lay gia tri khi chon don vi tren cay
    if (this.organizationIdSelected) {
      param['organizationId'] = this.organizationIdSelected;
    } else {
      param['organizationId'] = this.f.organizationId.value;
    }
    param['organizationRootId'] = this.f.organizationId.value;
    param['organizationIds'] = this.f.organizationId.value + '';
    param['salaryTypeId'] = arrSalaryType[0];
    //Xóa để tìm kiếm theo nhiều đơn vị
    delete param['organizationRootId'];
    delete param['organizationId'];
    this.isShowDelete = false;
    this.payrollCalculateService.search(param, event).subscribe(res => {
      this.resultList = res;
      this.checkBeforeShowDelete();
    });
    if (!event) {
      if (this.dataTable) {
        this.dataTable.first = 0;
      }
    }
  }

  checkBeforeShowDelete() {
    if (!this.checkSingle) {
      return;
    }
    let arrSalaryType = this.f.salaryTypeId.value;
    let param = Object.assign({}, this.formSearch.value);
    param['organizationRootId'] = this.f.organizationId.value;
    param['organizationIds'] = this.f.organizationId.value + '';
    param['salaryTypeId'] = arrSalaryType[0];
    //Xóa để tìm kiếm theo nhiều đơn vị
    delete param['organizationRootId'];
    delete param['organizationId'];
    this.payrollCalculateService.checkBeforeShowDelete(param).subscribe(res => {
      if (res && res.code === "success") {
        this.isShowDelete = true;
      }
    })
  }

  showMessage(payloadItem) {
    this.display = true;
    this.contentMessage = JSON.parse(payloadItem.message);
  }
  actionCalculate() {
    this.valueCal = 0;
    if (!CommonUtils.isValidForm(this.formSearch)) {
      return;
    }
    for (let _i = 0; _i < this.listPayrollPayload.length; _i++) {
      this.listPayrollPayload[_i] = {};
    }
    let params = Object.assign({}, this.formSearch.value);
    delete params["salaryTypeId"];
    delete params["organizationId"];
    params["organizationIds"] = this.formSearch.controls.organizationId.value;
    params["salaryTypeIds"] = this.formSearch.controls.salaryTypeId.value;
    this.app.confirmMessage('payroll.calculate.confirmCalculate', () => {// on accepted
      this.finishedProcess = false;
      this.finishedProcessCal = false;
      if (!this.checkSingle) {
        this.helperService.isProcessing(true);
      }
      this.payrollCalculateService.actionRunProcess(params).subscribe(res => {
        this.finishedProcess = true;
        if (res && res.code === "error") {
          if (res.type === "ERROR.payroll.calculate.isLock") {
            this.app.errorMessage('payroll.calculate', 'isLock');
            return;
          }
          else if (res.type === "ERROR.payroll.calculate.paid") {
            this.app.errorMessage('payroll.calculate', 'paid');
            return;
          }
          else if (res.type === "ERROR") {
            this.app.errorMessage('error', '');
            return;
          }
          else {
            this.app.errorMessage('payroll', this.messageError);
            return;
          }
        }
        else if (res && res.code === "success") {
          this.titlePopup = 'payroll.process.listError.title';
          this.listMessageCal = res.data;
          this.display = true;
          //this.helperService.APP_TOAST_MESSAGE.next({type: 'SUCCESS', code: 'success', message: null});
        }
        else if (res && res.type == "WARNING") {
          this.app.warningMessage(res.code);
          return;
        }
        // let payload = {
        //   periodId: this.formSearch.value.periodId,
        //   organizationId: this.formSearch.value.organizationId,
        //   salaryTypeId: this.formSearch.value.salaryTypeId,
        //   status: 1, //Đã tính lương 
        //   calculateDate: new Date(),
        //   isLock: 0, //0: Chưa khóa, 1: Đã khóa
        // }
        // this.payrollMasterService.saveOrUpdate(payload).subscribe(response => {
        //   console.log('response');
        // });
        if (this.payrollCalculateService.requestIsSuccess(res)) {
          this.actionSearchCalculate();
          this.errorProcess = false;
          this.errorProcessCal = false;
        } else {
          this.errorProcess = true;
          this.errorProcessCal = false;
        }
        if (!this.checkSingle) {
          this.helperService.isProcessing(true);
        }
      });
    }, null);
  }

  actionCancelCalculate() {
    if (!CommonUtils.isValidForm(this.formSearch)) {
      return;
    }
    let params = Object.assign({}, this.formSearch.value);
    delete params["salaryTypeId"];
    delete params["organizationId"];
    params["organizationIds"] = this.formSearch.controls.organizationId.value;
    params["salaryTypeIds"] = this.formSearch.controls.salaryTypeId.value;
    this.app.confirmMessage('payroll.calculate.cancelCalculate', () => {// on accepted
      this.payrollCalculateService.actionDestroyProcess(params).subscribe(res => {
        if (res && res.code === "error") {
          if (res.type === "ERROR.payroll.calculate.isLock") {
            this.app.errorMessage('payroll.calculate', 'isLock');
            return;
          }
          if (res.type === "ERROR.payroll.calculate.paid") {
            this.app.errorMessage('payroll.calculate', 'paid');
            return;
          }
        }
        else if (res && res.code === "success") {
          this.titlePopup = 'payroll.process.listCancel.title';
          this.listMessageCal = res.data;
          this.display = true;
        }
        this.actionSearchCalculate();
      });
    }, null);
  }

  private initSocket(): void {
    // const socket = new WebSocket(environment.payrollWs);
    var socket = new SockJS(environment.payrollWs);
    this.ws = Stomp.over(socket);
    this.ws.debug = null;
    const that = this;

    this.ws.connect({}, function (frame) {
      console.log('socket init', frame);
      const userInfo = HrStorage.getUserToken();
      that.ws.subscribe('/topic/payrollProcess-' + userInfo.employeeCode, function (message) {
        const messageBody = JSON.parse(message.body);
        if (messageBody.key === 'START') {
          that.listPayrollPayload[0] = messageBody;
        } else if (messageBody.key === 'FINISH') {
          that.listPayrollPayload[that.listPayrollPayload.length - 1] = messageBody;
        } else {
          const step = parseInt(messageBody.key.split('_')[1]);
          that.listPayrollPayload[step + 1] = messageBody;
        }
      });
      that.ws.subscribe('/topic/payrollProcessCal-' + userInfo.employeeCode, function (message) {
        const messageBody = JSON.parse(message.body);
        that.valueCal = messageBody.value;
        if (that.valueCal === 100) {
          if (messageBody.status === 0) {
            that.messageError = messageBody.message;
          }
          setTimeout(() => {
            that.valueCal = null;
          }, 2000);
        }
      });
    }, function (error) {
      console.log('error', error);
      that.finishedProcess = true;
      that.finishedProcessCal = true;
    });
  }

  private initPayrollItem() {
    if (this.f.organizationId.value && this.f.salaryTypeId.value) {
      let arrSalaryType = this.f.salaryTypeId.value;
      this.payrollTableConfigService.getListPayrollItemConfigV2(this.f.organizationId.value, arrSalaryType[0]).subscribe(
        res => {
          this.listPayrollConfig = res.data;
        }
      );
    } else {
      this.listPayrollConfig = [];
    }
  }

  private initDataTypeInput() {
    if (this.f.salaryTypeId.value) {
      let arrSalaryType = this.f.salaryTypeId.value;
      this.payrollCalculateService.getListDataCalculate(arrSalaryType[0]).subscribe(res => {
        this.listDataType = res.data;
        this.listPayrollPayload = [{}, {}];
        this.listDataType.forEach(x => {
          this.listPayrollPayload.push({});
        });
      });
    } else {
      this.listDataType = [];
      this.listPayrollPayload = [{}, {}];
    }
  }

  getStyle(item) {
    if (item.attributeType === '4' || item.attributeType === '5') {
      return 'vt-align-right';
    } else if (item.attributeType === '6') {
      return '';
    } else {
      return 'vt-align-center';
    }
  }

  // Xu ly cay don vi

  /**
   * getParams
   */
  private getParams() {
    return {
      operationKey: 'action.view',
      adResourceKey: 'resource.organization',
      nodeId: '',
      rootId: this.f.organizationId.value,
      showOrgExpried: false,
      checkPermission: false
    };
  }

  /**
 * action init ajax
 */
  private actionInitAjax() {
    this.orgSelectorService.actionInitAjaxPayroll(this.getParams())
      .subscribe((res) => {
        this.nodes = CommonUtils.toTreeNode(res);
      });
  }
  /**
   * actionLazyRead
   * @ param event
   */
  public actionLazyRead(event) {
    const params = this.getParams();
    params.nodeId = event.node.nodeId;
    if (event.node.children && event.node.children.length > 0) {
      return;
    }
    this.orgSelectorService.actionLazyReadPayroll(params)
      .subscribe((res) => {
        event.node.children = CommonUtils.toTreeNode(res);
      });
  }

  /**
   * Xu ly chon don vi tren cay
   */
  public nodeSelect(e, node?) {
    if (!CommonUtils.isValidForm(this.formSearch)) {
      return;
    }
    if (node) {
      this.organizationIdSelected = node.nodeId;
    } else {
      this.organizationIdSelected = e.node.nodeId;
    }

    let param = Object.assign({}, this.formSearch.value);
    param['organizationId'] = this.organizationIdSelected;
    param['organizationRootId'] = this.f.organizationId.value;
    this.orgId = this.f.organizationId.value;

    this.payrollCalculateService.search(param, null).subscribe(res => {
      this.resultList = res;
    });
    if (this.dataTable) {
      this.dataTable.first = 0;
    }
  }

  /*
  * auth: thaopv
  * onChangeOrganization
  * Lay danh sach loai luong theo cau hinh mapping don vi
  */
  initSalaryType() {
    if (this.f.organizationId.value) {
      this.salaryTypeOrganizationMappingService.findByOrgIds(this.f.organizationId.value).subscribe(res => {
        this.salaryTypeList = res.data;
        var defaultType = this.salaryTypeList.filter(x => x.salaryTypeId == 4949);
        if (defaultType.length > 0) {
          this.formSearch.controls['salaryTypeId'].setValue([4949]);
        }
      });
    } else {
      this.salaryTypeList = null;
    }
  }

  /**
   * quoctv
   * export action
   */
  public processExport(event?) {
    if (!CommonUtils.isValidForm(this.formSearch)) {
      return;
    }
    let params = Object.assign({}, this.formSearch.value);
    // add
    if (!event) {
      this.organizationIdSelected = null;
      this.selectedNode = null;
    }
    // Lay gia tri khi chon don vi tren cay
    if (this.organizationIdSelected) {
      params['organizationId'] = this.organizationIdSelected;
    } else {
      params['organizationId'] = this.f.organizationId.value;
    }
    params['organizationRootId'] = this.f.organizationId.value;
    // end
    this.payrollCalculateService.processExport(params).subscribe(res => {
      saveAs(res, 'payroll_calculate_report.xls');
    });
  }

  /**
   * thaopv
   *  add new action
   */
  public prepareSaveOrUpdate(payrollGeneralId?) {
    let params = Object.assign({}, this.formSearch.value);
    if (this.orgId > 0) {
      params['orgId'] = this.orgId;
    }
    params['organizationRootId'] = this.f.organizationId.value;
    params['organizationId'] = this.f.organizationId.value;
    params['salaryTypeId'] = this.f.salaryTypeId.value;
    params['periodId'] = this.f.periodId.value;
    if (payrollGeneralId > 0) {
      const modalRef = this.modalService.open(AddNewComponent, DEFAULT_MODAL_OPTIONS);
      this.payrollCalculateService.findByPayrollGeneralId(payrollGeneralId)
        .subscribe(res => {
          modalRef.componentInstance.setData(this.propertyConfigs, res.data, params);
          modalRef.result.then((result) => {
            if (!result) {
              return;
            }
            if (this.payrollCalculateService.requestIsSuccess(result)) {
              this.actionSearchCalculate(null);
            }
          });
        }, error => {
          console.log('oops', error);
        });
    } else {
      if (!CommonUtils.isValidForm(this.formSearch)) {
        this.app.errorMessage('payroll', 'notChoosePeriodSalary');
      } else {
        const modalRef = this.modalService.open(AddNewComponent, DEFAULT_MODAL_OPTIONS);
        modalRef.componentInstance.setData(this.propertyConfigs, params);
        modalRef.result.then((result) => {
          if (!result) {
            return;
          }
          if (this.payrollCalculateService.requestIsSuccess(result)) {
            this.actionSearchCalculate(null);
          }
        });
      }
    }
  }

  private initContextMenu() {
    if (this.hasPermission('action.update')) {
      this.menuItems = [
        {
          label: this.translation.translate('MenuContext.edit'),
          icon: 'pi pi-fw pi-pencil',
          command: (event) => this.prepareSaveOrUpdate(this.selectedObject.payrollGeneralId)
        }
      ];
    }

  }

  private initPayrollReportType() {
    if (this.f.salaryTypeId.value && this.f.organizationId.value) {
      let arrSalaryType = this.f.salaryTypeId.value;
      this.payrollReportTypeService.getListActiveReportType(arrSalaryType[0], this.f.organizationId.value).subscribe(
        res => {
          this.listPayrollReportType = res.data;
        }
      );
    } else {
      this.listPayrollReportType = [];
    }
  }

  private a() {
    const modalRef = this.modalService.open(AddNewComponent, DEFAULT_MODAL_OPTIONS);
  }

  public prepareImport(event?) {
    if (!CommonUtils.isValidForm(this.formSearch)) {
      return;
    }
    let arrSalaryType = this.f.salaryTypeId.value;
    let params = Object.assign({}, this.formSearch.value);
    // add
    if (!event) {
      this.organizationIdSelected = null;
      this.selectedNode = null;
    }
    // Lay gia tri khi chon don vi tren cay
    if (this.organizationIdSelected) {
      params['organizationId'] = this.organizationIdSelected;
    } else {
      params['organizationId'] = this.f.organizationId.value;
    }
    params['organizationRootId'] = this.f.organizationId.value;
    params['salaryTypeId'] = arrSalaryType[0];
    const modalRef = this.modalService.open(ImportPayrollGeneralComponent, DEFAULT_MODAL_OPTIONS);
    modalRef.componentInstance.setData(params);

    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
      if (this.payrollCalculateService.requestIsSuccess(result)) {
        if (this.selectedNode) {
          this.nodeSelect(null, this.selectedNode);
        } else {
          this.actionSearchCalculate(null);
        }
      }
    });
  }

  /**
   * Xu ly xem chi tiet phieu luong
   */
  public processViewPayslip(item) {
    const modalRef = this.modalService.open(PayslipDetailComponent, DEFAULT_MODAL_OPTIONS);
    const formData = {
      payrollGeneralId: item.payrollGeneralId,
      access_token: HrStorage.getUserToken().access_token
    };
    modalRef.componentInstance.setData(formData);
    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
    });
  }

  public processDownloadPayslip(item) {
    const formData = {
      payrollGeneralId: item.payrollGeneralId,
      access_token: HrStorage.getUserToken().access_token
    };
    this.payslipService.downloadDocx(formData).subscribe(res => {
      saveAs(res, `phieu_luong_${item.payrollGeneralId}.docx`);
    });
  }

  /**
   * xử lý gửi payslip
  */
  public processSendPayslip(item) {
    this.app.confirmMessage('payroll.calculate.sendPayslipConfirm', () => {// on accepted
      const sendPaylist = { payrollGeneralId: item.payrollGeneralId, };
      this.payslipService.processSendPayslip(sendPaylist).subscribe(res => { });
    }, null);
  }

  /**
   * xử lý gửi payslip theo don vi
  */
  public processSendPayslipAll() {
    this.app.confirmMessage('payroll.calculate.sendPayslipConfirm', () => {// on accepted
      let selected;
      if (this.organizationIdSelected) {
        selected = this.organizationIdSelected;
      } else {
        selected = this.f.organizationId.value;
      }
      const sendPaylist = {
        payrollGeneralId: this.f.payrollGeneralId.value,
        organizationRootId: this.f.organizationId.value,
        salaryTypeId: this.f.salaryTypeId.value[0],
        periodId: this.f.periodId.value,
        organizationId: selected,
        year: this.f.year.value
      };
      this.payslipService.processSendPayslip(sendPaylist).subscribe(res => { });
    }, null);
  }

  // Xuất báo cáo theo loại lương
  public processExportReport(item) {
    if (!CommonUtils.isValidForm(this.formSearch)) {
      return;
    }
    let params = Object.assign({}, this.formSearch.value);
    // Lay gia tri khi chon don vi tren cay
    if (this.organizationIdSelected) {
      params['organizationId'] = this.organizationIdSelected;
    } else {
      params['organizationId'] = this.f.organizationId.value;
    }
    params['organizationRootId'] = this.f.organizationId.value;
    // end

    params['payrollReportTypeId'] = item.payrollReportTypeId;
    this.payrollCalculateService.reportBySalatyType(params).subscribe(res => {
      saveAs(res, 'bao_cao_' + new Date().getTime() + '.xls');
    });
  }

  prepareTaxSync() {
    if (!CommonUtils.isValidForm(this.formSearch)) {
      return;
    }
    let data = Object.assign({}, this.formSearch.value);
    data.salaryTypeId = data.salaryTypeId[0];
    this.payrollCalculateService.prepareTaxSync(data).subscribe(res => {
      if (res && res.code === "error") {
        if (res.type === "ERROR.payroll.calculate.isLock") {
          this.app.errorMessage('payroll.calculate', 'isLock');
          return;
        }
        if (res.type === "ERROR.payroll.calculate.paid") {
          this.app.errorMessage('payroll.calculate', 'paid');
          return;
        }
      } else {
        const modalRef = this.modalService.open(TaxSyncComponent, DEFAULT_MODAL_OPTIONS);
        modalRef.componentInstance.setFormValue(this.propertyConfigs, data);
      }
    });
  }

  preparePaymentSync() {
    if (!CommonUtils.isValidForm(this.formSearch)) {
      return;
    }
    const modalRef = this.modalService.open(PaymentSyncComponent, DEFAULT_MODAL_OPTIONS);
    let dataRef = Object.assign({}, this.formSearch.value);
    dataRef["listPeriod"] = this.periodList;
    dataRef["salaryTypeId"] = dataRef.salaryTypeId[0];
    modalRef.componentInstance.setFormValue(this.propertyConfigs, dataRef);
    modalRef.result.then((result) => {
      if (result) {
        if (result.type === "ERROR.payroll.calculate.isLock") {
          this.app.errorMessage('payroll.calculate', 'isLock');
        }
        if (result.type === "ERROR.payroll.calculate.paid") {
          this.app.errorMessage('payroll.calculate', 'paid');
        }
        else {
          this.app.successMessage('sync.payment', 'message.success');
        }
      }
    });
  }

  processDelete(item) {
    if (item && !item.payrollGeneralId) {
      return;
    }
    this.app.confirmDelete(null, () => {// on accepted
      this.payrollCalculateService.deletePayrollGeneral(item.payrollGeneralId).subscribe(res => {
        if (res) {
          this.actionSearchCalculate(null);
        }
      })
    }, null);
  }

  processExportProgress() {
    if (!CommonUtils.isValidForm(this.formSearch)) {
      return;
    }
    let data = Object.assign({}, this.formSearch.value);
    data.salaryTypeId = data.salaryTypeId[0];
    this.payrollCalculateService.actionExportProcess(data).subscribe(res => {
      if (res && res.size > 0) {
        saveAs(res, "Export_Process_Calculate");
      } else {
        this.app.errorMessage('payroll', 'exportError');
      }
    });
  }

  checkSingleSelect() {
    let dataCheck = this.formSearch.value;
    if (!dataCheck.organizationId || !dataCheck.salaryTypeId || dataCheck.salaryTypeId == null) {
      this.checkSingle = false;
      return;
    }
    let orgIds = dataCheck.organizationId;
    let arrOrgIds = orgIds.split(',');
    if (arrOrgIds.length > 1 || dataCheck.salaryTypeId.length > 1) {
      this.checkSingle = false;
    }
    else {
      this.checkSingle = true;
    }
  }

  processDeleteDataImport() {
    let params = Object.assign({}, this.formSearch.value);
    params["salaryTypeId"] = params.salaryTypeId[0];
    this.app.confirmDelete(null, () => {// on accepted
      this.payrollCalculateService.deleteDataImport(params).subscribe(res => {
        if (res) {
          this.actionSearchCalculate();
        }
      })
    }, () => {

    });
  }
}
