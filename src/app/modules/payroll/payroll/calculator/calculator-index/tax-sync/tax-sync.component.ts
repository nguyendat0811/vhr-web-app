import { PayrollCalculateService } from './../../../../../../core/services/hr-payroll/payroll-calculate.service';
import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { RESOURCE, ACTION_FORM } from '@app/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from '@app/app.component';
import * as moment from 'moment';

@Component({
  selector: 'tax-sync',
  templateUrl: './tax-sync.component.html',
})
export class TaxSyncComponent extends BaseComponent implements OnInit {
  @Input() public periodId;
  listIncomeItem = [];
  listDeclarePeriod = [];
  objSyncTax = {};
  formSync: FormGroup;
  formConfig = {
    incomeId: [null],
    declarationPeriodId: [null],
    organizationId: [null],
    salaryTypeId: [null],
    year: [parseInt(moment(new Date()).format('YYYY'))],
    periodId: [null],
    employeeCode: [''],
    employeeName: [''],
    payrollGeneralId: [''],
    declarePeriodName: ['']
  };
  constructor(
    public actr: ActivatedRoute,
    public activeModal: NgbActiveModal,
    private payrollCalculateService: PayrollCalculateService,
    private app: AppComponent,
  ) {
    super(actr, RESOURCE.PAYROLL_GENERAL, ACTION_FORM.PAYROLL_SYNC_TAX);
    this.formSync = this.buildForm({}, this.formConfig, ACTION_FORM.PAYROLL_SYNC_TAX);
  }

  ngOnInit() {
    let that = this;
    this.formSync.get('declarationPeriodId').valueChanges.subscribe(val => {
      let item = that.listDeclarePeriod.find(x=>x.id == val);
      if(item){
        this.formSync.controls.declarePeriodName.setValue(item.name);
      }
    });
  }

  prepareForm(periodId){
    if(periodId){
      this.payrollCalculateService.getListIncomeItem(periodId).subscribe(
        res => {
          this.listIncomeItem = res.data;
        }
      );
      this.payrollCalculateService.getListDeclarePeriod(periodId).subscribe(
        res => {
          this.listDeclarePeriod = res.data;
        }
      );
    }
  }

  get f() {
    return this.formSync.controls;
  }

  processTaxSync() {
    this.app.confirmMessage('payroll.calculate.confirmCalculateTax', () => {// on accepted
      this.payrollCalculateService.processSyncTax(this.formSync.value).subscribe(res => {
        if (this.payrollCalculateService.requestIsSuccess(res)) {
          this.activeModal.close(res);
        } else {
          console.log("text1");
        }
      });
    }, null);
  }
    /**
   * setFormValue
   * param: Array propertyConfigs
   * param data
   */
  public setFormValue(propertyConfigs: any, data?: any) {
    this.propertyConfigs = propertyConfigs;
    if(data) {
       this.formSync = this.buildForm(data, this.formConfig, ACTION_FORM.PAYROLL_GENERAL_SEARCH);
       this.prepareForm(data.periodId);
    }
  }
}
