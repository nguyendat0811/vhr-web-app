import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { RESOURCE, ACTION_FORM, PayrollCalculateService } from '@app/core';
import { FormGroup } from '@angular/forms';
import { FileControl } from '@app/core/models/file.control';
import { CommonUtils, ValidationService } from '@app/shared/services';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from '@app/app.component';

@Component({
  selector: 'import-payroll-general',
  templateUrl: './import-payroll-general.component.html',
})
export class ImportPayrollGeneralComponent extends BaseComponent implements OnInit {
  formImportConfig = {
    organizationId : [''],
    salaryTypeId: [''],
    periodId: [''],
    organizationRootId: [''],
    isUpdate: [''],
  };
  public formImport: FormGroup;
  dataError: any;
  showButtonImport = false;
  isOk = false;
  constructor(public actr: ActivatedRoute
            , public activeModal: NgbActiveModal
            , private app: AppComponent
            , public payrollCalculateService: PayrollCalculateService) {
    super(actr, RESOURCE.DATA_INPUT_PROCESS, ACTION_FORM.IMPORT);
    this.formImport = this.buildForm({}, this.formImportConfig, ACTION_FORM.IMPORT);
    this.formImport.addControl('fileImport', new FileControl(null, ValidationService.required));
  }

  ngOnInit() {
  }

  get f() {
    return this.formImport.controls;
  }

  setData(data: any) {
    this.formImport = this.buildForm(data, this.formImportConfig, ACTION_FORM.IMPORT);
    this.formImport.addControl('fileImport', new FileControl(null, ValidationService.required));
  }

  actionDownloadTemplate() {
    const objdata = {
      organizationId: parseInt(this.f.organizationId.value),
      salaryTypeId: parseInt(this.f.salaryTypeId.value),
      periodId: parseInt(this.f.periodId.value),
      organizationRootId: parseInt(this.f.organizationRootId.value),
    };
    this.payrollCalculateService.downloadTemplateImport(objdata).subscribe(res => {
      saveAs(res, 'payroll_general_import.xls');
    });
  }

  processValidateImport() {
    this.formImport.controls['fileImport'].updateValueAndValidity();
    if (!CommonUtils.isValidForm(this.formImport)) {
      return;
    }
    this.payrollCalculateService.processValidateImport(this.formImport.value).subscribe(res => {
      if (!this.payrollCalculateService.requestIsSuccess(res)) {
        this.dataError = res.data;
        if (res.code === 'import.dataDuplicateError') {
          this.showButtonImport = true;
          this.isOk = true;
        } else {
          this.showButtonImport = false;
          this.isOk = false;
        }
      } else {
        this.dataError = null;
        this.showButtonImport = true;
        this.isOk = false;
      }
    });
  }

  processImport() {
    this.formImport.controls['fileImport'].updateValueAndValidity();
    if (!CommonUtils.isValidForm(this.formImport)) {
      return;
    }
    let stringMessage = null;
    if (this.isOk === true) {
      stringMessage = 'payrolls.caculate.comfirmUpdateDataDuplicate';
      this.formImport.value.isUpdate = 1;
    } else {
      stringMessage = null;
      this.formImport.value.isUpdate = null;
    }
    this.app.confirmMessage(stringMessage, () => {// on accepted
      this.payrollCalculateService.processImport(this.formImport.value).subscribe(res => {
        if (!this.payrollCalculateService.requestIsSuccess(res)) {
          this.dataError = res.data;
          this.showButtonImport = false;
        } else {
          this.dataError = null;
          this.activeModal.close(res);
        }
      });
    }, null);
  }
}
