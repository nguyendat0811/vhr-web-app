import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ACTION_FORM, RESOURCE, SalaryTypeService, SalaryTypeOrganizationMappingService, PayrollRecalculateService} from '@app/core';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ValidationService, CommonUtils } from '@app/shared/services';
import { FormatCurrencyPipe } from '@app/shared/pipes/format-currency.pipe';
import { PAYROLL_CHANGE_TYPE } from '@app/core/app-config';
import { AppComponent } from '@app/app.component';

@Component({
  selector: 'recalculate-index',
  providers: [FormatCurrencyPipe],
  templateUrl: './recalculate-index.component.html'
})
export class RecalculateIndexComponent extends BaseComponent implements OnInit {
  dataChange: any = [];
  salaryTypeList: any = {};
  dataTabs: any = {};
  lstTabDetails: any = [];
  listSalaryType: any = [];
  isShowDetail: boolean = false;
  titleEmployee: string;
  formSearch: FormGroup;
  selectedAll: boolean = true;
  salaryOld: number = 0;
  salaryNew: number = 0;
  salaryChangeOfPeriod: number = 0;
  arrears: any = PAYROLL_CHANGE_TYPE.ARREARS;
  formConfig = {
    organizationId: [null, [ValidationService.required]],
    dateCheck: [null, [ValidationService.required]]
  };

  constructor(public actr: ActivatedRoute,
    public salaryTypeService: SalaryTypeService
    , private payrollRecalculateService: PayrollRecalculateService
    , private salaryTypeOrganizationMappingService: SalaryTypeOrganizationMappingService
    , private app: AppComponent) {
    super(actr, RESOURCE.PAYROLL_GENERAL, ACTION_FORM.PAYROLL_GENERAL_SEARCH);
    if (!this.hasPermission('action.view')) {
      return;
    }
    this.formSearch = this.buildForm({}, this.formConfig, ACTION_FORM.PAYROLL_GENERAL_SEARCH, []);

      // xu ly lang nghe thay doi don vi
    this.f.organizationId.valueChanges.subscribe(data => {
      if (data) {
        this.initSalaryType();
      }
    });
  }

  ngOnInit() {
  }

  get f() {
    return this.formSearch.controls;
  }

  initSalaryType() {
    if (this.f.organizationId.value) {
      this.salaryTypeOrganizationMappingService.findByOrgId(this.f.organizationId.value).subscribe(res => {
        this.salaryTypeList = res.data;
      });
    } else {
      this.salaryTypeList = null;
    }
  }

  checkChange(){
    if (!CommonUtils.isValidForm(this.formSearch)) {
      return;
    }
    this.refreshValue();
    this.payrollRecalculateService.actionRunProcess(this.formSearch.value).subscribe(res => {
      if(res && res.type === "SUCCESS"){
        this.dataChange = res.data;
        this.dataChange.forEach(function(item){
          item.amountChange = item.salaryNew - item.salaryOld;
          item.amount = item.amountChange;
          item.isSelected = true;
          if(item.amountChange < 0){
            item.typeOfChange = PAYROLL_CHANGE_TYPE.ARREARS; //Loại thay đổi tiền: truy thu: 1, truy lĩnh: 2
          }else{
            item.typeOfChange = PAYROLL_CHANGE_TYPE.RETROSPECTIVE;
          }
        });
        
      }
    });
  }

  processViewDetail(item){
    if(item){
      this.titleEmployee = item.employeeName;
      this.listSalaryType = [];
      var that = this;
      this.isShowDetail = false;
      this.payrollRecalculateService.getDetailChange(item).subscribe(res => {
        if(res && res.type === "SUCCESS"){
          this.isShowDetail = true;
          that.lstTabDetails = Object.keys(res.data);
          that.dataTabs = res.data;
          this.salaryOld = that.dataTabs[Object.keys(that.dataTabs)[0]].salaryBeforeRecalculate;
          this.salaryNew = that.dataTabs[Object.keys(that.dataTabs)[0]].salaryRecalculate;
          this.salaryChangeOfPeriod = that.dataTabs[Object.keys(that.dataTabs)[0]].totalChange;
          that.dataTabs[Object.keys(that.dataTabs)[0]].salaryChange.forEach(function(tmp){
            let index = -1;
            if(that.listSalaryType.length > 0){
              index = that.listSalaryType.findIndex(x=>x.salaryTypeId ===tmp.salaryTypeId);
            }
            if(index < 0){
              tmp.valueCompare.sort((a, b) => (a.stt > b.stt) ? 1 : -1);
              that.listSalaryType.push({
                salaryTypeId: tmp.salaryTypeId,
                salaryTypeName: tmp.salaryType,
                listData: tmp.valueCompare,
                salaryBeforeRecalculate: tmp.salaryBeforeRecalculate,
                salaryRecalculate: tmp.salaryRecalculate,
                totalChange: tmp.totalChange
              });
              that.listSalaryType.sort((a, b) => (a.salaryTypeId < b.salaryTypeId) ? 1 : -1);
            }
          });
        }
      });
    }
  }

  refreshValue(){
    this.isShowDetail = false;
    this.listSalaryType = [];
    this.lstTabDetails =[];
    this.titleEmployee = '';
  }

  onChangeSalaryType(e){
    var index = e.index;
  }

  onSelectedAll(){
    if(this.selectedAll){
      this.dataChange.forEach(function(item){
        item.isSelected = true;
      });
    }
    else{
      this.dataChange.forEach(function(item){
        item.isSelected = false;
      });
    }
  }

  runRecalculate(){
    let dataPost = [];
    this.dataChange.forEach(function(item){
      if(item.isSelected){
        dataPost.push({
          employeeId: item.employeeId,
          employeeCode: item.employeeCode,
          employeeName: item.employeeName,
          organizationId: item.orgId,
          amount: item.amount,
          amountChange: item.amountChange,
          typeOfChange: item.typeOfChange,
          salaryNew: item.salaryNew,
          salaryOld: item.salaryOld
        });
      }
    });
    if(dataPost.length === 0){
      this.app.errorMessage('payroll.recalculate', 'message.noDataSelected');
      return;
    }
    this.payrollRecalculateService.runRecalculate(dataPost).subscribe(res => {
      if(res && res.type === "SUCCESS"){
        this.app.successMessage('payroll.recalculate', 'message.success');
      }else{
        this.app.errorMessage('payroll.recalculate', 'message.error');
      }
    });
  }

  handleChangeTab(e){
    var index = e.index;
    this.listSalaryType = [];
    var that = this;
    this.salaryOld = that.dataTabs[Object.keys(that.dataTabs)[index]].salaryBeforeRecalculate;
    this.salaryNew = that.dataTabs[Object.keys(that.dataTabs)[index]].salaryRecalculate;
    this.salaryChangeOfPeriod = that.dataTabs[Object.keys(that.dataTabs)[index]].totalChange;
    this.dataTabs[Object.keys(this.dataTabs)[index]].salaryChange.forEach(function(item){
      let idx = -1;
      if(that.listSalaryType.length > 0){
        idx = that.listSalaryType.findIndex(x=>x.salaryTypeId ===item.salaryTypeId);
      }
      if(idx < 0){
        item.valueCompare.sort((a, b) => (a.stt > b.stt) ? 1 : -1);
        that.listSalaryType.push({
          salaryTypeId: item.salaryTypeId,
          salaryTypeName: item.salaryType,
          listData: item.valueCompare
        });
        that.listSalaryType.sort((a, b) => (a.salaryTypeId < b.salaryTypeId) ? 1 : -1);
      }
    });
  }
}
