import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { RESOURCE, ACTION_FORM, PayrollTableConfigService, PayrollProgressService, PayrollCalculateService } from '@app/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { ValidationService } from '@app/shared/services';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { AppComponent } from '@app/app.component';
import { PeriodService } from '@app/core/services/hr-payroll/period.serviece';

@Component({
  selector: 'payroll-master',
  templateUrl: './payroll-master.component.html'
})
export class PayrollMasterFormComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  formConfigNew = {};
  formConfig = {
    paymentMoney: ['', [ValidationService.required]],
    paymentDate: ['', [ValidationService.required]],
  };
  targetData: any;
  listConfig: any;
  listMasterConfig: any;
  clueCode: string;
  clueName: string;
  salaryPeriod: string;
  @ViewChild("paymentMoney") paymentMoneyElem: ElementRef;
  constructor(public activeModal: NgbActiveModal
            , public actr: ActivatedRoute
            , public payrollProgressService: PayrollProgressService
            , public payrollTableConfigService: PayrollTableConfigService
            , private periodService: PeriodService
            , private payrollCalculateService: PayrollCalculateService
            , private app: AppComponent) {
    super(actr, RESOURCE.SYSTEM_PARAMETER, ACTION_FORM.INSERT);
    this.buildFormAdd({},  ACTION_FORM.INSERT, this.formConfig);
  }

  ngOnInit() {
  }

  get f () {
    return this.formSave.controls;
  }

  /**
   * buildFormAdd
   * @ param data
   * @ param actionForm
   * @ param form
   */
  private buildFormAdd(data?: any, actionForm?: ACTION_FORM, form?) {
    this.formSave = this.buildForm(data, form, actionForm, []);
  }

  /**
   * set dữ liệu cho form
   */
  setData(propertyConfigs: any, data, dataForm?) {
    this.targetData = data;
    if(data.paymentDate){
      this.formSave.controls.paymentDate.setValue(new Date(data.paymentDate));
    }else{
      this.formSave.controls.paymentDate.setValue(new Date());
    }
    
    this.clueCode = data.organizationCode;
    this.clueName = data.organizationName;
    this.payrollCalculateService.getTotalSalaryByOrg(data.organizationId, data.salaryTypeId, data.periodId).subscribe(res=>{
      if(res && res.data){
        this.formSave.controls.paymentMoney.setValue(res.data);
      }
    });
    this.periodService.findOne(data.periodId).subscribe(res=>{
      if(res && res.data){
        this.salaryPeriod = res.data.periodName;
      }
    });
    this.propertyConfigs = propertyConfigs;
    this.paymentMoneyElem.nativeElement.focus();
  }

  checkAttributeType(type) {
    let arrayValidate;
    if (type === 5) {
      arrayValidate = ',' + [ValidationService.positiveNumber];
    }
    return arrayValidate;
  }

  /**
   *  THực hiện lưu dữ liệu
   */
  processSaveOrUpdate() {
    this.targetData.paymentMoney = this.formSave.value.paymentMoney;
    this.targetData.paymentDate = this.formSave.value.paymentDate;
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    }
    this.app.confirmMessage(null, () => {// on accepted
      this.payrollProgressService.saveOrUpdate(this.targetData)
        .subscribe(res => {
          if (this.payrollProgressService.saveOrUpdate(res)) {
            this.activeModal.close(res);
          }
        });
      }, () => {// on rejected
    });
  }
}
