import { FormGroup } from '@angular/forms';
import { ValidationService, CommonUtils } from '@app/shared/services';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { Component, OnInit } from '@angular/core';
import { RESOURCE, ACTION_FORM, PayrollCalculateService, PayslipService } from '@app/core';
import * as moment from 'moment';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from '@app/app.component';
import { SalaryTypeOrganizationMappingService } from '@app/core';
import { APP_CONSTANTS, SMALL_MODAL_OPTIONS } from '@app/core/app-config';
import { PeriodService } from '@app/core/services/hr-payroll/period.serviece';
import { PayrollProgressService } from '@app/core/services/hr-payroll/payroll-progress.service';
import { PayrollMasterFormComponent } from '../payroll-master/payroll-master.component';

@Component({
  selector: 'progress-index',
  templateUrl: './progress-index.component.html',
  styleUrls: ['./progress-index.component.css'],
})
export class ProgressIndexComponent extends BaseComponent implements OnInit {
  formSearch: FormGroup;
  payrollMasterId: number;
  salaryTypeList: any = {};
  yearList: any;
  periodList: any;
  statusList: any = {};
  periodBeanForm: FormGroup;
  percentCalculated:Number = 0;
  stylePercentCalculated: any = {'width': '0%'};
  percentPaid:Number = 0;
  stylePercentPaid: any = {'width': '0%'};
  periodBean = {
    organizationId: [null],
    year: [null],
  };

  formConfig = {
    organizationId: [null, [ValidationService.required]],
    salaryTypeId: [null, [ValidationService.required]],
    year: [parseInt(moment(new Date()).format('YYYY'))],
    periodId: [null, [ValidationService.required]],
    status: ['']
  };

  constructor(public actr: ActivatedRoute
    , private modalService: NgbModal
    , private salaryTypeOrganizationMappingService: SalaryTypeOrganizationMappingService
    , public periodService: PeriodService
    , public payrollProgressService: PayrollProgressService
    , private payrollCalculateService: PayrollCalculateService
  ) {
    super(actr, RESOURCE.PAYROLL_MASTER, ACTION_FORM.SEARCH);
    this.setMainService(payrollProgressService);
    this.formSearch = this.buildForm({}, this.formConfig, ACTION_FORM.SEARCH);
    this.yearList = this.getYearList();
    this.periodBeanForm = this.buildForm({}, this.periodBean, ACTION_FORM.PAYROLL_GENERAL_SEARCH);
    this.statusList = APP_CONSTANTS.PAYROLL_MASTER_STATUS;

    //xu ly khi don vi thay doi
    this.f.organizationId.valueChanges.subscribe(data => {
      if (data) {
        this.f.salaryTypeId.setValue(null);
        this.initSalaryType(data);
        this.getPeriod(data, this.f.year.value);
        //this.processSearch();
        this.getPercent();
      }
    });
    //xu ly khi nam thay doi
    this.f.year.valueChanges.subscribe(data => {
      if (this.f.organizationId.value) {
        this.getPeriod(this.f.organizationId.value, data);
      }
    });
    this.payrollProgressService.processFilterData(this.formSearch).subscribe(res=>{
      this.resultList = res;
    });
  }

  ngOnInit() {}

  get f() {
    return this.formSearch.controls;
  }

  filterData(){
    if (!CommonUtils.isValidForm(this.formSearch)) {
      return;
    }
    this.payrollProgressService.processFilterData(this.formSearch).subscribe(res=>{
      this.resultList = res;
    });
    this.getPercent();
  }

  processSearchData(event?){
    if (!CommonUtils.isValidForm(this.formSearch)) {
      return;
    }
    this.payrollProgressService.processFilterData(this.formSearch, event).subscribe(res=>{
      this.resultList = res;
    });
  }

  //lay danh sach nam theo nam hien tai
  private getYearList() {
    this.yearList = [];
    const currentYear = new Date().getFullYear();
    for (let i = (currentYear - 50); i <= (currentYear + 50); i++) {
      const obj = {
        year: i
      };
      this.yearList.push(obj);
    }
    return this.yearList;
  }

  public getPercent(){
    let params = this.formSearch ? this.formSearch.value : null;
    delete params.status;
    this.payrollProgressService.getPercent(params).subscribe(res=>{
      if(res){
        this.percentCalculated = res.percentCalculated * 100;
        this.stylePercentCalculated = {'width': this.percentCalculated+'%'};
        this.percentPaid = res.percentPaid * 100;
        this.stylePercentPaid = {'width': this.percentPaid+'%'};
      }
    })
  }

  private getPeriod(organizationId?, year?): void {
    this.periodBeanForm.controls['organizationId'].setValue(organizationId);
    this.periodBeanForm.controls['year'].setValue(year);
    this.periodService.getDataByOrgIdAndYear(this.periodBeanForm.value).subscribe(res => {
      res.data.forEach(x => {
        x.periodName = x.periodName + '( ' +
          moment(new Date(x.startDate)).format(CommonUtils.getDateFormat().toUpperCase()) + ' - ' +
          moment(new Date(x.endDate)).format(CommonUtils.getDateFormat().toUpperCase()) + ' )';
      });
      this.periodList = res.data;
    });
  }

  onLock(isLock, item) {
    if (item) {
      item.isLock = isLock;
      this.payrollProgressService.saveOrUpdate(item).subscribe(res => {
        console.log('res', res);
      });
    }
  }

  openPayrollMasterForm(item) {
    const modalRef = this.modalService.open(PayrollMasterFormComponent, SMALL_MODAL_OPTIONS);
    modalRef.componentInstance.setData(this.propertyConfigs, item);
    modalRef.result.then((result) => {
      if (!result) {
        return;
      }else{
        this.processSearch();
        this.getPercent();
      }
      // if (this.payrollCalculateService.requestIsSuccess(result)) {
      //   this.actionSearchCalculate(null);
      // }
    });
  }

  /*
  * onChangeOrganization
  * Lay danh sach loai luong theo cau hinh mapping don vi cha
  */
  initSalaryType(id?) {
    if (id) {
      this.salaryTypeOrganizationMappingService.findByOrgId(id).subscribe(res => {
        this.salaryTypeList = res.data;
      });
    } else {
      this.salaryTypeList = null;
    }
  }

  /**
    * xử lý gửi payslip theo don vi
   */
  public processSendPayslipAll(item) {
    // this.app.confirmMessage('payroll.calculate.sendPayslipConfirm', () => {// on accepted
    //   const sendPaylist = {
    //     payrollGeneralId: 0,
    //     organizationRootId: item.organizationId,
    //     salaryTypeId: item.salaryTypeId,
    //     periodId: item.periodId,
    //     organizationId: item.organizationId
    //   };
    //   this.payslipService.processSendPayslip(sendPaylist).subscribe(res => { });
    // }, null);
  }

  processExport(item) {
    item['organizationRootId'] = item.organizationId;
    this.payrollCalculateService.processExport(item).subscribe(res => {
      saveAs(res, 'payroll_calculate_report.xls');
    });
  }

  checkPaid(item){
    if(!item.paymentDate){
      return false;
    }
    let now = new Date();
    let paymentDate = new Date(item.paymentDate);
    if(paymentDate > now){
      return false;
    }else{
      return true;
    }
  }
}
