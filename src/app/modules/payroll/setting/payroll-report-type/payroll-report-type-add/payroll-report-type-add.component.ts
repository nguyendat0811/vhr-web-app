import { FileControl } from '@app/core/models/file.control';
import { APP_CONSTANTS } from './../../../../../core/app-config';
import { AppComponent } from './../../../../../app.component';
import { ValidationService } from './../../../../../shared/services/validation.service';
import { CommonUtils } from './../../../../../shared/services/common-utils.service';
import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SalaryTypeService, SysCatService, RESOURCE, ACTION_FORM } from '@app/core';
import { PayrollReportTypeService } from '@app/core/services/hr-payroll/payroll-report-type.service';

@Component({
  selector: 'payroll-report-type-add',
  templateUrl: './payroll-report-type-add.component.html'
})
export class PayrollReportTypeAddComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  salaryTypeList: any = {};
  empTypeList: any = {};
  reportTypeList: any = {};
  resultList: any = {};
  public commonUtils = CommonUtils;
  formConfig = {
    payrollReportTypeId: [''],
    salaryTypeId: [null, [ValidationService.maxLength(20), ValidationService.required]],
    empTypeId: [null, [ValidationService.maxLength(20)]],
    reportType: [null, [ValidationService.maxLength(20), ValidationService.required]],
    startDate:[null, [ValidationService.required]],
    endDate: [null],
    organizationId: [null],
  };
  constructor(public actr: ActivatedRoute
    , public activeModal: NgbActiveModal
    , private app: AppComponent
    , private salaryTypeService: SalaryTypeService
    , private sysCatService: SysCatService
    , private payrollReportTypeService: PayrollReportTypeService
  ) {
    super(actr, RESOURCE.PAYROLL_REPORT_TYPE, ACTION_FORM.INSERT);
    this.buildForms({}, ACTION_FORM.INSERT);
    if(!this.hasPermission('action.view')){
      return;
    }
    this.salaryTypeService.getListActive().subscribe(res => {
      this.salaryTypeList = res.data;
    });
    this.empTypeList = APP_CONSTANTS.EMP_TYPE;
    this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.PAYROLL_REPORT_TYPE).subscribe(
      res => {
        this.reportTypeList = res.data;
      }
    );

  }

  get f() {
    return this.formSave.controls;
  }

  ngOnInit() {
  }

  /**
   * processSaveOrUpdate
   */
  processSaveOrUpdate() {
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    } else {
      this.app.confirmMessage(null, () => {
        this.payrollReportTypeService.saveOrUpdateFormFile(this.formSave.value)
        .subscribe(res => {
          if (this.payrollReportTypeService.requestIsSuccess(res)) {
            this.activeModal.close(res);
          }
        });
      }, () => {

      });
    }
  }

  /**
   * buildForm
   */
  private buildForms(data?: any, actionForm?: ACTION_FORM): void {
    this.formSave = this.buildForm(data, this.formConfig, actionForm, [ValidationService.notAffter('startDate', 'endDate', 'payroll.payrollReportType.endDate') ]);
    this.formSave.addControl('file', new FileControl(null, ValidationService.required));
    if (data.fileAttachment) {
      (this.formSave.controls['file'] as FileControl).setFileAttachment(data.fileAttachment.payrollReportTypeFile);
    }
  }

  public setFormValue(propertyConfigs: any, data?: any) {
    this.propertyConfigs = propertyConfigs;
    if (data && data.payrollReportTypeId > 0) {
      this.buildForms(data, ACTION_FORM.UPDATE);
    } else {
      this.buildForms(data, ACTION_FORM.INSERT);
    }
  }

}
