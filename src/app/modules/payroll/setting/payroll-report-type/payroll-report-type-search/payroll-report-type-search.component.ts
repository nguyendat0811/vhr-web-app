import { PayrollReportTypeAddComponent } from './../payroll-report-type-add/payroll-report-type-add.component';
import { AppComponent } from './../../../../../app.component';
import { FileStorageService } from './../../../../../core/services/file-storage.service';
import { BaseComponent } from './../../../../../shared/components/base-component/base-component.component';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RESOURCE, ACTION_FORM, SysCatService, APP_CONSTANTS, SalaryTypeService } from '@app/core';
import { ValidationService } from '@app/shared/services';
import { PayrollReportTypeService } from '@app/core/services/hr-payroll/payroll-report-type.service';

@Component({
  selector: 'payroll-report-type-search',
  templateUrl: './payroll-report-type-search.component.html',
})
export class PayrollReportTypeSearchComponent extends BaseComponent implements OnInit {
  formSearch: FormGroup;
  salaryTypeList: any = {};
  empTypeList: any = {};
  reportTypeList: any = {};
  resultList: any = {};
  formConfig = {
    salaryTypeId: [null, [ValidationService.maxLength(20)]],
    empTypeId: [null, [ValidationService.maxLength(20)]],
    reportType: [null, [ValidationService.maxLength(20)]],
    startDate:[null],
    endDate: [null],
  };
  constructor(public actr: ActivatedRoute
    , private modalService: NgbModal
    , private app: AppComponent
    , public fileStorage: FileStorageService
    , private salaryTypeService: SalaryTypeService
    , private sysCatService: SysCatService
    , private payrollReportTypeService: PayrollReportTypeService
  ) {
    super(actr, RESOURCE.PAYROLL_REPORT_TYPE,  ACTION_FORM.SEARCH);
    if (!this.hasPermission('action.view')) {
      return;
    }
    this.setMainService(payrollReportTypeService);
    this.salaryTypeService.findAll().subscribe(res => {
      this.salaryTypeList = res.data;
    });
    this.empTypeList = APP_CONSTANTS.EMP_TYPE;
    this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.PAYROLL_REPORT_TYPE).subscribe(
      res => {
        this.reportTypeList = res.data;
      }
    );
    this.formSearch = this.buildForm({}, this.formConfig, ACTION_FORM.SEARCH);
  }

  ngOnInit() {
    if (!this.hasPermission('action.view')) {
      return;
    }
    this.processSearch();
  }

  get f() {
    return this.formSearch.controls;
  }

  processDelete(item) {
    if (item && item.payrollReportTypeId > 0) {
      this.app.confirmDelete(null, () => {
        this.payrollReportTypeService.deleteById(item.payrollReportTypeId)
          .subscribe(res => {
            if (this.payrollReportTypeService.requestIsSuccess(res)) {
              this.processSearch(null);
            }
          });
      }, () => {

      });
    }
  }

  prepareSaveOrUpdate(item?: any) {
    if (item && item.payrollReportTypeId > 0) {
      this.payrollReportTypeService.findOne(item.payrollReportTypeId)
        .subscribe(resSCType => {
          this.activeFormModal(this.modalService, PayrollReportTypeAddComponent, resSCType.data);
        });
    } else {
        this.activeFormModal(this.modalService, PayrollReportTypeAddComponent, {});
    }
  }

  public downloadFile(fileData) {
    this.fileStorage.downloadFile(fileData.id).subscribe(res => {
        saveAs(res , fileData.fileName);
    });
  }
}
