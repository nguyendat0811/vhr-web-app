import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@app/shared';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';

import { SettingRoutingModule } from './setting-routing.module';
import { PeriodSearchComponent } from './period/period-search/period-search.component';
import { SalaryTypeSearchComponent } from './salary-type/salary-type-search/salary-type-search.component';
import { PeriodAddComponent } from './period/period-add/period-add.component';
import { SalaryTypeAddComponent } from './salary-type/salary-type-add/salary-type-add.component';
import { PayrollItemSearchComponent } from './payroll-item/payroll-item-search/payroll-item-search.component';
import { PayrollItemAddComponent } from './payroll-item/payroll-item-add/payroll-item-add.component';
import { PayrollReportTypeSearchComponent } from './payroll-report-type/payroll-report-type-search/payroll-report-type-search.component';
import { PayrollReportTypeAddComponent } from './payroll-report-type/payroll-report-type-add/payroll-report-type-add.component';
import { PayrollItemCloneComponent } from './payroll-item/payroll-item-clone/payroll-item-clone.component';
import { PayrollItemDeleteComponent } from './payroll-item/payroll-item-delete/payroll-item-delete.component';
import { PayrollPriceAddComponent } from './payroll-price/payroll-price-add/payroll-price-add.component';
import { PayrollPriceSearchComponent } from './payroll-price/payroll-price-search/payroll-price-search.component';
import { SaleOrderSourcePriceAddComponent } from './sale-order-source-price/sale-order-source-price-add/sale-order-source-price-add.component';
import { SaleOrderSourcePriceSearchComponent } from './sale-order-source-price/sale-order-source-price-search/sale-order-source-price-search.component';
import { PayrollServicePriceAddComponent } from './payroll-service-price/payroll-service-price-add/payroll-service-price-add.component';
import { PayrollServicePriceSearchComponent } from './payroll-service-price/payroll-service-price-search/payroll-service-price-search.component';
import { PayrollOrgMappingSearchComponent } from './payroll-org-mapping/payroll-org-mapping-search/payroll-org-mapping-search.component';
import { PayrollOrgMappingAddComponent } from './payroll-org-mapping/payroll-org-mapping-add/payroll-org-mapping-add.component';
import { XnkNotificatonAddComponent } from './xnk-notification/xnk-notificaton-add/xnk-notificaton-add.component';
import { XnkNotificatonSearchComponent } from './xnk-notification/xnk-notificaton-search/xnk-notificaton-search.component';
import { XnkConfigComponent } from './xnk-config/xnk-config.component';
import { PayrollPriceImportComponent } from './payroll-price-import/payroll-price-import.component';
import { PayrollSpecialPriceAddComponent } from './payroll-special-price/payroll-special-price-add/payroll-special-price-add.component';
import { PayrollSpecialPriceSearchComponent } from './payroll-special-price/payroll-special-price-search/payroll-special-price-search.component';

@NgModule({
  declarations: [PeriodSearchComponent
                , SalaryTypeSearchComponent
                , PeriodAddComponent
                , SalaryTypeAddComponent
                , PayrollItemSearchComponent
                , PayrollItemAddComponent
                , PayrollReportTypeSearchComponent
                , PayrollReportTypeAddComponent
                , PayrollItemCloneComponent
                , PayrollItemDeleteComponent
                , PayrollPriceSearchComponent
                , PayrollPriceAddComponent
                , PayrollSpecialPriceSearchComponent
                , PayrollSpecialPriceAddComponent
                , SaleOrderSourcePriceSearchComponent
                , SaleOrderSourcePriceAddComponent
                , PayrollServicePriceSearchComponent
                , PayrollServicePriceAddComponent
                , PayrollOrgMappingSearchComponent
                , PayrollOrgMappingAddComponent
                , XnkNotificatonSearchComponent
                , XnkNotificatonAddComponent, XnkConfigComponent, PayrollPriceImportComponent
                ],
  imports: [
    CommonModule,
    SettingRoutingModule,
    SharedModule,
    CKEditorModule
  ],
  entryComponents: [
    PeriodAddComponent
    , SalaryTypeAddComponent
    , PayrollItemAddComponent
    , PayrollReportTypeAddComponent
    , PayrollItemCloneComponent
    , PayrollItemDeleteComponent
    , PayrollPriceAddComponent
    , PayrollSpecialPriceAddComponent
    , SaleOrderSourcePriceAddComponent
    , PayrollServicePriceAddComponent
    , PayrollOrgMappingAddComponent
    , XnkNotificatonAddComponent
    , PayrollPriceImportComponent
  ]
})
export class SettingModule { }
