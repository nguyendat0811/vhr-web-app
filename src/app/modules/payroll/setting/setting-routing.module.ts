import { PayrollReportTypeSearchComponent } from './payroll-report-type/payroll-report-type-search/payroll-report-type-search.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PeriodSearchComponent } from './period/period-search/period-search.component';
import { SalaryTypeSearchComponent} from './salary-type/salary-type-search/salary-type-search.component';
import { PayrollItemSearchComponent } from './payroll-item/payroll-item-search/payroll-item-search.component';
import { PayrollPriceSearchComponent } from './payroll-price/payroll-price-search/payroll-price-search.component';
import { SaleOrderSourcePriceSearchComponent } from './sale-order-source-price/sale-order-source-price-search/sale-order-source-price-search.component';
import { PayrollServicePriceSearchComponent } from './payroll-service-price/payroll-service-price-search/payroll-service-price-search.component';
import { PayrollOrgMappingSearchComponent } from './payroll-org-mapping/payroll-org-mapping-search/payroll-org-mapping-search.component';
import { XnkNotificatonSearchComponent } from './xnk-notification/xnk-notificaton-search/xnk-notificaton-search.component';
import { XnkConfigComponent } from './xnk-config/xnk-config.component';
import { PayrollSpecialPriceSearchComponent } from './payroll-special-price/payroll-special-price-search/payroll-special-price-search.component';

const routes: Routes = [
  {
    path: 'period'
    , component: PeriodSearchComponent
  }
  , {
    path: 'salary-type'
    , component: SalaryTypeSearchComponent
  }
  , {
    path: 'payroll-item'
    , component: PayrollItemSearchComponent
  }
  , {
    path: 'payroll-report-type'
    , component: PayrollReportTypeSearchComponent
  }
  , {
    path: 'payroll-price'
    , component: PayrollPriceSearchComponent
  }
  , {
    path: 'special-price'
    , component: PayrollSpecialPriceSearchComponent
  }
  , {
    path: 'service-price'
    , component: PayrollServicePriceSearchComponent
  }
  , {
    path: 'sale-order-source-price'
    , component: SaleOrderSourcePriceSearchComponent
  }
  , {
    path: 'org-mapping'
    , component: PayrollOrgMappingSearchComponent
  }
  , {
    path: 'notification'
    , component: XnkNotificatonSearchComponent
  }
  , {
    path: 'xnk-config'
    , component: XnkConfigComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingRoutingModule { }
