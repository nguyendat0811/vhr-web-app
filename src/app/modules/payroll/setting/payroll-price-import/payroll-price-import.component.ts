import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute, Router } from '@angular/router';
import { RESOURCE, ACTION_FORM } from '@app/core';
import * as moment from 'moment';
import { FormGroup } from '@angular/forms';
import { AppComponent } from '@app/app.component';
import { CommonUtils, ValidationService } from '@app/shared/services';
import { FileControl } from '@app/core/models/file.control';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { PayrollPriceService } from '@app/core/services/hr-payroll/payroll-price.service';

@Component({
    selector: 'payroll-price-import',
    templateUrl: './payroll-price-import.component.html',
    styleUrls: ['./payroll-price-import.component.css']
})
export class PayrollPriceImportComponent extends BaseComponent implements OnInit  {

    public moment = moment;
    public formImport: FormGroup;
    public service: any;
    dataError: any;
    showButtonImport = false;
    private formImportConfig = {
        // organizationId: [null, [ValidationService.required]],
        // dataTypeId: [null, [ValidationService.required]],
        // year: [parseInt(moment(new Date()).format('YYYY'))],
        // periodId: [null, [ValidationService.required]]
    };
    constructor(public actr: ActivatedRoute
        , private router: Router
        , private app: AppComponent
        , public activeModal: NgbActiveModal) {
        super(actr, RESOURCE.DATA_INPUT_PROCESS, ACTION_FORM.IMPORT);
        this.formImport = this.buildForm({}, this.formImportConfig, ACTION_FORM.IMPORT);
        this.formImport.addControl('fileImport', new FileControl(null, ValidationService.required));
    }

    ngOnInit() {
    }

    get f() {
        return this.formImport.controls;
    }
    processValidateImport() {
        this.formImport.controls['fileImport'].updateValueAndValidity();
        if (!CommonUtils.isValidForm(this.formImport)) {
            return;
        }
        this.service.processValidateImport(this.formImport.value).subscribe(res => {
            if (!this.service.requestIsSuccess(res)) {
                this.dataError = res.data;
                this.showButtonImport = false;
            } else {
                this.dataError = null;
                this.showButtonImport = true;
            }
        });
    }

    processImport() {
        this.formImport.controls['fileImport'].updateValueAndValidity();
        if (!CommonUtils.isValidForm(this.formImport)) {
            return;
        }
        this.app.confirmMessage(null, () => {// on accepted
            this.service.processImport(this.formImport.value).subscribe(res => {
                if (!this.service.requestIsSuccess(res)) {
                    this.dataError = res.data;
                    this.showButtonImport = false;
                } else {
                    this.dataError = null;
                    this.activeModal.close(res);
                }
            });
        }, null);
    }

    public setFormValue(propertyConfigs: any, data?: any, service?: any) {
        this.formImport = this.buildForm(data, this.formImportConfig, ACTION_FORM.IMPORT);
        this.formImport.addControl('fileImport', new FileControl(null, ValidationService.required));
        this.service = service;
    }
}
