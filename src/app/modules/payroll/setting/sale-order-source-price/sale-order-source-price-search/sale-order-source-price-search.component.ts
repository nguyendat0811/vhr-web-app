import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { RESOURCE, ACTION_FORM, DEFAULT_MODAL_OPTIONS } from '@app/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CommonUtils } from '@app/shared/services';
import { ActivatedRoute } from '@angular/router';
import { AppComponent } from '@app/app.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SaleOrderSourcePriceService } from '@app/core/services/hr-payroll/sale-order-source-price.service';
import { SaleOrderSourcePriceAddComponent } from '../sale-order-source-price-add/sale-order-source-price-add.component';
import { PayrollGroupService } from '@app/core/services/hr-payroll/payroll-group.service';
import { SaleOrderSourceService } from '@app/core/services/hr-payroll/sale-order-source.service';
import { PayrollPriceImportComponent } from '../../payroll-price-import/payroll-price-import.component';

@Component({
    selector: 'sale-order-source-price-search',
    templateUrl: './sale-order-source-price-search.component.html'
})
export class SaleOrderSourcePriceSearchComponent extends BaseComponent implements OnInit {
    public commonUtils = CommonUtils;
    resultList: any = {};
    formSearch: FormGroup;
    listYear: Array<any>;
    payrollPriceId: number;
    checkFirstSearch = false;
    listPayrollGroup: any;
    listSaleOrderSource: any;
    formConfig = {
        startDate: [''],
        endDate: [''],
        saleOrderSourceId: [''],
        payrollGroupId: [''],
    };
    constructor(public actr: ActivatedRoute
        , private modalService: NgbModal
        , public saleOrderSourceService: SaleOrderSourceService
        , public payrollGroupService: PayrollGroupService
        , public payrollPriceService: SaleOrderSourcePriceService
        , private app: AppComponent) {
        super(actr, RESOURCE.PERIOD, ACTION_FORM.SEARCH);
        if (!this.hasPermission('action.view')) {
            return;
        }
        this.setMainService(payrollPriceService);
        this.payrollGroupService.findAll().subscribe(rs => {
            this.listPayrollGroup = rs.data.map(x => {
                return { payrollGroupId: x.GROUPPAYROLLID, payrollGroupName: x.NAMEGROUPPAYROLL };
            });
        });
        this.saleOrderSourceService.findAll().subscribe(rs => {
            this.listSaleOrderSource = rs.data.map(x => {
                return { saleOrderSourceId: x.SALEORDERSOURCEID, saleOrderSourceName: x.SALEORDERSOURCENAME };
            });
        });    
        this.formSearch = this.buildForm({}, this.formConfig, ACTION_FORM.SEARCH);
    }

    ngOnInit() {
        if (!this.hasPermission('action.view')) {
            return;
        }
        this.processSearch({'first':0, 'rows': 10});
    }

    //
    get f() {
        return this.formSearch.controls;
    }
    //
    prepareSaveOrUpdate(item?: any) {
        if (item && item.id > 0) {
            this.payrollPriceService.findOne(item.id)
                .subscribe(res => {
                    this.activeFormModal(this.modalService, SaleOrderSourcePriceAddComponent, res.data);
                });
        } else {
            this.activeFormModal(this.modalService, SaleOrderSourcePriceAddComponent, {});
        }
    }
    //
    processDelete(item) {
        if (item && item.id > 0) {
            this.app.confirmDelete(null, () => {
                this.payrollPriceService.deleteById(item.id)
                    .subscribe(res => {
                        if (this.payrollPriceService.requestIsSuccess(res)) {
                            this.processSearch({'first':0, 'rows': 10});
                        }
                    });
            }, null);
        }
    }

    public processExport(event?) {
        if (!CommonUtils.isValidForm(this.formSearch)) {
            return;
        }
        let params = Object.assign({}, this.formSearch.value);
        this.payrollPriceService.processExport(params).subscribe(res => {
            saveAs(res, 'xnk_payroll_sos_price_export.xls');
        });
    }

    prepareImport() {
        const modalRef = this.modalService.open(PayrollPriceImportComponent, DEFAULT_MODAL_OPTIONS);
        modalRef.componentInstance.setFormValue(this.propertyConfigs, this.formSearch.value, this.payrollPriceService);
        modalRef.result.then((result) => {
            if (!result) {
                return;
            }
            this.processSearch({'first':0, 'rows': 10});
        });
    }
}
