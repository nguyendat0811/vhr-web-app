import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SaleOrderSourcePriceService } from '@app/core/services/hr-payroll/sale-order-source-price.service';
import { AppComponent } from '@app/app.component';
import { ACTION_FORM, RESOURCE, APP_CONSTANTS } from '@app/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ValidationService, CommonUtils } from '@app/shared/services';
import { SaleOrderSourceService } from '@app/core/services/hr-payroll/sale-order-source.service';
import { PayrollGroupService } from '@app/core/services/hr-payroll/payroll-group.service';

@Component({
    selector: 'sale-order-source-price-add',
    templateUrl: './sale-order-source-price-add.component.html',
})
export class SaleOrderSourcePriceAddComponent extends BaseComponent implements OnInit {
    formSave: FormGroup;
    formParam: FormArray;
    id: number;
    listPayrollGroup: any;
    listSaleOrderSource: any;
    listTypePriceBy: any;

    formConfig = {
        id: [],
        saleOrderSourceId: ['', [ValidationService.required]],
        payrollGroupId: [''],
        typePriceBy: [1, [ValidationService.required]],
        startDate: ['', [ValidationService.required]],
        endDate: [''],
        salePrice: [0],
        accountingPrice: [0],
        technicalPrice: [0],
        warehousePrice: [0],
        leadPrice: [0],
        managerPrice: [0],
    };
    
    constructor(
        public actr: ActivatedRoute,
        private app: AppComponent,
        public activeModal: NgbActiveModal,
        public saleOrderSourceService: SaleOrderSourceService,
        public payrollGroupService: PayrollGroupService,
        public payrollPriceService: SaleOrderSourcePriceService) {

        super(actr, RESOURCE.PERIOD, ACTION_FORM.INSERT);
        this.buildFormSave({}, ACTION_FORM.INSERT);
        this.listTypePriceBy = [
            { value: 1, text: 'Theo tỉ lệ đơn giá gốc'},
            { value: 2, text: 'Theo doanh thu'},
            { value: 3, text: 'Theo sản lượng'},
        ];
        this.payrollGroupService.findAll().subscribe(rs => {
            this.listPayrollGroup = rs.data.map(x => {
                return { payrollGroupId: x.GROUPPAYROLLID, payrollGroupName: x.NAMEGROUPPAYROLL };
            });
        });
        this.saleOrderSourceService.findAll().subscribe(rs => {
            this.listSaleOrderSource = rs.data.map(x => {
                return { saleOrderSourceId: x.SALEORDERSOURCEID, saleOrderSourceName: x.SALEORDERSOURCENAME };
            });
        });        
    }

    ngOnInit() {
    }
    processSaveOrUpdate() {
        const isValidFormSave = CommonUtils.isValidForm(this.formSave);
        if (!(isValidFormSave)) {
            return;
        } else {
            this.app.confirmMessage(null, () => {
                this.formSave.value.isconfirm = 0;
                this.payrollPriceService.saveOrUpdate(this.formSave.value)
                    .subscribe(res => {
                        if (this.payrollPriceService.requestIsSuccess(res)) {
                            this.activeModal.close(res);
                        }
                        if (res.type === 'CONFIRM') {
                            this.app.confirmMessage('period.confirm.saveorupdate', () => {
                                this.formSave.value.isconfirm = 1;
                                this.payrollPriceService.saveOrUpdate(this.formSave.value).subscribe(resty => {
                                    if (this.payrollPriceService.requestIsSuccess(resty)) {
                                        this.activeModal.close(resty);
                                    }
                                });
                            }, () => { });
                        }
                    });
            }, () => {
            });
        }
    }
    // this.formSave.value.isconfirm = 1;
    //           this.periodService.saveOrUpdate(this.formSave.value).subscribe(resty => {
    //             if (this.periodService.requestIsSuccess(resty)) {
    //               this.activeModal.close(resty);
    //             }
    //           });
    // this.app.confirmMessage('bạn có muốn thêm', () => {
    // }, () => {} );
    get f() {
        return this.formSave.controls;
    }
    public setFormValue(propertyConfigs: any, data?: any) {
        this.propertyConfigs = propertyConfigs;
        if (data && data.id > 0) {
            this.buildFormSave(data, ACTION_FORM.UPDATE);
        } else {
            this.buildFormSave({}, ACTION_FORM.INSERT);
        }
    }

    /**
     * buildFormSave
     * @ param data
     * @ param actionForm
     */
    private buildFormSave(data?: any, actionForm?: ACTION_FORM) {
        this.formSave = this.buildForm(data, this.formConfig, actionForm,
            [ValidationService.notAffter('startDate', 'endDate', 'app.payrolls.period.dateto')]);
    }
}

