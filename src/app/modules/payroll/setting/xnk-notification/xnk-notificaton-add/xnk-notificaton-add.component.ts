import { Component, OnInit } from '@angular/core';
import { FormGroup, FormArray } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from '@app/app.component';
import { ACTION_FORM, RESOURCE } from '@app/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ValidationService, CommonUtils } from '@app/shared/services';
import { XnkNotificationService } from '@app/core/services/hr-payroll/xnk-notification.service';
import * as DecoupledEditor from '@ckeditor/ckeditor5-build-decoupled-document';

@Component({
    selector: 'xnk-notificaton-add',
    templateUrl: './xnk-notificaton-add.component.html'
})
export class XnkNotificatonAddComponent extends BaseComponent implements OnInit {
    formSave: FormGroup;
    formParam: FormArray;
    id: number;
    public Editor = DecoupledEditor;

    formConfig = {
        id: [],
        title: ['', [ValidationService.required]],
        startDate: ['', [ValidationService.required]],
        endDate: [''],
        active: [1],
        content: ['', [ValidationService.required]],
    };
    
    constructor(
        public actr: ActivatedRoute,
        private app: AppComponent,
        public activeModal: NgbActiveModal,
        public service: XnkNotificationService) {
        super(actr, RESOURCE.PERIOD, ACTION_FORM.INSERT);
        this.buildFormSave({}, ACTION_FORM.INSERT);
    }

    ngOnInit() {
    }

    public onReady(editor ) {
        editor.ui.getEditableElement().parentElement.insertBefore(
            editor.ui.view.toolbar.element,
            editor.ui.getEditableElement()
        );
    }

    processSaveOrUpdate() {
        const isValidFormSave = CommonUtils.isValidForm(this.formSave);
        if (!(isValidFormSave)) {
            return;
        } else {
            this.app.confirmMessage(null, () => {
                this.formSave.value.isconfirm = 0;
                this.service.saveOrUpdate(this.formSave.value)
                    .subscribe(res => {
                        if (this.service.requestIsSuccess(res)) {
                            this.activeModal.close(res);
                        }
                        if (res.type === 'CONFIRM') {
                            this.app.confirmMessage('period.confirm.saveorupdate', () => {
                                this.formSave.value.isconfirm = 1;
                                this.service.saveOrUpdate(this.formSave.value).subscribe(resty => {
                                    if (this.service.requestIsSuccess(resty)) {
                                        this.activeModal.close(resty);
                                    }
                                });
                            }, () => { });
                        }
                    });
            }, () => {
            });
        }
    }
    get f() {
        return this.formSave.controls;
    }
    public setFormValue(propertyConfigs: any, data?: any) {
        this.propertyConfigs = propertyConfigs;
        if (data && data.id > 0) {
            this.buildFormSave(data, ACTION_FORM.UPDATE);
        } else {
            this.buildFormSave({}, ACTION_FORM.INSERT);
        }
    }

    /**
     * buildFormSave
     * @ param data
     * @ param actionForm
     */
    private buildFormSave(data?: any, actionForm?: ACTION_FORM) {
        this.formSave = this.buildForm(data, this.formConfig, actionForm,
            [ValidationService.notAffter('startDate', 'endDate', 'app.payrolls.period.dateto')]);
    }
}

