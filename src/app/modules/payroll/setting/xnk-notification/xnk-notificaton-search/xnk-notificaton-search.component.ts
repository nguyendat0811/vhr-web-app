import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { RESOURCE, ACTION_FORM } from '@app/core';
import { FormGroup } from '@angular/forms';
import { CommonUtils } from '@app/shared/services';
import { ActivatedRoute } from '@angular/router';
import { AppComponent } from '@app/app.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { XnkNotificatonAddComponent } from '../xnk-notificaton-add/xnk-notificaton-add.component';
import { XnkNotificationService } from '@app/core/services/hr-payroll/xnk-notification.service';

@Component({
    selector: 'xnk-notificaton-search',
    templateUrl: './xnk-notificaton-search.component.html'
})
export class XnkNotificatonSearchComponent extends BaseComponent implements OnInit {
    public commonUtils = CommonUtils;
    resultList: any = {};
    formSearch: FormGroup;
    listYear: Array<any>;
    checkFirstSearch = false;
    formConfig = {
        search: ['']
    };
    constructor(public actr: ActivatedRoute
        , private modalService: NgbModal
        , public service: XnkNotificationService
        , private app: AppComponent) {
        super(actr, RESOURCE.PERIOD, ACTION_FORM.SEARCH);
        if (!this.hasPermission('action.view')) {
            return;
        }
        this.setMainService(service);
        this.formSearch = this.buildForm({}, this.formConfig, ACTION_FORM.SEARCH);
    }

    ngOnInit() {
        if (!this.hasPermission('action.view')) {
            return;
        }
        this.processSearch();
    }

    //
    get f() {
        return this.formSearch.controls;
    }
    //
    prepareSaveOrUpdate(item?: any) {
        if (item && item.id > 0) {
            this.service.findOne(item.id)
                .subscribe(res => {
                    this.activeFormModal(this.modalService, XnkNotificatonAddComponent, res.data);
                });
        } else {
            this.activeFormModal(this.modalService, XnkNotificatonAddComponent, {});
        }
    }
    //
    processDelete(item) {
        if (item && item.id > 0) {
            this.app.confirmDelete(null, () => {
                this.service.deleteById(item.id)
                    .subscribe(res => {
                        if (this.service.requestIsSuccess(res)) {
                            this.processSearch();
                        }
                    });
            }, null);
        }
    }
}
