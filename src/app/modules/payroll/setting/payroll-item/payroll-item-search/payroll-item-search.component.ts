import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { FormGroup } from '@angular/forms';
import { AppComponent } from '@app/app.component';
import { CommonUtils, ValidationService } from '@app/shared/services';
import { PayrollItemService } from '@app/core/services/hr-payroll/payroll-item.service';
import { RESOURCE, ACTION_FORM, SalaryTypeService, APP_CONSTANTS } from '@app/core';
import { PayrollItemAddComponent } from '../payroll-item-add/payroll-item-add.component';
import { PayrollItemCloneComponent } from '../payroll-item-clone/payroll-item-clone.component';
import { PayrollItemDeleteComponent } from '../payroll-item-delete/payroll-item-delete.component';

@Component({
  selector: 'payroll-item-search',
  templateUrl: './payroll-item-search.component.html',
})
export class PayrollItemSearchComponent extends BaseComponent implements OnInit {
  formSearch: FormGroup;
  public commonUtils = CommonUtils;
  payrollTypeList: any = {};
  statusList: any = {};
  formConfig = {
    code: ['', [ValidationService.maxLength(50)]],
    name: ['', [ValidationService.maxLength(100)]],
    salaryTypeId: ['', [ValidationService.maxLength(20)]],
    status: ['', [ValidationService.maxLength(1)]]
  };
  constructor(
    private modalService: NgbModal,
    public actr: ActivatedRoute,
    private app: AppComponent,
    private payrollItemService: PayrollItemService,
    public salaryTypeServices: SalaryTypeService,
    ) {
    super(actr, RESOURCE.PAYROLL_ITEM, ACTION_FORM.SEARCH);
    if (!this.hasPermission('action.view')) {
      return;
    }
    this.setMainService(payrollItemService);
    this.formSearch = this.buildForm({}, this.formConfig, ACTION_FORM.SEARCH);
    this.salaryTypeServices.findAll().subscribe(res => {
      this.payrollTypeList = res.data;
    });
    this.payrollItemService.search().subscribe(res => {
      this.resultList = res;
    });
    this.statusList = APP_CONSTANTS.PAYROLL_ITEM;
  }

  ngOnInit() {
  }

  get f() {
    return this.formSearch.controls;
  }

  processDelete(item) {
    if (item && item.payrollItemId > 0) {
      this.app.confirmDelete(null, () => {
        this.payrollItemService.deleteById(item.payrollItemId)
          .subscribe(res => {
            if (this.payrollItemService.requestIsSuccess(res)) {
              this.processSearch(null);
            }
          });
      }, () => {

      });
    }
  }

  prepareClone() {
    this.activeFormModal(this.modalService, PayrollItemCloneComponent, {});
  }

  prepareSaveOrUpdate(item?: any) {
    if (item && item.payrollItemId > 0) {
      this.payrollItemService.findOne(item.payrollItemId)
        .subscribe(resSCType => {
          this.activeFormModal(this.modalService, PayrollItemAddComponent, resSCType.data);
        });
    } else {
        this.activeFormModal(this.modalService, PayrollItemAddComponent, {});
    }
  }

  processDeleteBySalary() {
    this.activeFormModal(this.modalService, PayrollItemDeleteComponent, {});
  }
}
