import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppComponent } from '@app/app.component';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { RESOURCE, ACTION_FORM, APP_CONSTANTS, SalaryTypeService, SystemParameterService, SYSTEM_PARAMETER_CODE } from '@app/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { FormGroup, Validators } from '@angular/forms';
import { CommonUtils, ValidationService } from '@app/shared/services';
import { PayrollItemService } from '@app/core/services/hr-payroll/payroll-item.service';

@Component({
  selector: 'payroll-item-clone',
  templateUrl: './payroll-item-clone.component.html',
})
export class PayrollItemCloneComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  salarytypeList: any = {};
  atttributeTyleList: any = {};
  salaryFundList: any = {};
  paymentCodeList: any = {};
  columName: any = [];
  public commonUtils = CommonUtils;
  formConfig = {
    salaryTypeIdSrc: ['', [ValidationService.required]],
    salaryTypeIdTarget: ['', [ValidationService.required]],
  };
  constructor(
    public actr: ActivatedRoute,
    private app: AppComponent,
    public activeModal: NgbActiveModal,
    public salaryTypeService: SalaryTypeService,
    public systemParameterService: SystemParameterService,
    public  payrollItemService: PayrollItemService,
  ) {
    super(actr, RESOURCE.SALARY_TYPE, ACTION_FORM.INSERT);
    if (!this.hasPermission('action.view')) {
      return;
    }
    this.formSave = this.buildForm({}, this.formConfig);

    this.salarytypeList = salaryTypeService.getListActive().subscribe(res => {
      this.salarytypeList = res.data;
    });
    this.payrollItemService.getColumName()
    .subscribe(res => {
      this.setObjectvalue(res);
    });
  }

  get f() {
    return this.formSave.controls;
  }

  ngOnInit() {
  }

  // convert danh sach ten truong output thanh danh sach object
  public setObjectvalue(data) {
    const listTemp = [];
    for (let i = 0 ; i < data.length ; i++) {
      const obj = {
        name: data[i]
      };
      listTemp.push(obj);
    }
    this.columName = listTemp;
  }

  public setFormValue(propertyConfigs: any, data?: any) {
    this.propertyConfigs = propertyConfigs;
    if (data && data.payrollItemId > 0) {
      this.formSave = this.buildForm(data, this.formConfig, ACTION_FORM.UPDATE);
    } else {
      this.formSave = this.buildForm({}, this.formConfig, ACTION_FORM.INSERT);
    }
  }

  processClone() {
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    } else {
      this.app.confirmMessage(null, () => {// on accepted
        if(this.formSave.value.salaryTypeIdSrc ===  this.formSave.value.salaryTypeIdTarget){
          this.app.errorMessage('payroll.payrollItem', 'duplicateSelect');
          return;
        }else
        {
          this.payrollItemService.cloneData(this.formSave.value).subscribe(res=> {
            this.activeModal.close(res);
          })
        }
      }, null);
    }
  }
}
