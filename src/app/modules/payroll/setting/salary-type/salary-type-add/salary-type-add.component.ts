import { Component, OnInit } from '@angular/core';
import { ValidationService } from '@app/shared/services/validation.service';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RESOURCE, ACTION_FORM, SystemParameterService, SYSTEM_PARAMETER_CODE } from '@app/core';
import { AppComponent } from '@app/app.component';
import { SalaryTypeService } from '@app/core/services/hr-payroll/salary-type.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonUtils } from '@app/shared/services/common-utils.service';

@Component({
  selector: 'salary-type-add',
  templateUrl: './salary-type-add.component.html',
})
export class SalaryTypeAddComponent extends BaseComponent implements OnInit {
  resultList: any = {};
  formSave: FormGroup;
  salaryTypeList: any;
  checkIsLock: boolean;
  regulationSalaryList: any;
  formConfig = {
    salaryTypeId: [''],
    code: ['', [ValidationService.maxLength(50), ValidationService.required]],
    name: ['', [ValidationService.maxLength(200), ValidationService.required]],
    type: ['', ValidationService.required],
    status: [''],
    isLock: [],
    regulationSalary: ['', ValidationService.required],
  };
  constructor(
    public actr: ActivatedRoute,
    private app: AppComponent,
    public activeModal: NgbActiveModal,
    public salaryTypeServices: SalaryTypeService,
    public systemParameterService: SystemParameterService) {
    super(actr, RESOURCE.SALARY_TYPE, ACTION_FORM.INSERT);
    this.formSave = this.buildForm({ status: 1 }, this.formConfig);
    this.systemParameterService.findByName(SYSTEM_PARAMETER_CODE.SALARY_TYPE)
    .subscribe(arg => {
      this.salaryTypeList = arg.data;
    });
    this.systemParameterService.findByName(SYSTEM_PARAMETER_CODE.REGULATION_SALARY_TYPE)
    .subscribe(arg => {
      this.regulationSalaryList = arg.data;
    });

    this.checkIsLock = true;
  }

  ngOnInit() {
  }
  processSaveOrUpdate() {
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    } else {
      this.app.confirmMessage(null, () => {// on accepted
        // console.log('eeeeee', this.formSave.value.status);
        // if (this.formSave.value.status !== 1) {
        //   this.formSave.value.status = 0;
        // }
        // console.log('eeeeee', this.formSave.value.status);
        this.salaryTypeServices.saveOrUpdate(this.formSave.value)
          .subscribe(res => {
            if (this.salaryTypeServices.requestIsSuccess(res)) {
              this.activeModal.close(res);
            }
          });
      }, () => {
      });
    }
  }
  get f() {
    return this.formSave.controls;
  }
  public setFormValue(propertyConfigs: any, data?: any) {
    this.propertyConfigs = propertyConfigs;
    if (data && data.salaryTypeId > 0) {
      this.formSave = this.buildForm(data, this.formConfig, ACTION_FORM.UPDATE);
      if (data.isLock === 1) {
        this.checkIsLock = false;
      }
    } else {
      this.formSave = this.buildForm({ status: 1 }, this.formConfig, ACTION_FORM.INSERT);
    }
  }
}
