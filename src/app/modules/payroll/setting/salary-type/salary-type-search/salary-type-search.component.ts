import { Component, OnInit } from '@angular/core';
import { ValidationService } from '@app/shared/services/validation.service';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RESOURCE, ACTION_FORM, SystemParameterService, SYSTEM_PARAMETER_CODE, DEFAULT_MODAL_OPTIONS } from '@app/core';
import { SalaryTypeService } from '@app/core/services/hr-payroll/salary-type.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SalaryTypeAddComponent } from '../salary-type-add/salary-type-add.component';
import { AppComponent } from '@app/app.component';
import { CommonUtils } from '@app/shared/services';
import { HelperService } from '@app/shared/services/helper.service';

@Component({
  selector: 'salary-type-search',
  templateUrl: './salary-type-search.component.html',
})
export class SalaryTypeSearchComponent extends BaseComponent implements OnInit {
  public commonUtils = CommonUtils;
  resultList: any = [];
  formSearch: FormGroup;
  salaryTypeId: number;
  regulationSalaryList: any = [];
  salaryTypeList: any;
  rowGroupMetadata: any = [];
  formConfig = {
    code: ['', [ValidationService.maxLength(50)]],
    name: ['', [ValidationService.maxLength(200)]],
    regulationSalary: [''],
    type: ['']
  };
  constructor(
    private modalService: NgbModal,
    public actr: ActivatedRoute,
    public salaryTypeServices: SalaryTypeService,
    private helperService: HelperService,
    private app: AppComponent,
    public systemParameterService: SystemParameterService,
    ) {
    super(actr, RESOURCE.SALARY_TYPE, ACTION_FORM.SEARCH);
    if (!this.hasPermission('action.view')) {
      return;
    }
    this.setMainService(salaryTypeServices);
    this.formSearch = this.buildForm({}, this.formConfig, ACTION_FORM.SEARCH);
    this.systemParameterService.findByName(SYSTEM_PARAMETER_CODE.REGULATION_SALARY_TYPE)
    .subscribe(arg => {
      this.regulationSalaryList = arg.data;
    });
    this.systemParameterService.findByName(SYSTEM_PARAMETER_CODE.SALARY_TYPE)
    .subscribe(arg => {
      this.salaryTypeList = arg.data;
    });
    this.actionSearch(null);
  }

  ngOnInit() {
    if (!this.hasPermission('action.view')) {
      return;
    }
    this.salaryTypeServices.search().subscribe(res => {
      this.resultList = res;
    });
  }
  get f() {
    return this.formSearch.controls;
  }
  prepareSaveOrUpdate(item?: any) {
    if (item && item.salaryTypeId > 0) {
      this.salaryTypeServices.findOne(item.salaryTypeId)
        .subscribe(resSCType => {
          this.actionOpenModalSave(this.modalService, SalaryTypeAddComponent, resSCType.data);
        });
    } else {
        this.actionOpenModalSave(this.modalService, SalaryTypeAddComponent, {});
    }
  }
  actionOpenModalSave(service, component, data) {
    const modalRef = service.open(component, DEFAULT_MODAL_OPTIONS);
    modalRef.componentInstance.setFormValue(this.propertyConfigs, data);
    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
      if (this.salaryTypeServices.requestIsSuccess(result)) {
        this.actionSearch(null);
      }
    });
  }
  processDelete(item) {
    if (item && item.salaryTypeId > 0) {
      this.app.confirmDelete(null, () => {
        this.salaryTypeServices.deleteById(item.salaryTypeId)
          .subscribe(res => {
            if (this.salaryTypeServices.requestIsSuccess(res)) {
              this.actionSearch(null);
            }
          });
      }, () => {

      });
    }
  }
  changeStatus (item) {
    if (item.isLock === 1) {
      this.helperService.APP_TOAST_MESSAGE.next({type: 'WARNING', code: 'salary.type.cannotChange'});
    } else {
      this.app.confirmMessage('payroll.formula.config.confirmStatus',
      () => {
        if (item.status === 1) {
          item.status = 0;
        } else {
          item.status = 1;
        }
        this.salaryTypeServices.updateStatusById(item.salaryTypeId, item.status)
        .subscribe(res => {
          if (this.salaryTypeServices.requestIsSuccess(res)) {
            this.actionSearch(null);
          }
        });
      },
      () => {});
    }
  }
  public actionSearch(event?): void {
    if (!CommonUtils.isValidForm(this.formSearch)) {
      return;
    }
    const params = this.formSearch ? this.formSearch.value : null;
    this.salaryTypeServices.search(params, event).subscribe(res => {
      this.resultList = res;
      console.log('res: ', res)
      this.updateRowGroupMetaData();
    });
    if (!event) {
      if (this.dataTable) {
        this.dataTable.first = 0;
      }
    }
  }

  updateRowGroupMetaData() {
    this.rowGroupMetadata = {};
    if (this.resultList.data) {
      for (let i = 0; i < this.resultList.data.length; i++) {
        const rowData = this.resultList.data[i];
        const regulationSalary = rowData.regulationSalary;
        if (i === 0) {
            this.rowGroupMetadata[regulationSalary] = { index: 0, size: 1 };
        } else {
          const previousRowData = this.resultList.data[i - 1];
          const previousRowGroup = previousRowData.regulationSalary;
          if (regulationSalary === previousRowGroup) {
            this.rowGroupMetadata[regulationSalary].size++;
          } else {
            this.rowGroupMetadata[regulationSalary] = { index: i, size : 1 };
          }
        }
      }
    }
  }
}
