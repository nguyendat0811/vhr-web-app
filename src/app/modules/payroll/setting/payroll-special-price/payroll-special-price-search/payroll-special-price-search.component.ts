import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { RESOURCE, ACTION_FORM, DEFAULT_MODAL_OPTIONS } from '@app/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CommonUtils } from '@app/shared/services';
import { ActivatedRoute } from '@angular/router';
import { AppComponent } from '@app/app.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PayrollPriceImportComponent } from '../../payroll-price-import/payroll-price-import.component';
import { PayrollSpecialPriceService } from '@app/core/services/hr-payroll/payroll-special-price.service';
import { PayrollSpecialPriceAddComponent } from '../payroll-special-price-add/payroll-special-price-add.component';

@Component({
    selector: 'payroll-special-price-search',
    templateUrl: './payroll-special-price-search.component.html'
})
export class PayrollSpecialPriceSearchComponent extends BaseComponent implements OnInit {
    public commonUtils = CommonUtils;
    resultList: any = {};
    formSearch: FormGroup;
    listYear: Array<any>;
    payrollPriceId: number;
    checkFirstSearch = false;
    formConfig = {
        startDate: [''],
        endDate: [''],
        imei: [''],
        shopCode: [''],
    };
    constructor(public actr: ActivatedRoute
        , private modalService: NgbModal
        , public payrollPriceService: PayrollSpecialPriceService
        , private app: AppComponent) {
        super(actr, RESOURCE.PERIOD, ACTION_FORM.SEARCH);
        if (!this.hasPermission('action.view')) {
            return;
        }
        this.setMainService(payrollPriceService);
        this.formSearch = this.buildForm({}, this.formConfig, ACTION_FORM.SEARCH);
    }

    ngOnInit() {
        if (!this.hasPermission('action.view')) {
            return;
        }
        this.processSearch({ 'first': 0, 'rows': 10 });
    }

    get f() {
        return this.formSearch.controls;
    }

    prepareSaveOrUpdate(item?: any) {
        if (item && item.id > 0) {
            this.payrollPriceService.findOne(item.id)
                .subscribe(res => {
                    this.activeFormModal(this.modalService, PayrollSpecialPriceAddComponent, res.data);
                });
        } else {
            this.activeFormModal(this.modalService, PayrollSpecialPriceAddComponent, {});
        }
    }

    processDelete(item) {
        if (item && item.id > 0) {
            this.app.confirmDelete(null, () => {
                this.payrollPriceService.deleteById(item.id)
                    .subscribe(res => {
                        if (this.payrollPriceService.requestIsSuccess(res)) {
                            this.processSearch({ 'first': 0, 'rows': 10 });
                        }
                    });
            }, null);
        }
    }

    public processExport(event?) {
        if (!CommonUtils.isValidForm(this.formSearch)) {
            return;
        }
        let params = Object.assign({}, this.formSearch.value);
        this.payrollPriceService.processExport(params).subscribe(res => {
            saveAs(res, 'xnk_payroll_special_price_export.xls');
        });
    }

    prepareImport() {
        const modalRef = this.modalService.open(PayrollPriceImportComponent, DEFAULT_MODAL_OPTIONS);
        modalRef.componentInstance.setFormValue(this.propertyConfigs, this.formSearch.value, this.payrollPriceService);
        modalRef.result.then((result) => {
            if (!result) {
                return;
            }
            this.processSearch({ 'first': 0, 'rows': 10 });
        });
    }
}
