import { Component, OnInit } from '@angular/core';
import { FormGroup, FormArray } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AppComponent } from '@app/app.component';
import { RESOURCE, ACTION_FORM } from '@app/core';
import { PayrollOrgMappingService } from '@app/core/services/hr-payroll/payroll-org-mapping.service';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ValidationService, CommonUtils } from '@app/shared/services';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'payroll-org-mapping-add',
    templateUrl: './payroll-org-mapping-add.component.html',
    styleUrls: ['./payroll-org-mapping-add.component.css']
})
export class PayrollOrgMappingAddComponent extends BaseComponent implements OnInit {

    formSave: FormGroup;
    formParam: FormArray;
    listMarketLocation: any;
    listMarketGroup: any;

    formConfig = {
        organnizationId: ['', [ValidationService.required]],
        osOrgannizationId: [''],
        shopCode: [''],
        erpShopCode: [''],
        marketLocation: ['', [ValidationService.required]],
        marketGroup: ['', [ValidationService.required]],
    };

    constructor(
        public actr: ActivatedRoute,
        private app: AppComponent,
        public activeModal: NgbActiveModal,
        public payrollOrgMappingService: PayrollOrgMappingService) {
        super(actr, RESOURCE.PERIOD, ACTION_FORM.INSERT);
        this.buildFormSave({}, ACTION_FORM.INSERT);
        this.listMarketLocation = [
            { value: 1, text: 'Vùng 1'},
            { value: 2, text: 'Vùng 2'},
            { value: 3, text: 'Vùng 3'},
            { value: 4, text: 'Vùng 4'},
        ];
        this.listMarketGroup = [
            { value: 1, text: 'Nhóm 1'},
            { value: 2, text: 'Nhóm 2'},
            { value: 3, text: 'Nhóm 3'},
            { value: 4, text: 'Nhóm 4'},
            { value: 5, text: 'Nhóm 5'},
            { value: 6, text: 'Nhóm 6'},
            { value: 7, text: 'Nhóm cửa hàng'},
        ];
    }

    ngOnInit() {
    }
    processSaveOrUpdate() {
        const isValidFormSave = CommonUtils.isValidForm(this.formSave);
        if (!(isValidFormSave)) {
            return;
        } else {
            this.app.confirmMessage(null, () => {
                this.formSave.value.isconfirm = 0;
                this.payrollOrgMappingService.saveOrUpdate(this.formSave.value)
                    .subscribe(res => {
                        if (this.payrollOrgMappingService.requestIsSuccess(res)) {
                            this.activeModal.close(res);
                        }
                        if (res.type === 'CONFIRM') {
                            this.app.confirmMessage('period.confirm.saveorupdate', () => {
                                this.formSave.value.isconfirm = 1;
                                this.payrollOrgMappingService.saveOrUpdate(this.formSave.value).subscribe(resty => {
                                    if (this.payrollOrgMappingService.requestIsSuccess(resty)) {
                                        this.activeModal.close(resty);
                                    }
                                });
                            }, () => { });
                        }
                    });
            }, () => {
            });
        }
    }
    
    get f() {
        return this.formSave.controls;
    }
    public setFormValue(propertyConfigs: any, data?: any) {
        this.propertyConfigs = propertyConfigs;
        console.log(data);
        if (data && data.organnizationId > 0) {
            this.buildFormSave(data, ACTION_FORM.UPDATE);
        } else {
            this.buildFormSave({}, ACTION_FORM.INSERT);
        }
    }

    /**
     * buildFormSave
     * @ param data
     * @ param actionForm
     */
    private buildFormSave(data?: any, actionForm?: ACTION_FORM) {
        this.formSave = this.buildForm(data, this.formConfig, actionForm);
    }

}
