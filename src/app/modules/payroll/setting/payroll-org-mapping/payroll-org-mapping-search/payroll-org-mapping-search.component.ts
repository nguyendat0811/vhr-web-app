import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AppComponent } from '@app/app.component';
import { RESOURCE, ACTION_FORM, DEFAULT_MODAL_OPTIONS } from '@app/core';
import { PayrollOrgMappingService } from '@app/core/services/hr-payroll/payroll-org-mapping.service';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { CommonUtils } from '@app/shared/services';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PayrollPriceImportComponent } from '../../payroll-price-import/payroll-price-import.component';
import { PayrollOrgMappingAddComponent } from '../payroll-org-mapping-add/payroll-org-mapping-add.component';

@Component({
    selector: 'payroll-org-mapping-search',
    templateUrl: './payroll-org-mapping-search.component.html',
    styleUrls: ['./payroll-org-mapping-search.component.css']
})
export class PayrollOrgMappingSearchComponent extends BaseComponent implements OnInit {

    public commonUtils = CommonUtils;
    resultList: any = {};
    formSearch: FormGroup;
    listYear: Array<any>;
    payrollPriceId: number;
    checkFirstSearch = false;
    formConfig = {
        search: [''],
    };
    constructor(public actr: ActivatedRoute
        , private modalService: NgbModal
        , public payrollOrgMappingService: PayrollOrgMappingService
        , private app: AppComponent) {
        super(actr, RESOURCE.PERIOD, ACTION_FORM.SEARCH);
        if (!this.hasPermission('action.view')) {
            return;
        }
        this.setMainService(payrollOrgMappingService);
        this.formSearch = this.buildForm({}, this.formConfig, ACTION_FORM.SEARCH);
    }

    ngOnInit() {
        if (!this.hasPermission('action.view')) {
            return;
        }
        this.processSearch({'first':0, 'rows': 10});
    }

    //
    get f() {
        return this.formSearch.controls;
    }
    //
    prepareSaveOrUpdate(item?: any) {
        if (item && item.organnizationId > 0) {
            this.payrollOrgMappingService.findOne(item.organnizationId)
                .subscribe(res => {
                    this.activeFormModal(this.modalService, PayrollOrgMappingAddComponent, res.data);
                });
        } else {
            this.activeFormModal(this.modalService, PayrollOrgMappingAddComponent, {});
        }
    }
    //
    processDelete(item) {
        if (item && item.organnizationId > 0) {
            this.app.confirmDelete(null, () => {
                this.payrollOrgMappingService.deleteById(item.organnizationId)
                    .subscribe(res => {
                        if (this.payrollOrgMappingService.requestIsSuccess(res)) {
                            this.processSearch({'first':0, 'rows': 10});
                        }
                    });
            }, null);
        }
    }

    public processExport(event?) {
        if (!CommonUtils.isValidForm(this.formSearch)) {
            return;
        }
        let params = Object.assign({}, this.formSearch.value);
        this.payrollOrgMappingService.processExport(params).subscribe(res => {
            saveAs(res, 'xnk_payroll_org_mapping_export.xls');
        });
    }

    prepareImport() {
        const modalRef = this.modalService.open(PayrollPriceImportComponent, DEFAULT_MODAL_OPTIONS);
        modalRef.componentInstance.setFormValue(this.propertyConfigs, this.formSearch.value, this.payrollOrgMappingService);
        modalRef.result.then((result) => {
            if (!result) {
                return;
            }
            this.processSearch({ 'first': 0, 'rows': 10 });
        });
    }
}
