import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { Component, OnInit, ViewChildren } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AppComponent } from '@app/app.component';
import { ACTION_FORM, RESOURCE, SalaryTypeOrganizationMappingService, SalaryTypeService } from '@app/core';
import { XnkPayrollConfigService } from '@app/core/services/hr-payroll/xnk-payroll-config.service';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { CommonUtils, ValidationService } from '@app/shared/services';

@Component({
    selector: 'xnk-config',
    templateUrl: './xnk-config.component.html',
    styleUrls: ['./xnk-config.component.css']
})
export class XnkConfigComponent extends BaseComponent implements OnInit {

    formSave: FormGroup;
    checkFocus = 0; // focus vao input
    @ViewChildren('inputSearch') inputSearch;
    formSaveConfig = {
        configType: ['COMPENSATED_FORMULA'],
        organizationId: [null, [ValidationService.required]],
        salaryTypeId: [null, [ValidationService.required]],
        payrollItemList: [null]
    };
    nameFilter = '';
    listSalaryType = [];

    listPayroll = [];
    listPayrollConfig = [];

    constructor(
        private activatedRoute: ActivatedRoute,
        private salaryTypeService: SalaryTypeService,
        private xnkPayrollConfigService: XnkPayrollConfigService,
        private app: AppComponent,
        private salaryTypeOrganizationMappingService: SalaryTypeOrganizationMappingService) {
        super(activatedRoute, RESOURCE.PAYROLL_TABLE_CONFIG, ACTION_FORM.SEARCH);
        this.buildFormSave();
        this.salaryTypeService.getListActive().subscribe(res => {
            this.listSalaryType = res.data;
        });

        this.f.organizationId.valueChanges.subscribe(data => {
            this.onChangeOrganization(data);
        });
    }

    ngOnInit() {
        this.initData();
    }

    get f() {
        return this.formSave.controls;
    }

    /**
    * onChangeOrganization
     */
    onChangeOrganization(orgId) {
        if (orgId) {
            this.salaryTypeOrganizationMappingService.findByOrgId(orgId).subscribe(res => {
                this.listSalaryType = res.data;
            });
        }
    }
    /**
     * Xu ly build form save
     * @param data
     */
    private buildFormSave(data?: any) {
        this.formSave = this.buildForm(data || {}, this.formSaveConfig, this.actionForm);
        this.f.organizationId.valueChanges.subscribe(data => {
            this.initData();
        });
        this.f.salaryTypeId.valueChanges.subscribe(data => {
            this.initData();
        });
    }

    processSaveOrUpdate() {
        if (!CommonUtils.isValidForm(this.formSave)) {
            return;
        }

        this.app.confirmMessage(null, () => {
            this.f.payrollItemList.setValue(this.listPayrollConfig);
            this.xnkPayrollConfigService.saveOrUpdate(this.formSave.value)
                .subscribe(res => {
                    this.initData();
                    this.onChangeOrganization(this.f.organizationId.value);
                });
        }, null);
    }

    processCancel() {
        this.buildFormSave();
    }

    payrollItemCheck(item) {
        const obj = this.listPayrollConfig.findIndex(x => x.payrollItemId === item.payrollItemId);
        if (item.isConfig) {
            if (obj === -1) {
                this.listPayrollConfig.push(item);
            }
        } else {
            if (obj >= 0) {
                this.listPayrollConfig.splice(obj, 1);
            }
        }
    }

    drop(event: CdkDragDrop<string[]>) {
        moveItemInArray(this.listPayrollConfig, event.previousIndex, event.currentIndex);
    }

    private initData() {
        if (this.f.organizationId.value && this.f.salaryTypeId.value) {
            this.xnkPayrollConfigService.getListPayrollItem('COMPENSATED_FORMULA', this.f.salaryTypeId.value).subscribe(
                res => {
                    this.listPayroll = res.data;
                }
            );
            this.xnkPayrollConfigService.getListPayrollItemConfig('COMPENSATED_FORMULA', this.f.salaryTypeId.value).subscribe(
                res => {
                    this.listPayrollConfig = res.data;
                }
            );

            this.focusInputSearch();
        } else {
            this.listPayroll = [];
            this.listPayrollConfig = [];
        }
    }
    // check focus
    focusInputSearch() {
        setTimeout(() => {
            if (this.inputSearch) {
                this.inputSearch.first.nativeElement.focus();
            }
        }, 100);
    }

}