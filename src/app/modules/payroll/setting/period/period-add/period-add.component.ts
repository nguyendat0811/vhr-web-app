import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { PeriodService } from '@app/core/services/hr-payroll/period.serviece';
import { AppComponent } from '@app/app.component';
import { ACTION_FORM, RESOURCE, APP_CONSTANTS } from '@app/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ValidationService, CommonUtils } from '@app/shared/services';

@Component({
  selector: 'period-add',
  templateUrl: './period-add.component.html'
})
export class PeriodAddComponent extends BaseComponent implements OnInit {
  resultList: any = {};
  resultList1: any = {};
  formSave: FormGroup;
  formParam: FormArray;
  PeriodList: any;
  periodId: number;
  periodBeanForm: FormGroup;
  listAttributeType: any;
  listTypeOfPeriod: Array<any> = [
    {
      name: 'Tháng',
      value: 1
    },
    {
      name: 'Quý',
      value: 2
    },
    {
      name: 'Năm',
      value: 3
    },
    {
      name: 'Khác',
      value: 4
    }
  ];
  formConfig = {
    periodId: [],
    periodOrganizationId: ['', [ValidationService.required]],
    periodName: ['', [ValidationService.required]],
    startDate: ['', [ValidationService.required]],
    endDate: ['', [ValidationService.required]],
    isCurrent: [0],
    typeOfPeriod: [1, [ValidationService.required]]
  };
  formParamConfig = {
    periodParamId: [],
    periodId: [],
    paramCode: [null, [ValidationService.maxLength(200)]],
    paramName: [null, [ValidationService.maxLength(200)]],
    attributeType: [null],
    value: [null, [ValidationService.maxLength(200)]],
    description: [null, [ValidationService.maxLength(500)]]
  };
  constructor(
    public actr: ActivatedRoute,
    private app: AppComponent,
    public activeModal: NgbActiveModal,
    public periodService: PeriodService) {
    super(actr, RESOURCE.PERIOD, ACTION_FORM.INSERT);
    this.buildFormSave({}, ACTION_FORM.INSERT);
    this.buildPeriodParamForm();
    this.PeriodList = APP_CONSTANTS.PERIOD;
    this.listAttributeType = APP_CONSTANTS.ATTRIBUTE_TYPE;
  }

  ngOnInit() {
  }
  processSaveOrUpdate() {
    const isValidFormSave = CommonUtils.isValidForm(this.formSave);
    const isValidParamForm = CommonUtils.isValidFormAndValidity(this.formParam);
    if (!(isValidFormSave && isValidParamForm)) {
      return;
    } else {
      this.app.confirmMessage(null, () => {
        this.formSave.value.listParam = this.formParam.value;
        this.formSave.value.isconfirm = 0;
        this.periodService.saveOrUpdate(this.formSave.value)
          .subscribe(res => {
            if (this.periodService.requestIsSuccess(res)) {
              this.activeModal.close(res);
            }
            if (res.type === 'CONFIRM') {
              this.app.confirmMessage('period.confirm.saveorupdate', () => {
                this.formSave.value.isconfirm = 1;
                this.periodService.saveOrUpdate(this.formSave.value).subscribe(resty => {
                  if (this.periodService.requestIsSuccess(resty)) {
                    this.activeModal.close(resty);
                  }
                });
              }, () => { });
            }
          });
      }, () => {
      });
    }
  }
  // this.formSave.value.isconfirm = 1;
  //           this.periodService.saveOrUpdate(this.formSave.value).subscribe(resty => {
  //             if (this.periodService.requestIsSuccess(resty)) {
  //               this.activeModal.close(resty);
  //             }
  //           });
  // this.app.confirmMessage('bạn có muốn thêm', () => {
  // }, () => {} );
  get f() {
    return this.formSave.controls;
  }
  public setFormValue(propertyConfigs: any, data?: any) {
    this.propertyConfigs = propertyConfigs;
    if (data && data.periodId > 0) {
      this.buildFormSave(data, ACTION_FORM.UPDATE);
      this.periodService.getParamByPeriodId(data.periodId).subscribe(res => {
        this.buildPeriodParamForm(res.data);
      });
    } else {
      this.buildFormSave({}, ACTION_FORM.INSERT);
      this.buildPeriodParamForm();
    }
  }

  /**
   * buildFormSave
   * @ param data
   * @ param actionForm
   */
  private buildFormSave(data?: any, actionForm?: ACTION_FORM) {
    this.formSave = this.buildForm(data, this.formConfig, actionForm,
      [ValidationService.notAffter('startDate', 'endDate', 'app.payrolls.period.dateto')]);
  }

  private makePeriodParamForm(): FormGroup {
    const formGroup = this.buildForm({}, this.formParamConfig, this.actionForm,
      [ValidationService.requiredControlInGroup(['paramCode', 'paramName', 'value', 'attributeType'])]);
    return formGroup;
  }

  addConfigDetail() {
    const controls = this.formParam as FormArray;
    controls.insert(controls.length, this.makePeriodParamForm());
  }

  removeConfigDetail(index, item) {
    const controls = this.formParam as FormArray;
    if (item.get('periodParamId').value) {
      this.app.confirmDelete(null, () => {
        this.periodService.deleteParam(item.get('periodParamId').value)
          .subscribe(res => {
            if (this.periodService.requestIsSuccess(res)) {
              if (controls.length === 1) {
                this.buildPeriodParamForm();
              }
              controls.removeAt(index);
            }
          });
      }, null);
    } else {
      if (controls.length === 1) {
        this.buildPeriodParamForm();
      }
      controls.removeAt(index);
    }
  }

  /*
  * buildPeriodParamForm
  */
  private buildPeriodParamForm(listPeriodParam?: any) {
    const controls = new FormArray([]);
    if (listPeriodParam && listPeriodParam.length > 0) {
      for (const i in listPeriodParam) {
        const periodParam = listPeriodParam[i];
        const group = this.makePeriodParamForm();
        group.patchValue(periodParam);
        controls.push(group);
      }
    } else {
      const group = this.makePeriodParamForm();
      controls.push(group);
    }
    controls.setValidators(
      [
        ValidationService.duplicateArray(['paramCode'], 'paramCode', 'payrolls.period.duplicateCode.paramCode')
      ]);
    this.formParam = controls;
  }
}

