import { Component, OnInit, Input } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { RESOURCE, ACTION_FORM, EmpTimekeepingService, WorkdayTypeService, DEFAULT_MODAL_OPTIONS } from '@app/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { ValidationService, CommonUtils } from '@app/shared/services';
import * as moment from 'moment';
import { ActivatedRoute } from '@angular/router';
import { AppComponent } from '@app/app.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PeriodService } from '@app/core/services/hr-payroll/period.serviece';
import { PeriodAddComponent } from '../period-add/period-add.component';

@Component({
    selector: 'period-search',
    templateUrl: './period-search.component.html'
})
export class PeriodSearchComponent extends BaseComponent implements OnInit {
    public commonUtils = CommonUtils;
    resultList: any = {};
    formSearch: FormGroup;
    listYear: Array<any>;
    periodId: number;
    private expiredDate: string;
    checkFirstSearch = false;
    formConfig = {
        organizationId: ['', [ValidationService.required]],
        periodName: [''],
        year: [parseInt(moment(new Date()).format('YYYY')), [ValidationService.required]],
    };
    constructor(public actr: ActivatedRoute
        , private formBuilder: FormBuilder
        , private modalService: NgbModal
        , public periodService: PeriodService
        , private app: AppComponent) {
        super(actr, RESOURCE.PERIOD, ACTION_FORM.SEARCH);
        if (!this.hasPermission('action.view')) {
            return;
        }
        this.setMainService(periodService);
        this.listYear = this.getYearList();
        this.formSearch = this.buildForm({}, this.formConfig, ACTION_FORM.SEARCH);
        this.f.organizationId.valueChanges.subscribe(res => {
            // Xu ly tim kiem khi man hinh co set gia tri mac dinh cho don vi
            if (res && !this.checkFirstSearch) {
                setTimeout(() => {
                    this.processSearch();
                }, 100);
                this.checkFirstSearch = true;
            }
        });
    }

    ngOnInit() {
        if (!this.hasPermission('action.view')) {
            return;
        }
    }

    private getYearList() {
        this.listYear = [];
        const currentYear = new Date().getFullYear();
        for (let i = (currentYear - 50); i <= (currentYear + 50); i++) {
            const obj = {
                year: i
            };
            this.listYear.push(obj);
        }
        return this.listYear;
    }
    //
    get f() {
        return this.formSearch.controls;
    }
    //
    prepareSaveOrUpdate(item?: any) {
        if (item && item.periodId > 0) {
            this.periodService.findOne(item.periodId)
                .subscribe(res => {
                    this.activeFormModal(this.modalService, PeriodAddComponent, res.data);
                });
        } else {
            this.activeFormModal(this.modalService, PeriodAddComponent, {});
        }
    }
    //
    processDelete(item) {
        if (item && item.periodId > 0) {
            this.app.confirmDelete(null, () => {
                this.periodService.deleteById(item.periodId)
                    .subscribe(res => {
                        if (this.periodService.requestIsSuccess(res)) {
                            this.processSearch();
                        }
                    });
            }, null);
        }
    }

    prepareClosing(item) {
        if (item && item.periodId > 0) {
            this.app.confirmMessage('CONFIRM.payrolls.period.closing', () => {
                this.periodService.closing(item.periodId)
                    .subscribe(res => {
                        if (this.periodService.requestIsSuccess(res)) {
                            this.processSearch();
                        }
                    });
            }, null);
        }
    }
}
