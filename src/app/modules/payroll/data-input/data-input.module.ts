import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataInputRoutingModule } from './data-input-routing.module';
import { SalaryTypeMappingIndexComponent } from './salary-type-mapping/salary-type-mapping-index/salary-type-mapping-index.component';
import { SharedModule } from '@app/shared';
import { DataInputProcessSearchComponent } from './data-input-process/data-input-process-search/data-input-process-search.component';
import { DataInputProcessImportComponent } from './data-input-process/data-input-process-import/data-input-process-import.component';
import { SalaryOrganizationMappingIndexComponent } from './salary-organization-mapping/salary-organization-mapping-index/salary-organization-mapping-index.component';
import { SalaryTypeOrganizationMappingIndexComponent } from './salary_type_organization_mapping/salary-type-organization-mapping-index/salary-type-organization-mapping-index.component';
import { DataTypeInfoComponent } from './salary-type-mapping/data-type-info/data-type-info.component';

@NgModule({
  declarations: [
    SalaryTypeMappingIndexComponent,
    DataInputProcessSearchComponent,
    DataInputProcessImportComponent,
    SalaryOrganizationMappingIndexComponent,
    SalaryTypeOrganizationMappingIndexComponent,
    DataTypeInfoComponent,
  ],

  imports: [
    CommonModule,
    SharedModule,
    DataInputRoutingModule,
  ],
  entryComponents: [DataInputProcessImportComponent, DataTypeInfoComponent]
})
export class DataInputModule { }
