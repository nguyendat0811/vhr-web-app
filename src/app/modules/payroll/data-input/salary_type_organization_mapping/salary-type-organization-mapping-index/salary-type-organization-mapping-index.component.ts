import { ValidationService } from '@app/shared/services/validation.service';
import { Component, OnInit, ViewChildren } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { RESOURCE, ACTION_FORM, SalaryTypeOrganizationMappingService } from '@app/core';
import { ActivatedRoute } from '@angular/router';
import { AppComponent } from '@app/app.component';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { CommonUtils } from '@app/shared/services';

@Component({
  selector: 'salary-type-organization-mapping-index',
  templateUrl: './salary-type-organization-mapping-index.component.html'
})
export class SalaryTypeOrganizationMappingIndexComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  @ViewChildren('inputSearch') inputSearch;
  formSaveConfig = {
    organizationId: [null, [ValidationService.required]],
    salaryTypeList: [null],
    nameFilter: [null],
    configFilter: [null]
  };
  nameFilter = '';
  configFilter = '';
  sourceSalaryTypes = [];
  targetSalaryTypesMain = [];
  constructor(
    private activatedRoute: ActivatedRoute,
    private app: AppComponent,
    private salaryTypeOrganizationMappingService: SalaryTypeOrganizationMappingService
  ) {
    super(activatedRoute, RESOURCE.SALARY_TYPE_ORGANIZATION_MAPPING, ACTION_FORM.SALARY_TYPE_ORGANIZATION_MAPPING_UPDATE);
    if (!this.hasPermission('action.view')) {
      return;
    }
    this.buildFormSave();
    this.f.organizationId.valueChanges.subscribe(data => {
      this.onchangeSalaryOrganiztion(data);
    });
  }

  ngOnInit() {
    this.sourceSalaryTypes = [];
    this.targetSalaryTypesMain = [];
  }

  get f () {
    return this.formSave.controls;
  }

  /**
   * Xu ly build form save
   * @ param data
   */
  private buildFormSave(data?: any) {
    this.formSave = this.buildForm(data || {}, this.formSaveConfig, this.actionForm);
  }

  onchangeSalaryOrganiztion(orgId) {
    if (orgId) {
      this.salaryTypeOrganizationMappingService.getData(orgId).subscribe(res => {
        console.log('res: ', res);
        this.targetSalaryTypesMain = res.data;
        this.sourceSalaryTypes = res.extendData.listDataImput;
        console.log('res: ', this.sourceSalaryTypes);
      });
    } else {
      this.sourceSalaryTypes = [];
      this.targetSalaryTypesMain = [];
    }
    this.focusInputSearch();
  }

  // check foucus
  focusInputSearch() {
    setTimeout(() => {
      this.inputSearch.first.nativeElement.focus();
    }, 100);
  }

  private findRealIndex(tempIndex, previousContainerid) {
    let listSearch = {};
    let object = {};
    if (previousContainerid === 'cdk-drop-list-0') {
      if (!this.nameFilter) {
        return tempIndex;
      }
      listSearch = this.sourceSalaryTypes.filter( x =>
        x['name'].toLowerCase().includes(this.nameFilter.toLowerCase()));
      object = listSearch[tempIndex];
      return this.sourceSalaryTypes.findIndex( x => x['salaryTypeId'] === object['salaryTypeId'] );
    } else {
      if (!this.configFilter) {
        return tempIndex;
      }
      listSearch = this.targetSalaryTypesMain.filter( x =>
        x['name'].toLowerCase().includes(this.configFilter.toLowerCase()));
      object = listSearch[tempIndex];
      return this.targetSalaryTypesMain.findIndex( x => x['salaryTypeId'] === object['salaryTypeId'] );
    }
  }

  processCancel() {
    this.buildFormSave();
    this.f.organizationId.valueChanges.subscribe(data => {
      this.onchangeSalaryOrganiztion(data);
    });
  }

  /**
  * drop
  */
  drop(event: CdkDragDrop<string[]>, dropPosition?: any) {
    event.previousIndex = this.findRealIndex(event.previousIndex, event.previousContainer.id);
    if (event.previousContainer === event.container) {
      event.currentIndex = this.findRealIndex(event.currentIndex, event.previousContainer.id);
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);
    }
  }

  /*
  * processSaveOrUpdate
  */
  processSaveOrUpdate() {
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    }
    this.app.confirmMessage(null, () => {
      this.f.salaryTypeList.setValue(this.targetSalaryTypesMain);
      this.salaryTypeOrganizationMappingService.saveOrUpdate(this.formSave.value)
        .subscribe(res => {
          if (this.salaryTypeOrganizationMappingService.requestIsSuccess(res)) {
            this.onchangeSalaryOrganiztion(this.f.organizationId.value);
          }
        });
    }, null);
  }
}
