import { forEach } from '@angular/router/src/utils/collection';
import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ACTION_FORM, RESOURCE, DataTypeService, SalaryOrganizationMappingService, DEFAULT_MODAL_OPTIONS, SalaryTypeOrganizationMappingService } from '@app/core';
import { DataInputProcessService } from '@app/core/services/hr-payroll/data-input-process.service';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import * as moment from 'moment';
import { CommonUtils, ValidationService } from '@app/shared/services';
import { PeriodService } from '@app/core/services/hr-payroll/period.serviece';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DataInputProcessImportComponent } from '../data-input-process-import/data-input-process-import.component';
import { SelectItem } from 'primeng/api';
import { isEmpty } from 'rxjs/operators';
import { isNull } from '@angular/compiler/src/output/output_ast';
import { isNullOrUndefined } from '@syncfusion/ej2-base';
import { AppComponent } from '@app/app.component';

@Component({
  selector: 'data-input-process-search',
  templateUrl: './data-input-process-search.component.html'
})
export class DataInputProcessSearchComponent extends BaseComponent implements OnInit {
  resultList: any = {};
  clonedData = new Array();
  formSearch: FormGroup;
  periodBeanForm: FormGroup;
  listYear: Array<any>;
  dataTypeList = [];
  dataTypeSelected: any;
  salaryTypeList: any;
  periodIdList = [];
  // @Input() item: any;
  formConfig = {
    salaryTypeId: [''],
    organizationId: [null, [ValidationService.required]],
    dataTypeId: ['', [ValidationService.required]],
    year: [parseInt(moment(new Date()).format('YYYY'))],
    periodId: [null, [ValidationService.required]],
    employeeCode: ['', [ValidationService.maxLength(100)]],
    employeeName: ['', [ValidationService.maxLength(100)]],
  };
  periodBean = {
    organizationId: [null],
    year: [null],
  };
  showSearchData = false;

  constructor(public actr: ActivatedRoute
    , private periodService: PeriodService
    , private dataTypeService: DataTypeService
    , private salaryOrganizationMappingService: SalaryOrganizationMappingService
    , public dataInputProcessService: DataInputProcessService
    , private salaryTypeOrganizationMappingService: SalaryTypeOrganizationMappingService
    , private app: AppComponent
    , private modalService: NgbModal) {
    super(actr, RESOURCE.DATA_INPUT_PROCESS, ACTION_FORM.SEARCH);
    this.setMainService(dataInputProcessService);
    this.formSearch = this.buildForm({}, this.formConfig, ACTION_FORM.SEARCH);

    this.f.organizationId.valueChanges.subscribe(data => {
      this.onLineOrgChange();
      this.initSalaryType();
    });
    this.f.salaryTypeId.valueChanges.subscribe(data=>{
      this.initDataType();
    });
    this.f.year.valueChanges.subscribe(data => {
      this.onChangeYear(data);
    });
    this.f.dataTypeId.valueChanges.subscribe(data => {
      if (data && this.dataTypeList.length > 0) {
        this.dataTypeSelected = this.dataTypeList.find(x => x['dataTypeId'] === data);
      } else {
        this.dataTypeSelected = null;
      }
    });
  }

  ngOnInit() {
    this.listYear = this.getYearList();
    this.periodBeanForm = this.buildForm({}, this.periodBean, ACTION_FORM.PAYROLL_GENERAL_SEARCH);
  }

  initSalaryType() {
    if (this.f.organizationId.value) {
      this.salaryTypeOrganizationMappingService.findByOrgId(this.f.organizationId.value).subscribe(res => {
        this.salaryTypeList = res.data;
      });
    } else {
      this.salaryTypeList = null;
    }
  }

  initDataType(){
    if (this.f.organizationId.value) {
      this.salaryOrganizationMappingService.getDataFromConfig(this.f.organizationId.value, this.f.salaryTypeId.value).subscribe(res => {
        this.dataTypeList = res.data;
      });
    } else {
      this.salaryTypeList = null;
    }
  }

  private getYearList() {
    this.listYear = [];
    const currentYear = new Date().getFullYear();
    for (let i = (currentYear - 50) ; i <= (currentYear + 50) ; i++ ) {
      const obj = {
        year: i
      };
      this.listYear.push(obj);
    }
    return this.listYear;
  }

  get f() {
    return this.formSearch.controls;
  }

  actionSearch(event?) {
    if (!CommonUtils.isValidForm(this.formSearch)) {
      return;
    }
    const params = this.formSearch ? this.formSearch.value : null;
    this.dataInputProcessService.search(params, event).subscribe(res => {
      this.resultList = res;
      let index = 0;
      this.resultList.data.forEach(element => {
        element.idTable = index;
        index++;
      });
      this.showSearchData = true;
    });
    if (!event) {
      if (this.dataTable) {
        this.dataTable.first = 0;
      }
    }
  }

  actionDelete(){
    if (!CommonUtils.isValidForm(this.formSearch)) {
      return;
    }
    const params = this.formSearch ? this.formSearch.value : null;
    this.app.confirmDelete(null, () => {// on accepted
      this.dataInputProcessService.deleteData(params).subscribe(res=>{
        if(res){
          this.actionSearch();
        }
      })
    }, ()=> {

    });
  }

  onRowEditInit(item: any) {
    this.clonedData[item.idTable] = {...item};
  }

  onRowEditSave(item: any) {
    this.clonedData[item.idTable] = item;
    this.resultList.data[item.idTable] = this.clonedData[item.idTable];
    this.dataInputProcessService.updateProcessSearch(this.resultList.data[item.idTable]).subscribe(arg => {});
    delete this.clonedData[item.idTable];
  }

  onRowEditCancel(item: any) {
    this.resultList.data[item.idTable] = this.clonedData[item.idTable];
    delete this.clonedData[item.idTable];
  }

  actionPrepareImport() {
    const modalRef = this.modalService.open(DataInputProcessImportComponent, DEFAULT_MODAL_OPTIONS);
    modalRef.componentInstance.setFormValue(this.propertyConfigs, this.formSearch.value);
    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
      this.actionSearch();
    });
  }

  getStyle(item) {
    if (item.attributeType === '4' || item.attributeType === '5' ) {
      return 'vt-align-right';
    } else if (item.attributeType === '6') {
      return '';
    } else {
      return 'vt-align-center';
    }
  }

  public onLineOrgChange() {
    this.formSearch.get('dataTypeId').setValue(null);
    this.formSearch.get('periodId').setValue(null);
    if (!this.f.organizationId.value) {
      this.dataTypeList = [];
      this.periodIdList = [];
      return;
    }
    this.salaryOrganizationMappingService.getDataFromConfig(this.f.organizationId.value, this.f.salaryTypeId.value).subscribe(res => {
      this.dataTypeList = res.data;
    });
    this.getPeriod(this.f.organizationId.value, this.formSearch.value.year);
  }
  private onChangeYear(event) {
    this.getPeriod(this.formSearch.value.organizationId, event);
  }

  getPeriod(organizationId?, year?): void {
    this.periodBeanForm.controls['organizationId'].setValue(organizationId);
    this.periodBeanForm.controls['year'].setValue(year);
    this.periodService.getDataByOrgIdAndYear(this.periodBeanForm.value).subscribe(res => {
      res.data.forEach( x => {
        x.periodName = x.periodName + ' (' +
          moment(new Date(x.startDate)).format(CommonUtils.getDateFormat().toUpperCase()) + ' - ' +
          moment(new Date(x.endDate)).format(CommonUtils.getDateFormat().toUpperCase()) + ')';
      });
      this.periodIdList = res.data;
    });
  }
}
