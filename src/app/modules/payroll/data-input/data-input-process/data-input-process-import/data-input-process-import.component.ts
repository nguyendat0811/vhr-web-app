import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RESOURCE, ACTION_FORM, DataInputProcessService, SalaryOrganizationMappingService } from '@app/core';
import * as moment from 'moment';
import { FormGroup, Validators } from '@angular/forms';
import { AppComponent } from '@app/app.component';
import { CommonUtils, ValidationService } from '@app/shared/services';
import { FileControl } from '@app/core/models/file.control';
import { PeriodService } from '@app/core/services/hr-payroll/period.serviece';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'data-input-process-import',
  templateUrl: './data-input-process-import.component.html'
})
export class DataInputProcessImportComponent extends BaseComponent implements OnInit {
  public moment = moment;
  public formImport: FormGroup;
  periodBeanForm: FormGroup;
  dataError: any;
  showButtonImport = false;
  listYear: Array<any>;
  dataTypeList: [];
  periodIdList: [];
  private formImportConfig = {
    organizationId: [null, [ValidationService.required]],
    dataTypeId: [null, [ValidationService.required]],
    year: [parseInt(moment(new Date()).format('YYYY'))],
    periodId: [null, [ValidationService.required]]
  };
  periodBean = {
    organizationId: [null],
    year: [null],
  };
  constructor(public actr: ActivatedRoute
            , private service: DataInputProcessService
            , private router: Router
            , private periodService: PeriodService
            , private salaryOrganizationMappingService: SalaryOrganizationMappingService
            , private app: AppComponent
            , public activeModal: NgbActiveModal) {
    super(actr, RESOURCE.DATA_INPUT_PROCESS, ACTION_FORM.IMPORT);
    this.formImport = this.buildForm({}, this.formImportConfig, ACTION_FORM.IMPORT);
    this.formImport.addControl('fileImport', new FileControl(null, ValidationService.required));
  }

  ngOnInit() {
  }

  get f() {
    return this.formImport.controls;
  }
  actionDownloadTemplate() {
    const objdata = {
      organizationId: parseInt(this.f.organizationId.value),
      dataTypeId: parseInt(this.f.dataTypeId.value),
      periodId: parseInt(this.f.periodId.value),
      year: parseInt(this.f.year.value),
    }
    this.service.downloadTemplateImport(objdata).subscribe(res => {
      saveAs(res, 'data_input_import.xls');
    });
  }
  processValidateImport() {
    this.formImport.controls['fileImport'].updateValueAndValidity();
    if (!CommonUtils.isValidForm(this.formImport)) {
      return;
    }
    this.service.processValidateImport(this.formImport.value).subscribe(res => {
      if (!this.service.requestIsSuccess(res)) {
        this.dataError = res.data;
        this.showButtonImport = false;
      } else {
        this.dataError = null;
        this.showButtonImport = true;
      }
    });
  }

  processImport() {
    this.formImport.controls['fileImport'].updateValueAndValidity();
    if (!CommonUtils.isValidForm(this.formImport)) {
      return;
    }
    this.app.confirmMessage(null, () => {// on accepted
      this.service.processImport(this.formImport.value).subscribe(res => {
        if (!this.service.requestIsSuccess(res)) {
          this.dataError = res.data;
          this.showButtonImport = false;
        } else {
          this.dataError = null;
          this.activeModal.close(res);
        }
      });
    }, null);
  }

  public setFormValue(propertyConfigs: any, data?: any) {
    this.formImport = this.buildForm(data, this.formImportConfig, ACTION_FORM.IMPORT);
    this.formImport.addControl('fileImport', new FileControl(null, ValidationService.required));
  }
}
