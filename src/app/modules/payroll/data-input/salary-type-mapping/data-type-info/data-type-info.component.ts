import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { SystemParameterService, RESOURCE, ACTION_FORM, APP_CONSTANTS, DataTypeService, SYSTEM_PARAMETER_CODE } from '@app/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from '@app/app.component';

@Component({
  selector: 'data-type-info',
  templateUrl: './data-type-info.component.html'
})
export class DataTypeInfoComponent extends BaseComponent implements OnInit {
  formDataType: FormGroup;
  lstSpec: [];
  listGroupData: any;
  formDataTypeConfig = {
    dataTypeId: [''],
    dataName: [''],
    transferService: [''],
    isImport: [''],
    isPutMainTable: [''],
    isSplitProcess: [''],
    systemParameterId: ['']
  };
  editting: boolean;
  constructor(private activatedRoute: ActivatedRoute
    , public activeModal: NgbActiveModal
    , private app: AppComponent
    , private systemParameterService: SystemParameterService
    , private dataTypeService: DataTypeService) {
    super(activatedRoute, RESOURCE.DATA_TYPE, ACTION_FORM.INSERT);
    this.formDataType = this.buildForm({}, this.formDataTypeConfig);
    this.editting = true;
  }

  ngOnInit() {

  }
  public setFormValue(propertyConfigs: any, data?: any) {
    this.propertyConfigs = propertyConfigs;
    this.formDataType = this.buildForm(data, this.formDataTypeConfig);
    if (data.dataTypeId) {
      this.dataTypeService.findDataSpecification(data.dataTypeId).subscribe(res => {
        if (res && res.data) {
          this.lstSpec = res.data;
        }
      });
    }
  }

  public setListGroupData(data) {
    this.listGroupData = data;
  }

  renderAttributeType(type) {
    let attributeTypes = APP_CONSTANTS.ATTRIBUTE_TYPE;
    let data = attributeTypes.find(x => x.id === type);
    if (data) {
      return data.label;
    }
    return "";
  }

  renderGroupData(code) {
    if (this.listGroupData) {
      let data = this.listGroupData.find(x => x.code === code);
      if (data) {
        return data.value;
      }
    }
    let data = this.listGroupData.find(x => x.code === code);
    if (data) {
      return data.value;
    }
    return "";
  }

  editTranferservice() {
    this.editting = false;
  }
  cancelTranferservice() {
    this.editting = true;
  }
  saveTranferservice() {
    const formSystem = {
      systemParameterId: [''],
      description: ['']
    };
    formSystem['systemParameterId'] = this.formDataType.value.systemParameterId;
    formSystem['description'] = this.formDataType.value.transferService;
    this.app.confirmMessage(null, () => {
      this.systemParameterService.saveTranferSetrvice(formSystem)
        .subscribe(res => {
          if (this.systemParameterService.requestIsSuccess(res)) {
            this.cancelTranferservice();
          }
        });
    }, null);
  }
}
