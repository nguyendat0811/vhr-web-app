import { Component, OnInit, ViewChildren } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { AppComponent } from '@app/app.component';
import { RESOURCE, ACTION_FORM, SalaryTypeService, SalaryTypeMappingService, DataTypeService, SystemParameterService } from '@app/core';
import { CommonUtils } from '@app/shared/services';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { ValidationService } from '../../../../../shared/services/validation.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DEFAULT_MODAL_OPTIONS, SYSTEM_PARAMETER_CODE} from '@app/core/app-config';
import { DataTypeInfoComponent } from '../data-type-info/data-type-info.component';

@Component({
  selector: 'salary-type-mapping-index',
  templateUrl: './salary-type-mapping-index.component.html'
})
export class SalaryTypeMappingIndexComponent extends BaseComponent implements OnInit {

  @ViewChildren('inputSearch') inputSearch;
  formSave: FormGroup;
  formSaveConfig = {
    salaryTypeId: [null, [ValidationService.required]],
    dataTypeList: [null],
    nameFilter: [null]
  };

  nameFilter = '';
  listSalaryType = [];

  sourceDataTypes = [];
  listGroupData = [];

  targetDataTypesMain = [];
  targetDataTypesSync = [];
  targetDataTypesImport = [];

  constructor(
    private activatedRoute: ActivatedRoute,
    private app: AppComponent,
    private modalService: NgbModal,
    private salaryTypeService: SalaryTypeService,
    private salaryTypeMappingService: SalaryTypeMappingService,
    private dataTypeService: DataTypeService,
    private systemParameterService: SystemParameterService
  ) {
    super(activatedRoute, RESOURCE.SALARY_TYPE_MAPPING, ACTION_FORM.SALARY_TYPE_MAPPING_UPDATE);
    if (!this.hasPermission('action.view')) {
      return;
    }
    this.buildFormSave();
    this.salaryTypeService.getListActive().subscribe( res => {
      this.listSalaryType = res.data;
    });
  }

  ngOnInit() {
    this.sourceDataTypes = [];
    this.targetDataTypesMain = [];
    this.targetDataTypesSync = [];
    this.targetDataTypesImport = [];
  }

  get f () {
    return this.formSave.controls;
  }

  /**
   * Xu ly khi thay doi loai luong
   *
   */
  onchangeSalaryType(e) {
    if (e) {
      this.salaryTypeMappingService.getListDataTypeBySalary(e).subscribe(res => {
        this.sourceDataTypes = [];
        this.targetDataTypesMain = [];
        this.targetDataTypesSync = [];
        this.targetDataTypesImport = [];

        if (res.data && res.data.length > 0) {
          for (let i = 0; i < res.data.length; i++) {
            const obj = res.data[i];
            if (obj.isConfig === 1) {
              if (obj.configType === 1) {
                this.targetDataTypesMain.push(obj);
              } else if (obj.configType === 2) {
                this.targetDataTypesSync.push(obj);
              } else {
                this.targetDataTypesImport.push(obj);
              }

            } else {
              this.sourceDataTypes.push(obj);
            }
          }
        }
      });
    } else {
      this.sourceDataTypes = [];
      this.targetDataTypesMain = [];
      this.targetDataTypesSync = [];
      this.targetDataTypesImport = [];
    }
    this.focusInputSearch();
  }

  processSaveOrUpdate() {
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    }
    if (this.targetDataTypesMain.length === 0) {
      this.salaryTypeMappingService.processReturnMessage({type: 'WARNING', code: 'dataInputSync.mainProcessNotEmpty'});
      return;
    }
    this.app.confirmMessage(null, () => {
      this.f.dataTypeList.setValue(this.targetDataTypesMain.concat(this.targetDataTypesSync).concat(this.targetDataTypesImport));
      this.salaryTypeMappingService.saveOrUpdate(this.formSave.value)
        .subscribe(res => {
          this.onchangeSalaryType(this.f.salaryTypeId.value);
        });
    }, null);
  }

  processCancel() {
    this.buildFormSave();
  }

  /**
   * Xu ly build form save
   *
   */
  private buildFormSave(data?: any) {
    this.formSave = this.buildForm(data || {}, this.formSaveConfig, this.actionForm);
  }

  drop(event: CdkDragDrop<string[]>, dropPosition?: any) {
    // Xu ly validate
    if (event.previousContainer.id === 'cdk-drop-list-0') {
      event.previousIndex = this.findRealIndex(event.previousIndex);
    }
    const dataTypeMove = event.previousContainer.data[event.previousIndex];
    if (dataTypeMove['isImport'] === 1 && dropPosition === 2) {
      this.salaryTypeMappingService.processReturnMessage({type: 'WARNING', code: 'dataInputImport.mustConfigImport'});
      return;
    }

    if (dataTypeMove['isImport'] === 0 && dropPosition === 3) {
      this.salaryTypeMappingService.processReturnMessage({type: 'WARNING', code: 'dataInputSync.mustConfigSync'});
      return;
    }

    if (event.previousContainer === event.container) {
      if (event.previousContainer.id === 'cdk-drop-list-0') {
        event.currentIndex = this.findRealIndex(event.currentIndex);
      }
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);
    }

    // Xu ly set lai config type configType
    if (!dropPosition) {
      return;
    }
    if (dropPosition === 1) {
      event.container.data.forEach(x => {
        x['configType'] = 1;
      });
    } else if (dropPosition === 2) {
      event.container.data.forEach(x => {
        x['configType'] = 2;
      });
    } else if (dropPosition === 3) {
      event.container.data.forEach(x => {
        x['configType'] = 3;
      });
    }
  }

  private findRealIndex(tempIndex) {
    if (!this.nameFilter) {
      return tempIndex;
    }
    const listSearch = this.sourceDataTypes.filter( x =>
      x['dataName'].toLowerCase().includes(this.nameFilter.toLowerCase())
    );

    const object = listSearch[tempIndex];
    return this.sourceDataTypes.findIndex( x => x['dataTypeId'] === object['dataTypeId'] );
  }

    // check foucus
    focusInputSearch() {
      setTimeout(() => {
        this.inputSearch.first.nativeElement.focus();
      }, 100);
    }

    viewDataTypeInfo(event) {
      const modalRef = this.modalService.open(DataTypeInfoComponent, DEFAULT_MODAL_OPTIONS);
      this.dataTypeService.getBeanById(event.dataTypeId).subscribe(res => {
        this.systemParameterService
        .findEffectiveParamByName(SYSTEM_PARAMETER_CODE.GROUP_DATA).subscribe(res =>{
          modalRef.componentInstance.setListGroupData(res.data);
        });
        modalRef.componentInstance.setFormValue(this.propertyConfigs, res.data);
      });
    }
}
