import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SalaryTypeMappingIndexComponent } from './salary-type-mapping/salary-type-mapping-index/salary-type-mapping-index.component';
import { DataInputProcessSearchComponent } from './data-input-process/data-input-process-search/data-input-process-search.component';
import { DataInputProcessImportComponent } from './data-input-process/data-input-process-import/data-input-process-import.component';
import { SalaryOrganizationMappingIndexComponent } from './salary-organization-mapping/salary-organization-mapping-index/salary-organization-mapping-index.component';
import { SalaryTypeOrganizationMappingIndexComponent } from './salary_type_organization_mapping/salary-type-organization-mapping-index/salary-type-organization-mapping-index.component';

const routes: Routes = [
  {
    path: 'config'
    , component: SalaryTypeMappingIndexComponent
  },
  {
    path: 'salary-organization-mapping'
    , component: SalaryOrganizationMappingIndexComponent
  },
  {
    path: 'import',
    component: DataInputProcessImportComponent
    },
  {
    path: 'process-search'
    , component: DataInputProcessSearchComponent
  },
  {
    path: 'data-input-type',
    loadChildren: './data-input-type/data-input-type.module#DataInputTypeModule'
  },
  {
    path: 'salary-type-organization-mapping'
    , component: SalaryTypeOrganizationMappingIndexComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DataInputRoutingModule { }
