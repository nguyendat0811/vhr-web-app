import { Component, OnInit, ViewChildren } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { RESOURCE, ACTION_FORM, DataTypeService, SalaryTypeMappingService, SalaryOrganizationMappingService } from '@app/core';
import { ActivatedRoute } from '@angular/router';
import { AppComponent } from '@app/app.component';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { CommonUtils, ValidationService } from '@app/shared/services';

@Component({
  selector: 'salary-organization-mapping-index',
  templateUrl: './salary-organization-mapping-index.component.html'
})
export class SalaryOrganizationMappingIndexComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  @ViewChildren('inputSearch') inputSearch;
  formSaveConfig = {
    organizationId: [null, [ValidationService.required]],
    dataTypeList: [null],
    nameFilter: [null],
    configFilter: [null]
  };
  nameFilter = '';
  configFilter = '';
  sourceDataTypes = [];
  targetDataTypesMain = [];

  constructor(
    private activatedRoute: ActivatedRoute,
    private dataTypeService: DataTypeService,
    private app: AppComponent,
    private salaryTypeMappingService: SalaryTypeMappingService,
    private salaryOrganizationMappingService: SalaryOrganizationMappingService
  ) {
    super(activatedRoute, RESOURCE.SALARY_ORGANIZATION_MAPPING, ACTION_FORM.SALARY_ORGANIZATION_MAPPING_UPDATE);
    if (!this.hasPermission('action.view')) {
      return;
    }
    this.buildFormSave();
    this.f.organizationId.valueChanges.subscribe(data => {
      this.onchangeSalaryOrganiztion(data);
    });
  }

  ngOnInit() {
    this.sourceDataTypes = [];
    this.targetDataTypesMain = [];
  }
  get f() {
    return this.formSave.controls;
  }
  /**
  * Xu ly build form save
  * @ param data
  */
  private buildFormSave(data?: any) {
    this.formSave = this.buildForm(data || {}, this.formSaveConfig, this.actionForm);
  }

  /**
  * drop
  */
  drop(event: CdkDragDrop<string[]>, dropPosition?: any) {
    event.previousIndex = this.findRealIndex(event.previousIndex, event.previousContainer.id);
    if (event.previousContainer === event.container) {
      event.currentIndex = this.findRealIndex(event.currentIndex, event.previousContainer.id);
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
  }

  /**
   */
  onchangeSalaryOrganiztion(orgId) {
    if (orgId) {
      this.salaryOrganizationMappingService.getData(orgId).subscribe(res => {
        this.targetDataTypesMain = res.data;
        this.sourceDataTypes = res.extendData.listDataImput;
      });
    } else {
      this.sourceDataTypes = [];
      this.targetDataTypesMain = [];
    }
    this.focusInputSearch();
  }

  processSaveOrUpdate() {
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    }
    this.app.confirmMessage(null, () => {
      this.f.dataTypeList.setValue(this.targetDataTypesMain);
      this.salaryOrganizationMappingService.saveOrUpdate(this.formSave.value)
        .subscribe(res => {
          if (this.salaryOrganizationMappingService.requestIsSuccess(res)) {
            this.onchangeSalaryOrganiztion(this.f.organizationId.value);
          }
        });
    }, null);
  }

  processCancel() {
    this.buildFormSave();
    this.f.organizationId.valueChanges.subscribe(data => {
      this.onchangeSalaryOrganiztion(data);
    });
  }

  private findRealIndex(tempIndex, previousContainerid) {
    let listSearch = {};
    let object = {};
    if (previousContainerid === 'cdk-drop-list-0') {
      if (!this.nameFilter) {
        return tempIndex;
      }
      listSearch = this.sourceDataTypes.filter(x =>
        x['dataName'].toLowerCase().includes(this.nameFilter.toLowerCase()));
      object = listSearch[tempIndex];
      return this.sourceDataTypes.findIndex(x => x['dataTypeId'] === object['dataTypeId']);
    } else {
      if (!this.configFilter) {
        return tempIndex;
      }
      listSearch = this.targetDataTypesMain.filter(x =>
        x['dataName'].toLowerCase().includes(this.configFilter.toLowerCase()));
      object = listSearch[tempIndex];
      return this.targetDataTypesMain.findIndex(x => x['dataTypeId'] === object['dataTypeId']);
    }
  }
  // check foucus
  focusInputSearch() {
    setTimeout(() => {
      this.inputSearch.first.nativeElement.focus();
    }, 100);
  }
}
