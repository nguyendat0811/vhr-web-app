import { DataInputTypeFormComponent } from './data-input-type-form/data-input-type-form.component';
import { DataInputTypeSearchComponent } from './data-input-type-search/data-input-type-search.component';
import { DataInputTypeIndexComponent } from './data-input-type-index/data-input-type-index.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@app/shared';
import { DataInputTypeRoutingModule } from './data-input-type-routing.module';

@NgModule({
  declarations: [
    DataInputTypeIndexComponent,
    DataInputTypeSearchComponent,
    DataInputTypeFormComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    DataInputTypeRoutingModule
  ]
})
export class DataInputTypeModule { }
