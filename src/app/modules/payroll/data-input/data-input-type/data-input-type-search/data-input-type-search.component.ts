import { CommonUtils } from '@app/shared/services/common-utils.service';
import { Component, OnInit } from '@angular/core';
import { SystemParameterService, DataTypeService, SYSTEM_PARAMETER_CODE } from '@app/core';
import { RESOURCE, ACTION_FORM } from '@app/core/app-config';
import { ValidationService } from '@app/shared/services';
import { AppComponent } from '@app/app.component';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';

@Component({
    selector: 'data-input-type-search',
    templateUrl: './data-input-type-search.component.html'
})

export class DataInputTypeSearchComponent extends BaseComponent implements OnInit {
  isImport = false;
  listTransferService: any;
  formConfig = {
    dataName: ['', [ValidationService.maxLength(200)]],
    transferService: [''],
    isImport: [''],
    isPutMainTable: [''],
    isSplitProcess: [''],
    specDataTypeCode: ['', [ValidationService.maxLength(200)]],
  };
  public commonUtils = CommonUtils;

  constructor(public actr: ActivatedRoute,
              public dataTypeService: DataTypeService,
              private systemParameterService: SystemParameterService,
              private app: AppComponent) {
          super(actr, RESOURCE.DATA_TYPE, ACTION_FORM.SEARCH);
          this.setMainService(dataTypeService);
          this.formSearch = this.buildForm({}, this.formConfig, ACTION_FORM.SEARCH);
          // List service đồng bộ
          this.systemParameterService.findEffectiveParamByName(SYSTEM_PARAMETER_CODE.TRANSFER_SERVICE)
          .subscribe(res => this.listTransferService = res.data);
  }
  ngOnInit() {
    this.processSearch();
  }

  get f () {
    return this.formSearch.controls;
  }

  isImportChecked() {
    this.formSearch.removeControl('transferService');
    this.formSearch.addControl('transferService', CommonUtils.createControl(this.actionForm, 'transferService', null, []));
  }

  processDelete(item) {
    if (item && item.dataTypeId > 0) {
      if (item.isLock) {
        return this.dataTypeService.processReturnMessage({type: 'WARNING', code: 'dataType.recordInUsed'});
      }
      this.app.confirmDelete(null, () => {
        // on accepted
        this.dataTypeService.deleteById(item.dataTypeId)
          .subscribe(
            res => {
              if (this.dataTypeService.requestIsSuccess(res)) {
                this.processSearch(null);
              }
            },
            error => {
              this.app.requestIsError();
            },
            () => {
              // No errors, route to new page
            }
          );
        }, () => {// on rejected
      });
    }
  }
}
