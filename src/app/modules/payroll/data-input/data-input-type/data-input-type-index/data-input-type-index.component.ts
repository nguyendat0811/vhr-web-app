import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { RESOURCE, ACTION_FORM } from '@app/core/app-config';

@Component({
  selector: 'data-input-type-index',
  templateUrl: './data-input-type-index.component.html'
})
export class DataInputTypeIndexComponent extends BaseComponent implements OnInit {

  constructor(public actr: ActivatedRoute) {
    super(actr, RESOURCE.DATA_TYPE, ACTION_FORM.SEARCH);
  }

  ngOnInit() {
  }
}
