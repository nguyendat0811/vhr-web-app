import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { Component, OnInit, OnDestroy, Input, AfterViewInit } from '@angular/core';
import { FormGroup, Validators, FormArray, AbstractControl } from '@angular/forms';
import { AppComponent } from '@app/app.component';
import { ValidationService } from '@app/shared/services/validation.service';
import { SystemParameterService, RESOURCE, ACTION_FORM, APP_CONSTANTS, DataTypeService, SYSTEM_PARAMETER_CODE } from '@app/core';
import { HelperService } from '@app/shared/services/helper.service';

@Component({
  selector: 'data-input-type-form',
  templateUrl: './data-input-type-form.component.html',
})
export class DataInputTypeFormComponent extends BaseComponent implements OnInit, OnDestroy, AfterViewInit {
  formDataType: FormGroup;
  formDataSpecification: FormArray;
  listTransferService: any;
  listAttributeType: any;
  listGroupData: any;
  listMainTableColumn: any;
  listSubTableColumn: any;
  private navigationSubscription;
  checkPermission = true;
  isPutMainTableOrig: any;
  loading = false;
  showQueryField = false;

  @Input() item: any;
  formDataTypeConfig = {
    dataTypeId: [''],
    dataName: ['', [ValidationService.required, ValidationService.maxLength(200)]],
    transferService: ['', [ValidationService.required]],
    query: [''],
    isImport: [0],
    isPutMainTable: [0],
    isSplitProcess: [0],
    isMaxAllowance: [0],
    isSumAllowance: [0],
    mappingCode: ['', [ValidationService.required]],
  };

  formDataSpecConfig = {
    dataTypeId: [''],
    dataTypeSpecificationId: [''],
    columnOutput: ['', [ValidationService.required, ValidationService.maxLength(50)]],
    // identifier: ['', [ValidationService.required, ValidationService.dbTableColumnName, ValidationService.maxLength(50)]],
    attributeType: ['', [ValidationService.required]],
    mappingColumnOutput: ['', [ValidationService.maxLength(50)]],
    resourceKey: ['', [ValidationService.required, ValidationService.maxLength(200)]],
    groupData: ['', [ValidationService.maxLength(200)]],
    isRequired: [''],
    isPrimary: [''],
    description: ['', [ValidationService.maxLength(1000)]]
  };

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private helperService: HelperService,
              private dataTypeService: DataTypeService,
              private systemParameterService: SystemParameterService,
              private app: AppComponent) {
    super(activatedRoute, RESOURCE.DATA_TYPE, ACTION_FORM.INSERT);
    this.formDataType = this.buildForm({ isPutMainTable: 1 }, this.formDataTypeConfig);
    this.buildFormDataSpecification({});
    this.loadListSystemParam();
    this.navigationSubscription = this.router.events.subscribe((event: any) => {
      if (event instanceof NavigationEnd) {
        const params = this.activatedRoute.snapshot.params;
        if (params && CommonUtils.isValidId(params.dataTypeId)) {
          this.f.dataTypeId.setValue(params.dataTypeId);
        }
        this.prepareInsertOrUpdate();
      }
    });
  }

  public static validateRequired(isImport: AbstractControl) {
    return (c: AbstractControl): any | null => {
      if (!isImport || isImport.value) {
        return;
      }
      if (!c.value) {
        return {required: true};
      }
      return;
    };
  }

  ngOnDestroy() {
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
  }

  ngAfterViewInit() {
  }

  loadListSystemParam() {
    // List service đồng bộ
    this.systemParameterService.findEffectiveParamByName(SYSTEM_PARAMETER_CODE.TRANSFER_SERVICE)
    .subscribe(res => this.listTransferService = res.data);
    // List group data
    this.systemParameterService.findEffectiveParamByName(SYSTEM_PARAMETER_CODE.GROUP_DATA)
    .subscribe(res => this.listGroupData = res.data);
    this.listAttributeType = APP_CONSTANTS.ATTRIBUTE_TYPE;

    this.dataTypeService.findListMainTableColumnName()
    .subscribe(res => {
      this.listMainTableColumn = res.data;
    });

    this.dataTypeService.findListSubTableColumnName()
    .subscribe(res => {
      this.listSubTableColumn = res.data;
    });
  }

  /**
   * processSaveOrUpdate
   */
  processSaveOrUpdate() {
    if (!this.validateBeforeSave()) {
      return;
    }

    const formSave = {
      dataTypeId: [''],
      dataName: [''],
      transferService: [''],
      query: [''],
      isImport: [''],
      isPutMainTable: [],
      isSplitProcess: [''],
      lstDataSpecification: [''],
      isMaxAllowance: [],
      isSumAllowance: []
    };
    CommonUtils.copyProperties(formSave, this.formDataType.value);
    formSave['lstDataSpecification'] = this.formDataSpecification.value;
    this.app.confirmMessage(null, () => {
      // On accepted
      this.dataTypeService.saveOrUpdate(formSave)
        .subscribe(res => {
          if (this.dataTypeService.requestIsSuccess(res)) {
            this.router.navigate(['/payroll/data-input/data-input-type']);
          }
        });
    }, () => {
      // on rejected
    });
  }

  get f () {
    return this.formDataType.controls;
  }

  /**
   * validate before save
   */
  private validateBeforeSave(): boolean {
    const formDataTypeState = CommonUtils.isValidForm(this.formDataType);
    const formDataSpecificationState = CommonUtils.isValidForm(this.formDataSpecification);
    return formDataTypeState && formDataSpecificationState;
  }

  /**
   * setFormValue
   * param: Array propertyConfigs
   * param data
   */
  public prepareInsertOrUpdate() {
    if (this.f.dataTypeId.value) {
      if (this.hasPermission('action.update')) {
        this.checkPermission = true;
        this.dataTypeService.findOne(this.f.dataTypeId.value)
        .subscribe(res => {
          if (this.dataTypeService.requestIsSuccess(res)) {
            this.formDataType = this.buildForm(res.data, this.formDataTypeConfig, ACTION_FORM.UPDATE);
            this.isPutMainTableOrig = res.data.isPutMainTable === 1 ? true : false;
            this.loadDataSpecByDataTypeId();
            this.requireTransferService();
          }
        });
      } else {
        this.checkPermission = false;
        return;
      }
    } else {
      if (this.hasPermission('action.insert')) {
        this.checkPermission = true;
        this.formDataType = this.buildForm({ isPutMainTable: 1 }, this.formDataTypeConfig, ACTION_FORM.INSERT);
        this.loadDataSpecByDataTypeId();
      } else {
        this.checkPermission = false;
        return;
      }
    }
  }

  isPutMainTableChecked() {
    if (this.f.isPutMainTable.value !== this.isPutMainTableOrig) {
      // Xu ly truong hop edit, dang tu bang chinh chuyen sang bang phu hoac nguoc lai
      this.buildFormDataSpecification({});
      const controls = this.formDataSpecification as FormArray;
      controls.insert(0, this.makeDefaultDataSpecForm());
    } else {
      this.loadDataSpecByDataTypeId();
    }
  }

  requireTransferService() {
    const transferService = this.formDataType.value.transferService;
    if (this.f['isImport'].value) {
      // Truong hop cho phep import
      this.formDataType.removeControl('transferService');
      this.formDataType.addControl('transferService', CommonUtils.createControl(this.actionForm, 'transferService',
                      '', []));
    } else {
      // Truong hop khong cho phep import
      this.formDataType.removeControl('transferService');
      this.formDataType.addControl('transferService', CommonUtils.createControl(this.actionForm, 'transferService',
                      transferService, [ValidationService.required]));
    }
  }

  isImportChecked() {
    // require transferService if is import
    this.requireTransferService();

    // Rebuild table, remove columnJson if is import
    // this.dataTypeService.findDataSpecification(this.f['dataTypeId'].value)
    // .subscribe((res) => {
    //   if (this.dataTypeService.requestIsSuccess(res)) {
    //     this.buildFormDataSpecification(this.formDataSpecification.value);
    //   }
    // });
  }

  onBlurColumnOutput(event, item) {
    if (event) {
      item.controls.columnOutput.setValue(event);
      // item.controls.identifier.setValue(event);
      // Generate columnJson
      // let arr: [''];
      // arr = event.split('_');
      // let columnJson = arr[0].toLowerCase();
      // for (let i = 1; i < arr.length; i++) {
      //   columnJson += arr[i].charAt(0).toUpperCase() + arr[i].substr(1).toLowerCase();
      // }
      // item.controls.columnJson.setValue(columnJson);
      if (this.f['isPutMainTable'].value) {
        const selectedValue = this.listMainTableColumn.filter(x => x.columnName === event);
        if (selectedValue.length > 0 && selectedValue[0].columnComment) {
          item.controls.resourceKey.setValue(selectedValue[0].columnComment);
        } else {
          item.controls.resourceKey.setValue(event);
        }
      } else {
        const selectedValue = this.listSubTableColumn.filter(x => x.columnName === event);
        if (selectedValue.length > 0 && selectedValue[0].columnComment) {
          item.controls.resourceKey.setValue(selectedValue[0].columnComment);
        } else {
          item.controls.resourceKey.setValue(event);
        }
      }
    }
  }

  // Xử lý load danh sách data specification
  loadDataSpecByDataTypeId() {
    this.dataTypeService.findDataSpecification(this.f['dataTypeId'].value)
    .subscribe((res) => {
      if (this.dataTypeService.requestIsSuccess(res)) {
        this.buildFormDataSpecification(res.data);
      }
    });
  }

  private buildFormDataSpecification(listDataSpecification: any) {
    this.loading = true;
    if (!listDataSpecification) {
      this.formDataSpecification = new FormArray([]);
    } else {
      const controls = new FormArray([]);
      for (const i in listDataSpecification) {
        const dataSpecification = listDataSpecification[i];
        const group = this.makeDefaultDataSpecForm();
        group.patchValue(dataSpecification);
        controls.push(group);
      }
      controls.setValidators(
        [
          ValidationService.duplicateArray(['columnOutput'], 'columnOutput', 'payroll.dataInputType.columnOutput'),
          // ValidationService.duplicateArray(['identifier'], 'identifier', 'payroll.dataInputType.identifier'),
          ValidationService.duplicateArray(['mappingColumnOutput'], 'mappingColumnOutput', 'payroll.dataInputType.mappingColumnOutput'),
          ValidationService.duplicateArray(['resourceKey'], 'resourceKey', 'payroll.dataInputType.resourceKey')
        ]);
      this.formDataSpecification = controls;
    }
    this.loading = false;
  }

  /**
   * makeDefaultDataSpecForm
   */
  private makeDefaultDataSpecForm(): FormGroup {
    const formGroup = this.buildForm({}, this.formDataSpecConfig, ACTION_FORM.DATA_SPECIFICATION_UPDATE);
    // Thuc hien validate require cot columnJson khi khong cho phep import
    // if (this.f['isImport'].value) {
      // Truong hop cho phep import
      // formGroup.removeControl('columnJson');
      // formGroup.addControl('columnJson', CommonUtils.createControl(this.actionForm, 'columnJson', '',
      //   [ValidationService.onlyLetterNumber, ValidationService.maxLength(50)]));
    // } else {
      // Truong hop khong cho phep import
      // formGroup.removeControl('columnJson');
      // formGroup.addControl('columnJson', CommonUtils.createControl(this.actionForm, 'columnJson', '',
      //   [ValidationService.required, ValidationService.onlyLetterNumber, ValidationService.maxLength(50)]));
    // }
    return formGroup;
  }

  /**
   * addDataSpecification
   * param index
   * param item
   */
  public addDataSpecification(index: number, item: FormGroup) {
    const controls = this.formDataSpecification as FormArray;
    controls.insert(index + 1, this.makeDefaultDataSpecForm());
  }

  /**
   * removeDataSpecification
   * param index
   * param item
   */
  public removeDataSpecification(index: number, item: FormGroup) {
    const controls = this.formDataSpecification as FormArray;
    if (controls.length === 1) {
      this.buildFormDataSpecification({});
      const group = this.makeDefaultDataSpecForm();
      controls.push(group);
      this.formDataSpecification = controls;
    }
    controls.removeAt(index);
  }

  clickEventQueryField() {
    this.showQueryField = !this.showQueryField;
  }

  // sinh tu dong dinh nghi chi tiet loai du lieu
  generateSpecdata() {
    if (!CommonUtils.isNullOrEmpty(this.f.query.value)) {
      this.app.confirmMessage("payroll.dataInput.generateData.confirm", () => {
        this.dataTypeService.generateSpecdata(this.formDataType.value)
        .subscribe((res) => {
          if (this.dataTypeService.requestIsSuccess(res)) {
            this.buildFormDataSpecification(res.data);
          }
        });
      }, null);
    }
  }
}

