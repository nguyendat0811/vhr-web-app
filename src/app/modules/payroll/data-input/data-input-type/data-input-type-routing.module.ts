import { DataInputTypeFormComponent } from './data-input-type-form/data-input-type-form.component';
import { DataInputTypeIndexComponent } from './data-input-type-index/data-input-type-index.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RESOURCE } from '@app/core';
import { PropertyResolver } from '@app/shared/services/property.resolver';
import { CommonUtils } from '@app/shared/services';

const routes: Routes = [
  {
    path: '',
    component: DataInputTypeIndexComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.DATA_TYPE,
      nationId: CommonUtils.getNationId()
    }
  },
  {
    path: 'add',
    component: DataInputTypeFormComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.DATA_TYPE,
      nationId: CommonUtils.getNationId()
    }
  },
  {
    path: 'edit/:dataTypeId',
    component: DataInputTypeFormComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.DATA_TYPE,
      nationId: CommonUtils.getNationId()
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DataInputTypeRoutingModule { }
