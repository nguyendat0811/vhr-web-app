import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RESOURCE } from '@app/core';

import { BaseComponent } from './../../../../../../shared/components/base-component/base-component.component';

@Component({
  selector: 'config-index',
  templateUrl: './config-index.component.html'
})
export class ConfigIndexComponent extends BaseComponent implements OnInit {

  constructor( public actr: ActivatedRoute) {
    super(actr, RESOURCE.FORMULA_CONFIG);
  }

  ngOnInit() {
  }

}
