import { HelperService } from '@app/shared/services/helper.service';
import { FormulaConfigService } from './../../../../../../core/services/hr-payroll/formula-config.service';
import { AppComponent } from '@app/app.component';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { Component, OnInit } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { RESOURCE, ACTION_FORM } from '@app/core';
import { ValidationService } from '@app/shared/services/validation.service';

@Component({
  selector: 'config-detail-history-search',
  templateUrl: './config-detail-history-search.component.html'
})
export class ConfigDetailHistorySearchComponent extends BaseComponent implements OnInit {

  salaryTypeList: any = {};
  statusList: any = {};
  formulaUsed = new Array();
  name: any;
  formConfig = {
    variableName: [null, [ValidationService.maxLength(200)]],
    dfFormulaId: [ValidationService.required],
  };
  constructor(
      public actr: ActivatedRoute
    , private modalService: NgbModal
    , public activeModal: NgbActiveModal
    , private app: AppComponent
    , private formulaConfigService: FormulaConfigService
    , private helperService: HelperService
  ) {
    super(actr, RESOURCE.FORMULA_CONFIG, ACTION_FORM.SEARCH);
    this.formSearch = this.buildForm({}, this.formConfig);
   }

  ngOnInit() {
    //this.processSearchHistory(null);
  }

  get f () {
    return this.formSearch.controls;
  }

  processSearchHistory(event?: any){
    if(!this.formSearch.valid){
      return;
    }
    this.formulaConfigService.processSearchHistory(this.formSearch.value, event).subscribe(res=>{
      this.resultList = res;
    });
    if (!event) {
      if (this.dataTable) {
        this.dataTable.first = 0;
      }
    }
  }

  public setFormValue(data){
    this.formSearch.controls.dfFormulaId.setValue(data);
    this.processSearchHistory(null);
  }

  public setNameForm(data){
    this.name = data;
  }
  
}
