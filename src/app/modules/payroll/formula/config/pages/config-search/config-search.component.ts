import { HelperService } from '@app/shared/services/helper.service';
import { FormulaDiagramService } from './../../../../../../core/services/hr-payroll/formula-diagram.service';
import { DEFAULT_MODAL_OPTIONS } from './../../../../../../core/app-config';
import { ConfigFormComponent } from './../config-form/config-form.component';
import { FormulaConfigService } from './../../../../../../core/services/hr-payroll/formula-config.service';
import { AppComponent } from '@app/app.component';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RESOURCE, ACTION_FORM, SalaryTypeService, APP_CONSTANTS } from '@app/core';
import { ValidationService } from '@app/shared/services/validation.service';
import { TranslationService } from 'angular-l10n';
import { ConfigDetailHistorySearchComponent } from '../config-detail-history/config-detail-history-search.component';

@Component({
  selector: 'config-search',
  templateUrl: './config-search.component.html'
})
export class ConfigSearchComponent extends BaseComponent implements OnInit {

  salaryTypeList: any = {};
  statusList: any = {};
  formulaUsed = new Array();
  formConfig = {
    name: [null, [ValidationService.maxLength(200)]],
    salaryTypeId: [null],
  };
  constructor(
      public actr: ActivatedRoute
    , private modalService: NgbModal
    , private translation: TranslationService
    , private salaryTypeService: SalaryTypeService
    , private app: AppComponent
    , private formulaConfigService: FormulaConfigService
    , private formulaDiagramService: FormulaDiagramService
    , private helperService: HelperService
  ) {
    super(actr, RESOURCE.FORMULA_CONFIG, ACTION_FORM.SEARCH);
     this.setMainService(formulaConfigService);
     this.formSearch = this.buildForm({}, this.formConfig);
     this.salaryTypeService.getListActive()
      .subscribe(arg => this.salaryTypeList = arg.data);
     this.statusList = [this.translation.translate('payroll.formula.diagram.inactive'),
                        this.translation.translate('payroll.formula.diagram.active')];
     this.formulaDiagramService.findAllDfFormulaId()
       .subscribe(arg => this.formulaUsed = arg.data);
   }

  ngOnInit() {
    this.processSearch();
  }

  get f () {
    return this.formSearch.controls;
  }

  onSalaryTypeChange(event) {
    this.formSearch.value.salaryTypeId = event;
  }

  prepareSaveOrUpdate(item) {
    const modalRef = this.modalService.open(ConfigFormComponent, DEFAULT_MODAL_OPTIONS);
    if (item && item.dfFormulaId > 0) {
      this.formulaConfigService.findOne(item.dfFormulaId)
        .subscribe(res => {
          modalRef.componentInstance.setFormValue(this.propertyConfigs, res.data);
        });
    } else {
      modalRef.componentInstance.setFormValue(this.propertyConfigs);
    }

    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
      if (this.formulaConfigService.requestIsSuccess(result)) {
        this.processSearch();
      }
    });
  }

  changeStatus (item) {
    if (this.hasPermission('action.update')) {
      if (this.formulaUsed.includes(item.dfFormulaId)) {
        this.helperService.APP_TOAST_MESSAGE.next({type: 'WARNING', code: 'formula.config.cannotChange'});
      } else {
        this.app.confirmMessage('payroll.formula.config.confirmStatus',
        () => {
          if (item.isActive === '1') {
            item.isActive = 0;
          } else {
            item.isActive = 1;
          }
          this.formulaConfigService.updateStatusById(item.dfFormulaId, item.isActive)
          .subscribe(res => {
            if (this.formulaConfigService.requestIsSuccess(res)) {
              this.processSearch(null);
            }
          });
        },
        () => {});
      }
    }
    
  }
  /**
   * prepareDelete
   * param item
   */
  processDelete(item) {
    if (item && item.dfFormulaId > 0) {
      if (this.formulaUsed.includes(item.dfFormulaId)) {
        this.helperService.APP_TOAST_MESSAGE.next({type: 'WARNING', code: 'formula.config.cannotDelete'});
      } else {
        this.app.confirmDelete(null, () => {// on accepted
          this.formulaConfigService.deleteById(item.dfFormulaId)
          .subscribe(res => {
            if (this.formulaConfigService.requestIsSuccess(res)) {
              this.processSearch(null);
            }
          });
        }, () => {// on rejected
        });
      }
    }
  }

  viewHistory(item){
    if(item){
      const modalRef = this.modalService.open(ConfigDetailHistorySearchComponent, DEFAULT_MODAL_OPTIONS);
      if (item) {
        let dfFormulaId = item.dfFormulaId;
        modalRef.componentInstance.setFormValue(dfFormulaId);
        modalRef.componentInstance.setNameForm(item.name);
      }
      modalRef.result.then((result) => {
        if (!result) {
          return;
        }
      });
    }
    
  }

}
