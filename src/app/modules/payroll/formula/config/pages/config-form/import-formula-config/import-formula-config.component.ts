import { RESOURCE, ACTION_FORM } from '@app/core/app-config';
import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponent } from '@app/app.component';
import { FormulaConfigService } from './../../../../../../../core/services/hr-payroll/formula-config.service';
import { FormGroup, Validators } from '@angular/forms';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';

import { FileControl } from '@app/core/models/file.control';
import { CommonUtils } from '@app/shared/services';
import { saveAs } from 'file-saver';
import { ValidationService } from '../../../../../../../shared/services/validation.service';

@Component({
  selector: 'import-formula-config',
  templateUrl: './import-formula-config.component.html',
})

export class ImportFormulaConfigComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  dataError: any;
  showButtonImport = false;
  formConfig = {
    dfFormulaId: [''],
    salaryTypeId: [''],
  };
  result: any;
  constructor(
    public activatedRoute: ActivatedRoute,
    private app: AppComponent,
    private router: Router,
    public activeModal: NgbActiveModal,
    private formulaConfigService: FormulaConfigService,
  ) {
    super(activatedRoute, RESOURCE.FORMULA_CONFIG, ACTION_FORM.INSERT);
    this.formSave = this.buildForm({}, this.formConfig, ACTION_FORM.INSERT);
    this.formSave.addControl('fileImport', new FileControl(null, ValidationService.required));
  }

  ngOnInit() {
  }

  get f () {
    return this.formSave.controls;
  }

  setFormValue(data: any) {
    console.log('data', data)
    if (data.value.dfFormulaId) {
      this.formSave.get('dfFormulaId').setValue(data.value.dfFormulaId);
    }
    if (data.value.salaryTypeId) {
      this.formSave.get('salaryTypeId').setValue(data.value.salaryTypeId);
    }
  }

  /**
  * actionDownloadTemplate
  */
  actionDownloadTemplate() {
    this.formulaConfigService.processDownloadTemplateImport(this.formSave.get('salaryTypeId').value)
    .subscribe(res => {
      saveAs(res, 'formula_config_import.xls');
    });
  }

  /**
  * processImport
  */
  processImport() {
    this.formSave.controls['fileImport'].updateValueAndValidity();
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    }
    this.app.confirmMessage(null, () => {// on accepted
      this.formulaConfigService.processImport(this.formSave.value).subscribe(res => {
        if (!this.formulaConfigService.requestIsSuccess(res)) {
          this.dataError = res.data;
        } else {
          this.dataError = null;
          this.result = res;
          console.log('res: ', this.result)
        }
      });
    }, () => {
      // on rejected
    });
  }

  /**
  * processValidateImport
  */
  processValidateImport() {
    this.formSave.controls['fileImport'].updateValueAndValidity();
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    }
    this.formulaConfigService.processValidateImport(this.formSave.value).subscribe(res => {
      if (!this.formulaConfigService.requestIsSuccess(res)) {
        this.dataError = res.data;
        this.showButtonImport = false;
      } else {
        this.dataError = null;
        this.showButtonImport = true;
      }
    });
  }
}
