import { ValidationService } from '@app/shared/services/validation.service';
import { Component, OnInit, OnDestroy, ViewChild, EventEmitter, ViewChildren, Input, Output } from '@angular/core';
import { FormGroup, Validators, FormArray } from '@angular/forms';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { AppComponent } from '@app/app.component';
import { ACTION_FORM, RESOURCE, SalaryTypeService, DEFAULT_MODAL_OPTIONS, APP_CONSTANTS } from '@app/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormulaConfigService } from './../../../../../../core/services/hr-payroll/formula-config.service';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { PayrollItemService } from '@app/core/services/hr-payroll/payroll-item.service';
import { OverlayPanel } from 'primeng/overlaypanel';
import { ImportFormulaConfigComponent } from './import-formula-config/import-formula-config.component';
import { TranslationService } from 'angular-l10n';
import { FileControl } from '@app/core/models/file.control';

@Component({
  selector: 'config-form',
  templateUrl: './config-form.component.html'
})
export class ConfigFormComponent extends BaseComponent implements OnInit, OnDestroy {

  sub: any;
  form: FormGroup;
  formImport: FormGroup;
  formDetail: FormArray;
  dfFormulaId: number;
  isActive: number;
  elementInformation: any = [];
  salaryTypeList: any = {};
  payrollItemList: any = {};
  elementTitle: String;
  elementIdentifier: String;
  elementAttributeType: String;
  elementResourceKey: String;
  elementColumn: String;
  salaryTypeId: any;
  id: any;
  salaryTypeIdOrig: any;
  showInfo = true;
  showElement = true;
  showFormula = true;
  // liên quan đến xử lsy file start
  public files: any;
  private validMaxSize = -1;
  private validType: string;
  @Output()
  public onFileChange: EventEmitter<any> = new EventEmitter<any>();
  public emptyFile = new File([''], '-', {type: 'vhr_stored_file'});
  dataError: any;
  result: any;
 // liên quan đến xử lsy file end

  formConfig = {
    name: ['', [ValidationService.required, ValidationService.maxLength(200)]],
    salaryTypeId: [null, [ValidationService.maxLength(20)]],
    description: ['', [ValidationService.maxLength(5000)]],
    isActive: [null],
    dfFormulaId: [null],
  };
  formImportConfig = {
    dfFormulaId: [''],
    salaryTypeId: [''],
  };

  formConfigDetail = {
    dfExpressionId: [null],
    payrollItemId: [null],
    variableName: ['', [ValidationService.maxLength(100), ValidationService.required]],
    expression: ['', [ValidationService.maxLength(1000), ValidationService.required]],
    description: ['', [ValidationService.maxLength(5000)]],
  };
  @ViewChildren('file') file;
  constructor(public activeModal: NgbActiveModal
    , private router: Router
    , public translation: TranslationService
    , private formulaConfigService: FormulaConfigService
    , private payrollItemService: PayrollItemService
    , private salaryTypeService: SalaryTypeService
    , private modalService: NgbModal
    , private activatedRoute: ActivatedRoute
    , public actr: ActivatedRoute
    , private app: AppComponent) {
    super(actr, RESOURCE.FORMULA_CONFIG, ACTION_FORM.FORMULA_CONFIG_INSERT);
    this.buildForms({}, ACTION_FORM.FORMULA_CONFIG_INSERT);
    this.buildConfigDetailReason();

    // build form import
    this.buildFormImport({}, ACTION_FORM.FORMULA_CONFIG_IMPORT);
    this.formImport.addControl('fileImport', new FileControl(null));
    this.validType = 'xls';
    this.validMaxSize = 10;

    this.sub = this.router.events.subscribe((e: any) => {
      if (e instanceof NavigationEnd) {
        const params = this.activatedRoute.snapshot.params;
        this.isActive = params.isActive;
        if (params.dfFormulaId && params.isActive) {
          this.dfFormulaId = params.dfFormulaId;
          this.formulaConfigService.findOne(this.dfFormulaId)
            .subscribe(res => {
              if (res.data) {
                this.buildForms(res.data, ACTION_FORM.FORMULA_CONFIG_UPDATE);
                this.salaryTypeIdOrig = res.data.salaryTypeId;
                this.onSalaryTypeChange(res.data.salaryTypeId);
                if(!res.data.salaryTypeId){
                  this.formulaConfigService.findByPayrollConfigId(CommonUtils.nvl(this.dfFormulaId))
                  .subscribe(arg => {
                    this.buildConfigDetailReason(arg.data);
                  });
                }
              }
          });
        } else {
          this.buildForms({}, ACTION_FORM.FORMULA_CONFIG_INSERT);
          if (params.isActive) {
            this.form.controls['isActive'].setValue(params.isActive);
          } else {
            this.form.controls['isActive'].setValue(1);
          }
        }
      }
    });
    this.salaryTypeService.getListActive()
      .subscribe(arg => this.salaryTypeList = arg.data);
    this.salaryTypeId = null;
    this.id = null;
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  addConfigDetail(index) {
    const controls = this.formDetail as FormArray;
    controls.insert(index + 1, this.makeConfigDetailForm());
  }

  removeConfigDetail(index) {
    const controls = this.formDetail as FormArray;
    if (controls.length === 1) {
      this.buildConfigDetailReason();
      const group = this.makeConfigDetailForm();
      controls.push(group);
      this.formDetail = controls;
    }
    controls.removeAt(index);
  }

  onSalaryTypeChange(event) {
    if (event != null) {
      this.formulaConfigService.findListElement(event)
      .subscribe(arg => {
        this.elementInformation = arg.data;
      });
      this.payrollItemService.findBySalaryTypeId(event)
        .subscribe(arg => {
          this.payrollItemList = arg.data;
          this.payrollItemList.forEach(element => {
            element.code = element.code.toUpperCase();
        });
      });

      if (this.form.controls['salaryTypeId'].value !== this.salaryTypeIdOrig) {
        this.buildConfigDetailReason({});
      } else {
        this.formulaConfigService.findByPayrollConfigId(CommonUtils.nvl(this.form.controls['dfFormulaId'].value))
        .subscribe(arg => {
          this.buildConfigDetailReason(arg.data);
        });
      }
    } else {
      this.elementInformation = [];
      this.buildConfigDetailReason([]);
    }
  }

  processSaveOrUpdate() {
    if (!this.validateBeforeSave()) {
      return;
    } else {
      const formSave = {};
      formSave['configForm'] = this.form.value;
      formSave['configDetailForms'] = this.formDetail.value;
      this.app.confirmMessage(null, () => {// on accepted
        this.formulaConfigService.saveOrUpdate(formSave)
          .subscribe(res => {
            if (this.formulaConfigService.requestIsSuccess(res)) {
              this.router.navigate(['/payroll/formula/config']);
            }
          });
      }, () => {
      });
    }
  }

  get f() {
    return this.form.controls;
  }

  /*
  * fa
  */
  get fa() {
    return this.formImport.controls;
  }

  // quoctv
  // Load ImportComponent
  loadImportModal() {
    if (!this.form.get('salaryTypeId').value) {
      this.app.warningMessage('configDetail', 'salaryTypeIdNotNull');
      return;
    }
    this.file.first.nativeElement.value = '';
    this.file.first.nativeElement.click();
  }

  public hoverElement(event, item, overlaypanel: OverlayPanel) {
    this.elementColumn = this.translation.translate('payroll.dataInputType.columnOutput') + ': ' + item.columnOutput;
    // this.elementIdentifier = this.translation.translate('payroll.dataInputType.identifier') + ': ' + item.identifier;
// tslint:disable-next-line: max-line-length
    this.elementAttributeType = this.translation.translate('payroll.dataInputType.attributeType') + ':  ' + this.ConvertAtributeType(item.attributeType);
    this.elementResourceKey = this.translation.translate('payroll.dataInputType.resourceKey') + ': ' + item.resourceKey;
    overlaypanel.toggle(event);
  }

  private ConvertAtributeType (attributeType) {
    switch (attributeType) {
      case 1: {
        return APP_CONSTANTS.ATTRIBUTE_TYPE[0].label;
      }
      case 2: {
        return APP_CONSTANTS.ATTRIBUTE_TYPE[1].label;
      }
      case 3: {
        return APP_CONSTANTS.ATTRIBUTE_TYPE[2].label;
      }
      case 4: {
        return APP_CONSTANTS.ATTRIBUTE_TYPE[3].label;
      }
      case 5: {
        return APP_CONSTANTS.ATTRIBUTE_TYPE[4].label;
      }
      case 6: {
        return APP_CONSTANTS.ATTRIBUTE_TYPE[5].label;
      }
    }
  }

 /*
  * validateBeforeSave
  */
  private validateBeforeSave(): boolean {
    const isValidFormulaForm = CommonUtils.isValidForm(this.form);
    const isValidFormulaDetailForm = CommonUtils.isValidForm(this.formDetail);
    return isValidFormulaForm && isValidFormulaDetailForm;
  }

  /*
  * buildConfigDetailReason
  */
  private buildConfigDetailReason(listConfigDetail?: any) {
    const controls = new FormArray([]);
    if (listConfigDetail && listConfigDetail.length > 0) {
      for (const i in listConfigDetail) {
        const contractDetail = listConfigDetail[i];
        const group = this.makeConfigDetailForm();
        group.patchValue(contractDetail);
        controls.push(group);
      }
    } else {
      const group = this.makeConfigDetailForm();
      controls.push(group);
    }
    controls.setValidators(
        [
          ValidationService.duplicateArray(['payrollItemId'], 'payrollItemId', 'payroll.formula.configDetail.payrollItemId'),
          ValidationService.duplicateArray(['variableName'], 'variableName', 'payroll.formula.configDetail.variableName')
        ]);
    this.formDetail = controls;
  }

  /*
  * buildForms
  */
  private buildForms(data?: any, actionForm?: ACTION_FORM) {
    this.form = this.buildForm(data, this.formConfig, actionForm);
  }

  /*
  * makeConfigDetailForm
  */
  private makeConfigDetailForm(): FormGroup {
    const formGroup = this.buildForm({}, this.formConfigDetail, ACTION_FORM.FORMULA_CONFIG_UPDATE);
    return formGroup;
  }

  /**
  * actionDownloadTemplate
  */
  actionDownloadTemplate() {
    if (!this.form.get('salaryTypeId').value) {
      this.app.warningMessage('configDetail', 'salaryTypeIdNotNull');
      return;
    }
    if (this.form.get('salaryTypeId').value) {
      this.salaryTypeId =  this.form.get('salaryTypeId').value;
    }
    this.id = CommonUtils.nvl(this.form.get('dfFormulaId').value, 0);
    this.formulaConfigService.processDownloadTemplateImport(this.salaryTypeId, this.id)
    .subscribe(res => {
      saveAs(res, 'formula_config_import.xls');
    });
  }

  private buildFormImport(data?: any, actionForm?: ACTION_FORM) {
    this.formImport = this.buildForm(data, this.formImportConfig, actionForm);
  }

  public onChangeFile(event) {
    const files = event.target.files[0];
    if (!this.isValidFile(files)) {
      return;
    }
    this.files = files;
    this.onFileChanged();
    this.processImport();
  }

  /**
   * isValidFile
   */
  public isValidFile(files): boolean {
    if (!files) {
      return true;
    }
    if (this.validMaxSize > 0) {
      if (CommonUtils.tctGetFileSize(files) > this.validMaxSize) {
        this.app.warningMessage('configDetail', 'maxFile');
        return false;
      }
    }
    if (!CommonUtils.isNullOrEmpty(this.validType)) {
      const fileName = files.name;
      const temp = fileName.split('.');
      const ext = temp[temp.length - 1].toLowerCase();
      const ruleFile = ',' + this.validType;
      if (ruleFile.toLowerCase().indexOf(ext) === -1) {
        this.app.warningMessage('configDetail', 'falseTypeFile');
        return false;
      }
    }
    return true;
  }

  /**
   * onFileChanged
   */
  public onFileChanged() {
    this.formImport.controls['fileImport'].setValue(this.files);
    if (this.files === this.emptyFile) {
      return;
    }
    this.onFileChange.emit(this.files);
    if (! this.files) {
      return;
    }
    const fNames = [];

    fNames.push(this.files.name);
  }

  /**
  * processImport
  */
  processImport() {
    this.formImport.controls['fileImport'].updateValueAndValidity();
    this.formImport.controls['salaryTypeId'].setValue(this.form.value.salaryTypeId);
    this.app.confirmMessage('app.common.comfirmImport', () => {// on accepted
      this.formulaConfigService.processImport(this.formImport.value).subscribe(res => {
        if (!this.formulaConfigService.requestIsSuccess(res)) {
          this.dataError = res.data;
        } else {
          this.dataError = null;
          this.result = res.data;
          this.buildConfigDetailReason(this.result);
        }
      });

    }, () => {
      // on rejected
    });
  }
}
