import { PropertyResolver, CommonUtils } from '@app/shared/services';
import { ConfigIndexComponent } from './pages/config-index/config-index.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RESOURCE } from '@app/core';
import { ConfigFormComponent } from './pages/config-form/config-form.component';

const routes: Routes = [
  {
    path: '',
    component: ConfigIndexComponent,
    resolve: {
    props: PropertyResolver
    },
    data: {
      resource: RESOURCE.FORMULA_CONFIG,
      nationId: CommonUtils.getNationId()
    }
  },
  {
    path: 'add',
    component: ConfigFormComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.FORMULA_CONFIG,
      nationId: CommonUtils.getNationId()
    }
  },
  {
    path: 'edit/:dfFormulaId/:isActive',
    component: ConfigFormComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.FORMULA_CONFIG,
      nationId: CommonUtils.getNationId()
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfigRoutingModule { }
