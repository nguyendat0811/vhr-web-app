import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from '@app/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConfigRoutingModule } from './config-routing.module';
import { ConfigFormComponent } from './pages/config-form/config-form.component';
import { ConfigIndexComponent } from './pages/config-index/config-index.component';
import { ConfigSearchComponent } from './pages/config-search/config-search.component';
import { ImportFormulaConfigComponent } from './pages/config-form/import-formula-config/import-formula-config.component';
import { ConfigDetailHistorySearchComponent } from './pages/config-detail-history/config-detail-history-search.component';

@NgModule({
  declarations: [ConfigFormComponent, ConfigIndexComponent, ConfigSearchComponent, ImportFormulaConfigComponent, ConfigDetailHistorySearchComponent],
  imports: [
    CommonModule,
    SharedModule,
    ConfigRoutingModule
  ],
  providers: [
    NgbActiveModal,
  ],
  entryComponents: [ImportFormulaConfigComponent, ConfigDetailHistorySearchComponent],
})
export class ConfigModule { }
