import { SharedModule } from '@app/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DiagramRoutingModule } from './diagram-routing.module';
import { DiagramIndexComponent } from './pages/diagram-index/diagram-index.component';
import { DiagramSearchComponent } from './pages/diagram-search/diagram-search.component';
import { DiagramFormComponent } from './pages/diagram-form/diagram-form.component';

@NgModule({
  declarations: [DiagramIndexComponent, DiagramSearchComponent, DiagramFormComponent],
  imports: [
    CommonModule,
    SharedModule,
    DiagramRoutingModule
  ],
  entryComponents: [DiagramFormComponent]
})
export class DiagramModule { }
