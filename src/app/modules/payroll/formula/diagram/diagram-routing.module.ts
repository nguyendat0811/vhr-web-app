import { PropertyResolver } from '@app/shared/services/property.resolver';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonUtils } from '@app/shared/services';
import { RESOURCE } from '@app/core';
import { DiagramIndexComponent } from './pages/diagram-index/diagram-index.component';

const routes: Routes = [
  {
    path: '',
    component: DiagramIndexComponent,
    resolve: {
    props: PropertyResolver
    },
    data: {
      resource: RESOURCE.PAYROLL_DIAGRAM,
      nationId: CommonUtils.getNationId()
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DiagramRoutingModule { }
