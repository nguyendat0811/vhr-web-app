import { Component, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AppComponent } from '@app/app.component';
import { ACTION_FORM, RESOURCE } from '@app/core';
import { ValidationService } from '@app/shared/services';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslationService } from 'angular-l10n';
import { DEFAULT_MODAL_OPTIONS } from './../../../../../../core/app-config';
import { FormulaDiagramService } from './../../../../../../core/services/hr-payroll/formula-diagram.service';
import { BaseComponent } from './../../../../../../shared/components/base-component/base-component.component';
import { DiagramFormComponent } from './../diagram-form/diagram-form.component';

@Component({
  selector: 'diagram-search',
  templateUrl: './diagram-search.component.html',
})
export class DiagramSearchComponent extends BaseComponent implements OnInit {
  statusList: any = {};
  formConfig = {
    organizationId: [null, [ValidationService.required]],
    payrollDiagramName: [null, [ValidationService.maxLength(100)]],
    status: [null, [ValidationService.maxLength(11)]],
  };
  checkFirstSearch = false;
  constructor(
    public actr: ActivatedRoute
   , private modalService: NgbModal
   , private app: AppComponent
   , private translation: TranslationService
   , private formulaDiagramService: FormulaDiagramService
  ) {
    super(actr, RESOURCE.PAYROLL_DIAGRAM, ACTION_FORM.SEARCH);
    this.setMainService(formulaDiagramService);
    this.formSearch = this.buildForm({}, this.formConfig);
    this.statusList = [{actionType: 0 , name: this.translation.translate('payroll.formula.diagram.inactive')},
                        {actionType: 1 , name: this.translation.translate('payroll.formula.diagram.active')}];
    this.f.organizationId.valueChanges.subscribe(res => {
      // Xu ly tim kiem khi man hinh co set gia tri mac dinh cho don vi
      if (res && !this.checkFirstSearch) {
        setTimeout(() => {
          this.processSearch();
        }, 100);
        this.checkFirstSearch = true;
      }
    });
  }

  onStatusChange(event) {
    this.formSearch.value.status = event;
  }
  ngOnInit() {
  }
  get f () {
    return this.formSearch.controls;
  }
  prepareSaveOrUpdate(item) {
    // const modalRef = this.modalService.open(DiagramFormComponent, DEFAULT_MODAL_OPTIONS);
    if (item && item.payrollDiagramId > 0) {
      this.formulaDiagramService.findOne(item.payrollDiagramId)
        .subscribe(res => {
          this.activeFormModal(this.modalService, DiagramFormComponent, res.data);
        });
    } else {
      this.activeFormModal(this.modalService, DiagramFormComponent, {});
    }
  }
    /**
   * prepareDelete
   * param item
   */
  processDelete(item) {
    if (item && item.payrollDiagramId > 0) {
      this.app.confirmDelete(null, () => {// on accepted
        this.formulaDiagramService.deleteById(item.payrollDiagramId)
        .subscribe(res => {
          if (this.formulaDiagramService.requestIsSuccess(res)) {
            this.processSearch(null);
          }
        });
      }, () => {// on rejected

      });
    }
  }


}
