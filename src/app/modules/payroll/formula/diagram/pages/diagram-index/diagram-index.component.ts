import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { Component, OnInit } from '@angular/core';
import { RESOURCE } from '@app/core';

@Component({
  selector: 'diagram-index',
  templateUrl: './diagram-index.component.html',
})
export class DiagramIndexComponent extends BaseComponent implements OnInit {

  constructor( public actr: ActivatedRoute) {
    super(actr, RESOURCE.PAYROLL_DIAGRAM);
  }

  ngOnInit() {
  }

}
