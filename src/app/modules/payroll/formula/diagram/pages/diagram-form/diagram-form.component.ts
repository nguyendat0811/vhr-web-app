import { FormulaConfigService } from './../../../../../../core/services/hr-payroll/formula-config.service';
import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AppComponent } from '@app/app.component';
import { ACTION_FORM, RESOURCE, SalaryTypeService, SystemParameterService, SalaryTypeOrganizationMappingService } from '@app/core';
import { ValidationService } from '@app/shared/services';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslationService } from 'angular-l10n';
import { SYSTEM_PARAMETER_CODE } from './../../../../../../core/app-config';
import { FormulaDiagramService } from './../../../../../../core/services/hr-payroll/formula-diagram.service';
import { BaseComponent } from './../../../../../../shared/components/base-component/base-component.component';

@Component({
  selector: 'diagram-form',
  templateUrl: './diagram-form.component.html',
})
export class DiagramFormComponent extends BaseComponent implements OnInit {
  formDiagram: FormGroup;
  formDiagramDetail: FormArray;
  salaryTypeList: any = {};
  dfFormulaList: any = [];
  actionTypeList: any = {};
  serviceBeforeList: any = {};
  serviceAfterList: any = {};
  @Input() item: any;
  formConfig = {
    payrollDiagramId: [null],
    organizationId: [null, [ValidationService.required]],
    payrollDiagramName: ['', [ValidationService.required, ValidationService.maxLength(100)]],
    salaryTypeId: [null, [ValidationService.required]],
    startDate: [null, [ValidationService.required]],
    endDate: [null],
    status: [null]
  };

  formConfigDetail = {
    dfFormulaId: [null, [ValidationService.required]],
    actionType: [null, [ValidationService.required]],
    serviceRunBefore: [null, [ValidationService.maxLength(100)]],
    serviceRunAfter: [null, [ValidationService.maxLength(100)]],
    description: ['', [ValidationService.maxLength(1000)]],
  };
  constructor(public activeModal: NgbActiveModal
    , private formulaDiagramService: FormulaDiagramService
    , private formulaConfigService: FormulaConfigService
    , private salaryTypeService: SalaryTypeService
    , private translation: TranslationService
    , private systemParameterService: SystemParameterService
    , public actr: ActivatedRoute
    , private app: AppComponent
    , private salaryTypeOrganizationMappingService: SalaryTypeOrganizationMappingService) {
    super(actr, RESOURCE.PAYROLL_DIAGRAM, ACTION_FORM.FORMULA_DIAGRAM_INSERT);
    this.buildForms({}, ACTION_FORM.FORMULA_DIAGRAM_INSERT);
    this.buildDiagramDetailReason();
    // this.salaryTypeService.getListActive()
    //   .subscribe(arg => this.salaryTypeList = arg.data);
    this.actionTypeList = [{actionType: 0 , name: this.translation.translate('payroll.formula.diagram.insert')},
                          {actionType: 1 , name: this.translation.translate('payroll.formula.diagram.update')}];
    this.systemParameterService.findEffectiveParamByName(SYSTEM_PARAMETER_CODE.CALCULATE_SERVICE)
      .subscribe(arg => {
        this.serviceBeforeList = arg.data;
        this.serviceAfterList = arg.data;
      });
  }

  ngOnInit() {

  }

  onSalaryTypeChange(event?) {
    if (event) {
      this.formDiagramDetail.controls.forEach(element  => {
        const temp =  element as FormGroup;
        temp.removeControl('dfFormulaId');
        temp.addControl('dfFormulaId',
        CommonUtils.createControl(this.actionForm, 'dfFormulaId', null, [ValidationService.required]));
      });

      this.formulaConfigService.findBySalaryTypeId(event).subscribe(arg => this.dfFormulaList = arg.data);
    } else {
      this.dfFormulaList = [];
    }
  }

  checkStatusCondition(data) {
    const today = new Date();
    const mm = today.getMonth();
    const yyyy = today.getFullYear();
    const dd = today.getDate();
    const now = new Date (yyyy, mm, dd);
    const startDate = new Date(data.startDate);
    const endDate = data.endDate ? new Date(data.endDate) : now;
    if (startDate.getTime() > now.getTime() || endDate.getTime() < now.getTime()) {
       this.formDiagram.value.status = 0;
    } else {
       this.formDiagram.value.status = 1;
    }
  }

  removeDiagramDetail(index) {
    const controls = this.formDiagramDetail as FormArray;
    if (controls.length === 1) {
      this.buildDiagramDetailReason({});
      const group = this.makeDiagramDetailForm();
      controls.push(group);
      this.formDiagramDetail = controls;
    }
    controls.removeAt(index);
  }

  addDiagramDetail() {
    const controls = this.formDiagramDetail as FormArray;
    controls.insert(controls.length, this.makeDiagramDetailForm());
  }

  processSaveOrUpdate() {
    if (!this.validateBeforeSave()) {
      return;
    } 
      const formSave = {};
      formSave['diagramForm'] = this.formDiagram.value;
      formSave['diagramDetailForms'] = this.formDiagramDetail.value;
      this.app.confirmMessage(null, () => {// on accepted
        this.checkStatusCondition(formSave['diagramForm']);
        this.formulaDiagramService.saveOrUpdate(formSave)
          .subscribe(res => {
            if (this.formulaDiagramService.requestIsSuccess(res)) {
              this.activeModal.close(res);
            }
          });
      }, () => {// on rejected
      });
  }

  private validateBeforeSave(): boolean {
    const formDiagram = CommonUtils.isValidForm(this.formDiagram);
    const formDiagramDetail = CommonUtils.isValidFormAndValidity(this.formDiagramDetail);
    return formDiagram && formDiagramDetail;
  }
  get f() {
    return this.formDiagram.controls;
  }
  /*
  * buildForms
  */
  private buildForms(data?: any, actionForm?: ACTION_FORM) {
    this.formDiagram = this.buildForm(data, this.formConfig, actionForm,
      [ ValidationService.notAffter('startDate', 'endDate', 'payroll.formula.diagram.endDate')]);

    // Load loai luong theo don vi
    this.f.organizationId.valueChanges.subscribe(data => {
      this.salaryTypeOrganizationMappingService.findByOrgId(data).subscribe(res => {
        this.f.salaryTypeId.setValue(null)
        this.salaryTypeList = res.data;
      });
    });
  }

  private makeDiagramDetailForm(): FormGroup {
    const formGroup = this.buildForm({}, this.formConfigDetail, ACTION_FORM.FORMULA_DIAGRAM_UPDATE);
    return formGroup;
  }
  private buildDiagramDetailReason(listDiagramDetail?: any) {
    const controls = new FormArray([]);
    if (listDiagramDetail && listDiagramDetail.length > 0) {
      for (const i in listDiagramDetail) {
        const contractDetail = listDiagramDetail[i];
        const group = this.makeDiagramDetailForm();
        group.patchValue(contractDetail);
        controls.push(group);
      }
    } else {
      const group = this.makeDiagramDetailForm();
      controls.push(group);
    }
    this.formDiagramDetail = controls;
  }
  /**
   * setFormValue
   * param data
   */
  public setFormValue(propertyConfigs: any, data?: any) {
    this.propertyConfigs = propertyConfigs;
    if (data && data.payrollDiagramId > 0) {
      this.buildForms(data, ACTION_FORM.FORMULA_DIAGRAM_UPDATE);
      this.formulaDiagramService.findByPayrollDiagramId(data.payrollDiagramId)
        .subscribe(arg => {
          this.buildDiagramDetailReason(arg.data);
        });
        this.onSalaryTypeChange(data.salaryTypeId);
      // this.formulaConfigService.findBySalaryTypeId(data.salaryTypeId).subscribe(arg => this.dfFormulaList = arg.data);
      this.salaryTypeOrganizationMappingService.findByOrgId(data.organizationId).subscribe(res => {
        // this.f.salaryTypeId.setValue(data.salaryTypeId)
        this.salaryTypeList = res.data;
      });
    }
  }

}
