import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormulaRoutingModule } from './formula-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    FormulaRoutingModule
  ]
})
export class FormulaModule { }
