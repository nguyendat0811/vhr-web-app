import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'diagram',
    loadChildren: './diagram/diagram.module#DiagramModule'
  },
  {
    path: 'config',
    loadChildren: './config/config.module#ConfigModule'
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormulaRoutingModule { }
