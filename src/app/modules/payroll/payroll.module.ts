import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PayrollRoutingModule } from './payroll-routing.module';
import { SharedModule } from '@app/shared';
import { ConfigIndexComponent } from './payroll/config/config-index/config-index.component';
import { CalculatorIndexComponent } from './payroll/calculator/calculator-index/calculator-index.component';
import { AddNewComponent } from './payroll/calculator/calculator-index/add-new/add-new.component';
import { ImportPayrollGeneralComponent } from './payroll/calculator/calculator-index/import-payroll-general/import-payroll-general.component';
import { ProgressIndexComponent } from './payroll/progress/progress-index/progress-index.component';
import { PayslipDetailComponent } from './payroll/calculator/calculator-index/payslip-detail/payslip-detail.component';
import { RecalculateIndexComponent } from './payroll/calculator/recalculate-index/recalculate-index.component';
import { TaxSyncComponent } from './payroll/calculator/calculator-index/tax-sync/tax-sync.component';
import { PaymentSyncComponent } from './payroll/calculator/calculator-index/payment-sync/payment-sync.component';
import { TabViewModule } from 'primeng/tabview';
import { AccordionModule } from 'primeng/accordion';
import { CheckboxModule } from 'primeng/checkbox';
import { InputTextModule } from 'primeng/inputtext';
import { TooltipModule } from 'primeng/tooltip';
import { ErrorDetailComponent } from './payroll/calculator/calculator-index/error-detail/error-detail.component';

import { PayrollMasterFormComponent } from './payroll/progress/payroll-master/payroll-master.component';
 
@NgModule({
  declarations: [
    ConfigIndexComponent
    , CalculatorIndexComponent
    , PayslipDetailComponent
    , AddNewComponent
    , ImportPayrollGeneralComponent
    , ProgressIndexComponent
    , RecalculateIndexComponent
    , TaxSyncComponent
    , PaymentSyncComponent
    , ErrorDetailComponent
    , PayrollMasterFormComponent
  ],
  imports: [
    CommonModule
    , SharedModule
    , PayrollRoutingModule
    , TabViewModule
    , AccordionModule
    , CheckboxModule
    , InputTextModule
    , TooltipModule
  ],
  entryComponents: [PayslipDetailComponent, AddNewComponent, ImportPayrollGeneralComponent, TaxSyncComponent, PaymentSyncComponent, PayrollMasterFormComponent]
})
export class PayrollModule { }
