import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LockUnlockIndexComponent } from './lock-unlock-index/lock-unlock-index.component';
import { PropertyResolver } from '@app/shared/services/property.resolver';
import { RESOURCE } from '@app/core';
import { CommonUtils } from '@app/shared/services';

const routes: Routes = [
  {
    path: '',
    component: LockUnlockIndexComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.TIMEKEEPING_LOCK,
      nationId: CommonUtils.getNationId()
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LockUnlockRoutingModule { }
