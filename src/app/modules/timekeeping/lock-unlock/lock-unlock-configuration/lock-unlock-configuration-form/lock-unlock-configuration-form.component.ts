import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { APP_CONSTANTS, ACTION_FORM, RESOURCE } from '@app/core/app-config';
import { FormGroup, FormBuilder , Validators, FormArray, FormControl} from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { TimekeepingLockConfigService } from '@app/core';
import { AppComponent } from '@app/app.component';
import { ValidationService } from '@app/shared/services/validation.service';

@Component({
  selector: 'lock-unlock-configuration-form',
  templateUrl: './lock-unlock-configuration-form.component.html',
})
export class LockUnlockConfigurationFormComponent extends BaseComponent implements OnInit {
  formSaveArray: FormArray;
  formSave: FormGroup;
  formSaveGroupConfig = {
    organizationId: ['', [ValidationService.required]],
    lockType: [1, [ValidationService.required]],
    lockDateFrom: ['', [ValidationService.required, Validators.max(31), Validators.min(1)
                      , ValidationService.positiveInteger, ValidationService.maxLength(2)]],
    lockDateTo: ['', [ValidationService.required, Validators.max(31), Validators.min(1)
                      , ValidationService.positiveInteger, ValidationService.maxLength(2)]],
    closingDate: ['', [ValidationService.required, Validators.max(31), Validators.min(1)
                      , ValidationService.positiveInteger, ValidationService.maxLength(2)]],
    timekeepingLockConfigId: [''],
  };
  lockTypeList: any;
  isDay: boolean;
  constructor(public actr: ActivatedRoute
            , public activeModal: NgbActiveModal
            , private app: AppComponent
            , public timekeepingLockConfigService: TimekeepingLockConfigService) {
    super(actr, RESOURCE.TIMEKEEPING_LOCK, ACTION_FORM.TIMEKEEPING_LOCK_CONFIG_INSERT);
    this.formSave = this.buildForm({}, this.formSaveGroupConfig, ACTION_FORM.TIMEKEEPING_LOCK_CONFIG_INSERT,
      ValidationService.notAffterNumber('lockDateFrom', 'lockDateTo', 'app.payrolls.lockDateTo'));
    this.lockTypeList = APP_CONSTANTS.LOCK_TYPE;
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
  }
  /**
   * processSaveOrUpdate
   */
  processSaveOrUpdate() {
    if (!this.validateBeforeSave()) {
      return;
    } else {
      this.app.confirmMessage(null, () => {// on rejected
        this.timekeepingLockConfigService.saveOrUpdate(this.formSave.value)
        .subscribe(res => {
          if (this.timekeepingLockConfigService.requestIsSuccess(res)) {
            this.activeModal.close(res);
          }
        });
      }, () => {// on rejected

      });
    }
  }

  private validateBeforeSave(): boolean {
    return CommonUtils.isValidForm(this.formSave);
  }

  /**
   * setFormValue
   * param: Array propertyConfigs
   * param data
   */
  public setFormValue(propertyConfigs: any, data?: any) {
    this.propertyConfigs = propertyConfigs;
    if (data && data.timekeepingLockConfigId > 0) {
      this.formSave = this.buildForm(data, this.formSaveGroupConfig, ACTION_FORM.TIMEKEEPING_LOCK_CONFIG_UPDATE);
      this.onChaneLockType(data.lockType);
    } else {
      this.formSave = this.buildForm({}, this.formSaveGroupConfig, ACTION_FORM.TIMEKEEPING_LOCK_CONFIG_INSERT);
    }
  }
  get f () {
    return this.formSave.controls;
  }
  public onChaneLockType(event) {
    if (event === 1 ) {
      this.isDay = false;
      this.formSave.addControl('lockDateFrom', CommonUtils.createControl(this.actionForm, 'lockDateFrom', this.formSave.value.lockDateFrom
      , [ValidationService.required, Validators.max(31), ValidationService.positiveInteger, ValidationService.maxLength(2)]));
      this.formSave.addControl('lockDateTo', CommonUtils.createControl(this.actionForm, 'lockDateTo', this.formSave.value.lockDateTo
      , [ValidationService.required, Validators.max(31), ValidationService.positiveInteger, ValidationService.maxLength(2)]));
      this.formSave.addControl('closingDate', CommonUtils.createControl(this.actionForm, 'closingDate', this.formSave.value.closingDate
      , [ValidationService.required, Validators.max(31), ValidationService.positiveInteger, ValidationService.maxLength(2)]));
    } else {
      this.isDay = true;
      this.formSave.removeControl('lockDateFrom');
      this.formSave.removeControl('lockDateTo');
      this.formSave.removeControl('closingDate');
    }
  }
}
