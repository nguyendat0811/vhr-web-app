import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { RESOURCE, ACTION_FORM, APP_CONSTANTS, DEFAULT_MODAL_OPTIONS } from '@app/core';
import { ActivatedRoute } from '@angular/router';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { TimekeepingLockConfigService } from '@app/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LockUnlockConfigurationFormComponent } from './lock-unlock-configuration-form/lock-unlock-configuration-form.component';
import { ValidationService } from '@app/shared/services';

@Component({
  selector: 'lock-unlock-configuration',
  templateUrl: './lock-unlock-configuration.component.html',
})
export class LockUnlockConfigurationComponent extends BaseComponent implements OnInit {
  public commonUtils =  CommonUtils;

  formConfig = {
    organizationId: ['', [ValidationService.required]],
    lockType: [''],
  };
  showList: boolean;
  lockTypeList: any;
  constructor(public actr: ActivatedRoute
            , private modalService: NgbModal
            , public timekeepingLockConfigService: TimekeepingLockConfigService) {
    super(actr, RESOURCE.TIMEKEEPING_LOCK_CONFIG, ACTION_FORM.TIMEKEEPING_LOCK_CONFIG_SEARCH);
    this.setMainService(timekeepingLockConfigService);
    this.formSearch = this.buildForm({}, this.formConfig);
    this.lockTypeList = APP_CONSTANTS.LOCK_TYPE;
    this.showList = false;
  }

  ngOnInit() {
  }
  get f () {
    return this.formSearch.controls;
  }
  /**
   *processSearchTimekeepingLockConfig
   */
  public processSearchTimekeepingLockConfig(event?): void {
    if (!CommonUtils.isValidForm(this.formSearch)) {
      return;
    }

    const params = this.formSearch ? this.formSearch.value : null;

    this.timekeepingLockConfigService.search(params, event).subscribe(res => {
      this.resultList = res;
      // if (res.data.length !== 0) {
      //   this.showList = true;
      // } else {
      //   this.showList = false;
      // }
      this.showList = true;
    });
    if (!event) {
      if (this.dataTable) {
        this.dataTable.first = 0;
      }
    }
  }

  /**
   * prepareUpdate
   * param item
   */
  prepareSaveOrUpdate(item) {
    if (item && item.timekeepingLockConfigId > 0) {
      this.timekeepingLockConfigService.findOne(item.timekeepingLockConfigId)
        .subscribe(res => {
          this.activeModal(res.data);
        });
    } else {
      this.activeModal();
    }
  }
  /**
   * show model
   * data
   */
  private activeModal(data?: any) {
    const modalRef = this.modalService.open(LockUnlockConfigurationFormComponent, DEFAULT_MODAL_OPTIONS);
    if (data) {
      modalRef.componentInstance.setFormValue(this.propertyConfigs, data);
    }
    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
      if (this.timekeepingLockConfigService.requestIsSuccess(result)) {
        this.processSearch();
      }
    });
  }
}
