import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@app/shared';
import { LockUnlockRoutingModule } from './lock-unlock-routing.module';
import { LockUnlockIndexComponent } from './lock-unlock-index/lock-unlock-index.component';
import { LockUnlockAttendanceComponent } from './lock-unlock-attendance/lock-unlock-attendance.component';
import { LockUnlockConfigurationComponent } from './lock-unlock-configuration/lock-unlock-configuration.component';
import { LockUnlockConfigurationFormComponent } from './lock-unlock-configuration/lock-unlock-configuration-form/lock-unlock-configuration-form.component';


@NgModule({

  declarations: [LockUnlockIndexComponent
    , LockUnlockAttendanceComponent
    , LockUnlockConfigurationComponent
    , LockUnlockConfigurationFormComponent],

  imports: [
    CommonModule,
    SharedModule,
    LockUnlockRoutingModule,
  ],
  entryComponents: [LockUnlockConfigurationFormComponent]
})
export class LockUnlockModule { }
