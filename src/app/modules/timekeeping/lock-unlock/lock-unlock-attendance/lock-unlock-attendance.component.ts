import { Component, OnInit, ViewChild } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { RESOURCE, ACTION_FORM, TimekeepingLockService } from '@app/core';
import { ActivatedRoute } from '@angular/router';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import * as moment from 'moment';
import { Validators } from '@angular/forms';
import { AppComponent } from '@app/app.component';
import { HelperService } from '@app/shared/services/helper.service';
import { ValidationService } from '@app/shared/services';

@Component({
  selector: 'lock-unlock-attendance',
  templateUrl: './lock-unlock-attendance.component.html'
})
export class LockUnlockAttendanceComponent extends BaseComponent implements OnInit {
  length;
  moment = moment;
  public commonUtils =  CommonUtils;
  formConfig = {
    organizationId: ['',[ValidationService.required]],
    startDate: [moment(new Date()).startOf('day').startOf('month').toDate().getTime(), [ValidationService.required]],
    endDate: [moment(new Date()).startOf('day').endOf('month').toDate().getTime(), [ValidationService.required]],
    isLock: [''],
    date: [''],
    orgLeavesId: [''],
  };
  startDate: any;
  endDate: any;
  showList: boolean;
  count: number;
  public listData = [];
  resultListOrgLock: any = {};
  resultListOrgLockByDay: any = {};
  resultListOrgLockByDayNew: any = {};
  public listHeaderDate = [];
  @ViewChild('ptable') dataTable: any;
  constructor( public actr: ActivatedRoute
              , private timekeepingLockService: TimekeepingLockService
              , private helperService: HelperService
              , private app: AppComponent) {
    super(actr, RESOURCE.TIMEKEEPING_LOCK, ACTION_FORM.TIMEKEEPING_LOCK_SEARCH);
    this.formSearch = this.buildForm({}, this.formConfig);
    this.setMainService(timekeepingLockService);
    this.showList = false;
    this.timekeepingLockService.getDate().subscribe(res => {
      this.formSearch = this.buildForm(res, this.formConfig, ACTION_FORM.TIMEKEEPING_LOCK_SEARCH,
        [ValidationService.notAffter('startDate', 'endDate', 'app.payrolls.toDate')]);
    });
    this.initListDateSearch(this.formSearch.get('startDate').value, this.formSearch.get('endDate').value);
  }

  ngOnInit() {
    this.processSearchLockTimekeeping();
  }

  get f () {
    return this.formSearch.controls;
  }

  /**
   *processSearchLockTimekeeping
   */
  public processSearchLockTimekeeping(event?): void {
    if (!CommonUtils.isValidForm(this.formSearch)) {
      return;
    }

    const params = this.formSearch ? this.formSearch.value : null;

    this.timekeepingLockService.search(params, event).subscribe(res => {
      this.resultList = res;
      this.showList = true;
      this.initListDateSearch(params.startDate, params.endDate);
    });
    if (!event) {
      if (this.dataTable) {
        this.dataTable.first = 0;
      }
    }
  }

  /**
   * getMapDateFunction
   */
  private getMapDateFunction (length?: number, params?: any) {
    if (length !== 0) {
      this.showList = true;
      this.initListDateSearch(params.startDate, params.endDate);
    } else {
      this.showList = false;
    }
  }

  /**
   * initListDateSearch
   */
  initListDateSearch(startSearh, endSearh) {
    this.listData = [];
    this.listHeaderDate = [];
    const startDate = moment(startSearh);
    const endDate = moment(endSearh);
    const itemDate = startDate;
    let currentMonth = '';
    while (itemDate.isSameOrBefore(endDate)) {
      const format = itemDate.format('MM/YYYY');
      let listByHeader = [];
      if (currentMonth !== format) {
        currentMonth = format;
        listByHeader.push(itemDate.clone());
        this.listHeaderDate.push(listByHeader);
      } else {
        listByHeader = this.listHeaderDate[this.listHeaderDate.length - 1];
        listByHeader.push(itemDate.clone());
        this.listHeaderDate[this.listHeaderDate.length - 1] = listByHeader;
      }
      this.listData.push(itemDate.clone());
      itemDate.add(1, 'day');
    }
  }

  /**
   * processLockUnLock
   * @ param type
   */
  public processLockUnLock(type?: number, orgLeavesId?: number, date?: any): void {
    if (!this.validateBeforeSave()) {
      return;
    }
    if (type === 1) {
      this.comfirm(type, orgLeavesId, date, 'app.payrolls.comfirmLock');
    } else if (type === 0) {
      this.comfirm(type, orgLeavesId, date, 'app.payrolls.comfirmUnLock');
    }
  }

  public comfirm(type?: number, orgLeavesId?: number, date?: any, message?: string) {
    this.app.confirmMessage(message, () => {
      const formSubmit = this.formSearch.value;
      formSubmit['isLock'] = type;
      formSubmit['orgLeavesId'] = orgLeavesId;
      formSubmit['date'] = date;
      this.timekeepingLockService.saveOrUpdate(formSubmit)
        .subscribe(res => {
          if (this.timekeepingLockService.requestIsSuccess(res)) {
            setTimeout(() => {
              this.processSearchLockTimekeeping();
            }, 500);
          }
      });
    }, () => {

    });
  }
  /**
   * validateBeforeSave
   */
  private validateBeforeSave(): boolean {
    return CommonUtils.isValidForm(this.formSearch);
  }
}
