import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { HolidayService } from '@app/core/services/hr-timekeeping/holiday.service';
import * as moment from 'moment';
import { CommonUtils, ValidationService } from '@app/shared/services';
import { AppComponent } from '@app/app.component';

@Component({
  selector: 'holiday-definition-clone',
  templateUrl: './holiday-definition-clone.component.html',

})
export class HolidayDefinitionCloneComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  years: any;
  checkYear: boolean;
  listToYear: Array<any>;
  formConfig = {
    fromYear: [null, [ValidationService.required]],
    toYear: [null, [ValidationService.required]],
  };
  constructor(public actr: ActivatedRoute
            , public activeModal: NgbActiveModal
            , private app: AppComponent
            , private holidayService: HolidayService) {
    super(actr);
    this.buildForms();

   }

  ngOnInit() {
    this.listToYear = this.getToYearList();
  }

  /**
   * buildForm
   */
  private buildForms(years?: any): void {
    this.formSave = this.buildForm({}, this.formConfig);
    this.formSave.controls['fromYear'].setValue(years);
  }

  setFormValue(propertyConfigs: any, fromYear?: any) {
    this.propertyConfigs = propertyConfigs;
    this.years = fromYear;
      this.buildForms(this.years);
  }

  private getToYearList() {
    const conflictYear = this.formSave.controls['fromYear'].value;
    this.listToYear = [];
    const currentYear = new Date().getFullYear();
    for (let i = (currentYear) ; i <= (currentYear + 10) ; i++ ) {
      const obj = {
        year: i,
      };
      if (obj.year !== conflictYear) {
        this.listToYear.push(obj);
      }
    }
    return this.listToYear;
  }

  get f () {
    return this.formSave.controls;
  }

  processClone() {
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    }
    this.app.confirmMessage(null, () => {// on accepted
      this.holidayService.cloneHoliday(this.formSave.value)
        .subscribe(res => {
          this.activeModal.close(res);
        });
    }, () => {
      // on rejected
    });
  }
}
