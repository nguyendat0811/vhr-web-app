import { ValidationService } from '../../../../../shared/services/validation.service';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { HolidayService } from '../../../../../core/services/hr-timekeeping/holiday.service';
import { Validators, FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { WorkdayTypeService, ACTION_FORM, RESOURCE } from '@app/core';
import { AppComponent } from '@app/app.component';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'holiday-definition-form',
  templateUrl: './holiday-definition-form.component.html',
})
export class HolidayDefinitionFormComponent extends BaseComponent implements OnInit {
  formSave: FormArray;
  workdayTypeList: any = {};
  day: any;
  listIsDelete = [];
  formSaveConfig = {
    holidayId: [''],
    holidayDate: [''],
    workdayTypeCode: [null, [ValidationService.requiredIfHaveOne]],
    hours: [null, [ValidationService.maxLength(4), ValidationService.requiredIfHaveOne
      , ValidationService.positiveInteger, Validators.min(1), Validators.max(10)]],
  };

  constructor(private formBuilder: FormBuilder
            , public actr: ActivatedRoute
            , private app: AppComponent
            , public activeModal: NgbActiveModal
            , private holidayService: HolidayService
            , private workdayTypeService: WorkdayTypeService) {
    super(actr, RESOURCE.ATTENDANCE_TYPE, ACTION_FORM.UPDATE);
    this.buildFormHolidayDefinition({});

    this.workdayTypeService.findAllWorkdayTypeByMarketId().subscribe(
      res => {
        this.workdayTypeList = res.data;
      });
   }

  ngOnInit() {
  }

  /**
   * makeDefaultHolidayDefinitionForm
   */
  private makeDefaultHolidayDefinitionForm(): FormGroup {
    const formGroup = this.buildForm({}, this.formSaveConfig, ACTION_FORM.UPDATE);
    formGroup.controls['holidayDate'].setValue(this.day);
    return formGroup;
  }

  private buildFormHolidayDefinition(listHolidayDefinition?: any) {
    const controls = new FormArray([]);
    if (!listHolidayDefinition) {
      this.formSave = new FormArray([this.makeDefaultHolidayDefinitionForm()]);
    } else {
      for (const i in listHolidayDefinition) {
        const holidayDefinition = listHolidayDefinition[i];
        const group = this.makeDefaultHolidayDefinitionForm();
        group.patchValue(holidayDefinition);
        controls.push(group);
      }
      this.formSave = controls;
    }
  }

    /**
   * addHolidayDefinition
   * param index
   * param item
   */
  public addHolidayDefinition(index: number) {
    const controls = this.formSave as FormArray;
    controls.insert(index + 1, this.makeDefaultHolidayDefinitionForm());
  }
  /**
   * removeHolidayDefinition
   * param index
   * param item
   */
  public processDeleteHoliday(index: number, id: number) {
    if (id && id > 0) {
      this.app.confirmDelete(null, () => {// on accepted
        this.holidayService.deleteById(id)
          .subscribe(res => {
            if (this.holidayService.requestIsSuccess(res)) {
              this.holidayService.findByDate(this.day).subscribe(
                (data) => {
                this.buildFormHolidayDefinition(data);
                }
              );
            }
          });
      }, () => {// on rejected

      });
    } else {
      const controls = this.formSave as FormArray;
      if (controls.length === 1) {
        this.buildFormHolidayDefinition();
        return;
      }
      controls.removeAt(index);
    }
  }

  setFormValue(propertyConfigs: any, data?: any, day?: any) {
    this.propertyConfigs = propertyConfigs;
    this.day = day;
    if (data) {
      this.buildFormHolidayDefinition(data);
    } else {
      this.buildFormHolidayDefinition();
    }
  }

  public processSaveOrUpdate() {
    if (!CommonUtils.isValidFormAndValidity(this.formSave)) {
      return;
    }
    const submitData = {};
    submitData['holidayDate'] = this.day;
    submitData['listForm'] = this.formSave.value;
    this.app.confirmMessage(null, () => {// on accepted
      this.holidayService.saveOrUpdate(submitData)
        .subscribe(res => {
          if (this.holidayService.requestIsSuccess(res)) {
            this.activeModal.close(res);
          }
        });
    }, () => {
      // on rejected
    });
  }
}
