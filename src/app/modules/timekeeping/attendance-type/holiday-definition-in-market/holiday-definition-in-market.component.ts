import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { RESOURCE, ACTION_FORM, DEFAULT_MODAL_OPTIONS } from '@app/core';
import * as moment from 'moment';
import { HolidayService } from '@app/core/services/hr-timekeeping/holiday.service';
import { CommonUtils } from '@app/shared/services';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HolidayDefinitionCloneComponent } from './holiday-definition-clone/holiday-definition-clone.component';
import { Validators } from '@angular/forms';
import { ValidationService } from '../../../../shared/services/validation.service';

@Component({
  selector: 'holiday-definition-in-market',
  templateUrl: './holiday-definition-in-market.component.html',
})
export class HolidayDefinitionInMarketComponent extends BaseComponent implements OnInit {
  formConfig = {
    year: [parseInt(moment(new Date()).format('YYYY')), [ValidationService.required]],
  };
  public listMonth = [];
  month: string;
  holidayMap: any;
  listYear: Array<any>;
  constructor(public actr: ActivatedRoute
    , private modalService: NgbModal
    , public holidayService: HolidayService) {
    super(actr, RESOURCE.ATTENDANCE_TYPE, ACTION_FORM.SEARCH);
    this.formSearch = this.buildForm({}, this.formConfig);
    this.listYear = this.getYearList();
   }


  ngOnInit() {
    this.processSearchByYear();
  }
  public onChangeHoliday(event) {
    if (!event) {
      return;
    }
    if (this.holidayService.requestIsSuccess(event)) {
      this.listMonth = [];
      setTimeout(() => {
        this.processSearchByYear();
      }, 200);
    }
  }
  public processSearchByYear(event?) {
    if (!CommonUtils.isValidForm(this.formSearch)) {
      return;
    }
    const year = this.formSearch.controls['year'].value;
    this.holidayService.findByYear(year).subscribe(res => {
      if (this.holidayService.requestIsSuccess(res)) {
        this.initListMonthSearch();
        this.holidayMap = this.buildMap(res.data);
      }
    });
  }

  private getYearList() {
    this.listYear = [];
    const currentYear = new Date().getFullYear();
    for (let i = (currentYear - 50) ; i <= (currentYear + 50) ; i++ ) {
      const obj = {
        year: i
      };
      this.listYear.push(obj);
    }
    return this.listYear;
  }
  get f () {
    return this.formSearch.controls;
  }

  /**
   * initListMonthSearch
   * @ param year
   */
  initListMonthSearch() {
    const year = this.formSearch.controls['year'].value;
    this.listMonth = ['01/' + year, '02/' + year, '03/' + year, '04/' + year
                    , '05/' + year, '06/' + year, '07/' + year, '08/' + year
                    , '09/' + year, '10/' + year, '11/' + year, '12/' + year ];
  }
  buildMap(data): any {
    const map = {};
    if (!data) {
      return map;
    }

    for (const holiday of data) {
      let currList = [];
      if (map.hasOwnProperty(holiday.holidayDate)) {
        currList = map[holiday.holidayDate];
      }
      currList.push(holiday);
      map[holiday.holidayDate] = currList;
    }
    return map;
  }


  showPopupClone() {
    if (!CommonUtils.isValidForm(this.formSearch)) {
      return;
    }
    const fromYear = this.formSearch.controls['year'].value;
    const modalRef = this.modalService.open(HolidayDefinitionCloneComponent, DEFAULT_MODAL_OPTIONS);
    if (fromYear) {
      modalRef.componentInstance.setFormValue(this.propertyConfigs, fromYear);
    } else {
      modalRef.componentInstance.setFormValue(this.propertyConfigs);
    }
  }
}
