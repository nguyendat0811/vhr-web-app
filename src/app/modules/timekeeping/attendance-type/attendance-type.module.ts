import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@app/shared';
import { AttendanceTypeRoutingModule } from './attendance-type-routing.module';
import { AttendanceTypeIndexComponent } from './attendance-type-index/attendance-type-index.component';
import { AttendanceTypeDefinitionComponent } from './attendance-type-definition/attendance-type-definition.component';
import { HolidayDefinitionInMarketComponent } from './holiday-definition-in-market/holiday-definition-in-market.component';
import { HolidayDefinitionFormComponent } from './holiday-definition-in-market/holiday-definition-form/holiday-definition-form.component';
// tslint:disable-next-line:max-line-length
import { HolidayDefinitionCloneComponent } from './holiday-definition-in-market/holiday-definition-clone/holiday-definition-clone.component';


@NgModule({

  declarations: [ AttendanceTypeIndexComponent
                , AttendanceTypeDefinitionComponent, HolidayDefinitionInMarketComponent
                , HolidayDefinitionFormComponent, HolidayDefinitionCloneComponent],

  imports: [
    CommonModule,
    SharedModule,
    AttendanceTypeRoutingModule,
  ],
  entryComponents: [HolidayDefinitionFormComponent, HolidayDefinitionCloneComponent]
})
export class AttendanceTypeModule { }
