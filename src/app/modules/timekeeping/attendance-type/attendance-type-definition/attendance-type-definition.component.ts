import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute, Router, NavigationError, NavigationEnd, NavigationStart } from '@angular/router';
import { ACTION_FORM, RESOURCE, SysCatService, APP_CONSTANTS } from '@app/core';
import { Validators, FormGroup, FormArray } from '@angular/forms';
import { ValidationService, CommonUtils } from '@app/shared/services';
import { WorkdayTypeService } from '@app/core/services/hr-timekeeping/workday-type.service';
import { AppComponent } from '@app/app.component';
import { HelperService } from '@app/shared/services/helper.service';

@Component({
  selector: 'attendance-type-definition',
  templateUrl: './attendance-type-definition.component.html',
})

export class AttendanceTypeDefinitionComponent extends BaseComponent implements OnInit {
  list: any;
  public commonUtils =  CommonUtils;
  formConfig = {
    code: ['' , [ValidationService.maxLength(20)]],
    name: ['' , [ValidationService.maxLength(200)]],
    attenanceType: [''],
    lstAttenanceType: [null],
    marketCompanyId: [CommonUtils.getCurrentMarketCompanyId()],
  };
  formSaveConfig = {
    marketCompanyId: [CommonUtils.getCurrentMarketCompanyId()],
    workdayTypeId: [''],
    code: ['' , [ValidationService.maxLength(50)]],
    name: ['' , [ValidationService.maxLength(200)]],
    attenanceType: [''],
    note: ['', [ValidationService.maxLength(255)]],
    sysCatId: [''],
    modifiedBy: [''],
    modifiedDate: [''],
    createdBy: [''],
    createdDate: [''],
    lstAttenanceType: [null],
  };
  formSave: FormArray;
  attenanceTypeList: any;
  constructor(public actr: ActivatedRoute
              , private app: AppComponent
              , private helperService: HelperService
              , private workdayTypeService: WorkdayTypeService
              , public sysCatService: SysCatService) {
    super(actr, RESOURCE.ATTENDANCE_TYPE_DEFINITION, ACTION_FORM.SEARCH);
    this.setMainService(workdayTypeService);
    this.formSearch = this.buildForm({}, this.formConfig);
    this.buildFormSave([]);
    this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.ATTENDANCE_TYPE).subscribe(res => {
      this.attenanceTypeList = res.data;
    });
  }

  ngOnInit() {
    this.processSearchArray();
  }

  /**
   * processSearchArray
   * param event
   */
  public processSearchArray(event?): void {
    if(!this.hasPermission('action.view')){
      return;
    }
    if (!CommonUtils.isValidForm(this.formSearch)) {
      return;
    }
    const params = this.formSearch ? this.formSearch.value : null;
    this.workdayTypeService.search(params, event).subscribe(res => {
      this.resultList = res;
      if ( this.resultList ) {
        this.buildFormSave(res.data);
      }
    });
    if (!event) {
      if (this.dataTable) {
        this.dataTable.first = 0;
      }
    }
  }

  get f () {
    return this.formSearch.controls;
  }
  get fa () {
    return this.formSave.controls;
  }
  /**
   * add
   * param index
   * param item
   */
  public addRow(index: number, item: FormGroup) {
    const controls = this.formSave as FormArray;
    controls.insert(index + 1, this.makeDefaultFormSave());
  }
  /**
   * makeDefaultFormSave
   */
  private makeDefaultFormSave(data?: any): FormGroup {
    const formGroup = this.buildForm(data || {}, this.formSaveConfig, this.actionForm,
      [ ValidationService.requiredControlInGroup(['code', 'name', 'lstAttenanceType'], ['marketCompanyId'])]);
    return formGroup;
  }
  /**
   * buildFormSave
   */
  private buildFormSave(list?: any) {
    if (list.length === 0) {
      this.formSave = new FormArray([this.makeDefaultFormSave()]);
    } else {
      const controls = new FormArray([]);
      for (const i in list) {
        const attendanceTypeDefinition = list[i];
        const group = this.makeDefaultFormSave(attendanceTypeDefinition);
        controls.push(group);
      }
      this.formSave = controls;
    }
    this.formSave.setValidators([ValidationService.duplicateArray(['code'], 'code', 'app.detailReport.attendanceCode')]);
  }
  /**
   * removeRow
   * param index
   * param item
   */
  public removeRow(index: number, item: FormGroup, id?: number) {
    if (id && id > 0) {
      this.workdayTypeService.confirmDelete({
        messageCode: null,
        accept: () => {
          this.workdayTypeService.deleteById(id).subscribe(res => {
            if (this.workdayTypeService.requestIsSuccess(res)) {
              this.processSearchArray();
              this.remove(index, item);
            }
          });
        },
        reject: () => {}
      });
    } else {
      this.remove(index, item);
    }
  }
  /**
   * remove
   * param index
   * param item
   */
  public remove(index: number, item: FormGroup) {
    const controls = this.formSave as FormArray;
    if (controls.length === 1) {
      this.buildFormSave([]);
      return;
    }
    controls.removeAt(index);
  }

  private validateBeforeSave(): boolean {
    return CommonUtils.isValidForm(this.formSave);
  }

  processSaveOrUpdate() {
     if (!this.validateBeforeSave()) {
       return;
     }
    this.app.confirmMessage(null, () => {// on accepted
       this.workdayTypeService.saveOrUpdate(this.formSave.value)
        .subscribe(res => {
          if (this.workdayTypeService.requestIsSuccess(res)) {
            this.helperService.isProcessing(res.data);
            this.processSearchArray();
          }
         });
    }, () => {
      // on rejected
    });
  }
}
