import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PropertyResolver } from '@app/shared/services/property.resolver';
import { RESOURCE } from '@app/core';
import { AnnualLeaveIndexComponent } from './annual-leave-index/annual-leave-index.component';

const routes: Routes = [
  {
    path: '',
    component: AnnualLeaveIndexComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.ANNUAL_LEAVE
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AnnualLeaveRoutingModule { }
