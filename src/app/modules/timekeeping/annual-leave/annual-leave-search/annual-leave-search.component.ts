import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { RESOURCE, ACTION_FORM } from '@app/core';
import { Validators } from '@angular/forms';
import { ValidationService, CommonUtils } from '@app/shared/services';
import { AnnualLeaveService } from '@app/core/services/hr-timekeeping/annual-leave.service';
import { AppComponent } from '@app/app.component';
import { TranslationService } from 'angular-l10n';

@Component({
  selector: 'annual-leave-search',
  templateUrl: './annual-leave-search.component.html'
})
export class AnnualLeaveSearchComponent extends BaseComponent  implements OnInit {

  public current = new Date();
  public currentYear = new Date().getFullYear();
  public startYear = new Date().getFullYear() - 10;
  public endYear = new Date().getFullYear() + 1;
  public headerYear: any;
  private year: any;
  private startDateConfig: string;
  private endDateConfig: string;
  private expiredDateConfig: string;
  public listYear = [];

  private formSearchConfig = {
    year: [this.currentYear],
    organizationId: ['', [ValidationService.required]],
    employeeCode: ['', [ValidationService.maxLength(100)]]
  };

  constructor(public actr: ActivatedRoute
            , private app: AppComponent
            , public translation: TranslationService
            , public annualLeaveService: AnnualLeaveService) {
    super(actr, RESOURCE.ANNUAL_LEAVE, ACTION_FORM.SEARCH);
    this.setMainService(annualLeaveService);
    this.formSearch = this.buildForm({}, this.formSearchConfig, ACTION_FORM.SEARCH);
    this.processSearch();
   }

  ngOnInit() {
    this.createListYear(this.startYear, this.endYear);
    this.onchangeYear(this.currentYear);
  }

  get f () {
    return this.formSearch.controls;
  }

  private createListYear(startYear, endYear) {
    for (this.year = startYear; this.year <= endYear; this.year++) {
      this.listYear.push({'year': this.year});
    }
    return this.listYear;
  }

  public onchangeYear(event) {
    const selectedYear  = event;
    this.headerYear = selectedYear;
    if (this.headerYear === this.currentYear - 1) {
      this.annualLeaveService.getDateConfig(this.formSearch.value).subscribe(res => {
        this.expiredDateConfig = res['expiredDateConfig'];
      });
    }
  }

  public processExport() {
    if (!CommonUtils.isValidForm(this.formSearch)) {
      return;
    }
    const params = this.formSearch.value;
    this.annualLeaveService.export(params).subscribe(res => {
      saveAs(res, 'annual_leave_report');
    });
  }

  public processCalculate() {
    if (!CommonUtils.isValidForm(this.formSearch)) {
      return;
    }
    this.annualLeaveService.getDateConfig(this.formSearch.value).subscribe(res => {

      this.startDateConfig = res['startDateConfig'];
      this.endDateConfig = res['endDateConfig'] ;
      const expiredDateConfig = res['expiredDateConfig'];

      if (expiredDateConfig === null || expiredDateConfig === '') {
        return this.annualLeaveService.processReturnMessage({type: 'ERROR', code: 'expireDateNotConfigYet'});
      }

      const expiredDate = this.convertStringToDate(expiredDateConfig);
      if (this.current > expiredDate) {
        // Thuc hien khoa chot phep vao thoi diem ket thuc chu ky tinh phep
        // dao.lockCalculate(frm.getOrganizationId(), endDateConfig, req);
        return this.annualLeaveService.processReturnMessage({type: 'ERROR', code: 'expiredForCalculate'});
      }

      const trans = {'startDateConfig': this.startDateConfig, 'endDateConfig': this.endDateConfig};

      if (expiredDate > this.current && this.formSearch.value['year'] < this.currentYear) {
        this.app.getConfirmationService().confirm({
          message: this.translation.translate('app.annualLeave.warning.lockTimekeeping1')
                  + ' ' + trans['startDateConfig'] + ' '
                  + this.translation.translate('app.annualLeave.warning.lockTimekeeping2')
                  + ' ' + trans['endDateConfig'] + ' '
                  + this.translation.translate('app.annualLeave.warning.lockTimekeeping3'),
          header: this.translation.translate('common.message.confirmation'),
          icon: 'pi pi-exclamation-triangle',
          accept: () => { // on accepted
            this.calculate();
            this.processSearch();
          },
          reject: () => {
            // on rejected
          }
        });
      } else if (this.formSearch.value['year'] === this.currentYear) {
        this.app.getConfirmationService().confirm({
          message: this.translation.translate('app.annualLeave.warning.dataWillBeDelete'),
          header: this.translation.translate('common.message.confirmation'),
          icon: 'pi pi-exclamation-triangle',
          accept: () => { // on accepted
            this.calculate();
            this.processSearch();
          },
          reject: () => {
            // on rejected
          }
        });
      }
    });

  }

  private calculate() {
    this.annualLeaveService.calculate(this.formSearch.value)
        .subscribe(resf => {
          this.processSearch();
    });
  }

  private convertStringToDate(value: any): Date | null {
    if ((typeof value === 'string') && (value.indexOf('/') > -1)) {
      const str = value.split('/');

      const year = Number(str[2]);
      const month = Number(str[1]) - 1;
      const date = Number(str[0]);

      return new Date(year, month, date);
    } else if ((typeof value === 'string') && value === '') {
      return new Date();
    }
    const timestamp = typeof value === 'number' ? value : Date.parse(value);
    return isNaN(timestamp) ? null : new Date(timestamp);
  }
}
