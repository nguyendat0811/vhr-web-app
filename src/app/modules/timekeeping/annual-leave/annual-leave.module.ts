import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@app/shared';

import { AnnualLeaveRoutingModule } from './annual-leave-routing.module';
import { AnnualLeaveIndexComponent } from './annual-leave-index/annual-leave-index.component';
import { AnnualLeaveSearchComponent } from './annual-leave-search/annual-leave-search.component';

@NgModule({
  declarations: [AnnualLeaveIndexComponent, AnnualLeaveSearchComponent],
  imports: [
    CommonModule,
    SharedModule,
    AnnualLeaveRoutingModule
  ],
  entryComponents: [AnnualLeaveSearchComponent]
})
export class AnnualLeaveModule { }
