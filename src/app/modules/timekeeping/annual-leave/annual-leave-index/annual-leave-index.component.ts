import { Component, OnInit } from '@angular/core';
import { RESOURCE } from '@app/core';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';

@Component({
  selector: 'annual-leave-index',
  templateUrl: './annual-leave-index.component.html'
})
export class AnnualLeaveIndexComponent extends BaseComponent implements OnInit {

  constructor(public act: ActivatedRoute) { 
    super(act, RESOURCE.ANNUAL_LEAVE);
  }

  ngOnInit() {
  }

}
