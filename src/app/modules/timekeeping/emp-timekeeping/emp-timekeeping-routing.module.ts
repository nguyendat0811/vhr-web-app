import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TimekeepingComponent } from './timekeeping/timekeeping.component';
import { PropertyResolver } from '@app/shared/services/property.resolver';
import { RESOURCE } from '@app/core';
import { TimekeepingImportComponent } from './timekeeping/timekeeping-import.component';
import { TimekeepingSummaryReportComponent } from './report/timekeeping-summary-report.component';
import { TimekeepingDetailReportComponent } from './report/timekeeping-detail-report.component';
import { TimeKeepingUpdateComponent } from './update/timekeeping-update.component';

const routes: Routes = [
  {
    path: '',
    component: TimekeepingComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.EMP_TIMEKEEPING
    }
  },
  {
    path: 'update',
    component: TimeKeepingUpdateComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.EMP_TIMEKEEPING
    }
  },
  {
    path: 'search/:orgId',
    component: TimekeepingComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.EMP_TIMEKEEPING
    }
  },
  {
    path: 'import',
    component: TimekeepingImportComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.EMP_TIMEKEEPING
    }
  }, {
    path: 'summary-report',
    component: TimekeepingSummaryReportComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.EMP_TIMEKEEPING
    }
  }, {
    path: 'detail-report',
    component: TimekeepingDetailReportComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.EMP_TIMEKEEPING
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmpTimekeepingRoutingModule { }
