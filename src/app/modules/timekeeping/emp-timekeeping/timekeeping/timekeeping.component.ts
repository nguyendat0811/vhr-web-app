import { Component, OnInit, ViewChild } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute, NavigationEnd,Router } from '@angular/router';
import { RESOURCE, ACTION_FORM, EmpTimekeepingService, SystemParameterService, APP_CONSTANTS, EmpTypeBean } from '@app/core';
import { Validators } from '@angular/forms';
import { ValidationService, CommonUtils } from '@app/shared/services';
import * as moment from 'moment';
import { AppComponent } from '@app/app.component';

@Component({
  selector: 'timekeeping',
  templateUrl: './timekeeping.component.html'
})
export class TimekeepingComponent extends BaseComponent implements OnInit {
  public validHours = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'];
  public currentItem;
  public listDateSearch = [];
  public listHeaderDate = [];
  public moment = moment;
  private currentEvent: any;
  public listWorkdayTypeCode: string;
  public annualCode: any;
  private navigationSubscription;

  private formSearchConfig = {
    organizationId: ['', [ValidationService.required]],
    employeeId: [''],
    startDate: [moment(new Date()).startOf('day').startOf('month').toDate().getTime(), [ValidationService.required]],
    endDate: [moment(new Date()).startOf('day').endOf('month').toDate().getTime(), [ValidationService.required]],
  };
  @ViewChild('ptable') dataTable: any;

  constructor(public actr: ActivatedRoute
            , private router: Router
            , private empTimekeepingService: EmpTimekeepingService
            , private sysParamService: SystemParameterService
            , private app: AppComponent) {
    super(actr, RESOURCE.EMP_TIMEKEEPING, ACTION_FORM.SEARCH);
    this.formSearch = this.buildForm({}, this.formSearchConfig, ACTION_FORM.SEARCH,
        [ValidationService.notAffter('startDate', 'endDate', 'app.empTimekeeping.endDate')]);
// lay idOrg để tìm kiếm khi import xong
    this.navigationSubscription = this.router.events.subscribe((event: any) => {
      if (event instanceof NavigationEnd) {
        this.actr.params.subscribe(params => {
          if (params && params.orgId) {
            this.formSearch.controls['organizationId'].setValue(`${params.orgId}`);
          }
          this.processSearchTimekeeping();
        });
      }
    });
  //
  }
  get f () {
    return this.formSearch.controls;
  }
  ngOnInit() {
  }
  /**
   * xu ly tim kiem bang cham cong
   */
  processSearchTimekeeping(event?) {
    if (!CommonUtils.isValidForm(this.formSearch)) {
      return;
    }
    this.currentEvent = event;
    const params = this.formSearch ? this.formSearch.value : null;
    this.empTimekeepingService.search(params, this.currentEvent).subscribe(res => {
      this.resultList = this.groupItemByProperty(res, 'organizationId');
      this.listWorkdayTypeCode = this.makeListWorkdayTypeCode(res);
      this.annualCode = res.extendData.lstAnnualCode + '';
      this.initListDateSearch(params.startDate, params.endDate);
    });
    if(!this.currentEvent){
      if (this.dataTable) {
        this.dataTable.first = 0;
      }
    }
  }
  processSaveTimekeeping() {
    this.app.confirmMessage(null, () => {// on accepted
      const data = {et: this.getDataTimekeeping()};
      this.empTimekeepingService.saveOrUpdate(data)
       .subscribe(res => {
         this.processSearchTimekeeping(this.currentEvent);
        });
    }, () => {
      // on rejected
    });
  }
  private getDataTimekeeping() {
    const data = [];
    const listMarkedInput = document.getElementsByClassName('markedInput');
    Array.prototype.forEach.call(listMarkedInput, function(el) {
      const markedInput = (<HTMLInputElement> el);
      const name = markedInput.name.split('_');
      const etForm = {'dateTimekeeping': name[1], 'workProcessId': name[2], 'etStr': markedInput.value};
      data.push(etForm);
    });
    return data;
  }
  makeListWorkdayTypeCode(res): string {
    const a = [];
    for (const workdayType of res.extendData.listWorkdayType) {
      a.push(workdayType.code.toUpperCase());
    }
    return '|' + a.join('|') + '|';
  }
  initListDateSearch(startSearh, endSearh) {
    this.listDateSearch = [];
    this.listHeaderDate = [];
    const startDate = moment(startSearh);
    const endDate = moment(endSearh);
    const itemDate = startDate;
    let currentMonth = '';
    while (itemDate.isSameOrBefore(endDate)) {
      const format = itemDate.format('MM/YYYY');
      let listByHeader = [];
      if (currentMonth !== format) {
        currentMonth = format;
        listByHeader.push(itemDate.clone());
        this.listHeaderDate.push(listByHeader);
      } else {
        listByHeader = this.listHeaderDate[this.listHeaderDate.length - 1];
        listByHeader.push(itemDate.clone());
        this.listHeaderDate[this.listHeaderDate.length - 1] = listByHeader;
      }
      this.listDateSearch.push(itemDate.clone());
      itemDate.add(1, 'day');
    }
  }
  /**
   * groupItemByProperty
   * @ param res
   * @ param propertyName
   */
  groupItemByProperty(res, propertyName: string): any {
    const groupItem = [];
    if (!res.data) {
      res.groupItem = groupItem;
      return res;
    }
    let currId = 0;
    let index = parseInt(res.first);
    for (const item of res.data) {
      item.index = ++index;
      if (currId !== item[propertyName]) {
        currId = item[propertyName];
        const group = [];
        group.push(item);
        groupItem.push(group);
      } else {
        groupItem[groupItem.length - 1].push(item);
      }
    }
    res.groupItem = groupItem;
    return res;
  }


  markThisInput(event, empId, usedAnnual, currentAnnualTotal) {
    const inputObj = event.inputObj;
    const timekeepingStr = event.timekeepingStr;
    const checkAnnualCode = '' + this.annualCode;

    let value = inputObj.value;
    value = value.toUpperCase();
    value = CommonUtils.tctReplaceAll(value, ' ', '');
    value = CommonUtils.tctReplaceAll(value, ';', ',');
    inputObj.value = value;

    let check = true;
    let congChuan = '' + this.listWorkdayTypeCode;
    let congChecked = '';
    if (value !== '') {
      const str = value.split(',');
      let sumHours = 0;
      for (let i = 0; i < str.length; i++) {
        const workDay = str[i].split(':');
        if (workDay.length !== 2) {
          this.app.messageError('ERROR', 'app.empTimekeeping.errorTimekeeping' , value);
          check = false;
          break;
        } else {
          const cong = CommonUtils.trim(workDay[0]);
          // neu nhập trùng loại ngày công -thaopv
          if ((congChecked.indexOf('|' + cong.toUpperCase() + '|') >= 0)) {
            this.app.messageError('ERROR', 'timekeeping.empTimekeeping.erorrDuplicateWorkDayType' , value);
            check = false;
            break;
          } else {
            if (!(congChuan.indexOf('|' + cong.toUpperCase() + '|') >= 0)) {
              this.app.messageError('ERROR', 'empTimekeepingLock.attendanceTypeDoNotExits' , value);
              check = false;
              break;
            } else {
              try {
                const hours = workDay[1];
                if (this.validHours.indexOf(hours.replace(/^0+/, '')) < 0) {
                  this.app.messageError('ERROR', 'app.empTimekeeping.workDayInvalidTime' , value);
                  check = false;
                  break;
                }
                sumHours += parseFloat(hours);
                if (sumHours > 24) {
                  this.app.messError('ERROR', 'app.empTimekeeping.workDayTimeOver');
                  check = false;
                  break;
                }

          // kienpt
                // neu la loai cong nghi thi moi kiem tra
              if (checkAnnualCode.indexOf(cong.toUpperCase()) >= 0) {
                if (this.overAnnualLeave(empId,usedAnnual, currentAnnualTotal)) {
                  this.app.messageError('ERROR', 'empTimekeepingLock.overAnnualLeave' , value);
                  check = false;
                  break;
                }
               }
          // end
              } catch (e) {
                this.app.messageError('ERROR', 'app.empTimekeeping.errorTimekeeping' , value);
                check = false;
                break;
              }
            }
          }
          congChecked = congChecked + '|' + cong.toUpperCase() + '|';
          congChuan = CommonUtils.tctReplaceAll('|' + congChuan + '|', '|' + cong.toUpperCase() + '|', '|');
        }
      }
    }

    if (!check) {
        inputObj.value = '';
        setTimeout(function() { inputObj.focus(); }, 10);
    } else {
      if (timekeepingStr !== value) {
        if (!(inputObj.className.indexOf('markedInput') >= 0)) {
          inputObj.className = inputObj.className + ' markedInput';
        }
      } else {
        inputObj.classList.remove('markedInput');
      }
    }
  }

  moveBetweenCell(event) {
    const evt = event.evt;
    const inputObj = event.inputObj;
    let rowIndex = event.rowIndex;
    let colIndex = event.colIndex;
    // list code nghi phep
    try {
        let tempObj;
        switch (evt.which) {
            case 40: // Down
                rowIndex++;
                document.getElementById('et_' + rowIndex + '_' + colIndex).focus();
                return false;
            case 38: // Up
                rowIndex--;
                document.getElementById('et_' + rowIndex + '_' + colIndex).focus();
                return false;
            case 37: // Left
                if (inputObj.selectionStart === 0) {
                    colIndex--;
                    tempObj = document.getElementById('et_' + rowIndex + '_' + colIndex);
                    if (tempObj != null) {
                        tempObj.focus();
                        tempObj.selectionStart = tempObj.value.length;
                    }
                    return false;
                } else {
                    return true;
                }
            case 39: // Right
                if (inputObj.selectionStart === inputObj.value.length) {
                    colIndex++;
                    tempObj = document.getElementById('et_' + rowIndex + '_' + colIndex);
                    if (tempObj != null) {
                        tempObj.focus();
                        tempObj.selectionStart = 0;
                    }
                    return false;
                } else {
                    return true;
                }
            default:
                return true;
        }
    } catch (ex) {
        alert(ex.message);
    }
  }

  overAnnualLeave(empId, usedAnnual, currentAnnualTotal ) {
    const usedAnnualWP = this.getSumAnnualbyWorkProcess(empId);
    const emp = ".et" + ".ID" + empId;
    const listTimeKeeping = document.querySelectorAll(emp);

    // tinh tong phep da nghi
    let SumAnnual = 0;
    const checkAnnualCode = '' + this.annualCode;

    Array.prototype.forEach.call(listTimeKeeping, function(el) {
      const markedInput = (<HTMLInputElement> el);
      let listET = markedInput.value;
      listET = CommonUtils.tctReplaceAll(listET, ' ', '');
      listET = CommonUtils.tctReplaceAll(listET, ';', ',');

      // tach ra cac phan tu ngay cong
      const str = listET.split(',');
      for (let i = 0; i < str.length; i++) {
            const etStr = str[i].split(':');
            // can thay the da thi truong
            if ( checkAnnualCode.indexOf('|'+CommonUtils.trim(etStr[0].toUpperCase()+'|')) >= 0 ) // this.annualCode.indexOf(etStr[0].toUpperCase());
            {
              SumAnnual += parseFloat(etStr[1]);
            }
          }
    });
    SumAnnual = SumAnnual / 8.0;
    // Do chenh lech ngay nghi:  SumAnnual - alRemainingWP
    const  DifferentialAnnual = SumAnnual - usedAnnualWP;
    // Tong so ngay nghi can validate:  DifferentialAnnual +
    const newAnnualTotal = DifferentialAnnual + usedAnnual;
    if (newAnnualTotal > currentAnnualTotal ) {
      return true;
    }
    return false;

  }

  // Lay ra tong ngay nghi theo qua trinh cua nhan vien do o trang do
  getSumAnnualbyWorkProcess(empId) {
    let Sum = 0;
      for (const item of this.resultList.data) {
        if (item.employeeId === empId) {
          Sum += item.alRemainingWP;
        }
      }

    return Sum;
  }

}
