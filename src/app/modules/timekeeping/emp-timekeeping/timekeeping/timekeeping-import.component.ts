import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute, Router } from '@angular/router';
import { RESOURCE, ACTION_FORM, EmpTimekeepingService } from '@app/core';
import { Validators, FormGroup } from '@angular/forms';
import { ValidationService, CommonUtils } from '@app/shared/services';
import * as moment from 'moment';
import { AppComponent } from '@app/app.component';
import { FileControl } from '@app/core/models/file.control';
import { TranslationService } from 'angular-l10n';

@Component({
  selector: 'timekeeping-import',
  templateUrl: './timekeeping-import.component.html'
})
export class TimekeepingImportComponent extends BaseComponent implements OnInit {
  public moment = moment;
  public formImport: FormGroup;
  public dataError: any;
  private formImportConfig = {
    organizationId: ['', [ValidationService.required]],
    startDate: [moment(new Date()).startOf('month').toDate().getTime(), [ValidationService.required]],
    endDate: [moment(new Date()).endOf('month').toDate().getTime(), [ValidationService.required]],
  };
  constructor(public actr: ActivatedRoute
            , private service: EmpTimekeepingService
            , private router: Router
            , private translationService: TranslationService
            , private app: AppComponent) {
    super(actr, RESOURCE.EMP_TIMEKEEPING, ACTION_FORM.IMPORT);
    this.formImport = this.buildForm({}, this.formImportConfig, ACTION_FORM.IMPORT,
        [ValidationService.isRangeOf6Month('startDate', 'endDate', 'app.empTimekeeping.endDate')
        , ValidationService.notAffter('startDate', 'endDate', 'app.empTimekeeping.endDate')
        ]);
    this.formImport.addControl('fileImport', new FileControl(null, ValidationService.required));
  }
  get f () {
    return this.formImport.controls;
  }
  ngOnInit() {
  }
  processImportTimekeeping() {
    this.formImport.controls['fileImport'].updateValueAndValidity();
    if (!CommonUtils.isValidForm(this.formImport)) {
      return;
    }
    this.app.confirmMessage(null, () => {// on accepted
      this.service.processImport(this.formImport.value).subscribe(res => {
        if (!this.service.requestIsSuccess(res)) {
          this.dataError = res.data;

          /// truong hop row va colum vao o dau tien "STT" thi khong hien thi, phuc vu cho validate ngay cong
          this.dataError.forEach(element => {
            if(element.row == 8 && element.column == 1) {
              element.row  = null;
              element.columnLabel = null;
            }
          });
        } else {
          this.dataError = null;
          this.router.navigate(['/timekeeping/emp-timekeeping/search', this.formImport.get('organizationId').value]);
        }
      });
    }, () => {
      // on rejected
    });
  }
  processDownloadTemplate() {
    this.formImport.controls['fileImport'].clearValidators();
    this.formImport.controls['fileImport'].updateValueAndValidity();
    if (!CommonUtils.isValidForm(this.formImport)) {
      return;
    }
    const params = this.formImport.value;
    delete params['fileImport'];
    this.service.downloadTemplateImport(params).subscribe(res => {
      saveAs(res, 'template_timekeeping.xls');
    });
    this.formImport.controls['fileImport'].setValidators(ValidationService.required);
  }

}
