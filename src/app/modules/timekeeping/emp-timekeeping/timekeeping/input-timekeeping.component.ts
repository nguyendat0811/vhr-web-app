import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'input-timekeeping',
  template: '<input type="text" value="{{timekeepingStr}}" \
              id="{{styleId}}" \
              style="text-align: left" \
              (change)="markThisInput($event)" \
              (keyup)="moveBetweenCell($event)" \
              [disabled]="isLock" \
              [attr.name]="styleName" \
              class="{{class}}" \
              [ngClass]="{outOfWorkProcess: isOutOfWorkProcess}" \
              >',
})
export class InputTimekeepingComponent implements OnInit {
  public moment = moment;
  @Input()
  public styleId: string;
  @Input()
  public styleName: string;
  @Input()
  public class: string;
  @Input()
  public rowIndex: number;
  @Input()
  public colIndex: number;
  @Input()
  public dateTimekeeping: number;
  @Input()
  public extendData: any;
  @Input()
  public item: any;
  @Output()
  public onMove: EventEmitter<any> = new EventEmitter<any>();
  @Output()
  public onThisInput: EventEmitter<any> = new EventEmitter<any>();

  public workProcessId: number;
  public isLock: number;
  public isOutOfWorkProcess: boolean;
  public listTimeKeeping: any;
  public timekeepingStr: string;

  constructor() {
  }
  ngOnInit(): void {
    const now = new Date().getTime();
    this.workProcessId = this.item.workProcessId;
    const lockKey = this.dateTimekeeping + '_' + this.item.organizationId;
    const timeKeepingKey = this.dateTimekeeping + '_' + this.workProcessId;
    this.isLock = this.extendData.mapDateOrgLock.hasOwnProperty(lockKey) ?
                  this.extendData.mapDateOrgLock[lockKey] : 0;
    if (this.dateTimekeeping > now) {
      this.isLock = 1;
    }
    this.listTimeKeeping = this.extendData.mapEmpTimekeeping.hasOwnProperty(timeKeepingKey) ?
                           this.extendData.mapEmpTimekeeping[timeKeepingKey] : [];
    this.timekeepingStr = this.initTimekeepingStr();
    this.isOutOfWorkProcess = false;
    if (this.item.effectiveDate > this.dateTimekeeping) {
      this.isOutOfWorkProcess = true;
    } else if (this.item.expiredDate != null && this.item.expiredDate < this.dateTimekeeping) {
      this.isOutOfWorkProcess = true;
    }
  }
  initTimekeepingStr(): string {
    if (this.listTimeKeeping.length === 0) {
      return '';
    }
    const list = [];
    for (const timekeeping of this.listTimeKeeping) {
      list.push(timekeeping.workdayTypeCode.toUpperCase() + ':' + timekeeping.hoursWork);
    }
    return list.join(',');
  }
  moveBetweenCell(evt) {
    const e = {evt: evt, inputObj: evt.target, rowIndex: this.rowIndex, colIndex: this.colIndex};
    this.onMove.emit(e);
  }
  markThisInput(evt) {
    const e = {evt: evt, inputObj: evt.target, timekeepingStr: this.timekeepingStr};
    this.onThisInput.emit(e);
  }
}
