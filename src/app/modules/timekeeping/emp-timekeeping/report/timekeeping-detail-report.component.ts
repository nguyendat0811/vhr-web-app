import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { RESOURCE, ACTION_FORM, EmpTimekeepingService, WorkdayTypeService } from '@app/core';
import { Validators } from '@angular/forms';
import { ValidationService, CommonUtils } from '@app/shared/services';
import * as moment from 'moment';
import { AppComponent } from '@app/app.component';
import { EmpTimekeepingReportService } from '@app/core/services/hr-timekeeping/emp-timekeeping-report.service';

@Component({
  selector: 'timekeeping-detail-report',
  templateUrl: './timekeeping-detail-report.component.html'
})
export class TimekeepingDetailReportComponent extends BaseComponent implements OnInit {
  public validHours = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'];
  public currentItem;
  public listDateSearch = [];
  public listHeaderDate = [];
  public moment = moment;
  public attendanceCodeList: any;
  private formConfig = {
    organizationId: ['', [ValidationService.required]],
    listWorkdayTypeCode: [''],
    employeeId: [''],
    startDate: [moment(new Date()).startOf('day').startOf('month').toDate().getTime(), [ValidationService.required]],
    endDate: [moment(new Date()).startOf('day').endOf('month').toDate().getTime(), [ValidationService.required]],
  };


  constructor(public actr: ActivatedRoute
            , private reportService: EmpTimekeepingReportService
            , private timekeepingService: EmpTimekeepingService
            , private workdayTypeService: WorkdayTypeService
            , private app: AppComponent) {
    super(actr, RESOURCE.EMP_TIMEKEEPING, ACTION_FORM.DETAIL_REPORT);
    this.formSearch = this.buildForm({}, this.formConfig, ACTION_FORM.DETAIL_REPORT,
        [ValidationService.notAffter('startDate', 'endDate', 'app.empTimekeeping.endDate')]);
    this.workdayTypeService.findAllWorkdayTypeByMarketId().subscribe(
      res => {
        this.attendanceCodeList = res.data;
      });
  }
  get f () {
    return this.formSearch.controls;
  }
  ngOnInit() {
  }

  processExportDetail() {
    if (!CommonUtils.isValidForm(this.formSearch)) {
      return;
    }
    const submitData = {};
    submitData['listWorkdayTypeCode'] = this.formSearch.controls['listWorkdayTypeCode'].value;
    const params = this.formSearch.value;
    this.reportService.processDetailReport(params).subscribe(res => {
      saveAs(res, 'timekeeping_detail_report.xls');
    });
  }
}
