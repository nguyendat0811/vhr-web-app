import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { RESOURCE, ACTION_FORM, EmpTimekeepingService } from '@app/core';
import { Validators } from '@angular/forms';
import { ValidationService, CommonUtils } from '@app/shared/services';
import * as moment from 'moment';
import { AppComponent } from '@app/app.component';
import { EmpTimekeepingReportService } from '@app/core/services/hr-timekeeping/emp-timekeeping-report.service';

@Component({
  selector: 'timekeeping-summary-report',
  templateUrl: './timekeeping-summary-report.component.html'
})
export class TimekeepingSummaryReportComponent extends BaseComponent implements OnInit {
  public validHours = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'];
  public currentItem;
  public listDateSearch = [];
  public listHeaderDate = [];
  public moment = moment;
  public listWorkdayTypeCode: string;
  private formSearchConfig = {
    organizationId: ['', [ValidationService.required]],
    isGroupBy: [1],
    employeeCode: [''],
    startDate: [moment(new Date()).startOf('day').startOf('month').toDate().getTime(), [ValidationService.required]],
    endDate: [moment(new Date()).startOf('day').endOf('month').toDate().getTime(), [ValidationService.required]],
  };
  constructor(public actr: ActivatedRoute
            , private reportService: EmpTimekeepingReportService
            , private app: AppComponent) {
    super(actr, RESOURCE.EMP_TIMEKEEPING, ACTION_FORM.SUMMARY_REPORT);
    this.formSearch = this.buildForm({}, this.formSearchConfig, ACTION_FORM.SEARCH,
        [ValidationService.notAffter('startDate', 'endDate', 'app.empTimekeeping.endDate')]);
  }
  get f () {
    return this.formSearch.controls;
  }
  ngOnInit() {
  }
  /**
   * xu ly tim kiem bang cham cong
   */
  processSearchTimekeeping(event?) {
    if (!CommonUtils.isValidForm(this.formSearch)) {
      return;
    }

    const params = this.formSearch ? this.formSearch.value : null;
    this.reportService.search(params, event).subscribe(res => {
      this.initListDateSearch(params.startDate, params.endDate);
      this.resultList = this.groupItemByProperty(res, 'organizationId');
    });
  }
  /**
   * groupItemByProperty
   * @ param res
   * @ param propertyName
   */
  groupItemByProperty(res, propertyName: string): any {
    const groupItem = [];
    if (!res.data) {
      res.groupItem = groupItem;
      return res;
    }
    let currId = 0;
    let index = parseInt(res.first);
    for (const item of res.data) {
      item.index = ++index;
      item.timkeepingStr = this.initTimekeepingStr(item, res);
      if (currId !== item[propertyName]) {
        currId = item[propertyName];
        const group = [];
        group.push(item);
        groupItem.push(group);
      } else {
        groupItem[groupItem.length - 1].push(item);
      }
    }
    res.groupItem = groupItem;
    return res;
  }
  initListDateSearch(startSearh, endSearh) {
    this.listDateSearch = [];
    this.listHeaderDate = [];
    const startDate = moment(startSearh);
    const endDate = moment(endSearh);
    const itemDate = startDate;
    let currentMonth = '';
    while (itemDate.isSameOrBefore(endDate)) {
      const format = itemDate.format('MM/YYYY');
      let listByHeader = [];
      if (currentMonth !== format) {
        currentMonth = format;
        listByHeader.push(itemDate.clone());
        this.listHeaderDate.push(listByHeader);
      } else {
        listByHeader = this.listHeaderDate[this.listHeaderDate.length - 1];
        listByHeader.push(itemDate.clone());
        this.listHeaderDate[this.listHeaderDate.length - 1] = listByHeader;
      }
      this.listDateSearch.push(itemDate.clone());
      itemDate.add(1, 'day');
    }
  }
  initTimekeepingStr(item, res): any {
    const timkeepingStr = {};
    for (const date of this.listDateSearch) {
      const key = date.toDate().getTime();
      const timeKeepingKey = item.organizationId + '_' + item.employeeId + '_' + key;
      const listTimeKeeping = res.extendData.mapEmpTimekeeping.hasOwnProperty(timeKeepingKey) ?
                              res.extendData.mapEmpTimekeeping[timeKeepingKey] : [];
      if (listTimeKeeping.length === 0) {
        timkeepingStr[key] = '';
      }
      const list = [];
      for (const timekeeping of listTimeKeeping) {
        list.push(timekeeping.workdayTypeCode.toUpperCase() + ':' + timekeeping.hoursWork);
      }
      timkeepingStr[key] = list.join(',');
    }
    return timkeepingStr;
  }
  processExportSummary() {
    if (!CommonUtils.isValidForm(this.formSearch)) {
      return;
    }
    const params = this.formSearch.value;
    this.reportService.processReportSummary(params).subscribe(res => {
      saveAs(res, 'timekeeping_summary_report.xls');
    });
  }
}
