import { SharedModule } from '@app/shared';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmpTimekeepingRoutingModule } from './emp-timekeeping-routing.module';
import { TimekeepingComponent } from './timekeeping/timekeeping.component';
import { InputTimekeepingComponent } from './timekeeping/input-timekeeping.component';
import { TimekeepingImportComponent } from './timekeeping/timekeeping-import.component';
import { TimekeepingSummaryReportComponent } from './report/timekeeping-summary-report.component';
import { TimekeepingDetailReportComponent } from './report/timekeeping-detail-report.component';
import { TimeKeepingUpdateComponent } from './update/timekeeping-update.component';


@NgModule({

  declarations: [TimekeepingComponent, InputTimekeepingComponent, TimekeepingImportComponent
    , TimekeepingSummaryReportComponent, TimekeepingDetailReportComponent, TimeKeepingUpdateComponent],

  imports: [
    CommonModule,
    SharedModule,
    EmpTimekeepingRoutingModule,
  ],
  entryComponents: []
})
export class EmpTimekeepingModule { }
