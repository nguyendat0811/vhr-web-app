import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/dashboard/time_keeping',
    pathMatch: 'full'
  }, {
    path: 'attendance-type',
    loadChildren: './attendance-type/attendance-type.module#AttendanceTypeModule'
  }, {
    path: 'lock-unlock',
    loadChildren: './lock-unlock/lock-unlock.module#LockUnlockModule'
  }, {
    path: 'emp-timekeeping',
    loadChildren: './emp-timekeeping/emp-timekeeping.module#EmpTimekeepingModule'
  }, {
    path: 'work-schedule',
    loadChildren: './work-schedule/work-schedule.module#WorkScheduleModule'
  }, {
    path: 'annual-leave',
    loadChildren: './annual-leave/annual-leave.module#AnnualLeaveModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TimekeepingRoutingModule { }
