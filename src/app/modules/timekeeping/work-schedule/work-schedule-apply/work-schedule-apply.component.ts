
import { forEach } from '@angular/router/src/utils/collection';
import { Component, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { WorkScheduleService } from '@app/core/services/hr-timekeeping/work-schedule.service';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormArray, FormControl } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from '@app/app.component';

@Component({
  selector: 'work-schedule-apply',
  templateUrl: './work-schedule-apply.component.html',
})
export class WorkScheduleApplyComponent extends BaseComponent implements OnInit {
  @ViewChildren('inputSearch') inputSearch;
  workScheduleId: number;
  resultList: any;
  formOrgList: FormArray;
  isChecked: boolean;
  lstWorkScheduleSelected: any;
  constructor(private workScheduleService: WorkScheduleService
            , public actr: ActivatedRoute
            , public activeModal: NgbActiveModal
            , private app: AppComponent) {
    super(actr);
    this.setMainService(workScheduleService);
  }

  ngOnInit() {
  }

  setFormValue(workScheduleId, resultList) {
    this.resultList = resultList;
    this.workScheduleId = workScheduleId;
  }

  processApply() {
    const lstWorkScheduleIdNotApply = [];
    if (this.lstWorkScheduleSelected) {
      this.lstWorkScheduleSelected.forEach(x=> {
        lstWorkScheduleIdNotApply.push(x.workScheduleId);
      });
    }
    this.app.confirmMessage('workSchedule.apply.defaultMessageComfirm', () => {
      this.workScheduleService.processApply(this.workScheduleId, {lstWorkScheduleIdNotApply: lstWorkScheduleIdNotApply}).subscribe(
        res => {
          if (this.workScheduleService.requestIsSuccess(res)) {
            this.activeModal.close(res);
          }
        }
      );
    }, null );
  }
}
