import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { Validators, FormArray } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { HolidayService } from '@app/core/services/hr-timekeeping/holiday.service';
import { RESOURCE, ACTION_FORM, DEFAULT_MODAL_OPTIONS } from '@app/core';
import { CommonUtils } from '@app/shared/services';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { HolidayDefinitionCloneComponent } from '../../attendance-type/holiday-definition-in-market/holiday-definition-clone/holiday-definition-clone.component';

@Component({
  selector: 'saturday-calendar',
  templateUrl: './saturday-calendar.component.html'
})
export class SaturdayCalendarComponent extends BaseComponent implements OnInit {
  public lstDateTimekeeping: FormArray;
  public mapDateTimekeepingToTal: {} ;
  public list: any;
  public dateStart: any;
  public endDate: any;
  public listMonth = [];
  month: string;
  holidayMap: any;
  listYear: Array<any>;
  formConfig = {
    lstSaturday: [this.list],
    year: [parseInt(moment(new Date()).format('YYYY'))],
  };
  constructor(public actr: ActivatedRoute
    , private modalService: NgbModal
    , public activeModal: NgbActiveModal
    , public holidayService: HolidayService) {
    super(actr, RESOURCE.WORK_SCHEDULE, ACTION_FORM.SEARCH);
    this.formSearch = this.buildForm({}, this.formConfig);
  }
  ngOnInit() {
    this.processSearchByYear();
  }

  public onChangeHoliday(event) {
    if (!event) {
      return;
    }
    if (this.holidayService.requestIsSuccess(event)) {
      this.listMonth = [];
      setTimeout(() => {
        this.processSearchByYear();
      }, 200);
    }
  }
  public processSearchByYear() {
    if (!CommonUtils.isValidForm(this.formSearch)) {
      return;
    }
    const year = this.formSearch.controls['year'].value;
    this.holidayService.findByYear(year).subscribe(res => {
      if (this.holidayService.requestIsSuccess(res)) {
        this.initListMonthSearch();
        this.holidayMap = this.buildMap(res.data);
      }
    });
  }

  private getYearList() {
    this.listYear = [];
    const currentYear = new Date().getFullYear();
    for (let i = (currentYear - 50) ; i <= (currentYear + 50) ; i++ ) {
      const obj = {
        year: i
      };
      this.listYear.push(obj);
    }
    return this.listYear;
  }

  private setYearList(effectiveDate, expiredDate) {
    // set nam dau
    if(effectiveDate != null){
      this.dateStart = effectiveDate;
      effectiveDate = parseInt(moment(effectiveDate).format('YYYY'));
    } else{
      effectiveDate = parseInt(moment(new Date()).format('YYYY'));
      this.dateStart = new Date();
    }
    // set nam cuoi
    if( expiredDate != null){
      this.endDate = expiredDate;
      expiredDate = parseInt(moment(expiredDate).format('YYYY'));
    } else{
      expiredDate = effectiveDate + 20;
      this.endDate = moment('31/12/' + expiredDate, 'DD/MM/YYYY');
    }
    this.listYear = [];
    for (let i = (effectiveDate) ; i <= (expiredDate) ; i++ ) {
      const obj = {
        year: i
      };
      this.listYear.push(obj);
    }
  }
  get f () {
    return this.formSearch.controls;
  }

  /**
   * initListMonthSearch
   * @ param year
   */
  initListMonthSearch() {
    const year = this.formSearch.controls['year'].value;
    this.listMonth = ['01/' + year, '02/' + year, '03/' + year, '04/' + year
                    , '05/' + year, '06/' + year, '07/' + year, '08/' + year
                    , '09/' + year, '10/' + year, '11/' + year, '12/' + year ];
  }
  buildMap(data): any {
    const map = {};
    if (!data) {
      return map;
    }

    for (const holiday of data) {
      let currList = [];
      if (map.hasOwnProperty(holiday.holidayDate)) {
        currList = map[holiday.holidayDate];
      }
      currList.push(holiday);
      map[holiday.holidayDate] = currList;
    }
    return map;
  }

  closePopup() {
   this.activeModal.close();
  }
  public setLstDateTimekeeping(lstDateTimekeeping: FormArray) {
    this.lstDateTimekeeping = lstDateTimekeeping;
  }
  public setLstDateTimekeepingToTal(mapDateTimekeepingToTal: FormArray) {
    this.mapDateTimekeepingToTal = mapDateTimekeepingToTal;
  }
}


