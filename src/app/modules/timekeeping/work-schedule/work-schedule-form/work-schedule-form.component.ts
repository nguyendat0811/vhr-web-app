import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RESOURCE, ACTION_FORM, APP_CONSTANTS, WorkdayTypeService, DEFAULT_MODAL_OPTIONS } from '@app/core';
import { CommonUtils, ValidationService } from '@app/shared/services';
import { Validators, FormArray, FormGroup, FormControl } from '@angular/forms';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { WorkScheduleService } from '@app/core/services/hr-timekeeping/work-schedule.service';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { AppComponent } from '@app/app.component';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SaturdayCalendarComponent} from '../saturday-calendar/saturday-calendar.component';
import { TranslationService } from 'angular-l10n';
@Component({
  selector: 'work-schedule-form',
  templateUrl: './work-schedule-form.component.html',
})
export class WorkScheduleFormComponent extends BaseComponent implements OnInit {
  applyCount: any;
  mapDateTimekeepingToTal: any = {};
  lstDateTimekeepingToTal: any = {};
  workdayTypeList: any = {};
  list: any;
  listFormType: any;
  listAttendanceConfig: any;
  formWorkSchedule: FormGroup;
  formTableConfig: FormArray;
  formTableWeekendConfig: FormArray;
  isHidden: boolean;
  hasSatConf: any;
  checkedFlag: any;
  workScheduleId: any;
  public mapDayChosen = {};
  listDayError: any;
  formConfig = {
    workScheduleId: [null],
    formType: ['',[ValidationService.required]],
    type: [null, [ValidationService.required, ValidationService.maxLength(50)]],
    organizationId: ['', [ValidationService.required]],
    effectiveDate: [null, [ValidationService.required]],
    expiredDate: [null, [ValidationService.afterCurrentDate]],
    saturdayType: [null],
    applyCount: ['']
  };
  formSaveTableConfig = {
    workdayTypeId: [''],
    lstDayOfWeek: ['', [ValidationService.required]],
    workdayTypeCode: [null, [ValidationService.required]],
    hours: ['', [ValidationService.required, ValidationService.positiveInteger, Validators.max(10), Validators.min(1)]],
  };
  formSaveWeekendTableConfig = {
    workdayTypeId: [''],
    hours: ['', [ValidationService.required, ValidationService.positiveInteger, Validators.max(10), Validators.min(1)]],
    workdayTypeCode: [null, [ValidationService.required]],
  };
  dayOfWeekList: any;
  attendanceFormList: any;
  type: any;
  constructor(public actr: ActivatedRoute
            , public translationService: TranslationService
            , private router: Router
            , private workScheduleService: WorkScheduleService
            , private modalService: NgbModal
            , public activeModal: NgbActiveModal
            , private workdayTypeService: WorkdayTypeService
            , private app: AppComponent) {
    super(actr, RESOURCE.WORK_SCHEDULE, ACTION_FORM.SEARCH);
    this.setMainService(WorkScheduleService);
    this.formWorkSchedule = this.buildForm({saturdayType : 1}, this.formConfig, ACTION_FORM.INSERT
    , [ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.contractProcess.expiredDate')]);
     this.buildFormSaveConfig();
     this.buildFormSaveWeekendConfig();
     this.dayOfWeekList = APP_CONSTANTS.DAY_OF_WEEK;
     this.attendanceFormList = APP_CONSTANTS.ATTENDANCE_FORM;
     this.listAttendanceConfig = APP_CONSTANTS.ATTENDANCE_CONFIGURATION;
     this.workdayTypeService.findAllWorkdayTypeByMarketId().subscribe(
      res => {
        this.workdayTypeList = res.data;
      });
    this.isHidden = true;
   }

  ngOnInit() {
    const params = this.actr.snapshot.params;
    if (params && CommonUtils.isValidId(params.workScheduleId)) {
      this.workScheduleId = params.workScheduleId;
    }
    if (!this.workScheduleId) {
      return;
    }
     // load thong tin chinh
     this.workScheduleService.findOne(this.workScheduleId).subscribe(res => {
      if (this.workScheduleService.requestIsSuccess(res)) {
        this.formWorkSchedule = this.buildForm(res.data
                                      , this.formConfig
                                      , ACTION_FORM.UPDATE
                                      , [ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.contractProcess.expiredDate')]);
        // kiem tra xem da apply chua
        this.applyCount = this.formWorkSchedule.get('applyCount').value;

        // kiem tra xem co cau hinh thu 7 khong
        this.hasSatConf = this.formWorkSchedule.get('type').value;
        if ( this.formWorkSchedule.controls['saturdayType'].value === 2){
          this.show();
        }
      }
    });

    this.workScheduleService.findAllDetail(this.workScheduleId).subscribe(res => {
      if (this.workScheduleService.requestIsSuccess(res)) {
        this.buildFormSaveConfig(res.data);
      }
    });

    this.workScheduleService.findAllWeekend(this.workScheduleId).subscribe(res => {
      if (this.workScheduleService.requestIsSuccess(res)) {
        this.initMapTotal(res.data);
        this.buildFormSaveWeekendConfig(res.data);
      }
    });
    // load thong tin bang cau hinh
  }


  // load bang cau hanhi


  get f () {
    return this.formWorkSchedule.controls;
  }
  get f1 () {
    return this.formTableConfig.controls;
  }
  get f2 () {
    return this.formTableWeekendConfig.controls;
  }

  public addRow(index: number, item: FormGroup) {
    const controls = this.formTableConfig as FormArray;
    controls.insert(index + 1, this.makeDefaultFormSaveConfig());
  }
  public removeRow(index: number, item: FormGroup) {
    const controls = this.formTableConfig as FormArray;
    if (controls.length === 1) {
      this.buildFormSaveConfig();
      const group = this.makeDefaultFormSaveConfig();
      controls.push(group);
      this.formTableConfig = controls;
    }
    controls.removeAt(index);
  }
  public addRowWeekend(index: number, item: FormGroup) {
    const controls = this.formTableWeekendConfig as FormArray;
    controls.insert(index + 1, this.makeDefaultFormSaveWeekendConfig());
  }
  public removeRowWeekend(index: number, item: FormGroup) {
    const controls = this.formTableWeekendConfig as FormArray;
    if (controls.length === 1) {
      this.buildFormSaveWeekendConfig();
      const group = this.makeDefaultFormSaveWeekendConfig();
      controls.push(group);
      this.formTableWeekendConfig = controls;
    }
    controls.removeAt(index);
  }

  private buildFormSaveConfig(list?: any) {
    if (!list) {
      this.formTableConfig = new FormArray([this.makeDefaultFormSaveConfig()]);
    } else {
      const controls = new FormArray([]);
      for (const i in list) {
        const formTableConfig = list[i];
        const group = this.makeDefaultFormSaveConfig();
        group.patchValue(formTableConfig);
        controls.push(group);
      }
      this.formTableConfig = controls;
    }
  }

  // BiWeekend\
  hide() {
      this.isHidden = true;
    }
  show() {
    this.isHidden = false;
  }
  weekendConfig() {
    if (this.isHidden) {
      this.isHidden = false;
    } else {   // If expired area is shown
      this.clearData();
      this.isHidden = true;
    }
  }
  private clearData() {
    this.formTableWeekendConfig.reset();
    this.isHidden = true;
  }
  private buildFormSaveWeekendConfig(list?: any) {
    if (!list || list.length <=0) {
      this.formTableWeekendConfig = new FormArray([this.makeDefaultFormSaveWeekendConfig()]);
    } else {
      const controls = new FormArray([]);
      for (const i in list) {
        const group = this.makeDefaultFormSaveWeekendConfig(1);
        const formTableWeekendConfig = list[i];
        group.patchValue(formTableWeekendConfig);
        const lstDateTimekeeping = new FormArray([]);
        if (formTableWeekendConfig.lstDateTimekeeping && formTableWeekendConfig.lstDateTimekeeping.length > 0) {
          for(let i = formTableWeekendConfig.lstDateTimekeeping.length - 1 ; i >= 0; i--) {
            const control = new FormControl(formTableWeekendConfig.lstDateTimekeeping[i]);
            lstDateTimekeeping.insert(0, control);
            // this.lstDateTimekeepingToTal.insert(0, control);
          }
        }
        group.addControl('lstDateTimekeeping', lstDateTimekeeping);
        controls.push(group);
      }
      this.formTableWeekendConfig = controls;
    }
  }
  private makeDefaultFormSaveConfig(): FormGroup {
    const formGroup = this.buildForm({}, this.formSaveTableConfig, ACTION_FORM.UPDATE);
    return formGroup;
  }
  private makeDefaultFormSaveWeekendConfig(num ?: any):  FormGroup {
    const formGroup = this.buildForm({}, this.formSaveWeekendTableConfig, ACTION_FORM.UPDATE);
    if (!num) {
      const lstDateTimekeeping = new FormArray([]);
      formGroup.addControl('lstDateTimekeeping', lstDateTimekeeping);
    }
    return formGroup;
  }

  private validateBeforeSave(): boolean {
    if (!this.isHidden) {
      const formWorkScheduleState = CommonUtils.isValidForm(this.formWorkSchedule);
      const formTableConfigState = CommonUtils.isValidForm(this.formTableConfig);
      const formTableWeekendConfigState = CommonUtils.isValidForm(this.formTableWeekendConfig);
      return formWorkScheduleState && formTableConfigState
          && !this.validateDayDuplicate() && !this.validateChooseDay()
          && formTableWeekendConfigState && !this.outOfRangeDay(1);
    } else {
      const formWorkScheduleState = CommonUtils.isValidForm(this.formWorkSchedule);
      const formTableConfigState = CommonUtils.isValidForm(this.formTableConfig);
      return  formWorkScheduleState
          && !this.validateDayDuplicate()
          && formTableConfigState;
    }
  }
  processSaveOrUpdate() {
    if (!this.validateBeforeSave()) {
      return;
    }
    // gan gia tri mac dinh cho saturdayType khi chon tu thu 2 den thu 6
    if ( this.hasSatConf === 1 ) {
      this.formWorkSchedule.controls['saturdayType'].setValue(null);
    }
      // kiem tra type
    const formSave = {};
    formSave['formWorkSchedule'] = this.formWorkSchedule.value;
    formSave['formTableConfig'] = this.formTableConfig.value;
    if (!this.isHidden) {
      formSave['formTableWeekendConfig'] = this.formTableWeekendConfig.value;
    }

    this.app.confirmMessage('', () => {// on accept
      this.workScheduleService.saveOrUpdate(formSave)
      .subscribe(res => {
        if (this.workScheduleService.requestIsSuccess(res)) {
          this.activeModal.close(res);
          const modalRef  = this.router.navigate(['/timekeeping/work-schedule']);
        }
      });
    }, () => {// on rejected

    });
  }

  prepareCalendar(lstDateTimekeeping) {
    if (!this.formWorkSchedule.value.effectiveDate) {
      this.app.errorMessage('workSchedule', 'requireEffectiveDate');
      return;
    }
    if (!CommonUtils.isNullOrEmpty(this.outOfRangeDay(2))) {
      this.app.confirmMessageError('app.payrolls.workSchedule.outOfRangeComfirmDelete', () => { // on accepted
          this.actionActiveModal(lstDateTimekeeping);
        }, () => { // on rejected
      } , this.outOfRangeDay(2) );
    } else {
      this.actionActiveModal(lstDateTimekeeping);
    }
  }

  actionActiveModal(lstDateTimekeeping) {
    const modalRef = this.modalService.open(SaturdayCalendarComponent, DEFAULT_MODAL_OPTIONS);
    modalRef.componentInstance.setLstDateTimekeeping(lstDateTimekeeping);
    modalRef.componentInstance.setLstDateTimekeepingToTal(this.mapDateTimekeepingToTal);

// tslint:disable-next-line: max-line-length
    modalRef.componentInstance.setYearList(this.formWorkSchedule.get('effectiveDate').value, this.formWorkSchedule.get('expiredDate').value);
    modalRef.result.then((result) => {
    });
   }

  printSelectedDate(formArray: FormArray) {
    const list = [];
    if (formArray && formArray.controls) {
      for (const control of formArray.controls) {
        list.push( moment(control.value).format('DD/MM/YYYY') );
      }
    }
    return list.join(', ');
  }

  validateDayDuplicate(): Boolean {
    for(let j in this.formTableConfig.value) {
      const form = this.formTableConfig.value[j];
      for (let i in form.lstDayOfWeek) {
        if (this.mapDayChosen[form.lstDayOfWeek[i]]) {
          this.mapDayChosen = {};
          this.app.messError('ERROR', 'app.payrolls.workSchedule.isDuplicated');
          return true;
        } else {
          this.mapDayChosen[form.lstDayOfWeek[i]] = form.lstDayOfWeek[i];
        }
      }
    }
    this.mapDayChosen = {};
    return false;
  }

  validateChooseDay(): Boolean {
    for(let j in this.formTableWeekendConfig.value) {
      const form = this.formTableWeekendConfig.value[j];
// tslint:disable-next-line: no-shadowed-variable
        if (!form.lstDateTimekeeping || form.lstDateTimekeeping.length === 0 ) {
            this.app.messError('ERROR', 'app.payrolls.workSchedule.notCalendar');
            return true;
      }
    }
    return false;
  }
  // hien thu cau hinh thu 7
  ChangeHasSatConf() {
    const  type = this.formWorkSchedule.get('type').value;
    this.hasSatConf = type;
    if (this.hasSatConf === 2) {
      if ( this.formWorkSchedule.controls['saturdayType'].value == null) {
        this.formWorkSchedule.controls['saturdayType'].setValue(1);
      }
      if ( this.formWorkSchedule.controls['saturdayType'].value === 2) {
        this.isHidden = false;
      }
    } else {
      this.isHidden = true;
    }
  }


  // kiem tra ngay chon trong lich nam ngoai khoang
  outOfRangeDay(type?: any) {
    const effect = this.formWorkSchedule.get('effectiveDate').value;
    const expired = this.formWorkSchedule.get('expiredDate').value;
    const list = this.formTableWeekendConfig.value;
    this.listDayError = [];
    for (const i in list) {
      const formTableWeekendConfig = list[i];
      if (formTableWeekendConfig.lstDateTimekeeping && formTableWeekendConfig.lstDateTimekeeping.length > 0) {
        for (let i = formTableWeekendConfig.lstDateTimekeeping.length - 1 ; i >= 0; i--) {
          const day = formTableWeekendConfig.lstDateTimekeeping[i];
          if (day < effect || ( day > expired && expired != null) ) {
              const dayError = moment(day).format('DD/MM/YYYY');
              this.listDayError.push(dayError);
          }
        }
        if ( type === 1) {
          if (this.listDayError.length > 0) {
            this.app.messError('ERROR', 'app.payrolls.workSchedule.outOfRange' , this.listDayError);
            //this.app.messError('ERROR', 'app.payrolls.workSchedule.outOfRange' , this.listDayError);
            return true;
          }
        } else {
          return this.listDayError;
        }
      }
    }
    return false;
  }

  // load map total trong lich
  initMapTotal(list) {
    for (const i in list) {
      const formTableWeekendConfig = list[i];
      if (formTableWeekendConfig.lstDateTimekeeping && formTableWeekendConfig.lstDateTimekeeping.length > 0) {
        for(let i = formTableWeekendConfig.lstDateTimekeeping.length - 1 ; i >= 0; i--) {
          const control = new FormControl(formTableWeekendConfig.lstDateTimekeeping[i]);
          this.mapDateTimekeepingToTal[control.value] = control;
          // this.lstDateTimekeepingToTal.insert(0, control);
        }
      }
    }
  }
}
