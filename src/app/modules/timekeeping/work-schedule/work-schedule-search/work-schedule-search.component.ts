import { Component, OnInit } from '@angular/core';
import { NgModule } from '@angular/core';
import { Routes, RouterModule, ActivatedRoute } from '@angular/router';
import { PropertyResolver } from '@app/shared/services/property.resolver';
import { RESOURCE, ACTION_FORM, APP_CONSTANTS, SysCatService, DEFAULT_MODAL_OPTIONS, MEDIUM_MODAL_OPTIONS } from '@app/core';
import { CommonUtils, ValidationService } from '@app/shared/services';
import { Validators, FormGroup, FormArray } from '@angular/forms';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { WorkScheduleService } from '@app/core/services/hr-timekeeping/work-schedule.service';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { AppComponent } from '@app/app.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { WorkScheduleFormComponent } from '../work-schedule-form/work-schedule-form.component';
import { WorkScheduleApplyComponent } from '../work-schedule-apply/work-schedule-apply.component';
import { WorkScheduleEmployeeComponent } from '../work-schedule-employee/work-schedule-employee.component';
@Component({
  selector: 'work-schedule-search',
  templateUrl: './work-schedule-search.component.html'
})
export class WorkScheduleSearchComponent extends BaseComponent implements OnInit {
  formSearch: FormGroup;
  listFormType: any;
  listAttendanceConfig: any;
  formConfig = {
    organizationId: [''],
    effectiveDate: [''],
    expiredDate: [''],
    type: [''],
    saturdayType: ['']
  };
  constructor(public actr: ActivatedRoute
            , private router: Router
            , private workScheduleService: WorkScheduleService
            , private sysCatService: SysCatService
            , private app: AppComponent
            , private modalService: NgbModal) {
    super(actr, RESOURCE.WORK_SCHEDULE, ACTION_FORM.SEARCH);
    this.setMainService(workScheduleService);
    this.listAttendanceConfig = APP_CONSTANTS.ATTENDANCE_CONFIGURATION;
    this.formSearch = this.buildForm({}, this.formConfig, ACTION_FORM.SEARCH
    , [ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.detailReport.toDate')]);
    // this.buildFormSave({});
    // this.buildSearchBasicFormGroup();
    this.processSearch();
   }

  ngOnInit() {
  }


  get f () {
    return this.formSearch.controls;
  }

  checkEffectiveDate(item) {
    const now = new Date();
    const expiredDate = new Date(item.expiredDate);
    now.setHours(0, 0, 0, 0);
    if (item.expiredDate === null) {
      return true;
    }
    return expiredDate.getTime() < now.getTime() ? false : true;
  }

// private buildSearchBasicFormGroup(data?: any) {
//   this.formSearch = this.buildForm(data, this.formConfig
//     , ACTION_FORM.SEARCH
//     , [ ValidationService.notAffter('dateBirthFrom', 'dateBirthTo', 'app.employee-update.dateBirthTo')]);
// }

processDelete(item) {
  if (item && item.workScheduleId > 0) {
    this.app.confirmDelete(null, () => {// on accepted
      this.workScheduleService.deleteById(item.workScheduleId)
      .subscribe(res => {
        if (this.workScheduleService.requestIsSuccess(res)) {
          this.processSearch(null);
        }
      });
    }, () => {// on rejected

    });
  }
}

prepareSaveOrUpdate(item) {
  console.log(item);
  if (item && item.workScheduleId > 0) {
    this.workScheduleService.findOne(item.workScheduleId)
      .subscribe(res => {
        console.log(res);
        if (res.data) {
          const modalRef  = this.router.navigate(['/timekeeping/work-schedule/edit', item.workScheduleId]);
        }
      });
  }
  // if (item && item.workScheduleId > 0) {
  //   this.workScheduleService.findOne(item.workScheduleId)
  //     .subscribe(res => {
  //       modalRef.componentInstance.setFormValue(this.propertyConfigs, res.data);
  //     });
  // } else {
    // modalRef.componentInstance.setFormValue(this.propertyConfigs);
  // }
  // modalRef.result.then((result) => {
  //   if (!result) {
  //     return;
  //   }
  //   if (this.workScheduleService.requestIsSuccess(result)) {
  //     this.processSearch(null);
  //   }
  //   });
}

  printSelectedDate(formArray: FormArray) {
    const list = [];
    if (formArray && formArray.controls) {
      for (const control of formArray.controls) {
        list.push( moment(control.value).format('DD/MM/YYYY') );
      }
    }
    return list.join(', ');
  }

  //bao cao

  processExportDetail() {
    if (!CommonUtils.isValidForm(this.formSearch)) {
      return;
    }
    // khoi tao gia tri cho truong null khi export
    if ( this.formSearch.get('saturdayType').value == null) {
      this.formSearch.controls['saturdayType'].setValue('');
    }
    if ( this.formSearch.get('type').value == null) {
      this.formSearch.controls['type'].setValue('');
    }
    //
    const params = this.formSearch.value;
    this.workScheduleService.processDetailReport(params).subscribe(res => {
      saveAs(res, 'work_schedule_report.xls');
    });
  }
  // kiem tra nut apply
  checkApply(data?: any) {
    if(data == null) {
      return false;
    }
    const dateNow = new Date().getTime();
    if (data < dateNow ) {
        if ( dateNow - data < 99999999){
          return false;
        }
        return true;
    }
    return false;
  }

  // hien thu cau hinh thu 7
  changeHasSatConf() {
    const type = this.f.type.value;
    if (type === 2) {
      this.f.saturdayType.setValue(1);
    } else {
      this.f.saturdayType.setValue(null);
    }
  }

  processApplyEmployee(data) {
    const modalRef = this.modalService.open(WorkScheduleEmployeeComponent, MEDIUM_MODAL_OPTIONS);
    if (data) {
      modalRef.componentInstance.setFormValue(data);
    }
    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
      if (this.workScheduleService.requestIsSuccess(result)) {
        this.processSearch();
      }
    });
  }

  processCheckApply(item) {
    this.workScheduleService.processCheckApply(item.workScheduleId).subscribe(
      res => {
        if (this.workScheduleService.requestIsSuccess(res)) {
          if (res.data && res.data.length > 0) {
            const modalRef = this.modalService.open(WorkScheduleApplyComponent, DEFAULT_MODAL_OPTIONS);
            modalRef.componentInstance.setFormValue(item.workScheduleId, res);
            modalRef.result.then((result) => {
              if (!result) {
                return;
              }
              if (this.workScheduleService.requestIsSuccess(result)) {
                this.processSearch();
              }
            });
          } else {
            setTimeout(() => {
              this.app.confirmMessage('workSchedule.apply.defaultMessageComfirm', () => {// on accepted
                this.workScheduleService.processApply(item.workScheduleId, {applyData: null}).subscribe(
                  resApply => {
                    if (this.workScheduleService.requestIsSuccess(resApply)) {
                      this.processSearch();
                    }
                  }
                );
              }, () => {// on rejected

              });
            }, 500);
          }
        } else if (this.workScheduleService.requestIsConfirm(res) && res.message === 'empWorkScheduleExist') {
            this.app.confirmMessage('workSchedule.apply.empWorkScheduleExist',
               () => {// on accepted
                this.workScheduleService.processApply(item.workScheduleId, {applyData: null}).subscribe(
                  resApply => {
                    if (this.workScheduleService.requestIsSuccess(resApply)) {
                      this.processSearch();
                    }
                  }
                );
              }, () => {// on rejected
            });
        }
      }
    );
  }

}
