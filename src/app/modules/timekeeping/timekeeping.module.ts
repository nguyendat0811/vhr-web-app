import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TimekeepingRoutingModule } from './timekeeping-routing.module';
import { SharedModule } from '@app/shared';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SharedModule,
    TimekeepingRoutingModule
  ],
  exports: [

  ]
})
export class TimekeepingModule { }
