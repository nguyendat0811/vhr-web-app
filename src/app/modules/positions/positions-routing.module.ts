import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'position-category',
    loadChildren: './position-category/position-category.module#PositionCategoryModule'
  },
  {
    path: 'position-career',
    loadChildren: './position-career/position-career.module#PositionCareerModule'
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PositionsRoutingModule { }
