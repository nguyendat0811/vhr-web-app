import { InteractionFormComponent } from './pages/position-des-index/position-des-form/interaction-form/interaction-form.component';
import { JobRequirementComponent } from './pages/position-des-index/position-des-form/job-requirement/job-requirement.component';
import { AuthorityandScopeComponent } from './pages/position-des-index/position-des-form/authorityand-scope/authorityand-scope.component';
import { AccountabFormComponent } from './pages/position-des-index/position-des-form/accountab-form/accountab-form.componet';
import { JobPurposeFormComponent } from './pages/position-des-index/position-des-form/job-purpose-form/job-purpose-form.component';
import { PositionCategoryFormComponent } from './pages/position-category-form/position-category-form.component';
import { PositionCategorySearchComponent } from './pages/position-category-search/position-category-search.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@app/shared';
import { AccordionModule } from 'primeng/accordion';

import { PositionCategoryRoutingModule } from './position-category-routing.module';
import { PositionCategoryPageComponent } from './pages/position-category-index/position-category-index.component';
import { PositionDesIndexComponent } from './pages/position-des-index/position-des-index.component';
import { PositionViewDetailComponent } from './pages/position-view-detail/position-view-detail.component';

@NgModule({

  declarations: [ PositionCategoryPageComponent
    , PositionCategorySearchComponent
    , PositionCategoryFormComponent
    , PositionDesIndexComponent
    , JobPurposeFormComponent
    , AccountabFormComponent
    , InteractionFormComponent
    , PositionViewDetailComponent
    , AuthorityandScopeComponent
    , JobRequirementComponent],

  imports: [
    CommonModule,
    SharedModule,
    AccordionModule,
    PositionCategoryRoutingModule,
  ],
  entryComponents: [PositionCategoryFormComponent, PositionViewDetailComponent]
})
export class PositionCategoryModule { }
