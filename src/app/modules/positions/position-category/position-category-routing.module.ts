import { PropertyResolver } from '@app/shared/services/property.resolver';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PositionCategoryPageComponent } from './pages/position-category-index/position-category-index.component';
import { PositionDesIndexComponent } from './pages/position-des-index/position-des-index.component';
import { RESOURCE } from '@app/core';
import { CommonUtils } from '@app/shared/services';

const routes: Routes = [
  {
    path: '',
    component: PositionCategoryPageComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.POSITION_CATEGORY,
      nationId: CommonUtils.getNationId()
    }
  },
  {
    path: 'description/:id',
    component: PositionDesIndexComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PositionCategoryRoutingModule { }
