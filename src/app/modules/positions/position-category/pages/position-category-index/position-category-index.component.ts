import { Component, OnInit } from '@angular/core';
import { PositionCategoryService, RESOURCE } from '@app/core';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';

@Component({
  selector: 'position-category-page',
  templateUrl: './position-category-index.component.html'
})
export class PositionCategoryPageComponent extends BaseComponent implements OnInit {
  resultList: any = {};
  constructor (
      private positionCategoryService: PositionCategoryService
      , public act: ActivatedRoute) {
      super(act, RESOURCE.POSITION_CATEGORY);
  }

  ngOnInit() {
  }
}
