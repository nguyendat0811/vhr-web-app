import { LineOrgService } from '../../../../../core/services/hr-organization/line-org.service';
import { Router, ActivatedRoute } from '@angular/router';
import { PositionViewDetailComponent } from './../position-view-detail/position-view-detail.component';
import { PositionCategoryFormComponent } from './../position-category-form/position-category-form.component';
import { PositionDesIndexComponent } from './../position-des-index/position-des-index.component';
import { PositionCategoryService, ACTION_FORM, MajorCareerService } from '@app/core';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DEFAULT_MODAL_OPTIONS, RESOURCE } from '@app/core/app-config';
import { AppComponent } from '@app/app.component';
import { ValidationService, CommonUtils } from '@app/shared/services';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';

@Component({
  selector: 'position-category-search',
  templateUrl: './position-category-search.component.html',
})
export class PositionCategorySearchComponent extends BaseComponent implements OnInit {
  credentials: any = {};
  lineOrgList: any;
  majorCareerList: any;
  lineOrgId: number;
  statusList: [];
  formConfig = {
    organizationId: [''],
    lineOrgId: [''],
    majorCareerId: [''],
    code: ['' , [ValidationService.maxLength(50)]],
    name: ['', [ValidationService.maxLength(200)]],
    status: [''],
  };
  constructor(private formBuilder: FormBuilder
        , public actr: ActivatedRoute
        , private modalService: NgbModal
        , private positionCategoryService: PositionCategoryService
        , private majorCareerService: MajorCareerService
        , private lineOrgService: LineOrgService
        , private app: AppComponent
        , private router: Router) {
          super(actr, RESOURCE.POSITION_CATEGORY, ACTION_FORM.SEARCH);
          this.setMainService(positionCategoryService);
          this.formSearch = this.buildForm({}, this.formConfig);
          this.positionCategoryService.getListStatus().subscribe(res => {
            this.statusList = res.data;
          });
          this.lineOrgService.getListLineCareer().subscribe(res => {
            this.lineOrgList = res.data;
          });
  }
  ngOnInit() {
    this.processSearch();
  }
  get f () {
    return this.formSearch.controls;
  }
   /**
   * thuc hien xem chi tiet
   */
  processViewDetail(positionBean) {
    if (positionBean && positionBean.positionId > 0) {
        this.positionCategoryService.getPositionDetail(positionBean.positionId)
        .subscribe(res => {
          this.createPopupViewDetail(res.data);
        });
    }
  }
  createPopupViewDetail(data?: any) {
    const modalRef = this.modalService.open( PositionViewDetailComponent, DEFAULT_MODAL_OPTIONS);
    if (data) {
      modalRef.componentInstance.positionBean = data;
    }
    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
    });
  }
  /**
   * prepareUpdate
   * param item
   */
  prepareSaveOrUpdate(item) {
    const modalRef = this.modalService.open(PositionCategoryFormComponent, DEFAULT_MODAL_OPTIONS);
    if (item && item.positionId > 0) {
      this.positionCategoryService.findOne(item.positionId)
        .subscribe(res => {
          modalRef.componentInstance.setFormValue(this.propertyConfigs, res.data);
        });
    } else {
      modalRef.componentInstance.setFormValue(this.propertyConfigs);
    }
    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
      if (this.positionCategoryService.requestIsSuccess(result)) {
        this.processSearch(null);
      }
    });
  }

  createPopup(data?: any) {
    const modalRef = this.modalService.open(PositionCategoryFormComponent, DEFAULT_MODAL_OPTIONS);
    if (data) {
      modalRef.componentInstance.setFormValue(this.propertyConfigs, data);
    }
    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
      if (this.positionCategoryService.requestIsSuccess(result)) {
        this.processSearch(null);
      }
    });
  }
  /**
   * prepareDelete
   * param item
   */
  processDelete(item) {
    if (item && item.positionId > 0) {
      this.app.confirmDelete(null, () => {// on accepted
        this.positionCategoryService.deleteById(item.positionId)
        .subscribe(res => {
          if (this.positionCategoryService.requestIsSuccess(res)) {
            this.processSearch(null);
          }
        });
      }, () => {// on rejected

      });
    }
  }
  /**
   * prepareDescription
   * param item
   */
  prepareDescription(item) {
    if (item && item.positionId > 0) {
      this.positionCategoryService.findOne(item.positionId)
        .subscribe(res => {
          console.log(res.data);
          if (res.data) {
            this.router.navigate(['/employees/position-category/description', res.data.positionId]);
          } else {
            this.processSearch(null);
          }
      });
    }
  }
  public onLineOrgChange(event) {
    console.log('event', event);
    this.formSearch.get('majorCareerId').setValue(null);
    if (!event) {
      this.majorCareerList = [];
      return;
    }
    this.majorCareerService.getListMajorCareer(event).subscribe(res => {
      this.majorCareerList = res.data;
    });
  }
  public onStatusChange($event) {
    console.log($event);
  }
}
