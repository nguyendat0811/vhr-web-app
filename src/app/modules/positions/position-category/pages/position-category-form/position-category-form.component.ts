import { Subject } from 'rxjs';
import { ACTION_FORM } from '@app/core/app-config';
import { LineOrgService } from '../../../../../core/services/hr-organization/line-org.service';
import { Component, OnInit, Input} from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup,  Validators, FormArray} from '@angular/forms';
import { PositionCategoryService, MajorCareerService, RESOURCE, SalaryGradeService, SalaryTableService } from '@app/core';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { AppComponent } from '@app/app.component';
import { ValidationService } from '@app/shared/services/validation.service';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { TranslationService } from 'angular-l10n';

@Component({
    selector: 'position-category-form',
    templateUrl: './position-category-form.component.html',
  })
export class PositionCategoryFormComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  formPositionSalaly: FormArray;
  formPositionSalalyDelete = [];

  listSalaryRegulation = [];
  mapListGradeLadder = [[]];
  mapListGradeRate = [[]];
//  isDisabledED = false;
  @Input() item: any;
  lineOrgList: any;
  careerLevelList: any;
  majorCareerList: any;

  // codeLineOrg: any;
  // codeMajorCareer: any;
  // codeCareerLevel: any;
  formConfig = {
    positionId: [''],
    code: [''],
    name: ['' , [ ValidationService.required, ValidationService.maxLength(200)]],
    status: [''],
    lineOrgId: ['' , ValidationService.required ],
    careerLevelId: [''],
    shortName: ['' , [  ValidationService.maxLength(200), ValidationService.required]],
    majorCareerId: [''],
    effectiveDate: ['' ,  ValidationService.required ],
    expiredDate: ['', [ ValidationService.beforeCurrentDate ]],
    positionOrder: ['' , [  ValidationService.maxLength(5), ValidationService.positiveInteger, Validators.min(1)]],
  };
  formPositionSalaryConfig = {
    positionSalaryTableId: [null],
    salaryRegulationId: [null],
    salaryGradeLadderId: [null],
    salaryGradeTypeName: [null],
    salaryGradeType: [null],
    salaryGradeRateId: [null],
  };
  constructor(public actr: ActivatedRoute
            , public activeModal: NgbActiveModal
            , private positionCategoryService: PositionCategoryService
            , private lineOrgService: LineOrgService
            , private majorCareerService: MajorCareerService
            , private app: AppComponent
            , private salaryTableService: SalaryTableService
            , private salaryGradeService: SalaryGradeService
            , public translation: TranslationService) {
    super(actr, RESOURCE.POSITION_CATEGORY, ACTION_FORM.INSERT);
    this.formSave = this.buildForm({}, this.formConfig,
      ACTION_FORM.INSERT, [ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.position.expiredDate')]);
    this.lineOrgService.getListLineCareer().subscribe(res => {
      this.lineOrgList = res.data;
    });
    this.positionCategoryService.getListLevelCareer().subscribe( res => {
      this.careerLevelList = res.data;
    });
    this.salaryTableService.findSalaryPolicy().subscribe(
      res => {
        this.listSalaryRegulation = res.data;
      }
    );
    this.buildPSForm();
  }
  /**
   * ngOnInit
   */
  ngOnInit() {
  }

  /**
   * processSaveOrUpdate
   */
  processSaveOrUpdate() {
    if (!this.validateBeforeSave()) {
      return;
    }
    const formSubmit = this.formSave.value;
    formSubmit['listPositionSalaryTable'] = this.formPositionSalaly.value;
    formSubmit['listPositionSalaryTableDelete'] = this.formPositionSalalyDelete;
    this.app.confirmMessage(null, () => {
      this.positionCategoryService.saveOrUpdate(formSubmit)
      .subscribe(res => {
        if (this.positionCategoryService.requestIsSuccess(res)) {
          this.activeModal.close(res);
        }
      });
    }, null);
  }
  /**
     * validate save
     */
  private validateBeforeSave(): boolean {
    return CommonUtils.isValidForm(this.formSave);
  }

  /****************** CAC HAM COMMON DUNG CHUNG ****/
  get f () {
    return this.formSave.controls;
  }

  /**
   * setFormValue
   * param data
   */
  public setFormValue(propertyConfigs: any, data?: any) {
    this.propertyConfigs = propertyConfigs;
    if (data && data.positionId > 0) {
      this.formSave = this.buildForm(data, this.formConfig, ACTION_FORM.UPDATE
        , [ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.position.expiredDate')]);
      // const arrCode = data.code.split('.');
      // this.codeLineOrg = arrCode[0] || null;
      // this.codeMajorCareer = arrCode[1] || null;
      // this.codeCareerLevel = arrCode[2] || null;
      // build position salary
      this.positionCategoryService.getByPositionId(data.positionId).subscribe(res => {
        if (res) {
          this.buildPSForm(res.data);
        } else {
          this.buildPSForm();
        }
      });
    } else {
      this.formSave = this.buildForm({}, this.formConfig, ACTION_FORM.INSERT
        , [ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.position.expiredDate')]);
      // build position salary
      this.buildPSForm();
    }
    if (this.formSave.get('lineOrgId').value) {
      this.majorCareerService.getListMajorCareer(this.formSave.get('lineOrgId').value).subscribe(res => {
        this.majorCareerList = res.data;
      });
    }
  }

  public onLineOrgChange(event) {
    // this.codeLineOrg = null;
    // this.codeMajorCareer = null;
    this.formSave.get('majorCareerId').setValue('');
    if (!event) {
      this.majorCareerList = [];
      this.formSave.get('code').setValue('');
      return;
    }
    this.majorCareerService.getListMajorCareer(event).subscribe(res => {
      this.majorCareerList = res.data;
    });
    this.genCode();
    // if (this.formSave.get('lineOrgId').value) {
    //   this.lineOrgService.findOne(this.formSave.get('lineOrgId').value).subscribe(res => {
    //     this.codeLineOrg = res.data.code;
    //     this.genCode();
    //   });
    // }
  }

  public onMajorCareerChange(event) {
    this.genCode();
    // if (!event) {
    //   this.codeMajorCareer = null;
    //   this.genCode();
    //   return;
    // }
    // if (this.formSave.get('lineOrgId').value && this.formSave.get('majorCareerId').value) {
    //   this.majorCareerService.findOne(this.formSave.get('majorCareerId').value).subscribe(res => {
    //     this.codeMajorCareer = res.data.code;
    //     this.genCode();
    //   });
    // }
  }

  public onCareerLevelChange(event) {
    this.genCode();
    // if (!event) {
    //   this.codeCareerLevel = null;
    //   if (this.formSave.get('lineOrgId').value) {
    //     this.genCode();
    //   }
    //   return;
    // }
    // this.positionCategoryService.findCareerLevelById(event).subscribe(res => {
    //   this.codeCareerLevel = res.data.code;
    //   if (this.formSave.get('lineOrgId').value) {
    //     this.genCode();
    //   }
    // });
  }

  private genCode() {
    this.formSave.get('code').setValue('');
    const form = {
      lineOrgId: this.f.lineOrgId.value,
      majorCareerId: this.f.majorCareerId.value,
      careerLevelId: this.f.careerLevelId.value
    }

    this.positionCategoryService.getCodeExit(form).subscribe(res => {
      this.formSave.get('code').setValue(res.data);
    });

    // if (this.codeLineOrg && this.codeMajorCareer && this.codeCareerLevel) {
    //   this.positionCategoryService.getCodeExit(this.codeLineOrg + '.' + this.codeMajorCareer + '.' + this.codeCareerLevel + '.' )
    //   .subscribe(res => {
    //     this.formSave.get('code').setValue( this.codeLineOrg + '.' + this.codeMajorCareer + '.' + this.codeCareerLevel + '.'
    //     + res.data);
    //   });
    // } else {
    //   if (!this.codeMajorCareer) {
    //     this.codeMajorCareer = '000';
    //   }
    //   if (!this.codeCareerLevel) {
    //     this.codeCareerLevel = '000';
    //   }
    //   this.positionCategoryService.getCodeExit(this.codeLineOrg + '.' + this.codeMajorCareer + '.' + this.codeCareerLevel + '.' )
    //   .subscribe(res => {
    //     this.formSave.get('code').setValue( this.codeLineOrg + '.' + this.codeMajorCareer + '.' + this.codeCareerLevel + '.'
    //     + res.data);
    //   });
    // }
  }

  private makeDefaultPSForm(data?: any): FormGroup {
    return this.buildForm(data || {}, this.formPositionSalaryConfig, this.actionForm);
  }

  private buildPSForm(lstData?: any) {
    const controls = new FormArray([]);
    // start hiepnc xu ly reload lại list onchange
    this.mapListGradeLadder = [];
    this.mapListGradeRate = [];
    if (!lstData || lstData.length === 0) {
      const group = this.makeDefaultPSForm();
      controls.push(group);
      this.mapListGradeLadder = [[]];
      this.mapListGradeRate = [[]];
    } else {
      for (const i in lstData) {
        this.mapListGradeLadder.push([]);
        this.mapListGradeRate.push([]);
        const data = lstData[i];
        const group = this.makeDefaultPSForm(data);
        const regulateId = data['salaryRegulationId'];
        if (regulateId) {
          this.salaryTableService.findByParentId(regulateId).subscribe(
            res => {
              this.mapListGradeLadder[i] = res.data;
            }
          );
        }
        const gradeLadderId = data['salaryGradeLadderId'];
        if (gradeLadderId) {
          this.salaryTableService.findByParentId(gradeLadderId).subscribe(
            res => {
              const type = lstData[i].type;
              const typeValue = this.translation.translate(`position.salary.gradeType.${type}`);
              group.get('salaryGradeTypeName').setValue(typeValue);
              this.mapListGradeRate[i] = res.data;
            }
          );
        }
        // end
        controls.push(group);
      }
    }
    this.formPositionSalaly = controls;
  }

  public addRow(index) {
    this.mapListGradeLadder[index + 1] = [];
    this.mapListGradeRate[index + 1] = [];
    this.formPositionSalaly.insert(index + 1, this.makeDefaultPSForm());
  }

  deleteRow(id, index) {
    if (id && id > 0) {
      this.positionCategoryService.confirmDelete({
        messageCode: null,
        accept: () => {
          this.positionCategoryService.deletePositionSalaryTable(id)
          .subscribe(res => {
            if (this.positionCategoryService.requestIsSuccess(res)) {
              this.positionCategoryService.getByPositionId(this.formSave.get('positionId').value)
              .subscribe(result => {
                this.buildPSForm(result.data);
              });
            }
          });
        }
      });
    } else {
      if (this.formPositionSalaly.length === 1) {
        this.buildPSForm();
        return;
      }
      this.mapListGradeLadder.splice(index, 1);
      this.mapListGradeRate.splice(index, 1);
      this.formPositionSalaly.removeAt(index);
    }
  }
  public onSalaryRegulateChange(regulateId, index) {
    const formGroup = this.formPositionSalaly.controls[index];
    formGroup.get('salaryGradeLadderId').setValue(null);
    formGroup.get('salaryGradeType').setValue(null);
    formGroup.get('salaryGradeTypeName').setValue(null);
    formGroup.get('salaryGradeRateId').setValue(null);
    if (regulateId) {
      this.salaryTableService.findByParentId(regulateId).subscribe(
        res => this.mapListGradeLadder[index] = res.data
      );
    }
    this.mapListGradeLadder[index] = [];
    this.mapListGradeRate[index] = [];
  }
  public onGradeLadderChange(gradeLadderId, index) {
    const formGroup = this.formPositionSalaly.controls[index];
    formGroup.get('salaryGradeType').setValue(null);
    formGroup.get('salaryGradeTypeName').setValue(null);
    formGroup.get('salaryGradeRateId').setValue(null);
    if (gradeLadderId) {
      const type = this.mapListGradeLadder[index].find(x => x.salaryTableId === gradeLadderId).type;
      const typeValue = this.translation.translate(`position.salary.gradeType.${type}`);
      formGroup.get('salaryGradeType').setValue(type);
      formGroup.get('salaryGradeTypeName').setValue(typeValue);
      this.salaryTableService.findByParentId(gradeLadderId).subscribe(
        res => this.mapListGradeRate[index] = res.data
      );
    }
    this.mapListGradeRate[index] = [];
  }
}
