import { CommonUtils } from '@app/shared/services';
import { FormGroup, Validators } from '@angular/forms';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { PositionDescriptionService } from '@app/core/services/hr-organization/position-description.service';

@Component({
  selector: 'position-des-index',
  templateUrl: './position-des-index.component.html'
})
export class PositionDesIndexComponent implements OnInit, OnDestroy {

  positionId: number;
  private sub: any;

  formSave: FormGroup;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private positionDescriptionService: PositionDescriptionService,
  ) {
    this.sub = this.router.events.subscribe((e: any) => {
      // If it is a NavigationEnd event re-initalise the component
      if (e instanceof NavigationEnd) {
        const params = this.activatedRoute.snapshot.params;
        if (this.positionId !== params.id) {
          this.positionId = params.id;
          this.positionDescriptionService.getPositionDescriptionDetail(this.positionId)
            .subscribe(res => {
              if (res.data) {
                this.buildForm(res.data);
              }
          });
        }
      }
    });
    this.buildForm({});
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }
  /**Khoi tao gia tri ban dau */

  private buildForm(data): void {
    this.formSave = CommonUtils.createForm(data, {
      positionId: [null],
      code: [{value: null, disabled: true}],
      name: [{value: null, disabled: true}],
    });
  }
}
