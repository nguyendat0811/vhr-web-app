import { APP_CONSTANTS } from './../../../../../../../core/app-config';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { AppComponent } from '@app/app.component';
import { PositionDescriptionService } from '@app/core/services/hr-organization/position-description.service';
import { FormGroup, FormBuilder, FormArray, AbstractControl, Validators } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';
import { SysCatTypeService, ACTION_FORM, RESOURCE } from '@app/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { RequirementDetailService } from '@app/core/services/hr-organization/requirement-detail.service';
import { ValidationService } from '@app/shared/services';

@Component({
  selector: 'job-requirement',
  templateUrl: './job-requirement.component.html'
})
export class JobRequirementComponent extends BaseComponent implements OnInit {
  @Input() positionId: number;
  formJobRequirement: FormGroup;
  // danh sach list form
  FORM_ROW_TYPE = {
    EduRequirement: 1,
    LanRequirement: 2,
    OthRequirement: 3,
    ExpRequirement: 4
  };

  // Trinh do dao tao=> Education Level
  educationLevelList = [];
  // Chuyen nghanh dao tao => Specicality
  educationMajorList = [];
  // Ngoai ngu
  languageList = [];
  // Other Requirements
  requirementsList = [];
  // Relevant Work Experience
  experienceList = [];

  constructor(private formBuilder: FormBuilder
            , private positionDescriptionService: PositionDescriptionService
            , private requirementDetailService: RequirementDetailService
            , private app: AppComponent
            , private router: Router
            , public actr: ActivatedRoute
            , private sysCatTypeService: SysCatTypeService) {
              super(actr, RESOURCE.POSITION_CATEGORY, ACTION_FORM.POS_DES_JOB_REQUIREMENT_INSERT);
              this.buildForms();

  }
  /**
   * ngOnInit
   */
  ngOnInit() {
    this.positionDescriptionService.getPositionDescriptionDetail(this.positionId)
    .subscribe(res => {
      if (res.data) {
        this.formJobRequirement.get('positionId').setValue(res.data.positionId);
        this.init(res.data);
      } else {
        this.backToList();
      }
    });
  }

  private init(data) {
   // init form

    if ((data.positionEduRequirementList || []).length > 0) {
      this.initFormArrayRequirement(data.positionEduRequirementList, 1);
    }
    if ((data.positionLanRequirementList || []).length > 0) {
      this.initFormArrayRequirement(data.positionLanRequirementList, 2);
    }
    if ((data.positionOthRequirementList || []).length > 0) {
      this.initFormArrayRequirement(data.positionOthRequirementList, 3);
    }
    if ((data.positionExpRequirementList || []).length > 0) {
      this.initFormArrayRequirement(data.positionExpRequirementList, 4);
    }
  // init dropdown
    this.sysCatTypeService.getListSysCat(APP_CONSTANTS.SYS_CAT_TYPE_COCE.EDUCATION_LEVEL)
      .subscribe(res => this.educationLevelList = res.data);
    this.sysCatTypeService.getListSysCat(APP_CONSTANTS.SYS_CAT_TYPE_COCE.EDUCATION_TYPE)
      .subscribe(res => this.educationMajorList = res.data);
    this.sysCatTypeService.getListSysCat(APP_CONSTANTS.SYS_CAT_TYPE_COCE.LANGUAGE)
      .subscribe(res => this.languageList = res.data);
    this.sysCatTypeService.getListSysCat(APP_CONSTANTS.SYS_CAT_TYPE_COCE.REQUIREMENT)
      .subscribe(res => this.requirementsList = res.data);
    this.sysCatTypeService.getListSysCat(APP_CONSTANTS.SYS_CAT_TYPE_COCE.EXPERIENCE)
      .subscribe(res => this.experienceList = res.data);

  }
  private setListRequirementValue(nameControl, nameForm, requirementId, index) {
    if (requirementId === null) {
      const formArray = this.formJobRequirement.controls[nameForm] as FormArray;
      const formGroup = formArray.controls[index] as FormGroup;
      formGroup.removeControl(nameControl);
      formGroup.addControl(nameControl,
      CommonUtils.createControl(this.actionForm , nameControl, [], null, this.propertyConfigs));
    } else {
      this.requirementDetailService.getRequirementDetail(requirementId)
    .subscribe(res => {
      const formArray = this.formJobRequirement.controls[nameForm] as FormArray;
      const formGroup = formArray.controls[index] as FormGroup;
      formGroup.removeControl(nameControl);
      formGroup.addControl(nameControl,
      CommonUtils.createControl(this.actionForm , nameControl, res.data, null, this.propertyConfigs));
    });
    }
  }
  // onChange LANGUAGE
  private afterSelectLanguage(event, index) {
    this.setListRequirementValue('languageRequirementList', 'positionLanRequirementList', event, index);
  }
  // onChange OTHER
  private afterSelectOther(event, index) {
    this.setListRequirementValue('otherRequirementList', 'positionOthRequirementList', event, index);
  }
  // onChange EXPERIENCE
  private afterSelectExperience(event, index) {
    this.setListRequirementValue('expRequirementList', 'positionExpRequirementList', event, index);
  }

  private initFormArrayRequirement(data: any, type) {
    let control;
    switch (type) {
      case 1: {
        control = this.getListFormArray(this.FORM_ROW_TYPE.EduRequirement);
        break;
      }
      case 2: {
        control = this.getListFormArray(this.FORM_ROW_TYPE.LanRequirement);
        break;
      }
      case 3: {
        control = this.getListFormArray(this.FORM_ROW_TYPE.OthRequirement);
        break;
      }
      case 4: {
        control = this.getListFormArray(this.FORM_ROW_TYPE.ExpRequirement);
        break;
      }
    }
    this.clearFormArray(control);
    data.forEach((item, index) => {
      const formG = this.addRequirementFormGroup(type, item, index) as AbstractControl;
      formG.patchValue(item);
      control.push(formG);
    });
  }

  private clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  }

  addRowRequirement(type, index) {
    const control = this.getListFormArray(type);
    switch (type) {
      case this.FORM_ROW_TYPE.EduRequirement:
        control.insert(index + 1, this.addRequirementFormGroup(1));
        break;
      case this.FORM_ROW_TYPE.LanRequirement:
        control.insert(index + 1, this.addRequirementFormGroup(2));
        break;
      case this.FORM_ROW_TYPE.OthRequirement:
        control.insert(index + 1, this.addRequirementFormGroup(3));
        break;
      case this.FORM_ROW_TYPE.ExpRequirement:
        control.insert(index + 1, this.addRequirementFormGroup(4));
        break;
    }
  }

  deleteRow(type, index) {
    const control = this.getListFormArray(type);
    if (control.length === 1) {
      control.removeAt(index);
      control.insert(index + 1, this.addRequirementFormGroup(type));
      return;
    }
    control.removeAt(index);
  }

  private buildForms(): void {
    this.formJobRequirement = this.formBuilder.group({
      positionId: [null],
      positionEduRequirementList: this.formBuilder.array([
        this.addRequirementFormGroup(1)
      ]),
      positionLanRequirementList: this.formBuilder.array([
        this.addRequirementFormGroup(2)
      ]),
      positionOthRequirementList: this.formBuilder.array([
        this.addRequirementFormGroup(3)
      ]),
      positionExpRequirementList: this.formBuilder.array([
        this.addRequirementFormGroup(4)
      ]),
    });
  }

  private addRequirementFormGroup(type: number, item?, index?) {
    const group = {
      positionRequirementId: [null],
      type: [type],
      requirementId: [null],
      requirementDetailId: [null],
      note: [null, ValidationService.maxLength(500)],
      languageRequirementList: [],
      otherRequirementList: [],
      expRequirementList: [],
    };
     const formSave = this.buildForm({}, group);
    if (item && item.requirementId) {
      if (type === 2) {
        this.setListRequirementValue('languageRequirementList', 'positionLanRequirementList', item.requirementId, index);
      }
      if (type === 3) {
        this.setListRequirementValue('otherRequirementList', 'positionOthRequirementList', item.requirementId, index);
      }
      if (type === 4) {
        this.setListRequirementValue('expRequirementList', 'positionExpRequirementList', item.requirementId, index);
      }
    }
    // else {
    //   if (type === 2) {
    //     formSave.addControl('languageRequirementList',
    //      CommonUtils.createControl(this.actionForm , 'languageRequirementList', null,
    //       null, this.propertyConfigs));
    //   }
    //   if (type === 3) {
    //     formSave.addControl('otherRequirementList',
    //      CommonUtils.createControl(this.actionForm , 'otherRequirementList', null,
    //       null, this.propertyConfigs));
    //   }
    //   if (type === 4) {
    //     formSave.addControl('expRequirementList',
    //      CommonUtils.createControl(this.actionForm , 'expRequirementList', null,
    //       null, this.propertyConfigs));
    //   }
    // }
    return formSave;
  }
  // Lay danh sach form array
  getListFormArray(type) {
    switch (type) {
      case this.FORM_ROW_TYPE.EduRequirement:
        return this.formJobRequirement.controls['positionEduRequirementList'] as FormArray || new FormArray([]);
      case this.FORM_ROW_TYPE.LanRequirement:
        return this.formJobRequirement.controls['positionLanRequirementList'] as FormArray || new FormArray([]);
      case this.FORM_ROW_TYPE.OthRequirement:
        return this.formJobRequirement.controls['positionOthRequirementList'] as FormArray || new FormArray([]);
      case this.FORM_ROW_TYPE.ExpRequirement:
        return this.formJobRequirement.controls['positionExpRequirementList'] as FormArray || new FormArray([]);

    }
  }
  /** Xu ly nghiep vu START*/
  processSaveOrUpdate(): void {
    if (!CommonUtils.isValidForm(this.formJobRequirement)) {
      return;
    }
    this.app.confirmMessage(null, () => {// on rejected
      this.positionDescriptionService.saveOrUpdate(this.formJobRequirement.value)
      .subscribe(res => {

      });
    }, null);
  }

  /** Xu ly nghiep vu END*/
  backToList() {
    this.router.navigate(['/employees/position-category']);
  }
}
