import { Router, ActivatedRoute } from '@angular/router';
import { AppComponent } from '@app/app.component';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { PositionDescriptionService } from '@app/core/services/hr-organization/position-description.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { RESOURCE, ACTION_FORM } from '@app/core';
import { ValidationService } from '@app/shared/services';



@Component({
  selector: 'authorityand-scope',
  templateUrl: './authorityand-scope.component.html'
})
export class AuthorityandScopeComponent extends BaseComponent implements OnInit {
  @Input()
  positionId: number;
  formAuthorityandScope: FormGroup;
  formConfig = {
    positionId: [this.positionId],
    positionDescriptionId: [null],
    authority: [null, [ ValidationService.maxLength(2000), ValidationService.required ]],
    scopeAuthority: [null, [ ValidationService.maxLength(2000), ValidationService.required ]]
  };
  constructor(private formBuilder: FormBuilder
            , private positionDescriptionService: PositionDescriptionService
            , private app: AppComponent
            , public actr: ActivatedRoute
            , private router: Router) {
    super(actr, RESOURCE.POSITION_CATEGORY, ACTION_FORM.POS_DES_AUTHORITYAND_SCOPE_INSERT);
    this.buildForms({}, ACTION_FORM.POS_DES_AUTHORITYAND_SCOPE_INSERT);
  }
  ngOnInit() {
    this.positionDescriptionService.getPositionDescriptionDetail(this.positionId)
    .subscribe(res => {
      if (res.data) {
        this.buildForms(res.data.positionDescription, ACTION_FORM.POS_DES_AUTHORITYAND_SCOPE_INSERT);
      } else {
        this.backToList();
      }
    });
  }
  private buildForms(data?: any, actionForm?: ACTION_FORM): void {
    this.formAuthorityandScope = this.buildForm(data, this.formConfig, actionForm);
  }

  /** Xu ly nghiep vu START*/
  processSaveOrUpdate(): void {
    if (!CommonUtils.isValidForm(this.formAuthorityandScope)) {
      return;
    } else {
      const dataSubmit = {};
      dataSubmit['positionDescription'] = this.formAuthorityandScope.value;
      dataSubmit['positionId'] = this.positionId;
      this.app.confirmMessage(null, () => {// on rejected
        this.positionDescriptionService.saveOrUpdate(dataSubmit)
        .subscribe(res => {
          if (this.positionDescriptionService.requestIsSuccess(res)) {
            this.buildForms(res.data.positionDescription, ACTION_FORM.POS_DES_AUTHORITYAND_SCOPE_UPDATE);
          }
        });
      }, null);
    }
  }
  backToList() {
    this.router.navigate(['/employees/position-category']);
  }
  /** Xu ly nghiep vu END*/
   get f () {
    return this.formAuthorityandScope.controls;
  }
}
