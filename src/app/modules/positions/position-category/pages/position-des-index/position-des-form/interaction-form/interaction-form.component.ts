import { Router, ActivatedRoute } from '@angular/router';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { AppComponent } from '@app/app.component';
import { PositionDescriptionService } from '@app/core/services/hr-organization/position-description.service';
import { FormGroup, FormBuilder, FormArray, AbstractControl, Validators } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ACTION_FORM, RESOURCE } from '@app/core';
import { ValidationService } from '@app/shared/services';

@Component({
  selector: 'interaction-form',
  templateUrl: './interaction-form.component.html'
})
export class InteractionFormComponent extends BaseComponent implements OnInit {
  @Input() positionId: number;
  formInInteraction;
  formExInteraction;
  // danh sach list form
  FORM_ROW_TYPE = {
    InInteraction : 1,
    ExInteraction : 2,
  };
  constructor(private formBuilder: FormBuilder
            , private positionDescriptionService: PositionDescriptionService
            , private app: AppComponent
            , public actr: ActivatedRoute
            , private router: Router) {
              super(actr, RESOURCE.POSITION_CATEGORY, ACTION_FORM.POS_DES_INTERACTION_INSERT);
              this.buildForms();
  }
  /**
   * ngOnInit
   */
  ngOnInit() {
    this.positionDescriptionService.getPositionDescriptionDetail(this.positionId)
    .subscribe(res => {
      if (res.data) {
        // this.init(res.data);
      } else {
        this.backToList();
      }
    });
  }
  private init(data) {
    if ((data.positionInInteractionList || []).length > 0) {
      this.initFormArrayInteraction(data.positionInInteractionList, this.FORM_ROW_TYPE.InInteraction);
    }
    if ((data.positionExInteractionList || []).length > 0) {
      this.initFormArrayInteraction(data.positionExInteractionList, this.FORM_ROW_TYPE.ExInteraction);
    }
  }

  private initFormArrayInteraction(type, data?: any) {
    const controls = new FormArray([]);
    for (const item of data) {
      const formG = this.addInteractionFormGroup(type, item);
      controls.push(formG);
    }
    if (this.FORM_ROW_TYPE.InInteraction === type) {
      this.formInInteraction = controls;
    } else {
      this.formExInteraction = controls;
    }
  }

  private addInteractionFormGroup(type: number, data?: any) {
    const group = {
      positionInteractionId: [null],
      workingRelationship: [null, ValidationService.maxLength(2000)],
      content: [null, ValidationService.maxLength(2000)],
      type: [type],
    };
    return this.buildForm(data || {}, group, this.actionForm);
  }

  private clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  }

  addRowInteraction(type, index) {
    if (this.FORM_ROW_TYPE.InInteraction === type) {
      const control = this.formInInteraction as FormArray;
      control.insert(index + 1, this.addInteractionFormGroup(1));
    } else {
      const control = this.formExInteraction as FormArray;
      control.insert(index + 1, this.addInteractionFormGroup(2));
    }
  }

  deleteRow(type, index) {
    let control;
    if (this.FORM_ROW_TYPE.InInteraction === type) {
      control = this.formInInteraction as FormArray;
    } else {
      control = this.formExInteraction as FormArray;
    }
    if (control.length === 1) {
      control.removeAt(index);
      control.insert(index + 1, this.addInteractionFormGroup(type));
      return;
    }
    control.removeAt(index);
  }

  private buildForms(): void {
    this.formInInteraction = this.formBuilder.array([this.addInteractionFormGroup(1)]);
    this.formExInteraction = this.formBuilder.array([this.addInteractionFormGroup(2)]);
  }
  /** Xu ly nghiep vu START*/
  processSaveOrUpdate(): void {
    const validate1 = CommonUtils.isValidForm(this.formInInteraction);
    const validate2 = CommonUtils.isValidForm(this.formExInteraction);
    if (!validate1 && !validate2) {
      return;
    }
    let dataForm = {};
    dataForm['positionId'] = this.positionId;
    dataForm['positionInInteractionList'] = this.formInInteraction.value;
    dataForm['positionExInteractionList'] = this.formExInteraction.value;
    this.app.confirmMessage(null, () => {// on rejected
      this.positionDescriptionService.saveOrUpdate(dataForm).subscribe(res => {});
    }, null);
  }

  /** Xu ly nghiep vu END*/
  backToList() {
    this.router.navigate(['/employees/position-category']);
  }
}
