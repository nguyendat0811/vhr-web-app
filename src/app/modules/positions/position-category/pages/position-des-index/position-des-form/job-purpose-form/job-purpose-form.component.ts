import { BaseComponent } from './../../../../../../../shared/components/base-component/base-component.component';
import { Router, ActivatedRoute } from '@angular/router';
import { AppComponent } from '@app/app.component';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { PositionDescriptionService } from '@app/core/services/hr-organization/position-description.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { RESOURCE, ACTION_FORM } from '@app/core';
import { ValidationService } from '@app/shared/services';



@Component({
  selector: 'job-purpose-form',
  templateUrl: './job-purpose-form.component.html'
})
export class JobPurposeFormComponent extends BaseComponent implements OnInit {
  @Input()
  positionId: number;
  formPositionDescription: FormGroup;
  formConfig = {
    positionId: [this.positionId],
    positionDescriptionId: [null],
    jobPurpose: [null, [ ValidationService.maxLength(2000), ValidationService.required ]]
  };
  constructor( private positionDescriptionService: PositionDescriptionService
            , private app: AppComponent
            , public actr: ActivatedRoute
            , private router: Router) {
    super(actr, RESOURCE.POSITION_CATEGORY, ACTION_FORM.POS_DES_JOB_PURPOSE_INSERT);
    this.buildForms({}, ACTION_FORM.POS_DES_JOB_PURPOSE_INSERT);
  }
  ngOnInit() {
      this.positionDescriptionService.getPositionDescriptionDetail(this.positionId)
      .subscribe(res => {
        if (res.data) {
          if (res.data.positionDescription) {
            this.buildForms(res.data.positionDescription, ACTION_FORM.POS_DES_JOB_PURPOSE_INSERT);
          }
        } else {
          this.backToList();
        }
    });
  }

  private buildForms(data?: any, actionForm?: ACTION_FORM): void {
    this.formPositionDescription = this.buildForm(data || {}, this.formConfig, actionForm);
  }
  /** Xu ly nghiep vu START*/
  processSaveOrUpdate(): void {
    if (!CommonUtils.isValidForm(this.formPositionDescription)) {
      return;
    } else {
      const dataSubmit = {};
      dataSubmit['positionDescription'] = this.formPositionDescription.value;
      dataSubmit['positionId'] = this.positionId;
      this.app.confirmMessage(null, () => {
        this.positionDescriptionService.saveOrUpdate(dataSubmit)
        .subscribe(res => {
          if (this.positionDescriptionService.requestIsSuccess(res)) {
            this.buildForms(res.data.positionDescription, ACTION_FORM.POS_DES_JOB_PURPOSE_UPDATE);
          }
        });
      }, null);
    }

  }
  backToList() {
    this.router.navigate(['/employees/position-category']);
  }
  /** Xu ly nghiep vu END*/
   get f () {
    return this.formPositionDescription.controls;
  }
}
