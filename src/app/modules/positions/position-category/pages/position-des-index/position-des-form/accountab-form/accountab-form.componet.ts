import { ValidationService } from '../../../../../../../shared/services/validation.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { AppComponent } from '@app/app.component';
import { PositionDescriptionService } from '@app/core/services/hr-organization/position-description.service';
import { FormGroup, FormBuilder, FormArray, AbstractControl, Validators } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ACTION_FORM, RESOURCE } from '@app/core';

@Component({
  selector: 'accountab-form',
  templateUrl: './accountab-form.componet.html'
})
export class AccountabFormComponent extends BaseComponent implements OnInit {
  @Input() positionId: number;
  formPrincipalAccountabilities: FormGroup;
  // danh sach list form

  constructor(private formBuilder: FormBuilder
            , private positionDescriptionService: PositionDescriptionService
            , private app: AppComponent
            , public actr: ActivatedRoute
            , private router: Router) {
              super(actr, RESOURCE.POSITION_CATEGORY, ACTION_FORM.POS_DES_ACCOUNTAB_INSERT);
              this.buildForms();
  }
  /**
   * ngOnInit
   */
  ngOnInit() {
    this.positionDescriptionService.getPositionDescriptionDetail(this.positionId)
    .subscribe(res => {
      if (res.data) {
        this.formPrincipalAccountabilities.get('positionId').setValue(res.data.positionId);
        this.init(res.data);
      } else {
        this.backToList();
      }
    });
  }
  private init(data) {
    if ((data.positionAccountabilityList || []).length > 0) {
      this.initFormArrayAccountability(data.positionAccountabilityList);
    }
  }

  private initFormArrayAccountability(data: any) {
    const control = this.getListFormArray();
    this.clearFormArray(control);
    for (const item of data) {
      const formG = this.addAccountabilityFormGroup() as AbstractControl;
      formG.patchValue(item);
      control.push(formG);
    }
  }

  private addAccountabilityFormGroup(): FormGroup {
    const group = {
      positionAccountabilityId: [null],
      principalAccountability: [null, [ValidationService.maxLength(200)]],
      percent: [null, [Validators.max(100), Validators.min(0.01), ValidationService.positiveNumber ] ],
      majorActivities: [null, ValidationService.maxLength(1000)],
      outcomes: [null, ValidationService.maxLength(1000)],
    };
    return this.buildForm({}, group);
  }

  private clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  }

  addRowAccountability(index: number, item: FormGroup) {
      const control = this.getListFormArray();
      control.insert(index + 1, this.addAccountabilityFormGroup());
  }

  deleteRow(index: number, item: FormGroup) {
    const control = this.getListFormArray();
    if (control.length === 1) {
      control.reset();
      control.removeAt(index);
      control.insert(index + 1, this.addAccountabilityFormGroup());
      return;
    }
    control.removeAt(index);
  }

  private buildForms(): void {
    this.formPrincipalAccountabilities = this.formBuilder.group({
      positionId: [null],
      positionAccountabilityList: this.formBuilder.array([
        this.addAccountabilityFormGroup()
      ])
    });
  }
  // Lay danh sach form array
  getListFormArray() {
    return this.formPrincipalAccountabilities.controls['positionAccountabilityList'] as FormArray || new FormArray([]);
  }
  /** Xu ly nghiep vu START*/
  processSaveOrUpdate(): void {
    if (!CommonUtils.isValidForm(this.formPrincipalAccountabilities)) {
      return;
    }
    this.app.confirmMessage(null, () => {// on rejected
      this.positionDescriptionService.saveOrUpdate(this.formPrincipalAccountabilities.value)
      .subscribe(res => {
        this.ngOnInit();
      });
    }, null);
  }
  /** Xu ly nghiep vu END*/
  backToList() {
    this.router.navigate(['/employees/position-category']);
  }
}
