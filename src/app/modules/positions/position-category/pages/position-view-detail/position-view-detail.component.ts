import { ActivatedRoute, Router } from '@angular/router';
import { AppComponent } from '@app/app.component';
import { FormGroup, FormBuilder, Validators, FormArray, AbstractControl } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { PositionCategoryBean } from '@app/core/models/position-category.model';
import { PositionCategoryService, SysCatTypeService } from '@app/core';
import { PositionDescriptionService } from '@app/core/services/hr-organization/position-description.service';

@Component({
    selector: 'position-view-detail',
    templateUrl: './position-view-detail.component.html'
  })
export class PositionViewDetailComponent implements OnInit {
  positionBean: PositionCategoryBean;
  formViewDetail: FormGroup;
  positionId: number;
  private sub: any;
  tabIndex = 0;

  FORM_ROW_TYPE = {
    InInteraction : 0,
    ExInteraction : 1,
    Accountabilities : 2,
    EduRequirement: 3,
    LanRequirement: 4,
    OthRequirement: 5,
    ExpRequirement: 6
  };

  constructor(public activeModal: NgbActiveModal
              , private formBuilder: FormBuilder
              , private positionDescriptionService: PositionDescriptionService) {
    this.buildForm();
    }
  /**
   * ngOnInit
   */
  ngOnInit() {
      this.positionDescriptionService.getPositionDescriptionDetail(this.positionBean.positionId)
        .subscribe(res => {
          if (res.data) {
            // this.formViewDetail.get('positionId').setValue(res.data.positionId);
            // this.formViewDetail.get('name').setValue(res.data.name);
            // this.formViewDetail.get('code').setValue(res.data.code);
            this.init(res.data);

          }
      });
  }
  /**
   * buildForm
   */
  private init(data) {
    // init form
    if (data.positionDescription) {
      this.formViewDetail.get('positionDescription').patchValue(data.positionDescription);
    }
    if ((data.positionInInteractionList || []).length > 0) {
      this.initFormArrayInteraction(data.positionInInteractionList, 1);
    }
    if ((data.positionExInteractionList || []).length > 0) {
      this.initFormArrayInteraction(data.positionExInteractionList, 2);
    }
    if ((data.positionAccountabilityList || []).length > 0) {
      this.initFormArrayAccountability(data.positionAccountabilityList);
    }
    if ((data.positionEduRequirementList || []).length > 0) {
      this.initFormArrayRequirement(data.positionEduRequirementList, 1);
    }
    if ((data.positionLanRequirementList || []).length > 0) {
      this.initFormArrayRequirement(data.positionLanRequirementList, 2);
    }
    if ((data.positionOthRequirementList || []).length > 0) {
      this.initFormArrayRequirement(data.positionOthRequirementList, 3);
    }
    if ((data.positionExpRequirementList || []).length > 0) {
      this.initFormArrayRequirement(data.positionExpRequirementList, 4);
    }
  }
  initFormArrayInteraction(data: any, type) {
    let control;
    if (type === 1) {
      control = this.getListFormArray(this.FORM_ROW_TYPE.InInteraction);
    } else {
      control = this.getListFormArray(this.FORM_ROW_TYPE.ExInteraction);
    }
    this.clearFormArray(control);
    for (const item of data) {
      const formG = this.addInteractionFormGroup(type) as AbstractControl;
      formG.patchValue(item);
      control.push(formG);
    }
  }

  initFormArrayAccountability(data: any) {
    const control = this.getListFormArray(this.FORM_ROW_TYPE.Accountabilities);
    this.clearFormArray(control);
    for (const item of data) {
      const formG = this.addAccountabilityFormGroup() as AbstractControl;
      formG.patchValue(item);
      control.push(formG);
    }
  }

  initFormArrayRequirement(data: any, type) {
    let control;
    switch (type) {
      case 1: {
        control = this.getListFormArray(this.FORM_ROW_TYPE.EduRequirement);
        break;
      }
      case 2: {
        control = this.getListFormArray(this.FORM_ROW_TYPE.LanRequirement);
        break;
      }
      case 3: {
        control = this.getListFormArray(this.FORM_ROW_TYPE.OthRequirement);
        break;
      }
      case 4: {
        control = this.getListFormArray(this.FORM_ROW_TYPE.ExpRequirement);
        break;
      }
    }
    this.clearFormArray(control);
    for (const item of data) {
      const formG = this.addRequirementFormGroup(type) as AbstractControl;
      formG.patchValue(item);
      control.push(formG);
    }
  }
  // Xoa bo list object trong form array
  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  }
  // Description
  addDescriptionFormGroup() {
    return this.formBuilder.group({
      positionDescriptionId: [null],
      jobPurpose: [{value: null, disabled: true}],
      authority: [{value: null, disabled: true}],
      scopeAuthority: [{value: null, disabled: true}],
      status: [{value: null, disabled: true}],
      rejectReason: [{value: null, disabled: true}],
    });
  }
  // Interaction
  addInteractionFormGroup(type: number) {
    return this.formBuilder.group({
      positionInteractionId: [null],
      workingRelationship: [{value: null, disabled: true}],
      content: [{value: null, disabled: true}],
      type: [type],
    });
  }

  addRowInteraction(type, index) {
    const control = this.getListFormArray(type);
    if (this.FORM_ROW_TYPE.InInteraction === type) {
      control.insert(index + 1, this.addInteractionFormGroup(1));
    } else {
      control.insert(index + 1, this.addInteractionFormGroup(2));
    }
  }

  // Accountability
  addAccountabilityFormGroup() {
    return this.formBuilder.group({
      positionAccountabilityId: [null],
      principalAccountability: [{value: null, disabled: true}],
      percent: [{value: null, disabled: true}],
      majorActivities: [{value: null, disabled: true}],
      outcomes: [{value: null, disabled: true}],
    });
  }

  addRowAccountability(type, index) {
    if (this.FORM_ROW_TYPE.Accountabilities === type) {
      const control = this.getListFormArray(type);
      control.insert(index + 1, this.addAccountabilityFormGroup());
    }
  }

  // Requirement
  addRequirementFormGroup(type: number) {
    return this.formBuilder.group({
      positionRequirementId: [null],
      type: [type],
      requirementId: [{value: null, disabled: true}],
      requirementName: [{value: null, disabled: true}],
      requirementDetailId: [{value: null, disabled: true}],
      requirementDetailName: [{value: null, disabled: true}],
      note: [{value: null, disabled: true}],
    });
  }

  addRowRequirement(type, index) {
    const control = this.getListFormArray(type);
    switch (type) {
      case this.FORM_ROW_TYPE.EduRequirement:
        control.insert(index + 1, this.addRequirementFormGroup(1));
        break;
      case this.FORM_ROW_TYPE.LanRequirement:
        control.insert(index + 1, this.addRequirementFormGroup(2));
        break;
      case this.FORM_ROW_TYPE.OthRequirement:
        control.insert(index + 1, this.addRequirementFormGroup(3));
        break;
      case this.FORM_ROW_TYPE.ExpRequirement:
        control.insert(index + 1, this.addRequirementFormGroup(4));
        break;
    }
  }
  getListFormArray(type) {
    switch (type) {
      case this.FORM_ROW_TYPE.InInteraction:
        return this.formViewDetail.controls['positionInInteractionList'] as FormArray || new FormArray([]);
      case this.FORM_ROW_TYPE.ExInteraction:
        return this.formViewDetail.controls['positionExInteractionList'] as FormArray || new FormArray([]);
      case this.FORM_ROW_TYPE.Accountabilities:
        return this.formViewDetail.controls['positionAccountabilityList'] as FormArray || new FormArray([]);
      case this.FORM_ROW_TYPE.EduRequirement:
        return this.formViewDetail.controls['positionEduRequirementList'] as FormArray || new FormArray([]);
      case this.FORM_ROW_TYPE.LanRequirement:
        return this.formViewDetail.controls['positionLanRequirementList'] as FormArray || new FormArray([]);
      case this.FORM_ROW_TYPE.OthRequirement:
        return this.formViewDetail.controls['positionOthRequirementList'] as FormArray || new FormArray([]);
      case this.FORM_ROW_TYPE.ExpRequirement:
        return this.formViewDetail.controls['positionExpRequirementList'] as FormArray || new FormArray([]);
    }
  }
  private buildForm(): void {
    this.formViewDetail = this.formBuilder.group({
      positionDescription: this.addDescriptionFormGroup(),
      positionInInteractionList: this.formBuilder.array([
        this.addInteractionFormGroup(1)
      ]),
      positionExInteractionList: this.formBuilder.array([
        this.addInteractionFormGroup(2)
      ]),
      positionAccountabilityList: this.formBuilder.array([
        this.addAccountabilityFormGroup()
      ]),
      positionEduRequirementList: this.formBuilder.array([
        this.addRequirementFormGroup(1)
      ]),
      positionLanRequirementList: this.formBuilder.array([
        this.addRequirementFormGroup(2)
      ]),
      positionOthRequirementList: this.formBuilder.array([
        this.addRequirementFormGroup(3)
      ]),
      positionExpRequirementList: this.formBuilder.array([
        this.addRequirementFormGroup(4)
      ]),
    });
  }
}
