import { CommonUtils } from '@app/shared/services/common-utils.service';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PositionCareerIndexComponent } from './pages/position-career-index/position-career-index.component';
import { PropertyResolver } from '@app/shared/services/property.resolver';
import { RESOURCE } from '@app/core';

const routes: Routes = [
  {
    path: '',
    component: PositionCareerIndexComponent,
    resolve: {
    props: PropertyResolver
    },
    data: {
      resource: RESOURCE.POSITION_CAREER,
      nationId: CommonUtils.getNationId()
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PositionCareerRoutingModule { }
