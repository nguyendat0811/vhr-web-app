import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TreeModule } from 'primeng/tree';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@app/shared';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { PositionCareerRoutingModule } from './position-career-routing';
import { PositionCareerIndexComponent } from './pages/position-career-index/position-career-index.component';
import { PositionCareerSearchComponent } from './pages/position-career-search/position-career-search.component';
import { LineOrgFormComponent } from './pages/line-org-form/line-org-form.component';
import { MajorCareerFormComponent } from './pages/major-career-form/major-career-form.component';
import { PositionCareerSelectorComponent } from './pages/position-career-selector/position-career-selector.component';
import {TreeDragDropService} from 'primeng/api';

@NgModule({
  declarations: [
     PositionCareerIndexComponent,
     PositionCareerSearchComponent,
     LineOrgFormComponent,
     MajorCareerFormComponent,
     PositionCareerSelectorComponent
     ],
  imports: [
    CommonModule,
    SharedModule,
    PositionCareerRoutingModule,
    TreeModule,
    ScrollPanelModule,
  ],
  entryComponents: [LineOrgFormComponent, MajorCareerFormComponent],
  providers: [TreeDragDropService, NgbActiveModal]
})
export class PositionCareerModule { }
