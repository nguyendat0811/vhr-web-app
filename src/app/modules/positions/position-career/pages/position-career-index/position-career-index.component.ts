import { MajorCareerFormComponent } from './../major-career-form/major-career-form.component';
import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { OrgSelectorService, RESOURCE } from '@app/core';
import { PositionCareerSelectorComponent } from '../position-career-selector/position-career-selector.component';
import { PositionCareerSearchComponent } from '../position-career-search/position-career-search.component';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-position-career-index',
  templateUrl: './position-career-index.component.html'
})

export class PositionCareerIndexComponent extends BaseComponent implements OnInit, AfterViewInit {
  @ViewChild('positionSearch') positionSearch: PositionCareerSearchComponent;
  @ViewChild('positionTree') positionTree: PositionCareerSelectorComponent;
  resultList: any = {};
  constructor(public act: ActivatedRoute) {
    super(act, RESOURCE.POSITION_CAREER);
  }

  ngOnInit() {
  }
  ngAfterViewInit(): void {
    this.positionSearch.callBackAffterSave.subscribe(
      res => {
        this.positionTree.actionInitAjax(res);
      }
    );
  }
}
