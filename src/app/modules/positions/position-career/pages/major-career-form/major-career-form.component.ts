import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MajorCareerService, RESOURCE, ACTION_FORM } from '@app/core';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { AppComponent } from '@app/app.component';
import { ValidationService } from '@app/shared/services';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'major-career-form',
  templateUrl: './major-career-form.component.html'
})
export class MajorCareerFormComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  @Input() item: any;
  formConfig = {
    lineOrgName: [''],
    lineOrgId: [''],
    majorCareerId: [''],
    code: ['', [ValidationService.required, ValidationService.maxLength(50)]],
    name: ['', [ValidationService.required, ValidationService.maxLength(500)]],
    effectiveDate: ['', ValidationService.required],
    expiredDate: ['', [ValidationService.beforeCurrentDate]]
  };
  constructor(public actr: ActivatedRoute
    , public activeModal: NgbActiveModal
    , private majorCareerService: MajorCareerService
    , private app: AppComponent) {
    super(actr, RESOURCE.POSITION_CAREER, ACTION_FORM.MAJOR_CAREER_INSERT);
      this.buildForms({}, ACTION_FORM.MAJOR_CAREER_INSERT);
    }

  ngOnInit() {
  }

  /**
  * processSaveOrUpdate
  */
  processSaveOrUpdate() {
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    } else {
      this.app.confirmMessage(null, () => {// on accepted
        this.majorCareerService.saveOrUpdate(this.formSave.value)
        .subscribe(res => {
          if (this.majorCareerService.requestIsSuccess(res)) {
            this.activeModal.close(res);
          }
        });
      }, () => {// on rejected
      });
    }
  }

  /****************** CAC HAM COMMON DUNG CHUNG ****/
  get f () {
    return this.formSave.controls;
  }

  /**
  * buildForm
  */
  private buildForms(data?: any, actionForm ?: ACTION_FORM): void {
    this.formSave = this.buildForm(data, this.formConfig
      , actionForm
      , [ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.majorCareer.expiredDate')]);
  }

  public onChangeDate() {
  }

  /**
  * setFormValue
  * param data
  */
  public setFormValue(propertyConfigs: any, data: any) {
    this.propertyConfigs = propertyConfigs;
    if (data && data.majorCareerId > 0) {
      this.buildForms(data,  ACTION_FORM.MAJOR_CAREER_UPDATE);
    } else {
      this.buildForms(data,  ACTION_FORM.MAJOR_CAREER_INSERT);
    }
  }
}
