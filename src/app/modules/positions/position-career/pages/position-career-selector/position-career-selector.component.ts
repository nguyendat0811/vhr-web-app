import { BaseComponent } from './../../../../../shared/components/base-component/base-component.component';
import { map } from 'rxjs/operators';
import { HelperService } from '@app/shared/services/helper.service';
import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { PositionCareerService, RESOURCE } from '@app/core';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { TreeNode } from 'primeng/api';
import { MenuItem } from 'primeng/api';
import { TranslationService } from 'angular-l10n';
import { Tree } from 'primeng/tree';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'position-career-selector',
  templateUrl: './position-career-selector.component.html',
})

export class PositionCareerSelectorComponent extends BaseComponent implements OnInit {
  nodes: TreeNode[];
  selectedNode: TreeNode;
  items: MenuItem[] = [];
  // check vi tri
  indexAdd: any;
  checkCondition = 0;

  @ViewChild('tree')
  tree: Tree;

  @Input()
  public onNodeSelect: Function;
  @Output()
  public treeSelectNode: EventEmitter<TreeNode> = new EventEmitter<TreeNode>();
  @Output()
  public treeNewNode: EventEmitter<TreeNode> = new EventEmitter<TreeNode>();
  @Output()
  public onRemoveNode: EventEmitter<TreeNode> = new EventEmitter<TreeNode>();
  @Output()
  public onDroppableNode: EventEmitter<TreeNode> = new EventEmitter<TreeNode>();



  constructor(private service: PositionCareerService
            , private translation: TranslationService
            , private helperService: HelperService
            , public actr: ActivatedRoute) {
    super(actr, RESOURCE.POSITION_CAREER);
  }

 /**
  * action create Context Menu
  */
  nodeContextMenuSelect(event) {
    const labelAdd = this.translation.translate('MenuContext.add');
    const labelAddLineOrg = this.translation.translate('MenuContext.addLineOrg');
    const labelAddMajor = this.translation.translate('MenuContext.addMajor');
    const labelAddBefore = this.translation.translate('MenuContext.addBefore');
    const labelAddAfter = this.translation.translate('MenuContext.addAfter');
    const labelAddLineOrgBefore = this.translation.translate('MenuContext.addLineOrgBefore');
    const labelAddLineOrgAfter = this.translation.translate('MenuContext.addLineOrgAfter');
    const labelAddMajorBefore = this.translation.translate('MenuContext.addMajorBefore');
    const labelAddMajorAfter = this.translation.translate('MenuContext.addMajorAfter');
    const labelEdit = this.translation.translate('MenuContext.edit');
    const labelDelete = this.translation.translate('MenuContext.delete');
    const menuAddChild  = { icon: 'fa info fa-plus', command: () => this.addChild(event.node), label: labelAdd };
    const menuAddChildMajor  = { icon: 'fa info fa-plus', command: () => this.addChild(event.node), label: labelAddMajor };
    const menuAddLineOrgChild  = { icon: 'fa info fa-plus', command: () => this.addChild(event.node), label: labelAddLineOrg };
    const menuAddBefore = { icon: 'fa info fa-plus', command: () => this.treeAddSibling(event.node, true), label: labelAddBefore };
    const menuAddAfter =  { icon: 'fa info fa-plus', command: () => this.treeAddSibling(event.node, false), label: labelAddAfter };
    const menuAddLineOrgBefore = {icon: 'fa info fa-plus', command: () => this.treeAddSibling( event.node, true),
                                                                                               label: labelAddLineOrgBefore };
    const menuAddLineOrgAfter =  {icon: 'fa info fa-plus', command: () => this.treeAddSibling(event.node, false),
                                                                                               label: labelAddLineOrgAfter };
    const menuAddMajorBefore = {icon: 'fa info fa-plus', command: () => this.treeAddSibling(event.node, true), label: labelAddMajorBefore };
    const menuAddMajorAfter =  {icon: 'fa info fa-plus', command: () => this.treeAddSibling(event.node, false), label: labelAddMajorAfter };
    const menuEdit =      { icon: 'fa info fa-edit', command: () => this.editNode(event.node), label: labelEdit };
    const menuDelete =    { icon: 'fa danger fa-trash-alt', command: () => this.processRemoveNode(event.node), label: labelDelete};

    // Root Menu
    if (event.node.data === '-1') {
      this.items = [];
      if (this.hasPermission('action.insert')) {
        this.items.push(menuAddLineOrgChild);
      }
      return;
    }

    // add new
    if (event.node.data === '0') {
      this.items = [];
      if (this.hasPermission('action.insert')) {
        this.items.push(menuAddBefore, menuAddAfter);
      }
      if (this.hasPermission('action.delete')) {
        this.items.push(menuDelete);
      }
      return;
    }
    // Menu lineOrgId
    if (event.node.lineOrgId) {
      this.items = [];
      if (this.hasPermission('action.insert')) {
        this.items.push(menuAddLineOrgBefore, menuAddLineOrgAfter, menuAddChildMajor);
      }
      if (this.hasPermission('action.update')) {
        this.items.push(menuEdit);
      }
      if (this.hasPermission('action.delete')) {
        this.items.push(menuDelete);
      }
      return;
    }
    // Menu majorCareer
    if (event.node.majorCareerId) {
      this.items = [];
      if (this.hasPermission('action.insert')) {
        this.items.push(menuAddMajorBefore, menuAddMajorAfter);
      }
      if (this.hasPermission('action.update')) {
        this.items.push(menuEdit);
      }
      if (this.hasPermission('action.delete')) {
        this.items.push(menuDelete);
      }
      return;
    }
  }

  ngOnInit() {
    this.actionInitAjax();
    this.checkDragDropNode();
  }

  /**
  * action init ajax
  */
  public actionInitAjax(currentValue?: any) {
    this.service.actionInitAjax().subscribe((res) => {
      this.nodes = CommonUtils.toTreeNode(res);
      if (currentValue) {
// tslint:disable-next-line: no-shadowed-variable
        // Sau khi thêm/sửa/xóa thì đều expand node bạn vừa tác động vào !
        const parentNode =  this.nodes[0].children.find( res => res['lineOrgId'] === currentValue.data.lineOrgId);
        if (parentNode) {
          parentNode.expanded = true;
        }
        // Sau khi thêm/sửa thì focus vào node bạn vừa thực hiện hành động !
        if (!currentValue.data.action) {
// tslint:disable-next-line: no-shadowed-variable
          const childNode = parentNode.children.find( res => res['majorCareerId'] === currentValue.data.majorCareerId);
          this.selectedNode = childNode;
          this.treeSelectNode.emit({node: childNode} as TreeNode);
        }
      }
    });
  }
   /**
  * onDrop
  * @ param event
  */
  public onDrop(event) {
   if (event.dropNode.lineOrgId && event.dropNode.children.length > 0 ) {
     this.onDroppableNode.emit(event);
   }
  }

  /**
  * Xử lý khi di chuyển thành công nghề sang ngành
  * nodeSelect
  * @ param event
  */
  public nodeSelect(event) {
    if (event.node.data === '0') {// hiển thị form thêm mới
      this.treeNewNode.emit(event.node);
    } else {// tìm kiếm
      this.treeSelectNode.emit(event);
    }
  }
  /**
   * Xử lý khi xóa node
   * param node
   */
  public processRemoveNode(currentNode) {
    if (currentNode.data === '0') {
      const parentNode = currentNode.parent;
      const children = parentNode.children;
      const index = children.indexOf(currentNode);
      children.splice(index, 1);
      return;
    } else {
      this.onRemoveNode.emit(currentNode);
    }
  }

  private getDefaultNode (parentNode) {
    const managementType =  parentNode.lineOrgId ? 2 : 1;
    return {
        data: '0'
      , label: 'New Node'
      , leaf: true
      , icon: 'glyphicons glyphicons-list-alt'
      , lineOrgId: parentNode.lineOrgId || ''
      , managementType: managementType
    };
  }
  /**
   * addchild
   * @ param event
   * @ param type
   * @ param isDefault
   */
  private addChild(parentNode: any) {
    const childNode = this.getDefaultNode(parentNode);
    parentNode.children = parentNode.children || [];
    parentNode.expanded = true;
    parentNode.children.unshift(childNode);
  }

  private treeAddSibling(currentNode, isBefore) {
    const parentNode = currentNode.parent;
    const newNode = this.getDefaultNode(parentNode);
    const children = parentNode.children;
    const index = children.indexOf(currentNode);
    if (isBefore) {
      children.splice(index, 0, newNode);
    } else {
      if (index < children.length - 1) {
        children.splice(index + 1, 0, newNode);
      } else {
        children.push(newNode);
      }
    }
  }

  /**
   * onEditNode
   * @ param currentNode
   */
  private editNode(currentNode) {
    this.treeNewNode.emit(currentNode);
  }

  private checkDragDropNode() {
    this.tree.allowDrop = (dragNode: any, dropNode: any, dragNodeScope: any): boolean => {
     // let check = true;
      // Không được kéo nghề bị khóa hoặc ngành bị khóa
      if ( (dropNode && dropNode.dropAble === 'false') || !dragNode || dragNode.dragAble === 'false') {
        this.checkCondition = 1;
        return false;
      // Thực hiện check ngày hiệu lực của nghề(dragNode) không được trước ngày hiệu lực của ngành(dropNode) => dragNode < dropNode
      } else if (dropNode && dropNode.effectiveDate > dragNode.effectiveDate) {
        this.checkCondition = 2;
        return false;
      //  Check trùng tên khi thực hiện kéo thả từ ngành khác sang
      } else if (dropNode && dropNode.children.filter( child => child.label === dragNode.label).length > 0) {
        this.checkCondition = 3;
        return false;
      //  Check trùng mã khi thực hiện kéo thả từ ngành khác sang
      } else if (dropNode && dropNode.children.filter( child => child.code === dragNode.code).length > 0) {
        this.checkCondition = 4;
        return false;
      }
      return true;
    };
    this.tree.dragDropService.dragStop$.subscribe((data) => {
      if (this.checkCondition === 1 && data.node['dragAble'] && data.node['dragAble'] === 'false' && data.node['dropAble'] === 'false') {
        this.helperService.APP_TOAST_MESSAGE.next({type: 'WARNING', code: 'position.majorCareerExisted'});
      } else if (this.checkCondition === 2 && data.node['effectiveDate']) {
        this.helperService.APP_TOAST_MESSAGE.next({type: 'WARNING', code: 'majorCareer.effectiveDateNotBeforeLine'});
      } else if (this.checkCondition === 3 && data.subNodes.filter( subNode => subNode.label === data.node.label).length === 1) {
        this.helperService.APP_TOAST_MESSAGE.next({type: 'WARNING', code: 'majorCareerId.duplicateName'});
      } else if (this.checkCondition === 4 && data.subNodes.filter( subNode => subNode['code'] === data.node['code']).length === 1) {
        this.helperService.APP_TOAST_MESSAGE.next({type: 'WARNING', code: 'majorCareerId.duplicateCode'});
      }
    });
  }
}
