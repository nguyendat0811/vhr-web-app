import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LineOrgService, RESOURCE, ACTION_FORM } from '@app/core';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { AppComponent } from '@app/app.component';
import { ValidationService } from '@app/shared/services';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'line-org-form',
  templateUrl: './line-org-form.component.html'
})

export class LineOrgFormComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  @Input() item: any;
  formConfig = {
    lineOrgId: [''],
    code: ['', [ValidationService.required, ValidationService.maxLength(50)]],
    name: ['', [ValidationService.required, ValidationService.maxLength(200)]],
    effectiveDate: ['', ValidationService.required],
    expiredDate: ['', [ValidationService.beforeCurrentDate]]
  };
  constructor(public actr: ActivatedRoute
    , public activeModal: NgbActiveModal
    , private lineOrgService: LineOrgService
    , private app: AppComponent) {
    super(actr, RESOURCE.POSITION_CAREER, ACTION_FORM.LINE_ORG_INSERT);
      this.buildForms({}, ACTION_FORM.LINE_ORG_INSERT);
    }

  ngOnInit() {
  }

  /**
  * processSaveOrUpdate
  */
  processSaveOrUpdate() {
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    } else {
      console.log('this.formSave.value', this.formSave.value);
      this.app.confirmMessage(null, () => {// on accepted
        this.lineOrgService.saveOrUpdate(this.formSave.value)
        .subscribe(res => {
          if (this.lineOrgService.requestIsSuccess(res)) {
            this.activeModal.close(res);
          }
        });
      }, () => {// on rejected
      });
    }
  }

  /****************** CAC HAM COMMON DUNG CHUNG ****/

  get f () {
    return this.formSave.controls;
  }

  /**
  * buildForm
  */
  private buildForms(data?: any, actionForm ?: ACTION_FORM): void {
    this.formSave = this.buildForm(data, this.formConfig
      , actionForm
      , [ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.lineOrg.expiredDate')]);
  }

  public onChangeDate() {
    console.log('onDateChanged');
  }

  /**
  * setFormValue
  * param data
  */
  public setFormValue(propertyConfigs: any, data?: any) {
    console.log(data);
    this.propertyConfigs = propertyConfigs;
    if (data && data.lineOrgId > 0) {
      console.log('data', data);
      this.buildForms(data,  ACTION_FORM.LINE_ORG_UPDATE);
    } else {
      this.buildForms(data,  ACTION_FORM.LINE_ORG_INSERT);
    }
  }
}
