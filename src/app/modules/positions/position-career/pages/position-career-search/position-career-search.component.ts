import { ValidationService } from '@app/shared/services/validation.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PositionCareerService, SysCatTypeService, APP_CONSTANTS, SysCatService } from '@app/core';
import { LineOrgService } from '@app/core';
import { MajorCareerService } from '@app/core';
import { DEFAULT_MODAL_OPTIONS, ACTION_FORM, RESOURCE } from '@app/core/app-config';
import { AppComponent } from '@app/app.component';
import { LineOrgFormComponent } from '../line-org-form/line-org-form.component';
import { MajorCareerFormComponent } from '../major-career-form/major-career-form.component';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';

@Component({
  selector: 'position-career-search',
  templateUrl: './position-career-search.component.html'
})
export class PositionCareerSearchComponent extends BaseComponent implements OnInit {
  credentials: any = {};
  managementTypeList: any;
  @Output()
  public onReloadTree: EventEmitter<any> = new EventEmitter();
  @Output() callBackAffterSave: EventEmitter<any> = new EventEmitter<any>();
  formConfig = {
    managementType: [''],
    lineOrgId: [''],
    majorCareerId: [''],
    code: ['', [ValidationService.maxLength(50)]],
    name: ['', [ValidationService.maxLength(200)]],
    nationId: [CommonUtils.getNationId()],
    effectiveDate: [''],
    expiredDate: ['']
  };
  constructor(public actr: ActivatedRoute
            , private modalService: NgbModal
            , private positionCareerService: PositionCareerService
            , private lineOrgService: LineOrgService
            , private majorCareerService: MajorCareerService
            ,private sysCatService: SysCatService
            , private app: AppComponent) {
    super(actr, RESOURCE.POSITION_CAREER, ACTION_FORM.SEARCH);
    this.setMainService(positionCareerService);
    this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.MANAGEMENT_TYPE)
    .subscribe(res => {
      this.managementTypeList = res.data; 
      console.log('this.managementTypeList:', this.managementTypeList)
    });

    // this.managementTypeList = APP_CONSTANTS.MANAGEMENT_TYPE;
    this.formSearch = this.buildForm({}, this.formConfig
      , ACTION_FORM.SEARCH, [ValidationService.notAffter('effectiveDate', 'expiredDate', 'positionCareer.expiredDate')]);
}

  ngOnInit() {
    this.processSearch();
  }

  get f () {
    return this.formSearch.controls;
  }

  searchByNode(event) {
    const nodeSelect = event.node;
    if (nodeSelect.lineOrgId) {
      this.formSearch.controls['lineOrgId'].setValue(nodeSelect.lineOrgId);
    } else {
      this.formSearch.controls['lineOrgId'].setValue('');
    }
    if (nodeSelect.majorCareerId) {
      this.formSearch.controls['majorCareerId'].setValue(nodeSelect.majorCareerId);
    } else {
      this.formSearch.controls['majorCareerId'].setValue('');
    }
    this.processSearch(null);
  }

  /**
  * prepareUpdate
  * param item
  */
  prepareSaveOrUpdate(item) {
    const majorType = 2;
    if (!item.data) {
      // Neu la sua binh thuong
      if (item.managementType !== majorType) {
        if (item && item.lineOrgId > 0) {
          this.callData(this.lineOrgService, item.lineOrgId, LineOrgFormComponent, '');
        }
      } else {
          // Neu la nghe
          if (item && item.majorCareerId > 0) {
            this.callData(this.majorCareerService, item.majorCareerId, MajorCareerFormComponent, '');
          }
      }
    } else {
      // Neu la them moi/ sua tu node
      if (item.managementType === majorType) {
        if (item && item.majorCareerId > 0) {
          this.callData(this.majorCareerService, item.majorCareerId, MajorCareerFormComponent, '');
        } else {
          this.callData(this.majorCareerService, '', MajorCareerFormComponent, item);
        }
      } else {
        if (item && item.lineOrgId > 0) {
          this.callData(this.lineOrgService, item.lineOrgId, LineOrgFormComponent, '');
        } else {
        this.callData(this.lineOrgService, '', LineOrgFormComponent, item);
        }
      }
    }

  }

  /**
   * callData
   * @ param typeService
   * @ param fieldCheck
   * @ param formComponent
   */
  callData(typeService, fieldCheck, formComponent, item) {
    if (fieldCheck) {
      typeService.findOne(fieldCheck)
        .subscribe(res => {
        // Neu la nghe
        if (this.majorCareerService === typeService) {
          this.lineOrgService.findByLineOrgId(res.data.lineOrgId)
            .subscribe(resp => {
              res.data['lineOrgName'] = resp.data.name;
              this.showPopup(res.data, formComponent);
            });
        // Neu la nganh
        } else {
          this.showPopup(res.data, formComponent);
        }
      });
    } else {
      if (item.lineOrgId) {
        // Hien thi ten nganh khi them moi
        this.lineOrgService.findByLineOrgId(item.lineOrgId)
          .subscribe(resp => {
            item['lineOrgName'] = resp.data.name;
            this.showPopup(item, formComponent);
          });
      } else {
        // Hien thi khi them moi nghe
        this.showPopup('', formComponent);
      }
    }
  }

/**
   * DroppableNode
   * @ param data
   */
  droppableNode(data) {
   const majorCareerId = data.dragNode.majorCareerId;
   const newLineOrgId = data.dropNode.lineOrgId;
   this.majorCareerService.updateLineOrgId(newLineOrgId, majorCareerId)
   .subscribe(res => {
     if (res.code === 'success' && res.type === 'SUCCESS') {
      this.formSearch.controls['majorCareerId'].setValue(majorCareerId);
     } else {
      this.formSearch.controls['majorCareerId'].setValue('');
     }
   });
   this.processSearch(null);
  }
  /**
   * showPopup
   * @ param data
   * @ param formComponent
   */
  showPopup(data?: any, formComponent?: any) {
    const typeModal = this.modalService.open(formComponent, DEFAULT_MODAL_OPTIONS);
    if (data) {
      typeModal.componentInstance.setFormValue(this.propertyConfigs, data);
    }
    typeModal.result.then((result) => {
      if (!result) {
        return;
      }
      if (this.positionCareerService.requestIsSuccess(result)) {
        this.processSearch(null);
        this.onReloadTree.emit();
        this.callBackAffterSave.emit(result);
      }
    });
  }

  public onChangeDate() {
  }

  /**
  * prepareDelete
  * param item
  */
  processDelete(item) {
    if (item) {
      // Neu la nghe
      if (item.managementType === 2  && item.majorCareerId > 0) {
        this.app.confirmDelete(null, () => {// on accepted
          this.majorCareerService.deleteById(item.majorCareerId)
          .subscribe(res => {
            this.processSearch(null);
            this.onReloadTree.emit();
            this.callBackAffterSave.emit({data: {lineOrgId: item.parent.lineOrgId, action: ACTION_FORM.DELETE}});
          });
        }, () => {// on rejected
        });
      }
      // Neu la nganh
      if (item.managementType === 1  && item.lineOrgId > 0) {
        this.app.confirmDelete(null, () => {// on accepted
          this.lineOrgService.deleteById(item.lineOrgId)
          .subscribe(res => {
            this.processSearch(null);
            this.onReloadTree.emit();
          });
        }, () => {// on rejected
        });
      }
    }
  }
}
