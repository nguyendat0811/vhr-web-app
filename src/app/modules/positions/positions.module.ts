import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PositionsRoutingModule } from './positions-routing.module';
import { SharedModule } from '@app/shared';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
  ]
})
export class PositionsModule { }
