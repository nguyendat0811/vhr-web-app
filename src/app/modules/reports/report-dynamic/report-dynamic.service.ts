import { HelperService } from '@app/shared/services/helper.service';
import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from '@env/environment';
import { CONFIG } from '@app/core/app-config';
import { BasicService } from '@app/core/services/basic.service';

@Injectable({
  providedIn: 'root'
})
export class ReportDynamicService extends BasicService {
  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('report', 'reportDynamic', httpClient, helperService);
  }
  public export(data): Observable<any> {
    const url = `${this.serviceUrl}/export`;
    return this.postRequestFile(url, data);
  }
  public exportTables(data): Observable<any> {
    const url = `${this.serviceUrl}/export-tables`;
    return this.postRequest(url, data);
  }
  public getSelectData(reportParameterId): Observable<any> {
    const url = `${this.serviceUrl}/select-query/${reportParameterId}`;
    return this.httpClient.get(url).pipe();
  }

}
