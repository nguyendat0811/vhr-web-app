// tslint:disable:max-line-length
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportDynamicRoutingModule } from './report-dynamic-routing.module';
import { ReportDynamicIndexComponent } from './report-dynamic-index/report-dynamic-index.component';
import { ReportDynamicFormComponent } from './report-dynamic-form/report-dynamic-form.component';
import { SharedModule } from '@app/shared';
import { ReportDynamicParameterComponent } from './report-dynamic-form/form-childs/report-dynamic-parameter/report-dynamic-parameter.component';
import { ReportDynamicSqlComponent } from './report-dynamic-form/form-childs/report-dynamic-sql/report-dynamic-sql.component';
import { ReportDynamicColumnComponent } from './report-dynamic-form/form-childs/report-dynamic-column/report-dynamic-column.component';
import { ReportDynamicExportComponent } from './report-dynamic-export/report-dynamic-export.component';
import { ReportInputGenerateDirective } from './report-dynamic-export/report-input-generate.directive';
import { InputTextComponent } from './report-dynamic-export/entry-components/input-text.component';
import { InputDatePickerComponent } from './report-dynamic-export/entry-components/input-date-picker.component';
import { InputOrgSelectorComponent } from './report-dynamic-export/entry-components/input-org-selector.component';
import { InputGenderComponent } from './report-dynamic-export/entry-components/input-gender.component';
import { InputComboboxComponent } from './report-dynamic-export/entry-components/input-combobox.component';
import { InputOrgFullSelectorComponent } from './report-dynamic-export/entry-components/input-org-full-selector.component';
import { InputOrgMultiSelectorComponent } from './report-dynamic-export/entry-components/input-org-multi-selector.component';

@NgModule({
  declarations: [ ReportDynamicIndexComponent,
                  ReportDynamicFormComponent,
                  ReportDynamicParameterComponent,
                  ReportDynamicSqlComponent,
                  ReportDynamicColumnComponent,
                  ReportDynamicExportComponent,
                  ReportInputGenerateDirective,
                  InputTextComponent,
                  InputDatePickerComponent,
                  InputOrgSelectorComponent,
                  InputOrgFullSelectorComponent,
                  InputGenderComponent,
                  InputComboboxComponent,
                  InputOrgMultiSelectorComponent
                ],
  imports: [
    CommonModule,
    SharedModule,
    ReportDynamicRoutingModule
  ],
  entryComponents: [InputTextComponent,
                    InputDatePickerComponent,
                    InputOrgSelectorComponent,
                    InputGenderComponent,
                    InputComboboxComponent,
                    InputOrgFullSelectorComponent,
                    InputOrgMultiSelectorComponent]
})
export class ReportDynamicModule { }
