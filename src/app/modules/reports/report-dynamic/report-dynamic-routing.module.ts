import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportDynamicIndexComponent } from './report-dynamic-index/report-dynamic-index.component';
import { PropertyResolver } from '@app/shared/services/property.resolver';
import { RESOURCE } from '@app/core';
import { ReportDynamicFormComponent } from './report-dynamic-form/report-dynamic-form.component';
import { ReportDynamicExportComponent } from './report-dynamic-export/report-dynamic-export.component';

const routes: Routes = [
  {
    path: '',
    component: ReportDynamicIndexComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.REPORT_DYNAMIC
    }
  },
  {
    path: 'add',
    component: ReportDynamicFormComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.REPORT_DYNAMIC
    }
  },
  {
    path: 'edit/:id',
    component: ReportDynamicFormComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.REPORT_DYNAMIC
    }
  }
  ,
  {
    path: 'export',
    component: ReportDynamicExportComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.REPORT_DYNAMIC
    }
  },
  {
    path: 'export/:id',
    component: ReportDynamicExportComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.REPORT_DYNAMIC
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportDynamicRoutingModule { }
