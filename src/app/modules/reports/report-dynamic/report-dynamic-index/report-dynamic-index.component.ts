import { Component, OnInit, Input } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { AppComponent } from '@app/app.component';
import { RESOURCE, ACTION_FORM, LabourContractTypeService, SystemParameterService, SYSTEM_PARAMETER_CODE } from '@app/core';
import { ReportDynamicService } from '../report-dynamic.service';

@Component({
  selector: 'report-dynamic-index',
  templateUrl: './report-dynamic-index.component.html',
  styles: []
})
export class ReportDynamicIndexComponent extends BaseComponent implements OnInit {
  /* Variables */
  formConfig: any = {
    code: [''],
    name: [''],
    formatReport: [''],
    startDate: [''],
    endDate: ['']
  };
  listFormatReport = [];
  /* Constructor */
  constructor(public actr: ActivatedRoute
    , public reportDynamicService: ReportDynamicService
    , public labourContractTypeService: LabourContractTypeService
    , private systemParameterService: SystemParameterService
    , private app: AppComponent) {
    super(actr, RESOURCE.REPORT_DYNAMIC, ACTION_FORM.SEARCH);
    this.setMainService(this.reportDynamicService);
    this.formSearch = this.buildForm({}, this.formConfig);
    this.loadReference();
  }
  ngOnInit() {
    this.processSearch();
  }
  get f() {
    return this.formSearch.controls;
  }

  /**
   * Load cac list du lieu lien quan
   */
  private loadReference(): void {
    this.systemParameterService.findByName(SYSTEM_PARAMETER_CODE.REPORT_DYNAMIC_FORMAT_REPORT)
    .subscribe(res => this.listFormatReport = res.data);
  }
  /**
  * prepareDelete
  * param item
  */
  processDelete(item) {
    if (item && item.reportDynamicId > 0) {
      this.app.confirmDelete(null, () => {// on accepted
        this.reportDynamicService.deleteById(item.reportDynamicId)
          .subscribe(res => {
            if (this.reportDynamicService.requestIsSuccess(res)) {
              this.processSearch(null);
            }
          });
      }, () => {// on rejected
      });
    }
  }
}

