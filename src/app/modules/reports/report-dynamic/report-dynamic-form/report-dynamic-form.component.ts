import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ACTION_FORM, RESOURCE, LabourContractTypeService, SYSTEM_PARAMETER_CODE, SystemParameterService } from '@app/core';
import { ValidationService, CommonUtils } from '@app/shared/services';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { ReportDynamicService } from '../report-dynamic.service';
import { AppComponent } from '@app/app.component';
import { FileControl } from '@app/core/models/file.control';
import { ReportDynamicParameterComponent } from './form-childs/report-dynamic-parameter/report-dynamic-parameter.component';
import { ReportDynamicSqlComponent } from './form-childs/report-dynamic-sql/report-dynamic-sql.component';
import { HelperService } from '@app/shared/services/helper.service';
import { ReportDynamicColumnComponent } from './form-childs/report-dynamic-column/report-dynamic-column.component';

@Component({
  selector: 'report-dynamic-form',
  templateUrl: './report-dynamic-form.component.html',
  styles: []
})
export class ReportDynamicFormComponent extends BaseComponent implements OnInit {
  @ViewChild('parameter')
  public parameterForm: ReportDynamicParameterComponent;
  @ViewChild('column')
  public columnForm: ReportDynamicColumnComponent;
  @ViewChild('sql')
  public sqlForm: ReportDynamicSqlComponent;

  navigationSubscription;
  formSave: FormGroup;
  listFormatReport = [];
  reportDynamicId: number;
  formConfig: any = {
    reportDynamicId: [''],
    code: ['', [ValidationService.required]],
    name: ['', [ValidationService.required]],
    formatReport: [''],
    startDate: ['', [ValidationService.required]],
    endDate: [''],
    startRow: ['', [ValidationService.positiveInteger]],
    startRowSign: ['', [ValidationService.positiveInteger]],
    endRowSign: ['', [ValidationService.positiveInteger]]
  };

  constructor(public actr: ActivatedRoute
            , public reportDynamicService: ReportDynamicService
            , public labourContractTypeService: LabourContractTypeService
            , private systemParameterService: SystemParameterService
            , private app: AppComponent
            , private router: Router
            , private helperService: HelperService) {
    super(actr, RESOURCE.REPORT_DYNAMIC);
    if (!this.hasPermission('action.insert')) {
      return;
    }
    this.loadReference();
    this.formSave = this.buildForm({}, this.formConfig, ACTION_FORM.INSERT,
      [ValidationService.notAffter('startDate', 'endDate', 'app.reportDynamic.endDate')]);
    this.formSave.addControl('file', new FileControl(null, ValidationService.required));

    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      // If it is a NavigationEnd event re-initalise the component
      if (e instanceof NavigationEnd && this.router.url.indexOf('edit') >= 0) {
        this.reportDynamicId = this.actr.snapshot.params.id;
        // if (this.reportDynamicId) {
          this.reportDynamicService.findOne(this.reportDynamicId).subscribe(res => {
            this.helperService.isProcessing(true);
            this.formSave = this.buildForm(res.data, this.formConfig, ACTION_FORM.UPDATE,
              [ValidationService.notAffter('startDate', 'endDate', 'app.reportDynamic.endDate')]);
            this.formSave.addControl('file', new FileControl(null, ValidationService.required));
            if (res.data && res.data.fileAttachment) {
              (this.formSave.controls['file'] as FileControl).setFileAttachment(res.data.fileAttachment.reportDynamicFile);
            }
            this.parameterForm.initParameterForm(ACTION_FORM.UPDATE, this.propertyConfigs, res.data.lstReportParameter);
            this.sqlForm.initSqlForm(ACTION_FORM.UPDATE, this.propertyConfigs, res.data.lstReportSql);
            this.columnForm.initColumnForm(ACTION_FORM.UPDATE, this.propertyConfigs, res.data.lstReportColumn);
            this.helperService.isProcessing(false);
          });
        // }
      }
    });
  }
  ngOnInit() { }
  get f() {
    return this.formSave.controls;
  }
  private loadReference(): void {
    this.systemParameterService.findByName(SYSTEM_PARAMETER_CODE.REPORT_DYNAMIC_FORMAT_REPORT)
    .subscribe(res => this.listFormatReport = res.data);
  }
  /**
   * processSaveOrUpdate
   */
  processSaveOrUpdate() {
    const formSave = CommonUtils.isValidForm(this.formSave);
    const formParameter = CommonUtils.isValidForm(this.parameterForm.formParameter);
    const formColumn = CommonUtils.isValidForm(this.columnForm.formColumn);
    const formSql = CommonUtils.isValidForm(this.sqlForm.formSql);
    if (formSave && formParameter && formColumn && formSql) {
      const reqData = this.formSave.value;
      reqData.lstReportParameter = this.parameterForm.formParameter.value;
      reqData.lstReportColumn = this.columnForm.formColumn.value;
      reqData.lstReportSql = this.sqlForm.formSql.value;
      reqData.fileSave = this.getFormFile();
      this.app.confirmMessage(null, () => {// on accepted
        this.reportDynamicService.saveOrUpdateFormFile(reqData)
          .subscribe(res => {
            if (this.reportDynamicService.requestIsSuccess(res)) {
              this.router.navigate(['/reports/report-dynamic']);
            }
          });
      }, () => {// on rejected

      });
    }
  }
  private getFormFile() {
    const _formValue = this.formSave.value;
    const fileSave = [];
    if (_formValue.file && _formValue.file.length > 0) {
      const fileInfos = (this.formSave.controls['file'] as FileControl).getFileAttachment();
      for (let index = 0; index < _formValue.file.length; index++) {
        if (fileInfos[index].sysLanguageCode) {
          const f = {
            file: _formValue.file[index],
            sysLanguageCode: fileInfos[index].sysLanguageCode,
            id: fileInfos[index].id ? fileInfos[index].id : null
          };
          fileSave.push(f);
        }
      }
    }
    return fileSave;
  }
  /****************** CAC HAM COMMON DUNG CHUNG ****/

  /**
   * validate save
   */
  private validateBeforeSave(): boolean {
    return true;
  }

}
