import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { FormArray, FormGroup, Validators } from '@angular/forms';
import { AppComponent } from '@app/app.component';
import { ActivatedRoute } from '@angular/router';
import { RESOURCE, ACTION_FORM } from '@app/core';
import { ValidationService, CommonUtils } from '@app/shared/services';

@Component({
  selector: 'report-dynamic-sql',
  templateUrl: './report-dynamic-sql.component.html',
  styles: []
})
export class ReportDynamicSqlComponent  extends BaseComponent implements OnInit {

  formSql: FormArray;
  constructor(private app: AppComponent
    , public actr: ActivatedRoute) {
    super(actr, RESOURCE.REPORT_DYNAMIC, ACTION_FORM.INSERT);
    this.buildFormSql();
  }

  ngOnInit() {
  }
  /**
   * initSqlForm: form Parent call formChild
   */
  public initSqlForm(actionForm: any, propertyConfigs: any, listTemplateSql?: any) {
    this.actionForm = actionForm;
    this.propertyConfigs = propertyConfigs;
    this.buildFormSql(listTemplateSql);
  }
  /**
   * makeDefaultForm
   */
  private makeDefaultForm(): FormGroup {
    const group = {
      reportDynamicId: [null],
      templateSqlId: [null],
      sql: [null, [ValidationService.maxLength(5000)]],
    };
    return this.buildForm({}, group);
  }

  public buildFormSql(listTemplateSql?: any) {
    const controls = new FormArray([]);
    if (!listTemplateSql || listTemplateSql.length === 0) {
      const group = this.makeDefaultForm();
      controls.push(group);
    } else {
      for (const i in listTemplateSql) {
        const param = listTemplateSql[i];
        const group = this.makeDefaultForm();
        group.patchValue(param);
        controls.push(group);
      }
    }
    this.formSql = controls;
  }

  private validateBeforeSave(): boolean {
    return CommonUtils.isValidForm(this.formSql);
  }

  /**
   * addGroup
   * param index
   * param item
   */
  public addGroup(index: number, item: FormGroup) {
    const controls = this.formSql as FormArray;
    controls.insert(index + 1, this.makeDefaultForm());
  }
  /**
   * removeGroup
   * param index
   * param item
   */
  public removeGroup(index: number, item: FormGroup) {
    const controls = this.formSql as FormArray;
    if (controls.length === 1) {
      this.formSql.reset();
      const group = this.makeDefaultForm();
      const control = new FormArray([]);
      control.push(group);
      this.formSql = control;
      return;
    }
    controls.removeAt(index);
  }

  /**
   * Xu ly check box ton tai duy nhat
   */
  checkOnly(e, index, formControlName) {
    if (e.target.checked) {
      const controls = this.formSql as FormArray;
      for (let i = 0; i < controls.length; i++) {
        if (index !== i) {
          controls.controls[i].get(formControlName).setValue(false);
        }
      }
    }
  }
}

