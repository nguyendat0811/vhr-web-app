import { Component, OnInit, ViewChild, ComponentFactoryResolver } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { FormGroup, Validators, ValidatorFn } from '@angular/forms';
import { HelperService } from '@app/shared/services/helper.service';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { ReportDynamicService } from '../report-dynamic.service';
import { AppComponent } from '@app/app.component';
import { RESOURCE, ACTION_FORM, REPORT_DYNAMIC_CONDITION_TYPE } from '@app/core';
import { ReportInputGenerateDirective } from './report-input-generate.directive';
import { ReportInputGenerateComponent } from './report-input-generate.component';
import { CommonUtils, ValidationService } from '@app/shared/services';
import { InputTextComponent } from './entry-components/input-text.component';
import { InputOrgSelectorComponent } from './entry-components/input-org-selector.component';
import { InputDatePickerComponent } from './entry-components/input-date-picker.component';
import { InputGenderComponent } from './entry-components/input-gender.component';
import { InputComboboxComponent } from './entry-components/input-combobox.component';
import { InputOrgFullSelectorComponent } from './entry-components/input-org-full-selector.component';
import { InputOrgMultiSelectorComponent } from './entry-components/input-org-multi-selector.component';
import { saveAs } from 'file-saver';

@Component({
  selector: 'report-dynamic-export',
  templateUrl: './report-dynamic-export.component.html',
  styles: []
})
export class ReportDynamicExportComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  formConfig: any = {
    reportDynamicId: [0]
  };
  navigationSubscription;
  reportDynamicId: number;
  reportDynamicName: String;
  selectedReport: any;
  isHideSelectReport:Boolean;
  listReportDynamic = [];
  @ViewChild(ReportInputGenerateDirective) inputGenerate: ReportInputGenerateDirective;

  constructor(public actr: ActivatedRoute
            , public reportDynamicService: ReportDynamicService
            , private componentFactoryResolver: ComponentFactoryResolver
            , private app: AppComponent
            , private helperService: HelperService
            , private router: Router) {
    
    super(actr, RESOURCE.REPORT_DYNAMIC);
    this.isHideSelectReport = false;
   
    this.formSave = this.buildForm({}, this.formConfig, ACTION_FORM.VIEW);
    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      // If it is a NavigationEnd event re-initalise the component
      if (e instanceof NavigationEnd) {
        this.reportDynamicId = this.actr.snapshot.params.id;
        if (this.reportDynamicId) {
          var that = this;
          this.isHideSelectReport = true;
          this.loadReferenceById(this.reportDynamicId);
          setTimeout(function(){
            that.loadReportDynamicById(that.reportDynamicId);
          }, 1000);
        }
        else{
          this.isHideSelectReport = false;
          this.loadReference();
        }
      }
    });
  }
  public loadReportDynamicById(_reportDynamicId): void {
    // delete all controls
    if (_reportDynamicId) {
      // load controls for current report
      this.reportDynamicService.findOne(_reportDynamicId).subscribe(res => {
        this.selectedReport = res.data;
        this.rebuildForm();
      });
    }
  }

  private loadReferenceById(reportDynamicId): void {
    this.reportDynamicService.findOne(reportDynamicId).subscribe(res => {
      if (res && res.data) {
        this.listReportDynamic = [res.data];
        if(this.listReportDynamic.length>0){
          this.reportDynamicName = this.listReportDynamic[0].name;
        }
      }
    });
  }

  private loadReference(): void {
    this.reportDynamicService.findAll().subscribe(res => {
      if (res && res.data) {
        this.listReportDynamic = res.data;
      }
    });
  }
  ngOnInit() { }
  get f() {
    return this.formSave.controls;
  }

  public changeReportDynamic(reportDynamicId): void {
    // Clear old controls
    this.inputGenerate.viewContainerRef.clear();
    // delete all controls
    if(reportDynamicId){
      this.formSave = this.buildForm({reportDynamicId: reportDynamicId}, this.formConfig, ACTION_FORM.INSERT);
    }
    if (reportDynamicId) {
      // load controls for current report
      this.reportDynamicService.findOne(reportDynamicId).subscribe(res => {
        this.selectedReport = res.data;
        this.rebuildForm();
      });
    }
  }
  public rebuildForm(): void {
    for (const param of this.selectedReport.lstReportParameter) {
      // set Validate for new control
      const validateFn = [];
      let selectData = [];
      if (param.isRequire) {
        validateFn.push(ValidationService.required);
      }
      if (param.dataType === REPORT_DYNAMIC_CONDITION_TYPE.DOUBLE) {
        validateFn.push(ValidationService.number);
      }
      if (param.dataType === REPORT_DYNAMIC_CONDITION_TYPE.LONG) {
        validateFn.push(ValidationService.integer);
      }
      // add controls
      this.formSave.addControl(param.parameterCode,
        CommonUtils.createControl(ACTION_FORM.VIEW, param.parameterCode, null, validateFn, this.propertyConfigs));
      // load component
      if (param.dataType === REPORT_DYNAMIC_CONDITION_TYPE.COMBOBOX_CONDITION && param.description) {
        this.reportDynamicService.getSelectData(param.reportParameterId).subscribe(resp => {
          selectData = resp.data;
          this.loadComponent(this.formSave.controls[param.parameterCode], param, selectData);
        });
        continue;
      }
      if (param.dataType === REPORT_DYNAMIC_CONDITION_TYPE.COMBOBOX) {
        selectData = JSON.parse(param.description);
        this.loadComponent(this.formSave.controls[param.parameterCode], param, selectData);
        continue;
      }
      this.loadComponent(this.formSave.controls[param.parameterCode], param);
    }
  }
  public loadComponent(control: any, param: any, selectData?: any): void {
    let componentFactory: any;
    switch (param.dataType) {
      case REPORT_DYNAMIC_CONDITION_TYPE.ORGANIZATION_FULL:
          componentFactory = this.componentFactoryResolver.resolveComponentFactory(InputOrgFullSelectorComponent);
          break;
      case REPORT_DYNAMIC_CONDITION_TYPE.ORGANIZATION_TREE:
          componentFactory = this.componentFactoryResolver.resolveComponentFactory(InputOrgMultiSelectorComponent);
          break;
      case REPORT_DYNAMIC_CONDITION_TYPE.ORGANIZATION_REPORT:
      case REPORT_DYNAMIC_CONDITION_TYPE.ORGANIZATION_PERMISSION:
      case REPORT_DYNAMIC_CONDITION_TYPE.ORGANIZATION_SALARY:
        componentFactory = this.componentFactoryResolver.resolveComponentFactory(InputOrgSelectorComponent);
        break;
      case REPORT_DYNAMIC_CONDITION_TYPE.COMBOBOX:
      case REPORT_DYNAMIC_CONDITION_TYPE.COMBOBOX_CONDITION:
        componentFactory = this.componentFactoryResolver.resolveComponentFactory(InputComboboxComponent);
        break;
      case REPORT_DYNAMIC_CONDITION_TYPE.DATE:
        componentFactory = this.componentFactoryResolver.resolveComponentFactory(InputDatePickerComponent);
        break;
      case REPORT_DYNAMIC_CONDITION_TYPE.GENDER:
        componentFactory = this.componentFactoryResolver.resolveComponentFactory(InputGenderComponent);
        break;
      default:
        componentFactory = this.componentFactoryResolver.resolveComponentFactory(InputTextComponent);
        break;
    }
    const viewContainerRef = this.inputGenerate.viewContainerRef;
    const componentRef = viewContainerRef.createComponent(componentFactory);
    (<ReportInputGenerateComponent>componentRef.instance).control = control;
    (<ReportInputGenerateComponent>componentRef.instance).label = param.parameterName;
    if (selectData) {
      (<ReportInputGenerateComponent>componentRef.instance).selectData = selectData;
    }
  }
  /****************** CAC HAM COMMON DUNG CHUNG ****/

  /**
   * validate save
   */
  private validateBeforeSave(): boolean {
    return true;
  }
  /**
   * export
   */
  export() {
    if (CommonUtils.isValidForm(this.formSave)) {
      if(this.isHideSelectReport){
        this.formSave.value.reportDynamicId = this.reportDynamicId;
      }
      const reqData = this.formSave.value;
      console.log('reqData', reqData);
      this.app.isProcessing(true);
      this.reportDynamicService.export(reqData)
      .subscribe((res) => {
        this.app.isProcessing(false);
        if (res.type === 'application/json') {
          const reader = new FileReader();
          reader.addEventListener('loadend', (e) => {
            const text = e.srcElement['result'];
            const json = JSON.parse(text);
            this.reportDynamicService.processReturnMessage(json);
          });
          reader.readAsText(res);
        } else {
          saveAs(res, 'report_dynamic');
        }
      });
    }
  }
  exportTables() {
    if (CommonUtils.isValidForm(this.formSave)) {
      const reqData = this.formSave.value;
      console.log('reqData', reqData);
      this.app.isProcessing(true);
      this.reportDynamicService.exportTables(reqData)
      .subscribe((res) => {
        console.log(res);
      });
    }
  }
}
