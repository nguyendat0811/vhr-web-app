import { Component, Input } from '@angular/core';
import { ReportInputGenerateComponent } from '../report-input-generate.component';
import { FormControl } from '@angular/forms';
import { BaseControl } from '@app/core/models/base.control';

@Component({
  template: `
    <dynamic-input  [property]="control"
                    [labelValue]="label"
                    [labelClass]="'ui-g-12 ui-md-6 ui-lg-3 ui-lg-offset-2 control-label vt-align-right'"
                    [inputClass]="'ui-g-12 ui-md-6 ui-lg-4'"
                    [usedLocaleLabel]="'false'">
    </dynamic-input>
  `
})
export class InputTextComponent implements ReportInputGenerateComponent {
  @Input() control: BaseControl;
  @Input() label: string;
}
