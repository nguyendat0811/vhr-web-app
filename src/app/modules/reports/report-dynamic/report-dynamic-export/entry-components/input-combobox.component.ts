import { Component, Input } from '@angular/core';
import { ReportInputGenerateComponent } from '../report-input-generate.component';
import { FormControl } from '@angular/forms';
import { BaseControl } from '@app/core/models/base.control';

@Component({
  template: `
    <label [ngClass]="{'required': control.isRequire}" class="ui-g-12 ui-md-6 ui-lg-3 ui-lg-offset-2 control-label vt-align-right">
      {{label}}
    </label>
    <div class="ui-g-12 ui-md-6 ui-lg-4">
      <select-filter [property]="control"  [options]="selectData" optionLabel="name" optionValue="code"
        [placeHolder]="control.isRequire ? 'common.label.cboMustSelect' : 'common.label.cboSelect'" >
      </select-filter>
      <app-control-messages [control]="control"></app-control-messages>
    </div>
  `
})
export class InputComboboxComponent implements ReportInputGenerateComponent {
  @Input() control: BaseControl;
  @Input() label: string;
  @Input() selectData: [];

}
