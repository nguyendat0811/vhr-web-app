import { Component, Input } from '@angular/core';
import { ReportInputGenerateComponent } from '../report-input-generate.component';
import { FormControl } from '@angular/forms';
import { BaseControl } from '@app/core/models/base.control';

@Component({
  template: `
  <label [ngClass]="{'required': control.isRequire}" class="ui-g-12 ui-md-6 ui-lg-3 ui-lg-offset-2 control-label vt-align-right">
    {{label}}
  </label>
  <div class="ui-g-12 ui-md-6 ui-lg-4">
    <org-multi-selector
      [property]="control"
      operationKey="action.view"
      adResourceKey="resource.organization"
      [isRequiredField] = "control.isRequire">
    </org-multi-selector>
    <app-control-messages [control]="control"></app-control-messages>
  </div>
  `
})
export class InputOrgMultiSelectorComponent implements ReportInputGenerateComponent {
  @Input() control: BaseControl;
  @Input() label: string;
}
