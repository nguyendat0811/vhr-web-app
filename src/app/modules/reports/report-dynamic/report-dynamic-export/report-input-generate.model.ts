import { Type } from '@angular/core';

export class ReportInputGenerateModel {
  constructor(public component: Type<any>, public data: any) {}
}

