
import { Component, OnInit, ViewContainerRef, ViewChild, ComponentFactoryResolver, QueryList, ViewChildren } from '@angular/core';
import { FormGroup, FormArray } from '@angular/forms';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute, Router } from '@angular/router';
import { HelperService } from '@app/shared/services/helper.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from '@app/app.component';
import { RESOURCE, ACTION_FORM , APP_CONSTANTS, SysCatService, EmpTypeService, PositionCategoryService} from '@app/core';
import { EmployeeListService } from '../employee-list.service';
import { ValidationService, CommonUtils } from '@app/shared/services';
import { AnyTxtRecord } from 'dns';
import { saveAs } from 'file-saver';
import { AccordionTab } from 'primeng/accordion';
import { ReportInputGenerateComponent } from '../../report-dynamic/report-dynamic-export/report-input-generate.component';
import { InputTextComponent } from '../entry-components/input-text.component';
import { InputDateRangeComponent } from '../entry-components/input-date-range.component';
import { InputMultiComboboxComponent } from '../entry-components/input-multi-selectbox.component';
import { InputComboboxComponent } from '../entry-components/input-combobox.component';
import { InputNumberRangeComponent } from '../entry-components/input-number-range.component';
@Component({
  selector: 'employee-list-index',
  templateUrl: './employee-list-index.component.html',
  styleUrls: ['./employee-list-index.component.css']
})
export class EmployeeListIndexComponent  extends BaseComponent implements OnInit {
  formSearch: FormGroup;
  formGroupAdvance: FormGroup;
  formArrayAdvance: FormArray;
  formConfig = {
    reportDate : [new Date().getTime(), [ValidationService.required]]
    ,organizationId : ['', [ValidationService.required]]
    ,isPartyMember : ['']
    ,genderFilter : ['']
    ,empTypeId : ['']
    ,positionGroupIds : ['']
    ,selectOrgLevel :['']
    ,educationLevelIds : ['']
    ,lstCommonInfo :['']
    ,lstWorkProcessInfo :['']
    ,lstEmpTypeProcessInfo :['']
    ,lstEducationProcessInfo :['']
    ,lstMileStone :['']
    ,lstPartyInfo :['']
    ,lstFinancialInfo :['']
    ,lstSalaryFactorInfo :['']
  }

  formAdvanceConfig = {
    type: [null],
    properties: [null],
    fromDate: [null],
    toDate: [null],
    stringValue: [null],
    selectedValue: [null],
    multiSelectedValue: [null],
    fromNumber: [null, [ValidationService.number]],
    toNumber: [null, [ValidationService.number]]
  }

  listIsPartyMember = APP_CONSTANTS.PARTY_MEMBER;
  listGender = APP_CONSTANTS.GENDERS;
  listCommonInfo = APP_CONSTANTS.COMMON_INFO;
  listWorkProcessInfo= APP_CONSTANTS.WORK_PROCESS_INFO;
  listEducationProcessInfo= APP_CONSTANTS.EDUCATION_PROCESS_INFO;
  listPartyInfo = APP_CONSTANTS.PARTY_INFO;
  listFinancialInfo = APP_CONSTANTS.FINANCIAL_INFO;
  listSalaryFactorInfo = APP_CONSTANTS.SALARY_FACTOR;
  listManagementType: Array<any>;
  listEmpType : Array<any>;
  listWorkPlace : Array<any>;
  listPositionGroup : Array<any>;
  listEducationLevel : Array<any>;
  lstOrgIds : [];
  lstOrdIdsProcessed: [];
  data : any;
  progressStatus: any = 0;
  organizationName : any;
  numOfOrg: any;
  currentIdx : any;
  isGetData = false;
  @ViewChild('searchInfo') searchInfo: AccordionTab;
  @ViewChildren('viewContainerRef', { read: ViewContainerRef }) viewContainerRef: QueryList<ViewContainerRef>;
  rowCondition = [];
  listCondition = [];
  mapSelectData = {};
  constructor(public act: ActivatedRoute
    , private helperService: HelperService
    , private modalService: NgbModal
    , private router: Router
    , private sysCatService: SysCatService
    , public employeeListService: EmployeeListService
    , public empTypeService : EmpTypeService
    , private positionCategoryService: PositionCategoryService
    , private app: AppComponent
    , private CFR: ComponentFactoryResolver) {
    super(act, RESOURCE.REPORT_EMPLOYEE_LIST, ACTION_FORM.SEARCH);   //change resource
    this.setMainService(employeeListService);

    this.formSearch = this.buildForm({}, this.formConfig, ACTION_FORM.SEARCH);
    // this.positionCategoryService.getListPosition().subscribe(res => {
    //   this.listPositionGroup = res.data;
    // })
    // this.empTypeService.getAllByEmpTypeByIsUsed().subscribe(res => {
    //   this.listEmpType = res.data;
    // });
    // this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_CODE.EDUCATION_LEVEL).subscribe(res => {
    //   this.listEducationLevel = res.data;
    // });

    this.employeeListService.getDataFilter().subscribe(res => {
      this.listCondition = res.data.lstFieldCondition;
      this.mapSelectData = res.data.mapSelectData;
    });
  
    this.buildFormArrayAdvance();
  }

  ngOnInit() {
  }

  get f () {
    return this.formSearch.controls;
  }
  get fAdvance () {
    return this.formArrayAdvance.controls;
  }

  public processExport(){
    const formMaster = CommonUtils.isValidForm(this.formSearch);
    const formAdvance = CommonUtils.isValidForm(this.formArrayAdvance);
    if (!formMaster || !formAdvance) {
      return;
    }
    let data = this.formSearch.value;
    data.lstFilterCondition = this.formArrayAdvance.value;
    this.employeeListService.prepareExport(data).subscribe(res => {
      this.numOfOrg = res.data.lstOrgIds.length;
      this.isGetData = true;
      this.currentIdx = 0;
      this.addData(res.data);
    });
  }

  public addData(data){
    this.employeeListService.addData(data).subscribe(res => {
      if(res.data.lstOrgIds.length <= res.data.currentIndex){
        this.exportFile(res.data);
      } else {
        this.organizationName = data.organizationName;
        this.progressStatus = data.currentIndex/data.lstOrgIds.length*100 ;
        this.progressStatus = this.progressStatus.toFixed(2);
        this.currentIdx = data.currentIndex;
        this.addData(res.data);
      }
    })
  }

  public exportFile(data){
    this.organizationName = data.organizationName;
    this.progressStatus = (data.currentIndex)/data.lstOrgIds.length*100 ;
    this.progressStatus = this.progressStatus.toFixed(2);
    this.currentIdx = data.currentIndex;
    this.employeeListService.export(data).subscribe(res => {
      saveAs(res, data.exportFileName);
      this.isGetData = false;
    })
  }

  private buildFormArrayAdvance() {
    this.formArrayAdvance = new FormArray([this.buildForm({}, this.formAdvanceConfig, ACTION_FORM.SEARCH,
      [ValidationService.notAffter('fromDate', 'toDate', 'app.salaryProcess.effectiveDateToDate'),
        ValidationService.notAffterNumber('fromNumber', 'toNumber', 'app.validateMessage.valueTo')]
      )]);
  }
  /**
   * Xử lý thêm dòng điều kiện tìm kiếm
   * @param index
   */
  addRowCondition(index) {
    const controls = this.formArrayAdvance as FormArray;
    controls.insert(index + 1, this.buildForm({}, this.formAdvanceConfig, ACTION_FORM.SEARCH,
      [ValidationService.notAffter('fromDate', 'toDate', 'app.salaryProcess.effectiveDateToDate'),
        ValidationService.notAffterNumber('fromNumber', 'toNumber', 'app.validateMessage.valueTo')]
      ));
  }

  /**
   * Xử lý xóa dòng điều kiện tìm kiếm
   * @param index
   */
  removeRowCondition(index) {
    const controls = this.formArrayAdvance as FormArray;
    if (controls.length === 1) {
      this.buildFormArrayAdvance();
    }
    controls.removeAt(index);
  }

  /**
   * Xử lý tạo form điều kiện tìm kiếm
   * @param event 
   * @param index 
   */
  onSelectCondition(event, index) {
    const objectCondition = this.findObjectCondition(event);
    if (!objectCondition) {
      return;
    }
    let componentFactory: any;
    switch (objectCondition.type) {
      case 'text':
        componentFactory = this.CFR.resolveComponentFactory(InputTextComponent);
        break;
      case 'date':
        componentFactory = this.CFR.resolveComponentFactory(InputDateRangeComponent);
        break;
      case 'selectbox':
        componentFactory = this.CFR.resolveComponentFactory(InputComboboxComponent);
        break;
      case 'multiSelect':
        componentFactory = this.CFR.resolveComponentFactory(InputMultiComboboxComponent);
        break;
      case 'number':
        componentFactory = this.CFR.resolveComponentFactory(InputNumberRangeComponent);
        break;
    }

    const viewContainerRef = this.viewContainerRef.toArray().find(x=> x.element.nativeElement.dataset.target == index+'');
    viewContainerRef.clear();
    const componentRef = viewContainerRef.createComponent(componentFactory);

    const formArray = this.formArrayAdvance as FormArray;
    let formGroup = formArray.controls[index] as FormGroup;
    formGroup.controls['type'].setValue(objectCondition.type);
    formGroup.controls['properties'].setValue(objectCondition.properties);
    formGroup.controls['fromDate'].setValue(null);
    formGroup.controls['toDate'].setValue(null);
    formGroup.controls['stringValue'].setValue(null);
    formGroup.controls['selectedValue'].setValue(null);
    formGroup.controls['multiSelectedValue'].setValue(null);
    formGroup.controls['fromNumber'].setValue(null);
    formGroup.controls['toNumber'].setValue(null);
    (<ReportInputGenerateComponent>componentRef.instance).formGroup = formGroup;
    (<ReportInputGenerateComponent>componentRef.instance).label = objectCondition.label;
    if (objectCondition.type == 'selectbox' || objectCondition.type == 'multiSelect') {
      (<ReportInputGenerateComponent>componentRef.instance).selectData = this.mapSelectData[objectCondition.properties];
    }
  }

  private findObjectCondition(properties) {
    return this.listCondition.find(x=>x.properties == properties);
  }
}
