import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeeListIndexComponent } from './employee-list-index/employee-list-index.component';
import { EmployeeListRoutingModule } from './employee-list-routing.module';
import { SharedModule } from '@app/shared';
import { AccordionModule } from 'primeng/accordion';

@NgModule({
  declarations: [EmployeeListIndexComponent],
  imports: [
    CommonModule,
    SharedModule,
    EmployeeListRoutingModule,
    AccordionModule  
  ]

})
export class EmployeeListModule { }
