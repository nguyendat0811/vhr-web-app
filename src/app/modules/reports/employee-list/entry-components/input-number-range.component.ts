import { Component, Input } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { BaseControl } from '@app/core/models/base.control';
import { ReportInputGenerateComponent } from '../../report-dynamic/report-dynamic-export/report-input-generate.component';
import { TranslationService } from 'angular-l10n';

@Component({
  template: `
  <dynamic-input  [property]="formGroup.controls['fromNumber']"
                    [labelValue]="labelFrom"
                    [labelClass]="'ui-g-12 ui-md-6 ui-lg-1 control-label vt-align-right'"
                    [inputClass]="'ui-g-12 ui-md-6 ui-lg-4'"
                    [usedLocaleLabel]="false">
  </dynamic-input>
  <dynamic-input  [property]="formGroup.controls['toNumber']"
                    [labelValue]="labelTo"
                    [labelClass]="'ui-g-12 ui-md-6 ui-lg-2 control-label vt-align-right'"
                    [inputClass]="'ui-g-12 ui-md-6 ui-lg-4'"
                    [usedLocaleLabel]="false">
  </dynamic-input>
  `
})
export class InputNumberRangeComponent implements ReportInputGenerateComponent {
  @Input() control: BaseControl;
  @Input() formGroup: FormGroup;
  @Input() label: string;
  labelFrom: string = '';
  labelTo: string = '';
  constructor(private translationService: TranslationService){

  }
  ngAfterViewInit() {
    setTimeout(() => {
      this.labelFrom = this.translationService.translate('app.label.from');
      this.labelTo = this.translationService.translate('app.label.to');
    });

  }
}
