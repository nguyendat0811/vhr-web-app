import { Component, Input } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { BaseControl } from '@app/core/models/base.control';
import { ReportInputGenerateComponent } from '../../report-dynamic/report-dynamic-export/report-input-generate.component';

@Component({
  template: `
    <label class="ui-g-12 ui-md-6 ui-lg-1 control-label vt-align-right">

    </label>
    <div class="ui-g-12 ui-md-6 ui-lg-4">
      <select-filter appendTo="body" [property]="formGroup.controls['selectedValue']" [options]="selectData" optionLabel="value" optionValue="keyId"
        [placeHolder]="'common.label.cboSelectAll'" >
      </select-filter>
    </div>
  `
})
export class InputComboboxComponent implements ReportInputGenerateComponent {
  @Input() control: BaseControl;
  @Input() formGroup: FormGroup;
  @Input() label: string;
  @Input() selectData: [];
}
