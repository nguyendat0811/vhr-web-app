import { Component, Input } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { BaseControl } from '@app/core/models/base.control';
import { ReportInputGenerateComponent } from '../../report-dynamic/report-dynamic-export/report-input-generate.component';

@Component({
  template: `
    <label class="ui-g-12 ui-md-6 ui-lg-1 control-label vt-align-right">

    </label>
    <dynamic-input  [property]="formGroup.controls['stringValue']"
                    labelValue=""
                    [labelClass]="'ui-g-12 ui-md-6 ui-lg-2 control-label vt-align-right'"
                    [inputClass]="'ui-g-12 ui-md-6 ui-lg-4'"
                    [usedLocaleLabel]="true">
    </dynamic-input>
  `
})
export class InputTextComponent implements ReportInputGenerateComponent {
  @Input() control: BaseControl;
  @Input() formGroup: FormGroup;
  @Input() label: string;
}
