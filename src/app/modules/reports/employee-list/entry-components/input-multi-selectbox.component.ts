import { Component, Input } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { BaseControl } from '@app/core/models/base.control';
import { ReportInputGenerateComponent } from '../../report-dynamic/report-dynamic-export/report-input-generate.component';

@Component({
  template: `
    <label class="ui-g-12 ui-md-6 ui-lg-1 control-label vt-align-right">
    </label>
    <div class="ui-g-12 ui-md-6 ui-lg-4">
      <multi-select-filter
      [property]="formGroup.controls['multiSelectedValue']"
        [options]="selectData"
        optionLabel="value"
        optionValue="keyId"
        [filterPlaceHolder]="'common.button.table.hSearch'"
        [placeHolder]="'common.label.cboSelectAll'"
        appendTo="body"
        >
      </multi-select-filter>
    </div>
  `
})
export class InputMultiComboboxComponent implements ReportInputGenerateComponent {
  @Input() control: BaseControl;
  @Input() label: string;
  @Input() formGroup: FormGroup;
  @Input() selectData: [];

}
