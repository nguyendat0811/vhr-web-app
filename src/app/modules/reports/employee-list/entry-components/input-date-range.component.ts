import { Component, Input } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { BaseControl } from '@app/core/models/base.control';
import { ReportInputGenerateComponent } from '../../report-dynamic/report-dynamic-export/report-input-generate.component';

@Component({
  template: `
  <label class="ui-g-12 ui-md-6 ui-lg-1 control-label vt-align-right">
    {{'app.label.from' | translate}}
  </label>
  <div class="ui-g-12 ui-md-6 ui-lg-4">
    <date-picker appendTo="body" [property]="formGroup.controls['fromDate']"></date-picker>
    <app-control-messages [control]="formGroup.controls['fromDate']"></app-control-messages>
  </div>
  <label class="ui-g-12 ui-md-6 ui-lg-2 control-label vt-align-right">
    {{'app.label.to' | translate}}
  </label>
  <div class="ui-g-12 ui-md-6 ui-lg-4">
    <date-picker appendTo="body" [property]="formGroup.controls['toDate']"></date-picker>
    <app-control-messages [control]="formGroup.controls['toDate']"></app-control-messages>
  </div>
  `
})
export class InputDateRangeComponent implements ReportInputGenerateComponent {
  @Input() control: BaseControl;
  @Input() formGroup: FormGroup;
  @Input() label: string;
}
