import { Injectable } from '@angular/core';
import { BasicService } from '@app/core/services/basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';
import { Observable } from 'rxjs';
import { CommonUtils } from '@app/shared/services';

@Injectable({
  providedIn: 'root'
})
export class EmployeeListService extends BasicService  {

  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('report', 'employeeList', httpClient, helperService);
  }

  public export(data): Observable<any> {
    const url = `${this.serviceUrl}/export`;
    return this.postRequestFile(url,  CommonUtils.convertData(data));
  }

  public prepareExport(data): Observable<any> {
    const url = `${this.serviceUrl}/prepare-export`;
    return this.postRequest(url,  CommonUtils.convertFormFile(data));
  }

  public addData(data): Observable<any> {
    const url = `${this.serviceUrl}/add-data`;    
    return this.postRequest(url,  CommonUtils.convertData(data));
  }

  public getDataFilter(): Observable<any> {
    const url = `${this.serviceUrl}/get-data-filter`;
    return this.getRequest(url);
  }
}
