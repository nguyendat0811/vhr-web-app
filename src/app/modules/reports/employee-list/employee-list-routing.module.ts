import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeeListIndexComponent } from './employee-list-index/employee-list-index.component';
const routes: Routes = [
  {
    path: '',
    component: EmployeeListIndexComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeeListRoutingModule { }
