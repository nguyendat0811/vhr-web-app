import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// tslint:disable:max-line-length
import { TemplateDynamicRoutingModule } from './template-dynamic-routing.module';
import { TemplateDynamicIndexComponent } from './template-dynamic-index/template-dynamic-index.component';
import { TemplateDynamicFormComponent } from './template-dynamic-form/template-dynamic-form.component';
import { SharedModule } from '@app/shared';
import { TemplateDynamicParameterComponent } from './template-dynamic-form/form-childs/template-dynamic-parameter/template-dynamic-parameter.component';
import { TemplateDynamicSqlComponent } from './template-dynamic-form/form-childs/template-dynamic-sql/template-dynamic-sql.component';
import { TemplateDynamicSignatureComponent } from './template-dynamic-form/form-childs/template-dynamic-signature/template-dynamic-signature.component';

@NgModule({
  declarations: [TemplateDynamicIndexComponent, TemplateDynamicFormComponent, TemplateDynamicParameterComponent, TemplateDynamicSqlComponent, TemplateDynamicSignatureComponent],
  imports: [
    CommonModule,
    SharedModule,
    TemplateDynamicRoutingModule
  ],
  entryComponents: []
})
export class TemplateDynamicModule { }
