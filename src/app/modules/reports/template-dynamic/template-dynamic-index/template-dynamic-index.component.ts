import { Component, OnInit, Input } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { AppComponent } from '@app/app.component';
import { RESOURCE, ACTION_FORM, LabourContractTypeService, SystemParameterService, SYSTEM_PARAMETER_CODE } from '@app/core';
import { TemplateDynamicService } from '../template-dynamic.service';
import { HrStorage } from '@app/core/services/HrStorage';

@Component({
  selector: 'template-dynamic-index',
  templateUrl: './template-dynamic-index.component.html',
  styles: []
})
export class TemplateDynamicIndexComponent extends BaseComponent implements OnInit {
  /* Variables */
  formConfig: any = {
    organizationId: [''],
    positionId: [''],
    formTypeId: [''],
    labourContractTypeId: [''],
    startDate: [''],
    endDate: ['']
  };
  laboutContractTypeList = [];
  templateTypeList = [];
  /* Constructor */
  constructor(public actr: ActivatedRoute
    , public templateDynamicService: TemplateDynamicService
    , public labourContractTypeService: LabourContractTypeService
    , private systemParameterService: SystemParameterService
    , private app: AppComponent) {
    super(actr, RESOURCE.TEMPLATE_DYNAMIC, ACTION_FORM.SEARCH);
    this.setMainService(this.templateDynamicService);
    this.formSearch = this.buildForm({}, this.formConfig);
    this.loadReference();
  }
  ngOnInit() {
    this.processSearch();
  }
  get f() {
    return this.formSearch.controls;
  }
  /**
   * Load cac list du lieu lien quan
   */
  private loadReference(): void {
    this.labourContractTypeService.getAllLabourContractTypeByStatus().subscribe(res => {
      this.laboutContractTypeList = res.data;
    });
    this.systemParameterService.findByName(SYSTEM_PARAMETER_CODE.TEMPLATE_DYNAMIC_FORM_TYPE)
    .subscribe(res => this.templateTypeList = res.data);
  }
  /**
  * prepareDelete
  * param item
  */
  processDelete(item) {
    if (item && item.templateDynamicId > 0) {
      this.app.confirmDelete(null, () => {// on accepted
        this.templateDynamicService.deleteById(item.templateDynamicId)
          .subscribe(res => {
            if (this.templateDynamicService.requestIsSuccess(res)) {
              this.processSearch(null);
            }
          });
      }, () => {// on rejected
      });
    }
  }
}

