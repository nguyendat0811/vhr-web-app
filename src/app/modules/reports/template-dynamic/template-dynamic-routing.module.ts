import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TemplateDynamicIndexComponent } from './template-dynamic-index/template-dynamic-index.component';
import { PropertyResolver } from '@app/shared/services/property.resolver';
import { RESOURCE } from '@app/core';
import { TemplateDynamicFormComponent } from './template-dynamic-form/template-dynamic-form.component';

const routes: Routes = [
  {
    path: '',
    component: TemplateDynamicIndexComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.TEMPLATE_DYNAMIC
    }
  },
  {
    path: 'add',
    component: TemplateDynamicFormComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.TEMPLATE_DYNAMIC
    }
  },
  {
    path: 'edit/:id',
    component: TemplateDynamicFormComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.TEMPLATE_DYNAMIC
    }
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TemplateDynamicRoutingModule { }
