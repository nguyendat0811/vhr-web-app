import { HelperService } from '@app/shared/services/helper.service';
import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from '@env/environment';
import { CONFIG } from '@app/core/app-config';
import { BasicService } from '@app/core/services/basic.service';

@Injectable({
  providedIn: 'root'
})
export class TemplateDynamicService extends BasicService {
  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('report', 'templateDynamic', httpClient, helperService);
  }

  public export(data): Observable<any> {
    const url = `${this.serviceUrl}/download`;
    return this.getRequest(url, {params: data, responseType: 'blob'});
  }
}
