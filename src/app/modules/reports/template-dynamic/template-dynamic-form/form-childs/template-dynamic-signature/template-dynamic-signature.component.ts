import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { FormArray, FormGroup, Validators } from '@angular/forms';
import { AppComponent } from '@app/app.component';
import { ActivatedRoute } from '@angular/router';
import { RESOURCE, ACTION_FORM } from '@app/core';
import { CommonUtils, ValidationService } from '@app/shared/services';

@Component({
  selector: 'template-dynamic-signature',
  templateUrl: './template-dynamic-signature.component.html',
  styles: []
})
export class TemplateDynamicSignatureComponent extends BaseComponent implements OnInit {

  formSignature: FormArray;
  constructor(private app: AppComponent
    , public actr: ActivatedRoute) {
    super(actr, RESOURCE.TEMPLATE_DYNAMIC, ACTION_FORM.INSERT);
    this.buildFormSignature();
  }

  ngOnInit() {
  }
  /**
   * initSignatureForm: form Parent call formChild
   */
  public initSignatureForm(actionForm: any, propertyConfigs: any, listTemplateSignature?: any) {
    this.actionForm = actionForm;
    this.propertyConfigs = propertyConfigs;
    this.buildFormSignature(listTemplateSignature);
  }
  /**
   * makeDefaultForm
   */
  private makeDefaultForm(): FormGroup {
    const group = {
      templateDynamicId: [null],
      templateSignatureId: [null],
      title: [null, [ValidationService.maxLength(250)]],
      employeeId: [null],
      isViewImage: [null],
      numberOrder: [null, [ValidationService.positiveNumber]],
    };
    return this.buildForm({}, group);
  }

  public buildFormSignature(listTemplateSignature?: any) {
    const controls = new FormArray([]);
    if (!listTemplateSignature  || listTemplateSignature.length <= 0) {
      const group = this.makeDefaultForm();
      controls.push(group);
    } else {
      for (const i in listTemplateSignature) {
        const param = listTemplateSignature[i];
        const group = this.makeDefaultForm();
        group.patchValue(param);
        controls.push(group);
      }
    }
    this.formSignature = controls;
  }

  private validateBeforeSave(): boolean {
    return CommonUtils.isValidForm(this.formSignature);
  }

  /**
   * addGroup
   * param index
   * param item
   */
  public addGroup(index: number, item: FormGroup) {
    const controls = this.formSignature as FormArray;
    controls.insert(index + 1, this.makeDefaultForm());
  }
  /**
   * removeGroup
   * param index
   * param item
   */
  public removeGroup(index: number, item: FormGroup) {
    const controls = this.formSignature as FormArray;
    if (controls.length === 1) {
      this.formSignature.reset();
      const group = this.makeDefaultForm();
      const control = new FormArray([]);
      control.push(group);
      this.formSignature = control;
      return;
    }
    controls.removeAt(index);
  }

  /**
   * Xu ly check box ton tai duy nhat
   */
  checkOnly(e, index, formControlName) {
    if (e.target.checked) {
      const controls = this.formSignature as FormArray;
      for (let i = 0; i < controls.length; i++) {
        if (index !== i) {
          controls.controls[i].get(formControlName).setValue(false);
        }
      }
    }
  }
}

