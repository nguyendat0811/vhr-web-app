import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { FormArray, FormGroup, Validators } from '@angular/forms';
import { AppComponent } from '@app/app.component';
import { ActivatedRoute } from '@angular/router';
import { RESOURCE, ACTION_FORM } from '@app/core';
import { CommonUtils, ValidationService } from '@app/shared/services';

@Component({
  selector: 'template-dynamic-parameter',
  templateUrl: './template-dynamic-parameter.component.html',
  styles: []
})
export class TemplateDynamicParameterComponent extends BaseComponent implements OnInit {

  formParameter: FormArray;
  constructor(private app: AppComponent
    , public actr: ActivatedRoute) {
    super(actr, RESOURCE.TEMPLATE_DYNAMIC, ACTION_FORM.INSERT);
    this.buildFormParameter();
  }

  ngOnInit() {
  }
  /**
   * initParameterForm: form Parent call formChild
   */
  public initParameterForm(actionForm: any, propertyConfigs: any, listTemplateParameter?: any) {
    this.actionForm = actionForm;
    this.propertyConfigs = propertyConfigs;
    this.buildFormParameter(listTemplateParameter);
  }
  /**
   * makeDefaultForm
   */
  private makeDefaultForm(): FormGroup {
    const group = {
      templateDynamicId: [null],
      templateParameterId: [null],
      parameterCode: [null, [ValidationService.maxLength(50)]],
      parameterName: [null, [ValidationService.maxLength(250)]],
      parameterValue: [null, [ValidationService.maxLength(250)]],
      description: [null , [ValidationService.maxLength(250)]],
    };
    return this.buildForm({}, group);
  }

  public buildFormParameter(listTemplateParameter?: any) {
    const controls = new FormArray([]);
    if (!listTemplateParameter || listTemplateParameter.length <= 0) {
      const group = this.makeDefaultForm();
      controls.push(group);
    } else {
      for (const i in listTemplateParameter) {
        const param = listTemplateParameter[i];
        const group = this.makeDefaultForm();
        group.patchValue(param);
        // group.patchValue(this.emp);
        controls.push(group);
      }
    }
    this.formParameter = controls;
  }

  private validateBeforeSave(): boolean {
    return CommonUtils.isValidForm(this.formParameter);
  }

  /**
   * addGroup
   * param index
   * param item
   */
  public addGroup(index: number, item: FormGroup) {
    const controls = this.formParameter as FormArray;
    controls.insert(index + 1, this.makeDefaultForm());
  }
  /**
   * removeGroup
   * param index
   * param item
   */
  public removeGroup(index: number, item: FormGroup) {
    const controls = this.formParameter as FormArray;
    if (controls.length === 1) {
      this.formParameter.reset();
      const group = this.makeDefaultForm();
      const control = new FormArray([]);
      control.push(group);
      this.formParameter = control;
      return;
    }
    controls.removeAt(index);
  }

}

