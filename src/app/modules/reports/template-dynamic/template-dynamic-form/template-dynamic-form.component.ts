import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ACTION_FORM,  RESOURCE, LabourContractTypeService, SYSTEM_PARAMETER_CODE, SystemParameterService } from '@app/core';
import { ValidationService, CommonUtils } from '@app/shared/services';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { TemplateDynamicService } from '../template-dynamic.service';
import { AppComponent } from '@app/app.component';
import { FileControl } from '@app/core/models/file.control';
import { TemplateDynamicParameterComponent } from './form-childs/template-dynamic-parameter/template-dynamic-parameter.component';
import { TemplateDynamicSignatureComponent } from './form-childs/template-dynamic-signature/template-dynamic-signature.component';
import { TemplateDynamicSqlComponent } from './form-childs/template-dynamic-sql/template-dynamic-sql.component';
import { HelperService } from '@app/shared/services/helper.service';

@Component({
  selector: 'template-dynamic-form',
  templateUrl: './template-dynamic-form.component.html',
  styles: []
})
export class TemplateDynamicFormComponent extends BaseComponent implements OnInit {
  @ViewChild('parameter')
  public parameterForm: TemplateDynamicParameterComponent;
  @ViewChild('signature')
  public signatureForm: TemplateDynamicSignatureComponent;
  @ViewChild('sql')
  public sqlForm: TemplateDynamicSqlComponent;

  navigationSubscription;
  formSave: FormGroup;
  laboutContractTypeList = [];
  templateTypeList = [];
  templateDynamicId: number;
  formConfig: any = {
    templateDynamicId: [''],
    organizationId: ['', [ValidationService.required]],
    positionId: [''],
    formTypeId: ['', [ValidationService.required]],
    labourContractTypeId: [''],
    startDate: ['', [ValidationService.required]],
    endDate: [''],
    description: ['', [ValidationService.maxLength(250)]]
  };

  constructor(public actr: ActivatedRoute
            , public templateDynamicService: TemplateDynamicService
            , public labourContractTypeService: LabourContractTypeService
            , public systemParameterService: SystemParameterService
            , private app: AppComponent
            , private router: Router
            , private helperService: HelperService) {
    super(actr, RESOURCE.TEMPLATE_DYNAMIC);
    if(!this.hasPermission('action.insert')) {
      return;
    }
    this.formSave = this.buildForm({}, this.formConfig, ACTION_FORM.INSERT,
      [ValidationService.notAffter('startDate', 'endDate', 'app.templateDynamic.endDate')]);

    this.formSave.addControl('file', new FileControl(null, ValidationService.required));

    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      // If it is a NavigationEnd event re-initalise the component
      if (e instanceof NavigationEnd && this.router.url.indexOf('edit') >= 0) {
        this.templateDynamicId = this.actr.snapshot.params.id;
        if (this.templateDynamicId) {
          this.templateDynamicService.findOne(this.templateDynamicId).subscribe(res => {
            this.helperService.isProcessing(true);
            this.formSave = this.buildForm(res.data, this.formConfig, ACTION_FORM.UPDATE,
              [ValidationService.notAffter('startDate', 'endDate', 'app.templateDynamic.endDate')]);
            this.formSave.addControl('file', new FileControl(null, ValidationService.required));
            if (res.data && res.data.fileAttachment) {
              (this.formSave.controls['file'] as FileControl).setFileAttachment(res.data.fileAttachment.templateDynamicFile);
            }
            this.parameterForm.initParameterForm(ACTION_FORM.UPDATE, this.propertyConfigs, res.data.lstTemplateParameter);
            this.signatureForm.initSignatureForm(ACTION_FORM.UPDATE, this.propertyConfigs, res.data.lstTemplateSignature);
            this.sqlForm.initSqlForm(ACTION_FORM.UPDATE, this.propertyConfigs, res.data.lstTemplateSql);
            this.helperService.isProcessing(false);
          });
        }
      }
    });
    this.loadReference();
  }
  ngOnInit() { }
  get f() {
    return this.formSave.controls;
  }
  private loadReference(): void {
    this.labourContractTypeService.getAllLabourContractTypeByStatus().subscribe(res => {
      this.laboutContractTypeList = res.data;
    });
    this.systemParameterService.findByName(SYSTEM_PARAMETER_CODE.TEMPLATE_DYNAMIC_FORM_TYPE)
    .subscribe(res => this.templateTypeList = res.data);
  }
  /**
   * processSaveOrUpdate
   */
  processSaveOrUpdate() {
    const formSave = CommonUtils.isValidForm(this.formSave);
    const formParameter = CommonUtils.isValidForm(this.parameterForm.formParameter);
    const formSignature = CommonUtils.isValidForm(this.signatureForm.formSignature);
    const formSql = CommonUtils.isValidForm(this.sqlForm.formSql);

    if (formSave && formParameter && formSignature && formSql) {
      const reqData = this.formSave.value;
      reqData.lstTemplateParameter = this.parameterForm.formParameter.value;
      reqData.lstTemplateSignature = this.signatureForm.formSignature.value;
      reqData.lstTemplateSql = this.sqlForm.formSql.value;
      reqData.fileSave = this.getFormFile();
      this.app.confirmMessage(null, () => {// on accepted
        this.templateDynamicService.saveOrUpdateFormFile(reqData)
          .subscribe(res => {
            if (this.templateDynamicService.requestIsSuccess(res)) {
              this.router.navigate(['/reports/template-dynamic']);
            }
          });
      }, () => {// on rejected

      });
    }
  }
  private getFormFile() {
    const _formValue = this.formSave.value;
    const fileSave = [];
    if (_formValue.file && _formValue.file.length > 0) {
      const fileInfos = (this.formSave.controls['file'] as FileControl).getFileAttachment();
      for (let index = 0; index < _formValue.file.length; index++) {
        if (fileInfos[index].sysLanguageCode) {
          const f = {
            file: _formValue.file[index],
            sysLanguageCode: fileInfos[index].sysLanguageCode,
            id: fileInfos[index].id ? fileInfos[index].id : null
          };
          fileSave.push(f);
        }
      }
    }
    return fileSave;
  }
  /****************** CAC HAM COMMON DUNG CHUNG ****/

  /**
   * validate save
   */
  private validateBeforeSave(): boolean {
    return true;
  }
}
