import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
const routes: Routes = [
  {
    path: '',
    redirectTo: '/dashboard/report',
    pathMatch: 'full'
  },
  {
    path: 'template-dynamic',
    loadChildren: './template-dynamic/template-dynamic.module#TemplateDynamicModule'
  },
  {
    path: 'report-dynamic',
    loadChildren: './report-dynamic/report-dynamic.module#ReportDynamicModule'
  },
  {
    path: 'report-custom',
    loadChildren: './report-custom/report-custom.module#ReportCustomModule'
  },
  {
    path: 'employee-list',
    loadChildren: './employee-list/employee-list.module#EmployeeListModule'
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule { }
