import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ReportCustomStatisticComponent } from './report-custom-statistic/report-custom-statistic.component';
import { RESOURCE } from '@app/core';
import { PropertyResolver } from '@app/shared/services';

const routes: Routes = [
  {
    path: '',
    component: ReportCustomStatisticComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportCustomRoutingModule { }
