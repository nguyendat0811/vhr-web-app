import { Injectable } from '@angular/core';
import { BasicService } from '@app/core/services/basic.service';
import { HttpClient } from '@angular/common/http';
import { HelperService } from '@app/shared/services/helper.service';
import { Observable } from 'rxjs';
import { CommonUtils } from '@app/shared/services';

@Injectable({
  providedIn: 'root'
})
export class ReportCustomService extends BasicService {

  constructor(public httpClient: HttpClient, public helperService: HelperService) {
    super('report', 'reportCustom', httpClient, helperService);
  }
  public export(data): Observable<any> {
    const url = `${this.serviceUrl}/export`;
    return this.postRequestFile(url,  CommonUtils.convertFormFile(data));
  }

  
  public getDataFilter(): Observable<any> {
    const url = `${this.serviceUrl}/get-data-filter`;
    return this.getRequest(url);
  }

}
