import { Component, OnInit, ViewChild, ComponentFactoryResolver, ViewContainerRef, QueryList, ViewChildren } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { RESOURCE, ACTION_FORM, APP_CONSTANTS ,SysCatService, EmpTypeService, PositionCategoryService} from '@app/core';
import { ActivatedRoute } from '@angular/router';
import { AppComponent } from '@app/app.component';
import { ReportCustomService } from '../report-custom.service';
import { ValidationService, CommonUtils } from '@app/shared/services';
import { PartyTreeSelectorComponent } from '@app/shared/components/party-tree-selector/party-tree-selector.component';
import { saveAs } from 'file-saver';
import { FormArray, FormGroup } from '@angular/forms';
import { ReportInputGenerateComponent } from '../../report-dynamic/report-dynamic-export/report-input-generate.component';
import { InputTextComponent } from '../../employee-list/entry-components/input-text.component';
import { InputDateRangeComponent } from '../../employee-list/entry-components/input-date-range.component';
import { InputComboboxComponent } from '../../employee-list/entry-components/input-combobox.component';
import { InputMultiComboboxComponent } from '../../employee-list/entry-components/input-multi-selectbox.component';
import { InputNumberRangeComponent } from '../../employee-list/entry-components/input-number-range.component';
import { AccordionTab } from 'primeng/accordion';
import { TranslationService } from 'angular-l10n';
@Component({
  selector: 'report-custom-statistic',
  templateUrl: './report-custom-statistic.component.html',
  styleUrls: ['./report-custom-statistic.component.css']
})
export class ReportCustomStatisticComponent extends BaseComponent implements OnInit {
  @ViewChild('orgTree')
  orgTree: PartyTreeSelectorComponent;
  formConfig: any = {
    reportDate: [new Date().getTime(),[ValidationService.required]],
    isPartyMember: [''],
    genderFilter: [''],
    positionGroupIds:[''],
    educationLevelIds : [''],
    staticalTypes:['',[ValidationService.required]],
    lstNodeCheck: [''],
    empTypeId : [''],
    averageAge:[1],
    organizationId:['']
  };
  formAdvanceConfig = {
    type: [null],
    properties: [null],
    fromDate: [null],
    toDate: [null],
    stringValue: [null],
    selectedValue: [null],
    multiSelectedValue: [null],
    fromNumber: [null, [ValidationService.number]],
    toNumber: [null, [ValidationService.number]]
  }
  listEducationLevel : Array<any>;
  listEmpType : Array<any>;
  public listManagementType: any
  public listWorkPlace: any
  public listPositionGroup: any
  listCondition = [];
  mapSelectData = {};
  formGroupAdvance: FormGroup;
  formArrayAdvance: FormArray;
  listIsPartyMember = APP_CONSTANTS.PARTY_MEMBER;
  listGender = APP_CONSTANTS.GENDERS;
  listStaticalTypes=APP_CONSTANTS.STATICAL_TYPE;
  @ViewChild('searchInfo') searchInfo: AccordionTab;
  @ViewChildren('viewContainerRef', { read: ViewContainerRef }) viewContainerRef: QueryList<ViewContainerRef>;
  constructor(public actr: ActivatedRoute
    , public reportCustomService: ReportCustomService
    , private sysCatService: SysCatService
    , public empTypeService : EmpTypeService
    , private positionCategoryService: PositionCategoryService
    , private app: AppComponent
    , private CFR: ComponentFactoryResolver
    , private translationService : TranslationService) {
    super(actr, RESOURCE.REPORT_CUSTOM, ACTION_FORM.SEARCH);
    this.formSearch = this.buildForm({}, this.formConfig);

    // this.positionCategoryService.getListPosition().subscribe(res => {
    //   this.listPositionGroup = res.data;
    // })
    // this.empTypeService.getAllByEmpTypeByIsUsed().subscribe(res => {
    //   this.listEmpType = res.data;
    // });
    // this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_CODE.EDUCATION_LEVEL).subscribe(res => {
    //   this.listEducationLevel = res.data;
    // });

    this.reportCustomService.getDataFilter().subscribe(res => {
      this.listCondition = res.data.lstFieldCondition;
      this.mapSelectData = res.data.mapSelectData;
    });

    this.buildFormArrayAdvance();
  }

  get f () {
    return this.formSearch.controls;
  }
  get fAdvance () {
    return this.formArrayAdvance.controls;
  }

  ngOnInit() {
  }
  public isSelect(node : any){
    if(node.parent){
      if(node.parent.expanded){
        return this.isSelect(node.parent);
      } else {
        return false;
      }
    }
    return true;
  }
  getListTree () {
    const lstNodeCheck: Array<Number> = [];
    const lstNodeBo: Array<Number> = [];


    if(this.orgTree.selectedNode != undefined ){
      this.orgTree.selectedNode.forEach(element => {

        if(!this.orgTree.showOrgExpried && element.styleClass) {
          lstNodeBo.push(parseInt(element.data));
        } else {
          if(this.isSelect(element)){
            lstNodeCheck.push(parseInt(element.data));
          }

        }

        if(element.expanded == false){
          this.clearNodeChil(lstNodeBo,element);
        }
       });
    }

    lstNodeBo.forEach(element =>{
      var idx = lstNodeCheck.indexOf(element);
      if(idx  > 0){
        lstNodeCheck.splice(idx,1);
      }
    })

     return lstNodeCheck;
  }

  public clearNodeChil(lstNodeBo: Array<Number> , node: any){
      var chil = node.children;
      if(chil && chil.length >0 ){
        chil.forEach(item => {
          lstNodeBo.push(parseInt(item.data));
          this.clearNodeChil(lstNodeBo,item);
        });
      }
  }

  public exportCumtomStatistic(){
    if (!CommonUtils.isValidForm(this.formSearch)) {
      return;
    }

    this.f.lstNodeCheck.setValue(this.getListTree());

    if(this.f.lstNodeCheck.value != ''){
      const params = this.formSearch.value;
      params.lstFilterCondition = this.formArrayAdvance.value;
      this.reportCustomService.export(params).subscribe(res => {
        saveAs(res, this.translationService.translate('app.reportCustom.titleReport') + '.xlsx');
      });
    }
    else{
      this.app.warningMessage('reportCustom.invalidOrg');
    }
  }

  private buildFormArrayAdvance() {
    this.formArrayAdvance = new FormArray([this.buildForm({}, this.formAdvanceConfig, ACTION_FORM.SEARCH,
      [ValidationService.notAffter('fromDate', 'toDate', 'app.salaryProcess.effectiveDateToDate'),
        ValidationService.notAffterNumber('fromNumber', 'toNumber', 'app.validateMessage.valueTo')]
      )]);
  }
  /**
   * Xử lý thêm dòng điều kiện tìm kiếm
   * @param index
   */
  addRowCondition(index) {
    const controls = this.formArrayAdvance as FormArray;
    controls.insert(index + 1, this.buildForm({}, this.formAdvanceConfig, ACTION_FORM.SEARCH,
      [ValidationService.notAffter('fromDate', 'toDate', 'app.salaryProcess.effectiveDateToDate'),
        ValidationService.notAffterNumber('fromNumber', 'toNumber', 'app.validateMessage.valueTo')]
      ));
  }

  /**
   * Xử lý xóa dòng điều kiện tìm kiếm
   * @param index
   */
  removeRowCondition(index) {
    const controls = this.formArrayAdvance as FormArray;
    if (controls.length === 1) {
      this.buildFormArrayAdvance();
    }
    controls.removeAt(index);
  }

  /**
   * Xử lý tạo form điều kiện tìm kiếm
   * @param event 
   * @param index 
   */
  onSelectCondition(event, index) {
    const objectCondition = this.findObjectCondition(event);
    if (!objectCondition) {
      return;
    }
    let componentFactory: any;
    switch (objectCondition.type) {
      case 'text':
        componentFactory = this.CFR.resolveComponentFactory(InputTextComponent);
        break;
      case 'date':
        componentFactory = this.CFR.resolveComponentFactory(InputDateRangeComponent);
        break;
      case 'selectbox':
        componentFactory = this.CFR.resolveComponentFactory(InputComboboxComponent);
        break;
      case 'multiSelect':
        componentFactory = this.CFR.resolveComponentFactory(InputMultiComboboxComponent);
        break;
      case 'number':
        componentFactory = this.CFR.resolveComponentFactory(InputNumberRangeComponent);
        break;
    }

    const viewContainerRef = this.viewContainerRef.toArray().find(x=> x.element.nativeElement.dataset.target == index+'');
    viewContainerRef.clear();
    const componentRef = viewContainerRef.createComponent(componentFactory);

    const formArray = this.formArrayAdvance as FormArray;
    let formGroup = formArray.controls[index] as FormGroup;
    formGroup.controls['type'].setValue(objectCondition.type);
    formGroup.controls['properties'].setValue(objectCondition.properties);
    formGroup.controls['fromDate'].setValue(null);
    formGroup.controls['toDate'].setValue(null);
    formGroup.controls['stringValue'].setValue(null);
    formGroup.controls['selectedValue'].setValue(null);
    formGroup.controls['multiSelectedValue'].setValue(null);
    formGroup.controls['fromNumber'].setValue(null);
    formGroup.controls['toNumber'].setValue(null);
    (<ReportInputGenerateComponent>componentRef.instance).formGroup = formGroup;
    (<ReportInputGenerateComponent>componentRef.instance).label = objectCondition.label;
    if (objectCondition.type == 'selectbox' || objectCondition.type == 'multiSelect') {
      (<ReportInputGenerateComponent>componentRef.instance).selectData = this.mapSelectData[objectCondition.properties];
    }
  }

  private findObjectCondition(properties) {
    return this.listCondition.find(x=>x.properties == properties);
  }

}
