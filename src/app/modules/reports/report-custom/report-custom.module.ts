import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportCustomStatisticComponent } from './report-custom-statistic/report-custom-statistic.component';
import { ReportCustomRoutingModule } from './report-custom-routing.module';
import { SharedModule } from '@app/shared';
import { AccordionModule } from 'primeng/accordion';

@NgModule({
  declarations: [ReportCustomStatisticComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReportCustomRoutingModule,
    AccordionModule
  ],
  
})
export class ReportCustomModule { }
