import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { HrStorage } from '@app/core/services/HrStorage';
import { AuthService } from '@app/core/services/auth.service';
import { UrlConfig, Constants } from '@env/environment';
import { HelperService } from '@app/shared/services/helper.service';
import { EmployeeInfoService } from '@app/core';
import { HomeEmployeeService } from '@app/core/services/home/home-employee.service';
import { SysLanguageService } from '@app/core/services/sys-language/sys-language.service';
import { EmpSystemConfigService } from '@app/core/services/sys-language/emp-system-config.service';
import { LocaleService } from 'angular-l10n';
import { CommonUtils } from '@app/shared/services/common-utils.service';

@Component({
  selector: 'auth-sso-login',
  templateUrl: './auth-sso-login.component.html',
  styles: []
})

export class AuthSsoLoginComponent implements OnInit {
  navigationSubscription;
  error: string;

  constructor(public router: Router,
    public authService: AuthService,
    public activatedRoute: ActivatedRoute,
    public helperService: HelperService,
    public employeeInfoService: EmployeeInfoService,
    private homeEmployeeService: HomeEmployeeService,
    private sysLanguageService: SysLanguageService,
    private empSystemConfigService: EmpSystemConfigService,
    public locale: LocaleService) {
    this.activatedRoute.queryParams.subscribe(params => {
      if (params['ticket']) {
        this.helperService.isProcessing(true);
        const user: any = {};
        user.access_token = params['ticket'];
        user.redirect = params['redirect'];
        HrStorage.setUserToken(user);
        this.authService.tokenInfos().subscribe(res => {
          if (res && res.data) {
            user.userId = res.data.userId;
            user.employeeCode = res.data.staffCode;
            user.tokenExpiresIn = res.data.exp;
          }
          this.authService.actionRequestAuthorities(Constants.applicationCode)
            .subscribe(data => {
              const authorities = data.data;
              if (authorities && authorities.length > 0) {
                user.userPermissionList = authorities[0].permissions;
                user.userMenuList = authorities[0].grantedMenus;
                this.employeeInfoService.findByEmployeeCode(user.employeeCode)
                  .subscribe(userInfo => {
                    user.userInfo = userInfo.data;
                    this.empSystemConfigService.getIntro().subscribe(
                      resI => {
                        HrStorage.setIntro(resI.data);
                      }
                    );
                    HrStorage.setUserToken(user);
                    this.initMarketCompany();
                  });
              } else {
                this.error = 'Tài khoản chưa được phân quyền miền dữ liệu!';
              }
            });
        });
      } else {
        this.error = 'Invalid username or password';
      }
    });
  }
  public redirectToLogin() {
    CommonUtils.logoutAction();
  }

  private redirectAddress() {
    const url = '/home';
    // Do later: set currentUrl when redirect from SSO
    // let url = HrStorage.getCurrentUrl();
    // console.log(url);
    // if (url === undefined || url === null) {
    //   url = '/home';
    // }
    this.router.navigate([url]);
  }

  ngOnInit() {
  }
  private initSysLanguage() {
    /**
     * Lấy danh sách ngôn ngữ hỗ trợ của hệ thống
     */
    this.sysLanguageService.findAll().subscribe(res => {
      HrStorage.setListLang(res);
      const selectedMarket = HrStorage.getSelectedMarket();
      if (selectedMarket) {
        for (const lang of res) {
          if (lang.code === selectedMarket.language) {
            this.locale.setCurrentLanguage(lang.code);
            HrStorage.setSelectedLang(lang);
          }
        }
        this.helperService.isProcessing(false);
        this.redirectAddress();
      }
      else {
        this.error = 'Tài khoản chưa được phân quyền miền dữ liệu!';
      }
    });
  }
  private initMarketCompany() {
    // Lay thong tin market company
    this.homeEmployeeService.getMarketCompany().subscribe(res => {
      HrStorage.setListMarket(res.data);
      this.initSysLanguage();
    });
  }

}
