import { NgModule } from '@angular/core';

import { MyModalComponent } from './modals/my-modal.component';

import { HomeComponent } from './pages/home.component';
import { HomeRoutingModule } from './home.routing';
import { ChartModule } from 'primeng/chart';
import { SharedModule } from '@app/shared';
import { HomeWarningComponent } from './pages/home-warning.component';
import { HomeRadarComponent } from './pages/home-radar.component';
import { HomeLineChartComponent } from './pages/home-line-chart.component';

@NgModule({
    declarations: [
        HomeComponent,
        HomeWarningComponent,
        HomeRadarComponent,
        HomeLineChartComponent,
        MyModalComponent
    ],
    imports: [
        SharedModule,
        ChartModule,
        HomeRoutingModule
    ],
    exports: [],
    providers: [],
    entryComponents: [MyModalComponent]
})
export class HomeModule {}
