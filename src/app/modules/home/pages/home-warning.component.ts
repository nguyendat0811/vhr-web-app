import { OnInit, Component } from '@angular/core';
import { HomeWarningService } from '@app/core/services/home-warning.service';
import { CommonUtils } from '@app/shared/services';



@Component({
  selector: 'home-warning',
  templateUrl: './home-warning.component.html'
})
export class HomeWarningComponent implements OnInit {
  private listWarningType = ['warningEmployeeCount', 'warningExpriteContractProcess'
                              , 'warningWorkProcessCount', 'documentRejectReceiveWarning'
                              , 'documentReceiveWarning', 'documentWaitingApprove'
                              , 'partyNotify', 'politicalWarning'
                              , 'warningEmpHaveNotBankAccount', 'warningEmpHaveNotRelationShip'
                              , 'warningEmpHaveNotPositionSalary', 'warningEmpExpiredPositionSalary'
                              , 'warningEmpTranferVhrToGhr', 'warningRecruitAnouncementCount'];
  // Những cảnh báo chưa đủ table để làm :
  // - warningEmployeeCount: tác động đến quỹ lương của đơn vị( lí do : thiếu bảng employee_warning)
  // - warningEmpFileCount: Nhân viên thiếu hồ sơ ( lí do : thiếu bảng emp_document_mapping)
  // - documentRequestUpdate: Hồ sơ trong đơn vị cần cập nhật ( lí do : thiếu bảng document_config, emp_document_mapping)
  // - partyEmployeeTransfer: Đảng viên chưa tiếp nhận ( lí do : thiếu bảng party_employee_transfer)
  // - partyOrgNoneMaping: Đơn vị chưa thuộc tổ chức Đảng (lí do : thiếu bảng party_organization)
  // - employeeManagerWarn: Chưa nhập thông tin người QLTT (lí do : thiếu bảng EMPLOYEE_MANAGER)
  // - noSalaryIncrease: Cần đề xuất thay đổi hệ số lương (lí do : thiếu bảng Salary_increase_unexpect)
  // - noSalaryOrgNum: Quá trình tính lương chưa có đơn vị (chưa rõ luồng)
  // - warningRecruitAnouncementCount: Hồ sơ đã trúng tuyển (lí do: vì chưa call được đến ttnsSevice )
  // - noBookNum: Nhân viên chưa có sổ bảo hiểm (lí do: trong service thiếu bảng insurance_social_base)
  // - warningEmpHaveNotEst: Without working time (lí do: trong service thiếu bảng establish_timekeeping_emp)
  // - warningEmpExpiredEst: Nhân viên hết hạn cấu hình chấm công (lí do: trong service thiếu bảng establish_timekeeping_emp)

  public listWarningData = [];
  constructor(private homeWarningService: HomeWarningService) {

  }
  ngOnInit(): void {
    this.loadWarning(this.listWarningType[0]);
  }
  loadWarning(warningType: string) {
    this.homeWarningService.getWarningByType(warningType).subscribe(res => {
      const warningData = res.data;
      if (warningData && warningData.hasPermission) {
        this.listWarningData.push(warningData);
      }
      const nextWarningType = this.getNextWarningType(warningType);
      if (!CommonUtils.isNullOrEmpty(nextWarningType)) {
        this.loadWarning(nextWarningType);
      }
    });
  }
  getNextWarningType(warningType: string): string {
    const index = this.listWarningType.indexOf(warningType);
    if (index === -1) {
      return null;
    }
    if (index < this.listWarningType.length - 1) {
      return this.listWarningType[index + 1];
    }
    return null;
  }
}
