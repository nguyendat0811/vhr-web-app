import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { Project, RESOURCE, ACTION_FORM } from '@app/core';
import { Observable } from 'rxjs';

import { MyModalComponent } from '../modals/my-modal.component';
import { ProjectService } from '@app/core/services/project.service';

import { CommonUtils } from '@app/shared/services/common-utils.service';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent extends BaseComponent implements OnInit {
    public commonUtils: CommonUtils;
    projects$: Observable<Project[]>;

    constructor(
        private modalService: NgbModal,
        private projectService: ProjectService,
        public act: ActivatedRoute
    ) {
        super(act, RESOURCE.HOME, ACTION_FORM.SEARCH);
     }

    ngOnInit(): void {
        this.loadProjects();
    }

    loadProjects() {
        this.projects$ = this.projectService.getAll();
    }

    openMyModal() {
        const modalRef = this.modalService.open(MyModalComponent);
        modalRef.componentInstance.id = 1;
        modalRef.result.then((result) => {
            console.log(result);
        });
    }

}
