import { OnInit, Component } from '@angular/core';
import { HomeWarningService } from '@app/core/services/home-warning.service';
import { CommonUtils } from '@app/shared/services';



@Component({
  selector: 'home-radar',
  templateUrl: './home-radar.component.html'
})
export class HomeRadarComponent implements OnInit {
  data: any;
  constructor(private homeWarningService: HomeWarningService) {
    this.data = {
      labels: ['Nghành CNTT', 'Nghành kế toán', 'Ngành thuế', 'Ngành đầu tư', 'Ngành Tài sản', 'Ngành đào tạo', 'Đầu tư xây dựng'],
      datasets: [
          {
              label: 'Nam',
              backgroundColor: 'rgba(46,140,205,0.2)',
              borderColor: 'rgba(46,140,205,1)',
              pointBackgroundColor: 'rgba(46,140,205,1)',
              pointBorderColor: '#fff',
              pointHoverBackgroundColor: '#fff',
              pointHoverBorderColor: 'rgba(46,140,205,1)',
              data: [65, 59, 90, 81, 56, 55, 40]
          },
          {
              label: 'Nữ',
              backgroundColor: 'rgba(220,53,69,0.2)',
              borderColor: 'rgba(220,53,69,1)',
              pointBackgroundColor: 'rgba(220,53,69,1)',
              pointBorderColor: '#fff',
              pointHoverBackgroundColor: '#fff',
              pointHoverBorderColor: 'rgba(220,53,69,1)',
              data: [28, 48, 40, 19, 96, 27, 100]
          }
      ]
    };
  }
  ngOnInit(): void {
  }

}
