import { LabourContractDetailService } from '@app/core/services/labour-contract-detail/labour-contract-detail.service';
import { TranslationService } from 'angular-l10n';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { Component, OnInit, Input, ViewChildren, QueryList, HostListener } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ValidationService } from '@app/shared/services';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { AppComponent } from '@app/app.component';
import { EmployeeInfoService, NationService, SysCatService, OrganizationService, EthnicService, ReligionService, EmpTypeService, LabourContractTypeService, EmpTypeProcessService } from '@app/core';
import { DocumentTypeService, APP_CONSTANTS, RESOURCE, ACTION_FORM } from '@app/core';
import { NationConfigService } from '@app/core';
import { trigger, state, style, transition, animate, AnimationEvent } from '@angular/animations';
import { OrgSelectorComponent } from '@app/shared/components/org-selector/org-selector.component';
import { FileControl } from '@app/core/models/file.control';
import { ActivatedRoute } from '@angular/router';
import { MenuItem } from 'primeng/api';
import { WorkScheduleService } from '@app/core/services/hr-timekeeping/work-schedule.service';

@Component({
  selector: 'employee-info-index',
  templateUrl: './employee-info-index.component.html',
  styleUrls: ['./employee-info-index.component.css'],
  animations: [
    trigger('animation', [
      state('visible', style({

      })),
      transition('void => *', [
        style({ transform: 'translateX(50%)', opacity: 0 }),
        animate('300ms ease-out')
      ]),
      transition('* => void', [
        animate(('250ms ease-in'), style({
          height: 0,
          opacity: 0,
          transform: 'translateX(50%)'
        }))
      ])
    ])
  ],
})
export class EmployeeInfoIndexComponent extends BaseComponent implements OnInit {
  items: MenuItem[];
  activeIndex = 0;
  formSave: FormGroup;
  formCommonInfo: FormGroup;
  formSendWarning: FormGroup;
  formPersonalInfo: FormGroup;
  formEmploymentInfo: FormGroup;
  formSaveT: FormGroup;
  nationList: any;
  ethnicList: any;
  maritalList: any;
  recruitTypeList: any;
  religionList: any;
  documentTypeList: any;
  columns: number[];
  columns1: number[];
  checkNation: boolean;
  checkDocument: boolean;
  requirePersionalId: any;
  employee: any;
  addNewSuccess = false;
  unitName: String;
  listEmail: any;
  empWelcomeEmail: any;
  filterByViettelStartDate = '';
  empTypeList: any;
  workingTimeList: any;
  isNotHDLD: boolean; // ko phai hop dong lao dong
  isHDLD: boolean;  // la hop dong lao dong
  isLabourContract: boolean;
  isLabourService: boolean; // la họp dong dich vu
  isLabourNonRequired: boolean;
  fileControl = new FileControl(null);
  soldierLevelList: any;
  laboutTypeList: any;
  labourContractDetailList: any;

  @ViewChildren('unit') unit: QueryList<OrgSelectorComponent>;
  @ViewChildren('profileImg') profileImg;
  @Input() item: any;

  formCommonInfoConfig = {
    firstName: ['', [ ValidationService.required, Validators.maxLength(20) ]],
    middleName: ['', [ Validators.maxLength(20) ]],
    lastName: ['', [ ValidationService.required, Validators.maxLength(20) ]],
    dateOfBirth: ['', [ ValidationService.required, ValidationService.beforeCurrentDate ]],
    nationId: ['', [ ValidationService.required ]],
    otherEmail: ['', [ Validators.maxLength(200), ValidationService.emailFormat ]],
    gender: ['', [ ValidationService.required ]],
    pidNumber: [null, [ Validators.maxLength(20)]],
    pidIssuedDate: ['', [ ValidationService.beforeCurrentDate ]],
    pidIssuedPlace: ['', [ Validators.maxLength(200) ]],
    passportNumber: ['', [ Validators.maxLength(50) ]],
    passportIssueDate: ['', [ ValidationService.beforeCurrentDate ]],
    // imagePath: [null],
  };

  formPersonalInfoConfig = {
    religionId: [''],
    ethnicId: [''],
    maritalStatus: [''],
    placeOfBirth: ['', [ Validators.maxLength(500) ]],
    origin: ['', [ Validators.maxLength(500) ]],
    permanentAddress: ['', [ Validators.maxLength(500), Validators.maxLength(500) ]],
    currentAddress: ['', [ Validators.maxLength(500)]],
    mobileNumber: ['', [ Validators.maxLength(50), ValidationService.mobileNumber ]],
    mobileNumber2: ['', [ Validators.maxLength(50), ValidationService.mobileNumber ]],
    mobileNumber3: ['', [ Validators.maxLength(50), ValidationService.mobileNumber ]]
  };

  formEmploymentInfoConfig = {
    organizationId: ['', [ ValidationService.required]],
    viettelStartDate: ['', [ ValidationService.required, ValidationService.beforeCurrentDate ]],
    documentNumber: ['', [ Validators.maxLength(50), ValidationService.required ]],
    positionId: ['', [ ValidationService.required ]],
    documentTypeId: ['', [ ValidationService.required ]],
    recruitTypeId: ['']
  };

  formSendWarningConfig = {
    sendWarningId: [''],
    listEmailToSendWarning: ['']
  };
  formConfig = {
    empTypeProcessId: [''],
    empTypeId: [ '', [ValidationService.required]],
    labourContractTypeId: [ '', [ValidationService.required]],
    labourContractDetailId: [ '', [ValidationService.required]],
    effectiveDate: [ '', [ValidationService.required]],
    expiredDate: [''],
    signedDate: [ '', [ValidationService.required]],
    contractDecisionNumber: [ '', [ValidationService.required, ValidationService.maxLength(50)]],
    soldierLevelId: [''],
    managementTypeId: [ ''],
    description: [ '', [ValidationService.maxLength(500)]],
    contractMonth: [ '', [ValidationService.maxLength(4), ValidationService.positiveInteger, Validators.min(1), Validators.max(1000)]],
    workingTimeId: [ '', [ValidationService.required]],
  };
  formConfigNotHDLD = {
    empTypeProcessId: [''],
    empTypeId: [ '', [ValidationService.required]],
    effectiveDate: [ '', [ValidationService.required]],
    expiredDate: [''],
    signedDate: [ '', [ValidationService.required]],
    contractDecisionNumber: [ '', [ValidationService.required, ValidationService.maxLength(50)]],
    soldierLevelId: [''],
    managementTypeId: [ ''],
    description: [ '', [ValidationService.maxLength(500)]],
  };

  constructor(
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    public activeModal: NgbActiveModal,
    public translationService: TranslationService,
    private employeeInfoService: EmployeeInfoService,
    private orgService: OrganizationService,
    private documentTypeService: DocumentTypeService,
    private nationConfigService: NationConfigService,
    private nationService: NationService,
    private app: AppComponent,
    private sysCatService: SysCatService,
    private workScheduleService: WorkScheduleService,
    private empTypeService: EmpTypeService,
    private labourContractTypeService: LabourContractTypeService,
    private labourContractDetailService: LabourContractDetailService,
    private empTypeProcessService: EmpTypeProcessService

  ) {
    super(activatedRoute, RESOURCE.EMPLOYEE, ACTION_FORM.EMP_COMMON_INFO_INSERT);

    this.showSoldierLevel();
    this.builFormSaveT();
    this.makeFormSave();
    this.buildFormCommonInfo();
    this.buildFormEmploymentInfo();
    this.buildFormPersonalInfo();
    this.buildFormSendWarning();
    this.checkNation = true;
    this.checkDocument = true;
    this.listEmail = [];
    this.employee = {
      employeeId: '',
      employeeCode: '',
      fullName: '',
      dateOfBirth: '',
      organizationId: '',
      organizationName: '',
      organizationFullName: '',
      email: '',
      viettelStartDate: ''
    };

    // Quoc tich
    this.nationService.getNationList()
    .subscribe(res => {
      this.nationList = res.data;
    });

    // Loại tuyển dụng
    this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.RECUITMENT_CATEGORY)
    .subscribe(res => this.recruitTypeList = res.data);

    this.documentTypeService.findDocumentTypeJoinMarket()
    .subscribe(res => {
      this.documentTypeList = res.data.filter(item => item['inoutFlag'] === 1);
    });
    // diện đối tượng
    this.empTypeService.getAllByEmpTypeByIsUsed().subscribe(res => {
      this.empTypeList = res.data;
    });
    this.labourContractTypeService.findActiveLabourContractType().subscribe(res => {
      this.laboutTypeList = res.data;
    });
  }

  ngOnInit() {
    this.isNotHDLD = true;
    this.isHDLD = true;
    this.isLabourContract = true;
    this.isLabourService = false;
    this.isLabourNonRequired = true;
    this.columns = [];
    this.columns1 = [];
    this.items = [
      {
        label: this.translationService.translate('app.employee-info.commonInfo'),
        command: (event: any) => {
          this.activeIndex = 0;
        },
        disabled: false
      },
      {
        label: this.translationService.translate('app.employee-info.employmentInfo'),
        command: (event: any) => {
          this.activeIndex = 1;
        },
        disabled: true
      },
      {
        label: this.translationService.translate('app.contractProcess.insert'),
        command: (event: any) => {
          this.activeIndex = 2;
        },
        disabled: true
      },
      {
        label: this.translationService.translate('app.employee-info.personalInfo'),
        command: (event: any) => {
          this.activeIndex = 3;
        },
        disabled: true
      },
      {
        label: this.translationService.translate('app.employee-info.informEmail'),
        command: (event: any) => {
          this.activeIndex = 4;
        },
        disabled: true
      }
    ];
    this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.WORKING_TIME).subscribe(res => {
      this.workingTimeList = res.data;
    });
  }

  addMobileNumber2() {
    this.columns.push(this.columns.length);
  }

  removeMobileNumber2() {
    this.columns.splice(-1, 1);
    this.formPersonalInfo.controls['mobileNumber2'].reset();
  }

  addMobileNumber3() {
    this.columns1.push(this.columns1.length);
  }

  removeMobileNumber3() {
    this.columns1.splice(-1, 1);
    this.formPersonalInfo.controls['mobileNumber3'].reset();
  }

  /**
   * process save emp_common_info
   */
  processSaveOrUpdate() {
    if (!CommonUtils.isValidForm(this.formCommonInfo)) {
      this.activeIndex = 0;
      return;
    }

    if (!CommonUtils.isValidForm(this.formEmploymentInfo)) {
      this.activeIndex = 1;
      return;
    }

    if (!CommonUtils.isValidForm(this.formPersonalInfo)) {
      return;
    }
    this.formSaveT.addControl('fileEmpType', this.fileControl);
    CommonUtils.copyProperties(this.formSave.value, this.formCommonInfo.value);
    CommonUtils.copyProperties(this.formSave.value, this.formPersonalInfo.value);
    CommonUtils.copyProperties(this.formSave.value, this.formEmploymentInfo.value);
    CommonUtils.copyProperties(this.formSave.value, this.formSaveT.value);
    this.app.confirmMessage(null, () => {// on accepted
      this.employeeInfoService.saveOrUpdateFormFile(this.formSave.value)
        .subscribe(res => {
          if (this.employeeInfoService.requestIsSuccess(res)) {
            this.employee = res.data;

            this.employeeInfoService.getOrgManagerByOrgId(this.employee.organizationId)
            .subscribe(result => {
              if (result.data.email) {
                const emailAddress = {
                  display: result.data.email,
                  value: result.data.email
                };
                this.listEmail.push(emailAddress);
              }
            });

            this.activeIndex = 4;
            this.items[0].disabled = true;
            this.items[1].disabled = true;
            this.items[2].disabled = true;
            this.items[3].disabled = false;
            this.items[4].disabled = false;
            this.makeFormSave();
            this.buildFormCommonInfo();
            this.buildFormEmploymentInfo();
            this.buildFormPersonalInfo();
          }
        });
      }, () => {// on rejected
    });
  }

 /**
   * processCancel
   */
  processCancel() {
    this.app.confirmMessage('common.message.confirm.cancel', () => {
      // on accepted
      this.activeIndex = 0;
      this.items[1].disabled = true;
      this.items[2].disabled = true;
      this.makeFormSave();
      this.buildFormCommonInfo();
      this.buildFormEmploymentInfo();
      this.buildFormPersonalInfo();
    }, () => {
      // on rejected
    });
  }

  public makeFormSave() {
    this.formSave = this.formBuilder.group({
      employeeId: [''],
      firstName: [''],
      middleName: [''],
      lastName: [''],
      aliasName: [''],
      dateOfBirth: [''],
      nationId: [''],
      otherEmail: [''],
      gender: [''],
      pidNumber: [null],
      pidIssuedDate: [''],
      pidIssuedPlace: [''],
      passportNumber: [''],
      passportIssueDate: [''],
      // imagePath: [null],
      religionId: [''],
      ethnicId: [''],
      maritalStatus: [''],
      placeOfBirth: [''],
      origin: [''],
      permanentAddress: [''],
      currentAddress: [''],
      mobileNumber: [''],
      mobileNumber2: [''],
      mobileNumber3: [''],
      organizationId: [''],
      viettelStartDate: [''],
      documentNumber: [''],
      positionId: [''],
      documentTypeId: [''],
      recruitTypeId: [''],
      empTypeId: [''],
      labourContractTypeId: [''],
      labourContractDetailId: [''],
      effectiveDate: [''],
      expiredDate: [''],
      signedDate: [''],
      contractDecisionNumber: [''],
      soldierLevelId: [''],
      managementTypeId: [''],
      description: [''],
      contractMonth: [''],
      workingTimeId: ['']


    });
    this.formSave.addControl('file', new FileControl(null));
    this.formSave.addControl('fileEmpType', new FileControl(null));
  }

  buildFormCommonInfo() {
    this.formCommonInfo = this.buildForm({}, this.formCommonInfoConfig, ACTION_FORM.EMP_COMMON_INFO_INSERT);
    this.formCommonInfo.addControl('file', new FileControl(null));
  }
  builFormSaveT() {
    this.formSaveT = this.buildForm({}, this.formConfig
      , ACTION_FORM.INSERT
      , [ ValidationService.notAffter('signedDate', 'effectiveDate', 'app.contractProcess.effectiveDate')
         , ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.contractProcess.expiredDate')]);
    this.formSaveT.addControl('fileEmpType', new FileControl(null));
  }

  buildFormEmploymentInfo() {
    this.formEmploymentInfo = this.buildForm({}, this.formEmploymentInfoConfig, ACTION_FORM.EMP_EMPLOYMENT_INFO_INSERT);
    this.formEmploymentInfo.get('viettelStartDate').valueChanges.subscribe( res => {
      this.formEmploymentInfo.get('positionId').setValue(null);
      this.createFilterByViettelStartDate();
    });
    this.unitName = '';
  }

  buildFormPersonalInfo() {
    this.formPersonalInfo = this.buildForm({}, this.formPersonalInfoConfig, ACTION_FORM.EMP_PERSONAL_INFO_INSERT);
  }

  buildFormSendWarning() {
    this.formSendWarning = this.buildForm({}, this.formSendWarningConfig, ACTION_FORM.INSERT);
  }

  createFilterByViettelStartDate() {
    this.filterByViettelStartDate = '';
    const viettelStartDate = this.formEmploymentInfo.get('viettelStartDate').value || null;
    if (viettelStartDate) {
      const strSd = new Date(viettelStartDate).getFullYear() + '-'
                  + (new Date(viettelStartDate).getMonth() + 1) + '-'
                  + new Date(viettelStartDate).getDate();
      this.filterByViettelStartDate = ' AND ( obj.effective_date <= date(\'' + strSd + '\') ) AND obj.expired_date IS NULL ';
    }
  }
  private buildForms(data?: any,  actionForm?: ACTION_FORM): void {
    this.formSaveT = this.buildForm(data, this.formConfig
      , actionForm
      , [ValidationService.notAffter('signedDate', 'effectiveDate', 'app.contractProcess.effectiveDate')
      , ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.contractProcess.expiredDate')]);
  }
  private buildFormNotHDLD(data?: any,  actionForm?: ACTION_FORM) {
    this.formSaveT = this.buildForm(data, this.formConfigNotHDLD
      , actionForm
      , [ValidationService.notAffter('signedDate', 'effectiveDate', 'app.contractProcess.effectiveDate')
      , ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.contractProcess.expiredDate')]);
  }

  get f () {
    return this.formSaveT.controls;
  }

  get fCommonInfo() {
    return this.formCommonInfo.controls;
  }

  get fPersonalInfo() {
    return this.formPersonalInfo.controls;
  }

  get fEmploymentInfo() {
    return this.formEmploymentInfo.controls;
  }

  public validateFormCommonInfo() {
    if (CommonUtils.isValidForm(this.formCommonInfo)) {
      this.employeeInfoService.validateCommonInfo(this.formCommonInfo.value).subscribe(res => {
        if (this.orgService.requestIsSuccess(res)) {
          this.activeIndex = 1;
          this.items[1].disabled = false;
        }
      });
    }
  }

  public validateFormEmploymentInfo() {
    if (CommonUtils.isValidForm(this.formEmploymentInfo)) {
      this.employeeInfoService.validateWorkprocess(this.formEmploymentInfo.value).subscribe(res => {
        if (this.orgService.requestIsSuccess(res)) {
          this.activeIndex = 2;
          this.items[2].disabled = false;
        }
      });
    }
  }

  public addEmploymentInfo() {
    if (CommonUtils.isValidForm(this.formSaveT)) {
      this.activeIndex = 3;
      this.items[3].disabled = false;

    }
  }

  /**
   * onChangeUnit
   */
  public onChangeUnit(event) {
    if (event) {
      this.unit.last.displayName.first.nativeElement.value = event.code;
      this.orgService.checkAllowedAddEmployee(event.organizationId)
      .subscribe(result => {
        if (this.orgService.requestIsSuccess(result)) {
          this.orgService.findOrgFullName(event.organizationId)
          .subscribe(res => {
            this.unitName = res.data;
          });
        } else {
          this.unitName = '';
          this.formEmploymentInfo.controls['organizationId'].setValue('');
          this.unit.last.displayName.first.nativeElement.value = '';
        }
      });
    }
  }

  /**
   * onFileChange
   */
  public onFileChange(files) {
    if (files) {
      const reader = new FileReader();
      reader.onload = (e: any) => {
        this.profileImg.first.nativeElement.src = e.target.result;
      };

      reader.readAsDataURL(files);
    } else {
      this.profileImg.first.nativeElement.src = '/assets/images/noimage.png';
    }
  }

  // onChangeNation
  public onChangeNation(event) {
    const pidNumber = this.formCommonInfo.value.pidNumber;
    const nationId = CommonUtils.nvl(event);

    if (event) {
      this.nationService.findOne(nationId).subscribe(res => {
        this.requirePersionalId = res.data.requirePersionalId;
        if (this.requirePersionalId === 1) {
          this.checkNation = false;
          this.formCommonInfo.removeControl('pidNumber');
          this.formCommonInfo.addControl('pidNumber', new FormControl(pidNumber,
            [ValidationService.required, ValidationService.maxLength(20)]));
        } else {
          this.checkNation = true;
          this.formCommonInfo.removeControl('pidNumber');
          this.formCommonInfo.addControl('pidNumber', new FormControl(pidNumber,
            [ValidationService.maxLength(20)]));
        }
      });
    } else {
      // If remove nation => reset pIdNumber
      this.checkNation = true;
      this.formCommonInfo.removeControl('pidNumber');
      this.formCommonInfo.addControl('pidNumber', new FormControl(pidNumber,
        [ValidationService.maxLength(20)]));
    }

    // Get list ethnic by nation
    this.nationConfigService.getNationConfigList(APP_CONSTANTS.NATION_CONFIG_TYPE.ETHNIC, nationId).subscribe(
      res => {
        this.ethnicList = [];
        for (const item of res.data) {
          this.ethnicList.push({label: item.name, value: item.nationConfigId});
        }
      }
    );

    // Get list religion by nation
    this.nationConfigService.getNationConfigList(APP_CONSTANTS.NATION_CONFIG_TYPE.RELIGION, nationId).subscribe(
      res => {
        this.religionList = [];
        for (const item of res.data) {
          this.religionList.push({label: item.name, value: item.nationConfigId});
        }
      }
    );
  }

  public onChangeDocument(event) {
    const recruitTypeId = this.formEmploymentInfo.value.recruitTypeId;
    if (this.formEmploymentInfo.get('documentTypeId').value !== 387) {
      this.checkDocument = true;
      this.formEmploymentInfo.removeControl('recruitTypeId');
      this.formEmploymentInfo.addControl('recruitTypeId', new FormControl(recruitTypeId));
    } else {
      this.checkDocument = false;
      this.formEmploymentInfo.removeControl('recruitTypeId');
      this.formEmploymentInfo.addControl('recruitTypeId', new FormControl(recruitTypeId,
        [ ValidationService.required ]));
    }
  }

  public onAddEmailAddress() {
    // if (this.listEmail.length > 0) {
    //   const emailAddress = this.listEmail[this.listEmail.length - 1].value;
    //   if (emailAddress) {
    //     this.employeeInfoService.checkEmailAddress(emailAddress)
    //     .subscribe(res => {
    //       if (!res.data) {
    //         this.listEmail.splice(this.listEmail.length - 1, 1);
    //       }
    //     });
    //   }
    // }
  }

  public sendWarningEmail() {
    setTimeout(() => {
      const listEmailToSendWarning = [];
      if (this.listEmail) {
        for (let i = 0; i < this.listEmail.length; i++) {
          if (this.listEmail[i].value) {
            listEmailToSendWarning[i] = this.listEmail[i].value;
          }
        }
        this.formSendWarning.controls['listEmailToSendWarning'].setValue(listEmailToSendWarning);
        this.employeeInfoService.sendWelcomeEmail(this.employee.employeeId, this.formSendWarning)
          .subscribe(result => {
            if (this.employeeInfoService.requestIsSuccess(result)) {
              this.items[3].disabled = true;
              this.activeIndex = 5;
            } else {
              this.app.messageError('WARN', 'WARNING.emailAddress.deleted', ': ' + result.data);
            }
          });
      } else {
        this.items[3].disabled = true;
        this.activeIndex = 5;
      }
    }, 100);
  }

  // thêm
  public showSoldierLevel() {
    this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.SOLDIER_LEVEL).subscribe(res => {
      this.soldierLevelList = res.data;
    });
  }

   /**
   * onEmpTypeChange
   * param event
   */
  public onEmpTypeChange(event, data , isDefault?: boolean) {

    if (!event) {
      return;
    }
    let formDefaultValue;
    if (data) {
       formDefaultValue = data ;
    } else {
      formDefaultValue = this.formSaveT.value;
    }
    this.empTypeService.findByIdMarketCompanyId(CommonUtils.getCurrentMarketCompanyId(), event).subscribe(res => {

      if (res.data) {
        if (res.data.hasLabourContractInfo === 1 && res.data.hasSoldierInfo === 1) {

          this.buildForms(formDefaultValue, null);
          this.isNotHDLD = true;
          this.isHDLD = true;
          this.isLabourContract = true;
          this.isLabourService = false;
          this.isLabourNonRequired = true;
        } else if (res.data.hasLabourContractInfo === 1) {
          this.buildForms(formDefaultValue, null);
          this.formSaveT.removeControl('soldierLevelId');
          this.isNotHDLD = false;
          this.isHDLD = true;
        } else if (res.data.hasSoldierInfo === 1) {
          this.buildFormNotHDLD(formDefaultValue, null);
          this.showSoldierLevel();
          this.isNotHDLD = true;
          this.isHDLD = false;
          this.isLabourContract = true;
        } else if (res.data.hasLabourContractInfo === 0 && res.data.hasSoldierInfo === 0) {
          this.isNotHDLD = false;
          this.isHDLD = false;
        }
      }

    });
  }

  /**
   * onLabourContractTypeChange
   * param event
   */
  public onLabourContractTypeChange(e, isDefault?: boolean) {
    if (e) {
      this.labourContractTypeService.findByIdMarketCompanyId(CommonUtils.getCurrentMarketCompanyId(), e).subscribe(res => {
        // xu ly an hien chi tiet hop dong
        let contracTypeDetailValue = null;
        if (isDefault) {
          contracTypeDetailValue = this.formSaveT.value.labourContractDetailId;
        }
        // Kiem trloai hop dong dang chon la gi?
        this.labourContractTypeService.checkLabourType(res.data.code).subscribe( res1 => {
          if (res1.data === 1) {
            // Hop dong thoi vu hoac Hop dong CTV
            this.isLabourService = false;
            this.isLabourNonRequired = false;
            this.formSaveT.removeControl('workingTimeId');
          } else if (res1.data === 2) {
              // Chi la hop dong dich vu
              this.isLabourService = true;
              this.isLabourNonRequired = false;
              const workingTimeIdDefault = this.formSaveT.value.workingTimeId;
              this.formSaveT.removeControl('workingTimeId');
              this.formSaveT.addControl('workingTimeId', CommonUtils.createControl(this.actionForm, 'workingTimeId',
                workingTimeIdDefault, [ValidationService.required]));
          } else {
            // Cac loai hop dong khac
            this.isLabourService = false;
            this.isLabourNonRequired = true;
            this.formSaveT.removeControl('workingTimeId');
          }

          // Xu ly validate thoi han hop dong
          const contractMonthValue = this.formSaveT.value.contractMonth;
          if (res1.data === 1 || res1.data === 2) {
            this.formSaveT.removeControl('contractMonth');
            this.formSaveT.addControl('contractMonth', CommonUtils.createControl(this.actionForm, 'contractMonth',
              contractMonthValue, [ValidationService.required, ValidationService.positiveInteger, Validators.min(1),
                Validators.max(1000), ValidationService.maxLength(4)]));
          } else {
            this.formSaveT.removeControl('contractMonth');
            this.formSaveT.addControl('contractMonth', CommonUtils.createControl(this.actionForm, 'contractMonth',
              contractMonthValue, [ValidationService.positiveInteger, Validators.min(1), Validators.max(1000), ValidationService.maxLength(4)]));
          }
        });

        // Lay danh sach chi tiet hop dong
        this.labourContractDetailService.getListLabourContractDetail(e).subscribe(lst => {
          this.labourContractDetailList = lst.data;
          if (lst.data.length !== 0) {
            this.formSaveT.removeControl('labourContractDetailId');
            this.formSaveT.addControl('labourContractDetailId', new FormControl(contracTypeDetailValue, [ValidationService.required]));
            this.isLabourContract = false;
          } else {
            this.formSaveT.removeControl('labourContractDetailId');
            this.isLabourContract = true;
          }
        });
      });
    } else {
      this.isLabourContract = true;
      this.isLabourService = false;
    }
  }


}
