import { CommonUtils } from '@app/shared/services/common-utils.service';
import { RESOURCE } from '@app/core/app-config';
import { PropertyResolver } from '@app/shared/services/property.resolver';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeeInfoIndexComponent } from './page/employee-info-index/employee-info-index.component';

const routes: Routes = [
  {
    path: '',
    component: EmployeeInfoIndexComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.CREATE_EMPLOYEE,
      nationId: CommonUtils.getNationId()
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreateEmployeeRoutingModule { }
