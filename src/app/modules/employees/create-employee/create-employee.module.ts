import { TagInputModule } from 'ngx-chips';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@app/shared';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CreateEmployeeRoutingModule } from './create-employee-routing.module';
import { EmployeeInfoIndexComponent } from './page/employee-info-index/employee-info-index.component';
import { AutoCompleteModule } from 'primeng/autocomplete';

@NgModule({
  declarations: [EmployeeInfoIndexComponent],
  imports: [
    AutoCompleteModule,
    TagInputModule,
    CommonModule,
    SharedModule,
    CreateEmployeeRoutingModule,
  ],
  entryComponents: [EmployeeInfoIndexComponent],
  providers: [
    NgbActiveModal
  ]
})
export class CreateEmployeeModule { }
