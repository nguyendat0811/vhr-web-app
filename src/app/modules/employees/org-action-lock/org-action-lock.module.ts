import { OrgActionLockSearchComponent } from './org-action-lock-search/org-action-lock-search.component';
import { OrgActionLockIndexComponent } from './org-action-lock-index/org-action-lock-index.component';
import { OrgActionLockRoutingModule } from './org-action-lock-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@app/shared';

@NgModule({

  declarations: [OrgActionLockIndexComponent
    , OrgActionLockSearchComponent],

  imports: [
    CommonModule,
    SharedModule,
    OrgActionLockRoutingModule,
  ],
  entryComponents: [OrgActionLockSearchComponent]
})
export class OrgActionLockModule { }
