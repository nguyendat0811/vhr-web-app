import { APP_CONSTANTS } from './../../../../core/app-config';
import { OrgActionLockService } from '../../../../core/services/org-action-lock/org-action-lock.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { RESOURCE, ACTION_FORM } from '@app/core';
import { ActivatedRoute } from '@angular/router';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import * as moment from 'moment';
import { Validators } from '@angular/forms';
import { AppComponent } from '@app/app.component';
import { HelperService } from '@app/shared/services/helper.service';
import { ValidationService } from '@app/shared/services';

@Component({
  selector: 'org-action-lock-search',
  templateUrl: './org-action-lock-search.component.html'
})
export class OrgActionLockSearchComponent extends BaseComponent implements OnInit {
  length;
  lockTypeList: any;
  currentLockType: any;
  moment = moment;
  public commonUtils =  CommonUtils;
  formConfig = {
    organizationId: ['', [ValidationService.required]],
    startDate: ['', [ValidationService.required]],
    endDate: ['', [ValidationService.required]],
    isLock: [''],
    lockType: ['', [ValidationService.required]],
    date: [''],
    description: ['', [ValidationService.maxLength(500)]],
    orgLeavesId: [''],
    lockTypeFromGrid: [''],
  };
  startDate: any;
  endDate: any;
  showList: boolean;
  count: number;
  public listData = [];

  resultListOrgLock: any = {};
  resultListOrgLockByDay: any = {};
  resultListOrgLockByDayNew: any = {};
  public listHeaderDate = [];
  @ViewChild('ptable') dataTable: any;
  constructor( public actr: ActivatedRoute
              , private orgActionLockService: OrgActionLockService
              , private helperService: HelperService
              , private app: AppComponent) {
    super(actr, RESOURCE.ORG_ACTION_LOCK, ACTION_FORM.ORG_ACTION_LOCK_SEARCH);
    this.formSearch = this.buildForm({}, this.formConfig);
    this.setMainService(orgActionLockService);
    this.showList = false;
    this.lockTypeList = APP_CONSTANTS.ORG_ACTION_LOCK_TYPE;
    this.orgActionLockService.getDate().subscribe(res => {
      this.formSearch = this.buildForm(res, this.formConfig, ACTION_FORM.ORG_ACTION_LOCK_SEARCH,
        [ValidationService.notAffter('startDate', 'endDate', 'app.orgActionLock.toDate')]);
    });
  }

  ngOnInit() {
  }

  get f () {
    return this.formSearch.controls;
  }

  /**
   *processSearchOrgActionLock
   */
  public processSearchOrgActionLock(event?): void {
    if (!CommonUtils.isValidForm(this.formSearch)) {
      return;
    }

    this.currentLockType = this.formSearch.controls['lockType'].value;
    const params = this.formSearch ? this.formSearch.value : null;

    this.orgActionLockService.search(params, event).subscribe(res => {
      this.resultList = res;
      this.getMapDateFunction(res.data.length, params);
    });
    if (!event) {
      if (this.dataTable) {
        this.dataTable.first = 0;
      }
    }
  }

  /**
   * getMapDateFunction
   */
  private getMapDateFunction (length?: number, params?: any) {
      this.showList = true;
      this.initListDateSearch(params.startDate, params.endDate);
  }

  /**
   * initListDateSearch
   */
  initListDateSearch(startSearh, endSearh) {
    this.listData = [];
    this.listHeaderDate = [];
    const startDate = moment(startSearh);
    const endDate = moment(endSearh);
    const itemDate = startDate;
    let currentMonth = '';
    while (itemDate.isSameOrBefore(endDate)) {
      const format = itemDate.format('MM/YYYY');
      let listByHeader = [];
      if (currentMonth !== format) {
        currentMonth = format;
        listByHeader.push(itemDate.clone());
        this.listHeaderDate.push(listByHeader);
      } else {
        listByHeader = this.listHeaderDate[this.listHeaderDate.length - 1];
        listByHeader.push(itemDate.clone());
        this.listHeaderDate[this.listHeaderDate.length - 1] = listByHeader;
      }
      this.listData.push(itemDate.clone());
      itemDate.add(1, 'day');
    }
  }

  /**
   * processLockUnLock
   * @ param type
   */
  public processLockUnLock(type?: number, orgLeavesId?: number, lockTypeFromGrid?: number, date?: any): void {
    if (!this.validateBeforeSave()) {
      return;
    }
    if (type === 1) {
      this.comfirm(type, orgLeavesId, lockTypeFromGrid, date, 'app.orgActionLock.comfirmLock');
    } else if (type === 0) {
      this.comfirm(type, orgLeavesId, lockTypeFromGrid, date, 'app.orgActionLock.comfirmUnLock');
    }
  }

  public comfirm(type?: number, orgLeavesId?: number, lockTypeFromGrid?: number, date?: any, message?: string) {
    this.app.confirmMessage(message, () => {
      const formSubmit = this.formSearch.value;
      formSubmit['isLock'] = type;
      formSubmit['orgLeavesId'] = orgLeavesId;
      formSubmit['date'] = date;
      formSubmit['lockTypeFromGrid'] = lockTypeFromGrid;
      this.orgActionLockService.saveOrUpdate(formSubmit)
        .subscribe(res => {
          if (this.orgActionLockService.requestIsSuccess(res)) {
            // setTimeout(() => {
            //   this.formSearch.removeControl('lockType');
            //   this.formSearch.addControl('lockType', CommonUtils.createControl(this.actionForm, 'lockType',
            //     this.currentLockType, [ValidationService.required]));
            //   this.processSearchOrgActionLock();
            // }, 500);
              if (lockTypeFromGrid != null) {
                this.formSearch.removeControl('lockType');
                this.formSearch.addControl('lockType', CommonUtils.createControl(this.actionForm, 'lockType',
                  this.currentLockType, [ValidationService.required]));
              }
              this.processSearchOrgActionLock();
          }
      });
    }, () => {

    });
  }
  /**
   * validateBeforeSave
   */
  private validateBeforeSave(): boolean {
    return CommonUtils.isValidForm(this.formSearch);
  }
}
