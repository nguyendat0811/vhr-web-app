import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { RESOURCE } from '@app/core';

@Component({
  selector: 'org-action-lock-index',
  templateUrl: './org-action-lock-index.component.html'
})
export class OrgActionLockIndexComponent extends BaseComponent implements OnInit {

  constructor(public act: ActivatedRoute) { 
    super(act, RESOURCE.ORG_ACTION_LOCK);
  }

  ngOnInit() {
  }

}
