import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { TreeNode, MenuItem } from 'primeng/api';
import { TranslationService } from 'angular-l10n';
import { SalaryTableService } from '@app/core/services';
import { CommonUtils } from '@app/shared/services';
import { RESOURCE } from '@app/core/app-config';

@Component({
  selector: 'salary-table-tree',
  templateUrl: './salary-table-tree.component.html'
})
export class SalaryTableTreeComponent implements OnInit {
  nodes: TreeNode[];
  selectedNode: TreeNode;
  items: MenuItem[];
  // check vi tri
  indexAdd: any;
  showExpried = false;
  isGetAll = 1;
  isSingleClickNode = true;
  isPlanTypeChangeName = false;
  defaultEditNode: any;
  editableNode: TreeNode;

  permissions = CommonUtils.getPermissionByResourceCode(RESOURCE.SALARY_TABLE);

  @Input()
  public onNodeSelect: Function;
  @Output()
  public treeSelectNode: EventEmitter<TreeNode> = new EventEmitter<TreeNode>();
  @Output()
  public onRemoveNode: EventEmitter<TreeNode> = new EventEmitter<TreeNode>();


  constructor(private service: SalaryTableService
            , private translation: TranslationService) {
  }

 /**
  * action create Context Menu
  */
  nodeContextMenuSelect(event) {
    // Quy che
    const labelAddRegulation = this.translation.translate('MenuContext.addRegulation');
    const labelDeleteRegulation = this.translation.translate('MenuContext.deleteRegulation');
    // Bang luong
    const labelAddLadder = this.translation.translate('MenuContext.addLadder');
    const labelDeleteLadder = this.translation.translate('MenuContext.deleteLadder');
    // Ngach luong
    const labelAddRate = this.translation.translate('MenuContext.addRate');
    const labelDeleteRate = this.translation.translate('MenuContext.deleteRate');

    const menuAddRegulationAfter = { icon: 'fa info fa-plus',
      command: () => this.treeAddSibling(event.node, false), label: labelAddRegulation };
    const menuDeleteRegulation = { icon: 'fa danger fa-trash-alt',
      command: () => this.processRemoveNode(event.node), label: labelDeleteRegulation};
    const menuAddLadder = { icon: 'fa info fa-plus', command: () => this.addChild(event.node), label: labelAddLadder };
    const menuAddLadderAfter = { icon: 'fa info fa-plus',
      command: () => this.treeAddSibling(event.node, false), label: labelAddLadder };
    const menuDeleteLadder = { icon: 'fa danger fa-trash-alt',
      command: () => this.processRemoveNode(event.node), label: labelDeleteLadder};
    const menuAddRate = { icon: 'fa info fa-plus', command: () => this.addChild(event.node), label: labelAddRate };
    const menuAddRateAfter = { icon: 'fa info fa-plus',
        command: () => this.treeAddSibling(event.node, false), label: labelAddRate };
    const menuDeleteRate = { icon: 'fa danger fa-trash-alt',
        command: () => this.processRemoveNode(event.node), label: labelDeleteRate};

    const dataObject = JSON.parse(event.node.data);
    // Menu for quy che luong
    if (dataObject.tableType === 1) {
      this.items = [];
      if (this.hasPermission('action.insert')) {
        this.items.push(menuAddRegulationAfter);
      }
      if (this.hasPermission('action.delete')) {
        this.items.push(menuDeleteRegulation);
      }
      if (event.node.nodeId && this.hasPermission('action.insert')) {
        this.items.splice(1, 0, menuAddLadder);
      }
      return;
    }

    // Menu for bảng lương
    if (dataObject.tableType === 2) {
      this.items = [];
      if (this.hasPermission('action.insert')) {
        this.items.push(menuAddLadderAfter);
      }
      if (this.hasPermission('action.delete')) {
        this.items.push(menuDeleteLadder);
      }
      if (event.node.nodeId && this.hasPermission('action.insert')) {
        this.items.splice(1, 0, menuAddRate);
      }
      return;
    }
    // Menu for nghạch lương
    if (dataObject.tableType === 3) {
      this.items = [];
      if (this.hasPermission('action.insert')) {
        this.items.push(menuAddRateAfter);
      }
      if (this.hasPermission('action.delete')) {
        this.items.push(menuDeleteRate);
      }
      return;
    }
  }

  ngOnInit() {
    this.actionInitAjax();
  }


  /**
  * action init ajax
  */
  public actionInitAjax(resRef?: any, currentNode?: any) {
    this.service.actionInitAjax(CommonUtils.getNationId(), {isGetAll: this.isGetAll})
      .subscribe((res) => {
        this.nodes = CommonUtils.toTreeNode(res);
        // expant node neu co
        if (currentNode && currentNode.data) {
          const tableTypeObj = JSON.parse(currentNode.data);
          if (tableTypeObj.tableType === 2) {
            // TH chon bang luong thi expand quy che luong
            const expandNode = this.nodes.find(x => x['nodeId'] === currentNode.parent.nodeId);
            if (expandNode) {
              expandNode.expanded = true;
            }
          } else if (tableTypeObj.tableType === 3) {
            // TH chon ngach luong thi expand quy che luong, bang luong
            const expandNodeQuyChe = this.nodes.find(x => x['nodeId'] === currentNode.parent.parent.nodeId);
            if (expandNodeQuyChe) {
              expandNodeQuyChe.expanded = true;
              const expandNodeBang = expandNodeQuyChe.children.find(x => x['nodeId'] === currentNode.parent.nodeId);
              if (expandNodeBang) {
                expandNodeBang.expanded = true;
              }
            }
          }
        }
      });
  }

  /**
  * nodeSelect
  * @ param event
  */
  public nodeSelect(event) {
    if (this.hasPermission('action.update')) {
    this.treeSelectNode.emit(event.node);
    }
  }
  /**
   * Xử lý khi xóa node
   * param node
   */
  public processRemoveNode(currentNode) {
    this.onRemoveNode.emit(currentNode);

    const parentNode = currentNode.parent;
    const children = parentNode ? parentNode.children : this.nodes;
    const index = children.indexOf(currentNode);
    if (!currentNode.nodeId) {
      children.splice(index, 1);
    }
    if (children.length === 0 && !currentNode.nodeId) {
      parentNode.expanded = false;
      parentNode.leaf = true;
    }
    return;
  }

  private getDefaultNode (parentNode) {
    let tableType;
    let label;
    if (parentNode) {
      tableType = JSON.parse(parentNode.data).tableType + 1;
      if (tableType === 2) {
        label = this.translation.translate('app.salaryTable.gradeLadder.nameNew');
      } else if (tableType === 3) {
        label = this.translation.translate('app.salaryTable.gradeRate.nameNew');
      }
    } else {
      tableType = 1;
      label = this.translation.translate('app.salaryTable.regulation.nameNew');
    }
    return {
        data: '{"tableType":' + tableType + '}'
      , label: label
      , leaf: true
      , icon: 'glyphicons glyphicons-list-alt'
    };
  }
  /**
   * addchild
   * @ param event
   * @ param type
   * @ param isDefault
   */
  private addChild(parentNode: any) {
    const childNode = this.getDefaultNode(parentNode);
    parentNode.children = parentNode.children || [];
    parentNode.children.push(childNode);
    parentNode.expanded = true;
    parentNode.leaf = false;
    parentNode.collapsedIcon = 'glyphicons glyphicons-folder-closed';
    parentNode.expandedIcon = 'glyphicons glyphicons-folder-open';
  }

  private treeAddSibling(currentNode, isBefore) {
    const parentNode = currentNode.parent;
    const newNode = this.getDefaultNode(parentNode);
    const children = parentNode ? parentNode.children : this.nodes;
    const index = children.indexOf(currentNode);
    if (isBefore) {
      children.splice(index, 0, newNode);
    } else {
      if (index < children.length - 1) {
        children.splice(index + 1, 0, newNode);
      } else {
        children.push(newNode);
      }
    }
  }
  public getNodeData(node) {
    return JSON.parse(node.data);
  }

  private hasPermission(operationKey: string): boolean {
    if (!this.permissions || this.permissions.length <= 0) {
      return false;
    }
    const rsSearch = this.permissions.findIndex(x => x.operationCode === CommonUtils.getPermissionCode(operationKey));
    if (rsSearch < 0) {
      return false;
    }
    return true;
  }

  // onBlurInputTree(node) {
  //   if (node.label === '') { // set ve mac dinh khi khong nhap ten
  //     node.label = this.defaultEditNode;
  //   }
  //   this.editableNode = null;
  // }

  //   /**
  //  * Xu ly  chon node
  //  */
  // treeSelectNode1(node) {
  //   this.isSingleClickNode = true;
  //   setTimeout(() => {
  //     if (this.isSingleClickNode) {
  //       this.editNode(node);
  //     }
  //  }, 250);
  // }

  //   /**
  //  * onEditNode
  //  * @ param currentNode
  //  */
  // private editNode(currentNode) {
  //   this.treeSelectNode.emit(currentNode);
  // }
}
