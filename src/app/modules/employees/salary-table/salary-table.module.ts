import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SalaryTableRoutingModule } from './salary-table-routing.module';
import { IndexComponent } from './index/index.component';
import { SalaryTableTreeComponent } from './salary-table-tree/salary-table-tree.component';
import { SharedModule } from '@app/shared';
import { TreeModule } from 'primeng/tree';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { RegulationFormComponent } from './regulation-form/regulation-form.component';
import { GradeLadderFormComponent } from './grade-ladder-form/grade-ladder-form.component';
import { GradeRateFormComponent } from './grade-rate-form/grade-rate-form.component';
import { ListStepFormComponent } from './grade-rate-form/list-step-form/list-step-form.component';
import { ListGradeFormComponent } from './grade-rate-form/list-grade-form/list-grade-form.component';
import { ListPositionSalaryFormComponent } from './grade-rate-form/list-position-salary-form/list-position-salary-form.component';

@NgModule({
  declarations: [IndexComponent, SalaryTableTreeComponent, RegulationFormComponent,
    GradeLadderFormComponent, GradeRateFormComponent, ListStepFormComponent, ListGradeFormComponent, ListPositionSalaryFormComponent],
  imports: [
    CommonModule,
    SharedModule,
    TreeModule,
    ScrollPanelModule,
    SalaryTableRoutingModule
  ],
  entryComponents: [RegulationFormComponent, GradeLadderFormComponent, GradeRateFormComponent,
    ListStepFormComponent, ListGradeFormComponent]
})
export class SalaryTableModule { }
