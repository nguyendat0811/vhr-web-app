import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormArray, FormGroup, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { RESOURCE, ACTION_FORM } from '@app/core';
import { PositionSalaryService } from '@app/core/services/position-salary/position-salary.service';
import { CommonUtils } from '@app/shared/services';

@Component({
  selector: 'list-position-salary-form',
  templateUrl: './list-position-salary-form.component.html'
})
export class ListPositionSalaryFormComponent extends BaseComponent implements OnInit {
  positionSalaryId = new FormControl();
  listPosition = [];
  salaryTableId: number;

  constructor(public actr: ActivatedRoute
    , private positionSalaryService: PositionSalaryService) {
    super(actr, RESOURCE.SALARY_TABLE, ACTION_FORM.LIST_POSITION_SALARY);
    this.positionSalaryId.valueChanges.subscribe(val => {
      const pos = this.listPosition.filter(pos => pos.positionSalaryId === val);
      if (pos.length === 0) {
        this.positionSalaryService.findOne(val).subscribe(res => {
          if (res.data !== null) {
            this.listPosition.push(res.data);
          }
        });
      }
    });
  }

  ngOnInit() {

  }

  /****************** CAC HAM COMMON DUNG CHUNG ****/

  /**
   * remove
   * param index
   * param item
   */
  public processDelete(id) {
    for (let i = 0; i < this.listPosition.length; i++) {
      if (this.listPosition[i].positionSalaryId === id) {
        this.listPosition.splice(i, 1);
        return;
      }
    }
  }
}
