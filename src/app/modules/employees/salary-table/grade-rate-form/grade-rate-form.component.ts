import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from './../../../../shared/components/base-component/base-component.component';
import { SysCatService } from './../../../../core/services/hr-system/sys-cat.service';
import { APP_CONSTANTS, RESOURCE } from './../../../../core/app-config';
import { FormGroup, Validators, FormArray } from '@angular/forms';
import { Component, OnInit, Input, Output, EventEmitter
        , ViewChild, ViewContainerRef, ComponentFactoryResolver
        , ComponentRef,
        ViewRef
} from '@angular/core';
import { SalaryTableService, SalaryStepService, SalaryGradeService, ACTION_FORM } from '@app/core';
import { AppComponent } from '@app/app.component';
import { CommonUtils, ValidationService } from '@app/shared/services';
import { ListStepFormComponent } from './list-step-form/list-step-form.component';
import { ListGradeFormComponent } from './list-grade-form/list-grade-form.component';

@Component({
  selector: 'grade-rate-form',
  templateUrl: './grade-rate-form.component.html'
})
export class GradeRateFormComponent extends BaseComponent implements OnInit {
  @Input() data: any;
  @Output() close: EventEmitter<any> = new EventEmitter<any>();
  @Output() callBackAffterSave: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild('listChild', { read: ViewContainerRef }) listChild: ViewContainerRef;

  listGradeRateType = [];
  listCurrency = [];
  formSave: FormGroup;

  formConfig = {
    salaryTableId: [null],
    code: [null, [ValidationService.required, ValidationService.maxLength(50)]],
    name: [null, [ValidationService.required, ValidationService.maxLength(500)]],
    effectiveDate: [null, [ValidationService.required]],
    expiredDate: [null, [ValidationService.beforeCurrentDate]],
    description: [null, [ ValidationService.maxLength(1000)]],
    tableType: [null],
    type: [null, [ValidationService.required]],
    currencyId: [null],
    parentId: [null],
    gradeLadderName: [null],
    regulationName: [null],
  };

  componentChildRefTemp: any;
  ladderType: number; // Loai bang luong ( theo buoc hoac theo bac)

  constructor(
      private salaryTableService: SalaryTableService
    , public actr: ActivatedRoute
    , private salaryStepService: SalaryStepService
    , private salaryGradeService: SalaryGradeService
    , private app: AppComponent
    , private _cr: ComponentFactoryResolver
    , private sysCatService: SysCatService
  ) {
    super(actr, RESOURCE.SALARY_TABLE, ACTION_FORM.GRADE_RATE_UPDATE);

    this.buildForm({}, ACTION_FORM.UPDATE);
    this.listGradeRateType = APP_CONSTANTS.LIST_GRADE_RATE_TYPE;
    this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.CURRENCY).subscribe(
      res => this.listCurrency = res.data
    );
  }

  ngOnInit() {
    this.buildFormS(this.data);
    if (this.data.parentId) {
      this.salaryTableService.findOne(this.data.parentId).subscribe(
        res => {
          this.ladderType = res.data.type;
          this.loadListStepOrGrade(this.data.salaryTableId);
          // load danh sach chuc danh huong luong
          // this.loadPositionSalary(this.data.salaryTableId);
      });
    }
  }

  loadListStepForm(data?: any) {
    const componentFactory = this._cr.resolveComponentFactory(ListStepFormComponent);
    this.listChild.clear();
    const componentRef: ComponentRef<ListStepFormComponent> = this.listChild.createComponent(componentFactory);
    this.componentChildRefTemp = componentRef;
    (<ListStepFormComponent>componentRef.instance).data = data || [];
    (<ListStepFormComponent>componentRef.instance).salaryTableEffectiveDate = this.formSave.controls['effectiveDate'].value || '';
    (<ListStepFormComponent>componentRef.instance).salaryTableExpiredDate = this.formSave.controls['expiredDate'].value || '';

    if (!!componentRef.instance.close) {
      // close is eventemitter decorated with @output
      componentRef.instance.close.subscribe(this.close);
    }
  }

  loadListGradeForm(data?: any) {
    const componentFactory = this._cr.resolveComponentFactory(ListGradeFormComponent);
    this.listChild.clear();
    const componentRef: ComponentRef<ListGradeFormComponent> = this.listChild.createComponent(componentFactory);
    this.componentChildRefTemp = componentRef;
    (<ListGradeFormComponent>componentRef.instance).data = data || [];
    (<ListGradeFormComponent>componentRef.instance).salaryTableEffectiveDate = this.formSave.controls['effectiveDate'].value || '';
    (<ListGradeFormComponent>componentRef.instance).salaryTableExpiredDate = this.formSave.controls['expiredDate'].value || '';

    if (!!componentRef.instance.close) {
      // close is eventemitter decorated with @output
      componentRef.instance.close.subscribe(this.close);
    }
  }

  loadListStepOrGrade(salaryTableId: number) {
    if (this.ladderType === 1) {
      if (salaryTableId) {
        this.salaryStepService.getListBySalaryTable(salaryTableId).subscribe(
          resStep => {
            this.loadListStepForm(resStep.data);
          }
        );
      } else {
        this.loadListStepForm();
      }
    } else {
      if (salaryTableId) {
        this.salaryGradeService.getListBySalaryTable(salaryTableId).subscribe(
          resStep => {
            this.loadListGradeForm(resStep.data);
          }
        );
      } else {
        this.loadListGradeForm();
      }
    }
  }

  onChangeEffectiveDate() {
    if (this.formSave.controls['effectiveDate'].value && this.listChild) {
      if (this.ladderType === 1) {
        const data = (<ListStepFormComponent>this.componentChildRefTemp.instance).formSave.value;
        (<ListStepFormComponent>this.componentChildRefTemp.instance)
          .salaryTableEffectiveDate = this.formSave.controls['effectiveDate'].value;
        this.loadListStepForm(data);
      } else {
        const data = (<ListGradeFormComponent>this.componentChildRefTemp.instance).formSave.value;
        (<ListGradeFormComponent>this.componentChildRefTemp.instance)
        .salaryTableEffectiveDate = this.formSave.controls['effectiveDate'].value;
        this.loadListGradeForm(data);
      }
    }
  }

  onChangeExpiredDate() {
    if (this.formSave.controls['expiredDate'].value && this.listChild) {
      if (this.ladderType === 1) {
        const data = (<ListStepFormComponent>this.componentChildRefTemp.instance).formSave.value;
        (<ListStepFormComponent>this.componentChildRefTemp.instance)
          .salaryTableExpiredDate = this.formSave.controls['expiredDate'].value;
          this.loadListStepForm(data);
      } else {
        const data = (<ListGradeFormComponent>this.componentChildRefTemp.instance).formSave.value;
        (<ListGradeFormComponent>this.componentChildRefTemp.instance)
          .salaryTableExpiredDate = this.formSave.controls['expiredDate'].value;
        this.loadListGradeForm(data);
      }
    }
  }

  // loadPositionSalary(salaryTableId: number) {
  //   this.salaryTableService.getPositionSalaryTable(salaryTableId).subscribe(
  //     res => {
  //       this.posSalary.listPosition = res.data;
  //     }
  //   );
  // }

  /**
   * processSaveOrUpdate
   */
  processSaveOrUpdate() {
    let formListStepOrGrade;
    if (this.ladderType === 1) {
      formListStepOrGrade = (<ListStepFormComponent>this.componentChildRefTemp.instance).formSave;
    } else {
      formListStepOrGrade = (<ListGradeFormComponent>this.componentChildRefTemp.instance).formSave;
    }
    const validFormSave = CommonUtils.isValidForm(this.formSave);
    const validFormListStepOrGrade = CommonUtils.isValidForm(formListStepOrGrade);
    if (!validFormSave || !validFormListStepOrGrade) {
      return;
    } else {
      const submitForm = {};
      submitForm['salaryTableForm'] = this.formSave.value;
      if (this.ladderType === 1) {
        submitForm['listSalaryStep'] = formListStepOrGrade.value;
      } else {
        submitForm['listSalaryGrade'] = formListStepOrGrade.value;
      }
      submitForm['listPositionSalary'] = [];
      // this.posSalary.listPosition.forEach(pos => {
      //   submitForm['listPositionSalary'].push(pos.positionSalaryId);
      // });
      this.app.confirmMessage(null, () => {// on rejected
        this.salaryTableService.actionSaveGradeRate(submitForm)
        .subscribe(res => {
          res.data.gradeLadderName = this.data.gradeLadderName;
          res.data.regulationName = this.data.regulationName;
          this.data = res.data;
          this.buildFormS(this.data);
          this.loadListStepOrGrade(res.data.salaryTableId);
          // this.loadPositionSalary(res.data.salaryTableId);
          this.callBackAffterSave.emit(res);
        });
      }, () => {});
    }
  }

  actionCancel() {
    this.buildFormS(this.data);
    if (this.ladderType === 1) {
      (<ListStepFormComponent>this.componentChildRefTemp.instance).ngOnInit();
    } else {
      (<ListGradeFormComponent>this.componentChildRefTemp.instance).ngOnInit();
    }
  }
  /****************** CAC HAM COMMON DUNG CHUNG ****/
  get f () {
    return this.formSave.controls;
  }

  /**
  * buildForm
  */
  private buildFormS(data?: any, actionForm?: ACTION_FORM): void {
    this.formSave = this.buildForm(data, this.formConfig
      , actionForm
      , [ ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.salaryTable.expiredDate')]);
    this.formSave.get('regulationName').disable();
    this.formSave.get('gradeLadderName').disable();

    this.formSave.get('effectiveDate').valueChanges.subscribe(val => {
      this.onChangeEffectiveDate();
    });

    this.formSave.get('expiredDate').valueChanges.subscribe(val => {
      this.onChangeExpiredDate();
    });
  }
}
