import { ACTION_FORM } from './../../../../../core/app-config';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from './../../../../../shared/components/base-component/base-component.component';
import { Validators, FormGroup, FormArray } from '@angular/forms';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ValidationService, CommonUtils } from '@app/shared/services';
import { RESOURCE, SalaryStepService, SalaryTableService } from '@app/core';

@Component({
  selector: 'list-step-form',
  templateUrl: './list-step-form.component.html'
})
export class ListStepFormComponent extends BaseComponent implements OnInit {
  @Input() data: any;
  @Input() salaryTableEffectiveDate: any;
  @Input() salaryTableExpiredDate: any;
  @Output() close: EventEmitter<any> = new EventEmitter<any>();

  formSave: FormArray;
  constructor(public actr: ActivatedRoute
    , private salaryTableService: SalaryTableService
    , private salaryStepService: SalaryStepService) {
    super(actr, RESOURCE.SALARY_TABLE, ACTION_FORM.LIST_STEP_UPDATE);
    this.buildFormListStep();

   }

  ngOnInit() {
    this.buildFormListStep(this.data);
  }
  /****************** CAC HAM COMMON DUNG CHUNG ****/
  get f () {
    return this.formSave.controls;
  }

  /**
   * addSalaryStep
   * param index
   * param item
   */
  public addSalaryStep(index: number, item: FormGroup) {
    const controls = this.formSave as FormArray;
    controls.insert(index + 1, this.makeDefaultSalaryStepForm());
  }
  /**
   * removeSalaryStep
   * param index
   * param item
   */
  public removeSalaryStep(index: number, item: any) {
    if (item && item > 0) {
      this.salaryStepService.confirmDelete({
        messageCode: null,
        accept: () => {
          this.salaryStepService.deleteById(item)
          .subscribe(res => {
            if (this.salaryStepService.requestIsSuccess(res)) {
              this.salaryStepService.getListBySalaryTable(this.data[0].salaryTableId)
              .subscribe(result => {
                this.buildFormListStep(result.data);
              });
            }
          });
        }
      });
    } else {
    const controls = this.formSave as FormArray;
    if (controls.length === 1) {
      this.formSave.reset();
      const group = this.makeDefaultSalaryStepForm();
      const control = new FormArray([]);
      control.push(group);
      this.formSave = control;
      return;
    }
    controls.removeAt(index);
    }
  }

  /**
  * buildForm
  */
  private buildFormListStep(lstSalaryStep?: any): void {
    const controls = new FormArray([]);
    if (!lstSalaryStep) {
      this.formSave = new FormArray([]);
      const group = this.makeDefaultSalaryStepForm();
      controls.push(group);
    } else {
      if (lstSalaryStep.length === 0) {
        const group = this.makeDefaultSalaryStepForm();
        controls.push(group);
      }
      for (const i in lstSalaryStep) {
        const salaryStep = lstSalaryStep[i];
        const group = this.makeDefaultSalaryStepForm();
        group.patchValue(salaryStep);
        controls.push(group);
      }
    }
    controls.setValidators(ValidationService.duplicateArray(
      ['name'], 'name', 'app.salaryStep.nameExits'));
    this.formSave = controls;
  }

  private makeDefaultSalaryStepForm(): FormGroup {
    const group = {
      salaryStepId: [ null],
      salaryTableId: [ null],
      name: [ null, [ ValidationService.required, ValidationService.maxLength(100)]],
      factor: [ null, [ ValidationService.positiveNumber, Validators.max(999.99), Validators.min(0.01)]],
      money: [ null, [ ValidationService.positiveNumber, Validators.max(1000000000 ), Validators.min(0)]],
      duration: [ null, [ValidationService.required, ValidationService.positiveInteger, Validators.max(1000)]],
      effectiveDate: [ null, [ValidationService.required]],
      expiredDate: [null, [ValidationService.beforeCurrentDate]],
      description: [ null, [ ValidationService.maxLength(1000)]],
    };
    return this.buildForm({}, group, ACTION_FORM.LIST_STEP_UPDATE,
    [
      ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.salaryTable.steps.expiredDate'),
      ValidationService.beforeSpecifyDate('effectiveDate', this.salaryTableEffectiveDate, 'app.empSalaryProcess.salaryTableEffectiveDate'),
      ValidationService.afterSpecifyDate('effectiveDate', this.salaryTableExpiredDate, 'app.empSalaryProcess.salaryTableExpiredDate'),
      ValidationService.afterSpecifyDate('expiredDate', this.salaryTableExpiredDate, 'app.empSalaryProcess.salaryTableExpiredDate'),
      ValidationService.justRequiredOneField(['factor', 'money'])
    ]);
  }
}
