import { ACTION_FORM } from './../../../../../core/app-config';
import { BaseComponent } from './../../../../../shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, Validators, FormArray } from '@angular/forms';
import { CommonUtils, ValidationService } from '@app/shared/services';
import { RESOURCE, SalaryGradeService } from '@app/core';

@Component({
  selector: 'list-grade-form',
  templateUrl: './list-grade-form.component.html'
})
export class ListGradeFormComponent extends BaseComponent implements OnInit {
  @Input() data: any;
  @Input() salaryTableEffectiveDate: any;
  @Input() salaryTableExpiredDate: any;
  @Output() close: EventEmitter<any> = new EventEmitter<any>();

  formSave: FormArray;
  constructor(public actr: ActivatedRoute
    , private salaryGradeService: SalaryGradeService) {
    super(actr, RESOURCE.SALARY_TABLE, ACTION_FORM.LIST_GRADE_UPDATE);
    this.buildFormListGrade();
   }

  ngOnInit() {
    this.buildFormListGrade(this.data);
  }
  /****************** CAC HAM COMMON DUNG CHUNG ****/
  get f () {
    return this.formSave.controls;
  }

   /**
   * addSalaryGrade
   * param index
   * param item
   */
  public addSalaryGrade(index: number, item: FormGroup) {
    const controls = this.formSave as FormArray;
    controls.insert(index + 1, this.makeDefaultSalaryGradeForm());
  }

  /**
   * removeSalaryGrade
   * param index
   * param item
   */
  public removeSalaryGrade(index: number, item: any) {
    if (item && item > 0) {
      this.salaryGradeService.confirmDelete({
        messageCode: null,
        accept: () => {
          this.salaryGradeService.deleteById(item)
          .subscribe(res => {
            if (this.salaryGradeService.requestIsSuccess(res)) {
              this.salaryGradeService.getListBySalaryTable(this.data[0].salaryTableId)
              .subscribe(result => {
                this.buildFormListGrade(result.data);
              });
            }
          });
        }
      });
    } else {
    const controls = this.formSave as FormArray;
    if (controls.length === 1) {
      this.formSave.reset();
      const group = this.makeDefaultSalaryGradeForm();
      const control = new FormArray([]);
      control.push(group);
      this.formSave = control;
      return;
    }
    controls.removeAt(index);
  }
}

  /**
  * buildForm
  */
  private buildFormListGrade(lstSalaryGrade?: any): void {
    const controls = new FormArray([]);
    if (!lstSalaryGrade) {
      this.formSave = new FormArray([]);
      const group = this.makeDefaultSalaryGradeForm();
      controls.push(group);
    } else {
      if (lstSalaryGrade.length === 0) {
        const group = this.makeDefaultSalaryGradeForm();
        controls.push(group);
      }
      for (const i in lstSalaryGrade) {
        const salaryGrade = lstSalaryGrade[i];
        const group = this.makeDefaultSalaryGradeForm(salaryGrade);
        group.patchValue(salaryGrade);
        controls.push(group);
      }
    }
    controls.setValidators(ValidationService.duplicateArray(
      ['name'], 'name', 'app.salaryGrade.nameExits'));
    this.formSave = controls;
  }

  private makeDefaultSalaryGradeForm(data?: any): FormGroup {
    if (!data) {
      data = {};
    }
    const group = {
      salaryGradeId: [ null],
      salaryTableId: [ null],
      name: [ null, [ ValidationService.required, ValidationService.maxLength(100)]],
      totalSalaryMin: [ null, [ ValidationService.required, ValidationService.positiveNumber, Validators.max(1000000000), Validators.min(0)]],
      totalSalaryMedian: [ null, [ ValidationService.required, ValidationService.positiveNumber, Validators.max(1000000000), Validators.min(0)]],
      totalSalaryMax: [ null, [ ValidationService.required, ValidationService.positiveNumber, Validators.max(1000000000), Validators.min(0)]],
      baseSalaryMin: [ null, [ ValidationService.required, ValidationService.positiveNumber, Validators.max(1000000000), Validators.min(0)]],
      baseSalaryMedian: [ null, [ ValidationService.required, ValidationService.positiveNumber, Validators.max(1000000000), Validators.min(0)]],
      baseSalaryMax: [ null, [ ValidationService.required, ValidationService.positiveNumber, Validators.max(1000000000), Validators.min(0)]],
      bonus13thMonthMin: [ null, [ ValidationService.required, ValidationService.positiveNumber, Validators.max(1000000000), Validators.min(0)]],
      // tslint:disable-next-line:max-line-length
      bonus13thMonthMedian: [ null, [ ValidationService.required, ValidationService.positiveNumber, Validators.max(1000000000), Validators.min(0)]],
      bonus13thMonthMax: [ null, [ ValidationService.required, ValidationService.positiveNumber, Validators.max(1000000000), Validators.min(0)]],
      bonusYearMin: [ null, [ ValidationService.required, ValidationService.positiveNumber, Validators.max(1000000000), Validators.min(0)]],
      bonusYearMedian: [ null, [ ValidationService.required, ValidationService.positiveNumber, Validators.max(1000000000), Validators.min(0)]],
      bonusYearMax: [ null, [ ValidationService.required, ValidationService.positiveNumber, Validators.max(1000000000), Validators.min(0)]],
      duration: [ null, [ValidationService.required, ValidationService.positiveInteger, Validators.max(1000)]],
      effectiveDate: [ null, [ValidationService.required]],
      expiredDate: [null, [ValidationService.beforeCurrentDate]],
      description: [ null, [ ValidationService.maxLength(1000)]],
    };
    return this.buildForm(data, group, ACTION_FORM.LIST_GRADE_UPDATE,
    [
      ValidationService.notBeforeNumber('expiredDate', 'effectiveDate', 'app.salaryTable.grade.effectiveDate'),
      ValidationService.notBeforeNumber('totalSalaryMedian', 'totalSalaryMin', 'app.salaryTable.grade.minLower'),
      ValidationService.notBeforeNumber('totalSalaryMax', 'totalSalaryMedian', 'app.salaryTable.grade.medianLower'),
      ValidationService.notBeforeNumber('baseSalaryMedian', 'baseSalaryMin', 'app.salaryTable.grade.minLower'),
      ValidationService.notBeforeNumber('baseSalaryMax', 'baseSalaryMedian', 'app.salaryTable.grade.medianLower'),
      ValidationService.notBeforeNumber('bonus13thMonthMedian', 'bonus13thMonthMin', 'app.salaryTable.grade.minLower'),
      ValidationService.notBeforeNumber('bonus13thMonthMax', 'bonus13thMonthMedian', 'app.salaryTable.grade.medianLower'),
      ValidationService.notBeforeNumber('bonusYearMedian', 'bonusYearMin', 'app.salaryTable.grade.minLower'),
      ValidationService.notBeforeNumber('bonusYearMax', 'bonusYearMedian', 'app.salaryTable.grade.medianLower'),
      ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.salaryTable.steps.expiredDate'),
      ValidationService.beforeSpecifyDate('effectiveDate', this.salaryTableEffectiveDate, 'app.empSalaryProcess.salaryTableEffectiveDate'),
      ValidationService.afterSpecifyDate('effectiveDate', this.salaryTableExpiredDate, 'app.empSalaryProcess.salaryTableExpiredDate'),
      ValidationService.afterSpecifyDate('expiredDate', this.salaryTableExpiredDate, 'app.empSalaryProcess.salaryTableExpiredDate'),
    ]);
  }
}
