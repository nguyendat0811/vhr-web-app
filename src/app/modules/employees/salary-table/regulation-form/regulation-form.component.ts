import { RESOURCE } from './../../../../core/app-config';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { FileControl } from '@app/core/models/file.control';
import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { SalaryTableService, NationService, ACTION_FORM } from '@app/core';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { AppComponent } from '@app/app.component';
import { ValidationService } from '@app/shared/services/validation.service';
import { HrStorage } from '@app/core/services/HrStorage';

@Component({
  selector: 'regulation-form',
  templateUrl: './regulation-form.component.html'
})
export class RegulationFormComponent extends BaseComponent implements OnInit, AfterViewInit {
  @Input() data = {};
  @Output() close: EventEmitter<any> = new EventEmitter<any>();
  @Output() callBackAffterSave: EventEmitter<any> = new EventEmitter<any>();

  formSave: FormGroup;
  listMarketCompany = [];

  formConfig = {
    salaryTableId: [null],
    name: [null, [ ValidationService.required, ValidationService.maxLength(500)]],
    marketCompanyId: [null],
    tableType: [1],
    effectiveDate: [null, [ValidationService.required]],
    expiredDate: [null, [ValidationService.beforeCurrentDate]],
    description: [null, [ValidationService.maxLength(1000)]],
    nationId: [CommonUtils.getNationId(), [ValidationService.required]],
  };

  constructor(
    public actr: ActivatedRoute
    , private salaryTableService: SalaryTableService
    , private app: AppComponent
    , private nationService: NationService
  ) {
    super(actr, RESOURCE.SALARY_TABLE, ACTION_FORM.UPDATE);

    this.buildFormS({}, ACTION_FORM.UPDATE);

    this.listMarketCompany = HrStorage.getListMarket();
  }

  ngOnInit() {
    this.buildFormS(this.data, ACTION_FORM.UPDATE);
  }

  ngAfterViewInit() {
  }

  /**
   * processSaveOrUpdate
   */
  processSaveOrUpdate() {
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    } else {
      this.app.confirmMessage(null, () => {// on rejected
        this.salaryTableService.saveOrUpdateFormFile(this.formSave.value)
        .subscribe(res => {
          this.buildFormS(res.data);
          this.callBackAffterSave.emit(res);
        });
      }, () => {// on rejected

      });
    }
  }

  actionCancel() {
    // this.buildForm({});
    this.formSave = this.buildForm({}, this.formConfig);
  }

  /****************** CAC HAM COMMON DUNG CHUNG ****/
  get f () {
    return this.formSave.controls;
  }

  /**
  * buildForm
  */
  private buildFormS(data?: any, actionForm?: ACTION_FORM) {
    this.formSave = this.buildForm(data, this.formConfig
      , actionForm
      , [ ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.salaryTable.expiredDate')]);
    this.formSave.addControl('fileAttachment', new FileControl(null));
    if (data.fileAttachment) {
      (this.formSave.controls['fileAttachment'] as FileControl).setFileAttachment(data.fileAttachment.file);
    }
  }

  closeComponent = (): void => {
    this.close.emit('Close');
  }
}
