import { RESOURCE } from '@app/core/app-config';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { APP_CONSTANTS, ACTION_FORM } from './../../../../core/app-config';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { SalaryTableService } from '@app/core';
import { AppComponent } from '@app/app.component';
import { CommonUtils, ValidationService } from '@app/shared/services';

@Component({
  selector: 'grade-ladder-form',
  templateUrl: './grade-ladder-form.component.html'
})
export class GradeLadderFormComponent extends BaseComponent implements OnInit {
  @Input() data: any;
  @Output() close: EventEmitter<any> = new EventEmitter<any>();
  @Output() callBackAffterSave: EventEmitter<any> = new EventEmitter<any>();

  listSalaryTableForm = [];
  listSalaryTablType = [];
  formSave: FormGroup;
  disabledType = false;
  formConfig = {
    salaryTableId: [null],
    code: [null, [ ValidationService.required, ValidationService.maxLength(50)]],
    name: [null, [ ValidationService.required, ValidationService.maxLength(500)]],
    effectiveDate: [null, [ValidationService.required]],
    expiredDate: [null, [ValidationService.beforeCurrentDate]],
    description: [null, [ ValidationService.maxLength(1000)]],
    tableType: [null, []],
    type: [null, [ValidationService.required]],
    form: [null, [ValidationService.required]],
    parentId: [null],
    regulationName: [null],
  };
  constructor(
    public actr: ActivatedRoute
    , private salaryTableService: SalaryTableService
    , private app: AppComponent
  ) {
    super(actr, RESOURCE.SALARY_TABLE, ACTION_FORM.GRADE_LADDER_UPDATE);

    this.buildForms({}, ACTION_FORM.GRADE_LADDER_UPDATE);
  }

  ngOnInit() {
    this.listSalaryTableForm = APP_CONSTANTS.LIST_GRADE_FORM;
    this.listSalaryTablType = APP_CONSTANTS.LIST_GRADE_TYPE;
    this.buildForms(this.data);
  }

  /**
   * processSaveOrUpdate
   */
  processSaveOrUpdate() {
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    } else {
      this.app.confirmMessage(null, () => {// on rejected
        this.salaryTableService.saveOrUpdateFormFile(this.formSave.value)
        .subscribe(res => {
          res.data.regulationName = this.data.regulationName;
          this.buildForms(res.data);
          this.callBackAffterSave.emit(res);
        });
      }, () => {// on rejected

      });
    }
  }

  actionCancel() {
    this.buildForms({});
  }
  /****************** CAC HAM COMMON DUNG CHUNG ****/
  get f () {
    return this.formSave.controls;
  }

  /**
  * buildForm
  */
  private buildForms(data?: any, actionForm?: ACTION_FORM): void {

    this.formSave = this.buildForm(data, this.formConfig
      , actionForm
      , [ ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.salaryTable.expiredDate')]);
    this.formSave.get('regulationName').disable();

    // Xu ly thay doi hinh thuc bang luong
    if (this.formSave.get('form').value) {
      this.actionFormFieldChange(this.formSave.get('form').value);
    }
    this.formSave.get('form').valueChanges.subscribe(
      (value) => {
        this.actionFormFieldChange(value);
      }
    );
  }

  private actionFormFieldChange(value) {
    const typeDefault = this.formSave.get('type').value;
    this.formSave.removeControl('type');
    // if (value === 1) { // luong chuc danh
    //   this.disabledType = false;
    //   this.formSave.addControl('type', CommonUtils.createControl(this.actionForm, 'type', typeDefault, [ValidationService.required]));
    // }
     if (value === 2) { // luong bao hiem
      this.disabledType = true;
      // chi cho nhap type = 1 ( theo buoc)
      this.formSave.addControl('type', CommonUtils.createControl(this.actionForm, 'type', 1));
    } else {
      this.disabledType = false;
      this.formSave.addControl('type', CommonUtils.createControl(this.actionForm, 'type', typeDefault, [ValidationService.required]));
    }
  }
}
