import { TreeNode } from 'primeng/api';
import { AppComponent } from './../../../../app.component';
import { Component, OnInit, ViewChild, ViewContainerRef, ComponentFactoryResolver, OnDestroy, ComponentRef, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { RegulationFormComponent } from '../regulation-form/regulation-form.component';
import { GradeLadderFormComponent } from '../grade-ladder-form/grade-ladder-form.component';
import { GradeRateFormComponent } from '../grade-rate-form/grade-rate-form.component';
import { SalaryTableService } from '@app/core/services';
import { SalaryTableTreeComponent } from '../salary-table-tree/salary-table-tree.component';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { ACTION_FORM, RESOURCE } from '@app/core';

@Component({
  selector: 'index',
  templateUrl: './index.component.html'
})
export class IndexComponent extends BaseComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('childContainer', { read: ViewContainerRef }) childContainer: ViewContainerRef;

  @ViewChild('salaryTree') salaryTree: SalaryTableTreeComponent;

  private nodeCurrent: TreeNode;

  constructor(private _cr: ComponentFactoryResolver
    , private salaryTableService: SalaryTableService
    , private app: AppComponent
    , private ref: ChangeDetectorRef
    , public actr: ActivatedRoute
    ) { 
      super(actr, RESOURCE.SALARY_TABLE, ACTION_FORM.SEARCH);
    }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.loadRegulation();
    this.ref.detectChanges();
  }

  /**
   * Load form tieu chi luong
   */
  loadRegulation = (data?: any): void => {
    const componentFactory = this._cr.resolveComponentFactory(RegulationFormComponent);
    if (this.childContainer) {
      this.childContainer.clear();
    }
    const componentRef: ComponentRef<RegulationFormComponent> = this.childContainer.createComponent(componentFactory);
    (<RegulationFormComponent>componentRef.instance).data = data || {};
    if (!!componentRef.instance.close) {
      componentRef.instance.close.subscribe(this.close);
    }
    if (!!componentRef.instance.callBackAffterSave) {
      componentRef.instance.callBackAffterSave.subscribe(
        res => {
          this.salaryTree.actionInitAjax(res);
        }
      );
    }
  }

  /**
   * Load form bang luong
   */
  loadGradeLadder = (data?: any): void => {
    const componentFactory = this._cr.resolveComponentFactory(GradeLadderFormComponent);
    this.childContainer.clear();
    const componentRef: ComponentRef<GradeLadderFormComponent> = this.childContainer.createComponent(componentFactory);
    (<GradeLadderFormComponent>componentRef.instance).data = data || {};
    if (!!componentRef.instance.close) {
      componentRef.instance.close.subscribe(this.close);
    }
    if (!!componentRef.instance.callBackAffterSave) {
      componentRef.instance.callBackAffterSave.subscribe(
        res => {
          this.salaryTree.actionInitAjax(res, this.nodeCurrent);
        }
      );
    }
  }

  /**
   * Load form ngach luong
   */
  loadGradeRate = (data?: any): void => {
    const componentFactory = this._cr.resolveComponentFactory(GradeRateFormComponent);
    this.childContainer.clear();
    const componentRef: ComponentRef<GradeRateFormComponent> = this.childContainer.createComponent(componentFactory);
    (<GradeRateFormComponent>componentRef.instance).data = data || {};
    if (!!componentRef.instance.close) {
      componentRef.instance.close.subscribe(this.close);
    }
    if (!!componentRef.instance.callBackAffterSave) {
      componentRef.instance.callBackAffterSave.subscribe(
        res => {
          this.salaryTree.actionInitAjax(res, this.nodeCurrent);
        }
      );
    }
  }

  close = (): void => {
    // do cleanup stuff..
    this.childContainer.clear();
  }

  treeSelectNode(node) {
    this.nodeCurrent = node;
    if (!node.nodeId) {
      this.loadDefaultChildComponent(node);
      return;
    }
    this.salaryTableService.findOne(node.nodeId).subscribe(
      res => {
        const objectData = res.data;
        if (res.data.tableType === 1) {
          this.loadRegulation(objectData);
        } else if (res.data.tableType === 2) {
          objectData.regulationName = node.parent.label;
          this.loadGradeLadder(objectData);
        } else {
          objectData.regulationName = node.parent.parent.label;
          objectData.gradeLadderName = node.parent.label;
          this.loadGradeRate(objectData);
        }
      }
    );
  }

  onRemoveNode(node) {
    if (node.nodeId && node.nodeId > 0) {
      if (node.referenceNum && node.referenceNum > 0) {
        this.salaryTableService.processReturnMessage({type: 'WARNING', code: 'record.dontAlowDelete'});
        return;
      }
      this.app.confirmDelete(null, () => {// on accepted
        this.salaryTableService.deleteById(node.nodeId)
        .subscribe(res => {
          if (this.salaryTableService.requestIsSuccess(res)) {
            this.salaryTree.actionInitAjax();
            node.nodeId = null;
            node.label = '';
            this.loadDefaultChildComponent(node);
          }
        });
      }, null);
    } else {
      node.nodeId = null;
      node.label = '';
      this.loadDefaultChildComponent(node);
    }
    return;
  }

  private loadDefaultChildComponent(node) {
    const objectData = {};
    objectData['name'] = node.label;
    if (JSON.parse(node.data).tableType === 1) {
      objectData['tableType'] = 1;
      this.loadRegulation(objectData);
    } else if (JSON.parse(node.data).tableType === 2) {
      objectData['regulationName'] = node.parent.label;
      objectData['parentId'] = node.parent.nodeId;
      objectData['tableType'] = 2;
      this.loadGradeLadder(objectData);
    } else {
      objectData['regulationName'] = node.parent.parent.label;
      objectData['gradeLadderName'] = node.parent.label;
      objectData['parentId'] = node.parent.nodeId;
      objectData['tableType'] = 3;
      this.loadGradeRate(objectData);
    }
  }
  ngOnDestroy() {
  }
}
