import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DEFAULT_MODAL_OPTIONS, RESOURCE, ACTION_FORM,EmpTypeService } from '@app/core';
import { AppComponent } from '@app/app.component';
import { EmpCodeConfigService } from '@app/core/services/emp-code-config/emp-code-config.service';
import { EmpCodeConfigFormComponent } from '../emp-code-config-form/emp-code-config-form.component';
import { HrStorage } from '@app/core/services/HrStorage';
import { ValidationService } from '@app/shared/services';

@Component({
  selector: 'emp-code-config-search',
  templateUrl: './emp-code-config-search.component.html'
})
export class EmpCodeConfigSearchComponent extends BaseComponent  implements OnInit {
  listEmpType : [];
  formConfig = {
    empCodeConfigId: [''],
    marketCompanyId: [''],
    empTypeList: [''],
    prefixCode: ['',[ValidationService.maxLength(50)]]
  };
  listMarketCompany : [];
  listAllEmpType : [];
  public searchIndex : any;
  constructor(public actr: ActivatedRoute,
              private modalService: NgbModal,
              private empCodeConfigService: EmpCodeConfigService,
              private empTypeService: EmpTypeService,
              private app: AppComponent) {
    super(actr, RESOURCE.EMP_CODE_CONFIG, ACTION_FORM.SEARCH);
    this.setMainService(empCodeConfigService);
    this.formSearch = this.buildForm({}, this.formConfig);
    this.empTypeService.findAll().subscribe(res => {
      this.listEmpType = res.data;
      this.listAllEmpType = res.data;
    });
    this.empCodeConfigService.findAllMarket().subscribe(res => {
      this.listMarketCompany = res.data;
    });
    
  }

  findEmpTypeByMarket(){
    if(this.formSearch.controls.marketCompanyId  
        && this.formSearch.controls.marketCompanyId.value ){
          this.empTypeService.findEmpTypeByMarket(this.formSearch.controls.marketCompanyId.value ).subscribe(res => {
            this.listEmpType = res.data;
          });
    } else {
      this.listEmpType =  this.listAllEmpType ;
    }
   
  }

  ngOnInit() {
    this.searchIndex = 0;
    this.processSearch(null);
  }

  public processSearch(data?:any){
    this.processSearchData(data);
  }
  public processSearchData(event?: any) {
    
    // if (!this.formSearch.controls.tenderId.value) {
    //   this.formSearch.controls.tenderId.setValue('');
    // }
   

    this.empCodeConfigService.searchData(this.formSearch.value, event).subscribe(res => {
      this.resultList = res;
      this.searchIndex = 1;
    });
    if (!event) {
      if (this.dataTable) {
        this.dataTable.first = 0;
      }
    }
  }


  get f () {
    return this.formSearch.controls;
  }

  // Thêm mới hoặc update 
  prepareInsertOrUpdate(item) {
    // Update 
    if (item && item.empCodeConfigId > 0) {
      this.empCodeConfigService.findOne(item.empCodeConfigId)
        .subscribe(res => {
          const modalRef = this.modalService.open(EmpCodeConfigFormComponent, DEFAULT_MODAL_OPTIONS);
          modalRef.componentInstance.setFormValue(this.propertyConfigs, res.data);
          modalRef.result.then((result) => {
            if (!result) {
              return;
            }
            if (this.empCodeConfigService.requestIsSuccess(result)) {
              this.processSearch(null);
            }
          });
        });
    } else {    // Insert 
      const modalRef = this.modalService.open(EmpCodeConfigFormComponent, DEFAULT_MODAL_OPTIONS);
      modalRef.componentInstance.setFormValue(this.propertyConfigs);
      modalRef.result.then((result) => {
        if (!result) {
          return;
        }
        if (this.empCodeConfigService.requestIsSuccess(result)) {
          this.processSearch(null);
        }
      });
    }
  }

  processDelete(item) {
    if (item && item.empCodeConfigId > 0) {
      this.app.confirmDelete(null, () => {
        // on accepted
        this.empCodeConfigService.deleteById(item.empCodeConfigId)
        .subscribe(
          res => {
            if (this.empCodeConfigService.requestIsSuccess(res)) {
              this.processSearch(null);
            }
          },
          error => {
            this.app.requestIsError();
          },
          () => {
            // No errors, route to new page
          }
        );
      }, () => {// on rejected

      });
    }
  }
}
