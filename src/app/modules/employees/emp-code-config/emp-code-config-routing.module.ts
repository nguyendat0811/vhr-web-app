import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmpCodeConfigIndexComponent } from './emp-code-config-index/emp-code-config-index.component';
import { PropertyResolver, CommonUtils } from '@app/shared/services';
import { RESOURCE } from '@app/core';

const routes: Routes = [
  {
    path: '',
    component : EmpCodeConfigIndexComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmpCodeConfigRoutingModule { }
