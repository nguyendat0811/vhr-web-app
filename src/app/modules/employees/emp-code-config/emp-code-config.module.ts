import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmpCodeConfigRoutingModule } from './emp-code-config-routing.module';
import { EmpCodeConfigIndexComponent } from './emp-code-config-index/emp-code-config-index.component';
import { EmpCodeConfigSearchComponent } from './emp-code-config-search/emp-code-config-search.component';
import { EmpCodeConfigFormComponent } from './emp-code-config-form/emp-code-config-form.component';
import { SharedModule } from '@app/shared';
@NgModule({
  declarations: [EmpCodeConfigIndexComponent, EmpCodeConfigSearchComponent, EmpCodeConfigFormComponent],
  imports: [
    SharedModule,
    CommonModule,
    EmpCodeConfigRoutingModule
  ]
  , entryComponents: [EmpCodeConfigFormComponent]
})
export class EmpCodeConfigModule { }
