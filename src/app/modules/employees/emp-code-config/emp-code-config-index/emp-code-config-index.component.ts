import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RESOURCE } from '@app/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';

@Component({
  selector: 'emp-code-config-index',
  templateUrl: './emp-code-config-index.component.html'
})
export class EmpCodeConfigIndexComponent  extends BaseComponent  implements OnInit{
  resultList: any = {};
  constructor(public act: ActivatedRoute) { 
    super(act, RESOURCE.EMP_CODE_CONFIG);
  }


  ngOnInit() {
  }

}
