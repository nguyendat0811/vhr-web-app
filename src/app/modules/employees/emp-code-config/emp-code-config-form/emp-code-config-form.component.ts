import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from '@app/app.component';
import { RESOURCE, ACTION_FORM, EmpTypeService } from '@app/core';
import { EmpCodeConfigService } from '@app/core/services/emp-code-config/emp-code-config.service';
import { CommonUtils, ValidationService } from '@app/shared/services';
import { HrStorage } from '@app/core/services/HrStorage';

@Component({
  selector: 'emp-code-config-form',
  templateUrl: './emp-code-config-form.component.html'
})
export class EmpCodeConfigFormComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  listEmpType: [];
  liveView: any;
  formConfig = {
    empCodeConfigId: [''],
    marketCompanyId: ['', [ValidationService.required]],
    empTypeList: ['', [ValidationService.required]],
    prefixCode: ['', [ValidationService.required, ValidationService.maxLength(50)]]
  };
  listMarketCompany: [];
  constructor(public actr: ActivatedRoute,
              public activeModal: NgbActiveModal,
              private empCodeConfigService: EmpCodeConfigService,
              private empTypeService: EmpTypeService,
              private app: AppComponent) {
    super(actr, RESOURCE.EMP_CODE_CONFIG, ACTION_FORM.INSERT);
    this.formSave = this.buildForm({}, this.formConfig);

    this.empCodeConfigService.findAllMarket().subscribe(res => {
      this.listMarketCompany = res.data;
    });
  }
  ngOnInit() {
  }
  findEmpTypeByMarket(isChange?: boolean) {
    this.empTypeService.findEmpTypeByMarket(this.formSave.controls.marketCompanyId.value ).subscribe(res => {
      this.listEmpType = res.data;
      if (isChange) {
        this.formSave.controls.empTypeList.setValue(null);
      }
    });
  }
  get f () {
    return this.formSave.controls;
  }

  public setFormValue(propertyConfigs: any, data?: any) {
    this.propertyConfigs = propertyConfigs;
    if (data && data.empCodeConfigId > 0) {
      this.formSave = this.buildForm(data, this.formConfig, ACTION_FORM.UPDATE);
      this.findEmpTypeByMarket();
    } else {
      this.formSave = this.buildForm({}, this.formConfig, ACTION_FORM.INSERT);
    }
    this.showLiveView();
  }

  /**
   * validate before save
   */
  private validateBeforeSave(): boolean {
    return CommonUtils.isValidForm(this.formSave);
  }


  /**
   * processSaveOrUpdate
   */
  processSaveOrUpdate() {
    if (!this.validateBeforeSave()) {
      return;
    }
    this.app.confirmMessage(null, () => {// on accepted
      if ( !this.formSave.controls.empTypeList.value) {
        this.formSave.controls.empTypeList.setValue(null);
      }
      this.empCodeConfigService.saveOrUpdate(this.formSave.value)
        .subscribe(res => {
          if (this.empCodeConfigService.requestIsSuccess(res)) {
            this.activeModal.close(res);
          }
        });
    }, () => {
      // on rejected
    });
  }

  showLiveView() {
    this.liveView = this.formSave.controls.prefixCode.value + '000000';
  }
}
