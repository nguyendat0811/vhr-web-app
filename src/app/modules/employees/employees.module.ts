import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@app/shared';

import { EmployeesRoutingModule } from './employees-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SharedModule,
    EmployeesRoutingModule
  ],
  exports: [
  ]
})
export class EmployeesModule { }
