import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@app/shared';

import { UpdateEmployeeIndexComponent } from './page/update-employee-index/update-employee-index.component';
import { UpdateEmployeeRoutingModule } from './update-employee-routing.module';
import { UpdateEmployeeSearchComponent } from './page/update-employee-search/update-employee-search.component';
import { DeleteEmployeeFormComponent } from './page/delete-employee-form/delete-employee-form.component';

@NgModule({
  declarations: [
    UpdateEmployeeIndexComponent,
    UpdateEmployeeSearchComponent,
    DeleteEmployeeFormComponent
  ],
  imports: [
    CommonModule,
    SharedModule,

    UpdateEmployeeRoutingModule,
  ],
  entryComponents: [UpdateEmployeeIndexComponent, DeleteEmployeeFormComponent]
})
export class UpdateEmployeeModule { }
