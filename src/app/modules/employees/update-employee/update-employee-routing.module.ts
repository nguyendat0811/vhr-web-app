import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UpdateEmployeeIndexComponent } from './page/update-employee-index/update-employee-index.component';
import { PropertyResolver } from '@app/shared/services/property.resolver';
import { RESOURCE } from '@app/core';
import { CommonUtils } from '@app/shared/services';

const routes: Routes = [
  {
    path: '',
    component: UpdateEmployeeIndexComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.EMPLOYEE_INFO,
      nationId: CommonUtils.getNationId()
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UpdateEmployeeRoutingModule { }
