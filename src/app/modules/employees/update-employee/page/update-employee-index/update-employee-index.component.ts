import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { RESOURCE } from '@app/core';

@Component({
  selector: 'update-employee-index',
  templateUrl: './update-employee-index.component.html'
})
export class UpdateEmployeeIndexComponent extends BaseComponent implements OnInit {

  constructor(public act: ActivatedRoute) { 
    super(act, RESOURCE.EMPLOYEE);
  }

  ngOnInit() {
  }
}
