import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { EmployeeInfoService, EmpTypeService, SysCatService } from '@app/core';
import { FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit, ViewChild } from '@angular/core';
import { AppComponent } from '@app/app.component';
import { ValidationService } from '@app/shared/services';
import { Router, ActivatedRoute } from '@angular/router';
import { APP_CONSTANTS, ACTION_FORM, RESOURCE, MEDIUM_MODAL_OPTIONS } from '@app/core';
import { DeleteEmployeeFormComponent } from './../delete-employee-form/delete-employee-form.component';

@Component({
  selector: 'update-employee-search',
  templateUrl: './update-employee-search.component.html',
})
export class UpdateEmployeeSearchComponent extends BaseComponent implements OnInit {
  formSearchBasic: FormGroup;
  formSearchAdvanced: FormGroup;
  formSearch: FormGroup;
  laboutTypeList: any;
  empTypeList: any;
  familyTypeList: any;
  languageList: any;
  educationLevelList: any;
  educationTypeList: any;
  resultList: any = {};
  organizationName: [] ;
  isHidden: boolean;
  CONSTANTS = APP_CONSTANTS;
  @ViewChild('ptable') dataTable: any;

  SPECIALITY_CODE = APP_CONSTANTS.SYS_CAT_TYPE_COCE.EDUCATION_SPECIALITY;
  NATION_ID = CommonUtils.getNationId();
  MARKETCOMPANY_ID = CommonUtils.getCurrentMarketCompanyId();
  formConfigBasic = {
    organizationId: [''],
    employeeCode: ['', Validators.compose([
                      ValidationService.maxLength(100)
                      ])],
    fullName: ['', Validators.compose([
                  ValidationService.maxLength(100)
                  ])],
    mobileNumber: ['', Validators.compose([
                    ValidationService.maxLength(100) ])],
    email: ['', Validators.compose([ ValidationService.maxLength(200)
                                      ])],
    pidNumber: ['', Validators.compose([ValidationService.maxLength(50) ])],
    passportNumber: ['', Validators.compose([ValidationService.maxLength(50) ])],
    positionId: [''],
    laboutType: [''],
    labourContractTypeId: [''],
    empTypeId: [''],
    taxNumber: ['', Validators.compose([ ValidationService.maxLength(20) ])],
    dateBirthFrom: [''],
    dateBirthTo: [''],
    gender: [''],
    status: [''],
    isAdvanceSearch: [false]
  };
  formConfigAdvanced = {
    maritalStatus: [''],
    isPartyMember: [''],
    familyTypeId: [''],
    religionId: [''],
    ethnicId: [''],
    empEducationProcessId: [''],
    languageId: [''],
    educationLevelId: [''],
    educationTypeId: [''],
    joinCompanyDateFrom: [''],
    joinCompanyDateTo: [''],
    contractEffectiveDateFrom: [''],
    contractEffectiveDateTo: [''],
    contractExpiredDateFrom: [''],
    contractExpiredDateTo: [''],
  };
  constructor(public actr: ActivatedRoute
            , private app: AppComponent
            , private modalService: NgbModal
            , private employeeInfoService: EmployeeInfoService
            , private empTypeService: EmpTypeService
            , private sysCatService: SysCatService
            , private router: Router
            , private route: ActivatedRoute) {
    super(actr, RESOURCE.EMPLOYEE, ACTION_FORM.SEARCH);
    this.setMainService(employeeInfoService);
    this.buildSearchBasicFormGroup({employeeCode: route.snapshot.paramMap.get('employeeCode')});
    this.buildSearchAdvancedFormGroup({});
    this.onParamsChange();
  }
  ngOnInit() {
    this.processSearch(null);
  }

  private onParamsChange() {
    this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.LANGUAGE).subscribe(res => {
      this.languageList = res.data;
    });
    this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.EDUCATION_LEVEL).subscribe(res => {
      this.educationLevelList = res.data;
    });
    this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.EDUCATION_TYPE).subscribe(res => {
      this.educationTypeList = res.data;
    });
    this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.FAMILY_TYPE).subscribe(res => {
      this.familyTypeList = res.data;
    });
    this.empTypeService.getAllByEmpTypeByIsUsed().subscribe(res => {
      this.empTypeList = res.data;
    });
    this.empTypeService.getAllByEmpTypeByIsUsed().subscribe(res => {
      this.empTypeList = res.data;
    });
    this.isHidden = true;
  }

  private buildSearchBasicFormGroup(data?: any) {
    this.formSearchBasic = this.buildForm(data, this.formConfigBasic
      , ACTION_FORM.SEARCH
      , [ ValidationService.notAffter('dateBirthFrom', 'dateBirthTo', 'app.employee-update.dateBirthTo')]);
  }
  private buildSearchAdvancedFormGroup(data?: any) {
    this.formSearchAdvanced = this.buildForm(data, this.formConfigAdvanced
      , ACTION_FORM.SEARCH
      , [ ValidationService.notAffter('joinCompanyDateFrom', 'joinCompanyDateTo', 'app.employee-update.joinCompanyDateTo')
        , ValidationService.notAffter('contractEffectiveDateFrom', 'contractEffectiveDateTo', 'app.employee-update.contractEffectiveDateTo')
        , ValidationService.notAffter('contractExpiredDateFrom', 'contractExpiredDateTo', 'app.employee-update.contractExpiredDateTo')]);
  }
  /**
   * thuc hien tim kiem
   */
  processSearch(event) {
    this.formSearchBasic.controls['isAdvanceSearch'].setValue(!this.isHidden);
    if (this.isHidden) {
      if (!CommonUtils.isValidForm(this.formSearchBasic)) {
        return;
      } else if (CommonUtils.isValidForm(this.formSearchBasic.value)) {
        this.employeeInfoService.search(this.formSearchBasic.value, event).subscribe(res => {
          this.resultList = res;
          if (!event) {
            this.dataTable.first = 0;
          }
        });
      }
    } else {
      const formSearch = Object.assign({}, this.formSearchBasic.value, this.formSearchAdvanced.value);
      if (!CommonUtils.isValidForm(formSearch)) {
        return;
      } else if (CommonUtils.isValidForm(formSearch)) {
        this.employeeInfoService.search(formSearch, event).subscribe(res => {
          this.resultList = res;
          if (!event) {
            this.dataTable.first = 0;
          }
        });
      }
    }
  }

  get f () {
    return this.formSearchBasic.controls;
  }
  get fa () {
    return this.formSearchAdvanced.controls;
  }

  pressLabelSearchType() {
    if (this.isHidden) {
      this.isHidden = false;
    } else {   // If expired area is shown
      this.clearData();
      this.isHidden = true;
    }
  }
  clearData() {
    this.formSearchAdvanced.reset();
    this.isHidden = true;
  }
   /**
   * prepareUpdate
   * param item
   */
  prepareSaveOrUpdate(item) {
    if (item && item.employeeId > 0) {
      this.router.navigate(['/employees/personal', item.employeeId]);
    }
  }
  /**
   * prepareDelete
   * param item
   */
  processDelete(item) {
    if (item && item.employeeId > 0) {
      this.app.confirmDelete('app.employee-info.confirmDeleteMesage', () => {
        this.employeeInfoService.validateBeforeDelete(item.employeeId)
        .subscribe(res => {
          if (this.employeeInfoService.requestIsConfirm(res)) {
            const showConfirmDeleteEmployee = () => {
              const modalRef = this.modalService.open(DeleteEmployeeFormComponent, MEDIUM_MODAL_OPTIONS);
              modalRef.componentInstance.setFormValue(item);
              modalRef.result.then((result) => {
                if (!result) {
                  return;
                }
                if (this.employeeInfoService.requestIsSuccess(result)) {
                  this.processSearch(null);
                }
              });
            };
            eval(res.message);
          }
        });
      }, () => {
        // on rejected
      });
    }
  }
}
