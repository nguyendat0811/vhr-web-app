import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ACTION_FORM, RESOURCE } from '@app/core';
import { AppComponent } from '@app/app.component';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { EmployeeInfoService } from '@app/core';
import { TranslationService } from 'angular-l10n';
import { FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { ValidationService } from '@app/shared/services';

@Component({
  selector: 'delete-employee-form',
  templateUrl: './delete-employee-form.component.html'
})
export class DeleteEmployeeFormComponent extends BaseComponent implements OnInit {
  employeeId: any;
  employeeCode: any;
  formDeleteEmployee: FormGroup;

  formConfig = {
    employeeCode: ['', ValidationService.required]
  };

  constructor(
    public actr: ActivatedRoute,
    private app: AppComponent,
    public activeModal: NgbActiveModal,
    public translationService: TranslationService,
    public employeeInfoService: EmployeeInfoService
  ) {
    super(actr, RESOURCE.EMPLOYEE, ACTION_FORM.INSERT);
    this.formDeleteEmployee = this.buildForm({}, this.formConfig, ACTION_FORM.INSERT);
  }

  ngOnInit() {
  }

  get f () {
    return this.formDeleteEmployee.controls;
  }

  /**
   * processDelete
   */
  processDelete() {
    if (this.formDeleteEmployee.controls['employeeCode'].value !== this.employeeCode) {
      alert(this.translationService.translate('app.employee-info.invalidEmployeeCode'));
    }
    if (CommonUtils.isValidForm(this.formDeleteEmployee) && this.formDeleteEmployee.controls['employeeCode'].value === this.employeeCode) {
      this.app.confirmDelete('app.employee-info.confirmDeleteMesage', () => {
        this.employeeInfoService.deleteById(this.employeeId)
        .subscribe(res => {
          if (this.employeeInfoService.requestIsSuccess(res)) {
            this.activeModal.close(res);
            this.processSearch();
          }
        });
      }, () => {
        // on rejected
      });
    }
  }

  setFormValue(data: any) {
    this.employeeId = data.employeeId;
    this.employeeCode = data.employeeCode;
  }
}
