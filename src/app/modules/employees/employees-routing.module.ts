import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/dashboard/employee',
    pathMatch: 'full'
  },
  {
    path: 'personal/:id',
    loadChildren : './personal/personal.module#PersonalModule'
  },
  {
    path: 'employee-info',
    loadChildren: './create-employee/create-employee.module#CreateEmployeeModule'
  },
  {
    path: 'update-employee',
    loadChildren: './update-employee/update-employee.module#UpdateEmployeeModule'
  },
  {
    path: 'update-employee/:employeeCode',
    loadChildren: './update-employee/update-employee.module#UpdateEmployeeModule'
  },
  {
    path: 'salary-table',
    loadChildren: './salary-table/salary-table.module#SalaryTableModule'
  },
  {
    path: 'position-category',
    loadChildren: '../positions/position-category/position-category.module#PositionCategoryModule'
  },
  {
    path: 'position-career',
    loadChildren: '../positions/position-career/position-career.module#PositionCareerModule'
  },
  {
    path: 'org-action-lock',
    loadChildren: './org-action-lock/org-action-lock.module#OrgActionLockModule'
  },
  {
    path: 'emp-code-config',
    loadChildren: './emp-code-config/emp-code-config.module#EmpCodeConfigModule'
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeesRoutingModule { }
