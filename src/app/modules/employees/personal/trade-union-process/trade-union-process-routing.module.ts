import { CommonUtils } from '@app/shared/services';
import { RESOURCE } from '@app/core';
import { PropertyResolver } from './../../../../shared/services/property.resolver';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TradeUnionListComponent } from './pages/trade-union-list/trade-union-list.component';

const routes: Routes = [
  {
    path: '',
    component: TradeUnionListComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.TRADE_UNION,
      nationId: CommonUtils.getNationId()
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TradeUnionProcessRoutingModule { }
