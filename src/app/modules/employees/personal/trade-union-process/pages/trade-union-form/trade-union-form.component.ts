import { BaseComponent } from './../../../../../../shared/components/base-component/base-component.component';
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup , Validators } from '@angular/forms';
import { EmpTradeUnionService, APP_CONSTANTS, SysCatService, RESOURCE, ACTION_FORM } from '@app/core';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { AppComponent } from '@app/app.component';
import { ValidationService } from '@app/shared/services/validation.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'trade-union-form',
  templateUrl: './trade-union-form.component.html'
})
export class TradeUnionFormComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  tradeUnionList: any;
  formConfig = {
    empTradeUnionId: [''],
    employeeId: [''],
    tradeUnionId: ['', [ValidationService.required]],
    percentage: ['', [ValidationService.required, Validators.max(100), Validators.min(0.01), ValidationService.positiveNumber]],
    effectiveDate: ['', [ValidationService.required]],
    expiredDate: [''],
  };
  constructor(public actr: ActivatedRoute
            , public activeModal: NgbActiveModal
            , private empTradeUnionService: EmpTradeUnionService
            , private app: AppComponent
            , private sysCatService: SysCatService) {
    super(actr, RESOURCE.TRADE_UNION, ACTION_FORM.INSERT);
    this.formSave = this.buildForm({}, this.formConfig,
      ACTION_FORM.INSERT, [ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.empSalaryProcess.expiredDate')]);
    this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.TRADE_UNION).subscribe(res => {
      this.tradeUnionList = res.data;
    });
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
  }
  /**
   * processSaveOrUpdate
   */
  processSaveOrUpdate() {
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    } else {
      console.log(this.formSave);
      this.app.confirmMessage('app.empTradeUnion.finishPreviousProcess', () => {// on rejected
        this.empTradeUnionService.saveOrUpdate(this.formSave.value)
        .subscribe(res => {
          if (this.empTradeUnionService.requestIsSuccess(res)) {
            this.activeModal.close(res);
          }
        });
      }, () => {// on rejected

      });
    }
  }

  /****************** CAC HAM COMMON DUNG CHUNG ****/
  get f () {
    return this.formSave.controls;
  }

    /**
   * setFormValue
   * param: Array propertyConfigs
   * param data
   */
  public setFormValue(propertyConfigs: any, data?: any) {
    console.log(data);
    this.propertyConfigs = propertyConfigs;
    if (data && data.empTradeUnionId > 0) {
      this.formSave = this.buildForm(data, this.formConfig,
        ACTION_FORM.UPDATE, [ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.empSalaryProcess.expiredDate')] );
    } else {
      this.formSave = this.buildForm(data, this.formConfig,
        ACTION_FORM.INSERT, [ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.empSalaryProcess.expiredDate')] );
    }
  }
}
