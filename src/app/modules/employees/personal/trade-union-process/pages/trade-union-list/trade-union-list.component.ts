import { CommonUtils } from './../../../../../../shared/services/common-utils.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DEFAULT_MODAL_OPTIONS, RESOURCE } from '@app/core/app-config';
import { EmpTradeUnionService, ACTION_FORM } from '@app/core';
import { TradeUnionFormComponent } from '../trade-union-form/trade-union-form.component';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { EmployeeResolver } from '@app/shared/services/employee.resolver';

@Component({
  selector: 'trade-union-list',
  templateUrl: './trade-union-list.component.html'
})
export class TradeUnionListComponent extends BaseComponent implements OnInit {
  employeeId: number;
  @ViewChild('ptable') dataTable: any;

  constructor(private modalService: NgbModal
            , private empTradeUnionService: EmpTradeUnionService
            , private employeeResolver: EmployeeResolver
            , public actr: ActivatedRoute
            ) {
    super(actr, RESOURCE.TRADE_UNION, ACTION_FORM.SEARCH);
      this.setMainService(empTradeUnionService);
      this.employeeResolver.EMPLOYEE.subscribe(
        data => {
          if (data) {
              this.employeeId = data;
              this.formSearch =  this.buildForm({employeeId: this.employeeId}, {employeeId: ['']});
              this.processSearch();
          }
        }
      );

    }

  /**
   * ngOnInit
   */
  ngOnInit() {
  }
  /**
   * prepareUpdate
   * param item
   */
  prepareSaveOrUpdate(item) {
    if (item && item.empTradeUnionId > 0) {
      this.empTradeUnionService.findOne(item.empTradeUnionId)
        .subscribe(res => {
          this.activeModel(res.data);
        });
    } else {
      this.activeModel({employeeId: this.employeeId});
    }
  }

  /**
   * show model
   * data
   */
  private activeModel(data?: any) {
    const modalRef = this.modalService.open(TradeUnionFormComponent, DEFAULT_MODAL_OPTIONS);
    if (data) {
      modalRef.componentInstance.setFormValue(this.propertyConfigs, data);
    }
    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
      if (this.empTradeUnionService.requestIsSuccess(result)) {
        this.processSearch();
      }
    });
  }
}
