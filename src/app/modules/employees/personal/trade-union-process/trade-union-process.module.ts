import { SharedModule } from '@app/shared';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TradeUnionProcessRoutingModule } from './trade-union-process-routing.module';
import { TradeUnionListComponent } from './pages/trade-union-list/trade-union-list.component';
import { TradeUnionFormComponent } from './pages/trade-union-form/trade-union-form.component';

@NgModule({
  declarations: [TradeUnionListComponent, TradeUnionFormComponent],
  imports: [
    CommonModule,
    TradeUnionProcessRoutingModule,
    SharedModule
  ],
  entryComponents: [TradeUnionFormComponent]
})
export class TradeUnionProcessModule { }
