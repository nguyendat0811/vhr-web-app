import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EmpFileFormComponent } from '../emp-file-form/emp-file-form.component';
import { EmpFileService } from '@app/core';
import { DEFAULT_MODAL_OPTIONS, RESOURCE, ACTION_FORM } from '@app/core/app-config';
import { EmployeeResolver } from '@app/shared/services/employee.resolver';
import { FileStorageService } from '@app/core/services/file-storage.service';
import { saveAs } from 'file-saver';
import { CommonUtils } from '@app/shared/services/common-utils.service';

@Component({
  selector: 'emp-file-search',
  templateUrl: './emp-file-search.component.html'
})
export class EmpFileSearchComponent extends BaseComponent implements OnInit {
  resultList: any = {};
  credentials: any = {};
  formSearch: FormGroup;
  employeeId: number;
  empId: {employeeId: any};
  @ViewChild('ptable') dataTable: any;

  constructor( public actr: ActivatedRoute
    , private modalService: NgbModal
    , private empFileService: EmpFileService
    , private employeeResolver: EmployeeResolver
    , private fileStorage: FileStorageService) {
      super(actr, RESOURCE.EMP_FILE, ACTION_FORM.SEARCH);
      this.setMainService(empFileService);

      this.employeeResolver.EMPLOYEE.subscribe(
        data => {
          if (data) {
            this.employeeId = data;
            this.empId = {employeeId: data};
            this.formSearch =  this.buildForm({employeeId: this.employeeId}, {employeeId: ['']});
            this.processSearch();
          }
        }
      );
    }

  ngOnInit() {
  }
  get f() {
    return this.formSearch.controls;
  }

  /**
   * prepareUpdate
   * param item
   */
  prepareSaveOrUpdate(item) {
    if (item && item.empFileId > 0) {
      this.empFileService.findOne(item.empFileId)
        .subscribe(res => {
          this.activeModal(res.data);
        });
    } else {
      this.activeModal(this.empId);
    }
  }

  private activeModal(data?: any) {
    const modalRef = this.modalService.open(EmpFileFormComponent, DEFAULT_MODAL_OPTIONS);
    if (data) {
      modalRef.componentInstance.setFormValue(this.propertyConfigs, data);
    }
    modalRef.result.then((result) => {
      if (!result) {
        this.processSearch();
        return;
      }
      if (this.empFileService.requestIsSuccess(result)) {
        this.processSearch();
      }
    });
  }

  public downloadFile(fileData) {
    this.fileStorage.downloadFile(fileData.id).subscribe(res => {
        saveAs(res , fileData.fileName);
    });
  }
}
