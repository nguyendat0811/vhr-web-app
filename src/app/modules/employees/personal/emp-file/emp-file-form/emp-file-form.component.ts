import { RESOURCE } from '@app/core/app-config';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { EmpFileService, APP_CONSTANTS, ACTION_FORM, SysCatService } from '@app/core';
import { AppComponent } from '@app/app.component';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { FileControl} from '@app/core/models/file.control';
import { ValidationService } from '@app/shared/services';


@Component({
  selector: 'emp-file-form',
  templateUrl: './emp-file-form.component.html',
})
export class EmpFileFormComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  listEmpFileCode = [];
  listStatus = [];
  listFixedEmpFile = [];
  @ViewChild('fileChoser') fileChoser: any;

  formConfig = {
    employeeId: [null],
    empFileId: [null],
    fileId: [null, [ValidationService.required]],
    empFileCode: [null],
    organizationId: [null, [ValidationService.required]],
    fixedEmpFile: [null],
    description: [null, [ValidationService.maxLength(200)]],
    status: [null],
  };
  @Input() item: any;
  constructor(public activeModal: NgbActiveModal
    , private empFileService: EmpFileService
    , private sysCatService: SysCatService
    , private app: AppComponent
    , public actr: ActivatedRoute) {
      super(actr, RESOURCE.EMP_FILE, ACTION_FORM.INSERT);
      this.buildForms({}, ACTION_FORM.INSERT);
      // Lay danh sach loai ho so
      this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.EMP_FILE_LIST).subscribe(
        res => this.listEmpFileCode = res.data
      );
      this.listStatus = APP_CONSTANTS.EMP_FILE_STATUS;
      this.listFixedEmpFile = APP_CONSTANTS.EMP_FILE_FIXEDEMPFILE;
  }

  get f() {
    return this.formSave.controls;
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
  }

  /**
   * processSaveOrUpdate
   */
  processSaveOrUpdate() {
    // If file is empty => reInit file control
    if (!this.fileChoser.files) {
      this.formSave.removeControl('file');
      this.formSave.addControl('file', new FileControl(null, ValidationService.required));
    }
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    } else {
      this.app.confirmMessage(null, () => {// on accepted
        this.empFileService.saveOrUpdateFormFile(this.formSave.value)
          .subscribe(res => {
            if (this.empFileService.requestIsSuccess(res)) {
              this.activeModal.close(res);
            }
          });
      }, () => {// on rejected

      });
    }
  }

  /****************** CAC HAM COMMON DUNG CHUNG ****/
  /**
   * buildForm
   */
  private buildForms(data?: any, actionForm?: ACTION_FORM): void {
    this.formSave = this.buildForm(data, this.formConfig, actionForm);
    this.formSave.addControl('file', new FileControl(null, ValidationService.required));
    if (data.fileAttachment) {
      (this.formSave.controls['file'] as FileControl).setFileAttachment(data.fileAttachment.empFile);
    }
  }

  /**
   * setFormValue
   * param data
   */
  public setFormValue(propertyConfigs: any, data?: any) {
    this.propertyConfigs = propertyConfigs;
    if (data && data.empFileId > 0) {
      this.buildForms(data, ACTION_FORM.UPDATE);
      } else {
      this.buildForms(data, ACTION_FORM.INSERT);
    }
  }
}
