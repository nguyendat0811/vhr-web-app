import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@app/shared';

import { EmpFilePageComponent } from './emp-file-page.component';
import { EmpFileFormComponent } from './emp-file-form/emp-file-form.component';
import { EmpFileSearchComponent } from './emp-file-search/emp-file-search.component';
import { EmpFileRoutingModule } from './emp-file-routing.module';
import { EmployeeResolver } from '@app/shared/services/employee.resolver';

@NgModule({
  declarations: [EmpFilePageComponent, EmpFileFormComponent, EmpFileSearchComponent],
  imports: [
    CommonModule,
    SharedModule,

    EmpFileRoutingModule
  ],
  entryComponents: [EmpFileFormComponent],
  providers: [EmployeeResolver]
})
export class EmpFileModule { }
