import { CommonUtils } from '@app/shared/services/common-utils.service';
import { RESOURCE } from '@app/core/app-config';
import { PropertyResolver } from './../../../../shared/services/property.resolver';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmpFilePageComponent } from './emp-file-page.component';

const routes: Routes = [
  {
    path: '',
    component: EmpFilePageComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.EMP_FILE,
      nationId: CommonUtils.getNationId()
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmpFileRoutingModule { }
