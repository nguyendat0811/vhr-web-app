import { Component, OnInit } from '@angular/core';
import { EmpFileService } from '@app/core/services/emp-file/emp-file.service';
import { ActivatedRoute } from '@angular/router';
import { RESOURCE } from '@app/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';

@Component({
  selector: 'emp-file-page',
  templateUrl: './emp-file-page.component.html',
})
export class EmpFilePageComponent extends BaseComponent implements OnInit {
  resultList: any = {};
  constructor(public act: ActivatedRoute) { 
    super(act, RESOURCE.EMP_FILE);
  }

  ngOnInit() {
  }

}
