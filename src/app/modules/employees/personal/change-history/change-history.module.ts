import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChangeHistoryRoutingModule } from './change-history-routing.module';
import { ChangeHistoryIndexComponent } from './change-history-index/change-history-index.component';
import { ChangeHistorySearchComponent } from './change-history-search/change-history-search.component';
import { SharedModule } from '@app/shared';

@NgModule({
  declarations: [ChangeHistoryIndexComponent, ChangeHistorySearchComponent],
  imports: [
    CommonModule,
    SharedModule,
    ChangeHistoryRoutingModule
  ]
})
export class ChangeHistoryModule { }
