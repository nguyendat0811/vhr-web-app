import { ValidationService } from './../../../../../shared/services/validation.service';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from '@app/app.component';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { Validators, FormGroup } from '@angular/forms';
import { EmployeeResolver } from '@app/shared/services/employee.resolver';
import { InfoChangeService, RESOURCE, ACTION_FORM } from '@app/core';
import { ActivatedRoute } from '@angular/router';
import { FileStorageService } from '@app/core/services/file-storage.service';
import { saveAs } from 'file-saver';

@Component({
  selector: 'change-history-search',
  templateUrl: './change-history-search.component.html',
})
export class ChangeHistorySearchComponent extends BaseComponent implements OnInit {

  employeeId: number;
  formSearchInfoChange: FormGroup;
  formConfig = {
    employeeId: [this.employeeId],
    changeType: [null , [ValidationService.maxLength(1000)]],
    changedFields: [null , [ValidationService.maxLength(50)]],
    changeTimeFrom: [null],
    changeTimeTo: [null],
    createdBy: [null, [ValidationService.maxLength(50)]],
  };
  constructor(public actr: ActivatedRoute
            , private infoChangeService: InfoChangeService
            , public fileStorage: FileStorageService
            , private employeeResolver: EmployeeResolver
            ) {
    super(actr, RESOURCE.CHANGE_HISTORY, ACTION_FORM.SEARCH);
    this.setMainService(infoChangeService);
    this.buildFormSearch({});
    this.employeeResolver.EMPLOYEE.subscribe(
      data => {
        if (data) {
          this.employeeId = data;
          this.buildFormSearch({employeeId: this.employeeId});
          this.processSearch();
        }
      }
    );
  }
  get f () {
    return this.formSearch.controls;
  }

  private buildFormSearch(data) {
    this.formSearch = this.buildForm(data, this.formConfig
      , ACTION_FORM.SEARCH
      , [ ValidationService.notAffter('changeTimeFrom', 'changeTimeTo', 'app.infoChange.changeTimeTo')]);
  }
  /**
   * ngOnInit
   */
  ngOnInit() {
  }

  public downloadFile(fileData) {
    this.fileStorage.downloadFile(fileData.id).subscribe(res => {
        saveAs(res , fileData.fileName);
    });
  }
}
