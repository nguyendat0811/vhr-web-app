import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChangeHistoryIndexComponent } from './change-history-index/change-history-index.component';
import { PropertyResolver } from '@app/shared/services/property.resolver';
import { RESOURCE } from '@app/core';
import { CommonUtils } from '@app/shared/services';

const routes: Routes = [
  {
    path: '',
    component: ChangeHistoryIndexComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.CHANGE_HISTORY,
      nationId: CommonUtils.getNationId()
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChangeHistoryRoutingModule { }
