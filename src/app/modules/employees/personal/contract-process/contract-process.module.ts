import { SharedModule } from '@app/shared';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContractProcessRoutingModule } from './contract-process-routing.module';
import { ContractProcessListComponent } from './page/contract-process-list/contract-process-list.component';
import { ContractProcessFormComponent } from './page/contract-process-form/contract-process-form.component';

@NgModule({
  declarations: [ContractProcessListComponent, ContractProcessFormComponent],
  imports: [
    CommonModule,
    SharedModule,
    ContractProcessRoutingModule
  ],
  entryComponents: [ContractProcessListComponent, ContractProcessFormComponent]
})
export class ContractProcessModule { }
