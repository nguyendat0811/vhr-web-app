import { CommonUtils } from '@app/shared/services/common-utils.service';
import { ContractProcessListComponent } from './page/contract-process-list/contract-process-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PropertyResolver } from '@app/shared/services/property.resolver';
import { RESOURCE } from '@app/core';

const routes: Routes = [
  {
    path: '',
    component: ContractProcessListComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.EMP_TYPE_PROCESS,
      nationId: CommonUtils.getNationId()
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContractProcessRoutingModule { }
