import { APP_CONSTANTS, ACTION_FORM, RESOURCE } from './../../../../../../core/app-config';
import { FileControl } from './../../../../../../core/models/file.control';
import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { EmpTypeProcessService, LabourContractTypeService, EmpTypeService, SysCatService } from '@app/core/services';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { AppComponent } from '@app/app.component';
import { ValidationService } from '@app/shared/services/validation.service';
import { LabourContractDetailService } from '@app/core/services/labour-contract-detail/labour-contract-detail.service';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'contract-process-form',
  templateUrl: './contract-process-form.component.html'
})
export class ContractProcessFormComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  laboutTypeList: any;
  contractByOfficialList: any;
  contractByLabourTypeList: any;
  empTypeList: any;
  soldierLevelList: any;
  labourContractDetailList: any;
  workingTimeList: any;
  isNotHDLD: boolean; // ko phai hop dong lao dong
  isHDLD: boolean;  // la hop dong lao dong
  isLabourContract: boolean;
  isLabourService: boolean; // la họp dong dich vu
  isLabourNonRequired: boolean;
  empTypeProcessId: number;
  fileControl = new FileControl(null);

  formConfig = {
    empTypeProcessId: [''],
    employeeId: [ '', [ValidationService.required]],
    empTypeId: [ '', [ValidationService.required]],
    labourContractTypeId: [ '', [ValidationService.required]],
    labourContractDetailId: [ '', [ValidationService.required]],
    effectiveDate: [ '', [ValidationService.required]],
    expiredDate: [''],
    signedDate: [ '', [ValidationService.required]],
    contractDecisionNumber: [ '', [ValidationService.required, ValidationService.maxLength(50)]],
    soldierLevelId: [''],
    managementTypeId: [ ''],
    description: [ '', [ValidationService.maxLength(500)]],
    contractMonth: [ '', [ValidationService.maxLength(4), ValidationService.positiveInteger, Validators.min(1), Validators.max(1000)]],
    workingTimeId: [ '', [ValidationService.required]],
  };

  formConfigNotHDLD = {
    empTypeProcessId: [''],
    employeeId: [ '', [ValidationService.required]],
    empTypeId: [ '', [ValidationService.required]],
    effectiveDate: [ '', [ValidationService.required]],
    expiredDate: [''],
    signedDate: [ '', [ValidationService.required]],
    contractDecisionNumber: [ '', [ValidationService.required, ValidationService.maxLength(50)]],
    soldierLevelId: [''],
    managementTypeId: [ ''],
    description: [ '', [ValidationService.maxLength(500)]],
  };
  constructor(public actr: ActivatedRoute
            , public activeModal: NgbActiveModal
            , private empTypeProcessService: EmpTypeProcessService
            , private app: AppComponent
            , private labourContractTypeService: LabourContractTypeService
            , private empTypeService: EmpTypeService
            , private sysCatService: SysCatService
            , private labourContractDetailService: LabourContractDetailService) {
    super(actr, RESOURCE.REWARD_DISCIPLINE, ACTION_FORM.INSERT);
    this.formSave = this.buildForm({}, this.formConfig
      , ACTION_FORM.INSERT
      , [ ValidationService.notAffter('signedDate', 'effectiveDate', 'app.contractProcess.effectiveDate')
         , ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.contractProcess.expiredDate')]);
    this.formSave.addControl('file', new FileControl(null));
    this.empTypeService.getAllByEmpTypeByIsUsed().subscribe(res => {

      this.empTypeList = res.data;
      console.log('this.empTypeList',this.empTypeList);
    });
    this.labourContractTypeService.findActiveLabourContractType().subscribe(res => {
      this.laboutTypeList = res.data;
    });
    this.showSoldierLevel();
    this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.WORKING_TIME).subscribe(res => {
      this.workingTimeList = res.data;
    });
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.isNotHDLD = true;
    this.isHDLD = true;
    this.isLabourContract = true;
    this.isLabourService = false;
    this.isLabourNonRequired = true;
  }

  /**
   * processSaveOrUpdate
   */
  processSaveOrUpdate() {
    // Xet file vao form
    this.formSave.addControl('file', this.fileControl);
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    } else {
      this.empTypeProcessService.validateBeforeSave(this.formSave.value).subscribe(res => {
        if (this.empTypeProcessService.requestIsSuccess(res)) {
          const obj = JSON.parse(res.data);
          const returnCode = obj['returnCode'];
          const extraValue = obj['extraValue'];
          if (returnCode === 1) {
            // Giao qua trinh
            this.empTypeProcessService.processReturnMessage({type: 'WARNING', code: 'process.duplicateProcess'});
            return;
          } else if (returnCode === 2) {
            // Qua trinh khong lien mach
            this.app.confirmMessage('app.contracProcess.notContinuousProcess', () => {
              if (extraValue === 1) {
                // Khong co quyen voi don vi
                setTimeout(() => {
                  this.app.confirmMessage('app.contracProcess.confirmHasNotPermission', () => {
                    this.actionSave();
                  }, null);
                }, 500);
              } else {
                this.actionSave();
              }
            }, null);
          } else {
            if (extraValue === 1) {
              // Khong co quyen voi don vi
              this.app.confirmMessage('app.contracProcess.confirmHasNotPermission', () => {
                this.actionSave();
              }, null);
            } else {
              this.app.confirmMessage(null, () => {
                this.actionSave();
              }, null);
            }
          }
        }
      });
    }
  }

  private actionSave() {
    this.empTypeProcessService.saveOrUpdateFormFile(this.formSave.value)
    .subscribe(res => {
      if (this.empTypeProcessService.requestIsSuccess(res)) {
        this.activeModal.close(res);
      }
    });
  }
  /****************** CAC HAM COMMON DUNG CHUNG ****/
  get f () {
    return this.formSave.controls;
  }

  /**
  * buildForm
  */
  private buildForms(data?: any,  actionForm?: ACTION_FORM): void {
    this.formSave = this.buildForm(data, this.formConfig
      , actionForm
      , [ValidationService.notAffter('signedDate', 'effectiveDate', 'app.contractProcess.effectiveDate')
      , ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.contractProcess.expiredDate')]);
  }

  private buildFormNotHDLD(data?: any,  actionForm?: ACTION_FORM) {
    this.formSave = this.buildForm(data, this.formConfigNotHDLD
      , actionForm
      , [ValidationService.notAffter('signedDate', 'effectiveDate', 'app.contractProcess.effectiveDate')
      , ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.contractProcess.expiredDate')]);
  }

  private buildFormFile(data) {
    if (data.fileAttachment && data.fileAttachment.file) {
      this.fileControl.setFileAttachment(data.fileAttachment.file);
    }
  }
  /**
   * setFormValue
   * param data
   */
  public setFormValue(propertyConfigs: any, data?: any) {
    this.propertyConfigs = propertyConfigs;
    if (data && data.empTypeProcessId > 0) {
      this.buildForms(data, ACTION_FORM.UPDATE);
    } else {
      this.buildForms(data, ACTION_FORM.INSERT);
    }
    this.buildFormFile(data);
    if (data && data.empTypeId) {
      this.onEmpTypeChange(data.empTypeId, data , true);
    }
    if (data && data.labourContractTypeId) {
      this.onLabourContractTypeChange(data.labourContractTypeId, true);
    }
  }

  /**
   * onEmpTypeChange
   * param event
   */
  public onEmpTypeChange(event, data , isDefault?: boolean) {
    if (!event) {
      return;
    }
    let formDefaultValue;
    if (data) {
       formDefaultValue = data ;
    } else {
      formDefaultValue = this.formSave.value;
    }
    this.empTypeService.findByIdMarketCompanyId(CommonUtils.getCurrentMarketCompanyId(), event).subscribe(res => {
      if (res.data) {
        if (res.data.hasLabourContractInfo === 1 && res.data.hasSoldierInfo === 1) {
          this.buildForms(formDefaultValue, null);
          this.isNotHDLD = true;
          this.isHDLD = true;
          this.isLabourContract = true;
          this.isLabourService = false;
          this.isLabourNonRequired = true;
        } else if (res.data.hasLabourContractInfo === 1) {
          this.buildForms(formDefaultValue, null);
          this.formSave.removeControl('soldierLevelId');
          this.isNotHDLD = false;
          this.isHDLD = true;
        } else if (res.data.hasSoldierInfo === 1) {
          this.buildFormNotHDLD(formDefaultValue, null);
          this.showSoldierLevel();
          this.isNotHDLD = true;
          this.isHDLD = false;
          this.isLabourContract = true;
        } else if (res.data.hasLabourContractInfo === 0 && res.data.hasSoldierInfo === 0) {
          this.isNotHDLD = false;
          this.isHDLD = false;
        }
      }

    });
  }

  /**
   * onLabourContractTypeChange
   * param event
   */
  public onLabourContractTypeChange(e, isDefault?: boolean) {
    if (e) {
      this.labourContractTypeService.findByIdMarketCompanyId(CommonUtils.getCurrentMarketCompanyId(), e).subscribe(res => {
        // xu ly an hien chi tiet hop dong
        let contracTypeDetailValue = null;
        if (isDefault) {
          contracTypeDetailValue = this.formSave.value.labourContractDetailId;
        }
        // Kiem trloai hop dong dang chon la gi?
        this.labourContractTypeService.checkLabourType(res.data.code).subscribe( res1 => {
          if (res1.data === 1) {
            // Hop dong thoi vu hoac Hop dong CTV
            this.isLabourService = false;
            this.isLabourNonRequired = false;
            this.formSave.removeControl('workingTimeId');
          } else if (res1.data === 2) {
              // Chi la hop dong dich vu
              this.isLabourService = true;
              this.isLabourNonRequired = false;
              const workingTimeIdDefault = this.formSave.value.workingTimeId;
              this.formSave.removeControl('workingTimeId');
              this.formSave.addControl('workingTimeId', CommonUtils.createControl(this.actionForm, 'workingTimeId',
                workingTimeIdDefault, [ValidationService.required]));
          } else {
            // Cac loai hop dong khac
            this.isLabourService = false;
            this.isLabourNonRequired = true;
            this.formSave.removeControl('workingTimeId');
          }

          // Xu ly validate thoi han hop dong
          const contractMonthValue = this.formSave.value.contractMonth;
          if (res1.data === 1 || res1.data === 2) {
            this.formSave.removeControl('contractMonth');
            this.formSave.addControl('contractMonth', CommonUtils.createControl(this.actionForm, 'contractMonth',
              contractMonthValue, [ValidationService.required, ValidationService.positiveInteger, Validators.min(1),
                Validators.max(1000), ValidationService.maxLength(4)]));
          } else {
            this.formSave.removeControl('contractMonth');
            this.formSave.addControl('contractMonth', CommonUtils.createControl(this.actionForm, 'contractMonth',
              contractMonthValue, [ValidationService.positiveInteger, Validators.min(1), Validators.max(1000), ValidationService.maxLength(4)]));
          }
        });

        // Lay danh sach chi tiet hop dong
        this.labourContractDetailService.getListLabourContractDetail(e).subscribe(lst => {
          this.labourContractDetailList = lst.data;
          if (lst.data.length !== 0) {
            this.formSave.removeControl('labourContractDetailId');
            this.formSave.addControl('labourContractDetailId', new FormControl(contracTypeDetailValue, [ValidationService.required]));
            this.isLabourContract = false;
          } else {
            this.formSave.removeControl('labourContractDetailId');
            this.isLabourContract = true;
          }
        });
      });
    } else {
      this.isLabourContract = true;
      this.isLabourService = false;
    }
  }

  public clearFormData () {
    this.formSave.reset();
  }

  /**
   * showSoldierLevel
   * param event
   */
  public showSoldierLevel() {
    this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.SOLDIER_LEVEL).subscribe(res => {
      this.soldierLevelList = res.data;
    });
  }
}
