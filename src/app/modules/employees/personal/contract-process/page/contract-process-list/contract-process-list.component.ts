import { EmpTypeProcessReportService } from './../../../../../../core/services/hr-report/emp-type-process-report.service';
import { BaseComponent } from './../../../../../../shared/components/base-component/base-component.component';
import { EmployeeResolver } from '@app/shared/services/employee.resolver';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DEFAULT_MODAL_OPTIONS, RESOURCE, ACTION_FORM } from '@app/core/app-config';
import { AppComponent } from '@app/app.component';
import { EmpTypeProcessService } from '@app/core';
import { ContractProcessFormComponent } from '../contract-process-form/contract-process-form.component';
import { FileStorageService } from '@app/core/services/file-storage.service';
import { FileControl } from '@app/core/models/file.control';
import { saveAs } from 'file-saver';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'contract-process-list',
  templateUrl: './contract-process-list.component.html'
})
export class ContractProcessListComponent extends BaseComponent implements OnInit {
  property: FileControl;
  resultList: any = {};
  superSesultList: any = [];
  resultListLabourContractType: any = {};
  labourContractTypeId: any;
  labourContractTypeName: [];
  employeeId: number;
  empId: {employeeId: any};
  @ViewChild('pTable') dataTable: any;
  constructor(public actr: ActivatedRoute
            , private modalService: NgbModal
            , private app: AppComponent
            , private empTypeProcessService: EmpTypeProcessService
            , private employeeResolver: EmployeeResolver
            , private fileStorage: FileStorageService
            , private empTypeProcessReportService: EmpTypeProcessReportService
            ) {
    super(actr, RESOURCE.EMP_TYPE_PROCESS, ACTION_FORM.SEARCH);
    this.setMainService(empTypeProcessService);
    this.employeeResolver.EMPLOYEE.subscribe(
      data => {
        if (data) {
            this.employeeId = data;
            this.empId = {employeeId: data};
            this.formSearch =  this.buildForm(this.empId, {employeeId: ['']});
            this.processSearch();
        }
      }
    );
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
  }

  /**
   * prepareUpdate
   * param item
   */
  prepareSaveOrUpdate(item) {
    if (item && item.empTypeProcessId > 0) {
      this.empTypeProcessService.findOne(item.empTypeProcessId)
        .subscribe(res => {
          this.activeModal(res.data);
        });
    } else {
      this.activeModal({employeeId: this.employeeId});
    }
  }

  /**
   * show model
   * data
   */
  private activeModal(data?: any) {
    const modalRef = this.modalService.open(ContractProcessFormComponent, DEFAULT_MODAL_OPTIONS);
    if (data) {
      modalRef.componentInstance.setFormValue(this.propertyConfigs, data);
    }
    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
      if (this.empTypeProcessService.requestIsSuccess(result)) {
        this.employeeResolver.COMMON_INFO.next(this.employeeId);
        this.employeeResolver.EMP_TYPE_PROCESS.next(this.employeeId);
        this.processSearch();
        if (this.dataTable) {
          this.dataTable.first = 0;
        }
      }
    });
  }

  /**
   * Xu ly download file trong danh sach
   */
  public downloadFile(fileData) {
    this.fileStorage.downloadFile(fileData.id).subscribe(res => {
        saveAs(res , fileData.fileName);
    });
  }
  public exportContract(data) {
    this.empTypeProcessReportService.exportLabour(data).subscribe((res) => {
      if (res.type === 'application/json') {
        const reader = new FileReader();
        reader.addEventListener('loadend', (e) => {
          const text = e.srcElement['result'];
          const json = JSON.parse(text);
          this.empTypeProcessReportService.processReturnMessage(json);
        });
        reader.readAsText(res);
      } else {
        saveAs(res, 'employee_contract.pdf');
      }
    });
  }
  public exportTerminationAgreement(data) {
    this.empTypeProcessReportService.exportTerminationAgreement(data).subscribe((res) => {
      if (res.type === 'application/json') {
        const reader = new FileReader();
        reader.addEventListener('loadend', (e) => {
          const text = e.srcElement['result'];
          const json = JSON.parse(text);
          this.empTypeProcessReportService.processReturnMessage(json);
        });
        reader.readAsText(res);
      } else {
        saveAs(res, 'termination_agreement.pdf');
      }
    });
  }

  public processDeletes(item): void {
    if (item && item.empTypeProcessId > 0) {
      this.empTypeProcessService.confirmDelete({
        messageCode: null,
        accept: () => {
          this.empTypeProcessService.deleteById(item.empTypeProcessId)
          .subscribe(res => {
            if (this.empTypeProcessService.requestIsSuccess(res)) {
              this.employeeResolver.COMMON_INFO.next(this.employeeId);
              this.employeeResolver.EMP_TYPE_PROCESS.next(item.employeeId);
              this.processSearch();
            }
          });
        }
      });
    }
  }
}
