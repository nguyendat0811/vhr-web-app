import { SharedModule } from '@app/shared';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OccupationalSafetyPageComponent } from './occupational-safety-page/occupational-safety-page.component';
import { OccupationalSafetyRoutingModule } from './occupational-safety-routing.module';
import { OccupationalSafetyFormComponent } from './occupational-safety-form/occupational-safety-form.component';
import { OccupationalSafetyListComponent } from './occupational-safety-list/occupational-safety-list.component';

@NgModule({
  declarations: [OccupationalSafetyPageComponent, OccupationalSafetyListComponent, OccupationalSafetyFormComponent],
  imports: [
    CommonModule,
    SharedModule,

    OccupationalSafetyRoutingModule
  ],
  entryComponents: [OccupationalSafetyFormComponent],
})
export class OccupationalSafetyModule { }
