import { SysCatService } from './../../../../../core/services/hr-system/sys-cat.service';
import { TrainingPlanService } from './../../../../../core/services/training-plan/training-plan.service';
import { OccupationalSafetyService } from './../../../../../core/services/occupational-safety/occupational-safety.service';
import { FileControl } from '@app/core/models/file.control';
import { ValidationService } from './../../../../../shared/services/validation.service';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { ActivatedRoute } from '@angular/router';
import { AppComponent } from './../../../../../app.component';
import { RESOURCE, APP_CONSTANTS } from './../../../../../core/app-config';
import { FormGroup, Validators } from '@angular/forms';
import { BaseComponent } from './../../../../../shared/components/base-component/base-component.component';
import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ACTION_FORM } from '@app/core';

@Component({
  selector: 'occupational-safety-form',
  templateUrl: './occupational-safety-form.component.html',
})
export class OccupationalSafetyFormComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  periodList: any = {};
  diplomaList: any = {};
  formConfig = {
    occupationalSafetyId: [null],
    period: [null],
    employeeId: [null],
    trainingPlanId: [null, [ValidationService.required]],
    diplomaName: [null, [ValidationService.required]],
    diplomaTypeId: [null, [ValidationService.required]],
    issuedPlace: [null, [ValidationService.required]],
    effectiveDate: [null, [ValidationService.required]],
    expiredDate: [null],
    issuedDate: [null, [ValidationService.required]],
    revokedDate: [null],
    note: [null, [ValidationService.maxLength(2000)]],
  };
  constructor(public activeModal: NgbActiveModal
    , private occupationalSafetyService: OccupationalSafetyService
    , private trainingPlanService: TrainingPlanService
    , private sysCatService: SysCatService
    , private app: AppComponent
    , public actr: ActivatedRoute) {
      super(actr, RESOURCE.OCCUPATIONAL_SAFETY, ACTION_FORM.INSERT);
      this.buildForms({}, ACTION_FORM.INSERT);

      this.trainingPlanService.findAllTrainingPlan().subscribe(
        res => this.periodList = res.data
      );
      this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.DEGREE_DIPLOMA_TYPE).subscribe(
        res => {
          this.diplomaList = res.data;
        }
      );
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
  }

  /**
   * processSaveOrUpdate
   */
  processSaveOrUpdate() {
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    } else {
      this.app.confirmMessage(null, () => {
        this.occupationalSafetyService.saveOrUpdateFormFile(this.formSave.value)
        .subscribe(res => {
          if (this.occupationalSafetyService.requestIsSuccess(res)) {
          this.activeModal.close(res);
          }
        });
      }, () => {

      });
    }
  }

  /****************** CAC HAM COMMON DUNG CHUNG ****/
  get f () {
    return this.formSave.controls;
  }

  /**
   * buildForm
   */
  private buildForms(data?: any, actionForm?: ACTION_FORM): void {
    this.formSave = this.buildForm(data, this.formConfig, actionForm
    , [ValidationService.notBefore('revokedDate', 'effectiveDate', 'app.occupationalSafety.effectiveDate')
    , ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.occupationalSafety.expiredDate')
    , ValidationService.notAffter('issuedDate', 'effectiveDate', 'app.occupationalSafety.effectiveDate')]);
    this.formSave.addControl('file', new FileControl(null));
    if (data.fileAttachment) {
      (this.formSave.controls['file'] as FileControl).setFileAttachment(data.fileAttachment.occupationalSafetyFile);
    }
  }

  /**
   * setFormValue
   * param data
   */
  public setFormValue(propertyConfigs: any, data?: any) {
    this.propertyConfigs = propertyConfigs;
    if (data && data.occupationalSafetyId > 0) {
      this.buildForms(data, ACTION_FORM.UPDATE);
    } else {
      this.buildForms(data, ACTION_FORM.INSERT);
    }
  }
}
