import { OccupationalSafetyService } from './../../../../../core/services/occupational-safety/occupational-safety.service';
import { OccupationalSafetyFormComponent } from './../occupational-safety-form/occupational-safety-form.component';
import { ActivatedRoute } from '@angular/router';
import { FileStorageService } from '@app/core/services/file-storage.service';
import { EmployeeResolver } from '@app/shared/services/employee.resolver';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RESOURCE, ACTION_FORM, DEFAULT_MODAL_OPTIONS } from '@app/core';
import { CommonUtils } from '@app/shared/services';

@Component({
  selector: 'occupational-safety-list',
  templateUrl: './occupational-safety-list.component.html',
})
export class OccupationalSafetyListComponent extends BaseComponent implements OnInit {
  resultList: any = {};
  employeeId: number;
  empId: {employeeId: any};
  @ViewChild('ptable') dataTable: any;
  constructor(private modalService: NgbModal
    , private occupationalSafetyService: OccupationalSafetyService
    , private employeeResolver: EmployeeResolver
    , public fileStorage: FileStorageService
    , public actr: ActivatedRoute) {
      super(actr, RESOURCE.OCCUPATIONAL_SAFETY, ACTION_FORM.SEARCH);
      this.setMainService(occupationalSafetyService);
      this.employeeResolver.EMPLOYEE.subscribe(
        data => {
          if (data) {
            this.employeeId = data;
            console.log(data);
            this.empId = {employeeId: data};
            this.formSearch =  this.buildForm({employeeId: this.employeeId}, {employeeId: ['']});
            this.processSearch();
          }
        }
      );
    }

  /**
   * ngOnInit
   */
  ngOnInit() {
  }

  /**
   * prepareUpdate
   * param item
   */
  prepareSaveOrUpdate(item) {
    if (item && item.occupationalSafetyId > 0) {
      this.occupationalSafetyService.findOne(item.occupationalSafetyId)
        .subscribe(res => {

          this.activeModal(res.data);
        });
    } else {
      this.activeModal(this.empId);
    }
  }
  /**
   * show model
   * data
   */
  private activeModal(data?: any) {
    const modalRef = this.modalService.open(OccupationalSafetyFormComponent, DEFAULT_MODAL_OPTIONS);
    if (data) {
      modalRef.componentInstance.setFormValue(this.propertyConfigs, data);
    }
    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
      if (this.occupationalSafetyService.requestIsSuccess(result)) {
        this.processSearch();
      }
    });
  }

  public downloadFile(fileData) {
    this.fileStorage.downloadFile(fileData.id).subscribe(res => {
        saveAs(res , fileData.fileName);
    });
  }
}
