import { CommonUtils } from '@app/shared/services/common-utils.service';
import { PropertyResolver } from '@app/shared/services/property.resolver';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RESOURCE } from '@app/core';
import { OccupationalSafetyPageComponent } from './occupational-safety-page/occupational-safety-page.component';

const routes: Routes = [
  {
    path: '',
    component: OccupationalSafetyPageComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.OCCUPATIONAL_SAFETY,
      nationId: CommonUtils.getNationId()
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OccupationalSafetyRoutingModule { }
