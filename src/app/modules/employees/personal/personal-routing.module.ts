import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';

const routes: Routes = [
  {
    path: '',
    component : LayoutComponent,
    children: [
      {
        path: 'personal-information',
        loadChildren : './personal-information/personal-information.module#PersonalInformationModule'
      }, {
        path: 'financial',
        loadChildren : './financial/financial.module#FinancialModule'
      }, {
        path: 'other-information',
        loadChildren: './other-information/other-information.module#OtherInformationModule'
      }, {
        path: 'party-union',
        loadChildren: './party-union/party-union.module#PartyUnionModule'
      }, {
        path: 'military-information',
        loadChildren: './military-information/military-information.module#MilitaryInformationModule'
      }, {
        path: 'visa-passport-information',
        loadChildren: './visa-passport-information/visa-passport-information.module#VisaPassportInformationModule'
      }, {
        path: 'work-process',
        loadChildren: './work-process/work-process.module#WorkProcessModule'
      }, {
        path: 'contract-process',
        loadChildren: './contract-process/contract-process.module#ContractProcessModule'
      }, {
        path: 'education-process',
        loadChildren: './education-process/education-process.module#EducationProcessModule'
      }, {
        path: 'family-relationship',
        loadChildren: './family-relationship/family-relationship.module#FamilyRelationshipModule'
      }, {
        path: 'emp-salary-process',
        loadChildren: './emp-salary-process/emp-salary-process.module#EmpSalaryProcessModule'
      }, {
        path: 'reward-discipline',
        loadChildren: './reward-discipline/reward-discipline.module#RewardDisciplineModule'
      }, {
        path: 'allowance-process',
        loadChildren: './allowance-process/allowance-process.module#AllowanceProcessModule'
      }, {
        path: 'trade-union-process',
        loadChildren: './trade-union-process/trade-union-process.module#TradeUnionProcessModule'
      }, {
        path: 'social-insurance-process',
        loadChildren: './social-insurance-process/social-insurance-process.module#SocialInsuranceProcessModule'
      }, {
        path: 'health-insurance',
        loadChildren: './health-insurance/health-insurance.module#HealthInsuranceModule'
      }, {
        path: 'emp-file',
        loadChildren: './emp-file/emp-file.module#EmpFileModule'
      }, {
        path: 'change-history',
        loadChildren: './change-history/change-history.module#ChangeHistoryModule',
      }, {
        path: 'emp-position-salary',
        loadChildren: './emp-position-salary/emp-position-salary.module#EmpPositionSalaryModule',
      }, {
        path: 'emp-income-tax',
        loadChildren: './emp-income-tax/emp-income-tax.module#EmpIncomeTaxModule',
      }, {
        path: 'long-leave-process',
        loadChildren: './long-leave-process/long-leave-process.module#LongLeaveProcessModule',
      }, {
        path: 'occupational-safety',
        loadChildren: './occupational-safety/occupational-safety.module#OccupationalSafetyModule'
      }, {
        path: 'emp-work-schedule',
        loadChildren: './emp-work-schedule/emp-work-schedule.module#EmpWorkScheduleModule',
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PersonalRoutingModule { }
