import { CommonUtils } from '@app/shared/services';
import { RESOURCE } from '@app/core';
import { PropertyResolver } from './../../../../shared/services/property.resolver';
import { FinancialIndexComponent } from './page/financial-index/financial-index.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component : FinancialIndexComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.FINANCIAL,
      nationId: CommonUtils.getNationId()
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FinancialRoutingModule { }
