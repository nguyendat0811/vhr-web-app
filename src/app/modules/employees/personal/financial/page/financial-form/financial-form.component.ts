import { BaseComponent } from './../../../../../../shared/components/base-component/base-component.component';
import { FormArray, FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit, Input, ElementRef, Renderer2, ViewChild } from '@angular/core';
import { AppComponent } from '@app/app.component';
import { LocationService, APP_CONSTANTS, SysCatService, RESOURCE, ACTION_FORM } from '@app/core';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { EmpBankAccountService } from '@app/core/services/emp-bank-account/emp-bank-account.service';
import { EmployeeResolver } from '@app/shared/services/employee.resolver';
import { ActivatedRoute } from '@angular/router';
import { ValidationService } from '@app/shared/services';


@Component({
  selector: 'financial-form',
  templateUrl: './financial-form.component.html'
})
export class FinancialFormComponent extends BaseComponent implements OnInit {

  formSave: FormArray;
  listLocation: any = {};
  empId: number;
  CONSTANTS = APP_CONSTANTS;
  emp: {employeeId: any};
  @Input() item: any;
  constructor(private formBuilder: FormBuilder
    , private sysCatService: SysCatService
    , private locationService: LocationService
    , private empBankAccountService: EmpBankAccountService
    , private employeeResolver: EmployeeResolver
    , private app: AppComponent
    , public actr: ActivatedRoute) {
    super(actr, RESOURCE.FINANCIAL, ACTION_FORM.INSERT);

    this.buildFormFinancial();
     // Tỉnh Thành, Thành phố
     this.locationService.getListProvinces()
    .subscribe(res => this.listLocation = res);

    this.employeeResolver.EMPLOYEE.subscribe(
      data => {
        this.empId = data;
        this.emp = {employeeId: data};
        if (data) {
          this.empBankAccountService.findByEmployeeId(data).subscribe(
            (res) => {
              if (res) {
                this.buildFormFinancial(res);
              }
            }
          );
        }
      }
    );
  }

  ngOnInit() {
  }

  /**
   * makeDefaultFinancialForm
   */
  private makeDefaultFinancialForm(): FormGroup {
    const group = {
      empBankAccountId: [null],
      employeeId: [this.empId],
      bankAccount: [null, [ValidationService.required, ValidationService.maxLength(50)]],
      bankId: [null, [ValidationService.required, ValidationService.maxLength(20)]],
      bankBranch: [null, [ValidationService.maxLength(50)]],
      locationId: [null, [ValidationService.maxLength(20)]],
      isMainAccount: [null],
    };
    return this.buildForm({}, group);
  }

  private buildFormFinancial(listEmpBankAccount?: any) {
    const controls = new FormArray([]);
    if (!listEmpBankAccount) {
      const group = this.makeDefaultFinancialForm();
      controls.push(group);
    } else {
      for (const i in listEmpBankAccount) {
        const empBankAccount = listEmpBankAccount[i];
        const group = this.makeDefaultFinancialForm();
        group.patchValue(empBankAccount);
        group.patchValue(this.emp);
        controls.push(group);
      }
    }
    controls.setValidators(ValidationService.duplicateArray(
      ['bankAccount'], 'bankAccount', 'emp.financialInformation.accountNumber'));
    this.formSave = controls;
  }

  private validateBeforeSave(): boolean {
    return CommonUtils.isValidForm(this.formSave);
  }

  processSaveOrUpdate() {
     if (!this.validateBeforeSave()) {
       return;
     }
    this.app.confirmMessage(null, () => {// on accepted
       this.empBankAccountService.saveListEmpBankAccount(this.formSave.value, this.empId)
        .subscribe(res => {
          if (this.empBankAccountService.requestIsSuccess(res)) {
            this.empBankAccountService.findByEmployeeId(this.empId).subscribe(
              (data) => {
              this.buildFormFinancial(data);
              }
            );
          }
         });
    }, () => {
      // on rejected
    });
  }

  /**
   * addFinacialInfo
   * param index
   * param item
   */
  public addFinacialInfo(index: number, item: FormGroup) {
    const controls = this.formSave as FormArray;
    controls.insert(index + 1, this.makeDefaultFinancialForm());
  }

  /**
   * prepareDelete
   * param item
   */
  public processDeleteAccount(index: number, id: number) {
    if (id && id > 0) {
      const controls = this.formSave as FormArray;
      this.app.confirmDelete(null, () => {// on accepted
        this.empBankAccountService.deleteById(id)
          .subscribe(res => {
            if (this.empBankAccountService.requestIsSuccess(res)) {
              // this.empBankAccountService.findByEmployeeId(this.empId).subscribe(
              //   (data) => {
              //   this.buildFormFinancial(data);
              //   }
              // );
              controls.removeAt(index);
            }
          });
      }, () => {// on rejected

      });
    } else {
      const controls = this.formSave as FormArray;
      controls.removeAt(index);
    }
  }

  /**
   * Xu ly check box ton tai duy nhat
   */
  checkOnly(e, index, formControlName) {
    if (e.target.checked) {
      const controls = this.formSave as FormArray;
      for (let i = 0; i < controls.length; i++) {
        if (index !== i) {
          controls.controls[i].get(formControlName).setValue(false);
        }
      }
    }
  }
}
