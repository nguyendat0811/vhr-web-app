import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@app/shared';

import { FinancialRoutingModule } from './financial-routing.module';
import { FinancialIndexComponent } from './page/financial-index/financial-index.component';
import { FinancialFormComponent } from './page/financial-form/financial-form.component';

@NgModule({
  declarations: [FinancialIndexComponent, FinancialFormComponent],
  imports: [
    CommonModule,
    SharedModule,
    FinancialRoutingModule
  ],
  entryComponents: [FinancialIndexComponent, FinancialFormComponent]
})
export class FinancialModule { }
