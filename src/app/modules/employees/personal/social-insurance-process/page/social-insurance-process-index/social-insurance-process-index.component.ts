import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { RESOURCE } from '@app/core';

@Component({
  selector: 'social-insurance-process',
  templateUrl: './social-insurance-process-index.component.html'
})
export class SocialInsuranceProcessIndexComponent extends BaseComponent implements OnInit {

  constructor(public act: ActivatedRoute) { 
    super(act, RESOURCE.SOCIAL_INSURANCE_PROCESS);
  }

  ngOnInit() {
  }

}
