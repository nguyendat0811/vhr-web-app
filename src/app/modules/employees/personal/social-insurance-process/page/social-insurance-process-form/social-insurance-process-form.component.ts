import { ActivatedRoute } from '@angular/router';
import { ACTION_FORM } from './../../../../../../core/app-config';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, Validators } from '@angular/forms';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { AppComponent } from '@app/app.component';
import { ValidationService } from '@app/shared/services/validation.service';
import { EmpSocialInsuranceService } from '@app/core/services/social-insurance-process/social-insurance-process.service';
import { APP_CONSTANTS, SysCatService, RESOURCE } from '@app/core';
import { FileControl } from '@app/core/models/file.control';

@Component({
  selector: 'social-insurance-process-form',
  templateUrl: './social-insurance-process-form.component.html',
})

export class SocialInsuranceFormComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  fundList: any = {};
  formConfig = {
    empSocialInsuranceId: [null],
    employeeId: [null],
    insuranceNumber: [null, [ValidationService.required, ValidationService.maxLength(50)]],
    issuedDate: [null, [ValidationService.required]],
    socialFundId: [null, [ValidationService.required]],
    effectiveDate: [null, [ValidationService.required]],
    expiredDate: [null],
    note: [null, [ValidationService.maxLength(500)]],
  };

  constructor(public activeModal: NgbActiveModal
    , public actr: ActivatedRoute
    , private empSocialInsuranceService: EmpSocialInsuranceService
    , private sysCatService: SysCatService
    , private app: AppComponent) {
    super(actr, RESOURCE.SOCIAL_INSURANCE_PROCESS, ACTION_FORM.INSERT);

      this.formSave = this.buildForm({}, this.formConfig,
         ACTION_FORM.INSERT, [ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.socialInsuranceProcess.expiredDate'),
         ValidationService.notAffter('issuedDate', 'effectiveDate', 'app.socialInsuranceProcess.effectiveDate')]);
      this.formSave.addControl('file', new FileControl(null));

      this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.SOCIAL_INSURANCE_FUND).subscribe(
        res => this.fundList = res.data
      );
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
  }

  /**
   * processSaveOrUpdate
   */
  processSaveOrUpdate() {
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    }
    this.app.confirmMessage('app.empTradeUnion.finishPreviousProcess', () => {
      this.empSocialInsuranceService.saveOrUpdateFormFile(this.formSave.value)
        .subscribe(res => {
          if (this.empSocialInsuranceService.requestIsSuccess(res)) {
            this.activeModal.close(res);
          }
        });
    }, () => {

    });
  }


  /****************** CAC HAM COMMON DUNG CHUNG ****/
  get f() {
    return this.formSave.controls;
  }

  // /**
  // * buildForm
  // */
  // private buildForm(data): void {
  //   this.formSave = CommonUtils.createForm(data, {
  //     empSocialInsuranceId: [null],
  //     employeeId: [null
  //     ],
  //     insuranceNumber: [null, Validators.compose([ValidationService.required, ValidationService.maxLength(50)])
  //     ],
  //     issuedDate: [null, Validators.compose([ValidationService.required])
  //     ],
  //     socialFundId: [null, Validators.compose([ValidationService.required])
  //     ],
  //     effectiveDate: [null, Validators.compose([ValidationService.required])
  //     ],
  //     expiredDate: [null, Validators.compose([])
  //     ],
  //     note: [null, Validators.compose([ValidationService.maxLength(500)])
  //     ],
  //   }, Validators.compose([ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.socialInsuranceProcess.expiredDate'),
  //   ValidationService.notAffter('issuedDate', 'effectiveDate', 'app.socialInsuranceProcess.effectiveDate')]));
  //   this.formSave.addControl('file', new FileControl(null, [ValidationService.required]));
  //   // const fileControl = new FileControl(null, ValidationService.required);
  //   if (data.fileAttachment) {
  //     (this.formSave.controls['file'] as FileControl).setFileAttachment(data.fileAttachment.empSocialInsuranceFile);
  //   }
  //   // this.formSave.addControl('file', fileControl);
  // }

  /**
   * setFormValue
   * param data
   */
  public setFormValue(propertyConfigs: any, data?: any) {
    this.propertyConfigs = propertyConfigs;
    if (data && data.empSocialInsuranceId > 0) {
      this.formSave = this.buildForm(data, this.formConfig
        , ACTION_FORM.UPDATE
        , [ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.socialInsuranceProcess.expiredDate')
        , ValidationService.notAffter('issuedDate', 'effectiveDate', 'app.socialInsuranceProcess.effectiveDate')] );
    } else {
      this.formSave = this.buildForm(data, this.formConfig
        , ACTION_FORM.INSERT
        , [ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.socialInsuranceProcess.expiredDate')
        , ValidationService.notAffter('issuedDate', 'effectiveDate', 'app.socialInsuranceProcess.effectiveDate')] );
    }

    this.formSave.addControl('file', new FileControl(null));
    if (data.fileAttachment) {
      (this.formSave.controls['file'] as FileControl).setFileAttachment(data.fileAttachment.empSocialInsuranceFile);
    }
  }
}
