import { ACTION_FORM } from './../../../../../../core/app-config';
import { RESOURCE } from '@app/core/app-config';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { EmployeeResolver } from '@app/shared/services/employee.resolver';
import { EmpSocialInsuranceService } from '@app/core/services/social-insurance-process/social-insurance-process.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from '@app/app.component';
import { SysCatTypeService, DEFAULT_MODAL_OPTIONS } from '@app/core';
import { SocialInsuranceFormComponent } from '../social-insurance-process-form/social-insurance-process-form.component';
import { FileStorageService } from '@app/core/services/file-storage.service';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { CommonUtils } from '@app/shared/services';
import { saveAs } from 'file-saver';

@Component({
  selector: 'social-insurance-process-search',
  templateUrl: './social-insurance-process-search.component.html'
})
export class SocialInsuranceProcessSearchComponent extends BaseComponent implements OnInit {
  // resultList: any = {};
  fundList: any = {};
  employeeId: number;
  empId: {employeeId: any};
  // @ViewChild('ptable') dataTable: any;
  constructor(
      private modalService: NgbModal
    , private fileStorage: FileStorageService
    , private employeeResolver: EmployeeResolver
    , private sysCatTypeService: SysCatTypeService
    , private socialInsuranceProcessService: EmpSocialInsuranceService
    , private app: AppComponent
    , public actr: ActivatedRoute
  ) {
    super(actr, RESOURCE.SOCIAL_INSURANCE_PROCESS, ACTION_FORM.SEARCH);
    this.setMainService(socialInsuranceProcessService);
    this.employeeResolver.EMPLOYEE.subscribe(
      data => {
        if (data) {
          this.employeeId = data;
          this.empId = {employeeId: data};
          this.formSearch =  this.buildForm({employeeId: this.employeeId}, {employeeId: []});
          this.processSearch();
        }
      }
    );

    // Fund List
    this.sysCatTypeService.getListSysCat('DSTC').subscribe(res =>
      this.fundList = res.data);
   }

  ngOnInit() {
  }

  /* Action Modal*/
  private activeModel(data?: any) {
    const modalRef = this.modalService.open(SocialInsuranceFormComponent, DEFAULT_MODAL_OPTIONS);
    if (data) {
      modalRef.componentInstance.setFormValue(this.propertyConfigs, data);
    }
    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
      if (this.socialInsuranceProcessService.requestIsSuccess(result)) {
        this.processSearch();
      }
    });
  }

   /**
   * prepareUpdate
   * param item
   */
  prepareSaveOrUpdate(item) {
    if (item && item.empSocialInsuranceId > 0) {
      this.socialInsuranceProcessService.findOne(item.empSocialInsuranceId)
        .subscribe(res => {
          this.activeModel(res.data);
        });
    } else {
      this.activeModel(this.empId);
    }
  }

  public downloadFile(fileData) {
    this.fileStorage.downloadFile(fileData.id).subscribe(res => {
        saveAs(res , fileData.fileName);
    });
  }

}
