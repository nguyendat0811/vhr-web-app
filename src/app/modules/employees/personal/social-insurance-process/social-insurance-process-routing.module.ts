import { CommonUtils } from './../../../../shared/services/common-utils.service';
import { RESOURCE } from '@app/core/app-config';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SocialInsuranceProcessIndexComponent } from './page/social-insurance-process-index/social-insurance-process-index.component';
import { PropertyResolver } from '@app/shared/services/property.resolver';


const routes: Routes = [
  {
    path: '',
    component: SocialInsuranceProcessIndexComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.SOCIAL_INSURANCE_PROCESS,
      nationId: CommonUtils.getNationId()
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SocialInsuranceProcessRoutingModule { }
