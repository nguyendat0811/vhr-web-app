import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SocialInsuranceProcessRoutingModule } from './social-insurance-process-routing.module';
import { SharedModule } from '@app/shared';
import { SocialInsuranceProcessIndexComponent } from './page/social-insurance-process-index/social-insurance-process-index.component';
import { SocialInsuranceProcessSearchComponent } from './page/social-insurance-process-search/social-insurance-process-search.component';
import { SocialInsuranceFormComponent } from './page/social-insurance-process-form/social-insurance-process-form.component';

@NgModule({
  declarations: [SocialInsuranceProcessIndexComponent, SocialInsuranceProcessSearchComponent, SocialInsuranceFormComponent],
  imports: [
    CommonModule,
    SharedModule,

    SocialInsuranceProcessRoutingModule
  ],
  entryComponents: [SocialInsuranceProcessSearchComponent, SocialInsuranceFormComponent],

})
export class SocialInsuranceProcessModule { }
