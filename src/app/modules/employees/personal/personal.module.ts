import { SharedModule } from '@app/shared';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PersonalRoutingModule } from './personal-routing.module';
import { LayoutComponent, FilterPipe } from './layout/layout.component';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { EmpInfoComponent } from './layout/emp-info/emp-info.component';
import { PickTabModalComponent } from './layout/pick-tab-modal/pick-tab-modal.component';

import {CheckboxModule} from 'primeng/checkbox';
@NgModule({
  declarations: [LayoutComponent, EmpInfoComponent, PickTabModalComponent, FilterPipe],
  imports: [
    CommonModule,
    PersonalRoutingModule,
    SharedModule,
    CheckboxModule,
    ScrollPanelModule
  ],
  entryComponents: [PickTabModalComponent]
})
export class PersonalModule { }
