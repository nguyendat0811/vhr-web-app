import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WorkProcessRoutingModule } from './work-process-routing.module';
import { WorkProcessListComponent } from './page/work-process-list/work-process-list.component';
import { WorkProcessFormComponent } from './page/work-process-form/work-process-form.component';
import { SharedModule } from '@app/shared';
import { WorkProcessIndexComponent } from './page/work-process-index/work-process-index.component';

@NgModule({
  declarations: [WorkProcessListComponent, WorkProcessFormComponent, WorkProcessIndexComponent],
  imports: [
    CommonModule,
    SharedModule,
    WorkProcessRoutingModule
  ],
  entryComponents: [WorkProcessFormComponent]
})
export class WorkProcessModule { }
