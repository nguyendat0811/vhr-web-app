import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, Validators } from '@angular/forms';
import { Component, OnInit, Injectable, ViewChildren, QueryList } from '@angular/core';
import { WorkProcessService, APP_CONSTANTS, ACTION_FORM, RESOURCE } from '@app/core';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { AppComponent } from '@app/app.component';
import { ActivatedRoute } from '@angular/router';
import { ValidationService } from '@app/shared/services/validation.service';
import { EmployeeResolver } from '@app/shared/services/employee.resolver';
import { FileControl } from '@app/core/models/file.control';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { SysCatService, OrganizationService, DocumentTypeService, DocumentReasonService } from '@app/core';

@Component({
  selector: 'work-process-form',
  templateUrl: './work-process-form.component.html',
  providers: [EmployeeResolver ]
})
@Injectable()
export class WorkProcessFormComponent extends BaseComponent implements OnInit {

  documentTypeList: any;
  documentReasonList: any;
  workTypeList: any;
  isHasReason: any;
  workingPlaceList: any;
  formSaveInnerProcess: FormGroup;
  formSaveOuterProcess: FormGroup;
  formType = APP_CONSTANTS.WORK_PROCESS.TYPE_INNER;
  inOutFlag: any;
  @ViewChildren('unit')
  public unit;
  @ViewChildren('concurrentUnit')
  public concurrentUnit;
  @ViewChildren('concurrentPosition')
  public concurrentPosition;
  positionPickerFilter: string;

  formConfigInnerProcess = {
    workProcessId: [null],
    employeeId: [null],
    type: [1],
    effectiveDate: [null, [ValidationService.required, ValidationService.beforeCurrentDate]],
    expiredDate: [null],
    positionId: [null, [ValidationService.required]],
    organizationId: [ null, [ValidationService.required]],
    concurrentOrganizationId: [null],
    concurrentPositionId: [null],
    documentNumber: [null, [ValidationService.required, ValidationService.maxLength(50)]],
    signedDate: [null],
    intendedJoinDate: [null],
    documentTypeId: [null, [ValidationService.required]],
    workingPlaceId: [null],
    documentReasonId: [null],
    description: [null, [ValidationService.maxLength(500)]],
    isCurrent: [null]
  };

  formConfigOuterProcess = {
      workProcessId: [null],
      employeeId: [null],
      type: [null],
      detailDuty: [null, [ValidationService.maxLength(255)]],
      organization: [null, [ValidationService.required, ValidationService.maxLength(255)]],
      position: [null, [ValidationService.maxLength(255)]],
      address: [null, [ValidationService.maxLength(255)]],
      effectiveDate: [null, [ValidationService.required]],
      expiredDate: [null],
      isCurrent: [null]
  };

  constructor(public actr: ActivatedRoute
            , public activeModal: NgbActiveModal
            , private documentTypeService: DocumentTypeService
            , private documentReasonService: DocumentReasonService
            , private workProcessService: WorkProcessService
            , private sysCatService: SysCatService
            , private orgService: OrganizationService
            , private app: AppComponent) {
    super(actr, RESOURCE.WORK_PROCESS, ACTION_FORM.INSERT);

    // build form qua trinh trong
    this.buildFormInnerProcess({}, ACTION_FORM.EMP_INNER_PROCESS_INSERT);

    // build form qua trinh ngoai
    this.buildFormOuterProcess({}, ACTION_FORM.EMP_OUTER_PROCESS_INSERT);

    // Loại văn bản
    this.documentTypeService.getDocumentTypeInUsed().subscribe(res => {
      this.documentTypeList = res.data;
      this.isHasReason = false;
    });

    this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.WORK_PLACE).subscribe(
      res => this.workingPlaceList = res.data
    );
  }
  ngOnInit() {
    if (this.formSaveInnerProcess.get('employeeId').value) {
      // Check có dữ liệu nháp hay ko?
      this.workProcessService.getDraftProcess(this.formSaveInnerProcess.get('employeeId').value).subscribe(res => {
         // Nếu có Combobox Type chỉ hiện tạo quá trình trong
        if (res.data) {
          this.formType = APP_CONSTANTS.WORK_PROCESS.TYPE_INNER;
          this.workTypeList = APP_CONSTANTS.WORK_PROCESS.HAS_DRAFT;
        } else {// ko có dữ liệu nháp
          // nếu có quyền havePermissionWithEmployee thì
          // hiện cmb có quá trình trong và quá trình ngoài
          // Nếu có quyền haveOtherPermissionWithEmployee thì hiện cmb có thêm Quá trình công tác phụ
          // Todo something
          this.workTypeList = APP_CONSTANTS.WORK_PROCESS.NO_HAS_DRAFT;
        }
      });
    }
    if (this.formSaveInnerProcess.get('documentTypeId').value) {
      this.setLstDocumentReason(this.formSaveInnerProcess.get('documentTypeId').value);
    }
  }

  /**
   * processSaveOrUpdate
   */
  processSaveOrUpdate() {
    let formData: any;
    if (this.formType === 1) {
      formData = this.formSaveInnerProcess;
    } else {
      formData = this.formSaveOuterProcess;
    }
    if (!CommonUtils.isValidForm(formData)) {
      return;
    } else {
      if (this.inOutFlag === 2) {
        this.app.confirmMessage('app.workProcess.confirmMessageInOutFlag2', () => {
          this.workProcessService.saveOrUpdateFormFile(formData.value)
            .subscribe(res => {
              if (this.workProcessService.requestIsSuccess(res)) {
                this.activeModal.close(res);
              }
            });
        }, null);
      } else {
        this.app.confirmMessage(null, () => {
          this.workProcessService.saveOrUpdateFormFile(formData.value)
          .subscribe(res => {
            if (this.workProcessService.requestIsSuccess(res)) {
              this.activeModal.close(res);
            }
          });
        }, null);
      }
    }
  }

  /****************** CAC HAM COMMON DUNG CHUNG ****/
  get f () {
    return this.formSaveInnerProcess.controls;
  }

  get fa () {
    return this.formSaveOuterProcess.controls;
  }
  /**
  * buildFormOuterProcess
  */
  private buildFormOuterProcess(data?: any, actionForm?: ACTION_FORM) {
    this.formSaveOuterProcess = this.buildForm(data, this.formConfigOuterProcess, actionForm,
      [ ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.organization.expiredDate')]);
  }
  /**
  * buildFormInnerProcess
  */
  private buildFormInnerProcess(data?: any, actionForm?: ACTION_FORM) {
    this.formSaveInnerProcess = this.buildForm(data, this.formConfigInnerProcess, actionForm,
      [ ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.organization.expiredDate')]);

    this.formSaveInnerProcess.addControl('file', new FileControl(null));
    if (data.fileAttachment) {
      (this.formSaveInnerProcess.controls['file'] as FileControl).setFileAttachment(data.fileAttachment.file);
    }

    this.formSaveInnerProcess.get('effectiveDate').valueChanges.subscribe( res =>
      this.changePositionPickerFilter()
    );
    this.formSaveInnerProcess.get('expiredDate').valueChanges.subscribe( res =>
      this.changePositionPickerFilter()
    );

    this.initPositionPickerFilter();
  }
  /**
   * onWorkTypeChange
   * @ param event
   */
  public onWorkTypeChange(event) {
    // Neu la qua trinh trong
    if (event === APP_CONSTANTS.WORK_PROCESS.TYPE_INNER || event === null) {
      this.formSaveInnerProcess.get('type').setValue(APP_CONSTANTS.WORK_PROCESS.TYPE_INNER);
      this.formType = APP_CONSTANTS.WORK_PROCESS.TYPE_INNER ;
    } else {// Neu la qua trinh ngoai
      this.formSaveOuterProcess.get('type').setValue(APP_CONSTANTS.WORK_PROCESS.TYPE_OUTER);
      this.formType = APP_CONSTANTS.WORK_PROCESS.TYPE_OUTER ;
    }
  }

  resetDocumentReason() {
    const currentValue = this.formSaveInnerProcess.controls['documentReasonId'].value;
    if (this.isHasReason === true) {
      this.formSaveInnerProcess.removeControl('documentReasonId');
      this.formSaveInnerProcess.addControl('documentReasonId', CommonUtils.createControl(this.actionForm, 'documentReasonId', currentValue
        , [ValidationService.required], this.propertyConfigs));
    } else {
      this.formSaveInnerProcess.removeControl('documentReasonId');
      this.formSaveInnerProcess.addControl('documentReasonId', CommonUtils.createControl(this.actionForm, 'documentReasonId', null
        , [], this.propertyConfigs));
    }
  }
  /**
   * setLstDocumentReason
   * @ param id
   */
  setLstDocumentReason(id) {
    this.documentTypeService.findReasonByDocumentTypeId(id).subscribe(res => {
      if (res.length > 0) {
        this.documentReasonList = res;
        this.isHasReason = true;
        this.resetDocumentReason();
      } else {
        this.isHasReason = false;
        this.formSaveInnerProcess.get('documentReasonId').setValue('');
        this.resetDocumentReason();
      }
    });

    this.documentTypeService.findOne(id)
    .subscribe(res => {
      if (res.data) {
        this.inOutFlag = res.data.inoutFlag;
      }
    });
  }

  public onDocumentTypeChange(event) {
    if (this.formSaveInnerProcess.get('documentTypeId').value) {
      this.setLstDocumentReason(this.formSaveInnerProcess.get('documentTypeId').value);
    }
  }
  /**
   * setFormValue
   * param data
   */
  public setFormValue(propertyConfigs: any, data?: any) {
    this.propertyConfigs = propertyConfigs;
    if (data && data.workProcessId > 0) {
      this.buildFormInnerProcess(data, ACTION_FORM.EMP_INNER_PROCESS_UPDATE);
      this.buildFormOuterProcess(data, ACTION_FORM.EMP_OUTER_PROCESS_UPDATE);
    } else {
      this.buildFormInnerProcess(data, ACTION_FORM.EMP_INNER_PROCESS_INSERT);
      this.buildFormOuterProcess(data, ACTION_FORM.EMP_OUTER_PROCESS_INSERT);
    }
  }

  public setFormType(type) {
    this.formType = type;
  }

  public onChangeUnit(event) {
    this.orgService.checkAllowedAddEmployee(event.organizationId)
    .subscribe(result => {
      if (this.orgService.requestIsSuccess(result)) {
        return;
      } else {
        this.formSaveInnerProcess.controls['organizationId'].setValue('');
        this.unit.last.displayName.first.nativeElement.value = '';
      }
    });
  }

  public onChangeConcurrentUnit(event) {
    const temp = this.formSaveInnerProcess.controls['concurrentPositionId'].value;
    if (event.organizationId) {
      this.orgService.checkAllowedAddEmployee(event.organizationId)
      .subscribe(result => {
        if (this.orgService.requestIsSuccess(result)) {
          this.formSaveInnerProcess.removeControl('concurrentPositionId');
          const control = CommonUtils
            .createControl(this.actionForm, 'concurrentPositionId', temp, [ValidationService.required], this.propertyConfigs);
          this.formSaveInnerProcess.addControl('concurrentPositionId', control);
        } else {
          this.formSaveInnerProcess.controls['concurrentOrganizationId'].setValue('');
          this.concurrentUnit.last.displayName.first.nativeElement.value = '';
          this.formSaveInnerProcess.removeControl('concurrentPositionId');
          this.formSaveInnerProcess.addControl(
            'concurrentPositionId',
            CommonUtils.createControl(this.actionForm, 'concurrentPositionId', temp, [], this.propertyConfigs)
          );
        }
      });
    } else { // On remove concurrentUnit
      // Reset control + remove validate require when clear org-selector
      this.concurrentUnit.last.displayName.first.nativeElement.value = '';
      this.concurrentPosition.last.displayName.first.nativeElement.value = '';
      this.formSaveInnerProcess.removeControl('concurrentPositionId');
      this.formSaveInnerProcess.addControl(
        'concurrentPositionId',
        CommonUtils.createControl(this.actionForm, 'concurrentPositionId', temp, [], this.propertyConfigs)
      );
    }
  }

  public onChangeConcurrentPosition(event) {
    const temp = this.formSaveInnerProcess.controls['concurrentOrganizationId'].value;
    if (event.selectField) {
      // Replace concurrentUnit control on selected ConcurrentPosition
      if (!this.formSaveInnerProcess.controls['concurrentOrganizationId'].value) {
        this.formSaveInnerProcess.removeControl('concurrentOrganizationId');
        const control = CommonUtils
          .createControl(this.actionForm, 'concurrentOrganizationId', temp, [ValidationService.required], this.propertyConfigs);
        this.formSaveInnerProcess.addControl('concurrentOrganizationId', control);
      }
    } else { // On remove concurrentPosition
      this.formSaveInnerProcess.removeControl('concurrentOrganizationId');
      const control = CommonUtils
        .createControl(this.actionForm, 'concurrentOrganizationId', temp, [], this.propertyConfigs);
      this.formSaveInnerProcess.addControl('concurrentOrganizationId', control);
    }
  }

  private changePositionPickerFilter() {
    this.formSaveInnerProcess.removeControl('positionId');
    this.formSaveInnerProcess.addControl(
      'positionId',
      CommonUtils.createControl(this.actionForm, 'positionId', null, [ValidationService.required], this.propertyConfigs)
    );

    this.formSaveInnerProcess.removeControl('concurrentPositionId');
    this.formSaveInnerProcess.addControl(
      'concurrentPositionId',
      CommonUtils.createControl(this.actionForm, 'concurrentPositionId', null, null, this.propertyConfigs)
    );
    
    this.initPositionPickerFilter();
  }

  /**
   * Tao filter position theo thoi gian qua trinh cong tac
   */
  private initPositionPickerFilter() {
    this.positionPickerFilter = '';
    const sd = this.formSaveInnerProcess.get('effectiveDate').value || null;
    const ed = this.formSaveInnerProcess.get('expiredDate').value || 'null';
    if (sd) {
      const strSd = new Date(sd).getFullYear() + '-' + (new Date(sd).getMonth() + 1) + '-' + new Date(sd).getDate();
      const strEd = ed === 'null' ? 'null' :
        new Date(ed).getFullYear() + '-' + (new Date(ed).getMonth() + 1) + '-' + new Date(ed).getDate();

      // this.positionPickerFilter = ' and ( obj.effective_date <= IFNULL(' + strEd + ', obj.effective_date) '
      //   + 'and IFNULL(obj.expired_date, date(\'' + strSd + '\')) >= date(\'' + strSd + '\') ) ';
      if (strEd === 'null') {
        this.positionPickerFilter = ' and ( obj.effective_date <= date(\'' + strSd + '\') ) and obj.expired_date is null ';
      } else {
        this.positionPickerFilter = ' and ( obj.effective_date <= date(\'' + strSd + '\') ) '
          + ' and (obj.expired_date >= date(\'' + strEd + '\') or obj.expired_date is null )';
      }
    }
  }
}
