import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DEFAULT_MODAL_OPTIONS, RESOURCE, ACTION_FORM } from '@app/core/app-config';
import { AppComponent } from '@app/app.component';
import { WorkProcessService } from '@app/core';
import { WorkProcessFormComponent } from '../work-process-form/work-process-form.component';
import { EmployeeResolver } from '@app/shared/services/employee.resolver';
import { FormArray } from '@angular/forms';
import { FileStorageService } from '@app/core/services/file-storage.service';
import { saveAs } from 'file-saver';
import { ActivatedRoute } from '@angular/router';
import { WorkProcessBean } from '@app/core/models/work-process.model';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { TabView, TabPanel } from 'primeng/tabview';

@Component({
  selector: 'work-process-list',
  templateUrl: './work-process-list.component.html'
})
export class WorkProcessListComponent extends BaseComponent implements OnInit {
  processList: any = [];
  processGhrList: any = [];
  timeLineList: FormArray;
  emp: any;
  employeeId: number;
  empId: {employeeId: any};
  currentProcess: WorkProcessBean;
  previousProcess: WorkProcessBean;
  discontinuity: boolean;
  currentProcessGhr: WorkProcessBean;
  previousProcessGhr: WorkProcessBean;
  discontinuityGhr: boolean;
  @ViewChild('ptable') dataTable: any;
  @ViewChild('tabView') tabView: TabView;
  @ViewChild('tabPanel1') tabPanel1: TabPanel;
  @ViewChild('tabPanel2') tabPanel2: TabPanel;
  constructor(public actr: ActivatedRoute
            , private modalService: NgbModal
            , private app: AppComponent
            , private employeeResolver: EmployeeResolver
            , private workProcessService: WorkProcessService
            , private fileStorage: FileStorageService
            ) {
    super(actr, RESOURCE.WORK_PROCESS, ACTION_FORM.SEARCH);
    this.setMainService(workProcessService);
    this.employeeResolver.EMPLOYEE.subscribe(
      data => {
        if (data) {
            this.employeeId = data;
            this.empId = {employeeId: data};
            this.formSearch =  this.buildForm(this.empId, {employeeId: ['']});
            if(this.hasPermission('action.view')){
              this.getProcessList();
              this.processSearch();
            }  
        }
      }
    );
  }
  ngOnInit() {
  }

  private getProcessList() {
    this.workProcessService.getProcessList(this.employeeId).subscribe(resData => {
      if (resData.data && resData.data.length > 0) {
        resData.data.forEach(element => {
          if (element.ghrWorkProcessId) {
            this.processGhrList.push(element);
          } else {
            this.processList.push(element);
          }
        });
        if (this.processList.length == 0) this.tabView.closeTab(this.tabPanel1);
        if (this.processGhrList.length == 0) this.tabView.closeTab(this.tabPanel2);
        // Xu ly qua trinh trong nuoc
        this.currentProcess = this.processList[0];
        this.previousProcess = this.currentProcess;
        for (let i = 1; i < this.processList.length ; i++ ) {
          this.currentProcess = this.processList[i];
          if (CommonUtils.tctCompareDates(this.currentProcess.beforeStartDate, this.previousProcess.effectiveDate) !== 0) {
            this.discontinuity = false;
          } else {
            this.discontinuity = true;
          }
          this.previousProcess = this.currentProcess;
        }
        
        // Xu ly qua trinh ngoai nuoc
        this.currentProcessGhr = this.processGhrList[0];
        this.previousProcessGhr = this.currentProcessGhr;
        for (let i = 1; i < this.processGhrList.length ; i++ ) {
          this.currentProcessGhr = this.processGhrList[i];
          if (CommonUtils.tctCompareDates(this.currentProcessGhr.beforeStartDate, this.previousProcessGhr.effectiveDate) !== 0) {
            this.discontinuityGhr = false;
          } else {
            this.discontinuityGhr = true;
          }
          this.previousProcessGhr = this.currentProcessGhr;
        }
      }
    });
  }
  /**
   * prepareUpdate
   * param item
   */
  prepareSaveOrUpdate(item) {
    if (item && item.workProcessId > 0) {
      this.workProcessService.findOne(item.workProcessId)
        .subscribe(res => {
          this.activeModal(res.data);
        });
    } else {
      this.activeModal(this.empId);
    }
  }
  /**
   * show model
   * data
   */
  private activeModal(data?: any) {
    const modalRef = this.modalService.open(WorkProcessFormComponent, DEFAULT_MODAL_OPTIONS);
    // set mac dinh la qua trinh trong
    if (data) {
      modalRef.componentInstance.setFormValue(this.propertyConfigs, data);
      if (data.type) {
        modalRef.componentInstance.setFormType(data.type);
      }
    }
    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
      if (this.workProcessService.requestIsSuccess(result)) {
        this.employeeResolver.WORK_PROCESS.next(this.employeeId);
        this.getProcessList();
        this.processSearch();
      }
    });
  }

  /**
   * Xu ly download file trong danh sach
   */
  public downloadFile(fileData) {
    this.fileStorage.downloadFile(fileData.id).subscribe(res => {
        saveAs(res , fileData.fileName);
    });
  }

  public mouseEnter(div: string) {
    console.log("mouse enter : " + div);
  }

  public mouseLeave(div: string) {
    console.log('mouse leave :' + div);
  }

  /**
   * Xu ly xoa
   */
  public processDeletes(item): void {
    if (item && item.workProcessId > 0) {
      this.workProcessService.confirmDelete({
        messageCode: null,
        accept: () => {
          this.workProcessService.deleteById(item.workProcessId)
          .subscribe(res => {
            if (this.workProcessService.requestIsSuccess(res)) {
              this.employeeResolver.WORK_PROCESS.next(item.employeeId);
              this.getProcessList();
              this.processSearch();
            }
          });
        }
      });
    }
  }
}
