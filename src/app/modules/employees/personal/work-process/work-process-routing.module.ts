import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WorkProcessIndexComponent } from './page/work-process-index/work-process-index.component';
import { EmployeeResolver } from '@app/shared/services/employee.resolver';
import { PropertyResolver } from '@app/shared/services/property.resolver';
import { RESOURCE } from '@app/core';
import { CommonUtils } from '@app/shared/services';
const routes: Routes = [
  {
    path: '',
    component: WorkProcessIndexComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.WORK_PROCESS,
      nationId: CommonUtils.getNationId()
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [EmployeeResolver ]
})
export class WorkProcessRoutingModule { }
