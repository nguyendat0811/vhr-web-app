import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { APP_CONSTANTS, EmpEducationProcessService, SysCatService, RESOURCE, ACTION_FORM } from '@app/core';
import { CommonUtils, ValidationService } from '@app/shared/services';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from '@app/app.component';
import { FileControl } from '@app/core/models/file.control';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'interior-form',
  templateUrl: './interior-form.component.html'
})
export class InteriorFormComponent extends BaseComponent implements OnInit {

  formSave: FormGroup;
  ISSUED_PLACE_CODE = APP_CONSTANTS.SYS_CAT_TYPE_COCE.EDUCATION_UNIVER_CITY;
  SPECIALITY_CODE = APP_CONSTANTS.SYS_CAT_TYPE_COCE.EDUCATION_SPECIALITY;
  NATION_ID = CommonUtils.getNationId();
  MARKETCOMPANY_ID = CommonUtils.getCurrentMarketCompanyId();

  listEducationLevel = [];
  listEducationForm = [];
  listEducationRank = [];

  formConfig = {
    empEducationProcessId: [ null],
    educationForm: [ 1],
    degreeName: [ null, [ValidationService.required, ValidationService.maxLength(200)]],
    degreeNumber: [ null, [ValidationService.required, ValidationService.maxLength(50)]],
    isMainEduForm: [ null],
    educationGradeId: [ null, [ValidationService.required]],
    educationTypeId: [ null],
    courseName: [ null, [ValidationService.maxLength(200)]],
    className: [ null, [ValidationService.maxLength(200)]],
    issuedDate: [ null],
    issuedPlaceInterior: [ null, [ValidationService.maxLength(200)]],
    granduatedRankId: [ null],
    educationSubjectId: [ null],
    studyStartDate: [ null],
    studyEndDate: [ null],
    degreeKept: [ null],
    degreeSubmitDate: [ null],
    degreeReturnDate: [ null],
    empPayAmount: [ null, [ValidationService.number, Validators.min(0), Validators.max(9999999999999.99)]],
    viettelPayAmount: [ null, [ValidationService.number, Validators.min(0), Validators.max(9999999999999.99)]],
    totalCost: [null],
    description: [ null, [ ValidationService.maxLength(500)]],
    employeeId: [ null],
  };

  constructor(public actr: ActivatedRoute
    , public activeModal: NgbActiveModal
    , private empEducationProcessService: EmpEducationProcessService
    , private sysCatService: SysCatService
    , private app: AppComponent) {
    super(actr, RESOURCE.EMP_EDUCATION_PROCESS, ACTION_FORM.INTERIOR_INSERT);
    this.buildForms({}, ACTION_FORM.INTERIOR_INSERT);
    this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.EDUCATION_LEVEL).subscribe(
      res => this.listEducationLevel = res.data
    );
    this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.EDUCATION_TYPE).subscribe(
      res => this.listEducationForm = res.data
    );
    this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.EDUCATION_RANK).subscribe(
      res => this.listEducationRank = res.data
    );
  }

  ngOnInit() {
  }

  /**
   * processSaveOrUpdate
   */
  processSaveOrUpdate() {
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    } else {
      this.app.confirmMessage(null, () => {// on rejected
        this.empEducationProcessService.saveOrUpdateFormFile(this.formSave.value)
        .subscribe(res => {
          this.activeModal.close(res);
        });
      }, () => {// on rejected

      });
    }
  }

  /****************** CAC HAM COMMON DUNG CHUNG ****/
  get f () {
    return this.formSave.controls;
  }

  /**
  * buildForm
  */
  private buildForms(data?: any, actionForm?: ACTION_FORM): void {
    this.formSave = this.buildForm(data, this.formConfig
      , actionForm
      , [ ValidationService.notAffter('degreeSubmitDate', 'degreeReturnDate', 'app.empEducationProcess.degreeReturnDate')
      , ValidationService.notAffter('studyStartDate', 'studyEndDate', 'app.empEducationProcess.studyEndDate')]
    );
    this.formSave.addControl('fileAttachment', new FileControl(null));
    if (data && data.fileAttachment) {
      (this.formSave.controls['fileAttachment'] as FileControl).setFileAttachment(data.fileAttachment.file);
    }
    this.formSave.get('empPayAmount').valueChanges.subscribe(val => {
      if (data && (this.compareNums(data.empPayAmount, val) === 0)) {
        this.setValueTotal(data.totalCost);
      } else {
        this.setValueTotal();
      }
    });
    this.formSave.get('viettelPayAmount').valueChanges.subscribe(val => {
      if (data && (this.compareNums(data.viettelPayAmount, val) === 0)) {
        this.setValueTotal(data.totalCost);
      } else {
        this.setValueTotal();
      }
    });
  }

  public compareNums(num1, num2): number {
    const diff = num1 - num2;
   return (diff < 0) ? -1 : (diff === 0) ? 0 : 1;
  }

  public setValueTotal(data?: any) {
    const empPayAmount = parseFloat(this.formSave.get('empPayAmount').value) || 0;
    const viettelPayAmount = parseFloat(this.formSave.get('viettelPayAmount').value) || 0;
    if (!data) {
      const total = empPayAmount + viettelPayAmount;
      this.formSave.get('totalCost').setValue(total);
    } else {
      this.formSave.get('totalCost').setValue(data);
    }
  }

  public CurrencyFormat(amount?: Number) {

  }
  /**
   * setFormValue
   * param data
   */
  public setFormValue(propertyConfigs: any, data?: any) {
    this.propertyConfigs = propertyConfigs;
    if (data && data.labourContractTypeId > 0) {
      this.buildForms(data, ACTION_FORM.INTERIOR_UPDATE);
    } else {
      this.buildForms(data, ACTION_FORM.INTERIOR_INSERT);
    }
  }
}
