import { Component, OnInit, ViewChild } from '@angular/core';
import { ExteriorListComponent } from './exterior-list/exterior-list.component';
import { InteriorListComponent } from './interior-list/interior-list.component';

@Component({
  selector: 'education',
  templateUrl: './education.component.html'
})
export class EducationComponent implements OnInit {

  @ViewChild('exterior') exterior: ExteriorListComponent;
  @ViewChild('interior') interior: InteriorListComponent;
  constructor() {
  }

  ngOnInit() {
  }

  handleTabChange(e) {
    const index = e.index;
    if (index === 0) {
      this.exterior.processSearch();
    } else if (index === 1) {
      this.interior.processSearch();
    }
  }
}
