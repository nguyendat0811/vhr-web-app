import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DEFAULT_MODAL_OPTIONS, RESOURCE, ACTION_FORM } from '@app/core/app-config';
import { AppComponent } from '@app/app.component';
import { EmployeeResolver } from '@app/shared/services/employee.resolver';
import { EmpEducationProcessService } from '@app/core';
import { FileStorageService } from '@app/core/services/file-storage.service';
import { InteriorFormComponent } from '../interior-form/interior-form.component';
import { saveAs } from 'file-saver';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'interior-list',
  templateUrl: './interior-list.component.html'
})
export class InteriorListComponent extends BaseComponent implements OnInit {

  resultList: any = {};
  employeeId: number;
  constructor(private modalService: NgbModal
    , private empEducationProcessService: EmpEducationProcessService
    , private employeeResolver: EmployeeResolver
    , private fileStorage: FileStorageService
    , public actr: ActivatedRoute) {
    super(actr, RESOURCE.EMP_EDUCATION_PROCESS, ACTION_FORM.INTERIOR_SEARCH);
    this.setMainService(empEducationProcessService);
    this.employeeResolver.EMPLOYEE.subscribe(
      id => {
        if (id) {
          this.employeeId = id;
          this.formSearch =  this.buildForm({employeeId: id, educationForm: 1}
                        , {employeeId: [this.employeeId], educationForm: 1}
                        , ACTION_FORM.INTERIOR_SEARCH);
        }
      }
    );
  }

  ngOnInit() {
  }

  /**
  * prepareUpdate
  * param item
  */
  prepareSaveOrUpdate(item) {
    if (item && item.empEducationProcessId > 0) {
      this.empEducationProcessService.findOne(item.empEducationProcessId)
        .subscribe(res => {
          this.activeModel(res.data);
      });
    } else {
      const defaultData = {employeeId : this.employeeId, educationForm: 1};
      this.activeModel(defaultData);
    }
  }

  /* Action Modal*/
  private activeModel(data: any) {
    const modalRef = this.modalService.open(InteriorFormComponent, DEFAULT_MODAL_OPTIONS);
    if (data) {
      modalRef.componentInstance.setFormValue(this.propertyConfigs, data);
    }
    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
      if (this.empEducationProcessService.requestIsSuccess(result)) {
        this.employeeResolver.EDUCATION_PROCESS.next(this.employeeId);
        this.processSearch();
      }
    });
  }
  /**
   * Xu ly download file trong danh sach
   */
  public downloadFile(fileData) {
    this.fileStorage.downloadFile(fileData.id).subscribe(res => {
      saveAs(res , fileData.fileName);
    });
  }

  /**
   * Xu ly xoa
   */
  public processDeletes(item): void {
    console.log('this.employeeId:' , this.employeeId);
    if (item && item.empEducationProcessId > 0) {
      this.empEducationProcessService.confirmDelete({
        messageCode: null,
        accept: () => {
          this.empEducationProcessService.deleteById(item.empEducationProcessId)
          .subscribe(res => {
            if (this.empEducationProcessService.requestIsSuccess(res)) {
              this.employeeResolver.EDUCATION_PROCESS.next(this.employeeId);
              this.processSearch();
            }
          });
        }
      });
    }
  }
}
