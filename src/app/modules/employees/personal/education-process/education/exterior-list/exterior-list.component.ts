import { ExteriorFormComponent } from './../exterior-form/exterior-form.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DEFAULT_MODAL_OPTIONS, RESOURCE, ACTION_FORM } from '@app/core/app-config';
import { EmployeeResolver } from '@app/shared/services/employee.resolver';
import { EmpEducationProcessService } from '@app/core/services';
import { FileStorageService } from '@app/core/services/file-storage.service';
import { saveAs } from 'file-saver';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { CommonUtils } from '@app/shared/services';

@Component({
  selector: 'exterior-list',
  templateUrl: './exterior-list.component.html'
})
export class ExteriorListComponent extends BaseComponent implements OnInit {

  resultList: any = {};
  employeeId: number;
  constructor(private modalService: NgbModal
    , private empEducationProcessService: EmpEducationProcessService
    , private employeeResolver: EmployeeResolver
    , private fileStorage: FileStorageService
    , public actr: ActivatedRoute) {

      super(actr, RESOURCE.EMP_EDUCATION_PROCESS, ACTION_FORM.EXTERIOR_SEARCH);
      this.setMainService(empEducationProcessService);
      this.employeeResolver.EMPLOYEE.subscribe(
        id => {
          if (id) {
            this.employeeId = id;
            this.formSearch =  this.buildForm({employeeId: id, educationForm: 2}
                                            , {employeeId: [this.employeeId], educationForm: 2}
                                            , ACTION_FORM.EXTERIOR_SEARCH);
            this.processSearch();
          }
        }
      );
  }

  ngOnInit() {
  }

  /**
  * prepareUpdate
  * param item
  */
  prepareSaveOrUpdate(item) {
    if (item && item.empEducationProcessId > 0) {
      this.empEducationProcessService.findOne(item.empEducationProcessId)
        .subscribe(res => {
          this.activeModel(res.data);
      });
    } else {
      const defaultData = {employeeId : this.employeeId, educationForm: 2};
      this.activeModel(defaultData);
    }
  }

  /* Action Modal*/
  private activeModel(data: any) {
    const modalRef = this.modalService.open(ExteriorFormComponent, DEFAULT_MODAL_OPTIONS);
    if (data) {
      modalRef.componentInstance.setFormValue(this.propertyConfigs, data);
    }
    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
      if (this.empEducationProcessService.requestIsSuccess(result)) {
        this.employeeResolver.EDUCATION_PROCESS.next(this.employeeId);
        this.processSearch();
      }
    });
  }

  /**
   * Xu ly download file trong danh sach
   */
  public downloadFile(fileData) {
    this.fileStorage.downloadFile(fileData.id).subscribe(res => {
      saveAs(res , fileData.fileName);
    });
  }
  /**
   * Xu ly xoa
   */
  public processDeletes(item): void {
    if (item && item.empEducationProcessId > 0) {
      this.empEducationProcessService.confirmDelete({
        messageCode: null,
        accept: () => {
          this.empEducationProcessService.deleteById(item.empEducationProcessId)
          .subscribe(res => {
            this.employeeResolver.EDUCATION_PROCESS.next(this.employeeId);
            if (this.empEducationProcessService.requestIsSuccess(res)) {
              this.processSearch();
            }
          });
        }
      });
    }
  }
}
