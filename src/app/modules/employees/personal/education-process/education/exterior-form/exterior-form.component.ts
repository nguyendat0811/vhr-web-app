import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup , Validators } from '@angular/forms';
import { EmpEducationProcessService, SysCatService, LocationService } from '@app/core/services';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { AppComponent } from '@app/app.component';
import { ValidationService } from '@app/shared/services/validation.service';
import { APP_CONSTANTS, ACTION_FORM, RESOURCE } from '@app/core';
import { FileControl } from '@app/core/models/file.control';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'exterior-form',
  templateUrl: './exterior-form.component.html'
})
export class ExteriorFormComponent extends BaseComponent implements OnInit {

  formSave: FormGroup;
  ISSUED_PLACE_CODE = APP_CONSTANTS.SYS_CAT_TYPE_COCE.EDUCATION_UNIVER_CITY;
  SPECIALITY_CODE = APP_CONSTANTS.SYS_CAT_TYPE_COCE.EDUCATION_SPECIALITY;
  CONSTANTS = APP_CONSTANTS;
  NATION_ID = CommonUtils.getNationId();
  MARKETCOMPANY_ID = CommonUtils.getCurrentMarketCompanyId();

  listEducationLevel = [];
  listEducationForm = [];
  listEducationRank = [];

  formConfig = {
    empEducationProcessId: [ null],
    educationForm: [ 2],
    degreeName: [ null, [ValidationService.required, ValidationService.maxLength(200)]],
    degreeNumber: [ null, [ValidationService.required, ValidationService.maxLength(50)]],
    isMainEduForm: [ null],
    educationGradeId: [ null, [ValidationService.required]],
    educationTypeId: [ null],
    issuedDate: [ null],
    issuedPlaceId: [ null, [ValidationService.required]],
    granduatedRankId: [ null, [ValidationService.required]],
    educationSubjectId: [ null, [ValidationService.required]],
    locationId: [ null],
    entrancePoint: [ null, [ValidationService.number, Validators.min(0.01), Validators.max(999.99)]],
    graduationThese: [ null, [ValidationService.maxLength(200)]],
    granduationPoint: [ null, [ValidationService.number, Validators.min(0.01), Validators.max(999.99)]],
    degreeKept: [ null],
    degreeSubmitDate: [ null],
    degreeReturnDate: [ null],
    description: [ null, [ ValidationService.maxLength(500)]],
    employeeId: [ null],
    studyStartDate: [ null],
    studyEndDate: [ null],
  };
  constructor(public actr: ActivatedRoute
    , public activeModal: NgbActiveModal
    , private empEducationProcessService: EmpEducationProcessService
    , private sysCatService: SysCatService
    , private app: AppComponent
    , private locationService: LocationService) {
    super(actr, RESOURCE.EMP_EDUCATION_PROCESS, ACTION_FORM.EXTERIOR_INSERT);
    this.buildForms({}, ACTION_FORM.EXTERIOR_INSERT);
    this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.EDUCATION_LEVEL).subscribe(
      res => this.listEducationLevel = res.data
    );
    this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.EDUCATION_TYPE).subscribe(
      res => this.listEducationForm = res.data
    );
    this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.EDUCATION_RANK).subscribe(
      res => this.listEducationRank = res.data
    );
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
  }

  /**
   * processSaveOrUpdate
   */
  processSaveOrUpdate() {
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    } else {
      this.app.confirmMessage(null, () => {// on rejected
        this.empEducationProcessService.saveOrUpdateFormFile(this.formSave.value)
        .subscribe(res => {
          this.activeModal.close(res);
        });
      }, () => {// on rejected

      });
    }
  }

  /****************** CAC HAM COMMON DUNG CHUNG ****/
  get f () {
    return this.formSave.controls;
  }

  /**
  * buildForm
  */
  private buildForms(data?: any, actionForm?: ACTION_FORM): void {
    this.formSave = this.buildForm(data, this.formConfig
      , actionForm
      , [ ValidationService.notAffter('degreeSubmitDate', 'degreeReturnDate', 'app.empEducationProcess.degreeReturnDate')
      , ValidationService.notAffter('studyStartDate', 'studyEndDate', 'app.empEducationProcess.studyEndDate')]
    );
    this.formSave.addControl('fileAttachment', new FileControl(null));
    if (data && data.fileAttachment) {
      (this.formSave.controls['fileAttachment'] as FileControl).setFileAttachment(data.fileAttachment.file);
    }
  }

  /**
   * setFormValue
   * param data
   */
  public setFormValue(propertyConfigs: any, data?: any) {
    this.propertyConfigs = propertyConfigs;
    if (data && data.labourContractTypeId > 0) {
      this.buildForms(data, ACTION_FORM.EXTERIOR_UPDATE);
    } else {
      this.buildForms(data, ACTION_FORM.EXTERIOR_INSERT);
    }
  }
}
