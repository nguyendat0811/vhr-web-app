import { RESOURCE } from './../../../../../core/app-config';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { FormArray, FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Component, OnInit, OnDestroy} from '@angular/core';
import { AppComponent } from '@app/app.component';
import { APP_CONSTANTS, ACTION_FORM, SysCatService } from '@app/core';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { EmpPoliticalDegreeService } from '@app/core/services/hr-employee/emp-political-degree.service';
import { EmployeeResolver } from '@app/shared/services/employee.resolver';
import { ValidationService } from '@app/shared/services';
@Component({
  selector: 'political',
  templateUrl: './political.component.html'
})
export class PoliticalComponent extends BaseComponent implements OnInit, OnDestroy {

  formSave: FormArray;
  listLevel: any;
  empId: number;
  emp: {employeeId: any};
  constructor(private formBuilder: FormBuilder
    , private app: AppComponent
    , private empPoliticalDegreeService: EmpPoliticalDegreeService
    , private employeeResolver: EmployeeResolver
    , private sysCatService: SysCatService
    , public actr: ActivatedRoute) {
      super(actr, RESOURCE.EMP_EDUCATION_PROCESS, ACTION_FORM.POLITICAL_DEGREE_INSERT);
    this.buildFormPoliticalDegree();

    // this.listLevel = APP_CONSTANTS.SyscatType.LEVEL;
    this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.LEVEL).subscribe(
      res => this.listLevel = res.data
    );

    this.employeeResolver.EMPLOYEE.subscribe(
      data => {
        this.empId = data;
        this.emp = {employeeId: data};
        if (data) {
          this.empPoliticalDegreeService.findByEmployeeId(data).subscribe(
            (res) => {
              if (res) {
                this.buildFormPoliticalDegree(res);
              }
            }
          );
        }
      }
    );
  }

  ngOnInit() {
  }

  ngOnDestroy() {
  }

    /**
   * makeDefaultPoliticalDegreeForm
   */
  private makeDefaultPoliticalDegreeForm(): FormGroup {
    const group = {
      empPoliticalDegreeId: [''],
      employeeId: [this.empId],
      politicalDegreeType: ['', [ValidationService.required]],
      degreeIssuedDate: ['', [ValidationService.required]],
      degreeIssuedOrg: ['', [ValidationService.required, ValidationService.maxLength(200)]],
      isActive: [''],
    };
    return this.buildForm({}, group);
  }

  private buildFormPoliticalDegree(listEmpPoliticalDegree?: any) {
    const controls = new FormArray([]);
    if (!listEmpPoliticalDegree) {
      this.formSave = new FormArray([]);
      const group = this.makeDefaultPoliticalDegreeForm();
      controls.push(group);
    } else {
      for (const i in listEmpPoliticalDegree) {
        const empPoliticalDegree = listEmpPoliticalDegree[i];
        const group = this.makeDefaultPoliticalDegreeForm();
        group.patchValue(empPoliticalDegree);
        group.patchValue(this.emp);
        controls.push(group);
      }
    }
    this.formSave = controls;
  }

  private validateBeforeSave(): boolean {
    return CommonUtils.isValidForm(this.formSave);
  }

  processSaveOrUpdate() {
     if (!this.validateBeforeSave()) {
       return;
     }
    this.app.confirmMessage(null, () => {// on accepted
       this.empPoliticalDegreeService.saveListEmpPoliticalDegree(this.formSave.value, this.empId)
        .subscribe(res => {
            this.empPoliticalDegreeService.findByEmployeeId(this.empId).subscribe(
            (data) => {
            this.buildFormPoliticalDegree(data);
            }
          );
         });
    }, () => {
      // on rejected
    });
  }

    /**
   * addPoliticalInfo
   * param index
   * param item
   */
  public addPoliticalInfo(index: number, item: FormGroup) {
    const controls = this.formSave as FormArray;
    controls.insert(index + 1, this.makeDefaultPoliticalDegreeForm());
  }
  /**
   * removePoliticalInfo
   * param index
   * param item
   */
  public removePoliticalInfo(index: number, item: any) {
    if (item && item > 0) {
      const controls = this.formSave as FormArray;
      console.log(1500);
      this.empPoliticalDegreeService.confirmDelete({
        messageCode: null,
        accept: () => {
          this.empPoliticalDegreeService.deleteById(item)
          .subscribe(res => {
            if (this.empPoliticalDegreeService.requestIsSuccess(res)) {
              // this.empPoliticalDegreeService.findByEmployeeId(this.empId)
              // .subscribe(result => {
              //   this.buildFormPoliticalDegree(result);
              // });
              controls.removeAt(index);
            }
          });
        }
      });
    } else {
      const controls = this.formSave as FormArray;
      if (controls.length === 1) {
        this.formSave.reset();
        const group = this.makeDefaultPoliticalDegreeForm();
        const control = new FormArray([]);
        control.push(group);
        this.formSave = control;
        return;
      }
      controls.removeAt(index);
    }
  }

  /**
   * Xu ly check box ton tai duy nhat
   */
  checkOnly(e, index, formControlName) {
    if (e.target.checked) {
      const controls = this.formSave as FormArray;
      for (let i = 0; i < controls.length; i++) {
        if (index !== i) {
          controls.controls[i].get(formControlName).setValue(false);
        }
      }
    }
  }
}
