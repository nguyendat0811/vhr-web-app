import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EducationProcessRoutingModule } from './education-process-routing.module';
import { IndexComponent } from './index/index.component';
import { EducationComponent } from './education/education.component';
import { LanguageComponent } from './language/language.component';
import { ItComponent } from './it/it.component';
import { PoliticalComponent } from './political/political.component';
import { SharedModule } from '@app/shared';
import { ExteriorListComponent } from './education/exterior-list/exterior-list.component';
import { ExteriorFormComponent } from './education/exterior-form/exterior-form.component';
import { InteriorFormComponent } from './education/interior-form/interior-form.component';
import { InteriorListComponent } from './education/interior-list/interior-list.component';

@NgModule({
  declarations: [IndexComponent, EducationComponent, LanguageComponent, ItComponent,
    PoliticalComponent, ExteriorListComponent, ExteriorFormComponent, InteriorFormComponent, InteriorListComponent],
  imports: [
    CommonModule,
    EducationProcessRoutingModule,
    SharedModule
  ],
  entryComponents: [ExteriorFormComponent, InteriorFormComponent]
})
export class EducationProcessModule { }
