import { RESOURCE } from './../../../../core/app-config';
import { PropertyResolver } from '@app/shared/services/property.resolver';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './index/index.component';
import { CommonUtils } from '@app/shared/services';
import { LanguageComponent } from './language/language.component';

const routes: Routes = [
  {
    path: '',
    component: IndexComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.EMP_EDUCATION_PROCESS,
      nationId: CommonUtils.getNationId()
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EducationProcessRoutingModule { }
