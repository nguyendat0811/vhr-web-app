import { RESOURCE } from './../../../../../core/app-config';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from './../../../../../shared/components/base-component/base-component.component';
import { Component, OnInit, Input } from '@angular/core';
import { FormArray, Validators, FormGroup } from '@angular/forms';

import { LanguageDegreeService, SysCatService } from '@app/core/services';
import { CommonUtils, ValidationService } from '@app/shared/services';
import { AppComponent } from '@app/app.component';
import { APP_CONSTANTS, ACTION_FORM } from '@app/core';
import { EmployeeResolver } from '@app/shared/services/employee.resolver';


@Component({
  selector: 'language',
  templateUrl: './language.component.html'
})
export class LanguageComponent extends BaseComponent implements OnInit {
  formSave: FormArray;
  empId: number;
  emp: { employeeId: any };
  listCheck = [];
  listLanguageDegree = [];

  listLanguage = [];
  listLanguageType = [];
  listIdDelete = [];

  @Input()
  employeeId: number;

  constructor(private languageDegreeService: LanguageDegreeService
    , private app: AppComponent
    , private sysCatService: SysCatService
    , private employeeResolver: EmployeeResolver
    , public actr: ActivatedRoute) {
      super(actr, RESOURCE.EMP_EDUCATION_PROCESS, ACTION_FORM.LANGUAGE_INFORMATION_INSERT);
    // Build form mac dinh
    this.buildFormLanguageDegree();

    this.employeeResolver.EMPLOYEE.subscribe(
      data => {
        if (data) {
          this.employeeId = data;
          this.languageDegreeService.findByEmployeeId(data).subscribe(
            (res) => {
              if (res) {
                this.buildFormLanguageDegree(res);
              }
            }
          );
        }
      }
    );

    // Lay danh ngon ngu
    this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.LANGUAGE).subscribe(
      res => this.listLanguage = res.data
    );

    // Lay danh sach loai chung chi
    this.listLanguageType = APP_CONSTANTS.EDU_LANGUAGE_TYPE;
  }

  ngOnInit() {
  }

  /**
   * makeDefaultLanguageDegreeForm
   */
  private makeDefaultLanguageDegreeForm(): FormGroup {
    const group = {
      employeeId: [null],
      empLanguageDegreeId: [''],
      languageDegreeId: [null, ValidationService.required],
      languageDegreeType: [null, ValidationService.required],
      degreeName: [null, ValidationService.maxLength(50)],
      degreeIssuedDate: [null],
      degreeIssuedPlace: [null, ValidationService.maxLength(255)],
      resuilt: [null, ValidationService.maxLength(50)],
      isMainLanguage: [null],
    };
    return this.buildForm({}, group);
  }

  private buildFormLanguageDegree(listLanguageDegree?: any) {
    const controls = new FormArray([]);
    if (!listLanguageDegree) {
      // this.formSave = new FormArray([]);
      const group = this.makeDefaultLanguageDegreeForm();
      controls.push(group);
    } else {
    // if (listLanguageDegree) {
      for (const i in listLanguageDegree) {
        const languageDegree = listLanguageDegree[i];
        const group = this.makeDefaultLanguageDegreeForm();
        group.patchValue(languageDegree);
        controls.push(group);
      }
    }
    this.formSave = controls;
  }

  private validateBeforeSave(): boolean {
    return CommonUtils.isValidForm(this.formSave);
  }

  /**
 * setFormValue
 * param data
 */
  public setFormValue(data: any) {
    this.formSave.setValue(CommonUtils.copyProperties(this.formSave.value, data));
  }

  /**
   * processSaveOrUpdate
   */
  processSaveOrUpdate() {
    if (!this.validateBeforeSave()) {
      return;
    }
    if (!this.validateIsMainLanguage(this.formSave.value) === true) {
      return;
    }
    const submitData = {};
    submitData['employeeId'] = this.employeeId;
    submitData['listForm'] = this.formSave.value;
    submitData['listIdDelete'] = this.listIdDelete;
    this.app.confirmMessage(null, () => {// on accepted
      this.languageDegreeService.saveOrUpdate(submitData)
        .subscribe(res => {
          this.languageDegreeService.findByEmployeeId(this.employeeId).subscribe(
            (data) => {
            this.employeeResolver.LANGUAGE.next(this.employeeId);
            this.buildFormLanguageDegree(data);
            }
          );
        });
    }, () => {
      // on rejected
    });
  }

  /**
   * addLanguageDegreeInfo
   * param index
   * param item
   */
  public addLanguageDegreeInfo(index: number, item: FormGroup) {
    const controls = this.formSave as FormArray;
    controls.insert(index + 1, this.makeDefaultLanguageDegreeForm());
  }

  /**
   * removeLanguageDegreeInfo
   * param index
   * param item
   */
  public removeLanguageDegreeInfo(index: number, item: any) {
    if (item && item > 0) {
      const controls = this.formSave as FormArray;
      console.log(1500);
      this.languageDegreeService.confirmDelete({
        messageCode: null,
        accept: () => {
          this.languageDegreeService.deleteById(item)
          .subscribe(res => {
            if (this.languageDegreeService.requestIsSuccess(res)) {
              this.employeeResolver.LANGUAGE.next(this.employeeId);
              // this.languageDegreeService.findByEmployeeId(this.employeeId)
              // .subscribe(result => {
              //   console.log('result: ', result)
              //   this.buildFormLanguageDegree(result);
              // });
              controls.removeAt(index);
            }
          });
        }
      });
    } else {
      const controls = this.formSave as FormArray;
      if (controls.length === 1) {
        this.formSave.reset();
        const group = this.makeDefaultLanguageDegreeForm();
        const control2 = new FormArray([]);
        control2.push(group);
        this.formSave = control2;
        return;
      }
    controls.removeAt(index);
    }
  }

  checkOnly(e, index, formControlName) {
    if (e.target.checked) {
      const controls = this.formSave as FormArray;
      for (let i = 0; i < controls.length; i++) {
        if (index !== i) {
          controls.controls[i].get(formControlName).setValue(false);
        }
      }
    }
  }

  private validateIsMainLanguage(list?: any): boolean {
    this.listCheck = new Array;
    for (let i = 0; i < list.length; i++) {
      let mainI = list[i].isMainLanguage;
      if (mainI === 1) { mainI = true; }
      if (mainI === true) {
        this.listCheck.push(mainI);
      }
    }
    if (Array.isArray(this.listCheck) && this.listCheck.length) {
      return true;
    } else {
      this.app.warningMessage('languageDeggree', 'mainLanguage');
      return false;
    }
  }
}
