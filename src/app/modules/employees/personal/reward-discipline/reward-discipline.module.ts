import { DisciplineFormComponent } from './page/discipline-form/discipline-form.component';
import { DisciplineListComponent } from './page/discipline-list/discipline-list.component';
import { RewardProcessListComponent } from './page/reward-process-list/reward-process-list.component';
import { SharedModule } from './../../../../shared/shared.module';
import { RewardDisciplineIndexComponent } from './page/reward-discipline-index/reward-discipline-index.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RewardDisciplineRoutingModule } from './reward-discipline-routing.module';
import { RewardProcessFormComponent } from './page/reward-process-form/reward-process-form.component';

@NgModule({

  declarations: [RewardDisciplineIndexComponent
                , RewardProcessListComponent
                , DisciplineListComponent
                , RewardProcessFormComponent
                , DisciplineFormComponent],

  imports: [
    CommonModule,
    SharedModule,
    RewardDisciplineRoutingModule
  ],
  entryComponents: [RewardProcessFormComponent, DisciplineFormComponent]
})
export class RewardDisciplineModule { }
