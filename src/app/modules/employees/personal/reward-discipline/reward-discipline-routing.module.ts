import { CommonUtils } from '@app/shared/services/common-utils.service';
import { RESOURCE } from '@app/core';
import { RewardDisciplineIndexComponent } from './page/reward-discipline-index/reward-discipline-index.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PropertyResolver } from '@app/shared/services/property.resolver';


const routes: Routes = [
  {
    path: '',
    component: RewardDisciplineIndexComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.REWARD_DISCIPLINE,
      nationId: CommonUtils.getNationId()
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RewardDisciplineRoutingModule { }
