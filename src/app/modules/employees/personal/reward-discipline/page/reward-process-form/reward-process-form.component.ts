import { EmpRewardProcessService } from '@app/core/services/reward-process/reward-process.service';
import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder , Validators } from '@angular/forms';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { AppComponent } from '@app/app.component';
import { ValidationService } from '@app/shared/services/validation.service';
import { FileControl } from '@app/core/models/file.control';
import { SysCatService, APP_CONSTANTS, RESOURCE, ACTION_FORM } from '@app/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'reward-process-form',
  templateUrl: './reward-process-form.component.html'
})
export class RewardProcessFormComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  rewardFormList: any;
  rewardHonorList: any;
  rewardTypeList: any;
  formConfig = {
    empRewardProcessId: [ null],
    employeeId: [ null],
    organizationId: [ null , [ValidationService.required]],
    decissionNumber: [ null, [ ValidationService.maxLength(200), ValidationService.required]],
    signedDate: [ null, [ValidationService.required]],
    effectiveDate: [ null, [ValidationService.required]],
    rewardTypeId: [ null, [ValidationService.required]],
    reason: [ null, [ ValidationService.maxLength(1000)]],
    amountOfMoney: [ null, [ ValidationService.positiveNumber, ValidationService.required, Validators.min(0), Validators.max(9999999999999.99)]],
    rewardFormId: [ null],
    rewardTitleId: [ null],
  };
  constructor(public actr: ActivatedRoute
            , public activeModal: NgbActiveModal
            , private empRewardProcessService: EmpRewardProcessService
            , private app: AppComponent
            , private sysCatService: SysCatService) {
    super(actr, RESOURCE.REWARD_DISCIPLINE, ACTION_FORM.REWARD_PROCESS_INSERT);
    this.formSave = this.buildForm({}, this.formConfig
      , ACTION_FORM.REWARD_PROCESS_INSERT
      , [ValidationService.notAffter('signedDate', 'effectiveDate', 'app.contractProcess.effectiveDate')]);
    this.formSave.addControl('file', new FileControl(null));
    this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.REWARD_FORM).subscribe(res => {
      this.rewardFormList = res.data;
    });
    this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.REWARD_HONOR).subscribe(res => {
      this.rewardHonorList = res.data;
    });
    this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.REWARD_TYPE).subscribe(res => {
      this.rewardTypeList = res.data;
    });
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
  }

  /**
   * processSaveOrUpdate
   */
  processSaveOrUpdate() {
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    } else {
      this.app.confirmMessage(null, () => {// on rejected
        this.empRewardProcessService.saveOrUpdateFormFile(this.formSave.value)
        .subscribe(res => {
          if (this.empRewardProcessService.requestIsSuccess(res)) {
            this.activeModal.close(res);
          }
        });
      }, () => {// on rejected

      });
    }
  }

  /****************** CAC HAM COMMON DUNG CHUNG ****/
  get f () {
    return this.formSave.controls;
  }

  /**
   * setFormValue
   * param data
   */
  public setFormValue(propertyConfigs: any, data: any) {
    this.propertyConfigs = propertyConfigs;
    if (data && data.empRewardProcessId > 0) {
      this.formSave = this.buildForm(data, this.formConfig
        , ACTION_FORM.REWARD_PROCESS_UPDATE
        , [ValidationService.notAffter('signedDate', 'effectiveDate', 'app.contractProcess.effectiveDate')]);
    } else {
      this.formSave = this.buildForm(data, this.formConfig
        , ACTION_FORM.REWARD_PROCESS_INSERT
        , [ValidationService.notAffter('signedDate', 'effectiveDate', 'app.contractProcess.effectiveDate')]);
    }
    this.formSave.addControl('file', new FileControl(null));
    if (data.fileAttachment) {
      (this.formSave.controls['file'] as FileControl).setFileAttachment(data.fileAttachment.file);
    }
  }
}
