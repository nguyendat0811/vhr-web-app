import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { RESOURCE } from '@app/core';
@Component({
  selector: 'reward-discipline-index',
  templateUrl: './reward-discipline-index.component.html'
})
export class RewardDisciplineIndexComponent extends BaseComponent implements OnInit {

  tabIndex = 0; // tab index
  constructor(public act: ActivatedRoute) { 
    super(act, RESOURCE.REWARD_DISCIPLINE);
  }


  ngOnInit() {
  }

}
