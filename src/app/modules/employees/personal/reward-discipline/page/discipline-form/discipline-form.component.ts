import { RESOURCE } from '@app/core/app-config';
import { APP_CONSTANTS, ACTION_FORM } from './../../../../../../core/app-config';
import { ValidationService } from '@app/shared/services/validation.service';
import { EmpDisciplineService } from './../../../../../../core/services/discipline/emp-discipline.service';
import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup , Validators } from '@angular/forms';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { AppComponent } from '@app/app.component';
import { FileControl } from '@app/core/models/file.control';
import { SysCatService } from '@app/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'discipline-form',
  templateUrl: './discipline-form.component.html'
})
export class DisciplineFormComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  disciplineFormList: any;
  partyDisciplineFormList: any;
  disciplineTypeList: any;
  disciplineLevelList: any;
  formConfig = {
    empDisciplineId: [ null],
    employeeId: [ null],
    organizationId: [ null, [ValidationService.required]],
    decissionNumber: [ null, [ValidationService.required, ValidationService.maxLength(200)]],
    decissionLevel: [ null, [ValidationService.required, ValidationService.maxLength(200)]],
    signedDate: [ null, [ ValidationService.required]],
    effectiveDate: [ null, [ ValidationService.required]],
    expiredDate: [ null, []],
    disciplineTypeId: [ null, [ValidationService.required]],
    reason: [ null, [ ValidationService.required, ValidationService.maxLength(100)]],
    disciplineFormId: [ null],
    partyDisciplineFormId: [ null],
  };
  constructor(public actr: ActivatedRoute
            , public activeModal: NgbActiveModal
            , private empDisciplineService: EmpDisciplineService
            , private app: AppComponent
            , private sysCatService: SysCatService) {
    super(actr, RESOURCE.REWARD_DISCIPLINE, ACTION_FORM.DISCIPLINE_INSERT);
    this.formSave = this.buildForm({}, this.formConfig
      , ACTION_FORM.DISCIPLINE_INSERT
      , [ ValidationService.notAffter('signedDate', 'effectiveDate', 'app.contractProcess.effectiveDate')
         , ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.empSalaryProcess.expiredDate')]);
    this.formSave.addControl('file', new FileControl(null));

    this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.DISCIPLINE_FORM).subscribe(res => {
      this.disciplineFormList = res.data;
    });
    this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.PARTY_DISCIPLINE_FORM).subscribe(res => {
      this.partyDisciplineFormList = res.data;
    });
    this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.DISCIPLINE_TYPE).subscribe(res => {
      this.disciplineTypeList = res.data;
    });
    this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.DISCIPLINE_LEVEL).subscribe(res => {
      this.disciplineLevelList = res.data;
    });
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
  }

  /**
   * processSaveOrUpdate
   */
  processSaveOrUpdate() {
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    } else {
      console.log(this.formSave);
      this.app.confirmMessage(null, () => {// on rejected
        this.empDisciplineService.saveOrUpdateFormFile(this.formSave.value)
        .subscribe(res => {
          if (this.empDisciplineService.requestIsSuccess(res)) {
            this.activeModal.close(res);
          }
        });
      }, () => {// on rejected

      });
    }
  }

  /****************** CAC HAM COMMON DUNG CHUNG ****/
  get f () {
    return this.formSave.controls;
  }

  /**
   * setFormValue
   * param data
   */
  public setFormValue(propertyConfigs: any, data?: any) {
    this.propertyConfigs = propertyConfigs;
    if (data && data.empDisciplineId > 0) {
      this.formSave = this.buildForm(data, this.formConfig
        , ACTION_FORM.DISCIPLINE_UPDATE
        , [ValidationService.notAffter('signedDate', 'effectiveDate', 'app.contractProcess.effectiveDate')
          , ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.contractProcess.expiredDate')]);
    } else {
      this.formSave = this.buildForm(data, this.formConfig
        , ACTION_FORM.DISCIPLINE_INSERT
        , [ValidationService.notAffter('signedDate', 'effectiveDate', 'app.contractProcess.effectiveDate')
          , ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.contractProcess.expiredDate')]);
    }
    this.formSave.addControl('file', new FileControl(null));
    if (data.fileAttachment) {
      (this.formSave.controls['file'] as FileControl).setFileAttachment(data.fileAttachment.file);
    }
  }
}
