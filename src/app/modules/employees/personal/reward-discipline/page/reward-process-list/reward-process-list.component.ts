import { AppComponent } from '@app/app.component';
import { FileStorageService } from '@app/core/services/file-storage.service';
import { RewardProcessFormComponent } from './../reward-process-form/reward-process-form.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { EmpRewardProcessService } from '@app/core/services/reward-process/reward-process.service';
import { EmployeeResolver } from '@app/shared/services/employee.resolver';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DEFAULT_MODAL_OPTIONS, RESOURCE, ACTION_FORM } from '@app/core/app-config';
import { saveAs } from 'file-saver';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';

@Component({
  selector: 'reward-process-list',
  templateUrl: './reward-process-list.component.html'
})
export class RewardProcessListComponent extends BaseComponent implements OnInit {
  resultList: any = {};
  empId: {employeeId: any};
  emp: any;
  employeeId: number;
  @ViewChild('ptable') dataTable: any;
  constructor(private modalService: NgbModal
            , private empRewardProcessService: EmpRewardProcessService
            , private employeeResolver: EmployeeResolver
            , private fileStorage: FileStorageService
            , public actr: ActivatedRoute
            ) {
    super(actr, RESOURCE.REWARD_DISCIPLINE, ACTION_FORM.REWARD_PROCESS_SEARCH);
    this.setMainService(empRewardProcessService);
    this.employeeResolver.EMPLOYEE.subscribe(
      data => {
        if (data) {
          this.emp = data;
          this.empId = {employeeId: data};
          this.formSearch =  this.buildForm(this.empId, {employeeId: ['']});
          this.processSearch();
        }
      }
    );
  }
  /**
   * ngOnInit
   */
  ngOnInit() {
  }

  /**
   * prepareUpdate
   * param item
   */
  prepareSaveOrUpdate(item) {
    if (item && item.empRewardProcessId > 0) {
      this.empRewardProcessService.findOne(item.empRewardProcessId)
        .subscribe(res => {
          this.activeModel(res.data);
        });
    } else {
      this.activeModel(this.empId);
    }
  }

  /**
   * show model
   * data
   */
  private activeModel(data?: any) {
    const modalRef = this.modalService.open(RewardProcessFormComponent, DEFAULT_MODAL_OPTIONS);
    if (data) {
      modalRef.componentInstance.setFormValue(this.propertyConfigs, data);
    }
    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
      if (this.empRewardProcessService.requestIsSuccess(result)) {
        this.processSearch();
      }
    });
  }
  /**
   * Xu ly download file trong danh sach
   */
  public downloadFile(fileData) {
    this.fileStorage.downloadFile(fileData.id).subscribe(res => {
        saveAs(res , fileData.fileName);
    });
  }
}
