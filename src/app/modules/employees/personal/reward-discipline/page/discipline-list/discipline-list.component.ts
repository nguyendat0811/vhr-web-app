import { DEFAULT_MODAL_OPTIONS, RESOURCE, ACTION_FORM } from '@app/core/app-config';
import { AppComponent } from '@app/app.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { EmployeeResolver } from '@app/shared/services/employee.resolver';
import { EmpDisciplineService } from '@app/core/services/discipline/emp-discipline.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FileStorageService } from '@app/core/services/file-storage.service';
import { DisciplineFormComponent } from '../discipline-form/discipline-form.component';
import { saveAs } from 'file-saver';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';

@Component({
  selector: 'discipline-list',
  templateUrl: './discipline-list.component.html'
})
export class DisciplineListComponent extends BaseComponent implements OnInit {
  resultList: any = {};
  empId: {employeeId: any};
  emp: any;
  employeeId: number;
  @ViewChild('ptable') dataTable: any;
  constructor(private employeeResolver: EmployeeResolver
            , private empDisciplineService: EmpDisciplineService
            , private app: AppComponent
            , private modalService: NgbModal
            , private fileStorage: FileStorageService
            , public actr: ActivatedRoute
            ) {
    super(actr, RESOURCE.REWARD_DISCIPLINE, ACTION_FORM.DISCIPLINE_SEARCH);
    this.setMainService(empDisciplineService);
    this.employeeResolver.EMPLOYEE.subscribe(
      data => {
        if (data) {
          this.emp = data;
          this.empId = {employeeId: data};
          this.formSearch =  this.buildForm(this.empId, {employeeId: ['']});
          this.processSearch();
        }
      }
    );
  }
  /**
   * ngOnInit
   */
  ngOnInit() {
  }

  /**
   * prepareUpdate
   * param item
   */
  prepareSaveOrUpdate(item) {
    if (item && item.empDisciplineId > 0) {
      this.empDisciplineService.findOne(item.empDisciplineId)
        .subscribe(res => {
          this.activeModel(res.data);
        });
    } else {
      this.activeModel(this.empId);
    }
  }
  /**
   * show model
   * data
   */
  private activeModel(data?: any) {
    const modalRef = this.modalService.open(DisciplineFormComponent, DEFAULT_MODAL_OPTIONS);
    if (data) {
      modalRef.componentInstance.setFormValue(this.propertyConfigs, data);
    }
    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
      if (this.empDisciplineService.requestIsSuccess(result)) {
        this.processSearch();
      }
    });
  }
  /**
   * Xu ly download file trong danh sach
   */
  public downloadFile(fileData) {
    this.fileStorage.downloadFile(fileData.id).subscribe(res => {
        saveAs(res , fileData.fileName);
    });
  }
}
