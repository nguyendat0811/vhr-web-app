import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@app/shared';

import { FamilyRelationshipRoutingModule } from './family-relationship-routing.module';
import { FamilyRelationshipIndexComponent } from './page/family-relationship-index/family-relationship-index.component';
import { FamilyRelationshipSearchComponent } from './page/family-relationship-search/family-relationship-search.component';
import { FamilyRelationshipFormComponent } from './page/family-relationship-form/family-relationship-form.component';
import { EmployeeResolver } from '@app/shared/services/employee.resolver';

@NgModule({
  declarations: [FamilyRelationshipIndexComponent, FamilyRelationshipSearchComponent, FamilyRelationshipFormComponent],
  imports: [
    CommonModule,
    SharedModule,
    FamilyRelationshipRoutingModule
  ],
  entryComponents: [FamilyRelationshipSearchComponent, FamilyRelationshipFormComponent, FamilyRelationshipIndexComponent],
  providers: [EmployeeResolver]
})
export class FamilyRelationshipModule { }
