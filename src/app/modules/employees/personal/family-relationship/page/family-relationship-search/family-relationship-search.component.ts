import { RESOURCE } from './../../../../../../core/app-config';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from './../../../../../../shared/components/base-component/base-component.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DEFAULT_MODAL_OPTIONS } from '@app/core/app-config';
import { AppComponent } from '@app/app.component';
import { FamilyRelationshipFormComponent } from './../family-relationship-form/family-relationship-form.component';
import { FamilyRelationshipService, ACTION_FORM } from '@app/core';
import { EmployeeResolver } from '@app/shared/services/employee.resolver';
import { FileStorageService } from '@app/core/services/file-storage.service';
import { saveAs } from 'file-saver';

@Component({
  selector: 'family-relationship-search',
  templateUrl: './family-relationship-search.component.html'
})
export class FamilyRelationshipSearchComponent extends BaseComponent implements OnInit {

  resultList: any = {};
  employeeId: number;
  empId: {employeeId: any};
  @ViewChild('ptable') dataTable: any;

  constructor(private modalService: NgbModal
    , private fileStorage: FileStorageService
    , private employeeResolver: EmployeeResolver
    , private familyRelationshipService: FamilyRelationshipService
    , public actr: ActivatedRoute) {
      super(actr, RESOURCE.FAMILY_RELATIONSHIP, ACTION_FORM.SEARCH);
      this.setMainService(familyRelationshipService);

    this.employeeResolver.EMPLOYEE.subscribe(
      data => {
        if (data) {
          this.employeeId = data;
          this.empId = {employeeId: data};
          this.formSearch =  this.buildForm({employeeId: this.employeeId}, {employeeId: ['']});
          this.processSearch();
        }
      }
    );

  }

  ngOnInit() {
  }

  /* Action Modal*/
  private activeModel(data: any) {
    const modalRef = this.modalService.open(FamilyRelationshipFormComponent, DEFAULT_MODAL_OPTIONS);
    if (data) {
      modalRef.componentInstance.setFormValue(this.propertyConfigs, data);
    }
    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
      if (this.familyRelationshipService.requestIsSuccess(result)) {
        this.processSearch();
      }
    });
  }

    /**
   * prepareUpdate
   * param item
   */
  prepareSaveOrUpdate(item) {
    if (item && item.empFamilyRelationshipId > 0) {
      this.familyRelationshipService.findOne(item.empFamilyRelationshipId)
        .subscribe(res => {
          this.activeModel(res.data);
      });
    } else {
      this.activeModel(this.empId);
    }
  }

  public downloadFile(fileData) {
    this.fileStorage.downloadFile(fileData.id).subscribe(res => {
        saveAs(res , fileData.fileName);
    });
  }
}
