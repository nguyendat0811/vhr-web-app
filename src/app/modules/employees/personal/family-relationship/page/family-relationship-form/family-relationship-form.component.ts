import { ACTION_FORM } from '@app/core/app-config';
import { APP_CONSTANTS, RESOURCE, NationService } from '@app/core';
import { Component, OnInit , Input} from '@angular/core';
import { FormGroup, Validators} from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from '@app/app.component';
import { FamilyRelationshipService } from '@app/core';
import { SysCatTypeService } from '@app/core';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { FileControl } from '@app/core/models/file.control';
import { ValidationService } from '@app/shared/services';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'family-relationship-form',
  templateUrl: './family-relationship-form.component.html'
})
export class FamilyRelationshipFormComponent extends BaseComponent implements OnInit {

  formSave: FormGroup;
  conditionList: any = {};
  relationList: any = {};
  nationList: any = {};
  policyTypeList: any = {};
  listFile: any = {};
  @Input() item: any;
  formConfig = {
    empFamilyRelationshipId: [''],
    employeeId: [''],
    fullName: ['', [ValidationService.required, ValidationService.maxLength(100)]],
    relationTypeId: ['', [ValidationService.required]],
    dateOfBirth: ['', [ValidationService.beforeCurrentDate]],
    gender: ['', [ValidationService.required]],
    passportNumber: ['', [ValidationService.maxLength(50)]],
    taxNumber: ['', [ValidationService.positiveInteger, ValidationService.maxLength(50)]],
    relativeStatusId: [''],
    currentRelated: [''],
    nationId: [''],
    isLivingAbroad: [''],
    permanentResidence: ['', [ValidationService.maxLength(200)]],
    currentResidence: ['', [ValidationService.maxLength(200)]],
    job: ['', [ValidationService.maxLength(200)]],
    workOrganization: ['', [ValidationService.maxLength(200)]],
    unionParty: [''],
    policyType: [''],
    description: ['', [ValidationService.maxLength(2000)]],
  };

  constructor(public activeModal: NgbActiveModal
    , private familyRelationshipService: FamilyRelationshipService
    , private sysCatTypeService: SysCatTypeService
    , private nationService: NationService
    , private app: AppComponent
    , public actr: ActivatedRoute) {
      super(actr, RESOURCE.FAMILY_RELATIONSHIP, ACTION_FORM.INSERT);
      this.setMainService(familyRelationshipService);
      this.buildForms({}, ACTION_FORM.INSERT);

      // Loại quan hệ
      this.sysCatTypeService.getListSysCat(APP_CONSTANTS.SYS_CAT_TYPE_COCE.FAMILY_RELATION)
     .subscribe(res => this.relationList = res.data);

     // Tình trạng thân nhân
      this.sysCatTypeService.getListSysCat(APP_CONSTANTS.SYS_CAT_TYPE_COCE.RELATYPE)
     .subscribe(res => this.conditionList = res.data);

     // Loại chính sách
      this.sysCatTypeService.getListSysCat(APP_CONSTANTS.SYS_CAT_TYPE_COCE.POLICY_TYPE)
     .subscribe(res => this.policyTypeList = res.data);

     // Quốc tịch
     this.nationService.getNationList()
     .subscribe(res => this.nationList = res.data);
   }

  ngOnInit() {
  }

  get f () {
    return this.formSave.controls;
  }

  /**
   * buildForm
   */
  private buildForms(data?: any, actionForm?: ACTION_FORM): void {
    this.formSave = this.buildForm(data, this.formConfig, actionForm);
    this.formSave.addControl('file', new FileControl(null));
    if (data.fileAttachment) {
      (this.formSave.controls['file'] as FileControl).setFileAttachment(data.fileAttachment.empFamilyRelationshipFile);
    }
  }

  /**
   * processSaveOrUpdate
   */
  public processSaveOrUpdate() {
    console.log(this.formSave);
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    }
    this.app.confirmMessage(null, () => { // success
      this.familyRelationshipService.saveOrUpdateFormFile(this.formSave.value).subscribe(res => {
        if (this.familyRelationshipService.requestIsSuccess(res)) {
          this.activeModal.close(res);
          }
      });
    }, () => {
      // reject
    }
    );
  }


  /**
   * setFormValue
   * param data
   */
  public setFormValue(propertyConfigs: any, data?: any) {
    this.propertyConfigs = propertyConfigs;
    if (data && data.empFamilyRelationshipId > 0) {
      this.buildForms(data, ACTION_FORM.UPDATE);
      } else {
      this.buildForms(data, ACTION_FORM.INSERT);
    }
  }
}
