import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { RESOURCE } from '@app/core';

@Component({
  selector: 'family-relationship-index',
  templateUrl: './family-relationship-index.component.html'
})
export class FamilyRelationshipIndexComponent extends BaseComponent implements OnInit {

  constructor(public act: ActivatedRoute) { 
    super(act, RESOURCE.FAMILY_RELATIONSHIP);
  }

  ngOnInit() {
  }

}
