import { CommonUtils } from '@app/shared/services/common-utils.service';
import { RESOURCE } from './../../../../core/app-config';
import { PropertyResolver } from './../../../../shared/services/property.resolver';
import { FamilyRelationshipIndexComponent } from './page/family-relationship-index/family-relationship-index.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: FamilyRelationshipIndexComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.FAMILY_RELATIONSHIP,
      nationId: CommonUtils.getNationId()
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FamilyRelationshipRoutingModule { }
