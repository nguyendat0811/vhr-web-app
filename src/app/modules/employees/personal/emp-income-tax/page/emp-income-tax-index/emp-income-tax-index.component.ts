import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { ACTION_FORM, RESOURCE } from '@app/core';

@Component({
  selector: 'emp-income-tax-index',
  templateUrl: './emp-income-tax-index.component.html'
})
export class EmpIncomeTaxIndexComponent extends BaseComponent  implements OnInit {

  constructor(public act: ActivatedRoute) { 
    super(act, RESOURCE.EMP_INCOME_TAX);
  }
  ngOnInit() {
  }

}
