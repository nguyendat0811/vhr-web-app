import { FileControl } from '@app/core/models/file.control';
import { FamilyRelationshipService } from './../../../../../../../core/services/family-relationship/family-relationship.service';
import { AppComponent } from '@app/app.component';
import { CommonUtils } from './../../../../../../../shared/services/common-utils.service';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, Input} from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, Validators } from '@angular/forms';
import { EmpIncomeTaxReductionService, RESOURCE, ACTION_FORM, NationService, APP_CONSTANTS, SysCatTypeService } from '@app/core';
import { ValidationService } from '@app/shared/services';


@Component({
  selector: 'emp-income-tax-reduction-form',
  templateUrl: './emp-income-tax-reduction-form.component.html',
})
export class EmpIncomeTaxReductionFormComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  empFamilyRelationList = [];
  list = [];
  nationList = {};
  relationList = {};
  @Input() item: any;
  formConfig = {
      // Các trường hiện tại
      empTaxReductionId: [''],
      empFamilyRelationshipId: ['', [ValidationService.required]],
      declareOrganizationId: ['', [ValidationService.required]],
      profileNumber : ['', [ValidationService.required, ValidationService.maxLength(200)]],
      fromDate : ['', [ValidationService.required]],
      toDate : [''],
      employeeId: [''],
      // Các trường lấy từ bảng quan hệ thân nhân
      gender : [''],
      dateOfBirth : [''],
      nationId: [''],
      passportNumber: [''],
      taxNumber: [''],
      status: [''],
      residentAddress: [''],
      currentAddress: [''],
      currentRelated: [''],
      job: [''],
      workplace: [''],

  };
  constructor(public activeModal: NgbActiveModal
            , public actr: ActivatedRoute
            , private empIncomeTaxReductionService: EmpIncomeTaxReductionService
            , private sysCatTypeService: SysCatTypeService
            , private familyRelationshipService: FamilyRelationshipService
            , private nationService: NationService
            , private app: AppComponent) {
      super(actr, RESOURCE.EMP_TAX_REDUCTION, ACTION_FORM.EMP_TAX_REDUCTION_INSERT);
       this.buildForms({}, ACTION_FORM.EMP_TAX_REDUCTION_INSERT);
      // Quốc tịch
      this.nationService.getNationList()
      .subscribe(res => this.nationList = res.data);
      // Loại quan hệ
      this.sysCatTypeService.getListSysCat(APP_CONSTANTS.SYS_CAT_TYPE_COCE.FAMILY_RELATION)
      .subscribe(res => this.relationList = res.data);

  }

  /**
   * ngOnInit
   */
  ngOnInit() {
  }

  // Lựa chọn value từ bảng emp_family_relationship
  onRelativesChange(event) {
    this.familyRelationshipService.findOne(event)
      .subscribe( arg => {
        const data = arg.data;
        this.formSave.get('nationId').setValue(data.nationId);
        this.formSave.get('passportNumber').setValue(data.passportNumber);
        this.formSave.get('gender').setValue(data.gender);
        this.formSave.removeControl('dateOfBirth');
        this.formSave.addControl('dateOfBirth',
        CommonUtils.createControl(this.actionForm, 'dateOfBirth', data.dateOfBirth,
        null, this.propertyConfigs));
        this.formSave.get('taxNumber').setValue(data.taxNumber);
        this.formSave.get('status').setValue(data.relationTypeId);
        this.formSave.get('residentAddress').setValue(data.permanentResidence);
        this.formSave.get('currentAddress').setValue(data.currentResidence);
        this.formSave.get('currentRelated').setValue(data.currentRelated);
        this.formSave.get('job').setValue(data.job);
        this.formSave.get('workplace').setValue(data.workOrganization);
        this.formSave.get('employeeId').setValue(data.employeeId);
      });
  }

  // Quan hệ thân nhân theo employeeId và curentRelated
  getFamilyByEmployeedId(data, action) {
    this.familyRelationshipService.findFamilyByEmpId(data.employeeId, action).subscribe(arg =>  {
      this.empFamilyRelationList = arg.data; 
    });
  }

  /**
   * processSaveOrUpdate
   */
  processSaveOrUpdate() {
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    } else {
      this.app.confirmMessage(null, () => {// on accepted
        this.empIncomeTaxReductionService.saveOrUpdateFormFile(this.formSave.value)
          .subscribe(res => {
            if (this.empIncomeTaxReductionService.requestIsSuccess(res)) {
              this.activeModal.close(res);
            }
          });
      }, () => {// on rejected
      });
    }
  }

  /****************** CAC HAM COMMON DUNG CHUNG ****/
  get f () {
    return this.formSave.controls;
  }
    /**
   * buildForm
   */
  private buildForms(data?: any, actionForm?: ACTION_FORM): void {
    this.formSave = this.buildForm(data, this.formConfig, actionForm,
      [ ValidationService.notAffter('fromDate', 'toDate', 'app.taxReduction.toDate')]);
    this.formSave.addControl('file', new FileControl(null));
    if (data.fileAttachment) {
      (this.formSave.controls['file'] as FileControl).setFileAttachment(data.fileAttachment.empIncomeTaxReductionFile);
    }
  }
  /**
   * setFormValue
   * param data
   */
  public setFormValue(propertyConfigs: any, data?: any) {
    this.propertyConfigs = propertyConfigs;
    if (data && data.empTaxReductionId > 0) {
      this.buildForms(data, ACTION_FORM.EMP_TAX_REDUCTION_UPDATE);
      this.onRelativesChange(data.empFamilyRelationshipId);
      this.getFamilyByEmployeedId(data, ACTION_FORM.EMP_TAX_REDUCTION_UPDATE);
    } else {
      this.buildForms(data, ACTION_FORM.EMP_TAX_REDUCTION_INSERT);
      this.getFamilyByEmployeedId(data, ACTION_FORM.EMP_TAX_REDUCTION_INSERT);
    }
  }

}
