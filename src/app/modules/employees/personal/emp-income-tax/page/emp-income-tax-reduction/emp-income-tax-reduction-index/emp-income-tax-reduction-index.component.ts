import { EmployeeResolver } from './../../../../../../../shared/services/employee.resolver';
import { EmpIncomeTaxReductionFormComponent } from './../emp-income-tax-reduction-form/emp-income-tax-reduction-form.component';
import { EmpIncomeTaxReductionService } from './../../../../../../../core/services/emp-incomce-tax/emp-income-tax-reduction.service';
import { ACTION_FORM } from './../../../../../../../core/app-config';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DEFAULT_MODAL_OPTIONS, RESOURCE } from '@app/core/app-config';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { FileStorageService } from '@app/core/services/file-storage.service';

@Component({
  selector: 'emp-income-tax-reduction-index',
  templateUrl: './emp-income-tax-reduction-index.component.html',
})
export class EmpIncomeTaxReductionIndexComponent extends BaseComponent implements OnInit {
  empId: {employeeId: any};
  employeeId: number;

  constructor(
      public actr: ActivatedRoute
    , private modalService: NgbModal
    , private empIncomeTaxReductionService: EmpIncomeTaxReductionService
    , private fileStorage: FileStorageService
    , private employeeResolver: EmployeeResolver) {
    super(actr, RESOURCE.EMP_TAX_REDUCTION, ACTION_FORM.EMP_TAX_REDUCTION_SEARCH);
    this.setMainService(empIncomeTaxReductionService);

    this.employeeResolver.EMPLOYEE.subscribe(
      data => {
        if (data) {
          this.employeeId = data;
          this.empId = {employeeId: data};
          this.formSearch =  this.buildForm({employeeId: this.employeeId}, { employeeId: ['']});
        }
      }
    );
  }

  ngOnInit() {
    if(!this.hasPermission('action.view')){
      return;
    }
    this.processSearch();
  }

  get f () {
    return this.formSearch.controls;
  }

    /* Action Modal*/
    private activeModel(data: any) {
      const modalRef = this.modalService.open(EmpIncomeTaxReductionFormComponent, DEFAULT_MODAL_OPTIONS);
      if (data) {
        modalRef.componentInstance.setFormValue(this.propertyConfigs, data);
      }
      modalRef.result.then((result) => {
        if (!result) {
          return;
        }
        if (this.empIncomeTaxReductionService.requestIsSuccess(result)) {
          this.processSearch();
        }
      });
    }
  /*
   * prepareUpdate
   * param item
   */
  prepareSaveOrUpdate(item) {
    if (item && item.empTaxReductionId > 0) {
      this.empIncomeTaxReductionService.findOne(item.empTaxReductionId)
        .subscribe(res => {
          res.data.employeeId = this.empId.employeeId;
          this.activeModel(res.data);
      });
    } else {
      this.activeModel(this.empId);
    }
  }

  public downloadFile(fileData) {
    this.fileStorage.downloadFile(fileData.id).subscribe(res => {
        saveAs(res , fileData.fileName);
    });
  }
}
