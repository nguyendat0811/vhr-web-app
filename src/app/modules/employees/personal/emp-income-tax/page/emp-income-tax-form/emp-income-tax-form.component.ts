import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormArray } from '@angular/forms';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { AppComponent } from '@app/app.component';
import { EmployeeResolver } from '@app/shared/services/employee.resolver';
import { RESOURCE, ACTION_FORM, SysCatService, APP_CONSTANTS, EmployeeInfoService } from '@app/core';
import { EmpIncomeTaxService } from '@app/core/services/emp-incomce-tax/emp-income-tax.service';
import { CommonUtils, ValidationService } from '@app/shared/services';
import { FileControl } from '@app/core/models/file.control';
import * as moment from 'moment';

@Component({
  selector: 'emp-income-tax-form',
  templateUrl: './emp-income-tax-form.component.html'
})
export class EmpIncomeTaxFormComponent extends BaseComponent implements OnInit {
  public commonUtils =  CommonUtils;
  formEmpIncomeTax: FormGroup;
  formEmpIncomeTaxDetail: FormArray;
  listIdDelete = [];
  pid = '';
  currentDate: any;
  taxManageOrgList: any = {};
  calculateTypeList: any = {};
  sysCat: any;
  isReadOnly: Boolean;
  employeeId: number;
  CONSTANTS = APP_CONSTANTS;
  // partyUnion: any = {};


  formConfig = {
    empIncomeTaxId: [''],
    employeeId: [null],
    taxNumber: [null, [Validators.minLength(10), Validators.maxLength(20), ValidationService.required, ValidationService.positiveInteger]],
    taxNumberUpdatedTime: [null],
    taxCodeDate: [null, [ValidationService.required]],
    taxManageOrgId: [null, [ValidationService.required]],
    taxManageOrg: [null, [ValidationService.required]],
  };

  formIncomeTaxDetail = {
    empIncomeTaxDetailId: [null],
    employeeId: [null],
    calculateTypeId: [null],
    taxNumber: [null, [Validators.minLength(10), Validators.maxLength(20), ValidationService.positiveInteger]],
    taxPid: [null, Validators.minLength(10), Validators.maxLength(20)],
    taxPidIssuedDate: [null],
    taxPidIssuedPlace: [null],
    commitmentIncome: [null, [ValidationService.maxLength(15), ValidationService.positiveNumber]],
    effectiveDate: [null, [ValidationService.required]],
    expiredDate: [null],
  };

  constructor( public actr: ActivatedRoute
    , private app: AppComponent
    , private sysCatService: SysCatService
    , private employeeInfoService: EmployeeInfoService
    , private empIncomeTaxService: EmpIncomeTaxService
    , private employeeResolver: EmployeeResolver) {
      super(actr, RESOURCE.EMP_INCOME_TAX, ACTION_FORM.INSERT);
      this.currentDate = moment(new Date()).format('DD/MM/YYYY');
      this.buildFormIncomeTax({}, ACTION_FORM.INSERT);
      this.buildFormIncomeTaxDetail({});

      this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.MANAGE_ORG).subscribe(res => {
        this.taxManageOrgList = res.data;
      });

      this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.CALCULATE_TYPE).subscribe(res => {
        this.calculateTypeList = res.data;
      });

      this.employeeResolver.EMPLOYEE.subscribe(
        data => {
          if (data) {
            this.empIncomeTaxService.findIncomeTaxByEmployeeId(data).subscribe(
              (res) => {
                if (res) {
                   this.buildFormIncomeTax(res.data, ACTION_FORM.UPDATE);
                }
              });

            this.empIncomeTaxService.findIncomeDetailByEmployeeId(data).subscribe(
              (res) => {
                if (res[0].empIncomeTaxDetailId !== null) {
                // console.log('logXXXX', res[0].empIncomeTaxDetailId)
                this.buildFormIncomeTaxDetail(res);
                } else {
                  this.buildFormIncomeTaxDetail();
                }
              }
            );
            this.employeeId = data;
          }
     });
    }

  ngOnInit() {
    this.formEmpIncomeTax.get('employeeId').setValue(this.employeeId);
  }

  processSaveOrUpdate() {
    if (!this.validateBeforeSave()) {
      return;
    }
    if (this.checkConflictProcess(this.formEmpIncomeTaxDetail.value) === true) {
      return;
    }

    this.app.confirmMessage(null, () => { // on accepted/
        const formSave = {};
        const formEmpIncomeTaxNew = this.formEmpIncomeTax.value;
        formEmpIncomeTaxNew['taxNumberUpdatedTime'] = this.currentDate;
        formSave['empIncomeTax']  = formEmpIncomeTaxNew;
        formSave['empIncomeTaxDetail']  = this.formEmpIncomeTaxDetail.value;
        formSave['employeeId'] = this.employeeId;
        formSave['listIdDelete'] = this.listIdDelete;
        // console.log('formSave', formSave);
        this.empIncomeTaxService.saveOrUpdateFormFile(formSave)
        .subscribe(res => {
          // call back sau khi save
          this.buildFormIncomeTax(res.data, ACTION_FORM.UPDATE);

          this.empIncomeTaxService.findIncomeDetailByEmployeeId(this.employeeId).subscribe(
            (data) => {
            this.buildFormIncomeTaxDetail(data);
            }
          );
        });
      }, () => {
      // on rejected
    });
  }

  private buildFormIncomeTaxDetail(listIncomeTaxDetail?: any) {
    const controls = new FormArray([]);
    if (listIncomeTaxDetail) {
      for (const i in listIncomeTaxDetail) {
        const IncomeTaxDetail = listIncomeTaxDetail[i];
        const group = this.createFormIncomeTaxDetail({});
        group.patchValue(IncomeTaxDetail);
        if (IncomeTaxDetail.fileAttachment) {
          (group.controls['file'] as FileControl).setFileAttachment(IncomeTaxDetail.fileAttachment.empIncomeTaxDetailFile);
        }
        controls.push(group);
      }
    }
    this.formEmpIncomeTaxDetail = controls;
  }

  /**
   * createFormIncomeTaxDetail
   */
  private createFormIncomeTaxDetail(data?: any): FormGroup {
    const formGroup = this.buildForm(data, this.formIncomeTaxDetail, ACTION_FORM.INSERT,
      [ ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.empIncomeTax.invalidExpiredDate') ]);
    formGroup.addControl('file', new FileControl(null));
    return formGroup;
  }

  get f() {
    return this.formEmpIncomeTax.controls;
  }

  public addRow(item: FormGroup) {
    let pid = '';
    const controls = this.formEmpIncomeTaxDetail as FormArray;
    this.employeeInfoService.findOne(this.employeeId)
    .subscribe(res => {
      if (res.data.pidNumber || res.data.pidIssuedDate || res.data.pidIssuedPlace) {
        console.log(res.data)
        controls.insert(controls.length, this.createFormIncomeTaxDetail({ taxPid: res.data.pidNumber,
                                                                          taxPidIssuedDate: res.data.pidIssuedDate,
                                                                          taxPidIssuedPlace: res.data.pidIssuedPlace}));
      } else {
        controls.insert(controls.length, this.createFormIncomeTaxDetail({}));
      }
    });
  }

  /**
   * removeLabourContractDetail
   * param index
   * param item
   */
  public removeRow(index: number, item: any) {
    if (item && item > 0) {
      const controls = this.formEmpIncomeTaxDetail as FormArray;
      this.empIncomeTaxService.confirmDelete({
        messageCode: null,
        accept: () => {
          this.empIncomeTaxService.deleteByEmpIncomeTaxDetailId(item)
          .subscribe(res => {
            if (this.empIncomeTaxService.requestIsSuccess(res)) {
              // this.empIncomeTaxService.findEmpIncomeTaxDetailByEmployeeId(this.formEmpIncomeTax.get('employeeId').value)
              // .subscribe(result => {
              //   this.buildFormIncomeTaxDetail(result);
              // });
              controls.removeAt(index);
            }
          });
        }
      });
    } else {
    const controls = this.formEmpIncomeTaxDetail as FormArray;
    // if (controls.length === 1) {
    //   this.buildFormIncomeTaxDetail({});
    //   const group = this.createFormIncomeTaxDetail();
    //   controls.push(group);
    //   this.formEmpIncomeTaxDetail = controls;
    // }
    controls.removeAt(index);
    }
  }

  private buildFormIncomeTax(data?: any, actionForm?: ACTION_FORM): void {
    this.formEmpIncomeTax = this.buildForm(data, this.formConfig
      , actionForm);
  }


  private validateBeforeSave(): boolean {
    const formEmpIncomeTax = CommonUtils.isValidForm(this.formEmpIncomeTax);
    const formEmpIncomeTaxDetail = CommonUtils.isValidForm(this.formEmpIncomeTaxDetail);
    return formEmpIncomeTax && formEmpIncomeTaxDetail;
  }

  private checkConflictProcess(list?: any): boolean {
    for (let i = 0; i < list.length - 1; i++) {
      for (let j = i + 1; j < list.length; j ++ ) {
        const start1 = list[i].effectiveDate;
        const end1 = list[i].expiredDate;
        const start2 = list[j].effectiveDate;
        const end2 = list[j].expiredDate;
        if (CommonUtils.isConflictDate(start1, end1, start2, end2) === true ) {
          this.app.errorMessage('empIncomeTax', 'conflictProcess');
          return true;
        }
      }
    }
    return false;
  }
}
