import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { PropertyResolver } from '@app/shared/services/property.resolver';
import { RESOURCE } from '@app/core';
import { CommonUtils } from '@app/shared/services';
import { EmpIncomeTaxIndexComponent } from './page/emp-income-tax-index/emp-income-tax-index.component';

const routes: Routes = [
  {
    path: '',
    component: EmpIncomeTaxIndexComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.EMP_INCOME_TAX,
      nationId: CommonUtils.getNationId()
    }
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmpIncomeTaxRoutingModule { }
