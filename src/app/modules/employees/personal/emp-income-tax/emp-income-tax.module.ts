import { EmployeeResolver } from '@app/shared/services/employee.resolver';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@app/shared';
import { EmpIncomeTaxRoutingModule } from './emp-income-tax-routing.module';
import { EmpIncomeTaxIndexComponent } from './page/emp-income-tax-index/emp-income-tax-index.component';
import { EmpIncomeTaxFormComponent } from './page/emp-income-tax-form/emp-income-tax-form.component';
// tslint:disable-next-line: max-line-length
import { EmpIncomeTaxReductionIndexComponent} from './page/emp-income-tax-reduction/emp-income-tax-reduction-index/emp-income-tax-reduction-index.component';
// tslint:disable-next-line: max-line-length
import { EmpIncomeTaxReductionFormComponent } from './page/emp-income-tax-reduction/emp-income-tax-reduction-form/emp-income-tax-reduction-form.component';

@NgModule({
  declarations: [EmpIncomeTaxIndexComponent, EmpIncomeTaxFormComponent, EmpIncomeTaxReductionIndexComponent
    , EmpIncomeTaxReductionFormComponent],
  imports: [
    CommonModule,
    SharedModule,

    EmpIncomeTaxRoutingModule
  ],
  entryComponents: [EmpIncomeTaxReductionFormComponent],
  providers: [EmployeeResolver]
})
export class EmpIncomeTaxModule { }
