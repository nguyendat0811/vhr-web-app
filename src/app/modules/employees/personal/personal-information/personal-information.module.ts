import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@app/shared';

import { PersonalInformationRoutingModule } from './personal-information-routing.module';
import { PersonalInformationFormComponent } from './page/personal-information-form/personal-information-form.component';
import { PersonalInformationIndexComponent } from './page/personal-information-index/personal-information-index.component';

@NgModule({
  declarations: [PersonalInformationFormComponent, PersonalInformationIndexComponent],
  imports: [
    CommonModule,
    SharedModule,
    PersonalInformationRoutingModule
  ],
  entryComponents: [PersonalInformationFormComponent, PersonalInformationIndexComponent]
})
export class PersonalInformationModule { }
