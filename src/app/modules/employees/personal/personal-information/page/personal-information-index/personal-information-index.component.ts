import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { RESOURCE } from '@app/core';

@Component({
  selector: 'personal-information-index',
  templateUrl: './personal-information-index.component.html'
})
export class PersonalInformationIndexComponent extends BaseComponent implements OnInit {

  constructor(public act: ActivatedRoute) { 
    super(act, RESOURCE.EMPLOYEE);
  }

  ngOnInit() {
  }

}
