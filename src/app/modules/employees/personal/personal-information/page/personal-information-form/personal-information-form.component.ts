import { trigger, state, style, transition, animate } from '@angular/animations';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { APP_CONSTANTS, SysCatService, NationService, RESOURCE, ACTION_FORM } from '@app/core';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { AppComponent } from '@app/app.component';

import { EmployeeInfoService, NationConfigService } from '@app/core';
import { EmployeeResolver } from '@app/shared/services/employee.resolver';
import { ValidationService } from '@app/shared/services';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'personal-information-form',
  templateUrl: './personal-information-form.component.html',
  animations: [
    trigger('animation', [
        state('visible', style({

        })),
        transition('void => *', [
            style({transform: 'translateX(50%)', opacity: 0}),
            animate('300ms ease-out')
        ]),
        transition('* => void', [
            animate(('250ms ease-in'), style({
                height: 0,
                opacity: 0,
                transform: 'translateX(50%)'
            }))
        ])
    ])
],
encapsulation: ViewEncapsulation.None
})
export class PersonalInformationFormComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  nationList: any = {};
  religionList: any = {};
  ethnicList: any = {};
  recuitList: any = {};
  status: any = {};
  checkNation: boolean;
  requirePersionalId: any;
  formConfig = {
    employeeId: [''],
    firstName: ['', [ ValidationService.required, ValidationService.maxLength(20) ]],
    middleName: ['', [ ValidationService.maxLength(20) ]],
    lastName: ['', [ ValidationService.required, ValidationService.maxLength(20) ]],
    otherEmail: ['', [ ValidationService.maxLength(200), ValidationService.emailFormat ]],
    dateOfBirth: ['', [ValidationService.required]],
    pidNumber: ['', [ ValidationService.maxLength(20)]],
    pidIssuedDate: [''],
    pidIssuedPlace: ['', [ValidationService.maxLength(200)]],
    passportNumber: ['', [ValidationService.maxLength(50)]],
    passportIssueDate: [''],
    nationId: ['', [ValidationService.required]],
    gender: ['', [ValidationService.required]],
    religionId: [''],
    ethnicId: [''],
    status: [''],
    maritalStatus: [''],
    placeOfBirth: ['', [ValidationService.maxLength(500)]],
    origin: ['', [ValidationService.maxLength(500)]],
    permanentAddress: ['', [ValidationService.maxLength(500)]],
    currentAddress: ['', [ValidationService.maxLength(500)]],
    mobileNumber: ['', [ValidationService.maxLength(50), ValidationService.mobileNumber]],
    mobileNumber2: ['', [ValidationService.maxLength(50), ValidationService.mobileNumber]],
    mobileNumber3: ['', [ValidationService.maxLength(50), ValidationService.mobileNumber]],
    recruitTypeId: [''],
  };
  /* Xử lý phone number */
  columns: number[] = [];
  columns1: number[] = [];

  @Input() item: any;
  constructor(public actr: ActivatedRoute,
    private sysCatService: SysCatService,
    private employeeInfoService: EmployeeInfoService,
    private employeeResolver: EmployeeResolver,
    private nationService: NationService,
    private nationConfigService: NationConfigService,
    private app: AppComponent) {
    super(actr, RESOURCE.EMPLOYEE, ACTION_FORM.UPDATE);
    this.formSave = this.buildForm({}, this.formConfig);
    this.checkNation = true;

    // Trạng thái làm việc
    this.status = APP_CONSTANTS.WORKING_STATUS;

    // Quốc tịch
    this.nationService.getNationList()
    .subscribe(res => this.nationList = res.data);

    // Loại tuyển dụng
    this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.RECUITMENT_CATEGORY)
      .subscribe(res => this.recuitList = res.data);

    this.employeeResolver.EMPLOYEE.subscribe(
      data => {
        console.log('employeeResolver PersonalInformationFormComponent', data)
        if (data) {
          this.employeeInfoService.findOne(data).subscribe(res => {
            this.formSave = this.buildForm(res.data, this.formConfig);
            if (res.data.mobileNumber2) {
              this.columns = [0];
            }
            if (res.data.mobileNumber3) {
              this.columns1 = [0];
            }
            if (res.data.nationId) {
              this.onChangeNation(res.data.nationId);
            }
          }
          );
        }
      }
    );
  }

  ngOnInit() {
  }

  addMobileNumber2() {
    this.columns.push(this.columns.length);
  }

  removeMobileNumber2() {
    this.columns.splice(-1, 1);
    this.formSave.controls['mobileNumber2'].setValue('');
  }

  addMobileNumber3() {
    this.columns1.push(this.columns1.length);
  }

  removeMobileNumber3() {
    this.columns1.splice(-1, 1);
    this.formSave.controls['mobileNumber3'].setValue('');
  }

  /* */
  get f () {
    return this.formSave.controls;
  }

      /**
   * validate save
   */
  private validateBeforeSave(): boolean {
    return CommonUtils.isValidForm(this.formSave);
  }

  /**
   * processSaveOrUpdate
   */
  public processSaveOrUpdate() {
    if (!this.validateBeforeSave()) {
      return;
    }
    this.app.confirmMessage(null, () => { // success
      this.employeeInfoService.updateEmployeeInfo(this.formSave.value)
          .subscribe(res => {
            this.employeeInfoService.findOne(res.data.employeeId).subscribe(dataRes => {
              this.formSave = this.buildForm(res.data, this.formConfig);
              // Load lai thong tin chung tren cung ben trai
              this.employeeResolver.COMMON_INFO.next(res.data.employeeId);
              if (res.data.nationId) {
                this.onChangeNation(res.data.nationId);
              }
              if (dataRes.data.mobileNumber2) {
                this.columns = [0];
              }
              if (dataRes.data.mobileNumber3) {
                this.columns1 = [0];
              }
            });
          }
        );
      }, () => {
        // reject
      }
    );
  }

  // onChangeNation
  public onChangeNation(event) {
    const pidNumber = this.formSave.value.pidNumber;
    const nationId = CommonUtils.nvl(event);

    if (event) {
      this.nationService.findOne(nationId).subscribe(res => {
        if (res.data) {
          this.requirePersionalId = CommonUtils.nvl(res.data.requirePersionalId);
          if (this.requirePersionalId === 1) {
            this.checkNation = false;
            this.formSave.removeControl('pidNumber');
            this.formSave.addControl('pidNumber', new FormControl(pidNumber,
              [ValidationService.required, ValidationService.maxLength(20)]));
          } else {
            this.checkNation = true;
            this.formSave.removeControl('pidNumber');
            this.formSave.addControl('pidNumber', new FormControl(pidNumber,
              [ValidationService.maxLength(20)]));
          }
        }
      });
    } else {
      this.checkNation = true;
      this.formSave.removeControl('pidNumber');
      this.formSave.addControl('pidNumber', new FormControl(pidNumber,
        [ValidationService.maxLength(20)]));
    }

    // Get list ethnic by nation
    this.nationConfigService.getNationConfigList(APP_CONSTANTS.NATION_CONFIG_TYPE.ETHNIC, nationId).subscribe(
      res => {
        this.ethnicList = [];
        for (const item of res.data) {
          this.ethnicList.push({label: item.name, value: item.nationConfigId});
        }
      }
    );

    // Get list religion by nation
    this.nationConfigService.getNationConfigList(APP_CONSTANTS.NATION_CONFIG_TYPE.RELIGION, nationId).subscribe(
      res => {
        this.religionList = [];
        for (const item of res.data) {
          this.religionList.push({label: item.name, value: item.nationConfigId});
        }
      }
    );
  }
}
