import { CommonUtils } from './../../../../shared/services/common-utils.service';
import { TranslationService } from 'angular-l10n';
import { Component, OnInit, OnDestroy, AfterViewInit, PipeTransform, Pipe, Input } from '@angular/core';
import { EmployeeResolver } from '@app/shared/services/employee.resolver';
import { EmployeeInfoService, RESOURCE} from '@app/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { EmpSystemConfigService } from '@app/core/services/sys-language/emp-system-config.service';
import { FormGroup } from '@angular/forms';
import { HelperService } from '@app/shared/services/helper.service';

@Component({
  selector: 'layout',
  providers: [EmployeeResolver],
  templateUrl: './layout.component.html'
})
export class LayoutComponent implements OnInit, OnDestroy, AfterViewInit {
  @Input()
  formSearch: FormGroup;
  menuFilter = '';
  allItems = [];
  hardItems = [];
  items;
  defaultMenu = ['personal_information', 'work_process', 'contract_process', 'emp_salary_process'];
  navigationSubscription;
  private _employeeId;
  employeeBean;
  constructor(
    private translation: TranslationService,
    private employeeResolver: EmployeeResolver,
    private employeeInfoService: EmployeeInfoService,
    private empSystemConfigService: EmpSystemConfigService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private helperService: HelperService,
  ) {
    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      // If it is a NavigationEnd event re-initalise the component
      if (e instanceof NavigationEnd) {
        const params = this.activatedRoute.snapshot.params;
        if (this._employeeId !== params.id) {
          this.employeeInfoService.findOne(params.id).subscribe(
            res => {
              if (res.type === 'SUCCESS') {
                this._employeeId = params.id;
                this.createTabRouterLink();
                let arrTmp = this.hardItems.concat(this.allItems);
                this.helperService.resolveUrlEmployee(arrTmp);
                this.employeeResolver.resolve(res.data.employeeId);
                this.employeeBean = res.data;
                if (this.activatedRoute.children.length === 0) {
                  this.router.navigate(['/employees/personal', this._employeeId, 'personal-information']);
                }
              } else {
                if (this._employeeId) {
                  this.router.navigate(['/employees/personal', this._employeeId, 'personal-information']);
                } else {
                  this.router.navigate(['/employees']);
                }
              }
            }
          );
        } else {
          if (this.activatedRoute.children.length === 0) {
            this.router.navigate(['/employees/update-employee']);
          }
        }
      }
    });
  }

  ngOnInit() {
  }


  createTabRouterLink() {

    this.hardItems  = [
      {
        label: this.translation.translate('Employee.Menu.Personal'),
        routerLink: ['/employees/personal', this._employeeId, 'personal-information'],
        styleClass: 'personal_information',
        iconClass: 'glyphicons glyphicons-user',
        resource: 'EMPLOYEE'
      }, {
        label: this.translation.translate('Employee.Menu.WorkProcess'),
        routerLink: ['/employees/personal', this._employeeId, 'work-process'],
        styleClass: 'work_process',
        iconClass: 'glyphicons glyphicons-calendar',
        resource: 'WORK_PROCESS'
      }, {
        label: this.translation.translate('Employee.Menu.ContractProcess'),
        routerLink: ['/employees/personal', this._employeeId, 'contract-process'],
        styleClass: 'contract_process',
        iconClass: 'glyphicons glyphicons-veteran',
        resource: 'EMP_TYPE_PROCESS'
      }, {
        label: this.translation.translate('Employee.Menu.PositionSalaryProcess'),
        routerLink: ['/employees/personal', this._employeeId, 'emp-position-salary'],
        styleClass: 'emp-position-salary',
        iconClass: 'glyphicons glyphicons-money',
        resource: 'EMP_POSITION_SALARY'
      }
    ];
    this.allItems = [{
        label: this.translation.translate('Employee.Menu.EducationProcess'),
        routerLink: ['/employees/personal', this._employeeId, 'education-process'],
        styleClass: 'education_process',
        iconClass: 'glyphicons glyphicons-education',
        resource: 'EMP_EDUCATION_PROCESS'
      }, {
        label: this.translation.translate('Employee.Menu.Financial'),
        routerLink: ['/employees/personal', this._employeeId, 'financial'],
        styleClass: 'financial',
        iconClass: 'glyphicons glyphicons-usd',
        resource: 'FINANCIAL'
      }, {
        label: this.translation.translate('Employee.Menu.otherInformation'),
        routerLink: ['/employees/personal', this._employeeId, 'other-information'],
        styleClass: 'other_information',
        iconClass: 'glyphicons glyphicons-group',
        resource: 'OTHER_INFORMATION'
      }, {
        label: this.translation.translate('Employee.Menu.partyUnion'),
        routerLink: ['/employees/personal', this._employeeId, 'party-union'],
        styleClass: 'party_union',
        iconClass: 'glyphicons glyphicons-certificate',
        resource: 'PARTY_UNION'
      }, {
        label: this.translation.translate('Employee.Menu.MilitaryInformation'),
        routerLink: ['/employees/personal', this._employeeId, 'military-information'],
        styleClass: 'military_information',
        iconClass: 'glyphicons glyphicons-veteran',
        resource: 'MILITARY_INFORMATION'
      }, {
        label: this.translation.translate('Employee.Menu.VisaPassportInformation'),
        routerLink: ['/employees/personal', this._employeeId, 'visa-passport-information'],
        styleClass: 'visa_passport_information',
        iconClass: 'glyphicons glyphicons-nameplate',
        resource: 'VISA_PASSPORT_INFORMATION'
      }, {
        label: this.translation.translate('Employee.Menu.FamilyRelationship'),
        routerLink: ['/employees/personal', this._employeeId, 'family-relationship'],
        styleClass: 'family_relationship',
        iconClass: 'glyphicons glyphicons-family',
        resource: 'FAMILY_RELATIONSHIP'
      }, {
        label: this.translation.translate('Employee.Menu.RewardDiscipline'),
        routerLink: ['/employees/personal', this._employeeId, 'reward-discipline'],
        styleClass: 'reward_discipline',
        iconClass: 'glyphicons glyphicons-scale-classic',
        resource: 'REWARD_DISCIPLINE'
      }, {
        label: this.translation.translate('Employee.Menu.AllowanceProcess'),
        routerLink: ['/employees/personal', this._employeeId, 'allowance-process'],
        styleClass: 'allowance_process',
        iconClass: 'glyphicons glyphicons-usd',
        resource: 'EMP_ALLOWANCE_PROCESS'
      }, {
        label: this.translation.translate('Employee.Menu.TradeUnionProcess'),
        routerLink: ['/employees/personal', this._employeeId, 'trade-union-process'],
        styleClass: 'trade_union_process',
        iconClass: 'glyphicons glyphicons-small-payments',
        resource: 'TRADE_UNION'
      }, {
        label: this.translation.translate('Employee.Menu.SocialInsuranceProcess'),
        routerLink: ['/employees/personal', this._employeeId, 'social-insurance-process'],
        styleClass: 'social_insurance_process',
        iconClass: 'glyphicons glyphicons-invoice',
        resource: 'SOCIAL_INSURANCE_PROCESS'
      }, {
        label: this.translation.translate('Employee.Menu.HealthInsurance'),
        routerLink: ['/employees/personal', this._employeeId, 'health-insurance'],
        styleClass: 'health_insurance',
        iconClass: 'fa fa-user-md',
        resource: 'HEALTH_INSURANCE'
      }, {
        label: this.translation.translate('Employee.Menu.EmpFile'),
        routerLink: ['/employees/personal', this._employeeId, 'emp-file'],
        styleClass: 'emp_file',
        iconClass: 'glyphicons glyphicons-nameplate',
        resource: 'EMP_FILE'
      }, {
        label: this.translation.translate('Employee.Menu.longLeaveProcess'),
        routerLink: ['/employees/personal', this._employeeId, 'long-leave-process'],
        styleClass: 'long_leave_process',
        iconClass: 'glyphicons glyphicons-calendar',
        resource: 'LONG_LEAVE_PROCESS'
      }, {
        label: this.translation.translate('Employee.Menu.TaxReduction'),
        routerLink: ['/employees/personal', this._employeeId, 'emp-income-tax'],
        styleClass: 'emp-income-tax',
        iconClass: 'glyphicons glyphicons-coins',
        resource: 'EMP_TAX_INFORMATION'
      }, {
        label: this.translation.translate('Employee.Menu.occupationalSafety'),
        routerLink: ['/employees/personal', this._employeeId, 'occupational-safety'],
        styleClass: 'occupational_safety',
        iconClass: 'glyphicons glyphicons-vr-maintenance',
        resource: 'OCCUPATIONAL_SAFETY'
      }, {
        label: this.translation.translate('Employee.Menu.workSchedule'),
        routerLink: ['/employees/personal', this._employeeId, 'emp-work-schedule'],
        styleClass: 'emp_work_schedule',
        iconClass: 'glyphicons glyphicons-one-day',
        resource: 'EMP_WORK_SCHEDULE'
      }, {
        label: this.translation.translate('Employee.Menu.ChangeHistory'),
        routerLink: ['/employees/personal', this._employeeId, 'change-history'],
        styleClass: 'change_history',
        iconClass: 'fa fa-history',
        resource: 'CHANGE_HISTORY'
      }
    ];

    this.empSystemConfigService.getEmpTabs().subscribe(res => {
      if (this.empSystemConfigService.requestIsSuccess(res)) {
        this.buildDefaultMenu(res.data);
      } else {
        this.buildDefaultMenu();
      }
    });
  }

  /**
   * xu ly hien thi danh sach menu
   */
  private buildDefaultMenu(empTabs?) {
    this.items = [];
    const listResource = [];
    let listPermission: any;

    for (const item of this.hardItems) {
      listResource.push(RESOURCE[item.resource]);
    }
    for (const item of this.allItems) {
      listResource.push(RESOURCE[item.resource]);
    }

    listPermission = CommonUtils.getPermissionByListResourceCode(listResource);

    // List menu theo quyen
    let listTemp = [];
    for (const item of this.hardItems) {
      if (this.hasPermissionView(item.resource, listPermission)) {
        listTemp.push(item);
      }
    }
    this.hardItems = listTemp;
    listTemp = [];
    for (const item of this.allItems) {
      if (this.hasPermissionView(item.resource, listPermission)) {
        listTemp.push(item);
      }
    }
    this.allItems = listTemp;
    // Build list tab
    for (const item of this.hardItems) {
      this.items.push(item);
    }

    if (!empTabs) {
      for (const item of this.allItems) {
        item.checked = true;
        this.items.push(item);
      }
      return;
    }
    empTabs = JSON.parse(empTabs);
    for (const code of empTabs) {
      for (const item of this.allItems) {
        if (code === item.resource) {
          item.checked = true;
          this.items.push(item);
        }
      }
    }
  }

  public saveEmpTabs(item) {
    const checked = item.checked;
    if (checked) {
      this.items.push(item);
    } else {
      this.items.splice(this.items.indexOf(item), 1);
    }
    const data = [];
    for (const k of this.items) {
      data.push(k.resource);
    }
    this.empSystemConfigService.saveEmpTabs({'empTabs': data}).subscribe(res => {});
  }

  closeItem(event, index) {
    this.items = this.items.filter((item, i) => i !== index);
    event.preventDefault();
  }

  ngAfterViewInit() {
    // this.contentLayoutComponent.navViewChange(true);
  }

  ngOnDestroy() {
    // this.contentLayoutComponent.navViewChange(false);
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }
    // this.helperService.APP_SHOW_HIDE_LEFT_MENU.next(true);
  }

  public redirectTo(item) {
    this.router.navigate(item.routerLink);
  }

  private hasPermissionView(resourceCode: string, listPermissions): boolean {
    if (!listPermissions || listPermissions.length <= 0) {
      return false;
    }
    const rsSearch = listPermissions.findIndex(x => x.resourceCode === resourceCode && x.operationCode === 'VIEW');
    if (rsSearch < 0) {
      return false;
    }
    return true;
  }
}

@Pipe({ name: 'filter' })
export class FilterPipe implements PipeTransform {
  transform(items: any[], searchText: string): any[] {
    if (!items) { return []; }
    if (!searchText) { return items; }
    searchText = searchText.toLowerCase();
    return items.filter( it => {
      return it.label.toLowerCase().includes(searchText);
    });
   }
}
