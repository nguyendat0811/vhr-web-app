import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, Input, ViewChild, ViewChildren } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { EmployeeResolver } from '@app/shared/services/employee.resolver';
import { EmployeeBean } from '@app/core/models/employee.model';
import { FileStorageService } from '@app/core/services/file-storage.service';
import { AppComponent } from '@app/app.component';
import { EmployeeInfoService, EmpEducationProcessService } from '@app/core';
import { LanguageDegreeService, WorkProcessService, EmpTypeProcessService } from '@app/core';
import * as moment from 'moment';
import { LongLeaveProcessService } from '../../../../../core/services/long-leave-process/long-leave-process.service';
import { EmpWorkScheduleService } from '@app/core/services/hr-timekeeping/emp-work-schedule.service';
import { WorkScheduleCalendarComponent } from '@app/shared/components/work-schedule-calendar/work-schedule-calendar.component';
import { EmployeeReportService } from '@app/core/services/hr-report/employee-report.service';

@Component({
  selector: 'emp-info',
  templateUrl: './emp-info.component.html',
  styleUrls: ['./emp-info.component.css']
})
export class EmpInfoComponent implements OnInit {
  @Input()
  EmployeeInfo;
  formEmpInfo: FormGroup;
  employeeBean: EmployeeBean;
  employeeId = new FormControl();
  date;
  dateFormat;
  empInfo = {
    fullName: '',
    email: '',
    mobileNumber: '',
    phoneNumber: '',
    dateOfBirth: '',
    permanentAddress: '',
  };
  empId: number;
  empEducationInfo: {
    educationGradeName: '',
    educationSubjectName: '',
    granduatedRankName: '',
    issuedPlaceName: '',
    issuedDate: '',
  };
  empLanguageDegree: {
    languageDegreeName: '',
    degreeName: '',
    resuilt: ''
  };
  empWorkProcess: {
    positionName: '',
    organizationName: ''
  };
  empTypeProcess: {
    labourContractDetailName: '',
    effectiveDate: '',
    contractMonth: '',
    expiredDate: '',
    labourContractTypeName: '',
  };
  hireDate: {
    viettelStartDate: '',
    timeWork: '',
  };
  longLeaveList: {
    longLeaveReasonName: '',
    effectiveDate: '',
    expiredDate: '',
  };
  length;
  lenthEmpWorkSchedule;
  public startFrom;
  public startTo;
  public currentTime;
  @ViewChild('empCalendar1') empCalendar1: WorkScheduleCalendarComponent;
  @ViewChildren('fileAvatar') fileAvatar;
  constructor(
    private employeeResolver: EmployeeResolver
    , private employeeInfoService: EmployeeInfoService
    , private empWorkScheduleService: EmpWorkScheduleService
    , private empEducationProcessService: EmpEducationProcessService
    , private languageDegreeService: LanguageDegreeService
    , private workProcessService: WorkProcessService
    , private longLeaveProcessService: LongLeaveProcessService
    , private fileStorage: FileStorageService
    , private empTypeProcessService: EmpTypeProcessService
    , private activatedRoute: ActivatedRoute
    , private router: Router
    , private app: AppComponent
    // Kien pt them bao cao trich ngang ca nhan
    , private employeeReportService: EmployeeReportService
    //
  ) {
    this.currentTime = moment(new Date()).format('MM/YYYY');
    // Load thong tin chung
    this.employeeResolver.COMMON_INFO.subscribe(
      data => {
        if (data) {
          this.employeeInfoService.findOne(data).subscribe(
            res => {
              this.EmployeeInfo = res.data;
            }
          );
        }
      }
    );
    // load thông tin quá trình công tác hiện tại
    this.employeeResolver.WORK_PROCESS.subscribe(
      data => {
        if (data) {
          this.workProcessService.getMainProcessByEmployeeId(data).subscribe(resWorkProcess => {
                this.empWorkProcess = resWorkProcess.data;
          });
        }
      }
    );
    // load thông tin quá trình nghỉ dài ngày
    this.employeeResolver.LONG_LEAVE_PROCESS.subscribe(
      data => {
        if (data) {
          this.longLeaveProcessService.getInforLongLeaveProcessStillValidated(data).subscribe(resLongleave => {
            this.longLeaveList = resLongleave;
            this.length = resLongleave.length;
          });
        }
      }
    );
    // load thông tin quá trình đào tạo
    this.employeeResolver.EDUCATION_PROCESS.subscribe(
      data => {
        if (data) {
          this.empEducationProcessService.getEducationByEmployeeId(data).subscribe(res1 => {
              this.empEducationInfo = res1.data;
          });
        }
      }
    );
    // load thông tin trình độ ngôn ngữ
    this.employeeResolver.LANGUAGE.subscribe(
      data => {
        if (data) {
          this.languageDegreeService.getMainProcessByEmployeeId(data).subscribe(resLanguage => {
              this.empLanguageDegree = resLanguage.data;
          });
        }
      }
    );
    // load thông tin quá trình diện đối tượng
    this.employeeResolver.EMP_TYPE_PROCESS.subscribe(
      data => {
        if (data) {
          this.empTypeProcessService.getMainProcessByEmployeeId(data).subscribe(resETP => {
              this.empTypeProcess = resETP.data;
          });
        }
      }
    );
    this.employeeResolver.EMPLOYEE.subscribe(
      data => {
        if (data) {
          this.empId = data;
          this.employeeId = new FormControl(data);
          this.employeeId.valueChanges.subscribe(val => {
            this.router.navigate(['/employees/personal', val, router.url.split('/')[4]]);
          });
          this.getEmployeeInfor(data);
        }
      }
    );
  }
  ngOnInit() {
    const params = this.activatedRoute.snapshot.params;
    if (params) {
      this.empId = params.id;
      this.empWorkScheduleService.getEmpWorkScheduleBeanById(this.empId).subscribe(datas => {
        this.lenthEmpWorkSchedule = datas.data.length;
        this.initViewCalendar(datas.data);
      });
    }
  }

  private getEmployeeInfor(data?: any) {
    this.empEducationProcessService.getEducationByEmployeeId(data).subscribe(res1 => {
      if (this.empEducationProcessService.requestIsSuccess(res1) && res1.data) {
        this.empEducationInfo = res1.data;
      }
    });
    this.languageDegreeService.getMainProcessByEmployeeId(data).subscribe(resLanguage => {
      if (this.languageDegreeService.requestIsSuccess(resLanguage) && resLanguage.data) {
        this.empLanguageDegree = resLanguage.data;
      }
    });
    this.workProcessService.getMainProcessByEmployeeId(data).subscribe(resWorkProcess => {
      if (this.workProcessService.requestIsSuccess(resWorkProcess) && resWorkProcess.data) {
        this.empWorkProcess = resWorkProcess.data;
      }
    });
    this.empTypeProcessService.getMainProcessByEmployeeId(data).subscribe(resETP => {
      if (this.empTypeProcessService.requestIsSuccess(resETP) && resETP.data) {
        this.empTypeProcess = resETP.data;

      }
    });
    this.employeeInfoService.getJoinCompanyDate(data).subscribe(resHD => {
      if (this.employeeInfoService.requestIsSuccess(resHD) && resHD.data) {
        this.hireDate = resHD.data;
      }
    });
    this.longLeaveProcessService.getInforLongLeaveProcessStillValidated(data).subscribe(resLongleave => {
        this.longLeaveList = resLongleave;
        this.length = resLongleave.length;
    });
    this.empWorkScheduleService.getEmpWorkScheduleBeanById(data).subscribe(datas => {
      this.lenthEmpWorkSchedule = datas.data.length;
      this.initViewCalendar(datas.data);
    });
  }

  onChooseAvatar() {
    this.fileAvatar.first.nativeElement.click();
  }
  onChangeAvatar(event) {
    const files = event.target.files[0];
    if (files) {
      this.app.confirmMessage(null, () => {
        this.employeeInfoService.updateAvatar(this.empId, files).subscribe(res => {
          if (this.employeeInfoService.requestIsSuccess(res)) {
              this.employeeInfoService.findOne(this.empId).subscribe(res2 => {
                if (this.employeeInfoService.requestIsSuccess(res2)) {
                  this.EmployeeInfo = res2.data;
                }
              });
          }
      });
      }, null);
    }
  }
  public downloadFile(avatarFile) {
    this.fileStorage.downloadFile(avatarFile[0].id)
        .subscribe(res => {
          saveAs(res, avatarFile[0].fileName);
        });
  }

  public onChangeRedirect(event) {
    if (event === this.empEducationInfo || event === this.empLanguageDegree) {
      this.router.navigate(['/employees/personal', this.empId, 'education-process']);
    }
    if (event === this.empWorkProcess) {
      this.router.navigate(['/employees/personal', this.empId, 'work-process']);
    }
    if (event === this.empTypeProcess) {
      this.router.navigate(['/employees/personal', this.empId, 'contract-process']);
    }
    if (event === this.longLeaveList) {
      this.router.navigate(['/employees/personal', this.empId, 'long-leave-process']);
    }
    if (event === 'empWorkSchedule') {
      this.router.navigate(['/employees/personal', this.empId, 'emp-work-schedule']);
    }
  }

  public initViewCalendar(data) {
    if (!data || data.length == 0) {
      this.empCalendar1.showView = false;
      return;
    }
    let checkView = false;
    for (const item of data) {
      if (this.isNowProcess(item.effectiveDate, item.expritedDate)) {
        checkView = true;
        this.viewCalendar(item);
        break;
      }
    }
    if (!checkView) {
      this.empCalendar1.showView = false;
    }
  }
  public isNowProcess(effectiveDateTime, expritedDateTime) {
    const effectiveDate = moment(effectiveDateTime);
    const now = moment();
    if (expritedDateTime) {
      const expritedDate = moment(expritedDateTime);
      if (!now.isBetween(effectiveDate, expritedDate)) {
        return false;
      }
    } else if (now.isBefore(effectiveDate)) {
      return false;
    }
    return true;
  }
  public viewCalendar(item) {
    this.empCalendar1.setEmpWorkSchedule(item.empWorkScheduleId, item.effectiveDate, item.expritedDate);
  }
  moveListEmployeePage() {
    this.router.navigate(['/employees/update-employee']);
  }

  // export báo cáo trích ngang cá nhân
  public exportContract(data?) {
    this.employeeReportService.export2C(data.employeeId).subscribe((res) => {
      if (res.type === 'application/json') {
        const reader = new FileReader();
        reader.addEventListener('loadend', (e) => {
          const text = e.srcElement['result'];
          const json = JSON.parse(text);
          this.employeeReportService.processReturnMessage(json);
        });
        reader.readAsText(res);
      } else {
        const nowDate = moment(new Date()).format('DDMMYYYY');
        saveAs(res, nowDate + "_" + data.fullName + "_" + "2c"+".pdf");
      }
    });
  }
}
