import { AllowanceProcessIndexComponent } from './page/allowance-process-index/allowance-process-index.component';
import { CommonUtils } from './../../../../shared/services/common-utils.service';
import { RESOURCE } from '@app/core';
import { PropertyResolver } from './../../../../shared/services/property.resolver';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: AllowanceProcessIndexComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.EMP_ALLOWANCE_PROCESS,
      nationId: CommonUtils.getNationId()
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AllowanceProcessRoutingModule { }
