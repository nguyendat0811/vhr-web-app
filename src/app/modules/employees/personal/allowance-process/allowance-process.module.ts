import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AllowanceProcessRoutingModule } from './allowance-process-routing.module';
import { SharedModule } from '@app/shared';
import { AllowanceProcessIndexComponent } from './page/allowance-process-index/allowance-process-index.component';
import { AllowanceProcessSearchComponent } from './page/allowance-process-search/allowance-process-search.component';
import { AllowanceProcessFormComponent } from './page/allowance-process-form/allowance-process-form.component';

@NgModule({
  declarations: [AllowanceProcessIndexComponent, AllowanceProcessSearchComponent, AllowanceProcessFormComponent],
  imports: [
    CommonModule,
    SharedModule,

    AllowanceProcessRoutingModule
  ],
  entryComponents: [AllowanceProcessSearchComponent, AllowanceProcessFormComponent],
})
export class AllowanceProcessModule { }
