import { EmployeeResolver } from '@app/shared/services/employee.resolver';
import { FileStorageService } from './../../../../../../core/services/file-storage.service';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RESOURCE, ACTION_FORM, DEFAULT_MODAL_OPTIONS } from '@app/core';
import { EmpAllowanceProcessService } from '@app/core/services/emp-allowance-process/emp-allowance-process.service';
import { AllowanceProcessFormComponent } from '../allowance-process-form/allowance-process-form.component';
import { saveAs } from 'file-saver';

@Component({
  selector: 'allowance-process-search',
  templateUrl: './allowance-process-search.component.html'
})
export class AllowanceProcessSearchComponent extends BaseComponent implements OnInit {
  employeeId: number;
  empId: {employeeId: any};

  constructor(
      private modalService: NgbModal
    , public actr: ActivatedRoute
    , private employeeResolver: EmployeeResolver
    , public empAllowanceProcessService: EmpAllowanceProcessService
    , private fileStorage: FileStorageService) {
      super(actr, RESOURCE.EMP_ALLOWANCE_PROCESS, ACTION_FORM.SEARCH);
      this.setMainService(empAllowanceProcessService);

      this.employeeResolver.EMPLOYEE.subscribe(
        data => {
          if (data) {
            this.employeeId = data;
            this.empId = {employeeId: data};
            this.formSearch =  this.buildForm({employeeId: this.employeeId}, {employeeId: []});
            this.processSearch();
          }
        }
      );
  }

  ngOnInit() {
  }

  /* Action Modal*/
  private activeModel(data?: any) {
    const modalRef = this.modalService.open(AllowanceProcessFormComponent, DEFAULT_MODAL_OPTIONS);
    if (data) {
      modalRef.componentInstance.setFormValue(this.propertyConfigs, data);
      modalRef.componentInstance.loadListAllowanceType();
    }
    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
      if (this.empAllowanceProcessService.requestIsSuccess(result)) {
        this.processSearch();
      }
    });
  }

   /**
   * prepareUpdate
   * param item
   */
  prepareSaveOrUpdate(item) {
    if (item && item.empAllowanceProcessId > 0) {
      this.empAllowanceProcessService.findOne(item.empAllowanceProcessId)
        .subscribe(res => {
          this.activeModel(res.data);
        });
    } else {
      this.activeModel(this.empId);
    }
  }

  public downloadFile(fileData) {
    this.fileStorage.downloadFile(fileData.id).subscribe(res => {
        saveAs(res , fileData.fileName);
    });
  }

}
