import { AllowanceService } from "./../../../../../../core/services/allowance/allowance.service";
import { CommonUtils } from "./../../../../../../shared/services/common-utils.service";
import { FileControl } from "./../../../../../../core/models/file.control";
import { RESOURCE, ACTION_FORM } from "@app/core/app-config";
import { BaseComponent } from "@app/shared/components/base-component/base-component.component";
import { AppComponent } from "@app/app.component";
import { ActivatedRoute } from "@angular/router";
import { FormGroup, Validators } from "@angular/forms";
import { Component, OnInit, ViewChildren } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { EmpAllowanceProcessService } from "@app/core/services/emp-allowance-process/emp-allowance-process.service";
import { ValidationService } from "@app/shared/services";
import { AllowanceTypeService } from "@app/core";

@Component({
  selector: "allowance-process-form",
  templateUrl: "./allowance-process-form.component.html",
})
export class AllowanceProcessFormComponent
  extends BaseComponent
  implements OnInit
{
  formSave: FormGroup;
  allowanceTypeList: any = {};
  allowanceList: any = {};

  @ViewChildren("factor") factor;
  @ViewChildren("money") money;

  formConfig = {
    empAllowanceProcessId: [null],
    allowanceId: [null, [ValidationService.required]],
    employeeId: [null],
    decideCode: [
      null,
      [ValidationService.required, ValidationService.maxLength(50)],
    ],
    decideDate: [null, [ValidationService.required]],
    effectiveDate: [null, [ValidationService.required]],
    expiredDate: [null],
    allowanceTypeId: [null, [ValidationService.required]],
    factor: [null],
    money: [null],
    description: [null, [ValidationService.maxLength(1000)]],
  };

  constructor(
    public activeModal: NgbActiveModal,
    public actr: ActivatedRoute,
    private empAllowanceProcessService: EmpAllowanceProcessService,
    private allowanceService: AllowanceService,
    private allowanceTypeService: AllowanceTypeService,
    private app: AppComponent
  ) {
    super(actr, RESOURCE.EMP_ALLOWANCE_PROCESS, ACTION_FORM.INSERT);

    this.formSave = this.buildForm({}, this.formConfig, ACTION_FORM.INSERT, [
      ValidationService.notAffter(
        "effectiveDate",
        "expiredDate",
        "app.allowanceProcess.expiredDate"
      ),
      ValidationService.notAffter(
        "decideDate",
        "effectiveDate",
        "app.allowanceProcess.effectiveDate"
      ),
    ]);
    this.formSave.addControl("file", new FileControl(null));

    this.allowanceTypeService.findAllowanceTypeList().subscribe((res) => {
      this.allowanceTypeList = res.data;
    });
  }

  ngOnInit() {}

  /**
   * processSaveOrUpdate
   */
  processSaveOrUpdate() {
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    }
    this.app.confirmMessage(
      "app.empTradeUnion.finishPreviousProcess",
      () => {
        this.empAllowanceProcessService
          .saveOrUpdateFormFile(this.formSave.value)
          .subscribe((res) => {
            if (this.empAllowanceProcessService.requestIsSuccess(res)) {
              this.activeModal.close(res);
            }
          });
      },
      () => {}
    );
  }

  public onAllowanceTypeChange(event) {
    this.formSave.get("allowanceId").setValue("");
    this.formSave.get("factor").setValue("");
    this.formSave.get("money").setValue("");
    if (this.formSave.get("allowanceTypeId").value) {
      this.empAllowanceProcessService
        .getAllowanceList(this.formSave.get("allowanceTypeId").value)
        .subscribe((res) => (this.allowanceList = res.data));
    } else {
      this.allowanceList = [];
    }
  }

  public onAllowanceChange(event) {
    this.formSave.get("factor").setValue("");
    this.formSave.get("money").setValue("");
    this.allowanceService
      .findOne(this.formSave.get("allowanceId").value)
      .subscribe((res) => {
        if (this.formSave.get("allowanceId").value) {
          this.formSave.get("factor").setValue(res.data.factor);
          this.formSave.get("money").setValue(res.data.amountMoney);
        } else {
          this.formSave.get("factor").setValue(null);
          this.formSave.get("money").setValue(null);
        }
      });
  }

  public loadListAllowanceType() {
    if (this.formSave.get("allowanceId").value) {
      this.empAllowanceProcessService
        .getAllowanceTypeList(this.formSave.get("allowanceId").value)
        .subscribe((res) => {
          this.formSave
            .get("allowanceTypeId")
            .setValue(res.data[0]["allowanceTypeId"]);

          if (this.formSave.get("allowanceTypeId").value) {
            this.empAllowanceProcessService
              .getAllowanceList(this.formSave.get("allowanceTypeId").value)
              .subscribe((res1) => {
                this.allowanceList = res1.data;
                console.log(res1.data);
              });
          }
        });
    }
  }

  // public loadListAllowance() {
  //   if (this.formSave.get('allowanceTypeId').value) {
  //     this.empAllowanceProcessService.getAllowanceList(this.formSave.get('allowanceTypeId').value).subscribe(res => {
  //       this.allowanceList = res.data;
  //       console.log(res.data)
  //       }
  //     );
  //   }
  // }

  get f() {
    return this.formSave.controls;
  }

  public setFormValue(propertyConfigs: any, data?: any) {
    this.propertyConfigs = propertyConfigs;
    if (data && data.empAllowanceProcessId > 0) {
      this.formSave = this.buildForm(
        data,
        this.formConfig,
        ACTION_FORM.UPDATE,
        [
          ValidationService.notAffter(
            "effectiveDate",
            "expiredDate",
            "app.allowanceProcess.expiredDate"
          ),
          ValidationService.notAffter(
            "decideDate",
            "effectiveDate",
            "app.allowanceProcess.effectiveDate"
          ),
        ]
      );

      // if (this.formSave.get('salaryRegulateId').value) {
      //   this.empAllowanceProcessService.getAllowanceTypeList(this.formSave.get('allowanceId').value).subscribe(
      //     res => this.allowanceList = res.data
      //   );
      // }
    } else {
      this.formSave = this.buildForm(
        data,
        this.formConfig,
        ACTION_FORM.INSERT,
        [
          ValidationService.notAffter(
            "effectiveDate",
            "expiredDate",
            "app.allowanceProcess.expiredDate"
          ),
          ValidationService.notAffter(
            "decideDate",
            "effectiveDate",
            "app.allowanceProcess.effectiveDate"
          ),
        ]
      );
    }

    this.formSave.addControl("file", new FileControl(null));
    if (data.fileAttachment) {
      (this.formSave.controls["file"] as FileControl).setFileAttachment(
        data.fileAttachment.empAllowanceProcessFile
      );
    }
  }
}
