import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { RESOURCE } from '@app/core';

@Component({
  selector: 'emp-position-salary-index',
  templateUrl: './emp-position-salary-index.component.html'
})
export class EmpPositionSalaryIndexComponent extends BaseComponent implements OnInit {

  constructor(public act: ActivatedRoute) { 
    super(act, RESOURCE.EMP_POSITION_SALARY);
  }
  ngOnInit() {
  }

}
