import { EmpPositionSalaryIndexComponent } from './emp-position-salary-index/emp-position-salary-index.component';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { PropertyResolver } from './../../../../shared/services/property.resolver';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RESOURCE } from '@app/core';

const routes: Routes = [
  {
    path: '',
    component: EmpPositionSalaryIndexComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.EMP_POSITION_SALARY,
      nationId: CommonUtils.getNationId()
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmpPositionSalaryRoutingModule { }
