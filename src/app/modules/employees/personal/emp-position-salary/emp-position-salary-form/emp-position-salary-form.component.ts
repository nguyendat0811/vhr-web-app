import { EmpPositionSalaryService } from './../../../../../core/services/emp-position-salary/emp-position-salary.service';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { FileControl } from '@app/core/models/file.control';
import { AppComponent } from '@app/app.component';
import { ActivatedRoute } from '@angular/router';
import { ACTION_FORM, APP_CONSTANTS } from './../../../../../core/app-config';
import { RESOURCE } from '@app/core/app-config';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { Component, OnInit, ViewChildren } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SysCatService, SalaryTableService, SalaryStepService, SalaryGradeService } from '@app/core';
import { ValidationService } from '@app/shared/services';

@Component({
  selector: 'emp-position-salary-form',
  templateUrl: './emp-position-salary-form.component.html'
})
export class EmpPositionSalaryFormComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  salaryRegulateList: any = {}; // Quy che luong
  salaryGradeLadderList: any = {}; // Bang luong
  salaryGradeRateList: any = {}; // ngach luong
  salaryGradeStepList: any = {}; // Cach tinh luong
  gradeLadderType: number; // Loai bang luong. 1: tinh theo grade; 2: tinh theo step
  totalSalaryLength: any = {};
  baseSalaryLength: any = {};
  bonus13thMonthLength: any = {};
  bonusYearLength: any = {};
  fileControl = new FileControl(null);
  @ViewChildren('factorNumber') factorNumber;
  @ViewChildren('agreeSalary') agreeSalary;
  // @ViewChildren('salary') salary;

  formConfig = {
    empPositionSalaryProcessId: [null],
    employeeId: [null],
    decideCode: [null, [ValidationService.required, ValidationService.maxLength(50)]],
    decideDate: [null, [ValidationService.required]],
    effectiveDate: [null, [ValidationService.required]],
    expiredDate: [null],
    raiseDate: [null, [ValidationService.required]],
    agreeSalary: [null],
    salary: [null],
    bonus: [null, [ValidationService.positiveNumber, ValidationService.maxLength(9)]],
    factorNumber: [null],
    percent: [null, [ValidationService.positiveNumber, ValidationService.maxLength(5), Validators.max(100)]],
    description: [null, [ValidationService.maxLength(500)]],
    salaryRegulateId: [null, [ValidationService.required]],
    salaryLadderId: [null, [ValidationService.required]],
    salaryRateId: [null, [ValidationService.required]],
    salaryGradeStepId: [null, [ValidationService.required]],
    percentYearExcessive: [null, [ValidationService.maxLength(5)]],
    yearExcessiveDate: [null],
    positionSalaryId: [null],
    totalSalary: [null],
    baseSalary: [null],
    bonus13thMonth: [null],
    bonusYear: [null],
  };

  formGrade = {
    empPositionSalaryProcessId: [null],
    employeeId: [null],
    decideCode: [null, [ValidationService.required, ValidationService.maxLength(50)]],
    decideDate: [null, [ValidationService.required]],
    effectiveDate: [null, [ValidationService.required]],
    expiredDate: [null],
    raiseDate: [null, [ValidationService.required]],
    percent: [null, [ValidationService.positiveNumber, ValidationService.maxLength(5), Validators.max(100)]],
    bonus: [null, [ValidationService.positiveNumber, ValidationService.maxLength(9)]],
    description: [null, [ValidationService.maxLength(500)]],
    salaryRegulateId: [null, [ValidationService.required]],
    salaryLadderId: [null, [ValidationService.required]],
    salaryRateId: [null, [ValidationService.required]],
    salaryGradeStepId: [null, [ValidationService.required]],
    percentYearExcessive: [null, [ValidationService.maxLength(5)]],
    yearExcessiveDate: [null],
    positionSalaryId: [null],
    totalSalary: [null],
    baseSalary: [null],
    bonus13thMonth: [null],
    bonusYear: [null],
  };

  constructor(public activeModal: NgbActiveModal
    , public salaryStepService: SalaryStepService
    , public actr: ActivatedRoute
    , private empPositionSalaryService: EmpPositionSalaryService
    , private sysCatService: SysCatService
    , private app: AppComponent
    , private salaryTableService: SalaryTableService
    , private salaryGradeService: SalaryGradeService) {
    super(actr, RESOURCE.EMP_POSITION_SALARY, ACTION_FORM.INSERT);

    this.buildForms({}, ACTION_FORM.INSERT);
  }

  ngOnInit() {
  }

  get f () {
    return this.formSave.controls;
  }

  /**
    * buildForm
    */
  private buildForms(data?: any,  actionForm?: ACTION_FORM): void {
    this.formSave = this.buildForm(data, this.formConfig
      , actionForm
      , [ ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.salaryTable.expiredDate')
      , ValidationService.notAffter('decideDate', 'effectiveDate', 'app.salaryTable.effectiveDate')]);

      this.actionChangePositionSalary();
  }

  private buildFormGrade(data?: any,  actionForm?: ACTION_FORM) {
    this.formSave = this.buildForm(data, this.formGrade
      , actionForm
      , [ ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.salaryTable.expiredDate')
      , ValidationService.notAffter('decideDate', 'effectiveDate', 'app.salaryTable.effectiveDate')]);
      this.actionChangePositionSalary();
  }

    /**
   * setFormValue
   * param data
   */
  public setFormValue(propertyConfigs: any, data?: any) {
    this.propertyConfigs = propertyConfigs;
    if (data && data.empPositionSalaryProcessId > 0) {
      this.buildForms(data, ACTION_FORM.UPDATE);
      this.gradeLadderType = data.gradeLadderType;
      if (data.positionSalaryId) {
        this.actionLoadPositionSalaryRegulate(data.positionSalaryId);
        if (data.salaryRegulateId) {
          this.actionLoadPositionGradeLadder(data.positionSalaryId, data.salaryRegulateId);
        }
        if (data.salaryLadderId) {
          this.actionLoadPositionGradeRate(data.positionSalaryId, data.salaryLadderId);
        }
        if (data.salaryRateId) {
          this.loadListSalaryGradeStep();
        }

      } else {
        this.actionLoadSalaryRegulate();
        if (data.salaryRegulateId) {
          this.actionLoadGradeLadder(data.salaryRegulateId);
        }
        if (data.salaryLadderId) {
          this.actionLoadGradeRate(data.salaryLadderId);
        }
        if (data.salaryRateId) {
          this.loadListSalaryGradeStep();
        }
      }
      // hiển thị hệ các giá trị các trường hiển thị dựa vào việc chọn bậc lương !
      this.onSalaryGradeStepChange(data.salaryGradeStepId, data);

      // hien thi Hệ số chức danh, Lương thỏa thuận
      // if (this.gradeLadderType === 1 && data.salaryGradeStepId) {
      //   this.salaryStepService.findOne(data.salaryGradeStepId).subscribe( res => {
      //     this.factorNumber.first.nativeElement.value = res.data.factor;
      //     this.agreeSalary.first.nativeElement.value = res.data.money;
      //   });
      // }

    } else {
      this.buildForms(data, ACTION_FORM.INSERT);
      this.actionLoadSalaryRegulate();
    }

    if (data.fileAttachment) {
      this.fileControl.setFileAttachment(data.fileAttachment.empPositionSalaryProcessFile);
    }
  }

  /**
   * processSaveOrUpdate
   */
  processSaveOrUpdate() {
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    }

    this.formSave.addControl('file', this.fileControl);
    this.app.confirmMessage('app.empTradeUnion.finishPreviousProcess', () => {
      this.empPositionSalaryService.saveOrUpdateFormFile(this.formSave.value)
        .subscribe(res => {
          if (this.empPositionSalaryService.requestIsSuccess(res)) {
            this.activeModal.close(res);
          }
        });
    }, () => {

    });
  }

  /**
   * Xu ly khi thay doi quy che luong
   */
  public onSalaryRegulateChange(event) {
    // Start Xu ly set lai gia tri mac dinh cua cac truong phu thuoc
    this.formSave.get('salaryLadderId').setValue(null);
    this.formSave.get('salaryRateId').setValue(null);
    this.formSave.get('salaryGradeStepId').setValue(null);
    this.salaryGradeLadderList = [];
    this.salaryGradeRateList = [];
    this.salaryGradeStepList = [];
    this.gradeLadderType = null;
    // End

    if (event) {
      const positionId = this.formSave.get('positionSalaryId').value;
      if (positionId) {
        this.actionLoadPositionGradeLadder(positionId, event);
      } else {
        this.actionLoadGradeLadder(event);
      }
    }
  }

  /**
   * Xu ly khi thay doi bang luong
   */
  public onSalaryGradeLadderChange(event) {
    // Start Xu ly set lai gia tri mac dinh cua cac truong phu thuoc
    this.formSave.get('salaryRateId').setValue('');
    this.formSave.get('salaryGradeStepId').setValue('');
    this.salaryGradeRateList = [];
    this.salaryGradeStepList = [];
    this.gradeLadderType = null;
    // End

    if (event) {
      this.gradeLadderType = this.salaryGradeLadderList.find(x => x.salaryTableId === event).type;
      const positionId = this.formSave.get('positionSalaryId').value;
      if (positionId) {
        this.actionLoadPositionGradeRate(positionId, event);
      } else {
        this.actionLoadGradeRate(event);
      }
    }
  }

  public onSalaryGradeRateChange(event) {
    // Start Xu ly set lai gia tri mac dinh cua cac truong phu thuoc
    this.formSave.get('salaryGradeStepId').setValue('');
    this.salaryGradeStepList = [];
    // End

    if (event) {
      this.loadListSalaryGradeStep();
    }
  }

  public reInitFormSave(nameControl, min, max, currentValue) {
    this.formSave.removeControl(nameControl);
    this.formSave.addControl(nameControl,
    CommonUtils.createControl(this.actionForm, nameControl, null
    , [ValidationService.positiveNumber, Validators.min(min), Validators.max(max)], this.propertyConfigs));
    this.formSave.controls[nameControl].setValue(currentValue);

    if (nameControl === 'totalSalary') {
      this.totalSalaryLength = max.toString().length;
    } else if (nameControl === 'baseSalary') {
      this.baseSalaryLength = max.toString().length;
    } else if (nameControl === 'bonus13thMonth') {
      this.bonus13thMonthLength = max.toString().length;
    } else if (nameControl === 'bonusYear') {
      this.bonusYearLength = max.toString().length;
    }
  }

  public onSalaryGradeStepChange(event, data?) {
    const formDefaultValue = this.formSave.value;
    if (!this.formSave.get('salaryLadderId').value) {
      return;
    }
    if (event) {
      if (this.gradeLadderType === 1) { // Theo step
        this.buildForms(formDefaultValue, null);
        this.formSave.removeControl('totalSalary');
        this.formSave.removeControl('baseSalary');
        this.formSave.removeControl('bonus13thMonth');
        this.formSave.removeControl('bonusYear');

        this.salaryStepService.findOne(event).subscribe( res => {
          this.factorNumber.first.nativeElement.value = res.data.factor;
          this.agreeSalary.first.nativeElement.value = res.data.money;
        });
      } else { // Theo grade
        this.buildFormGrade(formDefaultValue, null);
        this.formSave.removeControl('factorNumber');
        this.formSave.removeControl('agreeSalary');
        this.formSave.removeControl('salary');
        this.formSave.removeControl('bonus');
        if (data) {
          this.salaryGradeService.findById(event).subscribe( res => {
            const condi = res.data;
            this.reInitFormSave('totalSalary', condi.totalSalaryMin, condi.totalSalaryMax, data.totalSalary);
            this.reInitFormSave('baseSalary', condi.baseSalaryMin, condi.baseSalaryMax, data.baseSalary);
            this.reInitFormSave('bonus13thMonth', condi.bonus13thMonthMin, condi.bonus13thMonthMax, data.bonus13thMonth);
            this.reInitFormSave('bonusYear', condi.bonusYearMin, condi.bonusYearMax, data.bonusYear);
          });
        } else {
          this.salaryGradeService.findById(event).subscribe( res => {
            const condi = res.data;
            this.reInitFormSave('totalSalary', condi.totalSalaryMin, condi.totalSalaryMax, null);
            this.reInitFormSave('baseSalary', condi.baseSalaryMin, condi.baseSalaryMax, null);
            this.reInitFormSave('bonus13thMonth', condi.bonus13thMonthMin, condi.bonus13thMonthMax, null);
            this.reInitFormSave('bonusYear', condi.bonusYearMin, condi.bonusYearMax, null);
          });
        }
      }
    }
  }

  public loadFactorAngreeSalary() {
    if (this.formSave.get('salaryLadderId').value) {
    this.salaryTableService.getTypeById(this.formSave.get('salaryLadderId').value)
      .subscribe(res => {
        if (this.formSave.get('salaryGradeStepId').value && res.data[0]['type'] === 1) {
          this.salaryStepService.findOne(this.formSave.get('salaryGradeStepId').value).subscribe(res1 => {
            this.factorNumber.first.nativeElement.value = res1.data.factor;
            this.agreeSalary.first.nativeElement.value = res1.data.money;
          });
        }
      });
    }
  }

  /**
   * Xu ly khi chon lai chuc danh huong luong
   */
  private actionChangePositionSalary() {
    this.formSave.get('positionSalaryId').valueChanges.subscribe(val => {
      this.formSave.get('salaryRegulateId').setValue('');
      this.onSalaryRegulateChange(null);
      if (val) { // Xu ly khi chon chuc danh
        this.actionLoadPositionSalaryRegulate(val);
      } else { // xu ly khi khong chon chuc danh
        this.actionLoadSalaryRegulate();
      }
    });
  }

  /**
   * Xu ly lay tat ca quy che luong theo thi truong
   */
  private actionLoadSalaryRegulate() {
    this.salaryTableService.findByParentId().subscribe(
      res => this.salaryRegulateList = res.data
    );
  }

  /**
    * Xu ly lay tat ca bang luong
    */
  private actionLoadGradeLadder(salaryRegulateId: any) {
    this.salaryTableService.findByParentId(salaryRegulateId).subscribe(
      res => this.salaryGradeLadderList = res.data
    );
  }

  /**
  * Xu ly lay tat ca ngach luong
  */
  private actionLoadGradeRate(salaryGradeLadderId: any) {
    this.salaryTableService.findByParentId(salaryGradeLadderId).subscribe(
      res => this.salaryGradeRateList = res.data
    );
  }

  /**
   * Xu ly lay danh sach quy che luong theo chuc danh
   */
  private actionLoadPositionSalaryRegulate(positionSalaryId?: any) {
    this.salaryTableService.findRegulateByPosition(positionSalaryId).subscribe(
      res => this.salaryRegulateList = res.data
    );
  }

  /**
  * Xu ly lay danh sach bang luong theo chuc danh
  */
  private actionLoadPositionGradeLadder(positionSalaryId: any, salaryRegulateId: any) {
    this.salaryTableService.findGradeLadderByPosition(positionSalaryId, salaryRegulateId).subscribe(
      res => this.salaryGradeLadderList = res.data
    );
  }

  /**
  * Xu ly lay danh sach ngach luong theo chuc danh
  */
  private actionLoadPositionGradeRate(positionSalaryId: any, salaryGradeLadderId: any) {
    this.salaryTableService.findGradeRateByPosition(positionSalaryId, salaryGradeLadderId).subscribe(
      res => this.salaryGradeRateList = res.data
    );
  }

    // Xu ly lay danh sach cach tinh luong theo dai hoac buoc
  private loadListSalaryGradeStep() {
    this.salaryTableService.getSalaryStepGrade(this.formSave.get('salaryLadderId').value, this.formSave.get('salaryRateId').value)
      .subscribe(
        res => this.salaryGradeStepList = res.data
      );
  }
}
