import { EmpPositionSalaryRoutingModule } from './emp-position-salary-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@app/shared';
import { EmpPositionSalaryIndexComponent } from './emp-position-salary-index/emp-position-salary-index.component';
import { EmpPositionSalarySearchComponent } from './emp-position-salary-search/emp-position-salary-search.component';
import { EmpPositionSalaryFormComponent } from './emp-position-salary-form/emp-position-salary-form.component';

@NgModule({
  declarations: [EmpPositionSalaryIndexComponent, EmpPositionSalarySearchComponent, EmpPositionSalaryFormComponent],
  imports: [
    CommonModule,
    SharedModule,

    EmpPositionSalaryRoutingModule
  ],
  entryComponents: [EmpPositionSalarySearchComponent, EmpPositionSalaryFormComponent],
})
export class EmpPositionSalaryModule { }
