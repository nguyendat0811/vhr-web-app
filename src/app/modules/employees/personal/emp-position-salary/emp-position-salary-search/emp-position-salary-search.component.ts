import { EmpPositionSalaryFormComponent } from './../emp-position-salary-form/emp-position-salary-form.component';
import { EmployeeResolver } from '@app/shared/services/employee.resolver';
import { EmpPositionSalaryService } from '@app/core/services/emp-position-salary/emp-position-salary.service';
import { ActivatedRoute } from '@angular/router';
import { AppComponent } from '@app/app.component';
import { FileStorageService } from '@app/core/services/file-storage.service';
import { ACTION_FORM, DEFAULT_MODAL_OPTIONS } from '@app/core/app-config';
import { BaseComponent } from './../../../../../shared/components/base-component/base-component.component';
import { Component, OnInit } from '@angular/core';
import { RESOURCE, SalaryTableService } from '@app/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { saveAs } from 'file-saver';
import { CommonUtils } from '@app/shared/services/common-utils.service';

@Component({
  selector: 'emp-position-salary-search',
  templateUrl: './emp-position-salary-search.component.html'
})
export class EmpPositionSalarySearchComponent extends BaseComponent implements OnInit {
  employeeId: number;
  empId: {employeeId: any};

  constructor(
    private modalService: NgbModal
    , private fileStorage: FileStorageService
    , private empPositionSalaryService: EmpPositionSalaryService
    , private employeeResolver: EmployeeResolver
    , private salaryTableService: SalaryTableService
    , private app: AppComponent
    , public actr: ActivatedRoute) {
    super(actr, RESOURCE.EMP_POSITION_SALARY, ACTION_FORM.SEARCH);
    this.setMainService(empPositionSalaryService);

    this.employeeResolver.EMPLOYEE.subscribe(
      data => {
        if (data) {
          this.employeeId = data;
          this.empId = {employeeId: data};
          this.formSearch =  this.buildForm({employeeId: this.employeeId}, {employeeId: []});
          this.processSearch();
        }
      }
    );
    }

  ngOnInit() {
  }

  /* Action Modal*/
  private activeModal(data?: any) {
    const modalRef = this.modalService.open(EmpPositionSalaryFormComponent, DEFAULT_MODAL_OPTIONS);
    if (data) {
      modalRef.componentInstance.setFormValue(this.propertyConfigs, data);
    }
    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
      if (this.empPositionSalaryService.requestIsSuccess(result)) {
        this.processSearch();
      }
    });
  }

     /**
   * prepareUpdate
   * param item
   */
  prepareSaveOrUpdate(item) {
    if (item && item.empPositionSalaryProcessId > 0) {
      this.empPositionSalaryService.findOne(item.empPositionSalaryProcessId)
        .subscribe(res => {
          this.activeModal(res.data);
      });
    } else {
      this.activeModal(this.empId);
    }
  }

  public downloadFile(fileData) {
    this.fileStorage.downloadFile(fileData.id).subscribe(res => {
        saveAs(res , fileData.fileName);
    });
  }
}
