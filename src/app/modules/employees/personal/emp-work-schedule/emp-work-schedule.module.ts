import { SharedModule } from '@app/shared';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmpWorkScheduleRoutingModule } from './emp-work-schedule-routing.module';
import { EmpWorkSchedulePageComponent } from './emp-work-schedule-page/emp-work-schedule-page.component';
import { EmpWorkScheduleListComponent } from './emp-work-schedule-list/emp-work-schedule-list.component';
import { EmpWorkScheduleCalendarComponent } from './emp-work-schedule-calendar/emp-work-schedule-calendar.component';
import { EmpWorkScheduleSetup } from './emp-work-schedule-calendar/emp-work-schedule-setup.component';

@NgModule({
  declarations: [EmpWorkSchedulePageComponent, EmpWorkScheduleListComponent, EmpWorkScheduleCalendarComponent, EmpWorkScheduleSetup],
  imports: [
    CommonModule,
    SharedModule,

    EmpWorkScheduleRoutingModule,
  ],
  entryComponents: [EmpWorkScheduleSetup]
})
export class EmpWorkScheduleModule { }
