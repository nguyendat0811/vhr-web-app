import { MEDIUM_MODAL_OPTIONS } from '@app/core/app-config';
import { MenuItem } from 'primeng/api';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { EmpWorkScheduleService } from '@app/core/services/hr-timekeeping/emp-work-schedule.service';
import { AppComponent } from '@app/app.component';
import { TranslationService } from 'angular-l10n';
import { CommonUtils } from '@app/shared/services';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EmpWorkScheduleSetup } from './emp-work-schedule-setup.component';

class ItemWorking {
  isDisabled: boolean;
  isPass: boolean;
  dateTimeKeeping: any;
  menuItems: MenuItem[];
  isWork: number;
  workdayTypeCode: string;
  hours: number;
  workdayType: string;
}
@Component({
  selector: 'emp-work-schedule-calendar',
  templateUrl: './emp-work-schedule-calendar.component.html',
})
export class EmpWorkScheduleCalendarComponent implements OnInit {
  public  toolFirst;
  public  toolPrev;
  public  toolNext;
  public  toolLast;
  public  firstDayOfMonth;
  public  empTimekeeping = {};
  public  listWeek = [];
  public  menuItems: MenuItem[];
  private empWorkScheduleId;
  private fromDate;
  private toDate;
  private numOfDays = 42;
  public  mapWorkingDays;
  public  mapOffWork;
  public  today;
  private month: string;
  public showView = false;
  constructor(private service: EmpWorkScheduleService
    , private translation: TranslationService
    , private modalService: NgbModal
    , private app: AppComponent) {
    this.today = moment(moment().format('DD/MM/YYYY'), 'DD/MM/YYYY').toDate().getTime();
    this.month = moment().format('MM/YYYY');
  }
  setEmpWorkScheduleId(empWorkScheduleId, effectiveDateTime, expritedDateTime) {
    this.empWorkScheduleId = empWorkScheduleId;
    const effectiveDate = moment(effectiveDateTime);
    const now = moment();
    let month = moment().format('MM/YYYY');
    if (expritedDateTime) {
      const expritedDate = moment(expritedDateTime);
      if (!now.isBetween(effectiveDate, expritedDate)) {
        month = effectiveDate.format('MM/YYYY');
      }
    } else if (now.isBefore(effectiveDate)) {
      month = effectiveDate.format('MM/YYYY');
    }
    this.month = month;
    this.loadEmpWorkCalendar(this.month);
  }
  ngOnInit() {
  }
  public loadEmpWorkCalendar(month: string) {
    this.firstDayOfMonth = moment('01/' + month, 'DD/MM/YYYY');
    const firstDayOfWeek = moment('01/' + month, 'DD/MM/YYYY').startOf('week');
    this.fromDate = firstDayOfWeek.clone().toDate().getTime();
    this.toDate = firstDayOfWeek.clone().add(this.numOfDays, 'day').toDate().getTime();
    this.service.findEmpCalendar(this.empWorkScheduleId, this.fromDate, this.toDate).subscribe(
      res => {
        if (this.service.requestIsSuccess(res)) {
          this.initMapCalendarSchedule(res.data);
          this.initCalendar(month);
          this.initTooling(res.data, month);
          this.empTimekeeping = res.data.empTimekeeping;
          this.showView = true;
        }
      }
    );
  }

  /**
   * initMapCalendarSchedule
   * @ param data
   */
  private initMapCalendarSchedule(data: any): any {
    this.mapOffWork = {};
    for (const workOff of data.lstEmpWorkOff) {
      this.mapOffWork[workOff.dateTimekeeping] = workOff;
    }
    this.mapWorkingDays = {};
    for (const calendar of data.lstCalendar) {
      this.mapWorkingDays[calendar.dateTimekeeping] = calendar;
    }
  }
  /**
   * caculatorSchedule
   * @ param data
   */
  private caculatorSchedule(item: any, month: string): ItemWorking {
    const key = item.toDate().getTime();
    const itemWorking = new ItemWorking();
    itemWorking.isDisabled = item.format('MM/YYYY') !== month;
    itemWorking.isPass = item.toDate().getTime() < this.today;
    itemWorking.dateTimeKeeping = item;
    const offWork = {
      label: this.translation.translate('app.empWorkSchedule.offWork') // thiet lap nghi
    , icon: 'fa danger fa-times'
    , command: () => { this.offWork(key); }
    };
    const onWorkByUnit = {
      label: this.translation.translate('app.empWorkSchedule.onWorkUnit') // Thiết lập đi làm theo đơn vị
    , icon: 'fa danger fa-times'
    , command: () => { this.onWorkUnit(key); }
    };
    const onWorkSpecial = {
      label: this.translation.translate('app.empWorkSchedule.onWorkSpecial') // Thiết lập ngày công
    , icon: 'fa danger fa-times'
    , command: () => { this.onWorkSpecial(key); }
    };
    if (this.mapWorkingDays.hasOwnProperty(key) && this.mapOffWork.hasOwnProperty(key)) {
      itemWorking.isWork = this.mapOffWork[key].isWork;
      itemWorking.workdayTypeCode = this.mapOffWork[key].workdayTypeCode;
      itemWorking.hours = this.mapOffWork[key].hours;
      itemWorking.workdayType = this.mapOffWork[key].workdayTypeCode + ':' + this.mapOffWork[key].hours;
      if (itemWorking.isWork === 1) {// dang duoc thiet lap di lam dac biet
        itemWorking.menuItems = [offWork, onWorkByUnit, onWorkSpecial];
      } else if (itemWorking.isWork === 0) {
        itemWorking.menuItems = [onWorkByUnit, onWorkSpecial];
      }

    } else if (this.mapWorkingDays.hasOwnProperty(key)) {
        itemWorking.isWork = 1;
        itemWorking.workdayTypeCode = this.mapWorkingDays[key].workdayTypeCode;
        itemWorking.hours = this.mapWorkingDays[key].hours;
        itemWorking.workdayType = this.mapWorkingDays[key].workdayTypeCode + ':' + this.mapWorkingDays[key].hours;
        itemWorking.menuItems = [offWork, onWorkSpecial];

      } else if (this.mapOffWork.hasOwnProperty(key)) {
        itemWorking.isWork = this.mapOffWork[key].isWork;
        itemWorking.workdayTypeCode = this.mapOffWork[key].workdayTypeCode;
        itemWorking.hours = this.mapOffWork[key].hours;
        itemWorking.workdayType = this.mapOffWork[key].workdayTypeCode + ':' + this.mapOffWork[key].hours;
        if (itemWorking.isWork === 1) {// dang duoc thiet lap di lam dac biet
          itemWorking.menuItems = [offWork, onWorkSpecial];
        } else if (itemWorking.isWork === 0) {
          itemWorking.menuItems = [onWorkSpecial];
        }
      } else {
        itemWorking.isWork = 0;
        itemWorking.menuItems = [onWorkSpecial];
      }
      return itemWorking;
  }
  private initCalendar(month: string) {
    this.listWeek = [];
    const firstDayOfWeek = moment('01/' + month, 'DD/MM/YYYY').startOf('week');
    for (let i = 0; i < this.numOfDays; i++) {
      // const t = moment('01/' + month, 'DD/MM/YYYY').startOf('week');
      let weekNum = i / 7;
      weekNum = parseInt('' + weekNum);
      let current = [];
      if (this.listWeek.length > weekNum) {
        current = this.listWeek[weekNum];
      } else {
        this.listWeek[weekNum] = current;
      }
      const item = firstDayOfWeek.clone().add(i, 'day');
      // Them xu ly init cac bien tu database
      const itemWorking = this.caculatorSchedule(item, month);
      current.push(itemWorking);
      // End
      this.listWeek[weekNum] = current;
    }
    console.log(this.listWeek);
  }
  initTooling(data, month) {
    const currentMonthDate = moment('01/' + month, 'DD/MM/YYYY').endOf('month');
    this.toolFirst = null;
    this.toolPrev = null;
    this.toolNext = null;
    this.toolLast = null;
    const effectiveDate = moment(data.effectiveDate);
    const firstMonth = effectiveDate.format('MM/YYYY');
    if (firstMonth !== month) {
      this.toolFirst = {month: firstMonth};
      this.toolPrev = {month: currentMonthDate.clone().add(-1, 'month').format('MM/YYYY')};
    }
    if (!data.expiredDate) {
      this.toolNext = {month: currentMonthDate.clone().add(1, 'month').format('MM/YYYY')};
    } else {
      const expiredDate = moment(data.expiredDate);
      if (!currentMonthDate.isAfter(expiredDate)) {
        this.toolNext = {month: currentMonthDate.clone().add(1, 'month').format('MM/YYYY')};
        this.toolLast = {month: expiredDate.format('MM/YYYY')};
      }
    }
  }
  goTo(tool) {
    this.month = tool.month;
    this.loadEmpWorkCalendar(this.month);
  }
  printEmpTimekeeping(dateTimekeeping, showShotcut?: boolean): string {
    const lst = this.empTimekeeping[dateTimekeeping];
    if (!lst) {
      return '';
    }
    if (showShotcut) {
      let tem = lst.join(', ');
      if (tem && tem.length > 3) {
        tem = tem.substring(0, 3) + '...';
      }
      return tem;
    } else {
      return lst.join(', ');
    }
  }
  onWork(dateTimekeeping) {
    this.app.confirmMessage(null
      , () => {
        this.processUpdateOnOffWorkingDay(dateTimekeeping, 1);
      }, () => {
      }
    );
  }
  onWorkUnit(dateTimekeeping) {
    this.app.confirmMessage(null
      , () => {
        this.processUpdateOnOffWorkingDay(dateTimekeeping, 2);
      }, () => {
      }
    );
  }
  onWorkSpecial(dateTimekeeping) {
    const modalRef = this.modalService.open(EmpWorkScheduleSetup, MEDIUM_MODAL_OPTIONS);
    this.service.findEmpWorkOff(this.empWorkScheduleId, dateTimekeeping).subscribe(res => {
      modalRef.componentInstance.setFormValue(this.empWorkScheduleId, dateTimekeeping, res.data);
    });
    modalRef.result.then((result) => {
      this.loadEmpWorkCalendar(this.month);
    });
  }
  offWork(dateTimekeeping) {
    this.app.confirmMessage(null
      , () => {
        this.processUpdateOnOffWorkingDay(dateTimekeeping, 0);
      }, () => {
      }
    );
  }
  processUpdateOnOffWorkingDay(dateTimekeeping, isWork) {
    const data = {'dateTimekeeping': dateTimekeeping, 'isWork': isWork};
    this.service.processOnOffWorkingDay(this.empWorkScheduleId, data).subscribe(
      res => {
        if (this.service.requestIsSuccess(res)) {
          this.loadEmpWorkCalendar(this.month);
        }
      }
    );
  }
  openCm(event, cm, itemWorking: ItemWorking) {
    if (CommonUtils.havePermission('action.update', 'resource.empWorkSchedule')) {
      if (itemWorking.isDisabled || itemWorking.isPass) {
        return;
      }
      this.menuItems = itemWorking.menuItems;
      event.preventDefault();
      event.stopPropagation();
      cm.toggle(event);
      return false;
    } else {
      return true;
    }
  }
}
