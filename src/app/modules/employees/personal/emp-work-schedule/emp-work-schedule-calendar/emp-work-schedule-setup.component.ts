import { ValidationService } from './../../../../../shared/services/validation.service';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { HolidayService } from '../../../../../core/services/hr-timekeeping/holiday.service';
import { Validators, FormGroup, FormArray } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { WorkdayTypeService, ACTION_FORM, RESOURCE } from '@app/core';
import { AppComponent } from '@app/app.component';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { EmpWorkScheduleService } from '@app/core/services/hr-timekeeping/emp-work-schedule.service';

@Component({
  selector: 'emp-work-schedule-setup',
  templateUrl: './emp-work-schedule-setup.component.html',
})
export class EmpWorkScheduleSetup extends BaseComponent implements OnInit {
  formSave: FormArray;
  workdayTypeList: any = {};
  empWorkScheduleId: number;
  dateTimekeeping: number;

  day: any;
  listIsDelete = [];
  formSaveConfig = {
    workdayTypeCode: [null, [ValidationService.required]],
    hoursWork: [null, [ValidationService.maxLength(4), ValidationService.required
      , ValidationService.positiveInteger, Validators.min(1), Validators.max(10)]],
  };

  constructor( public actr: ActivatedRoute
            , private app: AppComponent
            , public activeModal: NgbActiveModal
            , private service: EmpWorkScheduleService
            , private workdayTypeService: WorkdayTypeService) {
    super(actr, RESOURCE.ATTENDANCE_TYPE, ACTION_FORM.UPDATE);
    this.buildFormEmpWorkOff({});

    this.workdayTypeService.findAllWorkdayTypeByMarketId().subscribe(
      res => {
        this.workdayTypeList = res.data;
      });
   }

  ngOnInit() {
  }

  /**
   * makeDefaultEmpWorkOff
   */
  private makeDefaultEmpWorkOff(): FormGroup {
    const formGroup = this.buildForm({}, this.formSaveConfig, ACTION_FORM.UPDATE);
    return formGroup;
  }

  private buildFormEmpWorkOff(obj?: any) {
    const controls = new FormArray([]);
    if (!obj) {
      this.formSave = new FormArray([this.makeDefaultEmpWorkOff()]);
    } else {
      const group = this.makeDefaultEmpWorkOff();
      group.patchValue(obj);
      controls.push(group);
      this.formSave = controls;
    }
  }

  setFormValue(empWorkScheduleId: number, dateTimekeeping: number, obj?: any) {
    this.empWorkScheduleId = empWorkScheduleId;
    this.dateTimekeeping = dateTimekeeping;
    if (obj) {
      this.buildFormEmpWorkOff(obj);
    } else {
      this.buildFormEmpWorkOff({});
    }
  }

  public processSaveOrUpdate() {
    if (!CommonUtils.isValidFormAndValidity(this.formSave)) {
      return;
    }
    this.app.confirmMessage(null
      , () => {
        this.processUpdateOnOffWorkingDay(1);
      }, () => {
      }
    );
  }
  processUpdateOnOffWorkingDay(isWork) {
    const data = this.formSave.value;
    const postData = {
        dateTimekeeping: this.dateTimekeeping,
        isWork: isWork,
        empWorkScheduleId: this.empWorkScheduleId,
        workdayTypeCode: data[0].workdayTypeCode,
        hoursWork: data[0].hoursWork,
    };
    this.service.processOnOffWorkingDay(this.empWorkScheduleId, postData).subscribe(
      res => {
        if (this.service.requestIsSuccess(res)) {
          this.activeModal.close(res);
        }
      }
    );
  }
}
