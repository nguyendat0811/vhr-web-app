import { RESOURCE, ACTION_FORM } from '@app/core/app-config';
import { EmployeeResolver } from '@app/shared/services/employee.resolver';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EmpWorkScheduleCalendarComponent } from '../emp-work-schedule-calendar/emp-work-schedule-calendar.component';
import { EmpWorkScheduleService } from '@app/core/services/hr-timekeeping/emp-work-schedule.service';
import { CommonUtils } from '@app/shared/services';
import * as moment from 'moment';

@Component({
  selector: 'emp-work-schedule-list',
  templateUrl: './emp-work-schedule-list.component.html',
})
export class EmpWorkScheduleListComponent extends BaseComponent implements OnInit {
  resultList: any = {};
  employeeId: number;
  empId: {employeeId: any};
  @ViewChild('ptable') dataTable: any;
  @ViewChild('empCalendar') empCalendar: EmpWorkScheduleCalendarComponent;
  constructor(private employeeResolver: EmployeeResolver
            , private empWorkScheduleService: EmpWorkScheduleService
            , public actr: ActivatedRoute) {
              super(actr, RESOURCE.EMP_WORK_SCHEDULE, ACTION_FORM.SEARCH);
              this.setMainService(empWorkScheduleService);
              this.employeeResolver.EMPLOYEE.subscribe(
              data => {
                if (data) {
                  this.employeeId = data;
                  this.empId = {employeeId: data};
                  this.formSearch =  this.buildForm({employeeId: this.employeeId}, {employeeId: ['']});
                  this.processSearch();
                }
              }
            );
            }

  ngOnInit() {
  }
  public processSearch(event?): void {
    if (!CommonUtils.isValidForm(this.formSearch)) {
      return;
    }
    const params = this.formSearch ? this.formSearch.value : null;
    this.empWorkScheduleService.search(params, event).subscribe(res => {
      this.resultList = res;
      this.initViewCalendar(res.data);
    });
    if (!event) {
      if (this.dataTable) {
        this.dataTable.first = 0;
      }
    }
  }
  viewCalendar(item) {
    this.empCalendar.setEmpWorkScheduleId(item.empWorkScheduleId, item.effectiveDate, item.expritedDate);
  }
  initViewCalendar(data) {
    if (!data || data.length == 0) {
      this.empCalendar.showView = false;
      return;
    }
    let checkView = false;
    for (const item of data) {
      if (this.isNowProcess(item.effectiveDate, item.expritedDate)) {
        this.viewCalendar(item);
        checkView = true;
        break;
      }
    }
    if (!checkView) {
      this.empCalendar.showView = false;
    }
  }
  isNowProcess(effectiveDateTime, expritedDateTime) {
    const effectiveDate = moment(effectiveDateTime);
    const now = moment();
    if (expritedDateTime) {
      const expritedDate = moment(expritedDateTime);
      if (!now.isBetween(effectiveDate, expritedDate)) {
        return false;
      }
    } else if (now.isBefore(effectiveDate)) {
      return false;
    }
    return true;
  }
}
