import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { PropertyResolver } from '@app/shared/services/property.resolver';
import { RESOURCE } from '@app/core';
import { CommonUtils } from '@app/shared/services';
import { EmpWorkSchedulePageComponent } from './emp-work-schedule-page/emp-work-schedule-page.component';

const routes: Routes = [
  {
    path: '',
    component: EmpWorkSchedulePageComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.EMP_WORK_SCHEDULE,
      nationId: CommonUtils.getNationId()
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmpWorkScheduleRoutingModule { }
