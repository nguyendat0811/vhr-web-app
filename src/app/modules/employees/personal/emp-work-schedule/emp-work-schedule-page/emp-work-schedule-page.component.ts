import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { RESOURCE } from '@app/core';

@Component({
  selector: 'emp-work-schedule-page',
  templateUrl: './emp-work-schedule-page.component.html',
})
export class EmpWorkSchedulePageComponent extends BaseComponent  implements OnInit {

  constructor(public act: ActivatedRoute) { 
    super(act, RESOURCE.EMP_WORK_SCHEDULE);
  }

  ngOnInit() {
  }

}
