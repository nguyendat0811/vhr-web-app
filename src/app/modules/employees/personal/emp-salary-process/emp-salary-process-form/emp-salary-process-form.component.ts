import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { EmpSalaryProcessService } from '@app/core/services/emp-salary-process/emp-salary-process.service';
import { AppComponent } from '@app/app.component';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { ValidationService } from '@app/shared/services';
import { FileControl } from '@app/core/models/file.control';
import { EmployeeResolver } from '@app/shared/services/employee.resolver';
import { ActivatedRoute } from '@angular/router';
import { ACTION_FORM, RESOURCE } from '@app/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';

@Component({
  selector: 'emp-salary-process-form',
  templateUrl: './emp-salary-process-form.component.html',
})
export class EmpSalaryProcessFormComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;

  salarygradelist: any = {};
  salaryregulatelist: any = {};
  salarytablelist: any = {};
  salaryscalelist: any = {};
  @Input() item: any;
  constructor(public activeModal: NgbActiveModal
    , private employeeResolver: EmployeeResolver
    , private empSalaryProcesService: EmpSalaryProcessService
    , private app: AppComponent
    , public actr: ActivatedRoute) {
    super(actr, RESOURCE.EMP_SALARY_PROCESS, ACTION_FORM.INSERT);
    this.buildFormNew({});
    // this.empSalaryProcesService.getSalaryGrade().subscribe(res => {
    //   this.salarygradelist = res.data;
    // });
    this.salaryregulatelist = [{ id: 1, name: 'Quy chế 1' }, { id: 2, name: 'Quy chế 2' }, { id: 3, name: 'Quy chế 3' }];
  }

  ngOnInit() {

  }

  selectSalaryTableList(id) {
    this.salarytablelist = [{ id: 1, name: 'Bảng lương 1' }, { id: 2, name: 'Bảng lương 2' }, { id: 3, name: 'Bảng lương 3' }];
  }

  selectSalaryScaleList(id) {
    this.salaryscalelist = [{ id: 1, name: 'Ngạch lương 1' }, { id: 2, name: 'Ngạch lương 2' }, { id: 3, name: 'Ngạch lương 3' }];
  }

  selectSalaryGradeList(id) {
    this.salarygradelist = [{ id: 1, name: 'Bậc lương 1' }, { id: 2, name: 'Bậc lương 2' }, { id: 3, name: 'Bậc lương 3' }];
  }
  /**
   * processSaveOrUpdate
   */
  processSaveOrUpdate() {
    console.log(this.formSave);
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    } else {
      this.app.confirmMessage(null, () => {// on accepted
        this.empSalaryProcesService.saveOrUpdateFormFile(this.formSave.value)
          .subscribe(res => {
            this.activeModal.close(res);
          });
      }, () => {// on rejected

      });
    }

  }

  get f() {
    return this.formSave.controls;
  }

  /****************** CAC HAM COMMON DUNG CHUNG ****/
  /**
   * buildForm
   */
  private buildFormNew(data): void {
    this.formSave = CommonUtils.createForm(data, {
      empSalaryProcessId: [],
      employeeId: [''],
      // hiepnc fake id
      salaryRegulateId: ['', Validators.compose([ValidationService.required])],
      salaryTableId: ['', Validators.compose([ValidationService.required])],
      salaryScaleId: ['', Validators.compose([ValidationService.required])],
      // end
      salaryGradeId: ['', Validators.compose([ValidationService.required])],
      decisionNumber: ['', Validators.compose([ValidationService.required, ValidationService.maxLength(50)])],
      decisionDate: ['', Validators.compose([ValidationService.required])],
      effectiveDate: ['', Validators.compose([ValidationService.required])],
      expiredDate: [''],
      raisedDate: ['', Validators.compose([ValidationService.required])],
      factor: [''],
      salary: [''],
      percent: ['', Validators.compose([ValidationService.positiveNumber])],
      description: ['', Validators.compose([ValidationService.maxLength(2000)])],
    }, Validators.compose([ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.empSalaryProcess.expiredDate'),
    ValidationService.notAffter('decisionDate', 'effectiveDate', 'app.empSalaryProcess.effectiveDate')]));
    this.formSave.addControl('file', new FileControl(null));
  }

  public onChangeDate() {
    console.log('onDateChanged');
  }



  public onSalaryRegulateChange($event) {
    console.log($event);
    if (this.formSave.get('salaryRegulateId').value) {
      this.selectSalaryTableList(this.formSave.get('salaryTableId').value);
    } else {
      this.salarytablelist = [];
      this.salaryscalelist = [];
      this.salarygradelist = [];
    }
  }
  public onSalaryTableChange($event) {
    if (this.formSave.get('salaryTableId').value) {
      this.selectSalaryScaleList(this.formSave.get('salaryScaleId').value);
    } else {
      this.salaryscalelist = [];
      this.salarygradelist = [];
    }
  }

  public onSalaryScaleChange($event) {
    if (this.formSave.get('salaryScaleId').value) {
      this.selectSalaryGradeList(this.formSave.get('salaryGradeId').value);
    } else {
      this.salarygradelist = [];
    }
  }


  public setFormValue(data: any) {
    if (data.fileAttachment) {
    (this.formSave.controls['file'] as FileControl).setFileAttachment(data.fileAttachment.empSalaryProcessFile);
    }
    this.formSave.setValue(CommonUtils.copyProperties(this.formSave.value, data));
  }
}
