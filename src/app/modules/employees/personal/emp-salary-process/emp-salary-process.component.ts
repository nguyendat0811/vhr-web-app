import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { RESOURCE } from '@app/core';

@Component({
  selector: 'emp-salary-process',
  templateUrl: './emp-salary-process.component.html',
})
export class EmpSalaryProcessComponent extends BaseComponent  implements OnInit {
  resultList: any = {};
  constructor(public act: ActivatedRoute) { 
    super(act, RESOURCE.EMP_SALARY_PROCESS);
  }

  ngOnInit() {
  }

}
