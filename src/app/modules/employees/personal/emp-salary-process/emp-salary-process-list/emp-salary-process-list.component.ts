import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from '@app/app.component';
import { EmpSalaryProcessService } from '@app/core/services/emp-salary-process/emp-salary-process.service';
import { EmpSalaryProcessFormComponent } from '../emp-salary-process-form/emp-salary-process-form.component';
import { DEFAULT_MODAL_OPTIONS, RESOURCE } from '@app/core/app-config';
import { EmployeeResolver } from '@app/shared/services/employee.resolver';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'emp-salary-process-list',
  templateUrl: './emp-salary-process-list.component.html',
})
export class EmpSalaryProcessListComponent extends BaseComponent implements OnInit {
  emp: {employeeId: any};
  empId: any;
  resultList: any = {};
  credentials: any = {};
  formSearch: FormGroup;
  constructor(private modalService: NgbModal
    , private empSalaryService: EmpSalaryProcessService
    , private app: AppComponent
    , private employeeResolver: EmployeeResolver
    , public act: ActivatedRoute) {
      super(act, RESOURCE.EMP_SALARY_PROCESS);
      this.employeeResolver.EMPLOYEE.subscribe(
        data => {
          if (data) {
            this.empId = data;
            this.emp = {employeeId: data};
            this.processSearch(null);
          }
        }
      );
  }

  ngOnInit() {
  }

  get f() {
    return this.formSearch.controls;
  }

  public onChangeDate() {
    console.log('onDateChanged');
  }

  public processSearch(event) {
    if (!this.empId) {
      return;
    }
    const formSearch = {employeeId : this.empId};
    this.empSalaryService.search(formSearch, event).subscribe(res => {
      this.resultList = res;
      // this.app.isProcessing(false);
    });
}
  /**
   * prepareUpdate
   * param item
   */
  prepareSaveOrUpdate(item) {
    if (item && item.empSalaryProcessId > 0) {
      this.empSalaryService.findOne(item.empSalaryProcessId)
        .subscribe(res => {
          this.activeModel(res.data);
        });
    } else {
      this.employeeResolver.EMPLOYEE.subscribe(
        data => {
          if (data) {
            this.activeModel({employeeId: data});
          }
        }
      );
    }
  }

  private activeModel(data) {
    const modalRef = this.modalService.open(EmpSalaryProcessFormComponent, DEFAULT_MODAL_OPTIONS);
    if (data) {
      modalRef.componentInstance.setFormValue(data);
    }
    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
      if (this.empSalaryService.requestIsSuccess(result)) {
        this.processSearch(this.emp);
      }
    });
  }
  /**
   * prepareDelete
   * param item
   */
  processDelete(item) {
    if (item && item.empSalaryProcessId > 0) {
      this.app.confirmDelete(null, () => {// on accepted
        this.empSalaryService.deleteById(item.empSalaryProcessId)
          .subscribe(res => {
            if (this.empSalaryService.requestIsSuccess(res)) {
              this.processSearch(this.emp);
            }
          });
      }, () => {// on rejected

      });
    }
  }
}
