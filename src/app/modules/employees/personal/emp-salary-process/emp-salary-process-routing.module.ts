import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmpSalaryProcessComponent } from './emp-salary-process.component';

const routes: Routes = [
  {
    path: '',
    component: EmpSalaryProcessComponent
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmpSalaryProcessRoutingModule { }
