import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@app/shared';
import { EmpSalaryProcessComponent } from './emp-salary-process.component';
import { EmpSalaryProcessRoutingModule } from './emp-salary-process-routing.module';
import { EmpSalaryProcessListComponent } from './emp-salary-process-list/emp-salary-process-list.component';
import { EmpSalaryProcessFormComponent } from './emp-salary-process-form/emp-salary-process-form.component';
import { EmployeeResolver } from '@app/shared/services/employee.resolver';

@NgModule({
  declarations: [EmpSalaryProcessComponent, EmpSalaryProcessListComponent, EmpSalaryProcessFormComponent],
  imports: [
    CommonModule,
    SharedModule,

    EmpSalaryProcessRoutingModule
  ],
  entryComponents: [EmpSalaryProcessListComponent, EmpSalaryProcessFormComponent],
  providers: [EmployeeResolver]
})
export class EmpSalaryProcessModule { }
