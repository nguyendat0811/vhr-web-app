import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { RESOURCE } from '@app/core';

@Component({
  selector: 'other-information',
  templateUrl: './other-information.component.html',
})
export class OtherInformationComponent extends BaseComponent implements OnInit {
  resultList: any = {};

  constructor(public act: ActivatedRoute) { 
    super(act, RESOURCE.OTHER_INFORMATION);
  }

  ngOnInit() {
  }

}
