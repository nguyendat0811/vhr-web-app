import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@app/shared';

import { OtherInformationRoutingModule } from './other-information-routing.module';
import { OtherInformationComponent } from './other-information.component';
import { OtherInformationFormComponent } from './other-information-form/other-information-form.component';

@NgModule({
  declarations: [OtherInformationComponent, OtherInformationFormComponent],
  imports: [
    CommonModule,
    SharedModule,

    OtherInformationRoutingModule
  ],
  entryComponents: [OtherInformationFormComponent]
})
export class OtherInformationModule { }
