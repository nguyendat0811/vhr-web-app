import { RESOURCE } from '@app/core/app-config';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AppComponent } from '@app/app.component';
import { OtherInformationService } from '@app/core/services/other-information/other-information.service';
import { ValidationService, CommonUtils } from '@app/shared/services';
import { SysCatTypeService, ACTION_FORM } from '@app/core';
import { EmployeeResolver } from '@app/shared/services/employee.resolver';
import { FileControl } from '@app/core/models/file.control';

@Component({
  selector: 'other-information-form',
  templateUrl: './other-information-form.component.html',
})
export class OtherInformationFormComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  resultList: any = {};
  policyList: any = {};
  employeeId: number;
  formConfig = {
    // nationId: [CommonUtils.getNationId],
    employeeId: [null],
    otherInfo: ['', [ValidationService.maxLength(1000)]],
    weight: ['', [ValidationService.positiveNumber, ValidationService.maxLength(6), Validators.max(999)]],
    height: ['', [ValidationService.positiveNumber, ValidationService.maxLength(6), Validators.max(999)]],
    childGrade: ['', [ ValidationService.positiveInteger, ValidationService.maxLength(2)]],
    familyTypeId: [''],
    policyType: [''],
    isPolicy: [''],
    referencePeople: ['', [ValidationService.maxLength(100)]],
    referenceDepartment: ['', [ValidationService.maxLength(300)]],
    referenceRank: ['', [ValidationService.maxLength(200)]],
    referenceRelationship: ['', [ValidationService.maxLength(200)]],
  };

  familyTypeList: any = {};
  formSearch: FormGroup;
  @Input() item: any;
  constructor(public actr: ActivatedRoute
    , private otherInformationService: OtherInformationService
    , private app: AppComponent
    , private sysCatTypeService: SysCatTypeService
    , private employeeResolver: EmployeeResolver) {
      super(actr, RESOURCE.OTHER_INFORMATION, ACTION_FORM.INSERT);

    // this.formSave = this.buildForm({}, this.formConfig);
    // this.formSave.addControl('file', new FileControl(null));
    this.buildForms({}, ACTION_FORM.INSERT);
    this.employeeResolver.EMPLOYEE.subscribe(
      data => {
        if (data) {
          this.otherInformationService.findOne(data).subscribe(
            (res) => {
              if (res) {
                this.formSave = this.buildForm(res.data, this.formConfig);
                this.formSave.addControl('file', new FileControl(null));
                  console.log(res.data.fileAttachment);
                  if (res.data.fileAttachment) {
                      (this.formSave.controls['file'] as FileControl).setFileAttachment(res.data.fileAttachment.empOtherInformationFile);
                }
              }
            }
          );
          this.employeeId = data;
        }
      }
    );

    // Family Policy
    this.sysCatTypeService.getListSysCat('LCS').subscribe(res =>
      this.policyList = res.data);

    // Family Type
    this.sysCatTypeService.getListSysCat('TPGD').subscribe(res =>
      this.familyTypeList = res.data);
  }


  ngOnInit() {
    this.formSave.get('employeeId').setValue(this.employeeId);
  }

  get f() {
    return this.formSave.controls;
  }

  private buildForms(data?: any, actionForm?: ACTION_FORM): void {
    this.formSave = this.buildForm({}, this.formConfig, actionForm);
    this.formSave.addControl('file', new FileControl(null));
    if (data.fileAttachment) {
      (this.formSave.controls['file'] as FileControl).setFileAttachment(data.fileAttachment.empOtherInformationFile);
    }
  }

  public onChangeDate() {
    console.log('onDateChanged');
  }

  processSaveOrUpdate() {
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    }
    this.app.confirmMessage(null, () => {// on accepted
      this.otherInformationService.saveOrUpdateOther(this.formSave.value)
        .subscribe(res => {
          if (this.otherInformationService.requestIsSuccess(res)) {
            this.otherInformationService.findOne(res.data.employeeId)
            .subscribe(result => {
            this.formSave = this.buildForm(result.data, this.formConfig);
            this.formSave.addControl('file', new FileControl(null));
              if (result.data.fileAttachment) {
                  (this.formSave.controls['file'] as FileControl).setFileAttachment(result.data.fileAttachment.empOtherInformationFile);
              }
            });
          }
        });
    }, () => {// on rejected

    });
  }
}

