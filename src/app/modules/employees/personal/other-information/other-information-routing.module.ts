import { CommonUtils } from '@app/shared/services';
import { RESOURCE } from './../../../../core/app-config';
import { PropertyResolver } from './../../../../shared/services/property.resolver';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OtherInformationComponent } from './other-information.component';

const routes: Routes = [
  {
    path: '',
    component: OtherInformationComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.OTHER_INFORMATION,
      nationId: CommonUtils.getNationId()
    }
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OtherInformationRoutingModule { }
