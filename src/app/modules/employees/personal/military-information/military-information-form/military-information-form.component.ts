import { RESOURCE } from '@app/core/app-config';
import { BaseComponent } from './../../../../../shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MilitaryInformationService } from '@app/core/services/military-information/military-information.service';
import { AppComponent } from '@app/app.component';
import { CommonUtils, ValidationService } from '@app/shared/services';
import { EmployeeInfoService, ACTION_FORM } from '@app/core';
import { EmployeeResolver } from '@app/shared/services/employee.resolver';

@Component({
  selector: 'military-information-form',
  templateUrl: './military-information-form.component.html',

})
export class MilitaryInformationFormComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  @Input() item: any;
  formConfig = {
    employeeId: [],
    militaryNumber: ['', [ValidationService.maxLength(20)]],
    callUpDate: [''],
    expiredDate: ['']
  };
  constructor(public actr: ActivatedRoute
    , private employeeInfoService: EmployeeInfoService
    , private employeeResolver: EmployeeResolver
    , private militaryInfotmationService: MilitaryInformationService
    , private app: AppComponent) {
      super(actr, RESOURCE.MILITARY_INFORMATION, ACTION_FORM.UPDATE);
      this.formSave = this.buildForm({}, this.formConfig,
        ACTION_FORM.UPDATE, [ValidationService.notAffter('callUpDate', 'expiredDate', 'app.militaryInformation.expiredDate')]);

      this.employeeResolver.EMPLOYEE.subscribe(
        data => {
          if (data) {
            this.employeeInfoService.findOne(data).subscribe(res =>
              this.formSave = this.buildForm(res.data, this.formConfig,
                ACTION_FORM.UPDATE, [ValidationService.notAffter('callUpDate', 'expiredDate', 'app.militaryInformation.expiredDate')])
            );
          }
        }
      );
  }
  ngOnInit() {
  }

  public onChangeDate() {
    console.log('onDateChanged');
  }

  get f () {
    return this.formSave.controls;
  }

  private validateBeforeSave(): boolean {
    return CommonUtils.isValidForm(this.formSave);
  }

  public processSaveOrUpdate() {
    if (!this.validateBeforeSave()) {
      return;
    }
    this.app.confirmMessage(null, () => { // success
        this.militaryInfotmationService.updateMilitaryInfo(this.formSave.value)
        .subscribe(res => {

        });
    }, () => {
      // reject
    }
    );
  }

  /**
   * setFormValue
   * param data
   */
  public setFormValue(propertyConfigs: any, data: any) {
    this.propertyConfigs = propertyConfigs;
    this.formSave = this.buildForm(data, this.formConfig,
      ACTION_FORM.UPDATE, [ValidationService.notAffter('callUpDate', 'expiredDate', 'app.militaryInformation.expiredDate')]);
  }
}
