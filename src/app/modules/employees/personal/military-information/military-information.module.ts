import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MilitaryInformationFormComponent } from './military-information-form/military-information-form.component';
import { MilitaryInformationPageComponent } from './military-information-page/military-information-page.component';
import { SharedModule } from '@app/shared';
import { MilitaryInformationRoutingModule } from './military-information-routing.module';

@NgModule({
  declarations: [MilitaryInformationFormComponent,MilitaryInformationPageComponent],
  imports: [
    CommonModule,
    SharedModule,

    MilitaryInformationRoutingModule
  ],
  entryComponents: [MilitaryInformationFormComponent,MilitaryInformationPageComponent]
})
export class MilitaryInformationModule { }
