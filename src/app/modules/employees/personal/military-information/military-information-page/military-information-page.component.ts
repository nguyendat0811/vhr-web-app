import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { RESOURCE } from '@app/core';
@Component({
  selector: 'military-information-page',
  templateUrl: './military-information-page.component.html',

})
export class MilitaryInformationPageComponent extends BaseComponent implements OnInit {

  resultList: any = {};

  constructor(public act: ActivatedRoute) { 
    super(act, RESOURCE.MILITARY_INFORMATION);
  }

  ngOnInit() {
  }

}
