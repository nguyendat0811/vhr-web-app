import { CommonUtils } from '@app/shared/services/common-utils.service';
import { RESOURCE } from '@app/core';
import { NgModule } from '@angular/core';
import { MilitaryInformationPageComponent } from './military-information-page/military-information-page.component';
import { Routes, RouterModule } from '@angular/router';
import { PropertyResolver } from '@app/shared/services/property.resolver';

const routes: Routes = [
  {
    path: '',
    component: MilitaryInformationPageComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.MILITARY_INFORMATION,
      nationId: CommonUtils.getNationId()
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MilitaryInformationRoutingModule { }
