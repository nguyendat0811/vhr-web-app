import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@app/shared';

import { PartyUnionRoutingModule } from './party-union-routing.module';
import { PartyUnionIndexComponent } from './page/party-union-index/party-union-index.component';
import { PartyUnionFormComponent } from './page/party-union-form/party-union-form.component';


@NgModule({
  declarations: [PartyUnionIndexComponent, PartyUnionFormComponent],
  imports: [
    CommonModule,
    SharedModule,

    PartyUnionRoutingModule
  ],
  entryComponents: []
})
export class PartyUnionModule { }
