import { CommonUtils } from './../../../../shared/services/common-utils.service';
import { RESOURCE } from './../../../../core/app-config';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PartyUnionIndexComponent } from './page/party-union-index/party-union-index.component';
import { PropertyResolver } from '@app/shared/services/property.resolver';

const routes: Routes = [
  {
    path: '',
    component: PartyUnionIndexComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.PARTY_UNION,
      nationId: CommonUtils.getNationId()
    }
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PartyUnionRoutingModule { }
