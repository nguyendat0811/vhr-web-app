import { ACTION_FORM } from './../../../../../../core/app-config';
import { RESOURCE } from '@app/core/app-config';
import { ActivatedRoute } from '@angular/router';
import { BaseComponent } from './../../../../../../shared/components/base-component/base-component.component';
import { EmployeeResolver } from '@app/shared/services/employee.resolver';
import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { AppComponent } from '@app/app.component';
import { ValidationService, CommonUtils } from '@app/shared/services';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { PartyUnionService } from '@app/core/services/party-union/party-union.service';

@Component({
  selector: 'party-union-form',
  templateUrl: './party-union-form.component.html',
})
export class PartyUnionFormComponent extends BaseComponent implements OnInit {
  formSavePartyUnion: FormGroup;
  formSaveOtherParty: FormArray;
  partyUnion: any = {};
  listOtherParty: [];
  listIdDelete = [];
  employeeId: number;
  formConfig = {
    empPartyUnionId: [''],
    employeeId: [null],
    partyNumber: [null, [ValidationService.maxLength(50)]],
    partyAdmissionPlace: [null, [ValidationService.maxLength(100)]],
    partyOfficialAdmissionDate: [null],
    partyAdmissionDate: [null],
    unionNumber: [null, [ValidationService.maxLength(50)]],
    unionAdmissionDate: [null],
    unionAdmissionPlace: [null, [ ValidationService.maxLength(50)]],
  };

  columns: number[];
  columns1: number[];
  @Input() item: any;

  constructor(private formBuilder: FormBuilder
    , public actr: ActivatedRoute
    , private app: AppComponent
    , private partyUnionService: PartyUnionService
    , private employeeResolver: EmployeeResolver) {
      super(actr, RESOURCE.PARTY_UNION, ACTION_FORM.INSERT);

    // this.formSavePartyUnion = this.buildForm({}, this.formConfig
    //   , ACTION_FORM.INSERT
    //   , [ValidationService.notAffter('partyAdmissionDate', 'partyOfficialAdmissionDate', 'app.party_union.joinStDate')]);

      this.buildFormPartyUnion({}, ACTION_FORM.INSERT);
      this.buildFormOtherParty();

      this.employeeResolver.EMPLOYEE.subscribe(
        data => {
          if (data) {
            this.partyUnionService.findUnionByEmployeeId(data).subscribe(
              (res) => {
                if (res) {
                  // this.buildFormPartyUnion(res.data || {});
                  // this.formSavePartyUnion = this.buildForm(res.data, this.formConfig, ACTION_FORM.INSERT
                  //   , [ValidationService.notAffter('partyAdmissionDate', 'partyOfficialAdmissionDate', 'app.party_union.joinStDate')]);
                  this.buildFormPartyUnion(res.data, ACTION_FORM.INSERT);
                }
              }
            );

            this.partyUnionService.findByEmployeeId(data).subscribe(
              (res) => {
                if (res) {
                  this.buildFormOtherParty(res);
                }
              }
            );

            this.employeeId = data;
            // Lay thong tin Party Union
            this.formSavePartyUnion.patchValue({});
            // to du lay danh sach listOtherParty
            this.buildFormOtherParty(this.listOtherParty);


          }
        }
      );

  }

  ngOnInit() {
    this.formSavePartyUnion.get('employeeId').setValue(this.employeeId);
  }

  /**
   * addOtherParty
   * param index
   * param item
   */
  public addOtherParty(index: number) {
    const controls = this.formSaveOtherParty as FormArray;
    controls.insert(index + 1, this.createDefaultFormOtherParty());
  }
  /**
   * removeOtherParty
   * param index
   * param item
   */
  public removeOtherParty(index: number) {
    const controls = this.formSaveOtherParty as FormArray;
    if (controls.length === 1) {
      return;
    }
    const control = controls.controls[index] as FormGroup;
    if (control && control.get('otherPartyId')) {
      const value = control.get('otherPartyId').value;
      this.listIdDelete.push(value);
    }
    controls.removeAt(index);
  }

  get f() {
    return this.formSavePartyUnion.controls;
  }

  private buildFormOtherParty(listOtherParty?: any): void {
    const controls = new FormArray([]);
    if (!listOtherParty) {
      this.formSaveOtherParty = new FormArray([this.createDefaultFormOtherParty()]);
    } else {

      for (const item of listOtherParty) {
        // const empBankAccount = listOtherParty[item];
        const group = this.createDefaultFormOtherParty();
        group.patchValue(item);
        controls.push(group);
      }
    }
    this.formSaveOtherParty = controls;
  }

  private createDefaultFormOtherParty(): FormGroup {
    const group = {
      otherPartyId: [null],
      partyName: [null, Validators.compose([ ValidationService.maxLength(100)])],
      joinedPlace: [null, Validators.compose([ ValidationService.maxLength(100)])],
      joinedDate: [null],
    };
    return this.buildForm({}, group);
  }

  private buildFormPartyUnion(data?: any, actionForm?: ACTION_FORM): void {
    this.formSavePartyUnion = this.buildForm(data, this.formConfig
      , actionForm);
  }

  processSaveOrUpdate() {
    if (!this.validateBeforeSave()) {
      return;
    }
    this.app.confirmMessage(null, () => { // on accepted
        const formSave = {};
        formSave['partyUnion']  = this.formSavePartyUnion.value;
        formSave['otherParty']  = this.formSaveOtherParty.value;
        formSave['employeeId'] = this.employeeId;
        formSave['listIdDelete'] = this.listIdDelete;
        this.partyUnionService.saveOrUpdate(formSave).subscribe(res => {
          // call back sau khi save
          this.buildFormPartyUnion(res.data, ACTION_FORM.INSERT);

          this.partyUnionService.findByEmployeeId(this.employeeId).subscribe(
            (data) => {
            this.buildFormOtherParty(data);
            }
          );
        });
      }, () => {
      // on rejected
    });
  }

  public onJoinedDateChange(item) {
    console.log(item);
    const currentValue = item.value.partyName;
    if (item.value.joinedDate != null) {
      item.removeControl('partyName');
      item.addControl('partyName', new FormControl(currentValue, [ValidationService.required]));
    } else if (item.value.joinedDate == null) {
      item.removeControl('partyName');
      item.addControl('partyName', new FormControl(currentValue));
    }
  }


  private validateBeforeSave(): boolean {
    const formSavePartyUnion = CommonUtils.isValidForm(this.formSavePartyUnion);
    const formSaveOtherParty = CommonUtils.isValidForm(this.formSaveOtherParty);
    return formSavePartyUnion && formSaveOtherParty;
  }

}
