import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { SharedModule } from '@app/shared';

import { HealthInsuranceRoutingModule } from './health-insurance-routing.module';
import { HealthInsurancePageComponent } from './health-insurance-page/health-insurance-page.component';
import { EmpHealthInsuranceListComponent } from './emp-health-insurance/emp-health-insurance-list/emp-health-insurance-list.component';
import { EmpHealthInsuranceFormComponent } from './emp-health-insurance/emp-health-insurance-form/emp-health-insurance-form.component';
// tslint:disable-next-line:max-line-length
import { FamilyRelationshipHealthInsuranceFormComponent } from './family-reationship-health-insurance/family-relationship-health-insurance-form/family-relationship-health-insurance-form.component';
// tslint:disable-next-line:max-line-length
import { FamilyRelationshipHealthInsuranceListComponent } from './family-reationship-health-insurance/family-relationship-health-insurance-list/family-relationship-health-insurance-list.component';
import { EmployeeResolver } from '@app/shared/services/employee.resolver';



@NgModule({
  declarations: [EmpHealthInsuranceFormComponent, HealthInsurancePageComponent, EmpHealthInsuranceListComponent,
                 FamilyRelationshipHealthInsuranceFormComponent, FamilyRelationshipHealthInsuranceListComponent],
  imports: [
    CommonModule,
    SharedModule,

    HealthInsuranceRoutingModule
  ],
  entryComponents: [EmpHealthInsuranceFormComponent, FamilyRelationshipHealthInsuranceFormComponent],
  providers: [EmployeeResolver, DatePipe]
})
export class HealthInsuranceModule { }
