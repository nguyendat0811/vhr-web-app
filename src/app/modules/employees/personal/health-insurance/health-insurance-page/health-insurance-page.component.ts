import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { RESOURCE } from '@app/core';

@Component({
  selector: 'health-insurance-page',
  templateUrl: './health-insurance-page.component.html',
})
export class HealthInsurancePageComponent extends BaseComponent implements OnInit {

  constructor(public act: ActivatedRoute) { 
    super(act, RESOURCE.HEALTH_INSURANCE);
  }

  ngOnInit() {
  }

}
