import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from '@app/app.component';
import { HealthInsuranceService, RESOURCE, ACTION_FORM } from '@app/core';
import { CommonUtils, ValidationService } from '@app/shared/services';
import { FileControl } from '@app/core/models/file.control';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'emp-health-insurance-form',
  templateUrl: './emp-health-insurance-form.component.html',
})
export class EmpHealthInsuranceFormComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  formConfig = {
    empHealthInsuranceId: [null],
    employeeId: [null],
    familyRelationshipId: [null],
    insuranceForm: ['1'],
    insuranceNumber: [null, [ValidationService.required, ValidationService.maxLength(50)]],
    issuedDate: [null, [ValidationService.required]],
    effectiveDate: [null, [ValidationService.required]],
    expiredDate: [null],
    description: [null, [ValidationService.maxLength(500)]],
    status: [null],
  };
  constructor(public activeModal: NgbActiveModal
            , private empHealthInsuranceService: HealthInsuranceService
            , private app: AppComponent
            , public actr: ActivatedRoute) {
        super(actr, RESOURCE.HEALTH_INSURANCE, ACTION_FORM.EMP_HEALTH_INSURANCE_INSERT);
        this.buildForms({}, ACTION_FORM.INSERT);
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
  }

  /**
   * processSaveOrUpdate
   */
  processSaveOrUpdate() {
    console.log(this.formSave);
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    } else {
      this.app.confirmMessage('app.empHealthInsurance.finishPreviousProcess', () => {
        this.empHealthInsuranceService.saveOrUpdateFormFile(this.formSave.value)
        .subscribe(res => {
          if (this.empHealthInsuranceService.requestIsSuccess(res)) {
          this.activeModal.close(res);
          }
        });
      }, () => {

      });
    }
  }

  /****************** CAC HAM COMMON DUNG CHUNG ****/
  get f () {
    return this.formSave.controls;
  }

  /**
   * buildForm
   */
  private buildForms(data?: any, actionForm?: ACTION_FORM): void {
    this.formSave = this.buildForm(data, this.formConfig, actionForm
    , [ValidationService.notAffter('issuedDate', 'effectiveDate', 'app.empHealthInsurance.effectiveDate')
    , ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.empHealthInsurance.expiredDate')]);
    this.formSave.addControl('file', new FileControl(null));
    if (data.fileAttachment) {
      (this.formSave.controls['file'] as FileControl).setFileAttachment(data.fileAttachment.empHealthInsuranceFile);
    }
  }

  /**
   * setFormValue
   * param data
   */
  public setFormValue(propertyConfigs: any, data?: any) {
    this.propertyConfigs = propertyConfigs;
    if (data && data.empHealthInsuranceId > 0) {
      this.buildForms(data, ACTION_FORM.EMP_HEALTH_INSURANCE_UPDATE);
    } else {
      this.buildForms(data, ACTION_FORM.EMP_HEALTH_INSURANCE_INSERT);
    }
  }
}
