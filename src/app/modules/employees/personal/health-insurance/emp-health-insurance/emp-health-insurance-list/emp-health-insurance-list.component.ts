import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HealthInsuranceService, DEFAULT_MODAL_OPTIONS, RESOURCE, ACTION_FORM } from '@app/core';
import { EmpHealthInsuranceFormComponent } from '../emp-health-insurance-form/emp-health-insurance-form.component';
import { FileStorageService } from '@app/core/services/file-storage.service';
import { EmployeeResolver } from '@app/shared/services/employee.resolver';
import { saveAs } from 'file-saver';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';

@Component({
  selector: 'emp-health-insurance-list',
  templateUrl: './emp-health-insurance-list.component.html',
})
export class EmpHealthInsuranceListComponent extends BaseComponent implements OnInit {
  resultList: any = {};
  employeeId: number;
  empId: {employeeId: any};
  insuranceForm: any;
  @ViewChild('ptable') dataTable: any;
  constructor(private modalService: NgbModal
    , private empHealthInsuranceService: HealthInsuranceService
    , private employeeResolver: EmployeeResolver
    , public fileStorage: FileStorageService
    , public actr: ActivatedRoute) {
      super(actr, RESOURCE.HEALTH_INSURANCE, ACTION_FORM.SEARCH);
      this.setMainService(empHealthInsuranceService);
      this.employeeResolver.EMPLOYEE.subscribe(
      data => {
        if (data) {
          this.employeeId = data;
          this.empId = {employeeId: data};
          this.formSearch =  this.buildForm({employeeId: this.employeeId}, {employeeId: [''], insuranceForm: [1]});
          this.processSearch();
        }
      }
    );
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
  }

  /**
   * prepareUpdate
   * param item
   */
  prepareSaveOrUpdate(item) {
    if (item && item.empHealthInsuranceId > 0) {
      this.empHealthInsuranceService.findOne(item.empHealthInsuranceId)
        .subscribe(res => {
          this.activeModel(res.data);
        });
    } else {
      this.activeModel(this.empId);
    }
  }
  /**
   * show model
   * data
   */
  private activeModel(data?: any) {
    const modalRef = this.modalService.open(EmpHealthInsuranceFormComponent, DEFAULT_MODAL_OPTIONS);
    if (data) {
      modalRef.componentInstance.setFormValue(this.propertyConfigs, data);
    }
    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
      if (this.empHealthInsuranceService.requestIsSuccess(result)) {
        this.processSearch();
      }
    });
  }

  public downloadFile(fileData) {
    this.fileStorage.downloadFile(fileData.id).subscribe(res => {
        saveAs(res , fileData.fileName);
    });
  }
}
