import { CommonUtils } from '@app/shared/services';
import { RESOURCE } from '@app/core';
import { PropertyResolver } from './../../../../shared/services/property.resolver';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HealthInsurancePageComponent } from './health-insurance-page/health-insurance-page.component';

const routes: Routes = [
  {
    path: '',
    component: HealthInsurancePageComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.HEALTH_INSURANCE,
      nationId: CommonUtils.getNationId()
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HealthInsuranceRoutingModule { }
