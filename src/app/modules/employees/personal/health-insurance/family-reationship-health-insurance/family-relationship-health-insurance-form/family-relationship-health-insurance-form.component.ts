import { ACTION_FORM } from './../../../../../../core/app-config';
import { RESOURCE } from '@app/core/app-config';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { HealthInsuranceService, FamilyRelationshipService } from '@app/core';
import { AppComponent } from '@app/app.component';
import { CommonUtils, ValidationService } from '@app/shared/services';
import { FileControl } from '@app/core/models/file.control';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'family-relationship-health-insurance-form',
  templateUrl: './family-relationship-health-insurance-form.component.html',
})
export class FamilyRelationshipHealthInsuranceFormComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  familyRelationshipList: [];
  employeeId: number;
  dateOfbirth: any;
  passportNumber: any;
  @ViewChildren('dateOfbirths') dateOfbirths;
  @ViewChildren('passportNumbers') passportNumbers;
  formConfig = {
    empHealthInsuranceId: [null],
    employeeId: [null],
    familyRelationshipId: [null, [ValidationService.required]],
    insuranceForm: ['2'],
    insuranceNumber: [null, [ValidationService.required, ValidationService.maxLength(50)]],
    issuedDate: [null, [ValidationService.required]],
    effectiveDate: [null, [ValidationService.required]],
    expiredDate: [null],
    description: [null, [ValidationService.maxLength(500)]],
    status: [null],
  };
  constructor(public activeModal: NgbActiveModal
            , private datePipe: DatePipe
            , private empHealthInsuranceService: HealthInsuranceService
            , private familyRelationshipService: FamilyRelationshipService
            , private app: AppComponent
            , public actr: ActivatedRoute) {
        super(actr, RESOURCE.HEALTH_INSURANCE, ACTION_FORM.INSERT);
        this.buildForms({}, ACTION_FORM.INSERT);
  }
  /**
   * ngOnInit
   */
  ngOnInit() {
    this.employeeId = this.formSave.get('employeeId').value ;
    this.familyRelationshipService.getListByCurrenRelated(this.employeeId).subscribe(res => {
      console.log('data: ', res.data)
      this.familyRelationshipList = res.data;
    });
  }

  /**
   * processSaveOrUpdate
   */
  processSaveOrUpdate() {
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    }
      this.app.confirmMessage('app.empHealthInsurance.finishPreviousProcess', () => {
        this.empHealthInsuranceService.saveOrUpdateFormFile(this.formSave.value)
        .subscribe(res => {
          if (this.empHealthInsuranceService.requestIsSuccess(res)) {
            this.activeModal.close(res);
            }
          });
        }, () => {

        });
      }

  /****************** CAC HAM COMMON DUNG CHUNG ****/
  get f () {
    return this.formSave.controls;
  }

  /**
   * buildForm
   */
  private buildForms(data?: any, actionForm?: ACTION_FORM): void {
    this.formSave = this.buildForm(data, this.formConfig, actionForm
    , [ValidationService.notAffter('issuedDate', 'effectiveDate', 'app.empHealthInsurance.effectiveDate')
    , ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.empHealthInsurance.expiredDate')]);
    this.formSave.addControl('file', new FileControl(null));
    if (data.fileAttachment) {
      (this.formSave.controls['file'] as FileControl).setFileAttachment(data.fileAttachment.empHealthInsuranceFile);
    }
  }

  /**
   * setFormValue
   * param data
   */
  public setFormValue(propertyConfigs: any, data?: any) {
    this.familyRelationshipService.findOne(data.familyRelationshipId).subscribe(res => {
      const dateFormat = this.datePipe.transform(res.data.dateOfBirth, 'dd/MM/yyyy');
      this.dateOfbirths.first.nativeElement.value = dateFormat;
      this.passportNumbers.first.nativeElement.value = res.data.passportNumber;
    });
    this.propertyConfigs = propertyConfigs;
    if (data && data.empHealthInsuranceId > 0) {
      this.buildForms(data, ACTION_FORM.UPDATE);
    } else {
      this.buildForms(data, ACTION_FORM.INSERT);
    }
  }

  /**
   * onChangeRelative
   */
  public onChangeRelative(event) {
    this.familyRelationshipService.findOne(event).subscribe(res => {
      const dateFormat = this.datePipe.transform(res.data.dateOfBirth, 'dd/MM/yyyy');
      this.dateOfbirths.first.nativeElement.value = dateFormat;
      this.passportNumbers.first.nativeElement.value = res.data.passportNumber;
    });
  }
}
