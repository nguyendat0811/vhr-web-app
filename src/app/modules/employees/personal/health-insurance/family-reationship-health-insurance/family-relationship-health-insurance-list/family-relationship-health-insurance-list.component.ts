import { RESOURCE } from '@app/core/app-config';
import { BaseComponent } from './../../../../../../shared/components/base-component/base-component.component';
import { ACTION_FORM } from './../../../../../../core/app-config';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HealthInsuranceService, DEFAULT_MODAL_OPTIONS } from '@app/core';
// tslint:disable-next-line:max-line-length
import { FamilyRelationshipHealthInsuranceFormComponent } from '../family-relationship-health-insurance-form/family-relationship-health-insurance-form.component';
import { FileStorageService } from '@app/core/services/file-storage.service';
import { EmployeeResolver } from '@app/shared/services/employee.resolver';
import { saveAs } from 'file-saver';

@Component({
  selector: 'family-relationship-health-insurance-list',
  templateUrl: './family-relationship-health-insurance-list.component.html',
})
export class FamilyRelationshipHealthInsuranceListComponent extends BaseComponent implements OnInit {
  employeeId: number;
  insuranceForm: number;
  resultList: any = {};
  empId: {employeeId: any};
  @ViewChild('ptable') dataTable: any;
  constructor(private modalService: NgbModal
            , private empHealthInsuranceService: HealthInsuranceService
            , private employeeResolver: EmployeeResolver
            , public fileStorage: FileStorageService
            , public actr: ActivatedRoute) {
              super(actr, RESOURCE.HEALTH_INSURANCE, ACTION_FORM.SEARCH);
              this.setMainService(empHealthInsuranceService);
    this.employeeResolver.EMPLOYEE.subscribe(
      data => {
        if (data) {
          this.employeeId = data;
          this.empId = {employeeId: data};
          this.formSearch =  this.buildForm({employeeId: this.employeeId}, {employeeId: [''], insuranceForm: [2]});
          this.processSearch();
        }
      }
    );
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
  }

  /**
   * prepareUpdate
   * param item
   */
  prepareSaveOrUpdate(item) {
    if (item && item.empHealthInsuranceId > 0) {
      this.empHealthInsuranceService.findOne(item.empHealthInsuranceId)
        .subscribe(res => {
          this.activeModel(res.data);
        });
    } else {
      this.activeModel(this.empId);
    }
  }

  /**
   * show model
   * data
   */
  private activeModel(data?: any) {
    const modalRef = this.modalService.open(FamilyRelationshipHealthInsuranceFormComponent, DEFAULT_MODAL_OPTIONS);
    if (data) {
      modalRef.componentInstance.setFormValue(this.propertyConfigs, data);
    }
    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
      if (this.empHealthInsuranceService.requestIsSuccess(result)) {
        this.processSearch();
      }
    });
  }

  public downloadFile(fileData) {
    this.fileStorage.downloadFile(fileData.id).subscribe(res => {
        saveAs(res , fileData.fileName);
    });
  }
}
