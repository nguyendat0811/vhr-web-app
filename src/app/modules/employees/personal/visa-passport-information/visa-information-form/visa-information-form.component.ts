import { BaseComponent } from './../../../../../shared/components/base-component/base-component.component';
import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup , Validators } from '@angular/forms';
import { APP_CONSTANTS, SysCatService, RESOURCE, ACTION_FORM } from '@app/core';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { AppComponent } from '@app/app.component';
import { ValidationService } from '@app/shared/services/validation.service';
import { ActivatedRoute } from '@angular/router';
import { FileControl } from '@app/core/models/file.control';
import { EmpVisaService } from '@app/core/services/hr-employee/emp-visa.service';

@Component({
  selector: 'visa-information-form',
  templateUrl: './visa-information-form.component.html'
})
export class VisaInformationFormComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  visaTypeList: any;
  passportNumberList: any;
  formConfig = {
    visaId: [null],
    employeeId: [ null],
    effectiveDate: [ null, [ValidationService.required]],
    expiredDate: [ null],
    entries: [ null, [ValidationService.required]],
    issuedDate: [ null, [ValidationService.required, ValidationService.beforeCurrentDate]],
    issuedPlace: [ null, [ValidationService.maxLength(100), ValidationService.required]],
    visaNumber: [null, [ValidationService.maxLength(15), ValidationService.required]],
    issuedHolderPassportNumber: [null, [ValidationService.maxLength(20), ValidationService.required]],
    visaTypeId: [ null , [ValidationService.required]],
    note: [ null, [ ValidationService.maxLength(2000)]],
  };
  constructor(public actr: ActivatedRoute
    , public activeModal: NgbActiveModal
    , private empVisaService: EmpVisaService
    , private app: AppComponent
    , private sysCatService: SysCatService) {
      super(actr, RESOURCE.VISA_PASSPORT_INFORMATION, ACTION_FORM.VISA_PASSPORT_INSERT);
      this.formSave = this.buildForm({}, this.formConfig, ACTION_FORM.VISA_PASSPORT_INSERT
        , [ValidationService.notAffter('issuedDate', 'effectiveDate', 'app.contractProcess.effectiveDate')
        , ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.contractProcess.expiredDate')]);
      this.formSave.addControl('file', new FileControl(null));
      this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.VISA).subscribe(res => {
        this.visaTypeList = res.data;
        console.log(res.data);
      });
     }

  ngOnInit() {
  }

  /****************** CAC HAM COMMON DUNG CHUNG ****/
  get f () {
    return this.formSave.controls;
  }

    /**
   * setFormValue
   * param data
   */
  public setFormValue(propertyConfigs: any, data: any) {
    this.propertyConfigs = propertyConfigs;
    this.passportNumberList = this.empVisaService.getListPassportNumber(data.employeeId).subscribe(res => {
      this.passportNumberList = res;
    });
    if (data && data.visaId > 0) {
      this.formSave = this.buildForm(data, this.formConfig
        , ACTION_FORM.VISA_PASSPORT_UPDATE
        , [ValidationService.notAffter('issuedDate', 'effectiveDate', 'app.contractProcess.effectiveDate')
        , ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.contractProcess.expiredDate')]);
    } else {
      this.formSave = this.buildForm(data, this.formConfig
        , ACTION_FORM.VISA_PASSPORT_INSERT
        , [ValidationService.notAffter('issuedDate', 'effectiveDate', 'app.contractProcess.effectiveDate')
        , ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.contractProcess.expiredDate')]);
    }
    this.formSave.addControl('file', new FileControl(null));
    if (data.fileAttachment) {
      (this.formSave.controls['file'] as FileControl).setFileAttachment(data.fileAttachment.empVisaFile);
    }
  }

  /**
   * processSaveOrUpdate
   */
  processSaveOrUpdate() {
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    } else {
      this.app.confirmMessage('app.visaPassportInformation.confirm', () => {// on rejected
        this.empVisaService.saveOrUpdateFormFile(this.formSave.value)
        .subscribe(res => {
          if (this.empVisaService.requestIsSuccess(res)) {
            this.activeModal.close(res);
          }
        });
      }, () => {// on rejected

      });
    }
  }
}
