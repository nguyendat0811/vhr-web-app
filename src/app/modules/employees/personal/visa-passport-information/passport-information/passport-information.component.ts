import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DEFAULT_MODAL_OPTIONS, RESOURCE, ACTION_FORM  } from '@app/core/app-config';
import { ActivatedRoute } from '@angular/router';
import { EmployeeResolver } from '@app/shared/services/employee.resolver';
import { EmpPassportService } from '../../../../../core/services/hr-employee/emp-passport.service';
import { FileStorageService } from '@app/core/services/file-storage.service';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { saveAs } from 'file-saver';
import { PassportInformationFormComponent } from '../passport-information-form/passport-information-form.component';
@Component({
  selector: 'passport-information',
  templateUrl: './passport-information.component.html'
})
export class PassportInformationComponent extends BaseComponent implements OnInit {
  employeeId: number;
  empId: {employeeId: any};
  @ViewChild('ptable') dataTable: any;
  constructor(private modalService: NgbModal
    , private empPassportService: EmpPassportService
    , private employeeResolver: EmployeeResolver
    , private fileStorage: FileStorageService
    , public actr: ActivatedRoute) {

      super(actr, RESOURCE.VISA_PASSPORT_INFORMATION, ACTION_FORM.VISA_PASSPORT_SEARCH);
      this.setMainService(empPassportService);
      this.employeeResolver.EMPLOYEE.subscribe(
        data => {
          if (data) {
            this.employeeId = data;
            this.empId = {employeeId: data};
            this.formSearch =  this.buildForm({employeeId: this.employeeId}, {employeeId: [''], passportForm: [1]});
            this.processSearch();
            console.log(this.resultList);
          }
        }
      );
     }

  ngOnInit() {
  }

    /**
   * prepareUpdate
   * param item
   */
  prepareSaveOrUpdate(item) {
    console.log(item);
    if (item && item.passportId > 0) {
      this.empPassportService.findOne(item.passportId)
        .subscribe(res => {
          this.activeModel(res.data);
        });
    } else {
      this.activeModel({employeeId: this.employeeId});
    }
  }

  /**
   * show model
   * data
   */
  private activeModel(data?: any) {
    const modalRef = this.modalService.open(PassportInformationFormComponent, DEFAULT_MODAL_OPTIONS);
    if (data) {
      modalRef.componentInstance.setFormValue(this.propertyConfigs, data);
    }
    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
      if (this.empPassportService.requestIsSuccess(result)) {
        this.processSearch();
      }
    });
  }

  public downloadFile(fileData) {
    this.fileStorage.downloadFile(fileData.id).subscribe(res => {
        saveAs(res , fileData.fileName);
    });
  }
}
