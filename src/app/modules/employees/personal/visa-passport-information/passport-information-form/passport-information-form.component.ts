import { BaseComponent } from './../../../../../shared/components/base-component/base-component.component';
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup , Validators } from '@angular/forms';
import { APP_CONSTANTS, SysCatService, RESOURCE, ACTION_FORM , NationService} from '@app/core';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { AppComponent } from '@app/app.component';
import { ValidationService } from '@app/shared/services/validation.service';
import { ActivatedRoute } from '@angular/router';
import { FileControl } from '@app/core/models/file.control';
import { EmpPassportService } from '@app/core/services/hr-employee/emp-passport.service';

@Component({
  selector: 'passport-information-form',
  templateUrl: './passport-information-form.component.html'
})
export class PassportInformationFormComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  passportTypeList: any;
  listNation: any;
  formConfig = {
    passportId: [null],
    employeeId: [ null],
    expiredDate: [ null],
    issuedDate: [ null, [ValidationService.required, ValidationService.beforeCurrentDate]],
    passportNumber: [null, [ValidationService.maxLength(15), ValidationService.required]],
    passportTypeId: [ null , [ValidationService.required]],
    issuedCountry: [ null, [ValidationService.required]],
    issuedAuthority: [ null, [ValidationService.maxLength(100), ValidationService.required]],
    note: [ null, [ ValidationService.maxLength(2000)]],
  };
  constructor(public actr: ActivatedRoute
            , public activeModal: NgbActiveModal
            , private empPassportService: EmpPassportService
            , private app: AppComponent
            , private sysCatService: SysCatService
            , private nationService: NationService) {
    super(actr, RESOURCE.VISA_PASSPORT_INFORMATION, ACTION_FORM.VISA_PASSPORT_INSERT);
    this.formSave = this.buildForm({}, this.formConfig, ACTION_FORM.VISA_PASSPORT_INSERT
      , [ValidationService.notAffter('issuedDate', 'expiredDate', 'app.contractProcess.expiredDate')]);
    this.formSave.addControl('file', new FileControl(null));
    this.nationService.getNationList().subscribe(res => {
      this.listNation = res.data;
    });
    this.sysCatService.getAllBySysCatTypeCode(APP_CONSTANTS.SYS_CAT_TYPE_COCE.PASSPORT).subscribe(res => {
      this.passportTypeList = res.data;
    });
  }

  ngOnInit() {
  }

  /****************** CAC HAM COMMON DUNG CHUNG ****/
  get f () {
    return this.formSave.controls;
  }

    /**
   * setFormValue
   * param data
   */
  public setFormValue(propertyConfigs: any, data: any) {
    this.propertyConfigs = propertyConfigs;
    if (data && data.passportId > 0) {
      this.formSave = this.buildForm(data, this.formConfig
        , ACTION_FORM.VISA_PASSPORT_UPDATE
        , [ValidationService.notAffter('issuedDate', 'expiredDate', 'app.contractProcess.expiredDate')]);
    } else {
      this.formSave = this.buildForm(data, this.formConfig
        , ACTION_FORM.VISA_PASSPORT_INSERT
        , [ValidationService.notAffter('issuedDate', 'expiredDate', 'app.contractProcess.expiredDate')]);
    }
    this.formSave.addControl('file', new FileControl(null));
    console.log(data);
    if (data.fileAttachment) {
      (this.formSave.controls['file'] as FileControl).setFileAttachment(data.fileAttachment.empPassportFile);
    }
  }

  /**
   * processSaveOrUpdate
   */
  processSaveOrUpdate() {
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    } else {
      const formCheck = {};
      formCheck['passportId'] = this.formSave.value.passportId;
      formCheck['passportNumber'] = this.formSave.value.passportNumber;
      formCheck['issuedCountry'] = this.formSave.value.issuedCountry;
      formCheck['passportTypeId'] = this.formSave.value.passportTypeId;

      this.empPassportService.checkDuplicateNumber(formCheck).subscribe(check => {
        if (check.type === 'WARNING') {
         return;
        } else {
          this.app.confirmMessage('app.visaPassportInformation.confirm', () => {// on rejected
            this.empPassportService.saveOrUpdateFormFile(this.formSave.value)
            .subscribe(res => {
              if (this.empPassportService.requestIsSuccess(res)) {
                this.activeModal.close(res);
              }
            });
          }, () => {// on rejected

          });
        }
      });
    }
  }
}
