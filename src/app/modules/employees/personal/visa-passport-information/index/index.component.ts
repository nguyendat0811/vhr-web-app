import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { RESOURCE, ACTION_FORM } from '@app/core';

@Component({
  selector: 'index',
  templateUrl: './index.component.html'
})
export class IndexComponent extends BaseComponent implements OnInit {

  constructor(
    public actr: ActivatedRoute,
  ) { 
    super(actr, RESOURCE.VISA_PASSPORT_INFORMATION, ACTION_FORM.SEARCH);
  }
  
  ngOnInit() {
  }

}
