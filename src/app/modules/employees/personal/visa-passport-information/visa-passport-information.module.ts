import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@app/shared';

import { VisaPassportInformationRoutingModule } from './visa-passport-information-routing.module';
import { IndexComponent } from './index/index.component';
import { PassportInformationComponent } from './passport-information/passport-information.component';
import { VisaInformationComponent } from './visa-information/visa-information.component';
import { PassportInformationFormComponent } from './passport-information-form/passport-information-form.component';
import { VisaInformationFormComponent } from './visa-information-form/visa-information-form.component';

@NgModule({
  // tslint:disable-next-line:max-line-length
  declarations: [IndexComponent, PassportInformationComponent, VisaInformationComponent, PassportInformationFormComponent, VisaInformationFormComponent],
  imports: [
    CommonModule,
    SharedModule,
    VisaPassportInformationRoutingModule
  ],
  entryComponents: [PassportInformationFormComponent, VisaInformationFormComponent]
})
export class VisaPassportInformationModule { }
