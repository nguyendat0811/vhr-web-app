import { DEFAULT_MODAL_OPTIONS } from '@app/core/app-config';
import { ActivatedRoute } from '@angular/router';
import { EmployeeResolver } from './../../../../../shared/services/employee.resolver';
import { FileStorageService } from './../../../../../core/services/file-storage.service';
import { BaseComponent } from './../../../../../shared/components/base-component/base-component.component';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RESOURCE, ACTION_FORM } from '@app/core';
import { EmpVisaService } from '@app/core/services/hr-employee/emp-visa.service';
import { VisaInformationFormComponent } from '../visa-information-form/visa-information-form.component';
import { CommonUtils } from '@app/shared/services/common-utils.service';
@Component({
  selector: 'visa-information',
  templateUrl: './visa-information.component.html'
})
export class VisaInformationComponent extends BaseComponent implements OnInit {
  employeeId: number;
  empId: {employeeId: any};
  passportNumberList: any;

  constructor(
    private modalService: NgbModal
    , private fileStorage: FileStorageService
    , private empVisaService: EmpVisaService
    , private employeeResolver: EmployeeResolver
    , public actr: ActivatedRoute
  ) {
    super(actr, RESOURCE.VISA_PASSPORT_INFORMATION, ACTION_FORM.VISA_PASSPORT_SEARCH);
    this.setMainService(empVisaService);
    this.employeeResolver.EMPLOYEE.subscribe(
      data => {
        if (data) {
          this.employeeId = data;
          this.empId = {employeeId: data};
          this.formSearch =  this.buildForm({employeeId: this.employeeId}, {employeeId: []});
          this.processSearch();
        }
      }
    );

   }

  ngOnInit() {
  }

  /* Action Modal*/
  private activeModel(data?: any) {
    const modalRef = this.modalService.open(VisaInformationFormComponent, DEFAULT_MODAL_OPTIONS);
    if (data) {
      modalRef.componentInstance.setFormValue(this.propertyConfigs, data);
    }
    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
      if (this.empVisaService.requestIsSuccess(result)) {
        this.processSearch();
      }
    });
  }

  /**
   * prepareUpdate
   * param item
   */
  prepareSaveOrUpdate(item) {
    if (item && item.visaId > 0) {
      this.empVisaService.findOne(item.visaId)
        .subscribe(res => {
          this.activeModel(res.data);
        });
    } else {
      this.activeModel({employeeId: this.employeeId});
    }
  }

  public downloadFile(fileData) {
    this.fileStorage.downloadFile(fileData.id).subscribe(res => {
        saveAs(res , fileData.fileName);
    });
  }

}
