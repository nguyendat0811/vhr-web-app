import { LongLeaveProcessRoutingModule } from './long-leave-process-routing.module';
import { SharedModule } from '@app/shared';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LongLeaveProcessPageComponent } from './long-leave-process-page/long-leave-process-page.component';
import { LongLeaveProcessListComponent } from './long-leave-process-list/long-leave-process-list.component';
import { LongLeaveProcessFormComponent } from './long-leave-process-form/long-leave-process-form.component';

@NgModule({
  declarations: [LongLeaveProcessPageComponent, LongLeaveProcessListComponent, LongLeaveProcessFormComponent],
  imports: [
    CommonModule,
    SharedModule,

    LongLeaveProcessRoutingModule
  ],
  entryComponents: [LongLeaveProcessFormComponent],
})
export class LongLeaveProcessModule { }
