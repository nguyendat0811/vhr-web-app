import { CommonUtils } from '@app/shared/services/common-utils.service';
import { PropertyResolver } from '@app/shared/services/property.resolver';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LongLeaveProcessPageComponent } from './long-leave-process-page/long-leave-process-page.component';
import { RESOURCE } from '@app/core';

const routes: Routes = [
  {
    path: '',
    component: LongLeaveProcessPageComponent,
    resolve: {
      props: PropertyResolver
    },
    data: {
      resource: RESOURCE.LONG_LEAVE_PROCESS,
      nationId: CommonUtils.getNationId()
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LongLeaveProcessRoutingModule { }
