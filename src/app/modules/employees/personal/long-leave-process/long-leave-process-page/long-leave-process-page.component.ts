import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { ActivatedRoute } from '@angular/router';
import { RESOURCE } from '@app/core';

@Component({
  selector: 'long-leave-process-page',
  templateUrl: './long-leave-process-page.component.html',
})
export class LongLeaveProcessPageComponent extends BaseComponent implements OnInit {

  constructor(public act: ActivatedRoute) { 
    super(act, RESOURCE.LONG_LEAVE_PROCESS);
  }

  ngOnInit() {
  }

}
