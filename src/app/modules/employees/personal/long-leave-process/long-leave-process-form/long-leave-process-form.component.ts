import { LongLeaveReasonService } from '@app/core/services/hr-timekeeping/long-leave-reason.service';
import { FileControl } from '@app/core/models/file.control';
import { ValidationService } from './../../../../../shared/services/validation.service';
import { CommonUtils } from '@app/shared/services/common-utils.service';
import { ActivatedRoute } from '@angular/router';
import { AppComponent } from './../../../../../app.component';
import { LongLeaveProcessService } from '../../../../../core/services/long-leave-process/long-leave-process.service';
import { FormGroup, Validators } from '@angular/forms';
import { BaseComponent } from './../../../../../shared/components/base-component/base-component.component';
import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { RESOURCE, ACTION_FORM } from '@app/core';

@Component({
  selector: 'long-leave-process-form',
  templateUrl: './long-leave-process-form.component.html',
})
export class LongLeaveProcessFormComponent extends BaseComponent implements OnInit {
  formSave: FormGroup;
  reasonList = [];
  formConfig = {
    longLeaveId: [null],
    employeeId: [null],
    longLeaveReasonId: [null, [ValidationService.required]],
    isLaborDisciplinary: [null],
    effectiveDate: [null, [ValidationService.required]],
    expiredDate: [null],
    note: [null, [ValidationService.maxLength(500)]],
  };
  constructor(public activeModal: NgbActiveModal
    , private longLeaveProcessService: LongLeaveProcessService
    , private longLeaveReasonService: LongLeaveReasonService
    , private app: AppComponent
    , public actr: ActivatedRoute) {
      super(actr, RESOURCE.LONG_LEAVE_PROCESS, ACTION_FORM.INSERT);
      this.buildForms({}, ACTION_FORM.INSERT);

      this.longLeaveReasonService.findLongLeaveReasonByMarketId().subscribe(
        res => this.reasonList = res.data
      );
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
  }

  /**
   * processSaveOrUpdate
   */
  processSaveOrUpdate() {
    if (!CommonUtils.isValidForm(this.formSave)) {
      return;
    } else {
      this.app.confirmMessage('app.longLeaveProcess.finishPreviousProcess', () => {
        this.longLeaveProcessService.saveOrUpdateFormFile(this.formSave.value)
        .subscribe(res => {
          if (this.longLeaveProcessService.requestIsSuccess(res)) {
          this.activeModal.close(res);
          }
        });
      }, () => {

      });
    }
  }

  /****************** CAC HAM COMMON DUNG CHUNG ****/
  get f () {
    return this.formSave.controls;
  }

  /**
   * buildForm
   */
  private buildForms(data?: any, actionForm?: ACTION_FORM): void {
    this.formSave = this.buildForm(data, this.formConfig, actionForm
    , [ValidationService.notAffter('effectiveDate', 'expiredDate', 'app.longLeaveProcess.expiredDate')]);
    this.formSave.addControl('file', new FileControl(null));
    if (data.fileAttachment) {
      (this.formSave.controls['file'] as FileControl).setFileAttachment(data.fileAttachment.longLeaveFile);
    }
  }

  /**
   * setFormValue
   * param data
   */
  public setFormValue(propertyConfigs: any, data?: any) {
    this.propertyConfigs = propertyConfigs;
    if (data && data.empHealthInsuranceId > 0) {
      this.buildForms(data, ACTION_FORM.UPDATE);
    } else {
      this.buildForms(data, ACTION_FORM.INSERT);
    }
  }
}
