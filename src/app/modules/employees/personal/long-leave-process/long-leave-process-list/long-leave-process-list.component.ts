import { LongLeaveProcessFormComponent } from './../long-leave-process-form/long-leave-process-form.component';
import { ActivatedRoute } from '@angular/router';
import { FileStorageService } from '@app/core/services/file-storage.service';
import { EmployeeResolver } from '@app/shared/services/employee.resolver';
import { LongLeaveProcessService } from './../../../../../core/services/long-leave-process/long-leave-process.service';
import { BaseComponent } from '@app/shared/components/base-component/base-component.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RESOURCE, ACTION_FORM, DEFAULT_MODAL_OPTIONS } from '@app/core';
import { CommonUtils } from '@app/shared/services';

@Component({
  selector: 'long-leave-process-list',
  templateUrl: './long-leave-process-list.component.html',
})
export class LongLeaveProcessListComponent extends BaseComponent implements OnInit {
  resultList: any = {};
  employeeId: number;
  empId: {employeeId: number};
  @ViewChild('ptable') dataTable: any;
  constructor(private modalService: NgbModal
    , private longLeaveProcessService: LongLeaveProcessService
    , private employeeResolver: EmployeeResolver
    , public fileStorage: FileStorageService
    , public actr: ActivatedRoute) {
      super(actr, RESOURCE.LONG_LEAVE_PROCESS, ACTION_FORM.SEARCH);
      this.setMainService(longLeaveProcessService);

      this.employeeResolver.EMPLOYEE.subscribe(
        data => {
          if (data) {
            this.employeeId = data;
            this.empId = {employeeId: data};
            this.formSearch =  this.buildForm({employeeId: this.employeeId}, {employeeId: ['']});
            this.processSearch();
          }
        }
      );
    }

  /**
   * ngOnInit
   */
  ngOnInit() {
  }

  /**
   * prepareUpdate
   * param item
   */
  prepareSaveOrUpdate(item) {
    if (item && item.longLeaveId > 0) {
      this.longLeaveProcessService.findOne(item.longLeaveId)
        .subscribe(res => {
          this.activeModel(res.data);
        });
    } else {
      this.activeModel(this.empId);
    }
  }
  /**
   * show model
   * data
   */
  private activeModel(data?: any) {
    const modalRef = this.modalService.open(LongLeaveProcessFormComponent, DEFAULT_MODAL_OPTIONS);
    if (data) {
      modalRef.componentInstance.setFormValue(this.propertyConfigs, data);
    }
    modalRef.result.then((result) => {
      if (!result) {
        return;
      }
      if (this.longLeaveProcessService.requestIsSuccess(result)) {
        this.employeeResolver.LONG_LEAVE_PROCESS.next(this.employeeId);
        this.processSearch();
      }
    });
  }

  public downloadFile(fileData) {
    this.fileStorage.downloadFile(fileData.id).subscribe(res => {
        saveAs(res , fileData.fileName);
    });
  }

  public processDeletes(item): void {
    if (item && item.longLeaveId > 0) {
      this.longLeaveProcessService.confirmDelete({
        messageCode: null,
        accept: () => {
          this.longLeaveProcessService.deleteById(item.longLeaveId)
          .subscribe(res => {
            if (this.longLeaveProcessService.requestIsSuccess(res)) {
              this.employeeResolver.LONG_LEAVE_PROCESS.next(item.employeeId);
              this.processSearch();
            }
          });
        }
      });
    }
  }
}
