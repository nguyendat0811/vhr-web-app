import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FollowsRoutingModule } from './follows-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    FollowsRoutingModule
  ]
})
export class FollowsModule { }
