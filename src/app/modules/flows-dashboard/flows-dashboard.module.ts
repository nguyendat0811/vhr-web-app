import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SharedModule } from '@app/shared';
import { FlowsDashboardRoutingModule } from './flows-dashboard-routing.module';
import { DiagramAllModule, SymbolPaletteAllModule, OverviewAllModule, DiagramModule } from '@syncfusion/ej2-ng-diagrams';

@NgModule({
  declarations: [DashboardComponent],
  imports: [
    CommonModule,
    SharedModule,
    DiagramModule,
    FlowsDashboardRoutingModule
  ]
})
export class FlowsDashboardModule { }
