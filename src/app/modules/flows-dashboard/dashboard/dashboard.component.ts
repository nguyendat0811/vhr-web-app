import { Component, OnInit, OnChanges, SimpleChanges, SimpleChange, OnDestroy } from '@angular/core';
import { DiagramDashboardComponent } from '@app/shared/components/diagram-dashboard/diagram-dashboard.component';
import { Router, ActivatedRoute, ParamMap, NavigationEnd } from '@angular/router';
import { WorkFlowsService, WfMenuMappingService } from '@app/modules/settings/work-flows/work-flows.service';
import { HelperService } from '@app/shared/services/helper.service';
import { AuthService } from '@app/core/services/auth.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonUtils } from '@app/shared/services';

@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./material.css']
})
export class DashboardComponent extends DiagramDashboardComponent implements OnInit, OnDestroy {
  menus: [];
  dataSource: [];
  mainFunctions: [];
  constructor(public router: Router,
              public activatedRoute: ActivatedRoute,
              public workFlowsService: WorkFlowsService,
              public wfMenuMappingService: WfMenuMappingService,
              public helperService: HelperService,
              public authService: AuthService,
              public modalService: NgbModal
              ) {
    super(activatedRoute, router, workFlowsService, helperService, modalService);
    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      // If it is a NavigationEnd event re-initalise the component
      if (e instanceof NavigationEnd) {
        this.WORKFLOWS_ID = this.activatedRoute.snapshot.params.workFlows;
        this.dataSource = [];
        this.mainFunctions = [];
        if (!this.menus || this.menus.length === 0) {
          this.findAllMenus();
        } else {
          this.initialize();
        }
      }
    });
  }

  ngOnInit() {
  }
  ngOnDestroy() {
    this.diagram.destroy();
    this.diagramTmp.destroy();

  }
  private findAllMenus(): void {
    this.authService.findAllMenus().subscribe(res => {
      if (res.data && res.data.length > 0) {
        this.menus = res.data;
      }
      this.initialize();
    });
  }
  private initialize(): void {
    this.wfMenuMappingService.findNodesByWorkFlowCode(this.WORKFLOWS_ID).subscribe(res => {
      if (res.data && res.data.length > 0) {
        this.dataSource = CommonUtils.pureDataToTreeNode(this.menus, res.data);
        const mainFuncs = this.dataSource.filter(x => x['isMainAction'] === true && x['parentId'] !== null) as any[];
        this.mainFunctions = CommonUtils.sort(mainFuncs, 'sortOrder');
      }
    });
  }
}
