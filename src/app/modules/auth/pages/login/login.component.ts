import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Constants } from '@env/environment';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '@app/core/services/auth.service';
import { HrStorage } from '@app/core/services/HrStorage';
import { EmployeeInfoService } from '@app/core';
import { ValidationService } from '@app/shared/services';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  error: string;
  isLoading: boolean;
  loginForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService,
    private employeeInfoService: EmployeeInfoService,
  ) {
    this.buildForm();
  }

  ngOnInit() {}

  get f () {
    return this.loginForm.controls;
  }

  login() {
    this.isLoading = true;

    const credentials = this.loginForm.value;

    const params = new URLSearchParams();
    params.append('grant_type', 'password');
    params.append('username', credentials.username);
    params.append('password', credentials.password);

    this.authService.actionRequestToken(params).subscribe(res => {
      if ( res.access_token ) {
        const user = this.authService.extractTokenData(res);
        HrStorage.clear();
        HrStorage.setUserToken(user);
        this.authService.actionRequestAuthorities(Constants.applicationCode)
            .subscribe(data => {
              const authorities = data.data;
              if (authorities && authorities.length > 0) {
                user.userPermissionList = authorities[0].permissions;
                user.userMenuList = authorities[0].grantedMenus;
                HrStorage.setUserToken(user);
              }
              this.employeeInfoService.findByEmployeeCode(user.employeeCode)
                  .subscribe(userInfo => {
                    user.userInfo = userInfo.data;
                    HrStorage.setUserToken(user);
                    // Tạm thời
                    this.isLoading = false;
                    this.router.navigate(['/home']);
              });
            });
      } else {
        this.error = 'Invalid username or password';
      }
    } ,
    error => {
      this.error = 'Invalid username or password';
    });
  }

  private buildForm(): void {
    this.loginForm = this.formBuilder.group({
      username: ['077010', ValidationService.required],
      password: ['123456', ValidationService.required]
    });
  }


}
