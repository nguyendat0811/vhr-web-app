import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContentLayoutComponent } from './layouts/content-layout/content-layout.component';
import { CONTENT_ROUTES } from '@app/shared';
import { AuthGuard } from '@app/core';
import { ViewDetailPayslipComponent } from './modules/public-view/view-detail-payslip/view-detail-payslip.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/auth-sso',
    pathMatch: 'full'
  },
  {
    path: '',
    component: ContentLayoutComponent,
    canActivate: [AuthGuard], // Should be replaced with actual auth guard
    children: CONTENT_ROUTES,
    runGuardsAndResolvers: 'always'
  }, {
    path: 'auth-sso',
    loadChildren: './modules/auth-sso/auth-sso.module#AuthSsoModule',
  },
  // {
  //   path: '**', redirectTo: '/home', pathMatch: 'full'
  // },
  {
    path: 'public',
    loadChildren: './modules/public-view/public-view.module#PublicViewModule',
  }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
    exports: [RouterModule],
    providers: []
})
export class AppRoutingModule { }
